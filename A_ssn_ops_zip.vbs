' ****************************************************************
' GENERIC OPS_ZIP.VBS
'
'     Script for zipping and processing Standard Daily, Catchup, 
'     Previous Week Datafeeds and posting to BBS 1 and 2
' 
' Arguments in order (0) Operations Path         e.g. O:\CRESTFAX\
'                    (1) Operations ID           e.g. CRE
'                    (2) BBS Download Directory  e.g. CAB
'                    (3) Packup style            i.e. Nextday or Sameday
' ****************************************************************

  Dim fso,wso,colArgs,fldr,fcol,lastday,Seclast,CRLF

  CRLF = Chr(13) & Chr(10)

  Set colArgs = WScript.Arguments
  opspath = colArgs(0)
  opsid = colArgs(1)
  daypath = opspath & opsid & "\"
  holdpath = opspath & opsid & "HOLD\"
'bbs1path = "X:\modem\" & colArgs(2) & "\"
  bbs2path = "O:\modem\" & colArgs(2) & "\"

'Modify days for unusual weeks i.e. Christmas and Easter
  Seclast = vbThursday
  Lastday = vbFriday
  If colArgs(3) = "Sameday" Then
    Seclast = Lastday
  End If
'  
'  Response = MsgBox("__________________________________" & CRLF & CRLF & _
'                    "               Run " & opsid & "_ZIP script?" & CRLF & _
'                    "__________________________________", vbOkCancel)
'
'  If Response = vbOK Then
    Set fso = CreateObject("Scripting.FileSystemObject")
    Set fldr = fso.GetFolder(daypath)
    Set fcol = fldr.files
    If fcol.Count <> 0 Then 
    
      'If colArgs(3) = "Sameday" Then
        ' Check for Last processing day of week
        Set wso = Wscript.CreateObject("Wscript.Shell")
        ' tidy ahead
        Call Tidy() 
        Call Process()
        If WeekDay(now) = Lastday Then
          'fso.CopyFile bbs1path & opsid & "_WEEK.EXE", bbs1path & opsid & "_PREV.EXE", true
          'fso.CopyFile opspath & opsid & "_CAT.EXE", bbs1path & opsid & "_WEEK.EXE", true
          
          fso.CopyFile bbs2path & opsid & "_WEEK.EXE", bbs2path & opsid & "_PREV.EXE", true
          fso.CopyFile opspath & opsid & "_CAT.EXE", bbs2path & opsid & "_WEEK.EXE", true
          
          'fso.CopyFile bbs1path & opsid & "_WEEK.ZIP", bbs1path & opsid & "_PREV.ZIP", true
          'fso.CopyFile opspath & opsid & "_CAT.ZIP", bbs1path & opsid & "_WEEK.ZIP", true
         
         fso.CopyFile bbs2path & opsid & "_WEEK.ZIP", bbs2path & opsid & "_PREV.ZIP", true
          fso.CopyFile opspath & opsid & "_CAT.ZIP", bbs2path & opsid & "_WEEK.ZIP", true
          fso.DeleteFile(holdpath & "*.*")
        End If
        ' tidy behind
        Call Tidy() 
 
      'End If
    End If
'  End If

'  If Response <> vbOK Then
'    MsgBox("__________________________________" & CRLF & CRLF & _
'           " " & opsid & "_ZIP processing has NOT been run" & CRLF & _
'           "__________________________________")
'  Else
'    If fcol.Count = 0 Then 
'      MsgBox("__________________________________" & CRLF & CRLF & _
'             "            No Files to Process" & CRLF & _
'             "__________________________________")
'    Else
'      MsgBox("__________________________________" & CRLF & CRLF & _
'             "         " & opsid & "_ZIP processing complete" & CRLF & _
'             "__________________________________")
'    End If
'  End If

' **************************************************************

  Sub Process()

    ' copy things
    fso.CopyFile daypath & "*.*", holdpath, true

    'Zip things
    'Y = wso.Run ("O:\utilities\pkzip.exe " & opspath & opsid & "_DAY " & daypath & "*.*", 1, TRUE)
    'Y = wso.Run ("O:\utilities\zip2exe.exe " & opspath & opsid & "_DAY " & opspath, 1, TRUE)
    'Y = wso.Run ("O:\utilities\pkzip.exe " & opspath & opsid & "_CAT " & holdpath & "*.*", 1, TRUE)
    'Y = wso.Run ("O:\utilities\zip2exe.exe " & opspath & opsid & "_CAT " & opspath, 1, TRUE)

    Y = wso.Run ("O:\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a " & opspath & opsid & "_DAY " & daypath & "*.*", 1, TRUE)
    Y = wso.Run ("O:\AUTO\Apps\ZIP\7-zip\7z.exe a -sfx " & opspath & opsid & "_DAY " & opspath, 1, TRUE)
    Y = wso.Run ("O:\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a " & opspath & opsid & "_CAT " & holdpath & "*.*", 1, TRUE)
    Y = wso.Run ("O:\AUTO\Apps\ZIP\7-zip\7z.exe a -sfx " & opspath & opsid & "_CAT " & opspath, 1, TRUE)
   


' ivans debug
'      MsgBox(opspath & opsid & "_DAY.EXE")
'      MsgBox(bbs1path)


    ' Copy things
    'fso.CopyFile opspath & opsid & "_DAY.EXE", bbs1path, true
    'fso.CopyFile opspath & opsid & "_CAT.EXE", bbs1path, true
     fso.CopyFile opspath & opsid & "_DAY.EXE", bbs2path, true
    fso.CopyFile opspath & opsid & "_CAT.EXE", bbs2path, true
    'fso.CopyFile opspath & opsid & "_DAY.ZIP", bbs1path, true
    'fso.CopyFile opspath & opsid & "_CAT.ZIP", bbs1path, true
    fso.CopyFile opspath & opsid & "_DAY.ZIP", bbs2path, true
    'fso.CopyFile opspath & opsid & "_CAT.ZIP", bbs2path, true

  End Sub


  Sub Tidy()
    if fso.fileexists(opspath & opsid & "_DAY.ZIP") then
      fso.DeleteFile(opspath & opsid & "_DAY.ZIP")
    End If
    if fso.fileexists(opspath & opsid & "_DAY.EXE") then
      fso.DeleteFile(opspath & opsid & "_DAY.EXE")
    End If
    if fso.fileexists(opspath & opsid & "_CAT.ZIP") then
      fso.DeleteFile(opspath & opsid & "_CAT.ZIP")
    End If
    if fso.fileexists(opspath & opsid & "_CAT.EXE") then
      fso.DeleteFile(opspath & opsid & "_CAT.EXE")
    End If
  End Sub

' **************************************************************
