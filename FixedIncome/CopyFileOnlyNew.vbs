option explicit


'**************************************************************************
' 		 GENERIC CopyFileOnly.VBS
' 	
' Script for copying files from one directory to another
'
' Arguments in order (0) Source Path
'		     (1) Destination Path
'		     (2) sFileext
'		     (3) Sdate 	
'**************************************************************************

'***** MAIN *****

'***** VARIABLES *****

	dim fso, WshShell, sPath, dPath, sFileext, sFileext2, sdate, numArgs, sYear, lYear, sMonth, sDay, CustomDate
'***** PREPERATION *****

If Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		lYear = mid(Year(Now),1,4)
		sYear = mid(Year(Now),3,4)
		'sTime = (now)



	'CustomDate = lYear & sMonth & sDay 
	
	CustomDate = replace(CustomDate,"YYYY",lYear)
	CustomDate = replace(CustomDate,"YY",sYear)
	CustomDate = replace(CustomDate,"MM",sMonth)
	CustomDate = replace(CustomDate,"DD",sDay)	

	CustomDate = replace(CustomDate,"YYYY-",lYear)
	CustomDate = replace(CustomDate,"YY-",sYear)
	CustomDate = replace(CustomDate,"MM-",sMonth)
	



	numArgs = WScript.Arguments.Count

	sPath = WScript.Arguments.Item(0)
	dPath = WScript.Arguments.Item(1)
	sFileext = WScript.Arguments.Item(2)
	
	
    If wscript.arguments.count > 3 then
	   sFileext = WScript.Arguments.Item(3)	
	end if
	
	CustomDate = WScript.Arguments.Item(2)
	

	Set WshShell = WScript.CreateObject("WScript.Shell")
	Set fso = CreateObject("Scripting.FileSystemObject")


'***** PROCESSING *****


'msgbox sFileext
	if sFileext <> "*" then
	   copyfile(sPath & "*" & sFileext)
	
	Else 
	
	   'sFileext <> "*" Then
	   copyfile(sPath & CustomDate & sFileext)
	
		
	End if	


Private Sub copyfile(sFilename)

'wscript.echo sFilename

'wscript.echo sPath & sFileext	
	'fso.CopyFile(sFilename), dPath
	
on error resume next
fso.CopyFile(sFilename), dPath
if err.number > 0 then
	'msgbox(err.number & " there are no files to copy cnt")
	on error goto 0 
end if	
	
	
	

End Sub
	
'***** END MAIN *****