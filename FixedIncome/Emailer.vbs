		
' ****************************************************************
'Fixed Income Emailer
'
'     Script for emailing Notifications Without Attachments
' 
' Arguments in order (0) Client_ID	         e.g. 1
'                    (1) Source		         e.g. c:\autoexec.bat
'
' ****************************************************************

' **** SUB MAIN ****
	dim fso,f,L1,wso,colArgs,fldr,fcol,lastday,Seclast,CRLF,sSource,sOpspath,sOpspath2,sOpspath3,sTxtfile,sTargfile,sDatename,sArchpath

	CRLF = Chr(13) & Chr(10)

	Set colArgs = WScript.Arguments
 
	sClientID = colArgs(0)
	sSource = colArgs(1)
	sOpspath = "o:\worldequ\wsoexp\"
	sYear = "00"
	sDay = "00"
	sMonth = "00"
	
 
		Set fso = CreateObject("Scripting.FileSystemObject")
		Set wso = Wscript.CreateObject("Wscript.Shell")
	
		'-- Call custom routine by using the sClientID
		'		which was passed via command Line argument(0)
		
				
		Select case sClientID
			
			case "1"
					Call WFI_MorningSteps
			case "2"
					Call WFI_AfternoonSteps
			case "3"
					Call WFI_EveningSteps
			case "4"
					Call WFI_FTP_MorningSteps		
			case "5"
					Call WFI_FTP_AfternoonSteps
			case "6"
					Call WFI_FTP_EveningSteps
			case "7"
					Call WFI_FullSample
			case "8"
					Call PsResearchValidation
			case "9"
					Call SecurityReport
			case "10"
					Call WFI_Full_Weekly
			case "11"
					Call WFI_Full_Weekly_FTP
			case "12"
					Call NonfeedSecurityReport		
					
			case "13"
					Call XRates
					
			case "14"
					Call CDS
			case "15"
					Call WFI_Markit_Russian_New_Issuance
			case "16"
					Call WFI_Factset_PortfolioCoverage
			case "17"
					Call WFI_Factset_Daily_PortfolioCoverage
			case "18"
					Call Candeal_Missing_Reports
			case "19"
					Call Smartstream_Candeal_Missing_Reports
			case "20"
					Call South_America_Coverage_Report
			case "21"
					Call Loomis_WCA_Muni_Coverage
			case "22"
					Call Loomis_Daily_PortfolioCoverage
			case "23"
					Call Loomis_Full_PortfolioCoverage
			case "24"
					Call Loomis_Weekly_PortfolioCoverage
			case "25"
					Call RU_UA_CoverageReport
			case "26"
					Call RBC_EU_US_Feed
			case "27"
					Call Africa_News_Agency_Feed		
			case "28"
					Call Weekly_Missing_Fields_Feed
			case "29"
					Call Incremental_Bond_Feed
			case "30"
					Call Weekly_Missing_ISINs_Feed
			case "31"
					Call Weekly_Counts	
			case "32"
					Call SunFact_Daily
			case "33"
					Call SMF_Not_on_WCA_Coverage		
			Case "34"
					Call MStar_226_Daily_Port_Report
			case "35"
					Call SMF_Not_on_WCA_Coverage_auto
			case "36"
					Call WFI_Documenter_Coverage_Daily
			case "37"
					Call IE_Funds_on_SMF_weekly_Coverage
			case "38"
					Call Northern_Trust_Portfolio_Daily_Coverage
			case "39"
					Call Northern_Trust_Portfolio_Weekly_Coverage		
					
			end select
		
	

' **** END SUB MAIN ****

'**************************************************************
'	Remove all reference to any object created early in this
' script
'



Sub TidyUp()

'*********************************************************
 'Removed for Automation
 	'msg = "Operation has completed"
'msgbox msg,,"DateZip.vbs"
 	if fso.fileexists(sOpspath & sCustomName & ".TXT") then
 		fso.DeleteFile(sOpspath & sCustomName & ".TXT")
 	End If
  Set fso = Nothing
  Set wso = Nothing
End Sub



Sub WFI_MorningSteps()  
   
 '**************************************************************
 '	WFI_MorningSteps for number 1
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com;y.migou@exchange-data.com;fidev@exchange-data.com -ATTACH F:\Checksteps\LocalValidation\MorningValidationSteps\" & sDate & "_MorningValidationSteps.html -SUB WFI Morning Validation CheckSteps")
   
 
end sub

Sub WFI_AfternoonSteps()  
   
 '**************************************************************
 '	WFI_AfternoonSteps for number 2
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com;y.migou@exchange-data.com;fidev@exchange-data.com -ATTACH F:\Checksteps\LocalValidation\AfternoonValidationSteps\" & sDate & "_AfternoonValidationSteps.html -SUB WFI Afternoon Validation CheckSteps")
   'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com -ATTACH H:\FixedIncome\Checksteps\LocalValidation\AfternoonValidationSteps\" & sDate & "_AfternoonValidationSteps.html -SUB WFI Afternoon CheckSteps")
 
end sub

Sub WFI_EveningSteps()  
   
 '**************************************************************
 '	WFI_EveningSteps for number 3
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com;y.migou@exchange-data.com;fidev@exchange-data.com -ATTACH F:\Checksteps\LocalValidation\EveningValidationSteps\" & sDate & "_EveningValidationSteps.html -SUB WFI Evening Validation CheckSteps")
 
end sub


Sub WFI_FTP_MorningSteps()  
   
 '**************************************************************
 '	WFI_FTP_MorningSteps for number 4
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com;y.migou@exchange-data.com;fidev@exchange-data.com -ATTACH F:\Checksteps\FTPValidation\MorningFtpSteps\" & sDate & "_MorningFtpSteps.html -SUB WFI Morning FTP CheckSteps")
 
end sub

Sub WFI_FTP_AfternoonSteps()  
   
 '**************************************************************
 '	WFI_FTP_AfternoonSteps for number 5
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com;y.migou@exchange-data.com;fidev@exchange-data.com -ATTACH F:\Checksteps\FTPValidation\AfternoonFtpSteps\" & sDate & "_AfternoonFtpSteps.html -SUB WFI Afternoon FTP CheckSteps")
   'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com -ATTACH H:\FixedIncome\Checksteps\FTPValidation\AfternoonFtpSteps\" & sDate & "_AfternoonFtpSteps.html -SUB WFI Afternoon FTP CheckSteps")
   
end sub

Sub WFI_FTP_EveningSteps()  
   
 '**************************************************************
 '	WFI_FTP_EveningSteps for number 6
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com;y.migou@exchange-data.com;fidev@exchange-data.com -ATTACH F:\Checksteps\FTPValidation\EveningFtpSteps\" & sDate & "_EveningFtpSteps.html -SUB WFI Evening FTP CheckSteps")
 
end sub


Sub WFI_FullSample()  
   
 '**************************************************************
 '	WFI_FullSample for number 7
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com -ATTACH F:\Scripts\bat\WFISample2013\Portfolio\Output\" &  sDate & "_WFISample_Port.zip -SUB WFI Full Sample Portfolio")
 
end sub

Sub PsResearchValidation()  
   
 '**************************************************************
 '	PsResearchValidation for number 8
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO fixed.income@exchange-data.com;webmaster@exchange-data.com;y.migou@exchange-data.com;j.ellis-hall@exchange-data.com;fidev@exchange-data.com -ATTACH F:\Checksteps\LocalValidation\PsResearchValidation\" & sDate & "_PsResearchValidation.html -SUB PsResearch Upload Validation")
 
end sub




Sub SecurityReport()  
   
 '**************************************************************
 '	SecReport for number 9
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com;y.migou@exchange-data.com;fidev@exchange-data.com -ATTACH F:\Checksteps\SecReport\SecurityPortfolioReport_" & sDate & ".txt -SUB Security Portfolio Report")
 
end sub


Sub WFI_Full_Weekly()  
   
 '**************************************************************
 '	SecReport for number 10
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com;y.migou@exchange-data.com;fidev@exchange-data.com -ATTACH F:\Checksteps\LocalValidation\WFI_Full_Weekly\" & sDate & "_WFI_Full_Weekly.html -SUB Advantage SmartStream Weekly Full Feed")
   
end sub


Sub WFI_Full_Weekly_FTP()  
   
 '**************************************************************
 '	SecReport for number 11
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com;y.migou@exchange-data.com;fidev@exchange-data.com -ATTACH F:\Checksteps\FTPValidation\WFI_Full_Weekly\" & sDate & "_WFI_Full_Weekly.html -SUB Advantage SmartStream Weekly Full Feed FTP")
   
end sub

Sub NonfeedSecurityReport()  
   
 '**************************************************************
 '	SecReport for number 12
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com;fidev@exchange-data.com -ATTACH F:\Checksteps\NonFeedSecReport\SecurityPortfolioReport_" & sDate & ".txt -SUB Non Feed Security Portfolio Report")
 
end sub

Sub XRates()  
   
 '**************************************************************
 '	XRates for number 13
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & "-" & sMonth & "-" & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com;y.migou@exchange-data.com -ATTACH O:\Datafeed\Xrates\feed\daily\" & sDate & "_Rates.txt -SUB XRates Completed")
 
end sub

Sub CDS()  
   
 '**************************************************************
 '	CDS for number 14
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO d.johnson@exchange-data.com -SUB CDS File Not found")
 
end sub

Sub WFI_Markit_Russian_New_Issuance()  
   
 '**************************************************************
 '	SecReport for number 15
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com;Jar.Singh@markit.com; -ATTACH O:\Prodman\Dev\WFI\Feeds\Markit\Russian_New_Issuance\" & sDate & "_RU_List.txt -SUB Russian New Issuance")
   
end sub

Sub WFI_Factset_PortfolioCoverage()  
   
 '**************************************************************
 '	SecReport for number 16
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com;fi@baharinfocons.com;tech@bilav.com;k.herbergs@exchange-data.com;j.ellis-hall@exchange-data.com -ATTACH O:\Prodman\Dev\WFI\Feeds\Factset\Output\PortfolioCoverage\" & sDate & "_PortfolioCoverage.txt -SUB Factset Weekly Portfolio Coverage")
   
end sub


Sub WFI_Factset_Daily_PortfolioCoverage()  
   
 '**************************************************************
 '	SecReport for number 17
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com;j.ellis-hall@exchange-data.com -ATTACH O:\Prodman\Dev\WFI\Feeds\Factset\Output\PortfolioCoverage\" & sDate & "_DailyPortfolioCoverage.txt -SUB Factset Daily Portfolio Coverage")
   
end sub

Sub Candeal_Missing_Reports()  
   
 '**************************************************************
 '	SecReport for number 18
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com -ATTACH O:\Prodman\Dev\WFI\Feeds\Candeal\Output\" & sDate & "_CandealMissingReports.zip -SUB Candeal Missing Reports")
   
   
end sub

Sub Smartstream_Candeal_Missing_Reports()  
   
 '**************************************************************
 '	SecReport for number 19
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO Linda.Coffman@smartstream-stp.com -ATTACH O:\Prodman\Dev\WFI\Feeds\Candeal\Output\" & sDate & "_Counts.txt -SUB Candeal Missing Reports")
   
   
end sub


Sub South_America_Coverage_Report()  
   
 '**************************************************************
 '	SecReport for number 20
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;k.herbergs@exchange-data.com;m.talbi@exchange-data.com;d.moore@exchange-data.com;b.arrach@exchange-data.com;h.boulfoul@exchange-data.com;o.elkhirani@exchange-data.com -ATTACH O:\Prodman\Dev\WFI\Feeds\Internal\Weekly\Output\" & sDate & "_South_America_Weekly_Coverage.txt -SUB South America Weekly Coverage Feed")
   
   
end sub


Sub Loomis_WCA_Muni_Coverage()  
   
 '**************************************************************
 '	SecReport for number 21
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;k.herbergs@exchange-data.com;d.moore@exchange-data.com;j.ellis-hall@exchange-data.com;harikrishnan.g@exchange-data.in;janani.r@exchange-data.in;archana.s@exchange-data.in -ATTACH O:\Prodman\Dev\WFI\Feeds\Internal\Daily\Output\" & sDate & "_Loomis_WCA_Muni_Coverage.xls -SUB Loomis WCA Muni Daily Coverage Feed")
   
   
end sub

Sub Loomis_Daily_PortfolioCoverage()  
   
 '**************************************************************
 '	SecReport for number 22
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;k.herbergs@exchange-data.com;d.moore@exchange-data.com;j.ellis-hall@exchange-data.com;harikrishnan.g@exchange-data.in;janani.r@exchange-data.in;archana.s@exchange-data.in -ATTACH O:\Prodman\Dev\WFI\Feeds\Internal\Daily\Output\" & sDate & "_Loomis_Daily_Portfolio_Coverage.zip -SUB Loomis Daily Portfolio Coverage Not on WCA")
   
   
end sub




Sub Loomis_Weekly_PortfolioCoverage()  
   
 '**************************************************************
 '	SecReport for number 24
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com;j.ellis-hall@exchange-data.com;k.herbergs@exchange-data.com;FI@baharinfocons.com;janak@baharinfocons.com;it@baharinfocons.com;janani.r@exchange-data.in;harikrishnan.g@exchange-data.in -ATTACH O:\Prodman\Dev\WFI\Feeds\Internal\Weekly\Output\" & sDate & "_Loomis_Weekly_Portfolio_Coverage.zip -SUB Loomis Weekly Portfolio Coverage")
   
   
end sub

Sub RU_UA_CoverageReport()  
   
 '**************************************************************
 '	SecReport for number 25
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;k.herbergs@exchange-data.com;a.rusanova@exchange-data.com;d.moore@exchange-data.com -ATTACH O:\Prodman\Dev\WFI\Feeds\Internal\Weekly\Output\" & sDate & "_RU_UA_Coverage.txt -SUB Russia/Ukraine Weekly Coverage Feed")
   
   
end sub


Sub RBC_EU_US_Feed()  
   
 '**************************************************************
 '	SecReport for number 26
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO aysun.parker@rbccm.com -ATTACH O:\Prodman\Dev\WFI\Feeds\RBC\Output\US_EU\" & sDate & "_RBC_US_EU_Delta.zip -SUB RBC US/EU Daily Delta Feed")
   
   
end sub


Sub Africa_News_Agency_Feed()  
   
 '**************************************************************
 '	SecReport for number 27
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO grant.fredericks@africannewsagency.com;caryn.vanniekerk@africannewsagency.com;lisa.devilliers@africannewsagency.com;y.laifa@exchange-data.com;a.shanab@exchange-data.com;j.bloch@exchange-data.com -ATTACH O:\Prodman\Dev\WFI\Feeds\AfricanNewsAgency\Output\" & sDate & "_African_Indicies.xls -SUB African News Agency Daily Indicies Feed")
   
   
   
end sub

Sub Weekly_Missing_Fields_Feed()  
   
 '**************************************************************
 '	SecReport for number 28
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;harikrishnan.g@exchange-data.in;janani.r@exchange-data.in;j.ellis-hall@exchange-data.com;d.moore@exchange-data.com;k.herbergs@exchange-data.com;m.talbi@exchange-data.com -ATTACH F:\Coverage\Weekly\EDI\" & sDate & "_Weekly_Missing_Fields.txt -SUB Weekly Missing Fields " & sDay & "/" & sMonth & "/" & sYear)
   
   
   
end sub



Sub Incremental_Bond_Feed()  
   
 '**************************************************************
 '	SecReport for number 29
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   
   
   sTime = Time
     sTime = int(left(sTime,2))
   
   
      if sTime >= 07 And sTime <= 12  then
     
      'MsgBox (sTime)
      sCustomName = sYear & sMonth & sDay & "_BOND_1"
      
      sInc = "Increment 1"
     
      ElseIf sTime >= 13 And sTime <= 16  then
     
      'MsgBox (sTime)
      sCustomName = sYear & sMonth & sDay & "_BOND_2"
      
      sInc = "Increment 2"
     
      Elseif sTime >= 17 And sTime <= 23  Then
     
      'MsgBox (sTime)
      sCustomName = sYear & sMonth & sDay & "_BOND_3"
      
      sInc = "Increment 3"
     
   End If
   
   'test = O:\Prodman\Dev\WFI\Feeds\Generic\V73\Incremental\Output\" & sCustomName
   
   'MsgBox test
     
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com -ATTACH O:\Prodman\Dev\WFI\Feeds\Generic\V73\Incremental\Output\" & sCustomName & ".txt -SUB Markit " & sInc & " Bond Feed")
   
   
   
end sub


Sub Weekly_Missing_ISINs_Feed()  
   
 '**************************************************************
 '	SecReport for number 30
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com;j.ellis-hall@exchange-data.com;k.herbergs@exchange-data.com;fi@baharinfocons.com -ATTACH F:\Coverage\Weekly\EDI\" & sDate & "_Weekly_Missing_ISINs_Issuers_Created.zip -SUB Weekly Missing WFI ISIN's & Issuers Created " & sDay & "/" & sMonth & "/" & sYear)
   
   
   
end sub

Sub Weekly_Counts()  
   
 '**************************************************************
 '	SecReport for number 31
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com;j.ellis-hall@exchange-data.com;Sumaiya.SahilKhale@ihsmarkit.com;Rikta.DeyKundu@ihsmarkit.com;Saurabh.Sharma@ihsmarkit.com;Harsha.Magaji@ihsmarkit.com;Ankit.Mittal@ihsmarkit.com;ManavPreetSingh.kochar@ihsmarkit.com;mk-vendormanagementreferencedata@markit.com -ATTACH H:\d.moore\Weekly_Coverage_Feeds\" & sDate & "EDI_Weekly_Counts.zip -SUB Weekly Counts " & sDay & "/" & sMonth & "/" & sYear)
   'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com -ATTACH H:\d.moore\Weekly_Coverage_Feeds\" & sDate & "EDI_Weekly_Counts.zip -SUB Weekly Counts " & sDay & "/" & sMonth & "/" & sYear)
    Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com;j.ellis-hall@exchange-data.com;Sumaiya.SahilKhale@ihsmarkit.com;Rikta.DeyKundu@ihsmarkit.com;Saurabh.Sharma@ihsmarkit.com;Harsha.Magaji@ihsmarkit.com;Ankit.Mittal@ihsmarkit.com;ManavPreetSingh.kochar@ihsmarkit.com;mk-vendormanagementreferencedata@markit.com -ATTACH H:\d.moore\Weekly_Coverage_Feeds\" & sDate & "_EDI_Weekly_Counts.zip -SUB EDI Weekly Counts " & sDay & "/" & sMonth & "/" & sYear)
   
   
end sub

Sub SunFact_Daily()  
   
 '**************************************************************
 '	SecReport for number 32
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com;j.ellis-hall@exchange-data.com;k.herbergs@exchange-data.com;harikrishnan.g@exchange-data.in;archana.s@exchange-data.in -ATTACH O:\Datafeed\Debt\SunFact\Output\" & sDate & "_WCA_Conversion_Events.zip -SUB Daily WCA Conversion Events Feed " & sDay & "/" & sMonth & "/" & sYear)
   
   
   
end sub


Sub SMF_Not_on_WCA_Coverage()  
   
 '**************************************************************
 '	SecReport for number 33
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;k.herbergs@exchange-data.com -ATTACH O:\Prodman\Dev\WFI\Feeds\Internal\Quarterly\Output\" & sDate & "_SMF_Not_on_WCA_Coverage.txt -SUB EDI Quarterly SMF Not on WCA Coverage " & sDay & "/" & sMonth & "/" & sYear)
    Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com -SUB EDI SMF Monthly Coverage " & sDay & "/" & sMonth & "/" & sYear & " Available on FTP (/Custom/Bahar/FixedInc/SMF_Monthly_Coverage/)")

   
   
end sub

Sub MStar_226_Daily_Port_Report()  
   
 '**************************************************************
 '	SecReport for number 34
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com -ATTACH F:\Checksteps\SecurityWise\Feed\226\isin\" & sDate & "_MStar_226_Daily_Port_Reports.zip -SUB MStar 226 Daily Port Reports.zip " & sDay & "/" & sMonth & "/" & sYear) 
   
   
   
end sub


Sub SMF_Not_on_WCA_Coverage_auto()  
   
 '**************************************************************
 '	SecReport for number 35
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;k.herbergs@exchange-data.com -ATTACH O:\Datafeed\Debt\Internal\Quarterly\" & sDate & "_SMF_Not_on_WCA_Coverage.zip -SUB EDI Quarterly SMF Not on WCA Coverage " & sDay & "/" & sMonth & "/" & sYear)
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;k.herbergs@exchange-data.com -SUB EDI SMF Monthly Coverage " & sDay & "/" & sMonth & "/" & sYear & " Available on FTP (/Custom/Bahar/FixedInc/SMF_Monthly_Coverage/)")

   
   
end sub


Sub WFI_Documenter_Coverage_Daily()  
   
 '**************************************************************
 '	SecReport for number 36
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com;ManavPreetSingh.kochar@ihsmarkit.com;Harsha.Magaji@ihsmarkit.com -ATTACH O:\Prodman\Dev\WFI\Feeds\Markit\Output\Documenter\" & sDate & "_WFI_Documenter_Coverage.txt -SUB EDI Daily WFI Documenter Coverage " & sDay & "/" & sMonth & "/" & sYear) 
   
   
   
end sub

Sub IE_Funds_on_SMF_weekly_Coverage()  
   
 '**************************************************************
 '	SecReport for number 37
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;t.mccormack@exchange-data.com;harikrishnan.g@exchange-data.in -ATTACH O:\Prodman\Dev\WFI\Feeds\Internal\Weekly\Output\" & sDate & "_IE_Funds_on_SMF_Weekly_Coverage.zip -SUB IE Funds on SMF Weekly Coverage Feed")
   
   
end sub


Sub Northern_Trust_Portfolio_Daily_Coverage()  
   
 '**************************************************************
 '	SecReport for number 38
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;j.ellis-hall@exchange-data.com;fixedincome-support@exchange-data.com; -ATTACH O:\Upload\Acc\398\" & sDate & "_Northern_Trust_Portfolio_Daily_Coverage.zip -SUB Northern Trust Portfolio Daily Coverage Feed " & sDay & "/" & sMonth & "/" & sYear)
   
   
end sub


Sub Northern_Trust_Portfolio_Weekly_Coverage()  
   
 '**************************************************************
 '	SecReport for number 39
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;j.ellis-hall@exchange-data.com;fixedincome-support@exchange-data.com;fi-support-ediv@exchange-data.com;fi-support-edia@exchange-data.com;fi@baharinfocons.com;it@baharinfocons.com -ATTACH O:\Upload\Acc\398\weekly\" & sDate & "_Northern_Trust_Portfolio_Weekly_Coverage.zip -SUB Northern Trust Portfolio Weekly Coverage Feed " & sDay & "/" & sMonth & "/" & sYear)
   
   
end sub
