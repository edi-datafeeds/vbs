' ****************************************************************
' GENERIC FilesEx.VBS
'
'    This Script is used to check files exist and appends to the named log the file path and byte size to be used for comparison.
'    The log files are stored here: "O:\AUTO\logs\" and the final folder name is based on what you parse from the cmdline

' EXAMPLE CMDLINE
' Long Date 20110302| O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618i_not_US_CA\PreLDateSuf.618 -_2 WcaWebload_2
' Short Date 110302 | O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618i_not_US_CA\PreSDateSuf.618 -_2 WcaWebload_2
' Dashed Long Date 2011-04-05 | O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618\PreDLDateSuf.618 - WcaWebload_2
' Dashed Short Date 11-04-05 | O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618\PreDSDateSuf.618 - WcaWebload_2
' Incremental Files yymmdd_2.ext| O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618i_not_US_CA\PreDSDateSuf.618 - WcaWebload_2 i

' CMDLINE BREAKDOWN
' O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs is this file, must be called and fed the following parameters
' Param0: Source File, can be a real word and/or date or use a pseudonym ie, PreLDateSuf (20110209) or PreSDateSuf (110209) to get the date eg, O:\Datafeed\WCA\618\PreldateSuf.618
' Param1: Prefix & Suffix part of the file name. Prefix should be before the "-" and Suffix after (Pre-Suf) If no pre or suf you must include the "-"
' Param2: Task File Name without Ext. This will also become the folder created in "O:\AUTO\logs\". eg, WcaWebload_2 would yeild O:\AUTO\logs\WcaWebload_2\yyyymmdd_WcaWebload_2.txt
' Param3: If the file is an incremental and add "i" as the final argument will get you the increment number based on the time of day. 
'         12:00 to 15:00 = Inc 2 
'	  16:00 to 21:00 = Inc 3
'	  All other hours = Inc 1
' ****************************************************************

option explicit

' **** MAIN ****
	
' **** VARIABLES ****

	
	dim fso, WshShell, numArgs, sFile, sPath, sExt, dFile, dPath, dExt, sMonth, sDay, sYear, Source, Dest, x, y, Pre, sDate,fSource,iByteSize,log,LogFile,sTime,sInc,hline,sline, fDate
	Dim lDate, sAction, sPre, dPre, PreSdate, PreLdate, SdateSuf, SdateLuf, sSuf, fs, dFileEx, sFileEx,objFSO,objReadFile,contents,objFolder,objFile,objTextFile,Act,lYear,inc, hr,i

' **** PREPARATION ****
	
	
	'Checks to see if the correct number of paramters have been set
	
	Set numArgs = WScript.Arguments
	
	
	'Arguments

	Source = numArgs(0)
	Pre = numArgs(1)

	
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		lYear = mid(Year(Now),1,4)
		sYear = mid(Year(Now),3,4)
		sTime = (now)
	
	fDate = lYear & sMonth & sDay
	
		
		
	If numArgs.length = 2 then
	Set WshShell = WScript.CreateObject("WScript.Shell")
        Set fso = CreateObject("Scripting.FileSystemObject")	        
		sAction = numArgs(0)
		log = numArgs(1)
		contents = "<meta name='viewport' content='target-densitydpi=device-dpi'/> <style type=text/css>.note{color: #00688B;font-weight:bold;font-family: verdana;font-size:13px;}.failed {color: #FF3030;font-family: verdana;font-size:13px;}.ok {color: #636363;font-family: verdana;font-size:13px;} p{height:0px; border:solid 0px green; padding:px;} #test1{height:8px; border:0px solid green; padding-top:10px;}</style><p id='test1'><span class=note>" & sAction & " " & Log & " </span></p>"
		
		'LogFile = "\\192.168.2.163\users\y.laifa\FixedIncome\Checksteps\LocalValidation\" & log & "\" & fDate & "_" & log & ".html"
		'Dest = "\\192.168.2.163\users\y.laifa\FixedIncome\Checksteps\LocalValidation\" & log & "\"		
		
		LogFile = "F:\Checksteps\LocalValidation\" & log & "\" & fDate & "_" & log & ".html"
		Dest = "F:\Checksteps\LocalValidation\" & log & "\"		
		
		Call sAppend()
		wscript.Quit
		
	ElseIf numArgs.length = 3 then
	Set WshShell = WScript.CreateObject("WScript.Shell")
        Set fso = CreateObject("Scripting.FileSystemObject")	 
		
		if numArgs(2) = "i" then
			Call getInc()
			sAction = numArgs(0)
			log = numArgs(1) & sInc
			sTime = (now)

			contents = "<meta name='viewport' content='target-densitydpi=device-dpi'/><style type=text/css>.note{color: #00688B;font-weight:bold;font-family: verdana;font-size:13px;}.failed {color: #F00;font-family: verdana;font-size:13px;}.ok {color: #636363;font-family: verdana;font-size:13px;} p{height:0px; border:solid 0px green; padding:px;}</style><p id='test2'><span class=note>" & sAction & " " & Log & " </span></p>"
			
			'LogFile = "\\192.168.2.163\users\y.laifa\FixedIncome\Checksteps\LocalValidation\" & log & "\" & fDate & "_" & log & ".html"
			'Dest = "\\192.168.2.163\users\y.laifa\FixedIncome\Checksteps\LocalValidation\" & log & "\"		
			
			LogFile = "F:\Checksteps\LocalValidation\" & log & "\" & fDate & "_" & log & ".html"
			Dest = "F:\Checksteps\LocalValidation\" & log & "\"
			
			call sAppend()
			wscript.Quit
		else

			log = numArgs(2)
			'assign variable arguments if they exist
			Call splitPre()
			Call splitSource()
			Call FileExists()
			'wscript.quit
		end if
	ElseIf numArgs.length = 4 then
	
	Set WshShell = WScript.CreateObject("WScript.Shell")
    	Set fso = CreateObject("Scripting.FileSystemObject")	        
	inc = numArgs(3)
		log = numArgs(2)
	'assign variable arguments if they exist
		Call splitPre()
		Call getInc()
		Call splitSource()		
		Call FileExists()
	Else 
		msgbox "Syntax Err:Incorrect Number of Arguments"
		wscript.quit
	
	end if
	
'******************************************* Split Source String **********************************************************	
	

	'Split the Source string into fullpath,full filename and ext

	Sub splitSource()
	'msgbox "Source: " & Source
	x = Len(Source)
	for y = x to 1 step -1
		if mid(Source, y, 1) = "\" or mid(Source, y, 1) = "/" then
		    sFile = mid(Source, y+1)
   		   	'MsgBox "sFile: " & sFile
   		    sPath = mid(Source, 1, y-0)
			'MsgBox "sPath: " & sPath
		    exit for
		end if
		
		if mid(Source, y, 1) = "." then
			sExt = mid(Source, y-0)
			'msgbox "sExt: " & sExt
		end if
		
	next 
 		
		if sFile = "PreLDateSuf" & sExt Then
			sDay = datepart("w", now)
            if sDay  = 2  then
            	i=3       
                Call sPreLongDateSuf(i)
            Else i=1
				Call sPreLongDateSuf(i)
 			End if
		end if
		
		
		if sFile = "PreSDateSuf" & sExt then
			sDay = datepart("w", now)
            if sDay  = 2  then
            	i=3  	
				Call sPreShortDateSuf(i)
			Else i=1
				Call sPreShortDateSuf(i)
 			End if	
				
		end If
	
		
		If sFile = "PreDLDateSuf" & sExt then
			sDay = datepart("w", now)
            if sDay  = 2  then
            	i=3  	
				Call sPreLDashedDateSuf(i)
			Else i=1
				Call sPreLDashedDateSuf(i)
 			End if	
		
		End If
		
		
		If sFile = "PreDSDateSuf" & sExt then
			sDay = datepart("w", now)
            if sDay  = 2  then
            	i=3  	
				Call sPreSDashedDateSuf(i)
			Else i=1
				Call sPreSDashedDateSuf(i)
 			End if	
					
		end If
					 
 	end Sub	

'******************************************** Split Prefix-Suffix String ******************************************************	

'FORMAT ANY PREFIX OR SUFFIX APPLIED ON THE COMMAND LINE. NOTE: THE HYPHEN "-" IS ALWAYS REQUIRED ON THE COMMAND LINE BUT IS NOT USED IF NOTHING ADDED BEFORE OR AFTER.
'ONLY CHARACTURES BEFORE AND FATER THE "-" IS USED. I.E. "AL-_1" WOULD YEILD "AL20110328_1.620"

	Sub splitPre()

	  sPre = Pre
	  x = Len(Pre)
	  for y = x to 1 step -1
		if mid(Pre, y, 1) = ":" then
			sPre = mid(Pre, 1, y-1)
				'msgbox "Pre: " & sPre
			sSuf = mid(Pre, y+1, len(pre) - y)
				'msgbox "Suf: " & sSuf
			exit for
		end if
	   next
	
	end Sub
	
	
'****************************** GET INCREMENT NUMBER BASED ON TIME ****************************************************
 
'o:\Datafeed\Equity\620i\PreLDateSuf.620 - eod i
'Increment number based on time of day. Will need to change this if you want to run an inc out of sequence. 
'The "i" will insert the "_" and inc number i.e "_1"

Sub getInc()	

	sTime = Time
	sTime = int(left(sTime,2))
	'msgbox sTime
	
	if numArgs(1)= "123Trans" then
		if sTime >= 12 and sTime <= 14  then 
			sInc = "_1"
			log = numArgs(1) & sInc
		elseif sTime >= 17 and  sTime <= 23  then 
			sInc = "_2"
			log = numArgs(1) & sInc
		else	
			sInc = "_1"
			log = numArgs(1) & sInc
		end if
	ElseIf numArgs(1)= "CABTrans" then
		if sTime >= 11 and sTime <= 13  then 
			sInc = "_1"
			log = numArgs(1) & sInc
	
		elseif 	sTime >= 14 and  sTime <= 15  then 
			sInc = "_2"
			log = numArgs(1) & sInc
		elseif 	sTime >= 16 and  sTime <= 17  then 
			sInc = "_3"
			log = numArgs(1) & sInc
		elseif 	sTime >= 18 and  sTime <= 19  then 
			sInc = "_4"
			log = numArgs(1) & sInc
		else	
			sInc = "_5"
			log = numArgs(1) & sInc
		end If	
	Else
		if sTime >= 12 and sTime <= 16  then 
			sInc = "_2"
			log = numArgs(5) & sInc
	
		elseif 	sTime >= 17 and  sTime <= 23  then 
			sInc = "_3"
			log = numArgs(5) & sInc
		else	
			sInc = "_1"
			log = numArgs(5) & sInc
		end If	
	End If 
sTime = (now)

End Sub 
	
'****************************** SOURCE SHORT Prefix & SUFFIX DATE **********************************************************	

	
	Sub sPreShortDateSuf(i)
		
	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits

if Len(Day(now))=2 then
'If day(now) = vbMonday then
   ndate = dateadd("d", -3, Now)
else
   ndate = dateadd("d", -1, Now)
end if
	if Len(Month(ndate))=1 then 
		sMonth="0" & month(ndate)
	else
		sMonth=month(ndate)
	end if
			  
	if Len(Day(ndate))=1 then 
		sDay = "0" & day(ndate)
	else
		sDay = day(ndate)
	end if
		
		lYear = mid(Year(Now),1,4)
		sYear = mid(Year(Now),3,4)
		sTime = (now)

	sDate = lYear & sMonth & sDay
	
	'''''''''''''''''''pDate = sDate -1
	

		'-- Create the Custom filename
		'msgbox sPre & "= sPre"
		'msgbox sSuf & "= sSuf"
		sFile = sPre & sDate & sSuf & sInc & sExt
	End Sub

'******************************* SOURCE LONG PREFIX & SUFFIX DATE **********************************************************	
	
	Sub sPreLongDateSuf(i)
		
	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits
			  
if Len(Month(Now-i))=1 then 

           sMonth="0" & month(now -i)

        else

           sMonth=month(now -i)

        end if



        if Len(Day(now-i))=1 then 

            sDay = "0" & day(Now -i)

        else

           sDay = day(Now -i)

        end if



        sYear = Year(Now -i)



        ' Build filename

        sDate = sYear & sMonth & sDay

        

       'wScript.echo "Day: " & sDay       

        'WScript.echo "Month: " & sMonth         

        'wScript.echo "Year: " & sYear                    

        'msgbox "Date: " & sDate   





		'-- Create the Custom filename
		'msgbox sPre & "= sPre"
		'msgbox sSuf & "= sSuf"
		sFile = sPre & sDate & sSuf & sInc & sExt
	End Sub
	
	
'******************************* SOURCE Long DASHED PREFIX & SUFFIX DATE **********************************************************	

	'o:\Upload\Acc\185\feed\PreDLDateSuf.txt STANDING_FULL_- eod
		
		Sub sPreLDashedDateSuf(i)
	
	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits
			  
	if Len(Month(Now-i))=1 then 

           sMonth="0" & month(now -i)

        else

           sMonth=month(now -i)

        end if



        if Len(Day(now-i))=1 then 

            sDay = "0" & day(Now -i)

        else

           sDay = day(Now -i)

        end if



        sYear = Year(Now -i)



        ' Build filename

        sDate = sYear & sMonth & sDay

        

       'wScript.echo "Day: " & sDay       

        'WScript.echo "Month: " & sMonth         

        'wScript.echo "Year: " & sYear                    

        'msgbox "Date: " & sDate   

		
		'-- Create the Custom filename
		'msgbox sPre & "= sPre"
		'msgbox sSuf & "= sSuf"
		sFile = sPre & sDate & sSuf & sInc & sExt
	End Sub

'******************************* SOURCE Long DASHED PREFIX & SUFFIX DATE **********************************************************	
	
	
	'o:\Upload\Acc\185\feed\PreDSDateSuf.txt STANDING_FULL_- eod
	
	Sub sPreSDashedDateSuf(i)
			
	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits
			  
	if Len(Month(Now-i))=1 then 

           sMonth="0" & month(now -i)

        else

           sMonth=month(now -i)

        end if



        if Len(Day(now-i))=1 then 

            sDay = "0" & day(Now -i)

        else

           sDay = day(Now -i)

        end if



        sYear = Year(Now -i)



        ' Build filename

        sDate = sYear & sMonth & sDay

        

       'wScript.echo "Day: " & sDay       

        'WScript.echo "Month: " & sMonth         

        'wScript.echo "Year: " & sYear                    

        'msgbox "Date: " & sDate   
		
		
		
		'-- Create the Custom filename
		'msgbox sPre & "= sPre"
		'msgbox sSuf & "= sSuf"
		sFile = sPre & sDate & sSuf &  sInc & sExt
	End Sub

				
'************************ VALIDATE FILES FUNCTION *******************************************************************



'Return 0 if a file exists else -1

Sub FileExists()
'	LogFile = "H:\FixedIncome\Checksteps\LocalValidation\" & log & "\" & lYear & "\" & sMonth & "\" & lYear & sMonth & sDay & "_" & log & ".html"
'	Dest = "H:\FixedIncome\Checksteps\LocalValidation\" & log & "\" & lYear & "\" & sMonth & "\"
	
	'LogFile = "\\192.168.2.163\users\y.laifa\FixedIncome\Checksteps\LocalValidation\" & log & "\" & fDate & "_" & log & ".html"
	'Dest = "\\192.168.2.163\users\y.laifa\FixedIncome\Checksteps\LocalValidation\" & log & "\"
	
	LogFile = "F:\Checksteps\LocalValidation\" & log & "\" & fDate & "_" & log & ".html"
	Dest = "F:\Checksteps\LocalValidation\" & log & "\"

  Set fs = CreateObject("Scripting.FileSystemObject")

  if fs.FileExists(sPath & sFile) = False then
  	
    sFileEx = -1
  else
    sFileEx = 0
  end if

	if  sFileEx = 0 then
	       	iByteSize=GetFileSize
		contents = "<p><span class=ok>" & sTime & " | Bytesize = " & iByteSize & " | File: " & sPath & sFile & "</span></p>"
		'msgbox sPath & sFile & " | Bytesize = " & iByteSize & " | Log File = " & LogFile & sTime
		call sAppend()		
		
	else		
	
		'msgbox sTime & " | " & sPath & sFile & " File Failed to Generate, Please Try Again"
		contents ="<p><span class=failed>" & sTime & " | Failed to Generate File: " & sPath & sFile
		call sAppend()
		
	End if
end Sub	


'***************

'*********************************

'Return the length of a file or -1 if it does not exist

function GetFileSize()
Dim f
  GetFileSize = -1

  Set fs = CreateObject("Scripting.FileSystemObject")

  if fs.FileExists(sPath & sFile) = True then
    set f = fs.GetFile(sPath & sFile)
    GetFileSize = f.size
  end if
  if fs.FolderExists(sPath & sFile) = True then
    set f = fs.GetFolder(sPath & sFile)
    GetFileSize = f.size
  end if

Set f = Nothing
Set fs = Nothing
end function


'********************************
    	
    	
  	'WScript.Echo sFile
  	
  		

Sub sAppend()
		'hr = "<hr>"


'MsgBox "In Act - Append Function"



    	'*******************
    	
		'wscript.echo ("Source: " & sPath & sFile), ("Dest: " & dPath & dFile ), ("Action: " & action), ("Log" & log)
		'************
		'MsgBox sPath & sFile
		
		'***** PROCESSING *****
		
		
		'Read file contents
		
		'Display results
		'wscript.echo contents
			
		
		' Create the File System Object
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		
		' Check that the WriteDirectory folder exists
		If objFSO.FolderExists(Dest) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		 ' Create the WriteDirectory folder if folder does not exist
		   Set objFolder = objFSO.CreateFolder(Dest)
		   'WScript.Echo "Just created " & Dest
		End If
		
		'msgbox "LogFile Test: " & LogFile
		If objFSO.FileExists(LogFile) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		   Set objFile = objFSO.CreateTextFile(LogFile)
		   'Wscript.Echo "Just created " & LogFile
		End If
		
		set objFile = nothing
		set objFolder = nothing
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		Const ForAppending = 8		
		
		Set objTextFile = objFSO.OpenTextFile _
		(LogFile, ForAppending, True)
		
		
		' Writes WriteFile every time you run this VBScript
		objTextFile.WriteLine(contents )
		'objTextFile.WriteLine(hr)
		objTextFile.Close
		
		
	end sub	

		'*************		
			
			
	
	
' **** END MAIN **** 