option explicit


'**************************************************************************
' 		 GENERIC CopyFileOnly.VBS
' 	
' Script for copying files from one directory to another
'
' Arguments in order (0) Source Path
'		     (1) Destination Path
'		     (2) sFileext
'		     (3) Sdate 	
'**************************************************************************

'***** MAIN *****

'***** VARIABLES *****

	dim fso, WshShell, sPath, dPath, sFileext, sFileext2, sdate, numArgs, sYear, lYear, sMonth, sDay, CustomDate, sTime, sName, sInc
'***** PREPERATION *****

If Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		lYear = mid(Year(Now),1,4)
		sYear = mid(Year(Now),3,4)
		'sTime = (now)



	'CustomDate = lYear & sMonth & sDay 
	
	CustomDate = lYear & sMonth & sDay
	'MsgBox CustomDate
	

	sTime = Time
	sTime = int(left(sTime,2))
	'msgbox sTime
	

	
		if sTime >= 07 and sTime <= 12  then 
			sInc = "_1"
		elseif 	sTime >= 13 and  sTime <= 16  then 
			sInc = "_2"
		ElseIf 	sTime >= 17 and  sTime <= 23  Then
			sInc = "_3"
		else	
			sInc = "_1"
		End if	
	
	'MsgBox sTime & sInc


	numArgs = WScript.Arguments.Count

	sPath = WScript.Arguments.Item(0)
	dPath = WScript.Arguments.Item(1)
	sName = WScript.Arguments.Item(2)
	sFileext = WScript.Arguments.Item(3)
	    
	
	'CustomDate = WScript.Arguments.Item(2)
	

	Set WshShell = WScript.CreateObject("WScript.Shell")
	Set fso = CreateObject("Scripting.FileSystemObject")


'***** PROCESSING *****
    MsgBox sPath & CustomDate & "_" & sName & sFileext
	MsgBox sPath & CustomDate & "_" & sName & sInc & sFileext

'msgbox sFileext
	
		  
	   copyfile(sPath & CustomDate & "_" & sName & sFileext)
	   copyfile(sPath & CustomDate & "_" & sName & sInc & sFileext)

	
'	Else 
'	
'	   wscript.arguments.count > 4 then
'	   copyfile(sPath & CustomDate & sFileext & sInc)	
		
	


Private Sub copyfile(sFilename)

'wscript.echo sFilename

'wscript.echo sPath & sFileext	
	'fso.CopyFile(sFilename), dPath
	
on error resume next
fso.CopyFile(sFilename), dPath
if err.number > 0 then
	'msgbox(err.number & " there are no files to copy cnt")
	on error goto 0 
end if	
	
	
	

End Sub
	
'***** END MAIN *****