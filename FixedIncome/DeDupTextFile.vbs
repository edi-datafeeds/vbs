

Const ForReading = 1

Const ForWriting = 2


Set numArgs = WScript.Arguments

		
		fPath = numArgs(0)
		'WScript.Echo fPath
		
		fName = numArgs(1)
		'WScript.Echo fName 
		




Set objDictionary = CreateObject("Scripting.Dictionary")

Set objFSO = CreateObject("Scripting.FileSystemObject")

Set objFile = objFSO.OpenTextFile(fPath & fName, ForReading)

Do Until objFile.AtEndOfStream

    strName = objFile.ReadLine

    If Not objDictionary.Exists(strName) Then

        objDictionary.Add strName, strName

    End If

Loop

objFile.Close

Set objFile = objFSO.OpenTextFile(fPath & fName, ForWriting)

For Each strKey in objDictionary.Keys

    objFile.WriteLine strKey

Next

objFile.Close