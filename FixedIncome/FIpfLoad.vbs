'********************************************************************
'					
'					DATABASE IMPORTER
'
'
' Script for importing tab delimited files into mysql
'
'********************************************************************

Option Explicit

'**** VARIABLES ****

Dim DbServer, DbName, dbconn, resultset

Dim fso, ts, s, textfile1, oFSO_file, fname, objFS2, WshShell, objFSO, oFS1, oFS1_File, oFSO, oFS2, objFSO2, objFSO3
Dim oFSO_Fromfolder

Dim myquery, myapppath, MyArray

Dim AccID, Pftype, Tbltype

Dim Loadfile, Logfile, CONF, SVR

Dim Runpath, ReportPath, SourceDrive, TargetDrive

Dim acttime, startdate, accountname, limitdown, limitup, lastloadmessage, loadfiledate
Dim MatchedCode, totalct, insertct, japdate

Dim noloadflag, sPath, sFile, sFile2, objFile, objFolder

Dim loadfilect, blankrowct, badrowct, updatect, deletedct, WCADailyMissing, WCAPresentCodes, WCAMissingCodes, WCAMissing, WCAPresent, WCAActive, WCAInactive

Dim Title1, Title2, Title3, Title4, Title5, Title6, Code, IssuerName, SecurityDesc, SectyCD, CntryofIncorp, bondsrc  

Dim aFolders, sFolder, oSubF, sSubF

Const ForReading = 1, ForWriting = 2, ForAppending = 8, adOpenStatic = 3, adLockOptimistic = 3

'** PREPARATION **

updatect = 0
insertct = 0
totalct = 0

'Load Argument Data
AccID = WScript.Arguments.Item(0)
Pftype = WScript.Arguments.Item(1)
'Tbltype = WScript.Arguments.Item(2)
SVR = WScript.Arguments.Item(2)
Loadfile = pftype & ".txt"
Set oFSO = CreateObject("scripting.filesystemobject")

'** Hardcoded stuff
Runpath=Replace(WScript.ScriptFullName,WScript.ScriptName,"")
Loadfile="O:\upload\Acc\" &AccID &"\port\" &Loadfile
'MsgBox loadfile
ReportPath="\upload\Acc\" &AccID
SourceDrive="O:"
TargetDrive="N:"
CONF="O:\Auto\Configs\DbServers.cfg"

japdate=Year(Now) &"-" &Mid("0" &Month(now),Len(Month(Now)),2) &"-" &Mid("0" &day(now),Len(day(Now)),2)
'MsgBox japdate
'** check not run already today - if it has, quit task
If oFSO.FileExists("O:\upload\Acc\" &AccID &"\" &pftype &"_"  &japdate &"_Report.txt") Then
  If AccID >= 990 Then
    oFSO.DeleteFile("O:\upload\Acc\" &AccID &"\" &pftype &"_"  &japdate &"_Report.txt")
  Else
    WScript.Quit
  End if
ElseIf oFSO.FileExists("O:\upload\Acc\" &AccID &"\" &pftype &"_"  &japdate &"_Noload.txt") Then
  If AccID >= 990 Then
    oFSO.DeleteFile("O:\upload\Acc\" &AccID &"\" &pftype &"_"  &japdate &"_Noload.txt")
  Else
    WScript.Quit
  End if
End If

'MsgBox ("O:" &ReportPath)

Set oFSO_FromFolder = oFSO.GetFolder("O:" &ReportPath)
Archive_Old_Reports oFSO_FromFolder
  
'** Database Connections
Call OpenDbCon(SVR, CONF)
Set resultset = CreateObject("ADODB.Recordset")

'**** MAIN ****

'** clear valid row counter
loadfilect=0
Blankrowct=0
badrowct=0

'** Does account exist?
resultset.open "select accountname, limitdown," &_
               " limitup, lastloadmessage, convert(varchar(10),loadfiledate,111), insertct, totalct from client.dbo.pfacclog" &_
               " where accid =" &AccID &" and  pftype = '" &pftype &"'" _ 
               , dbconn, adOpenStatic, adLockOptimistic


'** No, so create
If resultset.EOF Then
  dbconn.Execute "insert into client.dbo.pfacclog(accid, pftype, acttime, actflag, startdate, limitup, limitdown) values (" _
                 &AccID &", '" &pftype &"', getdate(), 'I', getdate(), 0, 0)"
  limitdown=50
  limitup=0
  loadfiledate = japdate
  totalct=0
  insertct=0
Else
  '** Yes, get needed values
  accountname=resultset.Fields(0).value
  limitdown=resultset.Fields(1).value
  limitup=resultset.Fields(2).Value
  lastloadmessage=resultset.Fields(3).Value
  loadfiledate=resultset.Fields(4).Value
  insertct=resultset.Fields(5).Value
  totalct=resultset.Fields(6).Value
  '** delete all temp portfolios existing content
  If AccID >= 990 Then
    dbconn.Execute "delete client.dbo.pf" &Pftype &_
          " where accid =" &AccID
  End if
End if  
resultset.Close

If oFSO.FileExists(loadfile) Then
  Set oFS2 = oFSO.GetFile(loadfile)
  '** possible new pf, load in <pftype>_<accid> table, drop and create
  If (Pftype="issid" Or Pftype="secid") Then  
    dbconn.Execute "use client if exists (select * from sysobjects where name = '" &Pftype &"_" &accid &"')" &_
                   " drop table client.dbo." &Pftype &"_" &accid &_
                   " CREATE TABLE client.dbo." &Pftype &"_" &accid &" (mycode int NOT NULL, PRIMARY KEY (mycode))"

  Else
    dbconn.Execute "use client if exists (select * from sysobjects where name = '" &Pftype &"_" &accid &"')" &_
                   " drop table client.dbo." &Pftype &"_" &accid &_
                   " CREATE TABLE client.dbo." &Pftype &"_" &accid &" (mycode varchar(20) NOT NULL, PRIMARY KEY (mycode))"

  End if
  '** loop to load file into appropriate temp file
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set ts = fso.OpenTextFile(loadfile, ForReading)
  Do
    s = Replace(Replace(Replace(Replace(ts.ReadLine,".","A"),Chr(34),""),Chr(39),"")," ","")
    If s = "" Then
      blankrowct=blankrowct+1
    ElseIf (Pftype="issid" Or Pftype="secid") And Not IsNumeric(s) Or s = "0" Then  
       badrowct=badrowct+1
    ElseIf Pftype="isin" And Not len(s)=12 Then  
       badrowct=badrowct+1
    ElseIf Pftype="uscode" And Not len(s)=9 Then  
       badrowct=badrowct+1
    ElseIf Pftype="sedol" And Not len(s)=7 Then  
       badrowct=badrowct+1
    Else
      If (Pftype="issid" Or Pftype="secid") Then  
        loadfilect=loadfilect+1
        dbconn.execute "insert into client.dbo." &Pftype &"_" &AccID &" select '" & s &_
                   "' where " & s &" not in (select mycode from client.dbo." &Pftype &"_" &AccID &")"
      Else                   
        loadfilect=loadfilect+1
        dbconn.execute "insert into client.dbo." &Pftype &"_" &AccID &" select '" & s &_
                       "' where '" & s &"' not in (select mycode from client.dbo." &Pftype &"_" &AccID &")"
      End If
    End If
  Loop while NOT ts.AtEndOfStream
  ts.Close
Else
  noloadflag="T"
  lastloadmessage = "File not found: " &Pftype &".txt"
  Call update_pfacclog
  Call write_pf_stats
  Call clearobjects
  WScript.Quit
End If

If loadfilect=0 Then
  lastloadmessage = "Client portfolio has zero records: "  &Pftype &".txt"
  noloadflag="T"
  Call update_pfacclog
  Call write_pf_stats
  Call clearobjects
  WScript.Quit
End if


' compare totct with count id join between temp and master pf (doesn't include actflag<>'D')
resultset.Open "select count(code) as MatchedCode from client.dbo.pf" &Pftype &_
             " inner join client.dbo." &Pftype &"_" &AccID &" on client.dbo.pf" &Pftype &".code = client.dbo." &Pftype &"_" &AccID &".mycode" &_
             " and client.dbo.pf" &Pftype &".accid =" &AccID &_
             " where client.dbo.pf" &Pftype &".actflag<>'D'"
MatchedCode=resultset.Fields(0).Value
resultset.Close

If totalct=0 Then
  noloadflag="F"
  lastloadmessage="Loaded OK - Firsttime load"
  '** query to load temp table into pf table
  If Pftype="issid" or pftype="secid" then
    dbconn.Execute "insert into pf" &Pftype &_
                   " select distinct " &AccID &", mycode, 'I', getdate(), null" &_
                   " from " &Pftype &"_" &Accid &_
                   " where mycode not in (select code from pf" &Pftype &" where accid =" &AccID &")"
  Else
    dbconn.Execute "insert into pf" &Pftype &_
                   " select distinct " &AccID &", mycode, 'I', getdate(), null" &_
                   " from " &Pftype &"_" &Accid &_
                   " where mycode<>'' and mycode not in (select code from pf" &Pftype &" where accid =" &AccID &")"
  End if                   
  resultset.Open "select count(code) from client.dbo.pf" &Pftype &_
                 " where client.dbo.pf" &Pftype &".actflag = 'I'" &_
                 " and client.dbo.pf" &Pftype &".accid =" &accid
  insertct=resultset.Fields(0).Value
  resultset.Close
ElseIf MatchedCode = totalct And MatchedCode = loadfilect And insertct = 0 Then
   '** log only - leave loadfiledate and counts alone
   noloadflag="F"
   lastloadmessage="Loaded OK - No changes"
ElseIf MatchedCode = totalct And MatchedCode = loadfilect And insertct > 0 Then
   '** log only - leave loadfiledate and counts alone
   noloadflag="F"
   lastloadmessage="Loaded OK - No changes"
   insertct=0
   dbconn.Execute "update client.dbo.pf" &Pftype &_
                  " set actflag='U'" &_
                  " where actflag='I' and accid =" &Accid
ElseIf outside_limits() Then
  '** log only - leave loadfiledate and counts alone
  noloadflag="T"
Else
  noloadflag="F"
  lastloadmessage="Loaded OK - With changes"
  loadfiledate=japdate
  'loadfiledate=Now()
  '** set yesterday's inserts, present today also to updates
  dbconn.Execute "update client.dbo.pf" &Pftype &_
                " set actflag='U'" &_
                " where actflag='I' and accid =" &AccID &_
                " and code in (select code from" &Pftype &"_" &AccID &")"
  '** set existing deleted records, present today to inserts
  dbconn.Execute "update client.dbo.pf" &Pftype &_
                " set actflag='I'" &_
                " where actflag='D' and accid =" &AccID &_
                " and code in (select mycode from client.dbo." &Pftype &"_" &AccID &")"
   '** set any non-deleted records to deleted if not present today
'   msgbox " update pf" &Pftype &_
  dbconn.Execute "update client.dbo.pf" &Pftype &_
                " set actflag='D', lastdeleted=getdate()" &_
                " where accid =" &AccID &" and actflag<>'D'" &_
                " and code not in (select mycode from client.dbo. " &Pftype &"_" &AccID &")"
   '** insert any new codes
  dbconn.Execute "insert into client.dbo.pf" &Pftype &_
                " select distinct " &AccID & ", mycode, 'I', getdate(), null" &_
                " from client.dbo." &Pftype &"_" &AccID &_
                " where mycode<>''" &_
                " and mycode not in (select code from client.dbo.pf" &Pftype &" where accid =" &AccID &")"
End If

'** update query for pfacclog
Call write_pf_stats
'Call TidyReports
Call TotalMissingCodes
Call TotalPresentCodes
Call update_pfacclog
Call clearobjects

WScript.Quit

' **** SUBROUTINES ****
Sub write_pf_stats()
resultset.Open "select count(code) from client.dbo.pf" &Pftype &_
               " where client.dbo.pf" &Pftype &".actflag = 'I'" &_
               " and client.dbo.pf" &Pftype &".accid =" &AccID, dbconn
insertct=CLng(resultset.Fields(0).Value)
resultset.Close

resultset.open "select count(code) from client.dbo.pf" &Pftype &_
               " where client.dbo.pf" &Pftype &".actflag = 'U'" &_
               " and client.dbo.pf" &Pftype &".accid =" &AccID
updatect=CLng(resultset.Fields(0).Value)
resultset.Close

resultset.Open "select count(code) from client.dbo.pf" &Pftype &_
               " where client.dbo.pf" &Pftype &".actflag = 'D'" &_
               " and client.dbo.pf" &Pftype &".accid =" &AccID &_
               " and client.dbo.pf" &Pftype &".lastdeleted > getdate()-0.5"
deletedct=CLng(resultset.Fields(0).Value)
resultset.Close



If Pftype = "sedol" Then 

resultset.Open "Select count(code) from client.dbo.pf" &Pftype &_
			  " where accid =" &AccID &_ 
			  " and code not in (select distinct "&Pftype &" from wca.dbo.sedol)" &_
			  " and actflag <> 'D'" 
	
WCAMissing=CLng(resultset.Fields(0).Value)
resultset.Close


resultset.Open "Select count(code) from client.dbo.pf" &Pftype &_
			  " where accid =" &AccID &_ 
			  " and code not in (select distinct "&Pftype &" from wca.dbo.sedol)" &_
			  " and actflag <> 'D'" &_
			  " and client.dbo.pf" &Pftype &".acttime>(select max(client.dbo.pf" &Pftype &".acttime)-0.05" &_
			  " from client.dbo.pf" &Pftype &" where accid=" &AccID &")"
 		   
WCADailyMissing=CLng(resultset.Fields(0).Value)
resultset.Close





resultset.Open "Select count(code) from client.dbo.pf" &Pftype &_
			  " where accid =" &AccID &_ 
			  " and code in (select distinct "&Pftype &" from wca.dbo.sedol" &_
			  " left outer join wca.dbo.scmst on wca.dbo.sedol.secid = wca.dbo.scmst.secid" &_ 
			  " where wca.dbo.scmst.statusflag <> 'I')" &_
			  " and actflag <> 'D'" 
	
WCAActive=CLng(resultset.Fields(0).Value)
resultset.Close



resultset.Open "Select count(code) from client.dbo.pf" &Pftype &_
			  " where accid =" &AccID &_ 
			  " and code in (select distinct "&Pftype &" from wca.dbo.sedol" &_
			  " left outer join wca.dbo.scmst on wca.dbo.sedol.secid = wca.dbo.scmst.secid" &_ 
			  " where wca.dbo.scmst.statusflag = 'I')" &_
			  " and actflag <> 'D'" 
	
WCAInactive=CLng(resultset.Fields(0).Value)
resultset.Close











Else 

resultset.Open "Select count(code) from client.dbo.pf" &Pftype &_
			   " Left outer join wca.dbo.scmst On code =" &Pftype &_
			   " where client.dbo.pf" &Pftype &".accid =" &AccID &_
			   " and client.dbo.pf" &Pftype &".actflag <> 'D'" &_
			   " and wca.dbo.scmst.secid is null"
 
WCAMissing=CLng(resultset.Fields(0).Value)
resultset.Close




resultset.Open "Select count(code) from client.dbo.pf" &Pftype &_
			   " Left outer join wca.dbo.scmst On code =wca.dbo.scmst." &Pftype  &_
			   " where client.dbo.pf" &Pftype &".accid =" &AccID &_
			   " and client.dbo.pf" &Pftype &".actflag <> 'D'" &_
			   " and wca.dbo.scmst.statusflag <> 'I'"
 
WCAActive=CLng(resultset.Fields(0).Value)
resultset.Close




resultset.Open "Select count(code) from client.dbo.pf" &Pftype &_
			   " Left outer join wca.dbo.scmst On code =wca.dbo.scmst." &Pftype  &_
			   " where client.dbo.pf" &Pftype &".accid =" &AccID &_
			   " and client.dbo.pf" &Pftype &".actflag <> 'D'" &_
			   " and wca.dbo.scmst.statusflag = 'I'"
 
WCAInactive=CLng(resultset.Fields(0).Value)
resultset.Close











resultset.Open "Select count(code) from client.dbo.pf" &Pftype &_
			   " Left outer join wca.dbo.scmst on code =" &Pftype &_
			   " where client.dbo.pf" &Pftype &".accid =" &AccID &_
			   " and client.dbo.pf" &Pftype &".actflag <> 'D'" &_
			   " and wca.dbo.scmst.secid is null" &_
			   " and client.dbo.pf" &Pftype &".acttime>(select max(client.dbo.pf" &Pftype &".acttime)-0.05" &_
			   " from client.dbo.pf" &Pftype &" where accid=" &AccID &")"
 		   
WCADailyMissing=CLng(resultset.Fields(0).Value)
resultset.Close

End if







'MsgBox totalct+totalct
'MsgBox insertct
'MsgBox updatect+insertct
totalct=insertct+updatect
WCAPresent=totalct-WCAMissing


If noloadflag="T" then
  Logfile="O:\upload\Acc\" &AccID &"\" &pftype &"_"  &japdate &"_Noload.txt"
Else
  Logfile="O:\upload\Acc\" &AccID &"\" &pftype &"_"  &japdate &"_Report.txt"
End if

'** Initialise Log
Set oFS1 = CreateObject("Scripting.FileSystemObject")
Set oFS1_File = oFS1.OpenTextFile(Logfile, ForWriting, True)

AppendLog "*************************************************" &vbCrLf
AppendLog "Client portfolio report for: " &japdate &vbCrLf
AppendLog "Client portfolio last changed on: " &Replace(Replace(loadfiledate,"'",""),"/","-") &vbCrLf
AppendLog lastloadmessage &vbCrLf
AppendLog "Number of new codes added: " &insertct &vbCrLf
AppendLog "Number of new codes Missing: " &WCADailyMissing &vbCrLf
'AppendLog "***** " & WCAMissingCodes & " *****" &vbCrLf &vbCrLf
AppendLog "Number of code reloaded: " &updatect &vbcrlf
AppendLog "Number of codes removed: "&deletedct &vbcrlf
AppendLog "Total Number of Missing Codes on WCA: "&WCAMissing &vbCrLf
AppendLog "Total Number of Present Codes on WCA: "&WCAPresent &vbCrLf

AppendLog "Total Number of Active Codes on WCA: "&WCAActive &vbCrLf
AppendLog "Total Number of Inactive Codes on WCA: "&WCAInactive &vbCrLf

AppendLog "Total codes contained in current portfolio: " &totalct &vbcrlf
AppendLog "***************** End of Report *****************" &vbCrLf
oFS1_File.Close
End Sub

'Sub TidyReports




'End Sub 


Sub TotalMissingCodes

Set objFSO2 = CreateObject("Scripting.FileSystemObject")

If Pftype = "sedol" Then 

resultset.Open "Select code from client.dbo.pf" &Pftype &_
			  " where accid =" &AccID &_ 
			  " and code not in (select distinct "&Pftype &" from wca.dbo.sedol)" &_
			  " and actflag <> 'D'" 

Else

resultset.Open "select code from client.dbo.pf" &Pftype &_
			   " Left outer join wca.dbo.scmst on code =" &Pftype &_
			   " where client.dbo.pf" &Pftype &".accid =" &AccID &_
			   " and client.dbo.pf" &Pftype &".actflag <> 'D'" &_
			   " and wca.dbo.scmst.secid is null"
End if			


sPath = "F:\Checksteps\SecurityWise\Feed"
sFolder = sPath & "\" & AccID 
aFolders = Array(Pftype)
sFile = japdate & "_WCAMissingCodes_" & Pftype & ".txt"




Set objFSO2 = CreateObject("Scripting.FileSystemObject")

If objFSO2.FolderExists( sFolder ) Then
    'WScript.Echo "Folder '" & sFolder & "' already exists"
  Else
    objFSO2.CreateFolder sFolder ' easier than Subfolder.Add
  End If
 
 Set oSubF  = objFSO2.GetFolder( sFolder ).SubFolders
   For Each sSubF In aFolders
     If objFSO2.FolderExists( sFolder & "\" & sSubF ) Then
        'msgbox "Folder '" & sSubF & "' already exists"
     Else
        oSubF.Add sSubF
     End If

'MsgBox (sFolder & "\"  & sSubF & "\" & "*.txt")

		'Loop through the Files collection
Set objFolder = objFSO2.GetFolder(sFolder & "\"  & sSubF)

For Each objFile in objFolder.Files
'MsgBox objFile

objFSO2.DeleteFile(objFile)

Next 
	
	
  Set objFile = objFSO2.CreateTextFile(sFolder & "\"  & sSubF & "\" & sFile)
  'msgbox (sFolder & "\"  & sSubF & "\" & sFile)
  Set objFile = Nothing

Set objFile = objFSO2.OpenTextFile(sFolder & "\" & sSubF & "\" & sFile , ForAppending, True)
' Writes strText every time you run this VBScript
 
      
   objFile.WriteLine "*************************************************" &vbCrLf
   objFile.WriteLine "WCA Missing Codes Report for: " &japdate &vbCrLf
   
  
   Next
  

  Do While NOT resultset.Eof  
  
  WCAMissingCodes=resultset.Fields(0).Value
  'Wscript.Echo WCAMissingCodes


  objFile.WriteLine(WCAMissingCodes)
  resultset.MoveNext 
  Loop 


  objFile.WriteLine vbCrLf& "***************** End of Report *****************" &vbCrLf
  objFile.Close



  resultset.Close
  'Set resultset=nothing


  'MsgBox "File1 complete"

End Sub




Sub TotalPresentCodes

Set objFSO2 = CreateObject("Scripting.FileSystemObject")

If Pftype = "sedol" Then 

resultset.Open "select code, wca.dbo.issur.IssuerName, wca.dbo.scmst.SecurityDesc," &_
			  " wca.dbo.scmst.SectyCD, wca.dbo.issur.CntryofIncorp," &_
			  " case when wca.dbo.bond.bondsrc='' or wca.dbo.bond.bondsrc is null then 'Source Not Available' else wca.dbo.bond.bondsrc end as bondsrc" &_
			  " from client.dbo.pf" &Pftype &_
			  " Left outer join wca.dbo.sedol on code =" &Pftype &_
			  " Left outer join wca.dbo.scmst on wca.dbo.sedol.secid = wca.dbo.scmst.secid" &_
			  " Left outer join wca.dbo.issur on wca.dbo.issur.issid = wca.dbo.scmst.issid" &_
			  " Left outer join wca.dbo.bond on wca.dbo.scmst.secid = wca.dbo.bond.secid" &_
			  " where client.dbo.pf" &Pftype& ".actflag <> 'D'" &_
			  " and client.dbo.pf" &Pftype& ".accid =" &AccID &_
			  " and wca.dbo.scmst.secid is not Null" &_
			  " and wca.dbo.bond.bondsrc <> 'PR'" &_
			  " and wca.dbo.bond.bondsrc <> 'PS'" &_
			  " and wca.dbo.bond.bondsrc <> 'OC'" &_
			  " and wca.dbo.bond.bondsrc <> 'DE'"			  

Else


resultset.Open "select code, wca.dbo.issur.IssuerName, wca.dbo.scmst.SecurityDesc," &_
			  " wca.dbo.scmst.SectyCD, wca.dbo.issur.CntryofIncorp," &_
			  " case when wca.dbo.bond.bondsrc='' or wca.dbo.bond.bondsrc is null then 'Source Not Available' else wca.dbo.bond.bondsrc end as bondsrc" &_
			  " from client.dbo.pf" &Pftype &_			  
              " Left outer join wca.dbo.scmst on code =" &Pftype &_
			  " left outer join wca.dbo.bond on wca.dbo.scmst.secid = wca.dbo.bond.secid" &_
			  " left outer join wca.dbo.issur on wca.dbo.scmst.issid = wca.dbo.issur.issid" &_
			  " where client.dbo.pf" &Pftype& ".actflag <> 'D'" &_
			  " and client.dbo.pf" &Pftype& ".accid =" &AccID &_
			  " and wca.dbo.scmst.secid is not Null" &_
			  " and wca.dbo.bond.bondsrc <> 'PR'" &_
			  " and wca.dbo.bond.bondsrc <> 'PS'" &_
			  " and wca.dbo.bond.bondsrc <> 'OC'" &_
			  " and wca.dbo.bond.bondsrc <> 'DE'"


End If
			

sPath = "F:\Checksteps\SecurityWise\Feed"
sFolder = sPath & "\" & AccID 
'aFolders = Array( "SecurityWise\" & Pftype & "\")
aFolders = Array(Pftype)
sFile2 = japdate & "_WCAPresentCodes_" & Pftype & ".txt"

If objFSO2.FolderExists( sFolder ) Then
    'WScript.Echo "Folder '" & sFolder & "' already exists"
  Else
    objFSO2.CreateFolder sFolder ' easier than Subfolder.Add
  End If
 
 Set oSubF  = objFSO2.GetFolder( sFolder ).SubFolders
   For Each sSubF In aFolders
     If objFSO2.FolderExists( sFolder & "\" & sSubF ) Then
        'msgbox "Folder '" & sSubF & "' already exists"
     Else
        oSubF.Add sSubF
     End If

	
	
  Set objFile = objFSO2.CreateTextFile(sFolder & "\"  & sSubF & "\" & sFile2)
  'msgbox (sFolder & "\"  & sSubF & "\" & sFile)
  Set objFile = Nothing

Set objFile = objFSO2.OpenTextFile(sFolder & "\" & sSubF & "\" & sFile2 , ForAppending, True)
' Writes strText every time you run this VBScript
 
   Title1 = resultset.Fields(0).Name
   Title2 = resultset.Fields(1).Name
   Title3 = resultset.Fields(2).Name
   Title4 = resultset.Fields(3).Name
   Title5 = resultset.Fields(4).Name
   Title6 = resultset.Fields(5).Name
 
      
   objFile.WriteLine "****************************************************************************************************************************" &vbCrLf
   objFile.WriteLine "WCA Present Codes Report for: " &japdate &vbCrLf
   'WScript.Echo Title1 & vbTab & Title2 & vbTab & Title3 & vbTab & Title4 & vbTab & Title5 & vbTab & Title6  
   objFile.WriteLine(Title1 & vbTab & Title2 & vbTab & Title3 & vbTab & Title4 & vbTab & Title5 & vbTab & Title6) &vbCrLf
  
  
   Next
  

  Do While NOT resultset.Eof  
    
  
  Code = resultset.Fields(0).Value
  IssuerName = resultset.Fields(1).Value
  SecurityDesc = resultset.Fields(2).Value
  SectyCD = resultset.Fields(3).Value
  CntryofIncorp = resultset.Fields(4).Value
  bondsrc = resultset.Fields(5).Value
    
  
  'WScript.Echo Code & vbTab & IssuerName & vbTab & SecurityDesc & vbTab & SectyCD & vbTab & CntryofIncorp & vbTab & bondsrc
  objFile.WriteLine(Code & vbTab & IssuerName & vbTab & SecurityDesc & vbTab & SectyCD & vbTab & CntryofIncorp & vbTab & bondsrc) 
  resultset.MoveNext 
  Loop 


  objFile.WriteLine vbCrLf& "******************************************************* End of Report ******************************************************" &vbCrLf
  objFile.Close

  resultset.Close
  
  'MsgBox "File2 complete"

  End Sub
  






Function outside_limits()
If limitup=0 And limitdown=0 Then
  outside_limits=False
Else
  If limitdown<>0 And loadfilect<totalct and loadfilect*100/totalct<limitdown then
    lastloadmessage = loadfilect*100/totalct &"% decrease exceeded current limit of " &limitdown
    outside_limits=True
  ElseIf limitup<>0 And loadfilect>totalct and (loadfilect-totalct)*100/totalct>limitup then
    lastloadmessage = (loadfilect-totalct)*100/totalct &"% increase exceeded current limit of " &limitup
    outside_limits=True
  Else
    outside_limits=False
  End If
End If
End Function

Sub update_pfacclog()
If loadfiledate  = "" Then
  loadfiledate = "null"
Else
  loadfiledate = "'" & loadfiledate &"'"
End if    
dbconn.execute "update client.dbo.pfacclog" &_
             " set acttime = getdate(), actflag = 'U'" &_
             ", loadfiledate =" &loadfiledate &_ 
             ", insertct = " &Insertct &", totalct = " &Totalct &_
             ", blankrowct = " &blankrowct &", badrowct = " &badrowct &_
             ", noloadflag = '" &noloadflag &"', lastloadmessage = '" &lastloadmessage &_
             "' where accid = " &AccID &" and pftype = '" &pftype &"'"
End Sub

Sub AppendLog(ByVal MessageTxt)
oFS1_File.Write MessageTxt & vbCrLf
End Sub

Sub clearobjects ()
Set dbconn = nothing
Set oFSO = nothing
Set fso = nothing
Set ts = nothing
Set resultset = nothing
End Sub

Sub Archive_Old_Reports(FromFolder)
DIM ReportFilelist, ReportFile, TargetFile

Set ReportFilelist = FromFolder.Files

If oFSO.FolderExists(replace(FromFolder.Path, SourceDrive, TargetDrive)) then
  '** do nothing
Else
  '** create target folder if not exist
  oFSO.CreateFolder(replace(FromFolder.Path, SourceDrive, TargetDrive))
End If

For Each ReportFile in ReportFilelist
  If InStr(ReportFile.path,Pftype) <> 0 Then 
    'MsgBox ReportFile.path
    TargetFile = replace(ReportFile.path, SourceDrive, TargetDrive)
    'MsgBox ReportFile 
    If oFSO.FileExists(TargetFile) Then
      oFSO.DeleteFile(TargetFile)
    End If
    oFSO.CopyFile ReportFile.path, TargetFile
    ReportFile.Delete
  End if
Next
End Sub

Private Sub OpenDbCon (p_svr, p_conf)
Dim fso, f1, ts, s, uname, pword, prov, pcata, dsource
Const ForReading = 1
prov = "SQLOLEDB"
'** Get connection details
Set fso = CreateObject("Scripting.FileSystemObject")   
Set ts = fso.OpenTextFile(p_conf, ForReading)
Do 
  s = ts.ReadLine
  MyArray = Split(s, vbTab)
  If MyArray(0) = p_svr Then
    uname = MyArray(2)
    pword = MyArray(3)
    dsource = MyArray(4) 			
    Exit do
  End if		
Loop while NOT ts.AtEndOfStream
ts.Close
'** connect to database
On Error Resume Next
Set dbconn = CreateObject("ADODB.Connection")
dbconn.Open "Provider="&prov &";Data Source=" &dsource &";" &_
" Trusted_Connection=No;Initial Catalog=client;" &_
" User ID="&MyArray(2) &";Password=" &pword &";"
'dbconn.open "Provider=SQLOLEDB;Data Source=192.168.12.178;" &_
'" Trusted_Connection=No;Initial Catalog=portfolio;" &_
'" User ID=sa;Password=K376:lcnb;"
If Err.Number <> 0 Then
  MsgBox Err.Number & " " & Err.Description
End If
On Error Goto 0
'WScript.Quit
End Sub