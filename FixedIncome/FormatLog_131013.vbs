' ****************************************************************
' GENERIC FilesEx.VBS
'
'    This Script is used to check files exist and appends to the named log the file path and byte size to be used for comparison.
'    The log files are stored here: "O:\AUTO\logs\" and the final folder name is based on what you parse from the cmdline

' EXAMPLE CMDLINE
' Long Date 20110302| O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618i_not_US_CA\PreLDateSuf.618 -_2 WcaWebload_2
' Short Date 110302 | O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618i_not_US_CA\PreSDateSuf.618 -_2 WcaWebload_2
' Dashed Long Date 2011-04-05 | O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618\PreDLDateSuf.618 - WcaWebload_2
' Dashed Short Date 11-04-05 | O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618\PreDSDateSuf.618 - WcaWebload_2
' Incremental Files yymmdd_2.ext| O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618i_not_US_CA\PreDSDateSuf.618 - WcaWebload_2 i

' CMDLINE BREAKDOWN
' O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs is this file, must be called and fed the following parameters
' Param0: Source File, can be a real word and/or date or use a pseudonym ie, PreLDateSuf (20110209) or PreSDateSuf (110209) to get the date eg, O:\Datafeed\WCA\618\PreldateSuf.618
' Param1: Prefix & Suffix part of the file name. Prefix should be before the "-" and Suffix after (Pre-Suf) If no pre or suf you must include the "-"
' Param2: Task File Name without Ext. This will also become the folder created in "O:\AUTO\logs\". eg, WcaWebload_2 would yeild O:\AUTO\logs\WcaWebload_2\yyyymmdd_WcaWebload_2.txt
' Param3: If the file is an incremental and add "i" as the final argument will get you the increment number based on the time of day. 
'         12:00 to 15:00 = Inc 2 
'	  16:00 to 21:00 = Inc 3
'	  All other hours = Inc 1
' ****************************************************************

option explicit

' **** MAIN ****
	
' **** VARIABLES ****

	
	Dim fso, WshShell, numArgs, sFile, sPath, sExt, dFile, dPath, dExt, sMonth, sDay, sYear, Source, Dest, x, y, Pre, sDate,fSource,iByteSize,log,LogFile,sTime,sInc,hline,sline, Line 
	Dim lDate, sAction, sPre, dPre, PreSdate, PreLdate, SdateSuf, SdateLuf, sSuf, fs, dFileEx, sFileEx,objFSO,objReadFile,contents,objFolder,objFile,objTextFile,Act,lYear,inc, hr

' **** PREPARATION ****
	
	
	'Checks to see if the correct number of paramters have been set
	
	Set numArgs = WScript.Arguments
	
	
	'Arguments

	

	
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		lYear = mid(Year(Now),1,4)
		sYear = mid(Year(Now),3,4)
		sTime = (now)
	
	
	
	
			
		
	If numArgs.length = 2 then
	Set WshShell = WScript.CreateObject("WScript.Shell")
        Set fso = CreateObject("Scripting.FileSystemObject")	        
		sAction = numArgs(0)
		log = numArgs(1)
		contents = "<meta name='viewport' content='target-densitydpi=device-dpi'/><style type=text/css>.note{color: #00688B;font-family: verdana;font-size:13px;}.failed {color: #FF3030;line-height:3px;font-family: verdana;font-size:13px;}.ok {color: #636363;line-height:3px;font-family: verdana;font-size:13px;} p{height:0px; border:solid 0px green;}</style><p><span class=note>" & sAction & " " & Log & " </span></p>"
		
		'LogFile = "\\192.168.12.4\users\y.laifa\FixedIncome\Checksteps\LocalValidation\" & log & "\" & lYear & sMonth & sDay & "_" & log & ".html"
		'Dest = "\\192.168.12.4\users\y.laifa\FixedIncome\Checksteps\LocalValidation\" & log & "\"
		
		LogFile = "F:\Checksteps\LocalValidation\" & log & "\" & lYear & sMonth & sDay & "_" & log & ".html"
		Dest = "F:\Checksteps\LocalValidation\" & log & "\"
		
		Call sAppend()
		wscript.Quit
		
	ElseIf numArgs.length = 3 then
	Set WshShell = WScript.CreateObject("WScript.Shell")
        Set fso = CreateObject("Scripting.FileSystemObject")	 
		sAction = numArgs(0)
		'Line = "-" & numArgs(1)
		Log = numArgs(2)
		
		contents = "<meta name='viewport' content='target-densitydpi=device-dpi'/><style type=text/css>.note2{color: #33A1C9;font-family: verdana;font-size:13px;}.failed {color: #FF3030;line-height:3px;font-family: verdana;font-size:13px;}.ok {color: #636363;line-height:3px;font-family: verdana;font-size:13px;} p{height:0px; border:solid 0px green;} #feedTitle{padding:12px 0px;}</style><p id='feedTitle'><span class=note2>" & sAction & "  --------------------------------------------------------------------------------" & " </span></p>"
		
		'LogFile = "\\192.168.12.4\users\y.laifa\FixedIncome\Checksteps\LocalValidation\" & log & "\" & lYear & sMonth & sDay & "_" & log & ".html"
		'Dest = "\\192.168.12.4\users\y.laifa\FixedIncome\Checksteps\LocalValidation\" & log & "\"		
		
		LogFile = "F:\Checksteps\LocalValidation\" & log & "\" & lYear & sMonth & sDay & "_" & log & ".html"
		Dest = "F:\Checksteps\LocalValidation\" & log & "\"		
		
		Call sAppend()	
		else

		'	log = numArgs(2)
			'assign variable arguments if they exist
			
			'wscript.quit
		end if
	
	
	
	
	
	

'********************************
    	
    	
  	'WScript.Echo sFile
  	
  		

Sub sAppend()
		'hr = "<hr>"


'MsgBox "In Act - Append Function"



    	'*******************
    	
		'wscript.echo ("Source: " & sPath & sFile), ("Dest: " & dPath & dFile ), ("Action: " & action), ("Log" & log)
		'************
		'MsgBox sPath & sFile
		
		'***** PROCESSING *****
		
		
		'Read file contents
		
		'Display results
		'wscript.echo contents
			
		
		' Create the File System Object
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		
		' Check that the WriteDirectory folder exists
		If objFSO.FolderExists(Dest) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		 ' Create the WriteDirectory folder if folder does not exist
		   Set objFolder = objFSO.CreateFolder(Dest)
		   'WScript.Echo "Just created " & Dest
		End If
		
		'msgbox "LogFile Test: " & LogFile
		If objFSO.FileExists(LogFile) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		   Set objFile = objFSO.CreateTextFile(LogFile)
		   'Wscript.Echo "Just created " & LogFile
		End If
		
		set objFile = nothing
		set objFolder = nothing
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		Const ForAppending = 8		
		
		Set objTextFile = objFSO.OpenTextFile _
		(LogFile, ForAppending, True)
		
		
		' Writes WriteFile every time you run this VBScript
		objTextFile.WriteLine(contents )
		'objTextFile.WriteLine(hr)
		objTextFile.Close
		
		
	end sub	

		'*************		
			
			
	
	
' **** END MAIN **** 