Set re = New RegExp
re.Pattern = "^Pipeline_Master.*\.zip$"

Set fso = CreateObject("Scripting.FileSystemObject")
Set app = CreateObject("Shell.Application")

ExtractTo = "F:\Factset\PipeLineMaster_ToLoad"

For Each f In fso.GetFolder("F:\Factset\PipeLineMaster_ToLoad").Files
  If re.Test(f.Name) Then
    Set FilesInZip = app.NameSpace(f.Path).Items
    app.NameSpace(ExtractTo).CopyHere(FilesInZip)
  End If
Next