' ****************************************************************
' GENERIC DATE_ZIP.VBS
'
'     Script for zipping and producing custom zip file
' 
' Arguments in order (0) Client_ID	         e.g. 1
'                    (1) Source		         e.g. c:\autoexec.bat
'
' ****************************************************************

' **** SUB MAIN ****
	dim fso,f,L1,wso,colArgs,fldr,fcol,lastday,Seclast,CRLF,sSource,sOpspath,sTxtfile,sTargfile,sDatename,sArchpath

	CRLF = Chr(13) & Chr(10)

	Set colArgs = WScript.Arguments
 
	sClientID = colArgs(0)
	sSource = colArgs(1)
	sOpspath = "o:\worldequ\wsoexp\"
	
 
	'Response = MsgBox(CRLF & "------------------------------------------------------------" & CRLF & CRLF & _
                        ' "              Custom DateZip" & CRLF & CRLF & _
                        ' "------------------------------------------------------------" & CRLF, vbOkCancel)

		Set fso = CreateObject("Scripting.FileSystemObject")
		Set wso = Wscript.CreateObject("Wscript.Shell")
	
		'-- Call custom routine by using the sClientID
		'		which was passed via command Line argument(0)
		Select case sClientID
  		case "1"
				Call Factset_212
		case "2"
				Call Fnz_209		
  		case "3"
		 		Call Iods_wfi
		case "4"
		 		Call Rbc_ca
		case "5"
		 		Call Rimes_219
		case "6"
		 		Call snl_192  		
		case "7"
		 		Call Statpro_188
		case "8"
		 		Call wfi_sample
		case "9"
		 		Call SnP_229
		case "10"
		 		Call wfi_samplenew 		
		case "11"
		 		Call Markit_Agency
		case "12"
		 		Call Markit_JP
		case "13"
		 		Call Markit_US_Corp_Full
		case "14"
		 		Call Markit_US_Corp_Delta
		case "15"
		 		Call Advantage
		case "16"
		 		Call SnP_906
		case "17"
		 		Call wfi_sample_Full_2013
		case "18"
		 		Call wfi_sample_Delta_2013
		case "19"		 		
		 		Call wfi_Weekly_Full
		case "20"		 		
		 		Call Factset_SecurityWise			
		case "21"		 		
		 		Call MorningStar_225_Monthly
		case "22"
		 		Call Citicts235
		case "23"
		 		Call Markit_JP_New
		case "24"
		 		Call Rbc_ca_NewFull
		case "25"
		 		Call Sungard_237 		
		case "26"
		 		Call FIDA_238 		
		 	 	
		 		end select	
	

' **** END SUB MAIN ****

'**************************************************************
'	Remove all reference to any object created early in this
' script
'
Sub TidyUp()
 	msg = "Operation has completed"
 	msgbox msg,,"DateZip.vbs"
 	if fso.fileexists(sOpspath & sCustomName & ".TXT") then
 		fso.DeleteFile(sOpspath & sCustomName & ".TXT")
 	End If
  Set fso = Nothing
  Set wso = Nothing
End Sub


Sub Factset_212()  
  
'**************************************************************
'	Custom routine for opsid number 1
   
  sOpspath= "\\192.168.12.4\users\y.laifa\FixedIncome\Feeds\Factset\Output\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & "_212.zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub Fnz_209()  
  
'**************************************************************
'	Custom routine for opsid number 2
  
  sOpspath= "\\192.168.12.4\users\y.laifa\FixedIncome\Feeds\Fnz\Output\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & "_209.zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Iods_wfi()  
  
'**************************************************************
'	Custom routine for opsid number 3

  sOpspath= "\\192.168.12.4\users\y.laifa\FixedIncome\Feeds\Iods\Output\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & "_wfi.zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub Rbc_ca()  
  
'**************************************************************
'	Custom routine for opsid number 4
'
	sOpspath= "\\192.168.12.4\users\y.laifa\FixedIncome\Feeds\Rbc\Output\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)
  
  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & ".zip"
  
  
  Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)
 
end sub

Sub Rimes_219()  
  
'**************************************************************
'	Custom routine for opsid number 5 
 
  sOpspath= "\\192.168.12.4\users\y.laifa\FixedIncome\Feeds\Rimes\Output\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & "_219.zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub Snl_192()  
 
'**************************************************************
'	Custom routine for opsid number 6 
   
  sOpspath= "\\192.168.12.4\users\y.laifa\FixedIncome\Feeds\Snl\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & "SNL.zip" & " -suf TXT -ftidy -notidy",,true)
  sCustomName = sYear & sMonth & sDay & "SNL.zip"
  fso.CopyFile sOpspath & "SNL.ZIP", sOpspath & sCustomName,true

'Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT",,true)

end sub

Sub Statpro_188()  
 
'**************************************************************
'	Custom routine for opsid number 7 
   
  sOpspath= "\\192.168.12.4\users\y.laifa\FixedIncome\Feeds\Statpro\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & ".zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub wfi_sample()  
 
'**************************************************************
'	Custom routine for opsid number 8 
   
  sOpspath= "\\192.168.12.4\users\y.laifa\FixedIncome\Scripts\bat\WFISample\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = "WFISample_990_" & sYear & sMonth & sDay & ".zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub SnP_229()  
 
'**************************************************************
'	Custom routine for opsid number 9 
   
  sOpspath= "\\192.168.12.4\users\y.laifa\FixedIncome\Feeds\SnP\Output\229\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = "SnP_229_" & sYear & sMonth & sDay & ".zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub wfi_samplenew()  
 
'**************************************************************
'	Custom routine for opsid number 10 
   
  sOpspath= "O:\Datafeed\WFI\wfisample_eod\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = "WFISample_" & sYear & sMonth & sDay & ".zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Markit_Agency()  
 
'**************************************************************
'	Custom routine for opsid number 11 
   
  sOpspath= "O:\Datafeed\Debt\Markit_WFI\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_MarkitUSAgency.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Markit_JP()  
 
'**************************************************************
'	Custom routine for opsid number 12 
   
  sOpspath= "O:\datafeed\debt\markit_JP\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  sDay = datepart("w", now)

if sDay = 2 then
	i=3
	
Else
	i=1
	
end If

if Len(Month(Now-i))=1 then 
	sMonth="0" & month(now -i)
else
	sMonth=month(now -i)
end if

if Len(Day(Now-i))=1 then 
	sDay = "0" & day(Now -i)
else
	sDay = day(Now -i)
end if

sYear = Year(Now -i)

' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_MarkitJP.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Markit_US_Corp_Full()  
 
'**************************************************************
'	Custom routine for opsid number 13 
   
  sOpspath= "O:\Datafeed\Debt\Markit_US\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_MarkitUSCorp_Full.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub Markit_US_Corp_Delta()  
 
'**************************************************************
'	Custom routine for opsid number 14 
   
  sOpspath= "O:\Datafeed\Debt\Markit_US\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_MarkitUSCorp_Delta.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Advantage()  
 
'**************************************************************
'	Custom routine for opsid number 15 
   
  sOpspath= "H:\y.laifa\FixedIncome\Scripts\bat\Advantage\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_Advantage.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub SnP_906()  
 
'**************************************************************
'	Custom routine for opsid number 16 
   
  sOpspath= "H:\y.laifa\FixedIncome\Feeds\SnP\Output\906\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_SnP_906.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf 906 -notidy",,true)

end sub


Sub wfi_sample_Full_2013()  
 
'**************************************************************
'	Custom routine for opsid number 17 
   
  sOpspath= "H:\y.laifa\FixedIncome\Scripts\bat\WFISample2013\Full\Output\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_WFISample_Full.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub wfi_sample_Delta_2013()  
 
'**************************************************************
'	Custom routine for opsid number 18 
   
  sOpspath= "O:\Datafeed\Debt\WFIClientDaily\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_WFIDaily_Delta.zip"
      
        Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)
    
end sub


Sub wfi_Weekly_Full()  
 
'**************************************************************
'	Custom routine for opsid number 19 
   
  sOpspath= "O:\Datafeed\WFI\weeklyfull\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_WFIWeekly_Full.zip"
      
        Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)
    
end sub


Sub Factset_SecurityWise()  
 
'**************************************************************
'	Custom routine for opsid number 20 
   
  sOpspath= "O:\SecurityWise\Factset\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = "Factset_Securitywise_" & sDate & ".zip"
      
        'Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)
    
    	Y = wso.Run ("J:\java\Prog\Y.laifa\NB6\J2SE\ZipFile\dist\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)
    
end sub



Sub MorningStar_225_Monthly()  
 
'**************************************************************
'	Custom routine for opsid number 21 
   
  sOpspath= "O:\Upload\Acc\225\full\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & "-" & sMonth


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_yield_full" & ".zip"
      
        Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)
    
end sub

Sub Citicts235()  
  
'**************************************************************
   
  sOpspath= "o:\upload\acc\235\feed\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & "_235.zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


  
Sub Markit_JP_New()  
 
'**************************************************************
'	Custom routine for opsid number 23
   
  sOpspath= "O:\datafeed\debt\Markit_JP_New\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  sDay = datepart("w", now)

if sDay = 2 then
	i=3
	
Else
	i=1
	
end If

if Len(Month(Now-i))=1 then 
	sMonth="0" & month(now -i)
else
	sMonth=month(now -i)
end if

if Len(Day(Now-i))=1 then 
	sDay = "0" & day(Now -i)
else
	sDay = day(Now -i)
end if

sYear = Year(Now -i)

' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_MarkitJP.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Rbc_ca_NewFull()  
  
'**************************************************************
'	Custom routine for opsid number 24
'
	sOpspath= "O:\datafeed\debt\rbcNewFull\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)
  
  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & "_Full.zip"
  
  
  Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)
 
end sub


Sub Sungard_237()  
  
'**************************************************************
   
  sOpspath= "o:\upload\acc\237\feed\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & "_WFI_Delta_237.zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub FIDA_238()  
  
'**************************************************************
   
  sOpspath= "o:\upload\acc\238\feed\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & "_WFISample_FIDA.zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub
