Option Explicit 
On Error Resume Next 
Dim oFSO, oFolder, sDirectoryPath 
Dim oFileCollection, oFile, sDir 
Dim iDaysOld 

' Specify Directory Path From Where You want to clear the old files 

sDirectoryPath = "O:\SecurityWise\PipeLineMaster" 

' Specify Number of Days Old File to Delete

iDaysOld = 7

Set oFSO = CreateObject("Scripting.FileSystemObject") 
Set oFolder = oFSO.GetFolder(sDirectoryPath) 
Set oFileCollection = oFolder.Files 

For each oFile in oFileCollection

'This section will filter the log file as I have used for for test case 
'Specify the Extension of file that you want to delete 
'and the number with Number of character in the file extension 
	
	If LCase(Right(Cstr(oFile.Name), 3)) = "zip" Then
 	
 		If oFile.DateLastModified < (Date() - iDaysOld) Then 
 		oFile.Delete(True) 
 		End If 
 
	End If   
Next 

Set oFSO = Nothing 
Set oFolder = Nothing 
Set oFileCollection = Nothing 
Set oFile = Nothing 
