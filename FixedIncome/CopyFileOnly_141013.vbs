option explicit


'**************************************************************************
' 		 GENERIC CopyFileOnly.VBS
' 	
' Script for copying files from one directory to another
'
' Arguments in order (0) Source Path
'		     (1) Destination Path
'		     (2) sFileext
'		     (3) Sdate 	
'**************************************************************************

'***** MAIN *****

'***** VARIABLES *****

	dim fso, WshShell, sPath, dPath, sFileext, sFileext2, sdate, numArgs, sYear, sMonth, sDay

'***** PREPERATION *****


	numArgs = WScript.Arguments.Count

	sPath = WScript.Arguments.Item(0)
	dPath = WScript.Arguments.Item(1)
	sFileext = WScript.Arguments.Item(2)
	
	
	
	
	

	Set WshShell = WScript.CreateObject("WScript.Shell")
	Set fso = CreateObject("Scripting.FileSystemObject")


'***** PROCESSING *****


'msgbox sFileext
	if sFileext <> "*" then
		copyfile(sPath & "*" & sFileext)
	
	
	
		
	End if	


Private Sub copyfile(sFilename)

'wscript.echo sFilename

'wscript.echo sPath & sFileext	
	'fso.CopyFile(sFilename), dPath
	
on error resume next
fso.CopyFile(sFilename), dPath
if err.number > 0 then
	'msgbox(err.number & " there are no files to copy cnt")
	on error goto 0 
end if	
	
	
	

End Sub
	
'***** END MAIN *****