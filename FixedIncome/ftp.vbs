' ****************************************************************
' GENERIC PUSHFTP.VBS
'
'     Script for pushing files to EDI ftp boxes
' 
' Arguments in order (0) Local path
'                    (1) Remote path
'                    (2) File extension
'		     (3) SERVER
'		     (4) FTP Direction
'		     (5) Task File
'
' ****************************************************************

' **** MAIN ****
	
' **** VARIABLES ****

	dim WshShell,sLocalPath,sRemotePath,sFileext,sOpsFtp,sDirection,sXRC,sLog 

' **** PREPARATION ****
	
	Set colArgs = WScript.Arguments
 
 	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		lYear = mid(Year(Now),1,4)
		sYear = mid(Year(Now),3,4)

 
 
	sLocalPath = "-local " & replace(colArgs(0),"\","/") &" "
	sLocalPath = replace(sLocalPath,"YYYY",lYear)
	sLocalPath = replace(sLocalPath,"YY",sYear)
	sLocalPath = replace(sLocalPath,"MM",sMonth)
	sLocalPath = replace(sLocalPath,"DD",sDay)		
	'MsgBox(sLocalPath)
	sRemotePath = "-remote " & replace(colArgs(1),"\","/") &" "
	'MsgBox(sRemotePath)
	sFileext = "-fileext " & colArgs(2) & " "
	sFileext = replace(sFileext,"YYYY",lYear)
	sFileext = replace(sFileext,"YY",sYear)
	sFileext = replace(sFileext,"MM",sMonth)
	sFileext = replace(sFileext,"DD",sDay)
	'MsgBox(sFileext)
	sOpsFtp = "J:\java\Prog\Y.laifa\NB6\J2SE\opsftp\dist\opsftp.jar "
	sDirection = "-" & colArgs(4) & " "
	sXRC = "-XCRC on "
	sSite = "-site " & colArgs(3) & " "
	
	
	'MsgBox sFileext
	
	TSK = colArgs(5)
			
	'MsgBox TSK
	
	'sLog = "-log F:\Dave\FixedIncomeTestEnvironment\Checksteps\FTPValidation\" & TSK & "\" & sYear & sMonth & sDay & "_" & TSK & ".txt"
	
	'sLog = "-log o:\AUTO\logs\Ftp.log"
	'sLog = "-log \\192.168.2.163\users\y.laifa\FixedIncome\Checksteps\FTPValidation\" & TSK & "\" & lYear & sMonth & sDay & "_" & TSK & ".html"
	sLog = "-log F:\Checksteps\FTPValidation\" & TSK & "\" & lYear & sMonth & sDay & "_" & TSK & ".html"
	
	'MsgBox sLog
	Set WshShell = WScript.CreateObject("WScript.Shell")


' **** PROCESSING ****

	If colArgs(3) = "both" Then

	' FTP FILES TO ActiveHost
	sSite = "-site dotcom "
	ftp2=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog)
		If ftp2 = "1" Then
			ftp2=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)	
				If ftp2 = "1" Then
			'	msgbox ".net Failed"	
				End If
		End If


	' FTP FILES TO ICLOUD
	sSite = "-site dotnet "
	ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)
		If ftp1 = "1" Then
		ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)	
			If ftp1 = "1" Then
		'msgbox ".com Failed"	
			End If
		End If
	
	
	ElseIf colArgs(3) = "Dotnet" Then

	' FTP FILES TO DOTNET
	sSite = "-site dotnet "
	ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)
		If ftp1 = "1" Then
		ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)	
			If ftp1 = "1" Then
		'msgbox ".com Failed"	
			End If
		End If
		
		
	ElseIf colArgs(3) = "Dotcom" Then

	' FTP FILES TO DOTCOM
	sSite = "-site dotcom "
	ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)
		If ftp1 = "1" Then
		ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)	
			If ftp1 = "1" Then
		'msgbox ".com Failed"	
			End If
		End If
		
	End If		
		
' **** END MAIN **** 