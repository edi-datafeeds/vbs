' ****************************************************************
'Fixed Income Emailer
'
'     Script for emailing Notifications Without Attachments
' 
' Arguments in order (0) Client_ID	         e.g. 1
'                    (1) Source		         e.g. c:\autoexec.bat
'
' ****************************************************************

' **** SUB MAIN ****
	dim fso,f,L1,wso,colArgs,fldr,fcol,lastday,Seclast,CRLF,sSource,sOpspath,sOpspath2,sOpspath3,sTxtfile,sTargfile,sDatename,sArchpath

	CRLF = Chr(13) & Chr(10)

	Set colArgs = WScript.Arguments
 
	sClientID = colArgs(0)
	sSource = colArgs(1)
	sOpspath = "o:\worldequ\wsoexp\"
	sYear = "00"
	sDay = "00"
	sMonth = "00"
	
 
		Set fso = CreateObject("Scripting.FileSystemObject")
		Set wso = Wscript.CreateObject("Wscript.Shell")
	
		'-- Call custom routine by using the sClientID
		'		which was passed via command Line argument(0)
		
				
		Select case sClientID
			
			case "1"
					Call WFI_MorningSteps
			case "2"
					Call WFI_AfternoonSteps
			case "3"
					Call WFI_EveningSteps
			case "4"
					Call WFI_FTP_MorningSteps		
			case "5"
					Call WFI_FTP_AfternoonSteps
			case "6"
					Call WFI_FTP_EveningSteps
			case "7"
					Call WFI_FullSample
			case "8"
					Call PsResearchValidation
			case "9"
					Call SecurityReport
			case "10"
					Call WFI_WeeklyFull
			case "11"
					Call WFI_WeeklyFull_FTP
			case "12"
					Call NonfeedSecurityReport		
					
			end select
		
	

' **** END SUB MAIN ****

'**************************************************************
'	Remove all reference to any object created early in this
' script
'



Sub TidyUp()

'*********************************************************
 'Removed for Automation
 	'msg = "Operation has completed"
'msgbox msg,,"DateZip.vbs"
 	if fso.fileexists(sOpspath & sCustomName & ".TXT") then
 		fso.DeleteFile(sOpspath & sCustomName & ".TXT")
 	End If
  Set fso = Nothing
  Set wso = Nothing
End Sub



Sub WFI_MorningSteps()  
   
 '**************************************************************
 '	WFI_MorningSteps for number 1
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com;ops@exchange-data.com -ATTACH \\192.168.12.4\users\y.laifa\FixedIncome\Checksteps\LocalValidation\MorningValidationSteps\" & sDate & "_MorningValidationSteps.html -SUB WFI Morning Validation CheckSteps")
   
 
end sub

Sub WFI_AfternoonSteps()  
   
 '**************************************************************
 '	WFI_AfternoonSteps for number 2
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com;ops@exchange-data.com -ATTACH \\192.168.12.4\users\y.laifa\FixedIncome\Checksteps\LocalValidation\AfternoonValidationSteps\" & sDate & "_AfternoonValidationSteps.html -SUB WFI Afternoon Validation CheckSteps")
   'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1 -TO y.laifa@exchange-data.com -ATTACH H:\FixedIncome\Checksteps\LocalValidation\AfternoonValidationSteps\" & sDate & "_AfternoonValidationSteps.html -SUB WFI Afternoon CheckSteps")
 
end sub

Sub WFI_EveningSteps()  
   
 '**************************************************************
 '	WFI_EveningSteps for number 3
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com;ops@exchange-data.com -ATTACH \\192.168.12.4\users\y.laifa\FixedIncome\Checksteps\LocalValidation\EveningValidationSteps\" & sDate & "_EveningValidationSteps.html -SUB WFI Evening Validation CheckSteps")
 
end sub


Sub WFI_FTP_MorningSteps()  
   
 '**************************************************************
 '	WFI_FTP_MorningSteps for number 4
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com;ops@exchange-data.com -ATTACH \\192.168.12.4\users\y.laifa\FixedIncome\Checksteps\FTPValidation\MorningFtpSteps\" & sDate & "_MorningFtpSteps.html -SUB WFI Morning FTP CheckSteps")
 
end sub

Sub WFI_FTP_AfternoonSteps()  
   
 '**************************************************************
 '	WFI_FTP_AfternoonSteps for number 5
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com;ops@exchange-data.com -ATTACH \\192.168.12.4\users\y.laifa\FixedIncome\Checksteps\FTPValidation\AfternoonFtpSteps\" & sDate & "_AfternoonFtpSteps.html -SUB WFI Afternoon FTP CheckSteps")
   'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1 -TO y.laifa@exchange-data.com -ATTACH H:\FixedIncome\Checksteps\FTPValidation\AfternoonFtpSteps\" & sDate & "_AfternoonFtpSteps.html -SUB WFI Afternoon FTP CheckSteps")
   
end sub

Sub WFI_FTP_EveningSteps()  
   
 '**************************************************************
 '	WFI_FTP_EveningSteps for number 6
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com;ops@exchange-data.com -ATTACH \\192.168.12.4\users\y.laifa\FixedIncome\Checksteps\FTPValidation\EveningFtpSteps\" & sDate & "_EveningFtpSteps.html -SUB WFI Evening FTP CheckSteps")
 
end sub


Sub WFI_FullSample()  
   
 '**************************************************************
 '	WFI_FullSample for number 7
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1 -TO y.laifa@exchange-data.com -ATTACH o:\upload\acc\300\feed\" & sDate & "_300.zip -SUB WFI Sample 300")
 
end sub

Sub PsResearchValidation()  
   
 '**************************************************************
 '	PsResearchValidation for number 8
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com;j.ellis-hall@exchange-data.com;fixed.income@exchange-data.com;ops@exchange-data.com -ATTACH \\192.168.12.4\users\y.laifa\FixedIncome\Checksteps\LocalValidation\PsResearchValidation\" & sDate & "_PsResearchValidation.html -SUB PsResearch Upload Validation")
 
end sub




Sub SecurityReport()  
   
 '**************************************************************
 '	SecReport for number 9
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com;webmaster@exchange-data.com;d.johnson@exchange-data.com;a.gupta@exchange-data.com -ATTACH \\192.168.12.4\users\y.laifa\FixedIncome\Checksteps\SecReport\SecurityPortfolioReport_" & sDate & ".txt -SUB Security Portfolio Report")
 
end sub


Sub WFI_WeeklyFull()  
   
 '**************************************************************
 '	SecReport for number 10
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com -ATTACH \\192.168.12.4\users\y.laifa\FixedIncome\Checksteps\LocalValidation\WFI_Full_Weekly\" & sDate & "_WFI_Full_Weekly.html -SUB Advantage/SmartStream Weekly Full CheckSteps")
 
end sub


Sub WFI_WeeklyFull_FTP()  
   
 '**************************************************************
 '	SecReport for number 11
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com -ATTACH \\192.168.12.4\users\y.laifa\FixedIncome\Checksteps\FTPValidation\WFI_Full_Weekly\" & sDate & "_WFI_Full_Weekly.html -SUB Advantage/SmartStream Weekly Full FTP CheckSteps")
 
end sub


Sub NonfeedSecurityReport()  
   
 '**************************************************************
 '	SecReport for number 12
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com;ops@exchange-data.com -ATTACH \\192.168.12.4\users\y.laifa\FixedIncome\Checksteps\NonFeedSecReport\SecurityPortfolioReport_" & sDate & ".txt -SUB Non Feed Security Portfolio Report")
 
end sub