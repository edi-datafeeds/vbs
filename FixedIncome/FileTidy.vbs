' ****************************************************************
' GENERIC FileTidy.VBS
'
'     Script for Deleting Files in the Directory
' 
' Arguments in order (0) Path
'                    (1) Explicit File Extension
'		     
' ****************************************************************

' **** MAIN ****
	
' **** VARIABLES ****

	dim fso, WshShell, sPath, sFileext
	

' **** PREPARATION ****
	
	Set colArgs = WScript.Arguments
	if colArgs.length <2 then
		msgbox "Syntax:- FileTidy.vbs path extention"
		wscript.quit
	end if
	
	'msgbox colArgs.length
	
	sPath = colArgs(0)
	sFileext = colArgs(1)
		
	Set WshShell = WScript.CreateObject("WScript.Shell")
        Set fso = CreateObject("Scripting.FileSystemObject")	        


' **** PROCESSING ****
'msgbox sFileext
	'spath = "\\192.168.2.163\users\y.laifa\FixedIncome\Checksteps\SecurityWise"
	'sFileext = ".txt"
	'if ".txt" <> "*" then 
	
	'If sFileext <> "*" then
		fso.DeleteFile(sPath & "*" & sFileext)
	
		
	'End if	
		
' **** END MAIN **** 


