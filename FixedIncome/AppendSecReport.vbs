' *******************************************************************************************************************************
' 											Security Portfolio Report
'											  Author: Youcef Laifa
'											    Date: 20/09/2012
'	
'	This script opens up Portfolio Security Reports and appends to one main report
'	Example: AppendSecReport.vbs ftitle (Morningstar) fpath (c:\portfolio\) fname (isin) dest (c:\fullportfolioreport\)
'
' *******************************************************************************************************************************



Option explicit

Dim sMonth, sDay, lYear, sYear, sTime, CustomDate, numArgs, rTitle, fTitle, fPath, fName 
Dim objFSO, FSO, Dest, objFolder, OutputFile, OutputFile1, InputFile, objFile, contents, contents1, Line


If Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		lYear = mid(Year(Now),1,4)
		sYear = mid(Year(Now),3,4)
		sTime = (now)



CustomDate = lYear & "-" & sMonth & "-" & sDay 
'WScript.Echo CustomDate


Set numArgs = WScript.Arguments


		fTitle = numArgs(0)
		'WScript.Echo fTitle
		
		fPath = numArgs(1)
		'WScript.Echo fPath
		
		fName = numArgs(2)
		'WScript.Echo fName 
		
		Dest = numArgs(3)
		'WScript.Echo Dest
		
		'LogFile = numArgs(4)
		'WScript.Echo LogFile

		

OutputFile = Dest & "SecurityPortfolioReport_" & lYear & sMonth & sDay & ".txt"
OutputFile1= Dest & "SecurityPortfolioReport_" & lYear & sMonth & sDay & ".txt"
InputFile = fPath & fName & "_" & CustomDate & "_Report.txt"

'WScript.Echo InputFile

contents = vbCrLf & vbCrLf & fTitle & " (" & fName & ")" & vbCrLf
contents1 = vbCrLf & vbCrLf & fTitle & " (" & fName & ")" & vbCrLf & vbCrLf & "*************************************************" & vbCrLf & vbCrLf & "Report for " & CustomDate & " is not available" & vbCrLf & vbCrLf & "***************** End of Report *****************" & vbCrLf

'contents = Line

'contents = "<meta name='viewport' content='target-densitydpi=device-dpi'/><style type=text/css>.note2{color: #33A1C9;font-family: verdana;font-size:13px;}.failed {color: #FF3030;line-height:3px;font-family: verdana;font-size:13px;}.ok {color: #636363;line-height:3px;font-family: verdana;font-size:13px;} p{height:0px; border:solid 0px green;} #feedTitle{padding:12px 0px;}</style><p id='feedTitle'><span class=note2>" & fTitle & " </span></p>"


		
	Call sAppend		

	
	
	Sub sAppend()

'Read file contents
		
			
		' Create the File System Object
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		
		' Check that the WriteDirectory folder exists
		If objFSO.FolderExists(Dest) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		 ' Create the WriteDirectory folder if folder does not exist
		   Set objFolder = objFSO.CreateFolder(Dest)
		   'WScript.Echo "Just created " & Dest
		End If
		
		'msgbox "LogFile Test: " & LogFile
		If objFSO.FileExists(OutputFile) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		   Set objFile = objFSO.CreateTextFile(OutputFile)
		   
		  Set objFile = nothing
		  Set objFolder = nothing
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		Const ForWriting = 2		
		
		Set OutputFile = objFSO.OpenTextFile _
		(OutputFile, 2, True)
		
		
		' Writes WriteFile every time you run this VBScript
		OutputFile.WriteLine "*******************************************" & " Security Portfolio Report " & CustomDate & " *******************************************" & vbCrLf
		
		OutputFile.Close
		   
		'Wscript.Echo "Just created " & LogFile
	    
	    Set objFile = nothing
		set objFolder = nothing
	
		End If
		
		'set objFile = nothing
		'set objFolder = nothing
		
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		
		
		Const ForReading = 1
		Const ForAppending = 8		
		
		If objFSO.FileExists(InputFile) Then
		   
		Set InputFile = objFSO.OpenTextFile(InputFile, 1, True)   
		   
		Set OutputFile = objFSO.OpenTextFile(OutputFile1, 8, True)
		
		
		' Writes WriteFile every time you run this VBScript
		OutputFile.WriteLine(contents)
		
		Do Until InputFile.AtEndOfStream
		
		Line = InputFile.ReadLine
				
		OutputFile.WriteLine(Line)
		
		Loop
		
		InputFile.Close
		OutputFile.Close
		
	'end sub
		
		
		Else
		   				
		Set OutputFile = objFSO.OpenTextFile(OutputFile1, 8, True)
		
		
		' Writes WriteFile every time you run this VBScript
		OutputFile.WriteLine(contents1)
						
		OutputFile.Close
		
		End If
		
	end sub