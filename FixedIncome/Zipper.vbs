' ****************************************************************
' GENERIC DATE_ZIP.VBS
'
'     Script for zipping and producing custom zip file
' 
' Arguments in order (0) Client_ID	         e.g. 1
'                    (1) Source		         e.g. c:\autoexec.bat
'
' ****************************************************************

' **** SUB MAIN ****	dim fso,f,L1,wso,colArgs,fldr,fcol,lastday,Seclast,CRLF,sSource,sOpspath,sTxtfile,sTargfile,sDatename,sArchpath

	CRLF = Chr(13) & Chr(10)

	Set colArgs = WScript.Arguments
 
	sClientID = colArgs(0)
	sSource = colArgs(1)
	sOpspath = "o:\worldequ\wsoexp\"
	
 
	'Response = MsgBox(CRLF & "------------------------------------------------------------" & CRLF & CRLF & _
                        ' "              Custom DateZip" & CRLF & CRLF & _
                        ' "------------------------------------------------------------" & CRLF, vbOkCancel)

		Set fso = CreateObject("Scripting.FileSystemObject")
		Set wso = Wscript.CreateObject("Wscript.Shell")
	
		'-- Call custom routine by using the sClientID
		'		which was passed via command Line argument(0)
		Select case sClientID
  		case "1"
				Call Factset_286
		case "2"
				Call Fnz_209		
  		case "3"
		 		Call Iods_wfi
		case "4"
		 		Call Rbc_ca
		case "5"
		 		Call Rimes_219
		case "6"
		 		Call snl_192  		
		case "7"
		 		Call Statpro_188
		case "8"
		 		Call wfi_sample
		case "9"
		 		Call SnP_229
		case "10"
		 		Call wfi_samplenew 		
		case "11"
		 		Call Markit_Agency
		case "12"
		 		Call Markit_JP
		case "13"
		 		Call Markit_Incremental
		case "14"
		 		Call Markit_US_Agency_new
		case "15"
		 		Call Advantage
		case "16"
		 		Call SnP_906
		case "17"
		 		Call wfi_sample_Full_2013
		case "18"
		 		Call wfi_sample_Delta_2013
		case "19"		 		
		 		Call wfi_Weekly_Full
		case "20"		 		
		 		Call Factset_SecurityWise			
		case "21"		 		
		 		Call MorningStar_225_Monthly
		case "22"
		 		Call Citicts235
		case "23"
		 		Call Markit_JP_New
		case "24"
		 		Call Rbc_ca_NewFull
		case "25"
		 		Call Sungard_237 		
		case "26"
		 		Call FIDA_238 		
		case "27"
		 		Call Statpro_New
		case "28"
		 		Call Interdealer_241
		case "29"
		 		Call Markit_JP_Incremental
		case "30" 		
		 		Call Markit_USAgency_Incremental
		case "31" 		
		 		Call BlueRock_244
		case "32" 		
		 		Call BeastApps_YieldFull
		case "33" 		
		 		Call BondSigma_YieldFull
		case "34" 		
		 		Call PipeLineMaster_Ignore
		case "35" 		
		 		Call PipeLineMaster_Defunct
		case "36" 		
		 		Call Trumid_Incremental
		case "37" 		
		 		Call Factset_NewStyle_Delta
		case "38" 		
		 		Call BondIT_Delta
		case "39" 		
		 		Call ITG_Delta
		case "40" 		
		 		Call Brigade_258
		case "41" 		
		 		Call Kamakura_Full
		case "42" 		
		 		Call Kamakura_Delta
		case "43" 		
		 		Call CompanyWatch_Full
		case "44" 		
		 		Call Candeal_MissingReports 		
		case "45" 		
		 		Call Torstone_Delta 
		case "46" 		
		 		Call Trumid_New_Incremental
		case "47"	
				Call Trumid_New_Full
		case "48"	
				Call Markit_JP_2015
		case "49"	
				Call Markit_USAgency_2015
		case "50"	
				Call SGGG_Delta
		case "51"	
				Call Wind_CN_Delta	
		case "52"	
				Call Wind_G20_Delta
		case "53"	
				Call Concerto_Delta
		case "54"	
				Call Concerto_Full
		case "55"	
				Call BondIT_Full
		case "56"	
				Call BondIT_Delta
		case "57"	
				Call GFProducts_Full
		case "58"	
				Call GFProducts_Delta
		case "59"	
				Call Iress_Full
		case "60"	
				Call Markit_Global_Full
		case "61"	
				Call Markit_Global_Incremental
		case "62"	
				Call Cantors_Full_286
		case "63"	
				Call Cantors_Delta_286
		case "64"	
				Call Iress_Full
		case "65"	
				Call Iress_Delta
		case "66"	
				Call Sungard_237_MYSQL_Delta
		case "67"	
				Call Triad_Delta		
		case "68"	
				Call BlueRock_Full
		case "69"	
				Call BlueRock_Delta
		case "70"
				Call wfi_Sample_ISIN
		case "71"
				Call wfi_Sample_ISSID
		case "72"
				Call wfi_Sample_SECID
		case "73"
				Call wfi_Sample_SEDOL
		case "74"
				Call wfi_Sample_USCODE
		case "75"
				Call wfi_Sample_CntryCurrency
		case "76"
				Call wfi_Sample_CntryofIncorp
		case "77"
				Call wfi_Sample_Daily
		case "78"
				Call wfi_Sample_Exchanges
		case "79"
				Call wfi_Sample_CorpActions_ISIN
		case "80"
				Call wfi_Sample_SRF_Sample
		case "81"
				Call Markit_SRF_Global_Full
		case "82"
				Call Markit_SRF_Global_Delta
		case "83"
				Call ENSO_Full	
		case "84"
				Call ENSO_Delta
		case "85"
				Call RBC_US_EU_Delta
		case "86"
				Call Mergent_RiskSpan_Incremental
		case "87"
				Call Mergent_RiskSpan_Full
		case "88"
				Call Hessegim_Full
		case "89"
				Call Hessegim_Delta
		case "90"
				Call Quovo_Full
		case "91"
				Call Quovo_Delta
		case "92"
				Call ReddIntelligence_BondSigma_Hist
		case "93"
				Call ReddIntelligence_Full
		case "94"
				Call ReddIntelligence_Delta		
		case "95"
				Call NoeticPartners_Full
		case "96"
				Call DataInHarmony_Delta
		case "97"
				Call InvestSoft_Full
		case "98"
				Call Loomis_Daily_Portfolio_Coverage
		case "99"
				Call Loomis_Full_Portfolio_Coverage
		case "100"
				Call Factset_NoBlock_Delta
		case "101"
				Call Loomis_Weekly_Portfolio_Coverage
		case "102"
				Call CDS_BondSigma_Yield
				
		case "103"
				Call Weekly_Missing_ISINs
			
				
		 		end select	
	

' **** END SUB MAIN ****

'**************************************************************
'	Remove all reference to any object created early in this
' script
'
Sub TidyUp()
 	msg = "Operation has completed"
 	msgbox msg,,"DateZip.vbs"
 	if fso.fileexists(sOpspath & sCustomName & ".TXT") then
 		fso.DeleteFile(sOpspath & sCustomName & ".TXT")
 	End If
  Set fso = Nothing
  Set wso = Nothing
End Sub


Sub Factset_286()  
  
'**************************************************************
'	Custom routine for opsid number 1
   
  sOpspath= "O:\Datafeed\FixedIncome\Feeds\Factset\Output\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & "_286.zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub Fnz_209()  
  
'**************************************************************
'	Custom routine for opsid number 2
  
  sOpspath= "O:\Datafeed\FixedIncome\Feeds\Fnz\Output\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & "_209.zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Iods_wfi()  
  
'**************************************************************
'	Custom routine for opsid number 3

  sOpspath= "O:\Datafeed\FixedIncome\Feeds\Iods\Output\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & "_wfi.zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub Rbc_ca()  
  
'**************************************************************
'	Custom routine for opsid number 4
'
	sOpspath= "O:\Datafeed\FixedIncome\Feeds\Rbc\Output\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)
  
  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & ".zip"
  
  
  Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)
 
end sub

Sub Rimes_219()  
  
'**************************************************************
'	Custom routine for opsid number 5 
 
  sOpspath= "O:\Datafeed\FixedIncome\Feeds\Rimes\Output\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & "_219.zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub Snl_192()  
 
'**************************************************************
'	Custom routine for opsid number 6 
   
  sOpspath= "O:\Datafeed\FixedIncome\Feeds\Snl\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & "SNL.zip" & " -suf TXT -ftidy -notidy",,true)
  sCustomName = sYear & sMonth & sDay & "SNL.zip"
  fso.CopyFile sOpspath & "SNL.ZIP", sOpspath & sCustomName,true

'Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT",,true)

end sub

Sub Statpro_188()  
 
'**************************************************************
'	Custom routine for opsid number 7 
   
  sOpspath= "O:\Datafeed\FixedIncome\Feeds\Statpro\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & ".zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub wfi_sample()  
 
'**************************************************************
'	Custom routine for opsid number 8 
   
  sOpspath= "O:\Datafeed\FixedIncome\Scripts\bat\WFISample\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = "WFISample_990_" & sYear & sMonth & sDay & ".zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub SnP_229()  
 
'**************************************************************
'	Custom routine for opsid number 9 
   
  sOpspath= "O:\Datafeed\FixedIncome\Feeds\SnP\Output\229\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = "SnP_229_" & sYear & sMonth & sDay & ".zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub wfi_samplenew()  
 
'**************************************************************
'	Custom routine for opsid number 10 
   
  sOpspath= "O:\Datafeed\WFI\wfisample_eod\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = "WFISample_" & sYear & sMonth & sDay & ".zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Markit_Agency()  
 
'**************************************************************
'	Custom routine for opsid number 11 
   
  sOpspath= "O:\Datafeed\Debt\Markit_WFI\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_MarkitUSAgency.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Markit_JP()  
 
'**************************************************************
'	Custom routine for opsid number 12 
   
  sOpspath= "O:\datafeed\debt\markit_JP\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  sDay = datepart("w", now)

if sDay = 2 then
	i=3
	
Else
	i=1
	
end If

if Len(Month(Now-i))=1 then 
	sMonth="0" & month(now -i)
else
	sMonth=month(now -i)
end if

if Len(Day(Now-i))=1 then 
	sDay = "0" & day(Now -i)
else
	sDay = day(Now -i)
end if

sYear = Year(Now -i)

' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_MarkitJP.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Markit_Incremental()  
 
'**************************************************************
'	Custom routine for opsid number 13
 
  sOpspath= "O:\Datafeed\Debt\Markit_Incremental\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)


  sTime = Time
  sTime = int(left(sTime,2))


   if sTime >= 07 And sTime <= 12  then
  
   'MsgBox (sTime)
   sCustomName = sYear & sMonth & sDay & "_Markit_Global_Delta_1.zip"
  
   ElseIf sTime >= 13 And sTime <= 16  then
  
   'MsgBox (sTime)
   sCustomName = sYear & sMonth & sDay & "_Markit_Global_Delta_2.zip"
  
   Elseif sTime >= 17 And sTime <= 23  Then
  
   'MsgBox (sTime)
   sCustomName = sYear & sMonth & sDay & "_Markit_Global_Delta_3.zip"
  
   End If
  
'  Else If Hour(Now()) >= 14:30 OR hour(Now()) < 18:00 Then
  
'   sCustomName = sYear & sMonth & sDay & "_Markit_Global_Delta_2.zip"
  
'  Else If Hour(Now()) >= 18:30 OR hour(Now()) < 06:30 Then
  
'  sCustomName = sYear & sMonth & sDay & "_Markit_Global_Delta_3.zip"
  
'  End If



  '-- Create the Custom filename without a extension  
  'sCustomName = sYear & sMonth & sDay & "_Markit_NewStyle_Full.zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub Markit_US_Agency_new()  
 
'**************************************************************
'	Custom routine for opsid number 14 
   
  sOpspath= "O:\datafeed\debt\Markit_USAgency_New\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_MarkitUSAgency.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Advantage()  
 
'**************************************************************
'	Custom routine for opsid number 15 
   
  sOpspath= "H:\y.laifa\FixedIncome\Scripts\bat\Advantage\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_Advantage.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub SnP_906()  
 
'**************************************************************
'	Custom routine for opsid number 16 
   
  sOpspath= "H:\y.laifa\FixedIncome\Feeds\SnP\Output\906\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_SnP_906.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf 906 -notidy",,true)

end sub


Sub wfi_sample_Full_2013()  
 
'**************************************************************
'	Custom routine for opsid number 17 
   
  sOpspath= "H:\y.laifa\FixedIncome\Scripts\bat\WFISample2013\Full\Output\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_WFISample_Full.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub wfi_sample_Delta_2013()  
 
'**************************************************************
'	Custom routine for opsid number 18 
   
  sOpspath= "O:\Datafeed\Debt\WFIClientDaily\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_WFIDaily_Delta.zip"
      
        Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)
    
end sub


Sub wfi_Weekly_Full()  
 
'**************************************************************
'	Custom routine for opsid number 19 
   
  sOpspath= "O:\Datafeed\WFI\weeklyfull\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_WFIWeekly_Full.zip"
      
        Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)
    
end sub


Sub Factset_SecurityWise()  
 
'**************************************************************
'	Custom routine for opsid number 20 
   
  sOpspath= "O:\SecurityWise\Factset\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = "Factset_Securitywise_" & sDate & ".zip"
      
        'Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)
    
    	Y = wso.Run ("J:\java\Prog\Y.laifa\NB6\J2SE\ZipFile\dist\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)
    
end sub



Sub MorningStar_225_Monthly()  
 
'**************************************************************
'	Custom routine for opsid number 21 
   
  sOpspath= "O:\Upload\Acc\225\full\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & "-" & sMonth


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_yield_full" & ".zip"
      
        Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)
    
end sub

Sub Citicts235()  
  
'**************************************************************
   
  sOpspath= "o:\upload\acc\235\feed\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & "_235.zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


  
Sub Markit_JP_New()  
 
'**************************************************************
'	Custom routine for opsid number 23
   
  sOpspath= "O:\datafeed\debt\Markit_JP_New\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  sDay = datepart("w", now)

if sDay = 2 then
	i=3
	
Else
	i=1
	
end If

if Len(Month(Now-i))=1 then 
	sMonth="0" & month(now -i)
else
	sMonth=month(now -i)
end if

if Len(Day(Now-i))=1 then 
	sDay = "0" & day(Now -i)
else
	sDay = day(Now -i)
end if

sYear = Year(Now -i)

' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_MarkitJP.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Rbc_ca_NewFull()  
  
'**************************************************************
'	Custom routine for opsid number 24
'
	sOpspath= "O:\datafeed\debt\rbcNewFull\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)
  
  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & "_Full.zip"
  
  
  Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)
 
end sub


Sub Sungard_237()  
  
'**************************************************************
 '	Custom routine for opsid number 25
 
  sOpspath= "o:\upload\acc\237\feed\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & "_WFI_Delta_237.zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub FIDA_238()  
  
'**************************************************************
   '	Custom routine for opsid number 26
   
  sOpspath= "o:\upload\acc\238\feed\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & "_FIDA_238_Delta.zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Statpro_New()  
  
'**************************************************************
   '	Custom routine for opsid number 27
   
  sOpspath= "O:\Datafeed\Debt\StatproNew\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & ".zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub Interdealer_241()

'**************************************************************
'	Custom routine for opsid number 28
 
  sOpspath= "O:\Upload\Acc\241\feed\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & "_InterDealer_Delta.zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub Markit_JP_Incremental()

'**************************************************************
'	Custom routine for opsid number 29
 
  sOpspath= "O:\Datafeed\Debt\Markit_JP_Incremental\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)


  sTime = Time
  sTime = int(left(sTime,2))


   if sTime >= 07 And sTime <= 12  then
  
   'MsgBox (sTime)
   sCustomName = sYear & sMonth & sDay & "_Markit_JP_Delta_1.zip"
  
   ElseIf sTime >= 13 And sTime <= 16  then
  
   'MsgBox (sTime)
   sCustomName = sYear & sMonth & sDay & "_Markit_JP_Delta_2.zip"
  
   Elseif sTime >= 17 And sTime <= 23  Then
  
   'MsgBox (sTime)
   sCustomName = sYear & sMonth & sDay & "_Markit_JP_Delta_3.zip"
  
   End If
  

  '-- Create the Custom filename without a extension  
  'sCustomName = sYear & sMonth & sDay & "_Markit_NewStyle_Full.zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Markit_USAgency_Incremental()

'**************************************************************
'	Custom routine for opsid number 30
 
  sOpspath= "O:\Datafeed\Debt\Markit_US_Incremental\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)


  sTime = Time
  sTime = int(left(sTime,2))


   if sTime >= 07 And sTime <= 12  then
  
   'MsgBox (sTime)
   sCustomName = sYear & sMonth & sDay & "_Markit_USAgency_Delta_1.zip"
  
   ElseIf sTime >= 13 And sTime <= 16  then
  
   'MsgBox (sTime)
   sCustomName = sYear & sMonth & sDay & "_Markit_USAgency_Delta_2.zip"
  
   Elseif sTime >= 17 And sTime <= 23  Then
  
   'MsgBox (sTime)
   sCustomName = sYear & sMonth & sDay & "_Markit_USAgency_Delta_3.zip"
  
   End If
  



  '-- Create the Custom filename without a extension  
  'sCustomName = sYear & sMonth & sDay & "_Markit_NewStyle_Full.zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub BlueRock_244()

'**************************************************************
'	Custom routine for opsid number 31
 
  sOpspath= "O:\Datafeed\Debt\BlueRock\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & "_WFI_Delta_244.zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub BeastApps_YieldFull()

'**************************************************************
'	Custom routine for opsid number 32
 
  sOpspath= "O:\Datafeed\Bespoke\BeastApps\Calcin\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  
  sCustomName = "EDI_BeastApps_" & sYear & "-" & sMonth & "-" & sDay & ".zip"
  
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf CSV -notidy",,true)

end sub


Sub BondSigma_YieldFull()

'**************************************************************
'	Custom routine for opsid number 33
 
  sOpspath= "O:\Datafeed\Bespoke\BondSigma\Calcin\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  
  sCustomName = "EDI_BondSigma_" & sYear & "-" & sMonth & "-" & sDay & ".zip"
  
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf CSV -notidy",,true)

end sub


Sub PipeLineMaster_Ignore()

'**************************************************************
'	Custom routine for opsid number 34
 
  sOpspath= "O:\SecurityWise\PipeLineMaster\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = mid(Year(Now),3,4)

  '-- Create the Custom filename without a extension  
  
  sCustomName = "Ignored_" & sYear & sMonth & sDay & ".zip"
  
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -tidy",,true)

end sub


Sub PipeLineMaster_Defunct()

'**************************************************************
'	Custom routine for opsid number 35
 
  sOpspath= "O:\SecurityWise\PipeLineMaster\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = mid(Year(Now),3,4)

  '-- Create the Custom filename without a extension  
  
  sCustomName = "Defunct_" & sYear & sMonth & sDay & ".zip"
  
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -tidy",,true)

end sub


Sub Trumid_Incremental()

'**************************************************************
'	Custom routine for opsid number 36
 
  sOpspath= "O:\Datafeed\Debt\Trumid_Incremental\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)


  sTime = Time
  sTime = int(left(sTime,2))


   if sTime >= 07 And sTime <= 12  then
  
   'MsgBox (sTime)
   sCustomName = sYear & sMonth & sDay & "_Trumid_Delta_1.zip"
  
   ElseIf sTime >= 13 And sTime <= 16  then
  
   'MsgBox (sTime)
   sCustomName = sYear & sMonth & sDay & "_Trumid_Delta_2.zip"
  
   Elseif sTime >= 17 And sTime <= 23  Then
  
   'MsgBox (sTime)
   sCustomName = sYear & sMonth & sDay & "_Trumid_Delta_3.zip"
  
   End If
  

      
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Factset_NewStyle_Delta()

'**************************************************************
'	Custom routine for opsid number 37
   
  sOpspath= "O:\Datafeed\Debt\FactsetNew\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    'sCustomName = sYear & sMonth & sDay & "_Factset.zip"
    sCustomName = sYear & sMonth & sDay & "_Factset_Delta.zip"  
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub BondIT_Delta()

'**************************************************************
'	Custom routine for opsid number 38
   
  sOpspath= "O:\Datafeed\Debt\BondIT\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_BondIT_257_Delta.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub ITG_Delta()

'**************************************************************
'	Custom routine for opsid number 39
   
  sOpspath= "O:\Datafeed\Debt\ITG\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_ITG_Delta.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Brigade_258()

'**************************************************************
'	Custom routine for opsid number 40
   
  sOpspath= "O:\Datafeed\Debt\Brigade\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_Brigade_258.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Kamakura_Full()

'**************************************************************
'	Custom routine for opsid number 41
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Kamakura\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_Kamakura_Full.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Kamakura_Delta()

'**************************************************************
'	Custom routine for opsid number 42
   
  sOpspath= "O:\Datafeed\Debt\Kamakura\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_Kamakura_Delta.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub CompanyWatch_Full()

'**************************************************************
'	Custom routine for opsid number 43
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\CompanyWatch\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_BKRP_Full.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Candeal_MissingReports()

'**************************************************************
'	Custom routine for opsid number 44
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Candeal\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_CandealMissingReports.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Torstone_Delta()

'**************************************************************
'	Custom routine for opsid number 45
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Torstone\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_Torstone_Delta.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub



Sub Trumid_New_Incremental()

'**************************************************************
'	Custom routine for opsid number 46
 
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Trumid\Output\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)


  sTime = Time
  sTime = int(left(sTime,2))


   if sTime >= 07 And sTime <= 12  then
  
   'MsgBox (sTime)
   sCustomName = sYear & sMonth & sDay & "_Trumid_Delta_1.zip"
  
   ElseIf sTime >= 13 And sTime <= 16  then
  
   'MsgBox (sTime)
   sCustomName = sYear & sMonth & sDay & "_Trumid_Delta_2.zip"
  
   Elseif sTime >= 17 And sTime <= 23  Then
  
   'MsgBox (sTime)
   sCustomName = sYear & sMonth & sDay & "_Trumid_Delta_3.zip"
  
   End If
  

      
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Trumid_New_Full()

'**************************************************************
'	Custom routine for opsid number 47
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Trumid\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_Trumid_Full.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Markit_JP_2015()

'**************************************************************
'	Custom routine for opsid number 48
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Markit\Output\JP\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_Markit_JP_Sample.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Markit_USAgency_2015()

'**************************************************************
'	Custom routine for opsid number 49
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Markit\Output\USAgency\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_Markit_USAgency_Sample.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub SGGG_Delta()

'**************************************************************
'	Custom routine for opsid number 50
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\SGGG\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_SGGG_Delta.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Wind_CN_Delta()

'**************************************************************
'	Custom routine for opsid number 51
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Wind\Output\CntryCurrenCD\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_Wind_CN_Delta.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Wind_G20_Delta()

'**************************************************************
'	Custom routine for opsid number 52
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Wind\Output\G20\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_Wind_G20_Delta.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Concerto_Delta()

'**************************************************************
'	Custom routine for opsid number 53
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Concerto\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_Concerto_Delta.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Concerto_Full()

'**************************************************************
'	Custom routine for opsid number 54
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Concerto\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_Concerto_Full.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub BondIT_Full()

'**************************************************************
'	Custom routine for opsid number 55
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\BondIT\Output\Full\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_BondIT_Full.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub BondIT_Delta()

'**************************************************************
'	Custom routine for opsid number 56
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\BondIT\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_BondIT_Delta.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub GFProducts_Full()

'**************************************************************
'	Custom routine for opsid number 57
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\GFProducts\Output\Full\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_GFProducts_Full.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub GFProducts_Delta()

'**************************************************************
'	Custom routine for opsid number 58
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\GFProducts\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_GFProducts_Delta.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub Iress_Full()

'**************************************************************
'	Custom routine for opsid number 59
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Iress\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_Iress_Full.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub Markit_Global_Full()

'**************************************************************
'	Custom routine for opsid number 60
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Markit\Output\Global\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_Markit_Global_Full.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Markit_Global_Incremental()

'**************************************************************
'	Custom routine for opsid number 61
 
  sOpspath= "O:\Datafeed\Debt\Markit_GlobalInc\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)


  sTime = Time
  sTime = int(left(sTime,2))


   if sTime >= 07 And sTime <= 12  then
  
   'MsgBox (sTime)
   sCustomName = sYear & sMonth & sDay & "_Markit_Global_Delta_1.zip "
  
   ElseIf sTime >= 13 And sTime <= 16  then
  
   'MsgBox (sTime)
   sCustomName = sYear & sMonth & sDay & "_Markit_Global_Delta_2.zip "
  
   Elseif sTime >= 17 And sTime <= 23  Then
  
   'MsgBox (sTime)
   sCustomName = sYear & sMonth & sDay & "_Markit_Global_Delta_3.zip "
  
   End If
  

      
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Cantors_Full_286()

'**************************************************************
'	Custom routine for opsid number 62
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Cantors\Output\Full\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_Cantors_Full.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Cantors_Delta_286()

'**************************************************************
'	Custom routine for opsid number 63
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Cantors\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_Cantors_Delta.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub Iress_Full()

'**************************************************************
'	Custom routine for opsid number 64
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Iress\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_Iress_Full.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub Iress_Delta()

'**************************************************************
'	Custom routine for opsid number 65
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Iress\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_Iress_Delta.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub Sungard_237_MYSQL_Delta()

'**************************************************************
'	Custom routine for opsid number 66
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Sungard\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_WFI_Delta_237.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Triad_Delta()

'**************************************************************
'	Custom routine for opsid number 67
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Triad\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_Triad_Delta.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub BlueRock_Full()

'**************************************************************
'	Custom routine for opsid number 68
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\BlueRock\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_BlueRock_Full.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub BlueRock_Delta()

'**************************************************************
'	Custom routine for opsid number 69
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\BlueRock\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_BlueRock_Delta.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub wfi_Sample_ISIN()  
 
'**************************************************************
'	Custom routine for opsid number 70 
   
  sOpspath= "O:\Prodman\SalesFeeds\WFI\FullSample\Portfolio\ISIN\Output\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_WFISample_ISIN.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub wfi_Sample_ISSID()  
 
'**************************************************************
'	Custom routine for opsid number 71 
   
  sOpspath= "O:\Prodman\SalesFeeds\WFI\FullSample\Portfolio\ISSID\Output\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_WFISample_ISSID.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub wfi_Sample_SECID()  
 
'**************************************************************
'	Custom routine for opsid number 72 
   
  sOpspath= "O:\Prodman\SalesFeeds\WFI\FullSample\Portfolio\SECID\Output\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_WFISample_SECID.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub wfi_Sample_SEDOL()  
 
'**************************************************************
'	Custom routine for opsid number 73 
   
  sOpspath= "O:\Prodman\SalesFeeds\WFI\FullSample\Portfolio\SEDOL\Output\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_WFISample_SEDOL.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub wfi_Sample_USCODE()  
 
'**************************************************************
'	Custom routine for opsid number 74 
   
  sOpspath= "O:\Prodman\SalesFeeds\WFI\FullSample\Portfolio\USCODE\Output\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_WFISample_USCODE.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub wfi_Sample_CntryCurrency()  
 
'**************************************************************
'	Custom routine for opsid number 75 
   
  sOpspath= "O:\Prodman\SalesFeeds\WFI\FullSample\CntryCurrency\Output\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_WFISample_CntryCurrency.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub wfi_Sample_CntryofIncorp()  
 
'**************************************************************
'	Custom routine for opsid number 76 
   
  sOpspath= "O:\Prodman\SalesFeeds\WFI\FullSample\CntryofIncorp\Output\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_WFISample_CntryofIncorp.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub wfi_Sample_Daily()  
 
'**************************************************************
'	Custom routine for opsid number 77 
   
  sOpspath= "O:\Prodman\SalesFeeds\WFI\FullSample\Daily\Output\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_WFISample_Daily.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub wfi_Sample_Exchanges()  
 
'**************************************************************
'	Custom routine for opsid number 78 
   
  sOpspath= "O:\Prodman\SalesFeeds\WFI\FullSample\Exchanges\Output\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_WFISample_Exchanges.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub wfi_Sample_CorpActions_ISIN()  
 
'**************************************************************
'	Custom routine for opsid number 79 
   
  sOpspath= "O:\Prodman\SalesFeeds\WFI\CorporateActions\Portfolio\ISIN\Output\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_WFICorpActions_ISIN.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub wfi_Sample_SRF_Sample()  
 
'**************************************************************
'	Custom routine for opsid number 80 
   
  sOpspath= "O:\Prodman\SalesFeeds\WFI\SRF\BBSEDOL\Output\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_WFI_SRF_Sample.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Markit_SRF_Global_Full()  
 
'**************************************************************
'	Custom routine for opsid number 81 
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Markit\Output\SRF\Full\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_Markit_SRF_Global_Full.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Markit_SRF_Global_Delta()  
 
'**************************************************************
'	Custom routine for opsid number 82
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Markit\Output\SRF\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_Markit_SRF_Global_Delta.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub ENSO_Full()  
 
'**************************************************************
'	Custom routine for opsid number 83
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\ENSO\Output\Full\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_ENSO_Full.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub ENSO_Delta()  
 
'**************************************************************
'	Custom routine for opsid number 84
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\ENSO\Output\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_ENSO_Delta.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub RBC_US_EU_Delta()  
 
'**************************************************************
'	Custom routine for opsid number 85
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\RBC\Output\US_EU\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_RBC_US_EU_Delta.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Mergent_RiskSpan_Incremental()

'**************************************************************
'	Custom routine for opsid number 86
 
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Mergent_RiskSpan\Output\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)


  sTime = Time
  sTime = int(left(sTime,2))


   if sTime >= 07 And sTime <= 12  then
  
   'MsgBox (sTime)
   sCustomName = sYear & sMonth & sDay & "_Mergent_RiskSpan_Delta_1.zip"
  
   ElseIf sTime >= 13 And sTime <= 16  then
  
   'MsgBox (sTime)
   sCustomName = sYear & sMonth & sDay & "_Mergent_RiskSpan_Delta_2.zip"
  
   Elseif sTime >= 17 And sTime <= 23  Then
  
   'MsgBox (sTime)
   sCustomName = sYear & sMonth & sDay & "_Mergent_RiskSpan_Delta_3.zip"
  
   End If
  

      
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Mergent_RiskSpan_Full()  
 
'**************************************************************
'	Custom routine for opsid number 87
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Mergent_RiskSpan\Output\Full\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_Mergent_RiskSpan_Full.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub Hessegim_Full()  
 
'**************************************************************
'	Custom routine for opsid number 88
   
  sOpspath= "O:\Upload\Acc\291\Feed\Full\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_Hessegim_Full.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub Hessegim_Delta()  
 
'**************************************************************
'	Custom routine for opsid number 89
   
  sOpspath= "O:\Upload\Acc\291\Feed\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_Hessegim_Delta.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub Quovo_Full()  
 
'**************************************************************
'	Custom routine for opsid number 90
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Quovo\Output\Full\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_Quovo_Full.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Quovo_Delta()  
 
'**************************************************************
'	Custom routine for opsid number 91
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Quovo\Output\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_Quovo_Delta.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub ReddIntelligence_BondSigma_Hist()  
 
'**************************************************************
'	Custom routine for opsid number 92
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\RedIntelligence\Output\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_RedIntelligence_BondSigma_Hist.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub ReddIntelligence_Full()  
 
'**************************************************************
'	Custom routine for opsid number 93
   
  sOpspath= "O:\Upload\Acc\292\Feed\Full\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_RedIntelligence_Full.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub ReddIntelligence_Delta()  
 
'**************************************************************
'	Custom routine for opsid number 94
   
  sOpspath= "O:\Upload\Acc\292\Feed\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_RedIntelligence_Delta.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub NoeticPartners_Full()  
 
'**************************************************************
'	Custom routine for opsid number 95
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\NoeticPartners\Output\Full\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_NoeticPartners_Full.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub DataInHarmony_Delta()  
 
'**************************************************************
'	Custom routine for opsid number 96
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\DataInHarmony\Output\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_DataInHarmony_Delta.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub InvestSoft_Full()  
 
'**************************************************************
'	Custom routine for opsid number 97
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\InvestSoft\Output\Full\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_InvestSoft_Full.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Loomis_Daily_Portfolio_Coverage()  
 
'**************************************************************
'	Custom routine for opsid number 98
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Internal\Daily\Output\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_Loomis_Daily_Portfolio_Coverage.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Loomis_Full_Portfolio_Coverage()  
 
'**************************************************************
'	Custom routine for opsid number 99
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Internal\Daily\Output\"			
 		
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)


' Build filename
sDate = sYear & sMonth & sDay


'WScript.Echo (sDate)


  '-- Create the Custom filename without a extension  
    sCustomName = sDate & "_Loomis_Full_Portfolio_Coverage.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Factset_NoBlock_Delta()

'**************************************************************
'	Custom routine for opsid number 100
   
  sOpspath= "O:\Datafeed\Debt\Factset_NoBlock\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_Factset.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Loomis_Weekly_Portfolio_Coverage()

'**************************************************************
'	Custom routine for opsid number 101
   
  sOpspath= "O:\Prodman\Dev\WFI\Feeds\Internal\Weekly\Output\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_Loomis_Weekly_Portfolio_Coverage.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub CDS_BondSigma_Yield()

'**************************************************************
'	Custom routine for opsid number 102
   
  sOpspath= "O:\Datafeed\Debt\CDS\FIPS\Feed\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
 dt = DateAdd("d", -1, Date)
 
yesterday = Right(Year(dt),4) &"-"& Right("0" & Month(dt),2) &"-"& Right("0" & Day(dt),2)

  '-- Create the Custom filename without a extension  
    sCustomName = yesterday & "_CDS_TMX_BondSigma.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub Weekly_Missing_ISINs()

'**************************************************************
'	Custom routine for opsid number 103
   
  sOpspath= "F:\Coverage\Weekly\EDI\"			
  'sOpspath= "O:\Datafeed\Debt\snl\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
    sCustomName = sYear & sMonth & sDay & "_Weekly_Missing_ISINs_Issuers_Created.zip"
      
    Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf xls -tidy",,true)

end sub

