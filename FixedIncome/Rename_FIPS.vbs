option explicit


'**************************************************************************
' 		 GENERIC COPYRENAMEFILE.VBS
' 	
' Script for copying files from one directory to another
'
' Arguments in order (0) Source Path
'		     (1) Destination Path
'		     (2) sfName
'		     (3) dfName
'**************************************************************************

'***** MAIN *****

'***** VARIABLES *****

	dim objFSO, WshShell, sPath, dPath, sfName, dfName, numArgs, sExt, objFolder, colFiles, objFile
'***** PREPERATION *****


	numArgs = WScript.Arguments.Count

	
	
	sPath = WScript.Arguments.Item(0)
	sExt = WScript.Arguments.Item(1)
	dPath = WScript.Arguments.Item(2)
	dfName = WScript.Arguments.Item(3)
	
	
	

	Set WshShell = WScript.CreateObject("WScript.Shell")
	Set objFSO = CreateObject("Scripting.FileSystemObject")




'***** PROCESSING *****


		
		
		
		Set objFolder = objFSO.GetFolder(sPath)
		
		Set colFiles = objFolder.Files
		For Each objFile in colFiles
		   if instr(objFile.Name,sExt) then
		       		       
		       'MsgBox "sPath & objFile.Name,  dPath & dfName"
		       objFSO.CopyFile sPath & objFile.Name,  dPath & dfName
		       objFSO.DeleteFile(sPath & objFile.Name)
		   end if
Next
	
'***** END MAIN *****