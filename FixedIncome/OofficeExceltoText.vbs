Dim Excel, objCalc, sMonth, sDay, sYear, numArgs, strExcelPath, strExcelFile, objFSO, objSheet, loopCount, i, strLogPath
Dim strLogFile, objFolder, objFile, sPath, isheet, ssheet, icell, scell, value, strLine, LogFile, args() 


'-- Create Date Function of current date  
 if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)




		Set numArgs = WScript.Arguments

		sPath = numArgs(0)

		isheet = numArgs(1)
		isheet=(Mid(isheet,10))
		ssheet= CInt (isheet)
		'WScript.Echo ssheet
		
		
		
		
		
		icell = numArgs(2)
		icell=(Mid(icell,5))
		scell= CInt (icell)
		'WScript.Echo scell



'strExcelPath = "\\192.168.2.163\fixincarch\" & sPath & "\" & sYear & "\" & sMonth & "_" & sYear & "\"
strExcelPath = "F:\" & sPath & "\" & sYear & "\" & sMonth & "_" & sYear & "\"
strExcelFile = "PSResearchUploaded" & sYear & "_" & sMonth & "_" & sDay & ".xls"
'WScript.Echo strExcelPath & strExcelFile

'strExcelPath= "c:\youcef\"



'strLogPath = "\\192.168.2.163\fixincarch\" & sPath & "\ExceltoText\"
strLogPath = "F:\" & sPath & "\ExceltoText\"
strLogFile = sYear & sMonth & sDay & ".txt"


'Checks to see if Open Office Calc installed on Machine            
 On error resume Next
 
 		'-- Checks to see if file exists otherwise quit program 
		Set objFSO = CreateObject("Scripting.FileSystemObject")
  
		set objServiceManager = CreateObject("com.sun.star.ServiceManager")
		'Set StarDesktop = objCalc.createInstance("com.sun.star.frame.Desktop")
 
        If (Err.Number <> 0) Then
    	On Error GoTo 0
            
            MsgBox("Open Office is not found, This script will now exit")
            'MsgBox("This script will now exit")
            
            set objServiceManager = Nothing
            Wscript.Quit
            
            
        Else 'WScript.echo("Open Office is Installed")
	           
	            
			'Create the CoreReflection service that is later used to create structs
			Set objCoreReflection = objServiceManager.createInstance("com.sun.star.reflection.CoreReflection")
			
			'Create the Desktop
			Set objDesktop = objServiceManager.createInstance("com.sun.star.frame.Desktop")
						
			   			   
			'WScript.Echo strExcelPath & strExcelFile
			
			If objFSO.FileExists(strExcelPath & strExcelFile) Then
			
				
			
								   
			'-- Open specified spreadsheet
			'objExcel.WorkBooks.Open strExcelPath & strExcelFile
			
			Set objWorkbook = objDesktop.loadComponentFromURL("file:///" & strExcelPath & strExcelFile, "_blank", 0, args)
			
			objWorkbook.CurrentController.Frame.ContainerWindow.setVisible(false) 
						
			
			'objWorkbook.getCurrentController.getFrame.getContainerWindow.setVisible(False)
			
			
		Else
		   
		   msgbox "The xls file you are trying to open does not exist, please click 'ok' to quit program"
		   
		   set objServiceManager = Nothing
		   Set objCoreReflection = Nothing
		   Set objDesktop = Nothing
		   Set objWorkbook = Nothing
		   
		   Wscript.Quit    
            
                   
        End If         
        	On Error GoTo 0
                     		
        		Call CalcToText()
	            Call Dedup()
				Call RemoveBlanks()
        		
				set objCalc = Nothing
		        Wscript.Quit
	
		End If





'Checks to see if Microsoft Excel installed on Machine
On error resume next
 
 set objExcel = CreateObject("Excel.Application")
 'Set objExcel = CreateObject("Excel.Application") 
 
        If (Err.Number <> 0) Then
    	On Error GoTo 0
            MsgBox("Micorsoft Office is not found")
            set objExcel = Nothing
            
            Else MsgBox("Micorsoft Office is Installed")
	           
	            
	    '-- Checks to see if file exists otherwise quit program 
		Set objFSO = CreateObject("Scripting.FileSystemObject")


			If objFSO.FileExists(strExcelPath & strExcelFile) Then
		   
		   '-- Open specified spreadsheet
			objExcel.WorkBooks.Open strExcelPath & strExcelFile
			
		Else
		   
		   msgbox "The xls file you are trying to open does not exist, please click 'ok' to quit program"
		   
		   Wscript.Quit
		   
		  
		End If
	       On Error GoTo 0     
	            
	                        
	            Call ExcelToText()
	            Call Dedup()
				Call RemoveBlanks()
	            
	            set Excel = Nothing
 				Wscript.Quit
				
            
        End If  
        'On Error GoTo 0 	
		
		
						
		
		Sub ExcelToText()
			
		Const ForAppending = 8 
    
    
    '-- Selects specified worksheet
    Set objSheet = objExcel.ActiveWorkbook.Worksheets(ssheet)
    
    'We want to skip the header row, and then the blank row below
	loopCount = 2
	
	
	If IsEmpty (objSheet.Cells(loopCount, scell)) Then 
   
   	'WScript.Echo "Column " & (scell) & " and Row " & (loopCount) & " from Worksheet " & (ssheet) &" is empty"
   	
   								   
    objExcel.ActiveWorkbook.Close
	objExcel.Application.Quit
	set objCalc = Nothing
    WScript.Quit
   
   else   
    
    Do while not isempty(objSheet.Cells(loopCount, scell).Value)
  

  'Selects Column
  For i = scell To scell
    value = objSheet.Cells(loopCount, i).Value
            
       
        ' Create the File System Object
'Set objFSO = CreateObject("Scripting.FileSystemObject")
  
  ' Check that the strDirectory folder exists
If objFSO.FolderExists(strLogPath) Then
   Set objFolder = objFSO.GetFolder(strLogPath)
Else
   Set objFolder = objFSO.CreateFolder(strLogPath)
   'WScript.Echo "Just created " & strExcelPath
End If
  
  
  
If objFSO.FileExists(strLogPath & strLogFile) Then
   Set objFolder = objFSO.GetFolder(strLogPath)
Else
   Set objFile = objFSO.CreateTextFile(strLogPath & strLogFile)
   'Wscript.Echo "Just created " & strExcelPath & strLogFile
End If   
  
  set objFile = nothing
  set objFolder = nothing

Set objFile = objFSO.OpenTextFile(strLogPath & strLogFile, ForAppending, True)

' Writes strText every time you run this VBScript
objFile.WriteLine(value)
'WScript.Echo value
objFile.Close
  
   
  Next
  loopCount = loopCount + 1

Loop
    
    objExcel.ActiveWorkbook.Close
	objExcel.Application.Quit
       
  End If 
 
	
	
		End Sub
		





		
		
Sub CalcToText()
		
Const ForAppending = 8 
    
    
    
    '-- Selects specified worksheet
    'Set objSheet = objExcel.ActiveWorkbook.Worksheets(ssheet)
    
    ' Get Worksheet collection
	Set objWorksheets = objWorkbook.Sheets

    ssheet = ssheet -1
    scell = scell -1
    
    ' Open worksheet 1 - note the count begins at 0 in OpenOffice
	Set objSheet = objWorksheets.getByIndex(ssheet)
        
    'We want to skip the header row, and then the blank row below
	loopCount = 1
	
	'WScript.Echo "Column " & (scell) & " and Row " & (loopCount) & " from Worksheet " & (ssheet)
		
	
	If (objSheet.getCellByPosition(scell, loopCount).String) = "" Then 
   
   	'WScript.Echo "Column " & (scell) & " and Row " & (loopCount) & " from Worksheet " & (ssheet) &" is empty"
   	
   								   
    objWorkbook.Close(true)
	'objExcel.Application.Quit
	set objServiceManager = Nothing
	Set objCoreReflection = Nothing
	Set objDesktop = Nothing
	Set objWorkbook = Nothing		
	
    WScript.Quit
   
   else   
    
    'Do while (objSheet.getCellByPosition(scell, loopCount).String) <> ""
    
    Do until (objSheet.getCellByPosition(scell, loopCount).String) = ""
  


	'For i = scell To scell
    'value = objSheet.Cells(loopCount, i).Value


  'Selects Column
  For i = scell To scell
    value = objSheet.getCellByPosition(i, loopCount).String
    
    
     ' Create the File System Object
'Set objFSO = CreateObject("Scripting.FileSystemObject")
  
  ' Check that the strDirectory folder exists
If objFSO.FolderExists(strLogPath) Then
   Set objFolder = objFSO.GetFolder(strLogPath)
Else
   Set objFolder = objFSO.CreateFolder(strLogPath)
   'WScript.Echo "Just created " & strExcelPath
End If
  
  
  
If objFSO.FileExists(strLogPath & strLogFile) Then
   Set objFolder = objFSO.GetFolder(strLogPath)
Else
   Set objFile = objFSO.CreateTextFile(strLogPath & strLogFile)
   'Wscript.Echo "Just created " & strExcelPath & strLogFile
End If   
  
  set objFile = nothing
  set objFolder = nothing

Set objFile = objFSO.OpenTextFile(strLogPath & strLogFile, ForAppending, True)

' Writes strText every time you run this VBScript
objFile.WriteLine(value)
'WScript.Echo value
objFile.Close
    
    
    
    
    'WScript.Echo value
            

Next
  loopCount = loopCount + 1

Loop
    
    objWorkbook.Close(true)
	set objServiceManager = Nothing
	Set objCoreReflection = Nothing
	Set objDesktop = Nothing
	Set objWorkbook = Nothing
   
  End If 
		



			
End Sub







		
		
		
Sub Dedup()
'-- Remove Duplicates Function

Const ForReading = 1
Const ForWriting = 2

Set objDictionary = CreateObject("Scripting.Dictionary")
Set objFile = objFSO.OpenTextFile(strLogPath & strLogFile)


Do Until objFile.AtEndOfStream
    strName = objFile.ReadLine
    If Not objDictionary.Exists(strName) Then
        objDictionary.Add strName, strName
    End If
Loop

objFile.Close

Set objFile = objFSO.OpenTextFile(strLogPath & strLogFile, ForWriting)

For Each strKey in objDictionary.Keys
    objFile.WriteLine strKey
Next

objFile.Close

End Sub 



Sub RemoveBlanks()
'-- Remove Blank Lines Function

Const ForReading = 1
Const ForWriting = 2

Set objFile = objFSO.OpenTextFile(strLogPath & strLogFile, ForReading)
 
Do Until objFile.AtEndOfStream
    strLine = objFile.Readline
    strLine = Trim(strLine)
    If Len(strLine) > 0 Then
        strNewContents = strNewContents & strLine & vbCrLf
    End If
Loop

objFile.Close

Set objFile = objFSO.OpenTextFile(strLogPath & strLogFile, ForWriting)

objFile.Write strNewContents

objFile.Close

set objFile = nothing
set objFolder = nothing

End Sub
