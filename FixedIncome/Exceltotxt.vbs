' *******************************************************************************************************************************
' 											      PsResearchUpload
'												   (Excel to Txt)
'											    Author: Youcef Laifa
'											      Date: 20/09/2012
'	
'	This script opens up Portfolio Security Reports and appends to one main report
'	Example: AppendSecReport.vbs ftitle (Morningstar) fpath (c:\portfolio\) fname (isin) dest (c:\fullportfolioreport\)
'
' *******************************************************************************************************************************

Dim objExcel, sMonth, sDay, sYear, numArgs, strExcelPath, strExcelFile, objFSO, objSheet, loopCount, i, strLogPath, strLogFile
Dim objFolder, objFile, sPath, isheet, ssheet, icell, scell, value, strLine, LogFile



' Bind to Excel object.
On Error Resume Next
Set objExcel = CreateObject("Excel.Application")
If (Err.Number <> 0) Then
    On Error GoTo 0
    Wscript.Echo "Excel application not found."
    Wscript.Quit
End If
On Error GoTo 0


'-- Create Date Function of current date  
 if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)


		Set numArgs = WScript.Arguments

		sPath = numArgs(0)

		isheet = numArgs(1)
		isheet=(Mid(isheet,10))
		ssheet= CInt (isheet)
		'WScript.Echo ssheet
		
		icell = numArgs(2)
		icell=(Mid(icell,5))
		scell= CInt (icell)
		'WScript.Echo scell
		
		

'strExcelPath = "\\192.168.2.163\fixincarch\" & sPath & "\" & sYear & "\" & sMonth & "_" & sYear & "\"
strExcelPath = "F:\" & sPath & "\" & sYear & "\" & sMonth & "_" & sYear & "\"
strExcelFile = "PSResearchUploaded" & sYear & "." & sMonth & "." & sDay & ".xls"
'WScript.Echo strExcelPath & strExcelFile

'strLogPath = "\\192.168.2.163\fixincarch\" & sPath & "\ExceltoText\"
strLogPath = "F:\" & sPath & "\ExceltoText\"
strLogFile = sYear & sMonth & sDay & ".txt"




'-- Checks to see if file exists otherwise quit program 
Set objFSO = CreateObject("Scripting.FileSystemObject")


If objFSO.FileExists(strExcelPath & strExcelFile) Then
		   
		   '-- Open specified spreadsheet
			objExcel.WorkBooks.Open strExcelPath & strExcelFile
			
		Else
		   
		   WScript.Echo "The Excel file you are trying to open does not exist, please click 'ok' to quit program"
		   
							   

							   
		   
		   
		   
		   
		   Wscript.Quit
		   
		End If



Call ExcelToText()
Call Dedup()
Call RemoveBlanks()

WScript.Quit


Sub ExcelToText()
		

     Const ForAppending = 8 
    
    
    '-- Selects specified worksheet
    Set objSheet = objExcel.ActiveWorkbook.Worksheets(ssheet)
    
    'We want to skip the header row, and then the blank row below
	loopCount = 2
	
	
	If IsEmpty (objSheet.Cells(loopCount, scell)) Then 
   
   	'WScript.Echo "Column " & (scell) & " and Row " & (loopCount) & " from Worksheet " & (ssheet) &" is empty"
   	
   								   
   	
   	
   	
   	
   	objExcel.ActiveWorkbook.Close
	objExcel.Application.Quit
    WScript.Quit
   
   else   
    
    Do while not isempty(objSheet.Cells(loopCount, scell).Value)
  

  'Selects Column
  For i = scell To scell
    value = objSheet.Cells(loopCount, i).Value
            
       
        ' Create the File System Object
'Set objFSO = CreateObject("Scripting.FileSystemObject")
  
  ' Check that the strDirectory folder exists
If objFSO.FolderExists(strLogPath) Then
   Set objFolder = objFSO.GetFolder(strLogPath)
Else
   Set objFolder = objFSO.CreateFolder(strLogPath)
   'WScript.Echo "Just created " & strExcelPath
End If
  
If objFSO.FileExists(strLogPath & strLogFile) Then
   Set objFolder = objFSO.GetFolder(strLogPath)
Else
   Set objFile = objFSO.CreateTextFile(strLogPath & strLogFile)
   'Wscript.Echo "Just created " & strExcelPath & strLogFile
End If   
  
set objFile = nothing
set objFolder = nothing


Set objFile = objFSO.OpenTextFile(strLogPath & strLogFile, ForAppending, True)

' Writes strText every time you run this VBScript
objFile.WriteLine(value)
'WScript.Echo value
objFile.Close
  
   
  Next
  loopCount = loopCount + 1

Loop
    
    objExcel.ActiveWorkbook.Close
	objExcel.Application.Quit
    'WScript.Quit
   
  End If 
 
      
End Sub




Sub Dedup()

Const ForReading = 1
Const ForWriting = 2

Set objDictionary = CreateObject("Scripting.Dictionary")
Set objFile = objFSO.OpenTextFile(strLogPath & strLogFile)


'-- Remove Duplicates Function

Do Until objFile.AtEndOfStream
    strName = objFile.ReadLine
    If Not objDictionary.Exists(strName) Then
        objDictionary.Add strName, strName
    End If
Loop

objFile.Close

Set objFile = objFSO.OpenTextFile(strLogPath & strLogFile, ForWriting)

For Each strKey in objDictionary.Keys
    objFile.WriteLine strKey
Next

objFile.Close
'WScript.Quit

End Sub 



Sub RemoveBlanks()
'-- Remove Blank Lines Function

Const ForReading = 1
Const ForWriting = 2

Set objFile = objFSO.OpenTextFile(strLogPath & strLogFile, ForReading)
 
Do Until objFile.AtEndOfStream
    strLine = objFile.Readline
    strLine = Trim(strLine)
    If Len(strLine) > 0 Then
        strNewContents = strNewContents & strLine & vbCrLf
    End If
Loop

objFile.Close

Set objFile = objFSO.OpenTextFile(strLogPath & strLogFile, ForWriting)

objFile.Write strNewContents

objFile.Close
'WScript.Quit

End Sub








