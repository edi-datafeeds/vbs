option explicit


'**************************************************************************
' 		 GENERIC COPYRENAMEFILE.VBS
' 	
' Script for copying files from one directory to another
'
' Arguments in order (0) Source Path
'		     		 (1) Destination Path
'		             (2) sfName
'		             (3) dfName
'**************************************************************************

'***** MAIN *****

'***** VARIABLES *****

	dim fso, WshShell, sPath, dPath, sfName, dfName, numArgs, sMonth, sDay, sYear, sCustomName 
'***** PREPERATION *****


	numArgs = WScript.Arguments.Count

	sPath = WScript.Arguments.Item(0)
	sfName = WScript.Arguments.Item(1)
	dPath = WScript.Arguments.Item(2)
	dfName = WScript.Arguments.Item(3)
	

	Set WshShell = WScript.CreateObject("WScript.Shell")
	Set fso = CreateObject("Scripting.FileSystemObject")




'***** PROCESSING *****


		
		
		
		'-- Build custom output file name, ensuring that
		  '		both sMonth and sDay are always two digits
		  
					  if Len(Month(Now))=1 then 
			  	sMonth="0" & month(now)
			  else
			  	sMonth=month(now)
			  end if
			  
			  if Len(Day(now))=1 then 
			  	sDay = "0" & day(Now)
			  else
			  	sDay = day(Now)
			  end if
			
			  'sYear = mid(Year(Now),3,4)
			  
			  sYear = Year(Now)
					  
		  '-- Create the Custom filename
		 
		    sCustomName = sYear & sMonth & sDay
	
		'MsgBox sCustomName	
		
		'MsgBox sPath & sCustomName & "_" & sfName



	'msgbox sPath & sCustomName & sfName ,  dPath & dfName

		

		'Function FileExists
			
			If (fso.FileExists(sPath & sCustomName & "_" & sfName)) Then
		      'msg = filespec & " exists."
		      fso.CopyFile sPath & sCustomName & "_" & sfName ,  dPath & dfName
		   ElseIf (fso.FileExists(sPath & sfName)) Then
		      'msg = filespec & " doesn't exist."
		      fso.CopyFile sPath & sfName ,  dPath & sCustomName & "_" & dfName
		   End If
		   'ReportFileStatus = msg
		'End Function
	
	
	
	
	'Function FileExists(sfName)

	'fso.CopyFile sPath & sCustomName & sfName ,  dPath & sCustomName & "_" & dfName
	
	'End Function
	
'***** END MAIN *****