' ****************************************************************
' GENERIC PUSHFTP.VBS
'
'     Script for pushing files to EDI ftp boxes
' 
' Arguments in order (0) Local path
'                    (1) Remote path
'                    (2) File extension
'
' ****************************************************************

' **** MAIN ****
	
' **** VARIABLES ****

	dim WshShell,sLocalPath,sRemotePath,sFileext,sOpsFtp,sDirection,sXRC,sYear,sMonth,sDay

	

' **** PREPARATION ****
	
	Set colArgs = WScript.Arguments
	
	'Get Year
	sYear = Year(Now)
	
	'Get Month
	if Len(Month(Now))=1 then 
	  sMonth="0" & Month(now)
	else
	  sMonth=Month(now)
	end if
	
	'Get Date
	if Len(Day(now))=1 then 
	  sDay = "0" & Day(Now)
	else
	  sDay = Day(Now)
  	end if

 
 
	
	sLocalPath = "-local " & replace(colArgs(0),"\","/") &" "
	sLocalPath = replace(sLocalPath,"YYYY",sYear)
	sLocalPath = replace(sLocalPath,"MM",sMonth)	
	'MsgBox(sLocalPath)
	sRemotePath = "-remote " & replace(colArgs(1),"\","/") &" "
	'MsgBox(sRemotePath)
	sFileext = "-fileext " & colArgs(2) & " "
	sFileext = replace(sFileext,"YYYY",sYear)
	sFileext = replace(sFileext,"MM",sMonth)
	sFileext = replace(sFileext,"DD",sDay)
	'MsgBox(sFileext)
	sOpsFtp = "J:\java\Prog\h.patel\opsftp\dist\opsftp.jar "
	sDirection = "-upload "
	sXRC = "-XCRC off "
	
	Set WshShell = WScript.CreateObject("WScript.Shell")


' **** PROCESSING ****
	
	' PUSH FILE TO DOTNET
	sSite = "-site dotnet "
	ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC, 1, true)
		If ftp1 = "1" Then
		ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)	
			If ftp1 = "1" Then
			'msgbox "DotNet Failed"	
			End If
		End if
	
	
	' PUSH FILE TO DOCOM
	sSite = "-site dotcom "
	ftp2=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC, 1, true)
		If ftp2 = "1" Then
		ftp2=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)	
			If ftp2 = "1" Then
			'msgbox "DotCom Failed"	
			End If
		End if
	
	
	' PUSH FILE TO ICLOUD
	sSite = "-site ICLOUD2 "
	ftp3=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC, 1, true)
		If ftp3 = "1" Then
		ftp3=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)	
			If ftp3 = "1" Then
			'msgbox "DotCom Failed"	
			End If
	
		End if
			
' **** END MAIN **** 