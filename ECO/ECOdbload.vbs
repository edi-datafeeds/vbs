'*************************************************************************
'
'                             ECO Tables Update Loader
'
'**************************************************************************

Option Explicit
'****Variables****
Dim loadtbl, CONF, dbconn, rs1, rs2, rs3, rs4, rs5, oFSO, oFSO_File, WshShell, PreserveDate, FromDate, FeedDate
Dim MyArray, ValID, ID1, ID2, pID, OldID, pOldID, TagID, Name, RecCD, RecTM, UsrID, PstCD, DoUpdate, FixedRecTM, Forecastflag
Dim MyArray2, Logfile, myquery, ClientNote, StaffNote, a, b, cd, type_cd, StartID, MaxID, CatID, NextID
'****Constants****
Const ForWriting = 2
Const ForAppending = 8
'****Load Argument Data
loadtbl = WScript.Arguments.Item(0)

Set WshShell = WScript.CreateObject("WScript.Shell")

'****Hard Coded Data****
CONF="o:\Auto\configs\dbservers.cfg"
Logfile="o:\prodman\eco\log\loadlog1.txt"
PreserveDate="Yes"

'****Initialise Log****
Set oFSO=CreateObject("Scripting.FileSystemObject")
Set oFSO_File=oFSO.OpenTextFile(Logfile, ForAppending, True)
'****DataBase Connections****
Call OPENDBCON(CONF, "MY_AFED")


'****Set Recordsets****
Set rs1=CreateObject("ADODB.Recordset")
Set rs2=CreateObject("ADODB.Recordset")
Set rs3=CreateObject("ADODB.Recordset")
Set rs4=CreateObject("ADODB.Recordset")
Set rs5=CreateObject("ADODB.Recordset")

'Set FromDate
If rs1.State = 1 Then rs1.Close
  rs1.Open "select max(RecTM) from eco.val where RecTM<>'2017-02-10 22:00:00'", dbconn
  If rs1.EOF Then
    MsgBox "Max(RecTM) could not be retrieved from eco.val"
  Else  
    FromDate=Mid(FORMAT_TM(rs1.Fields(0).Value),1,19)
  End If

'Set FeedDate
If rs1.State = 1 Then rs1.Close
rs1.Open "select max(Acttime) from afedivan.list_series_value", dbconn
If rs1.EOF Then
  MsgBox "Max(Acttime) could not be retrieved from afedivan.list_series_value"
Else  
  FeedDate=Mid(FORMAT_TM(rs1.Fields(0).Value),1,10)
End If

FixedRecTM=FeedDate &" 22:00:00"

'****Main****
If loadtbl="UPREF" Then 
  Call UPDATE_CAL
  Call UPDATE_CAT
  Call UPDATE_CTS
  Call UPDATE_FRQ
  Call UPDATE_GRP
  Call UPDATE_SCA
  Call UPDATE_SRC
  Call UPDATE_SUB
  Call UPDATE_UNT
'  Call UPDATE_VAL
'  Call UPDATE_VCL
'  Call VAL_UPDATE
ElseIf loadtbl="CAL" Then 
  Call UPDATE_CAL
ElseIf loadtbl="CAT" Then
  Call UPDATE_CAT
ElseIf loadtbl="CTS" Then
  Call UPDATE_CTS
ElseIf loadtbl="FRQ" Then
  Call UPDATE_FRQ
ElseIf loadtbl="LGEO" Then
' Loads GEO,LOC
  WshShell.Run "O:\Prodman\ECO\bat\OPS\GEO_RESET.bat", 1, True
  Call LOAD_GEO_LOCALITY
ElseIf loadtbl="DGEO" Then
' Loads GEO,LOC
  Call LOAD_GEO_DISTRICTSRC1
  Call LOAD_GEO_DISTRICT2
ElseIf loadtbl="LGEO_UP" Then
' Updates GEO,LOC
  Call UP_GEO_LOCALITY
ElseIf loadtbl="DGEO_UP" Then
' Updates GEO,LOC
  Call UP_GEO_DISTRICT1
  Call UP_GEO_DISTRICT2
ElseIf loadtbl="GRP" Then
  Call UPDATE_GRP
ElseIf loadtbl="SCA" Then
  Call UPDATE_SCA
ElseIf loadtbl="SRC" Then 
  Call UPDATE_SRC
ElseIf loadtbl="SUB" Then
  Call UPDATE_SUB
ElseIf loadtbl="SIN" Then
ElseIf loadtbl="LOADTIN" Then
' Loads TIN, LAB
'  WshShell.Run "O:\Prodman\ECO\bat\OPS\TIN_RESET.bat", 1, True
  Call RESET_TIN
  Call LOAD_TIN1
  Call LOAD_TIN2
  Call LOAD_TIN3
  Call LOAD_TIN4
ElseIf loadtbl="UNT" Then
  Call UPDATE_UNT
ElseIf loadtbl="UPMAIN" Then
' Loads VAL,ORF,STG,VTY,VLX
  Call UPDATE_VAL
ElseIf loadtbl="VCL" Then
  Call UPDATE_VCL
End if

oFSO_File.Close
WScript.Quit   

'*******************
'****SUBROUTINES****
'*******************
Function UPDATE_ecoLoadLog
UPDATE_ecoLoadLog="True"
End Function


Sub UPDATE_CAL 
' disappeared record deletion to pass on through feed
  dbconn.Execute "update eco.cal as newtab" &_
                 " left outer join afedivan.cal_type as oldtab on newtab.CalID=oldtab.ct_id" &_
                 " Set RecCD='D'," &_ 
                 " RecTM='" &FixedRecTM &"'" &_
                 " where newtab.RecCD<>'D' and oldtab.ct_id is null"
' records actually marked as deleted
  dbconn.Execute "update eco.cal as newtab" &_
                 " inner join afedivan.cal_type as oldtab on newtab.CalID=oldtab.ct_id" &_
                 " set RecTM=oldtab.modified," &_
                 " RecTM='" &FixedRecTM &"'" &_
                 " where newtab.RecCD<>'D' and oldtab.modified>newtab.rectm and oldtab.actflag='D'"

  If rs1.State = 1 Then rs1.Close
' Lookup cal_type
  rs1.Open "select * from afedivan.cal_type where actflag <> 'X'", dbconn
  rs1.MoveFirst

  Do
    If Not IsNull(rs1.Fields(3).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(3).Value) 
    Else
      RecTM=FixedRecTM
    End If
 
    If Not IsNull(rs1.Fields(4).Value) Then
      UsrID=rs1.Fields(4).Value
    ElseIf Not IsNull(rs1.Fields(2).Value) Then
      UsrID=rs1.Fields(2).Value
    Else
      UsrID="Null"
    End If

    If IsNull(rs1.Fields(13).Value) Then
      PstCD="I"
    ElseIf rs1.Fields(13).Value=0 Then
      PstCD="I"
    ElseIf rs1.Fields(13).Value=1 Then
      PstCD="A"
    ElseIf rs1.Fields(13).Value=2 Then
      PstCD="P"
    End If
  
  ' ID1=rs1.Fields(5).Value
    If rs2.State = 1 Then rs2.Close
    rs2.Open "select * from eco.cal where CalID  = " &rs1.Fields(5).Value, dbconn
    If rs2.EOF Then
    ' Insert new CAL
      dbconn.Execute "insert into eco.cal(RecCD, RecTM, UsrID, CalID, CalName, Sort, CalDesc, Q1Month, Q2Month, Q3Month, Q4Month, StartMonth, PstCD)" &_
                     " values('I','" &RecTM &"'," &UsrID &"," &ID1 &",'" &rs1.Fields(6).Value &"',"  &rs1.Fields(16).Value &",'" &rs1.Fields(7).Value &"'," &_
                                  rs1.Fields(8).Value &"," &rs1.Fields(9).Value &"," &rs1.Fields(10).Value &"," &rs1.Fields(11).Value &"," &rs1.Fields(12).Value &",'P')"
    Else
    ' Has pre-existing CAL changed
      DoUpdate="Yes"
      If rs2.Fields(4).value=rs1.Fields(6).Value Then 'calname
        If rs2.Fields(5).value=rs1.Fields(16).Value Then 'sort
          If rs2.Fields(6).value=rs1.Fields(7).Value Then 'caldesc
            If rs2.Fields(7).value=rs1.Fields(8).Value Then 'q1month
              If rs2.Fields(8).value=rs1.Fields(9).value Then 'q2month
                If rs2.Fields(9).value=rs1.Fields(10).Value Then 'q3month
                  If rs2.Fields(10).value=rs1.Fields(11).Value Then 'q4month
                    If rs2.Fields(11).value=rs1.Fields(12).value Then 'startmonth
                      If rs2.Fields(12).Value=PstCD Then 'pstcd
                        DoUpdate="No"
                      End if
                    End if
                  End if
                End if
              End if
            End if
          End if
        End if
      End if
    End If
    
    If DoUpdate="Yes" Then
      dbconn.Execute "update eco.cal set RecCD='U'" &_
                     ", RecTM='" &RecTM &"'" &_
                     ", UsrID=" &UsrID &_
                     ", CalName='" &rs1.Fields(6).Value &"'" &_
                     ", Sort=" &rs1.Fields(16).Value &_
                     ", CalDesc='" &rs1.Fields(7).Value &"'" &_
                     ", Q1Month=" &rs1.Fields(8).Value &_
                     ", Q2Month=" &rs1.Fields(9).Value &_
                     ", Q3Month=" &rs1.Fields(10).Value &_
                     ", Q4Month=" &rs1.Fields(11).Value &_
                     ", StartMonth=" &rs1.Fields(12).Value &_
                     ", PstCD='" &PstCD &"'" &_
                     " where CalID=" &rs1.Fields(5).Value
    End if 

    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
End Sub

Sub UPDATE_CAT
' disappeared record deletion to pass on through feed
  dbconn.Execute "update eco.cat as newtab" &_
                 " left outer join afedivan.source_category as oldtab on newtab.CatID=oldtab.cat_id" &_
                 " set RecCD='D'," &_ 
                 " RecTM='" &FixedRecTM &"'" &_
                 " where newtab.RecCD<>'D' and oldtab.cat_id is null"

  RecCD="I"
  UsrID="Null"

  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.source_category", dbconn
  rs1.MoveFirst

  Do
    If IsNull(rs1.Fields(2).Value) Then
      PstCD="I"
    ElseIf rs1.Fields(2).Value=0 Then
      PstCD="I"
    ElseIf rs1.Fields(2).Value=1 Then
      PstCD="A"
    ElseIf rs1.Fields(2).Value=2 Then
      PstCD="P"
    End If
    If rs2.State = 1 Then rs2.Close
    rs2.Open "select * from eco.cat where CatID  = " & rs1.Fields(0).Value, dbconn
    If rs2.EOF Then
    ' Insert new CAT
      dbconn.Execute "insert into eco.cat(RecCD, RecTM, UsrID, CatID, CatName, PstCD, Sort)" &_
                     " values('" &RecCD &"','" &FixedRecTM &"'," &UsrID &", " &rs1.Fields(0).Value &", '" &rs1.Fields(1).Value &"', '" &PstCD &"'," &_
                     " 1)"  
    Else 
    ' Has pre-existing CAL changed
      DoUpdate="Yes"
      If rs2.Fields(4).Value=rs1.Fields(1).value Then 'catname
        If rs2.Fields(5).value=PstCD Then 'PstCD
          DoUpdate="No"
        End if
      End If
    End If

    If DoUpdate="Yes" Then
      dbconn.Execute "update eco.cat set RecCD='U'" &_
                     ", RecTM='" &FixedRecTM &"'" &_
                     ", UsrID=" &UsrID &_
                     ", CatName='" &rs1.Fields(1).Value &"'" &_
                     ", PstCD='" &PstCD &"'" &_
                     " where CatID=" &rs1.Fields(0).Value
    End if 

    rs1.MoveNext
  Loop While Not rs1.EOF

  rs1.Close
End Sub


Sub UPDATE_CTS
Dim CtsID
' disappeared record deletion to pass on through feed
  dbconn.Execute "update eco.cts as newtab" &_
                 " left outer join afedivan.mtx_locality as oldtab on newtab.GtyCD=oldtab.type_cd" &_
                 " and newtab.CtsCD=oldtab.cd" &_
                 " and newtab.CtyCD=oldtab.value" &_
                 " set RecCD='D'," &_ 
                 " RecTM='" &FixedRecTM &"'" &_
                 " where newtab.RecCD<>'D' and oldtab.type_cd is null"
' records actually marked as deleted
  dbconn.Execute "update eco.cts as newtab" &_
                 " inner join afedivan.mtx_locality as oldtab on newtab.GtyCD=oldtab.type_cd" &_
                 " and newtab.CtsCD=oldtab.cd" &_
                 " and newtab.CtyCD=oldtab.value" &_
                 " set RecTM='" &FixedRecTM &"'," &_
                 " RecCD='D'" &_
                 " where newtab.RecCD<>'D' and oldtab.modified>newtab.rectm and oldtab.actflag='D'"

' Load Aggregate and Region CTS records
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select afedivan.mtx_locality.* from afedivan.mtx_locality" &_
           " where afedivan.mtx_locality.actflag <> 'D' and afedivan.mtx_locality.cd<>''" &_
           " and afedivan.mtx_locality.cd<>'EAZ' and afedivan.mtx_locality.cd<>'EAT'" &_
           " and (afedivan.mtx_locality.type_cd='A' or afedivan.mtx_locality.type_cd='R')" &_
           " and afedivan.mtx_locality.value<>'MH'" &_
           " order by afedivan.mtx_locality.type_cd, afedivan.mtx_locality.cd", dbconn
  rs1.MoveFirst
  Do
    If Not IsNull(rs1.Fields(3).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(3).Value) 
    ElseIf Not IsNull(rs1.Fields(1).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(1).Value) 
    Else
      RecTM=FixedRecTM
    End If
 
  ' Get RecCD
    If Not IsNull(rs1.Fields(0).Value) Then
      RecCD=rs1.Fields(0).Value
    Else 
      RecCD="I"
    End If
  ' Get UsrID
    If Not IsNull(rs1.Fields(4).Value) Then
      UsrID=rs1.Fields(4).Value
    ElseIf Not IsNull(rs1.Fields(2).Value) Then
      UsrID=rs1.Fields(2).Value
    Else
      UsrID="Null"
    End If
    If IsNull(rs1.Fields(8).Value) Then
      PstCD="I"
    ElseIf rs1.Fields(8).Value=0 Then
      PstCD="I"
    ElseIf rs1.Fields(8).Value=1 Then
      PstCD="A"
    ElseIf rs1.Fields(8).Value=2 Then
      PstCD="P"
    End If

    type_cd=rs1.Fields(6).value
    cd=rs1.Fields(5).Value
    
  ' Lookup CTS
    If rs2.State = 1 Then rs2.Close
    rs2.Open "select * from eco.cts where" &_
             " GtyCD='" &rs1.Fields(6).Value &"' and" &_
             " CtsCD='" &rs1.Fields(5).Value &"' and" &_
             " CtyCD='" &rs1.Fields(7).Value &"'", dbconn
    If rs2.EOF Then
      If rs3.State = 1 Then rs3.Close
      rs3.Open "select max(CtsID) from eco.cts", dbconn
      If IsNull(rs3.Fields(0).Value) Then
        NextID=1
      Else
        NextID=CStr(CLng(rs3.Fields(0).Value)+1)
      End If  
    ' Insert new CTS
      dbconn.Execute "insert into eco.cts(RecCD, RecTM, UsrID, CtsID, GtyCD, CtsCD, CtyCD, PstCD)" &_
                     " values('I','" &RecTM &"'," &UsrID &"," &NextID &"," &_
                     ",'" &type_CD &"','" &cd &"','" &rs1.Fields(7).Value & "'," &PstCD &"')"
    ' Store CtsID
      CtsID = NextID
    Else
      CtsID = rs2.Fields(3).Value  
     ' Has pre-existing CTS changed
      DoUpdate="Yes"
      If rs2.Fields(2).Value=UsrID Then 'UsrID
        If rs2.Fields(8).value=PstCD Then 'PstCD
          DoUpdate="No"
        End if
      End If
 
      If DoUpdate="Yes" Then
        dbconn.Execute "update eco.cts set RecCD='U'" &_
                       ", RecTM='" &RecTM &"'" &_
                       ", UsrID=" &UsrID &_
                       ", PstCD='" &PstCD &"'" &_
                       " where CtsID=" &CtsID
      End If                       
    End if 
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
End Sub

Sub UPDATE_FRQ 
' disappeared record deletion to pass on through feed
  dbconn.Execute "update eco.frq as newtab" &_
                 " left outer join afedivan.freq as oldtab on newtab.FrqID=oldtab.frq_id" &_
                 " Set RecCD='D'," &_ 
                 " RecTM='" &FixedRecTM &"'" &_
                 " where newtab.RecCD<>'D' and oldtab.frq_id is null"
' records actually marked as deleted
  dbconn.Execute "update eco.frq as newtab" &_
                 " inner join afedivan.freq as oldtab on newtab.FrqID=oldtab.frq_id" &_
                 " set RecTM='" &FixedRecTM &"'," &_
                 " RecCD='D'" &_
                 " where newtab.RecCD<>'D' and oldtab.modified>newtab.rectm and oldtab.actflag='D'"

  If rs1.State = 1 Then rs1.Close
' Lookup cal_type
  rs1.Open "select * from afedivan.freq where actflag <> 'D'", dbconn
  rs1.MoveFirst

  Do
    If Not IsNull(rs1.Fields(3).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(3).Value) 
    Else
      RecTM=FixedRecTM
    End If
 
    If Not IsNull(rs1.Fields(4).Value) Then
      UsrID=rs1.Fields(4).Value
    ElseIf Not IsNull(rs1.Fields(2).Value) Then
      UsrID=rs1.Fields(2).Value
    Else
      UsrID="Null"
    End If

    If IsNull(rs1.Fields(7).Value) Then
      PstCD="I"
    ElseIf rs1.Fields(7).Value=0 Then
      PstCD="I"
    ElseIf rs1.Fields(7).Value=1 Then
      PstCD="A"
    ElseIf rs1.Fields(7).Value=2 Then
      PstCD="P"
    End If

    ClientNote = rs1.Fields(8).value &" "
    
    ClientNote = Replace(ClientNote,"'","`")
    
    StaffNote = rs1.Fields(9).value &" "
    
    StaffNote = Replace(StaffNote,"'","`")
  

  ' ID1=rs1.Fields(5).Value
    If rs2.State = 1 Then rs2.Close
    rs2.Open "select * from eco.frq where frqID  = " &rs1.Fields(5).Value, dbconn
    If rs2.EOF Then
    ' Insert new FRQ
      dbconn.Execute "insert into eco.frq(RecCD, RecTM, UsrID, FrqID, FrqName, Sort, ClientNote, StaffNote)"&_
                     " values('" &RecCD &"','" &RecTM &"'," &UsrID &"," &rs1.Fields(5).Value &",'" &rs1.Fields(6).Value &"',"&_
                                  rs1.Fields(10).Value &",'" &ClientNote &"','" &StaffNote &"')"
    Else
    ' Has pre-existing FRQ changed
      DoUpdate="Yes"
      If rs2.Fields(4).value=rs1.Fields(6).Value Then 'frqname
        If rs2.Fields(5).value=rs1.Fields(10).Value Then 'sort
          If rs2.Fields(6).value=ClientNote Then
            If rs2.Fields(7).value=StaffNote Then
              If rs2.Fields(8).Value=PstCD Then 'pstcd
                DoUpdate="No"
              End if
            End if
          End if
        End if
      End if
    End If
    
    If DoUpdate="Yes" Then
      dbconn.Execute "update eco.frq set RecCD='U'" &_
                     ", RecTM='" &RecTM &"'" &_
                     ", UsrID=" &UsrID &_
                     ", FrqName='" &rs1.Fields(6).Value &"'" &_
                     ", Sort=" &rs1.Fields(10).Value &_
                     ", ClientNote='" &ClientNote &"'" &_
                     ", StaffNote='" &StaffNote &"'" &_
                     ", PstCD='" &PstCD &"'" &_
                     " where FrqID=" &rs1.Fields(5).Value
    End if 

    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
End Sub

Sub UPDATE_GRP 
' disappeared record deletion to pass on through feed
  dbconn.Execute "update eco.grp as newtab" &_
                 " left outer join afedivan.source_group as oldtab on newtab.GrpCD=oldtab.groupcd" &_
                 " Set RecCD='D'," &_ 
                 " RecTM='" &FixedRecTM &"'" &_
                 " where newtab.RecCD<>'D' and oldtab.groupcd is null"

  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.source_group", dbconn
  rs1.MoveFirst

  Do
    UsrID="Null"
    If IsNull(rs1.Fields(2).Value) Then
      PstCD="I"
    ElseIf rs1.Fields(2).Value=0 Then
      PstCD="I"
    ElseIf rs1.Fields(2).Value=1 Then
      PstCD="A"
    ElseIf rs1.Fields(2).Value=2 Then
      PstCD="P"
    End If

    If rs2.State = 1 Then rs2.Close
    rs2.Open "select * from eco.grp where GrpCD='" &rs1.Fields(0).Value &"'", dbconn
    If rs2.EOF Then
    ' Insert new GRP
      dbconn.Execute "insert into eco.grp(RecCD, RecTM, UsrID, GrpCD, GrpName, Sort)"&_
                    " values('I','" &FixedRecTM &"'," &UsrID &",'" &rs1.Fields(0).Value &"','" &rs1.Fields(1).Value &"',"&_
                                 rs1.Fields(3).Value &",'" &PstCD &"')"
    Else
    ' Has pre-existing GRP changed
      DoUpdate="Yes"
      If rs2.Fields(3).Value=rs1.Fields(0).Value Then 'grpcd
        If rs2.Fields(4).value=rs1.Fields(1).Value Then 'grpname
          If rs2.Fields(5).value=rs1.Fields(3).Value Then 'sort
            If rs2.Fields(6).Value=PstCD Then 'pstcd
              DoUpdate="No"
            End if
          End if
        End If
      End if
    End If
    
    If DoUpdate="Yes" Then
      dbconn.Execute "update eco.grp set RecCD='U'" &_
                     ", RecTM='" &FixedRecTM &"'" &_
                     ", UsrID=" &UsrID &_
                     ", GrpName='" &rs1.Fields(1).Value &"'" &_
                     ", Sort=" &rs1.Fields(3).Value &_
                     ", PstCD='" &PstCD &"'" &_
                     " where GrpCD='" &rs1.Fields(0).Value &"'"
    End if 

    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
End Sub

Sub UPDATE_SCA 
' disappeared record deletion to pass on through feed
  dbconn.Execute "update eco.sca as newtab" &_
                 " left outer join afedivan.scales as oldtab on newtab.ScaID=oldtab.sc_id" &_
                 " Set RecCD='D'," &_ 
                 " RecTM='" &FixedRecTM &"'" &_
                 " where newtab.RecCD<>'D' and oldtab.sc_id is null"
' records actually marked as deleted
  dbconn.Execute "update eco.sca as newtab" &_
                 " inner join afedivan.scales as oldtab on newtab.ScaID=oldtab.sc_id" &_
                 " set RecTM='" &FixedRecTM &"'," &_
                 " RecCD='D'" &_
                 " where newtab.RecCD<>'D' and oldtab.modified>newtab.rectm and oldtab.actflag='D'"

  If rs1.State = 1 Then rs1.Close
' Lookup cal_type
  rs1.Open "select * from afedivan.scales where actflag <> 'D'", dbconn
  rs1.MoveFirst

  Do
    If Not IsNull(rs1.Fields(3).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(3).Value) 
    Else
      RecTM=FixedRecTM
    End If
 
    If Not IsNull(rs1.Fields(4).Value) Then
      UsrID=rs1.Fields(4).Value
    ElseIf Not IsNull(rs1.Fields(2).Value) Then
      UsrID=rs1.Fields(2).Value
    Else
      UsrID="Null"
    End If

    If IsNull(rs1.Fields(7).Value) Then
      PstCD="I"
    ElseIf rs1.Fields(7).Value=0 Then
      PstCD="I"
    ElseIf rs1.Fields(7).Value=1 Then
      PstCD="A"
    ElseIf rs1.Fields(7).Value=2 Then
      PstCD="P"
    End If

    ClientNote = rs1.Fields(8).value &" "
    
    ClientNote = Replace(ClientNote,"'","`")
    
    StaffNote = rs1.Fields(9).value &" "
    
    StaffNote = Replace(StaffNote,"'","`")
  

  ' ID1=rs1.Fields(5).Value
    If rs2.State = 1 Then rs2.Close
    rs2.Open "select * from eco.sca where scaID  = " &rs1.Fields(5).Value, dbconn
    If rs2.EOF Then
    ' Insert new SCA
      dbconn.Execute "insert into eco.sca(RecCD, RecTM, UsrID, ScaID, ScaName, Sort, ClientNote, StaffNote, PstCD)"&_
                     " values('" &RecCD &"','" &RecTM &"'," &UsrID &"," &rs1.Fields(5).Value &",'" &rs1.Fields(6).Value &"',"&_
                                  rs1.Fields(10).Value &",'" &ClientNote &"','" &StaffNote &"','" &PstCD & "')"
    Else
    ' Has pre-existing SCA changed
      DoUpdate="Yes"
      If rs2.Fields(4).value=rs1.Fields(6).Value Then 'scaname
        If rs2.Fields(5).value=rs1.Fields(10).Value Then 'sort
          If rs2.Fields(6).value=ClientNote Then
            If rs2.Fields(7).value=StaffNote Then
              If rs2.Fields(8).Value=PstCD Then 'pstcd
                DoUpdate="No"
              End if
            End if
          End If
        End if
      End if
    End If
    
    If DoUpdate="Yes" Then
      dbconn.Execute "update eco.sca set RecCD='U'" &_
                     ", RecTM='" &RecTM &"'" &_
                     ", UsrID=" &UsrID &_
                     ", ScaName='" &rs1.Fields(6).Value &"'" &_
                     ", Sort=" &rs1.Fields(10).Value &_
                     ", ClientNote='" &ClientNote &"'" &_
                     ", StaffNote='" &StaffNote &"'" &_
                     ", PstCD='" &PstCD &"'" &_
                     " where ScaID=" &rs1.Fields(5).Value
    End if 

    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
End Sub

Sub UPDATE_SRC 
' disappeared record deletion to pass on through feed
  dbconn.Execute "update eco.src as newtab" &_
                 " left outer join afedivan.source as oldtab on newtab.SrcID=oldtab.src_id" &_
                 " Set RecCD='D'," &_ 
                 " RecTM='" &FixedRecTM &"'" &_
                 " where newtab.RecCD<>'D' and oldtab.src_id is null"
' records actually marked as deleted
  dbconn.Execute "update eco.src as newtab" &_
                 " inner join afedivan.source as oldtab on newtab.SrcID=oldtab.src_id" &_
                 " set RecTM='" &FixedRecTM &"'," &_
                 " RecCD='D'" &_
                 " where newtab.RecCD<>'D' and oldtab.modified>newtab.rectm and oldtab.actflag='D'"

  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.source where actflag <> 'D'", dbconn
  rs1.MoveFirst

  Do
    If Not IsNull(rs1.Fields(3).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(3).Value) 
    Else
      RecTM=FixedRecTM
    End If
 
    If Not IsNull(rs1.Fields(4).Value) Then
      UsrID=rs1.Fields(4).Value
    ElseIf Not IsNull(rs1.Fields(2).Value) Then
      UsrID=rs1.Fields(2).Value
    Else
      UsrID="Null"
    End If

    If IsNull(rs1.Fields(8).Value) Then
      PstCD="I"
    ElseIf rs1.Fields(8).Value=0 Then
      PstCD="I"
    ElseIf rs1.Fields(8).Value=1 Then
      PstCD="A"
    ElseIf rs1.Fields(8).Value=2 Then
      PstCD="P"
    End If

    If Not IsNull(rs1.Fields(14).Value) Then
      Forecastflag=rs1.Fields(14).Value
    Else
      Forecastflag="Null"
    End If

    If Not IsNull(rs1.Fields(10).Value) Then
      CatID=rs1.Fields(10).Value
    Else
      CatID="Null"
    End If

    ClientNote = rs1.Fields(11).value &" "
    
    ClientNote = Replace(ClientNote,"'","`")
    
    StaffNote = rs1.Fields(12).value &" "
    
    StaffNote = Replace(StaffNote,"'","`")
  

    If rs2.State = 1 Then rs2.Close
    rs2.Open "select * from eco.src where SrcID  = " &rs1.Fields(5).Value, dbconn
    If rs2.EOF Then
    ' Insert new SRC
      dbconn.Execute "insert into eco.src(RecCD, RecTM, UsrID, SrcID, SrcName, Sort, CatID, GrpCD, SrcCD, ClientNote, StaffNote, PstCD, Forecastflag)"&_
                     " values('I','" &RecTM &"'," &UsrID &"," &rs1.Fields(5).Value &",'" &Replace(rs1.Fields(7).Value,"'","`") &"',"&_
                                  rs1.Fields(13).Value &"," &CatID &",'" &rs1.Fields(9).Value &"','" &rs1.Fields(6).Value &"','" &_
                                  ClientNote &"','" &StaffNote &"','" &PstCD &"','" &Forecastflag & "')"
    Else
    ' Has pre-existing SRC changed
      DoUpdate="Yes"
      If rs2.Fields(4).value=Replace(rs1.Fields(7).Value,"'","`") Then 'srcname
        If rs2.Fields(5).value=rs1.Fields(13).Value Then 'sort
          If rs2.Fields(6).value=CatID Then
            If rs2.Fields(7).value=rs1.Fields(9).Value Then 'grpcd
              If rs2.Fields(8).value=rs1.Fields(6).Value Then 'srccd
                If rs2.Fields(9).value=ClientNote Then
                  If rs2.Fields(10).value=StaffNote Then
                    If rs2.Fields(11).Value=PstCD Then 'pstcd
                      If rs2.Fields(12).value=Forecastflag Then
                        DoUpdate="No"
                      End if
                    End If
                  End If
                End If
              End if
            End if
          End If
        End If
      End if
    End If
    
    If DoUpdate="Yes" Then
      dbconn.Execute "update eco.src set RecCD='U'" &_
                     ", RecTM='" &RecTM &"'" &_
                     ", UsrID=" &UsrID &_
                     ", SrcName='" &Replace(rs1.Fields(7).Value,"'","`") &"'" &_
                     ", Sort=" &rs1.Fields(13).Value &_
                     ", CatID=" &CatID &_
                     ", GrpCD='" &rs1.Fields(9).Value &"'" &_
                     ", SrcCD='" &rs1.Fields(6).Value &"'" &_
                     ", ClientNote='" &ClientNote &"'" &_
                     ", StaffNote='" &StaffNote &"'" &_
                     ", PstCD='" &PstCD &"'" &_
                     ", Forecastflag=" &Forecastflag &_
                     " where SrcID=" &rs1.Fields(5).Value
    End if 

    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
End Sub

Sub UPDATE_SUB 
' disappeared record deletion to pass on through feed
  dbconn.Execute "update eco.sub as newtab" &_
                 " left outer join afedivan.sub_source as oldtab on newtab.SubID=oldtab.sbsrc_id" &_
                 " Set RecCD='D'," &_ 
                 " RecTM='" &FixedRecTM &"'" &_
                 " where newtab.RecCD<>'D' and oldtab.sbsrc_id is null"
' records actually marked as deleted
  dbconn.Execute "update eco.sub as newtab" &_
                 " inner join afedivan.sub_source as oldtab on newtab.SubID=oldtab.sbsrc_id" &_
                 " set RecTM='" &FixedRecTM &"'," &_
                 " RecCD='D'" &_
                 " where newtab.RecCD<>'D' and oldtab.modified>newtab.rectm and oldtab.actflag='D'"

  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.sub_source where actflag <> 'D'", dbconn
  rs1.MoveFirst

  Do
    If Not IsNull(rs1.Fields(3).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(3).Value) 
    Else
      RecTM=FixedRecTM
    End If
 
    If Not IsNull(rs1.Fields(4).Value) Then
      UsrID=rs1.Fields(4).Value
    ElseIf Not IsNull(rs1.Fields(2).Value) Then
      UsrID=rs1.Fields(2).Value
    Else
      UsrID="Null"
    End If

    If IsNull(rs1.Fields(9).Value) Then
      PstCD="I"
    ElseIf rs1.Fields(9).Value=0 Then
      PstCD="I"
    ElseIf rs1.Fields(9).Value=1 Then
      PstCD="A"
    ElseIf rs1.Fields(9).Value=2 Then
      PstCD="P"
    End If

    If Not IsNull(rs1.Fields(13).Value) Then
      Forecastflag=rs1.Fields(13).Value
    Else
      Forecastflag="Null"
    End If

    ClientNote = rs1.Fields(10).value &" "
    
    ClientNote = Replace(ClientNote,"'","`")
    
    StaffNote = rs1.Fields(11).value &" "
    
    StaffNote = Replace(StaffNote,"'","`")
  

    If rs2.State = 1 Then rs2.Close
    rs2.Open "select * from eco.sub where SubID  = " &rs1.Fields(5).Value, dbconn
    If rs2.EOF Then
    ' Insert new SUB
      dbconn.Execute "insert into eco.sub(RecCD, RecTM, UsrID, SubID, SubName, Sort, SrcID, SrcCD, ClientNote, StaffNote, PstCD, Forecastflag)"&_
                     " values('I','" &RecTM &"'," &UsrID &"," &rs1.Fields(5).Value &",'" &Replace(rs1.Fields(7).Value,"'","`") &"',"&_
                                  rs1.Fields(12).Value &"," &rs1.Fields(8).Value &",'" &rs1.Fields(6).Value &"','" &_
                                  ClientNote &"','" &StaffNote &"','" &PstCD &"','" &Forecastflag & "')"
    Else
    ' Has pre-existing SRC changed
      DoUpdate="Yes"
      If rs2.Fields(4).value=Replace(rs1.Fields(7).Value,"'","`") Then 'subname
        If rs2.Fields(5).value=rs1.Fields(12).Value Then 'sort
          If rs2.Fields(6).value=rs1.Fields(8).Value Then 'srcid
            If rs2.Fields(7).value=rs1.Fields(6).Value Then 'srccd
              If rs2.Fields(8).value=ClientNote Then
                If rs2.Fields(9).value=StaffNote Then
                  If rs2.Fields(10).Value=PstCD Then 'pstcd
                    If rs2.Fields(11).value=Forecastflag Then
                      DoUpdate="No"
                    End if
                  End If
                End If
              End if
            End if
          End If
        End If
      End If
    End If
    
    If DoUpdate="Yes" Then
      dbconn.Execute "update eco.sub set RecCD='U'" &_
                     ", RecTM='" &RecTM &"'" &_
                     ", UsrID=" &UsrID &_
                     ", SubName='" &Replace(rs1.Fields(7).Value,"'","`") &"'" &_
                     ", Sort=" &rs1.Fields(12).Value &_
                     ", SrcID=" &rs1.Fields(8).Value &_
                     ", SrcCD='" &rs1.Fields(6).Value &"'" &_
                     ", ClientNote='" &ClientNote &"'" &_
                     ", StaffNote='" &StaffNote &"'" &_
                     ", PstCD='" &PstCD &"'" &_
                     ", Forecastflag=" &Forecastflag &_
                     " where SubID=" &rs1.Fields(5).Value
    End if 

    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
End Sub

Sub UPDATE_UNT 
' disappeared record deletion to pass on through feed
  dbconn.Execute "update eco.unt as newtab" &_
                 " left outer join afedivan.units as oldtab on newtab.UntID=oldtab.unt_id" &_
                 " Set RecCD='D'," &_ 
                 " RecTM='" &FixedRecTM &"'" &_
                 " where newtab.RecCD<>'D' and oldtab.unt_id is null"
' records actually marked as deleted
  dbconn.Execute "update eco.unt as newtab" &_
                 " inner join afedivan.units as oldtab on newtab.UntID=oldtab.unt_id" &_
                 " set RecTM='" &FixedRecTM &"'," &_
                 " RecCD='D'" &_
                 " where newtab.RecCD<>'D' and oldtab.modified>newtab.rectm and oldtab.actflag='D'"

  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.units where actflag <> 'D'", dbconn
  rs1.MoveFirst

  Do
    If Not IsNull(rs1.Fields(3).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(3).Value) 
    Else
      RecTM=FixedRecTM
    End If
 
    If Not IsNull(rs1.Fields(4).Value) Then
      UsrID=rs1.Fields(4).Value
    ElseIf Not IsNull(rs1.Fields(2).Value) Then
      UsrID=rs1.Fields(2).Value
    Else
      UsrID="Null"
    End If

    If IsNull(rs1.Fields(7).Value) Then
      PstCD="I"
    ElseIf rs1.Fields(7).Value=0 Then
      PstCD="I"
    ElseIf rs1.Fields(7).Value=1 Then
      PstCD="A"
    ElseIf rs1.Fields(7).Value=2 Then
      PstCD="P"
    End If

    ClientNote = rs1.Fields(9).value &" "
    
    ClientNote = Replace(ClientNote,"'","`")
    
    StaffNote = rs1.Fields(10).value &" "
    
    StaffNote = Replace(StaffNote,"'","`")
  

  ' ID1=rs1.Fields(5).Value
    If rs2.State = 1 Then rs2.Close
    rs2.Open "select * from eco.unt where UntID  = " &rs1.Fields(5).Value, dbconn
    If rs2.EOF Then
    ' Insert new UNT
      dbconn.Execute "insert into eco.unt(RecCD, RecTM, UsrID, UntID, UntName, Sort, ClientNote, StaffNote, PstCD)"&_
                     " values('" &RecCD &"','" &RecTM &"'," &UsrID &"," &rs1.Fields(5).Value &",'" &Replace(rs1.Fields(6).Value,"'","`") &"',"&_
                                  rs1.Fields(11).Value &",'" &ClientNote &"','" &StaffNote &"','" &PstCD & "')"
    Else
    ' Has pre-existing UNT changed
      DoUpdate="Yes"
      If rs2.Fields(4).value=rs1.Fields(6).Value Then 'untname
        If rs2.Fields(6).value=rs1.Fields(11).Value Then 'sort
          If rs2.Fields(7).value=ClientNote Then
            If rs2.Fields(8).value=StaffNote Then
              If rs2.Fields(9).Value=PstCD Then 'pstcd
                DoUpdate="No"
              End if
            End if
          End If
        End If
      End if
    End If
    
    If DoUpdate="Yes" Then
      dbconn.Execute "update eco.unt set RecCD='U'" &_
                     ", RecTM='" &RecTM &"'" &_
                     ", UsrID=" &UsrID &_
                     ", UntName='" &Replace(rs1.Fields(6).Value,"'","`") &"'" &_
                     ", Sort=" &rs1.Fields(11).Value &_
                     ", ClientNote='" &ClientNote &"'" &_
                     ", StaffNote='" &StaffNote &"'" &_
                     ", PstCD='" &PstCD &"'" &_
                     " where UntID=" &rs1.Fields(5).Value
    End if 

    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
End Sub

Sub UPDATE_VAL()
Dim StgID, VtyID, GeoID, IndID, PstCD, PubDT, EffDT, Actby, DoUpdate
' counters
Dim TotCt, ValinsCt, ValupdCt, ValdelCt, ValerrCt, OrfinsCt, OrfupdCt, GeoerrCt, TinerrCt, SetinsCt, SeterrCt, VtyinsCt, VtyerrCt, VlxinsCt, VlxupdCt, ValinDel, NoUpdate

  TotCt=0
  ValinsCt=0
  ValinDel=0
  ValupdCt=0
  ValdelCt=0
  NoUpdate=0
  ValerrCt=0
  OrfinsCt=0
  OrfupdCt=0
  GeoerrCt=0
  TinerrCt=0
  SetinsCt=0
  SeterrCt=0
  VtyinsCt=0
  VtyerrCt=0
  VlxinsCt=0
  VlxupdCt=0
  DoUpdate="Yes"

' disappeared record deletion to pass on through feed
  dbconn.Execute "update eco.val as newtab" &_
                 " left outer join afedivan.list_series_value as oldtab on newtab.value_id=oldtab.value_id" &_
                 " set RecCD='D', RecTM='" &FixedRecTM &"'" &_
                 " where newtab.RecCD<>'D' and newtab.RecTM>'2017-01-01' and oldtab.value_id is null"
  dbconn.Execute "update eco.val as newtab" &_
                 " left outer join afedivan.list_series_value as oldtab on newtab.value_id=oldtab.value_id" &_
                 " set RecCD='D', RecTM='" &FixedRecTM &"'" &_
                 " where newtab.RecCD<>'D' and newtab.RecTM>'2016-07-01' and newtab.RecTM<'2017-01-01' and oldtab.value_id is null"
  dbconn.Execute "update eco.val as newtab" &_
                 " left outer join afedivan.list_series_value as oldtab on newtab.value_id=oldtab.value_id" &_
                 " set RecCD='D', RecTM='" &FixedRecTM &"'" &_
                 " where newtab.RecCD<>'D' and newtab.RecTM>'2016-01-01' and newtab.RecTM<'2016-07-01' and oldtab.value_id is null"
  dbconn.Execute "update eco.val as newtab" &_
                 " left outer join afedivan.list_series_value as oldtab on newtab.value_id=oldtab.value_id" &_
                 " set RecCD='D', RecTM='" &FixedRecTM &"'" &_
                 " where newtab.RecCD<>'D' and newtab.RecTM<'2016-01-01' and oldtab.value_id is null"
' records actually marked as deleted
  dbconn.Execute "update eco.val as newtab" &_
                 " inner join afedivan.list_series_value as oldtab on newtab.value_id=oldtab.value_id" &_
                 " set RecTM=oldtab.acttime," &_
                 " RecCD='D'" &_
                 " where oldtab.acttime>'" &Fromdate &"' and newtab.RecCD<>'D' and oldtab.acttime>newtab.rectm and oldtab.actflag='D'"
  dbconn.Execute "update eco.vlx as vx" &_
                 " inner join eco.val as va on vx.valid=va.valid" &_
                 " set vx.RecTM= va.RecTM," &_
                 " vx.RecCD='D'" &_
                 " where va.RecTM>'" &Fromdate &"' and vx.RecCD<>'D' and va.RecCD='D'"


' get the latest deleted count
  If rs2.State = 1 Then rs2.Close
  rs2.Open "select count(valid) from eco.val" &_
           " where RecTM>'" &FromDate &"' and RecCD='D'", dbconn
  If rs2.EOF Then
    ValdelCt=0
  Else  
    ValdelCt=CLng(rs2.Fields(0).Value)
  End If

  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.list_series_value" &_
           " where acttime>'" &FromDate &"' and actflag<>'D'", dbconn
  If rs1.EOF Then
    MsgBox "No new LSV data to load to ECO database."
    APPENDLOG "*************************************************" &vbCrLf
    APPENDLOG "Run at: " &FORMAT_TM(Now()) &vbCrLf
    APPENDLOG "No new LSV data to load to ECO database." &vbCrLf
    APPENDLOG "***************** End of Report *****************" &vbCrLf
    oFSO_File.Close
    WScript.Quit   
  End If
  rs1.MoveFirst
  Do
    TotCt = TotCt+1
    ID1 = rs1.Fields(0).Value
    'ValID = ValID+1
    If Not IsNull(rs1.Fields(2).Value) Then
      UsrID = rs1.Fields(2).Value
    ElseIf IsNull(rs1.Fields(23).Value) Then
      UsrID = rs1.Fields(23).Value
    Else
      UsrID="Null"
    End If
  ' Get RecTM
    If PreserveDate="Yes" Then
      RecTM=FORMAT_TM(rs1.Fields(3).Value) 
    Else
      RecTM=FixedRecTM
    End If

    EffDT="null"
  ' Get EffDT
    If IsNull(rs1.Fields(31).Value) Then
      EffDT="null"
    ElseIf rs1.Fields(31).Value = "0001-09-30" then
      EffDT="2016-09-30"
    ElseIf rs1.Fields(31).Value = "0001-09-30" Then
      EffDT="2016-09-30"
    Else   
      EffDT=RTrim(FORMAT_TM(rs1.Fields(31).Value)) 
    End If  
  ' Get PubDT
    If Not IsNull(rs1.Fields(30).Value) Then
      PubDT=RTrim(FORMAT_TM(rs1.Fields(30).Value)) 
    Else 
      PubDT="null"
    End If
  ' Set PstCD
    If IsNull(rs1.Fields(22).Value) Then
      PstCD="I"
    ElseIf rs1.Fields(22).Value=0 Then
      PstCD="I"
    ElseIf rs1.Fields(22).Value=1 Then
      PstCD="A"
    ElseIf rs1.Fields(22).Value=2 Then
      PstCD="P"
    End If
  ' Get Actby
    If Not IsNull(rs1.Fields(20).Value) Then
      ActBy=rs1.Fields(20).Value
    Else
      ActBy="Null"
    End If

  ' Lookup ORF
    If rs2.State = 1 Then rs2.Close
    rs2.Open "select * from eco.orf where OrfID  = " &rs1.Fields(29).Value, dbconn
    If rs2.EOF Then
    ' Insert new ORF
      dbconn.Execute "insert into eco.orf(RecCD, RecTM, UsrID, OrfID, OrfName, SrcID, SubID, PubDT, PstCD)" &_
                     " values('I','" &RecTM &"'," &UsrID &"," &rs1.Fields(29).Value &_
                     ",'" &Replace(rs1.Fields(34).Value &" ","'","`") &"'," &rs1.Fields(4).Value &"," &rs1.Fields(5).Value &",'" &PubDT &"','" &PstCD &"')"
      OrfinsCt = OrfinsCt+1
    Else
    ' If there are field changes update ORF otherwise skip
      DoUpdate="Yes"
      If rs2.Fields(1).Value<=rs1.Fields(3).value Then
        If rs2.Fields(3).value=rs1.Fields(29).value Then
          If rs2.Fields(5).value=rs1.Fields(4).value Then
            If rs2.Fields(6).value=rs1.Fields(5).value Then
              If Trim(FORMAT_TM(rs2.Fields(8).value))=PubDT Then
                If rs2.Fields(9).value=PstCD Then
                  DoUpdate="No"
                End If
              End If
            End If
          End If
        End If
      End If
      If rs1.Fields(1).value="D" Then
        DoUpdate="No"
      End If  
      If DoUpdate="Yes" Then
        dbconn.Execute "update eco.orf set RecCD='U'" &_
                       ", RecTM='" &RecTM &"'" &_
                       ", UsrID=" &UsrID &_
                       ", OrfName='" &Replace(rs1.Fields(34).Value &" ","'","`") &"'" &_
                       ", SrcID=" &rs1.Fields(4).value &_
                       ", SubID=" &rs1.Fields(5).value &_
                       ", PubDT='" &PubDT &"'" &_
                       ", PstCD='" &PstCD &"'" &_
                       " where OrfID=" &rs1.Fields(29).Value
        OrfupdCt = OrfupdCt+1
      End If
    End If ' * * * Lookup ORF

  ' Lookup VTY
    If rs2.State = 1 Then rs2.Close
    rs2.Open "select VtyID from eco.vty where VclID  = " &rs1.Fields(17).Value &_
                                                      " and CalID  = " &rs1.Fields(32).Value &_
                                                      " and FrqID  = " &rs1.Fields(16).Value, dbconn
    If rs2.EOF Then
      If rs3.State = 1 Then rs3.Close
      rs3.Open "select max(VtyID) from eco.vty", dbconn
      If IsNull(rs3.Fields(0).Value) Then
        NextID=1
      Else
        NextID=CStr(CLng(rs3.Fields(0).Value)+1)
      End If  
    ' Insert new VTY
      dbconn.Execute "insert into eco.vty(RecCD, RecTM, UsrID, VtyID, VclID, CalID, FrqID)" &_
                     " values('I','" &RecTM &"'," &UsrID &"," &NextID &"," &rs1.Fields(17).Value &"," &_
                                  rs1.Fields(32).Value &"," &rs1.Fields(16).Value &")"
      VtyinsCt = VtyinsCt+1
    ' Store VtyID for VAL insert/update
      VtyID = NextID
    Else
      VtyID = rs2.Fields(0).Value  
    End If

  ' Lookup STG
    If rs2.State = 1 Then rs2.Close
    rs2.Open "select StgID from eco.stg where UntID  = " &rs1.Fields(10).Value &_
                                                      " and ScaID  = " &rs1.Fields(9).Value &_
                                                      " and Base  = '" &rs1.Fields(15).Value &"'", dbconn
    If rs2.EOF Then
      If rs3.State = 1 Then rs3.Close
      rs3.Open "select max(StgID) from eco.stg", dbconn
      If IsNull(rs3.Fields(0).Value) Then
        NextID=1
      Else
        NextID=CStr(CLng(rs3.Fields(0).Value)+1)
      End If  
    ' Insert new STG
      dbconn.Execute "insert into eco.stg(RecCD, RecTM, UsrID, StgID, UntID, ScaID, Base)" &_
                     " values('I','" &RecTM &"'," &UsrID &"," &NextID &"," &rs1.Fields(10).Value &"," &_
                                  rs1.Fields(9).Value &",'" &rs1.Fields(15).Value &"')"
      SetinsCt = SetinsCt+1
    ' Store StgID for VAL insert/update
      StgID = NextID
    Else
      StgID = rs2.Fields(0).Value  
    End If ' * * * Lookup STG
    
  ' get GeoID
    If  rs1.Fields(26).Value = "0" Then
      type_cd=rs1.Fields(7).Value
      If rs2.State = 1 Then rs2.Close
      If rs1.Fields(8).Value="EP" Or rs1.Fields(8).Value="EF" Then
        type_cd="S"
      ElseIf rs1.Fields(8).Value="ET" Then 
        type_cd="C"
      End If
      rs2.Open "select GeoID from eco.geo where GtyCD  = '" &type_cd &"' and CtsCD = '" &rs1.Fields(8).Value &"'", dbconn
      If rs2.EOF Then
        GeoID=888888
        GeoerrCt = GeoerrCt+1
      Else
        GeoID = rs2.Fields(0).Value
      End If
    Else
      If rs2.State = 1 Then rs2.Close
      rs2.Open "select GeoID from eco.geo where CtyCD  = '" &rs1.Fields(8).Value &"' and Dist_ID = " &rs1.Fields(26).Value, dbconn
      If rs2.EOF Then
        GeoID=999999
        GeoerrCt = GeoerrCt+1
      Else
        GeoID = rs2.Fields(0).Value
      End If
    End If ' * * * Lookup GEO
   
  ' Get IndID
    If rs2.State = 1 Then rs2.Close
    rs2.Open "select TinID from eco.tin where LtyCD='I' and OldTinID = "&rs1.Fields(6).Value, dbconn
    If rs2.EOF Then
      IndID=999999
      TinerrCt = TinerrCt+1
    Else
      IndID = rs2.Fields(0).Value
    End If
  
    If rs2.State = 1 Then rs2.Close
  ' Lookup VAL with old value_id
    rs2.Open "select * from eco.val where Value_ID  = " &rs1.Fields(0).Value, dbconn
  ' If not exists new record
    If rs2.EOF Then
      If rs3.State = 1 Then rs3.Close
      rs3.Open "select max(ValID) from eco.val", dbconn
      If IsNull(rs3.Fields(0).Value) Then
        NextID=1
      Else
        NextID=CStr(CLng(rs3.Fields(0).Value)+1)
      End If  
      dbconn.Execute "insert into eco.val(RecCD, RecTM, UsrID, ValID, Amount, EffDT, StgID, VtyID, OrfID, IndID, GeoID, Lbl_ID, Value_ID)" &_
                     " values('I','" &RecTM &"'," &UsrID &"," &NextID &"," &rs1.Fields(11).Value &",'" &EffDT &"'," &StgID &"," &_
                                  VtyID &"," &rs1.Fields(29).Value &"," &IndID &"," &GeoID  &"," &rs1.Fields(6).Value &"," &rs1.Fields(0).Value &")"
      ValinsCt = ValinsCt+1
      ValID=NextID
    Else
    ' Store in variable
      ValID=rs2.Fields(3).Value
    ' Has pre-existing VAL changed
      DoUpdate="Yes"
      If rs2.Fields(1).Value<=rs1.Fields(3).value Then
        If CStr(rs2.Fields(4).value)=CStr(rs1.Fields(11).value) Then
          If Trim(FORMAT_TM(rs2.Fields(5).value))=RTrim(FORMAT_TM(rs1.Fields(31).value)) Then
            If rs2.Fields(6).value=StgID Then
              If rs2.Fields(7).value=VtyID Then
                If rs2.Fields(8).value=rs1.Fields(29).value Then
                  If rs2.Fields(9).value=IndID Then
                    If rs2.Fields(10).value=GeoID Then
                      If CStr(rs2.Fields(11).value)=CStr(rs1.Fields(6).value) Then
                        If rs2.Fields(0).value=rs1.Fields(1).value Then
                          DoUpdate="No"
                        End if  
                      End if  
                    End if  
                  End if  
                End if  
              End if  
            End if  
          End if  
        End if  
      End If
      If DoUpdate="Yes" Then
        dbconn.Execute "update eco.val set RecCD='" &rs1.Fields(1).value &"'" &_
                       ", RecTM='" &RecTM &"'" &_
                       ", UsrID=" &UsrID &_
                       ", Amount=" &rs1.Fields(11).Value &_
                       ", EffDT='" &EffDT &"'" &_
                       ", StgID=" &StgID &_
                       ", VtyID=" &VtyID &_
                       ", OrfID=" &rs1.Fields(29).Value &_
                       ", IndID=" &IndID &_
                       ", GeoID=" &GeoID &_
                       ", Lbl_ID=" &rs1.Fields(6).Value &_
                       " where ValID=" &Valid
        If rs1.Fields(1).Value="D" then
          ValdelCt = ValdelCt+1
        Else
          ValupdCt = ValupdCt+1
        End if  
      Else
        NoUpdate=NoUpdate+1
      End If
    End if ' * * * Lookup VAL

  ' Update Vlx regardless
    If rs2.State = 1 Then rs2.Close
  ' Lookup VLX with ValID
    rs2.Open "select * from eco.vlx where ValID  = " &ValID, dbconn
  ' If not exists new record
    If rs2.EOF Then
      If rs1.Fields(1).Value<>"D" Then
        dbconn.Execute "insert into eco.vlx(RecCD, RecTM, UsrID, ValID, CreatedBy, CodedBy, CheckedBy, SubmitBy, ActionBy, SubmitNotes, ActionComment, Notes, Value_ID)" &_
                       " values('I','" &RecTM &"'," &UsrID &"," &ValID &"," &rs1.Fields(23).Value &"," &rs1.Fields(24).Value &"," &_
                                    rs1.Fields(25).Value &"," &rs1.Fields(18).Value &"," &Actby &",'" &Replace(rs1.Fields(19).Value&" ","'","`") &_
                                    "','" &Replace(rs1.Fields(21).Value &" ","'","`") &"','" &Replace(rs1.Fields(14).Value&" ","'","`") &"'," &rs1.Fields(0).Value &")"
        VlxinsCt = VlxinsCt+1
      End If  
    Else
      dbconn.Execute "update eco.vlx set RecCD='" &rs1.Fields(1).value &"'" &_
                   ", RecTM='" &RecTM &"'" &_
                   ", UsrID=" &UsrID &_
                   ", CreatedBy=" &rs1.Fields(23).Value &_
                   ", CodedBy=" &rs1.Fields(24).Value &_
                   ", CheckedBy=" &rs1.Fields(25).Value &_
                   ", SubmitBy=" &rs1.Fields(18).Value &_
                   ", ActionBy=" &Actby &_
                   ", SubmitNotes='" &Replace(rs1.Fields(19).Value &" ","'","`") &"'" &_
                   ", ActionComment='" &Replace(rs1.Fields(21).Value &" ","'","`") &"'" &_
                   ", Notes='" &Replace(rs1.Fields(14).Value &" ","'","`") &"'" &_
                   ", Value_ID=" &rs1.Fields(0).Value &_
                   " where ValID=" &ValID
      VlxupdCt = VlxupdCt+1
    End If
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close

  APPENDLOG "*************************************************" &vbCrLf
  APPENDLOG "Run at  : " &FORMAT_TM(Now()) &vbCrLf
  APPENDLOG "Total   : " &TotCt &vbCrLf
  APPENDLOG "ValinsCt: " &ValinsCt &vbCrLf
  APPENDLOG "ValupdCt: " &ValupdCt &vbCrLf
  APPENDLOG "ValdelCt: " &ValdelCt &vbCrLf
  APPENDLOG "ValinDel: " &Valindel &vbCrLf
  APPENDLOG "ValerrCt: " &ValerrCt &vbCrLf
  APPENDLOG "NoUpdate: " &NoUpdate &vbCrLf
  APPENDLOG "OrfinsCt: " &OrfinsCt &vbCrLf
  APPENDLOG "OrfupdCt: " &OrfupdCt &vbCrLf
  APPENDLOG "GeoerrCt: " &GeoerrCt &vbCrLf
  APPENDLOG "TinerrCt: " &TinerrCt &vbCrLf
  APPENDLOG "SetinsCt: " &SetinsCt &vbCrLf
  APPENDLOG "SeterrCt: " &SeterrCt &vbCrLf
  APPENDLOG "VtyinsCt: " &VtyinsCt &vbCrLf
  APPENDLOG "VtyerrCt: " &VtyerrCt &vbCrLf
  APPENDLOG "VlxinsCt: " &VlxinsCt &vbCrLf
  APPENDLOG "VlxupdCt: " &VlxupdCt &vbCrLf
  APPENDLOG "***************** End of Report *****************" &vbCrLf
' WScript.Echo (Hour(now) &":" &Minute(now) &":" &Second(now) &" Records: " & CStr(ValID))
' Send back last value_id to calling loop
'  LOAD_VAL=ID1
End Sub


Sub LOAD_TIN1
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.topics where actflag <> 'X'", dbconn
  rs1.MoveFirst
  Do
  ' Get RecCD
    If Not IsNull(rs1.Fields(0).Value) Then
      RecCD=rs1.Fields(0).Value
    Else 
      RecCD="I"
    End If
  ' Get RecTM
    If Not IsNull(rs1.Fields(3).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(3).Value) 
    ElseIf Not IsNull(rs1.Fields(1).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(1).Value) 
    Else
      RecTM=FixedRecTM
    End If
  ' Get UsrID
    If Not IsNull(rs1.Fields(4).Value) Then
      UsrID=rs1.Fields(4).Value
    ElseIf Not IsNull(rs1.Fields(2).Value) Then
      UsrID=rs1.Fields(2).Value
    Else
      UsrID="Null"
    End If
  ' tidy LabName
    Name = Replace(rs1.Fields(7).value,"'","`")
  ' Lookup LabName
    If rs2.State = 1 Then rs2.Close
    rs2.Open "select LabID from eco.lab where LtyCD='T' and LabName  = ('" &Name &"')", dbconn
    If rs2.EOF Then
      If rs3.State = 1 Then rs3.Close
      rs3.Open "select max(LabID) from eco.lab", dbconn
      If IsNull(rs3.Fields(0).Value) Then
        NextID=1
      Else
        'msgbox rs3.Fields(0).Value 
        NextID=CStr(CLng(rs3.Fields(0).Value)+1)
      End If  
      dbconn.Execute "insert into eco.lab(RecCD, RecTM, UsrID, LabID, LtyCD, LabName, Sort)" &_
                     " values('" &RecCD &"','" &RecTM &"'," &UsrID &"," &NextID &",'T','" &Name & "',"&rs1.Fields(11).Value &")"
    ' Store LabID for TIN insert
      ID1 = NextID
    Else
      ID1 = rs2.Fields(0).Value  
    End If
    'ClientNote = rs1.Fields(9).value &" "
    'ClientNote = Replace(ClientNote,"'","`")
    StaffNote = rs1.Fields(8).value &" "
    StaffNote = Replace(StaffNote,"'","`")
  ' Get OldID  
    OldID=rs1.Fields(5).Value
  ' Get pID
    pID="Null"
    dbconn.Execute "insert into eco.tin(RecCD, RecTM, UsrID, LtyCD, pTinID, TinID, LabID, Active1, Sort, StaffNote, OldTinID)" &_
                     " values('" &RecCD &"','" &RecTM &"'," &UsrID &",'T'," &pID &"," &rs1.Fields(5).Value &"," &ID1 &"," &_
                     rs1.Fields(10).Value &"," &rs1.Fields(11).Value &",'" &StaffNote &"'," &OldID &")"
    MyArray=Split(rs1.Fields(6).value,".")
    a = "x" &OldID &"x"
    b = "x" &MyArray(UBound(MyArray)) &"x"
  ' Test record is good
    If a<>b Then
    ' Log bad record
      'WScript.Echo "Mismatched Topic_ID and Topic_CD=" &a &" " &b
    End if
                     
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
END SUB

Sub LOAD_TIN2
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.topics where actflag <> 'X'", dbconn
  rs1.MoveFirst
  Do
  ' Split topic_cd into array 
    MyArray=Split(rs1.Fields(6).value,".")
  ' Get pID
    If UBound(MyArray)=0 Then
      ' Insert TIN node record already done but update rTinID
      dbconn.Execute "update eco.tin set rTinID=" &MyArray(0) &" where TinID=" &MyArray(0)
    Else
      dbconn.Execute "update eco.tin set pTinID=" &MyArray(UBound(MyArray)-1) &", rTinID=" &MyArray(0) &" where TinID=" &MyArray(UBound(MyArray))
    End If  
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
End SUB

Sub LOAD_TIN3
Dim active3, TinID
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.list_series where actflag <> 'X'" &_
           " and afedivan.list_series.lbl_qck_id <> ''", dbconn
  rs1.MoveFirst
  Do
  ' Split lbl_qck_id in to array
    MyArray=Split(rs1.Fields(7).value,".")
  ' Get OldID  
    OldID=rs1.Fields(6).Value
  ' Get pID
    pID="Null"
    a = "x" &OldID &"x"
    b = "x" &MyArray(UBound(MyArray)) &"x"
  ' Test record is good
    If a=b Then
     'MsgBox "good record"
    ' Get RecCD
      If Not IsNull(rs1.Fields(0).Value) Then
        RecCD=rs1.Fields(0).Value
      Else 
        RecCD="I"
      End If
    ' Get RecTM
      If Not IsNull(rs1.Fields(3).Value) Then
        RecTM=FORMAT_TM(rs1.Fields(3).Value) 
      ElseIf Not IsNull(rs1.Fields(1).Value) Then
        RecTM=FORMAT_TM(rs1.Fields(1).Value) 
      End If
    ' Get UsrID
      If Not IsNull(rs1.Fields(4).Value) Then
        UsrID=rs1.Fields(4).Value
      ElseIf Not IsNull(rs1.Fields(2).Value) Then
        UsrID=rs1.Fields(2).Value
      Else
        UsrID="Null"
      End If
    ' tidy LabName
      Name = Replace(rs1.Fields(8).value,"'","`")
    ' Lookup LabName
      If rs2.State = 1 Then rs2.Close
      rs2.Open "select LabID from eco.lab where LtyCD='I' and LabName  = ('" &Name &"')", dbconn
      If rs2.EOF Then
        If rs3.State = 1 Then rs3.Close
        rs3.Open "select max(LabID) from eco.lab", dbconn
        If IsNull(rs3.Fields(0).Value) Then
          NextID=1
        Else
          NextID=CStr(CLng(rs3.Fields(0).Value)+1)
        End If  
        dbconn.Execute "insert into eco.lab(RecCD, RecTM, UsrID, LabID, LtyCD, LabName, Sort)" &_
                       " values('" &RecCD &"','" &RecTM &"'," &UsrID &"," &NextID &",'I','" &Name & "',"&rs1.Fields(15).Value &")"
      ' Store LabID for TIN insert
        ID1 = NextID
      Else
        ID1 = rs2.Fields(0).Value  
      End If
      StaffNote = rs1.Fields(8).value &" "
      StaffNote = Replace(StaffNote,"'","`")
      If IsNull(rs1.Fields(19).Value) Then 
        active3="0" 
      else      
        active3=rs1.Fields(19).Value 
      End If
      
      TinID=CStr(CLng(OldID)+10000)

      dbconn.Execute "insert into eco.tin(RecCD, RecTM, UsrID, LtyCD, pTinID, TinID, LabID, Active1, Active2, Active3, Sort, StaffNote, OldTinID)" &_
                     " values('" &RecCD &"','" &RecTM &"'," &UsrID &",'I'," &pID &"," &TinID &"," &ID1 &"," &rs1.Fields(12).Value &_
                     "," &rs1.Fields(17).Value &"," &active3 &"," &rs1.Fields(15).Value &",'" &StaffNote &"'," &OldID &")"

    Else
    ' Log bad record
'      WScript.Echo "Mismatched Lbl_ID and Lbl_CD=" &a &" " &b
    End If
  rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
END SUB

Sub LOAD_TIN4
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.list_series where actflag <> 'X'" &_
           " and afedivan.list_series.lbl_qck_id <> ''", dbconn
  rs1.MoveFirst
  Do
  ' Assign the OldID of the current ListSeries records
    OldID=rs1.Fields(6).value
  ' Split lbl_qck_id in to array
    MyArray=Split(rs1.Fields(7).value,".")
  ' If an I node
    If UBound(MyArray)=0 Then
    ' Split topic_cd into array 
      MyArray=Split(rs1.Fields(5).value,".")
    ' Get pID by locating next record up the I chain
      pID=MyArray(UBound(MyArray))
      dbconn.Execute "update eco.tin set pTinID=" &MyArray(UBound(MyArray))&", rTinID=" &MyArray(0) &" where LtyCD='I' and OldTinID=" &OldID
    Else
      pID=CStr(CLng(MyArray(UBound(MyArray)-1)+10000))
    ' Split topic_cd into array 
      MyArray=Split(rs1.Fields(5).value,".")
      dbconn.Execute "update eco.tin set pTinID=" &pID &", rTinID=" &MyArray(0) &" where LtyCD='I' and OldTinID=" &OldID
    End If
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
End SUB

Sub RESET_TIN
dbconn.Execute "DROP TABLE eco.tin"
dbconn.Execute  "CREATE TABLE eco.`tin` (" &_
"  `RecCD` char(1) COLLATE utf8_unicode_ci DEFAULT ''," &_
"  `RecTM` timestamp NULL DEFAULT NULL," &_
"  `UsrID` smallint(6) DEFAULT NULL," &_
"  `LtyCD` char(1) COLLATE utf8_unicode_ci DEFAULT ''," &_
"  `rTinID` int(11) DEFAULT NULL," &_
"  `pTinID` int(11) DEFAULT NULL," &_
"  `TinID` int(11) NOT NULL," &_
"  `LabID` int(11) NOT NULL," &_
"  `Active1` smallint(6) DEFAULT 0," &_
"  `Active2` smallint(6) DEFAULT 0," &_
"  `Active3` smallint(6) DEFAULT 0," &_
"  `Sort` smallint(6) DEFAULT '999'," &_
"  `StaffNote` varchar(4000) COLLATE utf8_unicode_ci NOT NULL," &_
"  `OldTinID` int(11) NOT NULL," &_
"  PRIMARY KEY (`TinID`)," &_
"  KEY `IX_pTinID` (`pTinID`)," &_
"  KEY `IX_rTinID` (`rTinID`)," &_
"  KEY `IX_RecTM` (`RecTM`)," &_
"  KEY `IX_OldID` (`OldTinID`)" &_
") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci"
End Sub


Function FORMAT_TM(p_TM)
Dim sMonth, sDay, sHour, sMinute, sSecond
' Format datatime to YYYY-MM-DD 00:00:00
  If IsNull(p_TM) Then
  Else
  ' Build custom output file name, ensuring that both sMonth and sDay are always two digits
    If Len(Month(p_TM))=1 Then sMonth="0" & month(p_TM) Else sMonth=month(p_TM) End if
    If Len(day(p_TM))=1 Then sDay="0" & Day(p_TM) Else sDay=Day(p_TM) End if
    If Len(Hour(p_TM))=1 Then sHour="0" & Hour(p_TM) Else sHour=Hour(p_TM) End if
    If Len(Minute(p_TM))=1 Then sMinute="0" & Minute(p_TM) Else sMinute=Minute(p_TM) End if
    If Len(Second(p_TM))=1 Then sSecond="0" & Second(p_TM) Else sSecond=Second(p_TM) End if
    FORMAT_TM=Year(p_TM)& "-" & sMonth & "-" & sDay & " " & sHour & ":" & sMinute & ":" & sSecond
    'Mid(MyArray2(2),1,4) & "-" & Mid(MyArray2(1),1,2) & "-" & Mid(MyArray2(0),1,2) & " " & Mid(myarray2(2),6,8)
  End If  
END Function

Sub APPENDLOG(byval messagetxt)
  oFSO_File.Write MessageTxt & vbCrLf
END SUB

PRIVATE SUB OPENDBCON(p_conf, p_svr)
  Dim fso, f1, ts, s, uname, pword, prov, pcata, dsource, MyArray
  Const ForReading = 1
  prov = "MySQL ODBC 5.1 Driver"
' Get connection details
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set ts = fso.OpenTextFile(p_conf, ForReading)
  Do 
    s = ts.ReadLine
    MyArray = Split(s, vbTab)
    If MyArray(0) = p_svr Then
      uname = MyArray(2)
      pword = MyArray(3)
      dsource = MyArray(4) 			
      Exit do
    End if		
  Loop while NOT ts.AtEndOfStream
  ts.Close
' connect to database
  On Error Resume Next
  Set dbconn = CreateObject("ADODB.Connection")
  dbconn.Open "Driver={"&prov &"}" &";Server=" &dsource &";" &_
  " Database=eco;" &_
  " User="&uname &";Password=" &pword &";"
  If Err.Number <> 0 Then
    AppendLog Err.Number & " " & Err.Description
  ' MsgBox "no connection established"
  Else
  ' msgbox "connection established"
  End If
  On Error Goto 0
END SUB

