
Sub xLOAD_TIN1
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.topics where actflag <> 'X'", dbconn
  rs1.MoveFirst
  Do
  ' Get RecTM
      If Not IsNull(rs1.Fields(3).Value) Then
        RecTM=FORMAT_TM(rs1.Fields(3).Value) 
      ElseIf Not IsNull(rs1.Fields(1).Value) Then
        RecTM=FORMAT_TM(rs1.Fields(1).Value) 
      Else
        RecTM=FixedRecTM
      End If
  ' Get RecCD
    If Not IsNull(rs1.Fields(0).Value) Then
      RecCD=rs1.Fields(0).Value
    Else 
      RecCD="I"
    End If
  ' Get UsrID
    If Not IsNull(rs1.Fields(4).Value) Then
      UsrID=rs1.Fields(4).Value
    ElseIf Not IsNull(rs1.Fields(2).Value) Then
      UsrID=rs1.Fields(2).Value
    Else
      UsrID="Null"
    End If
  ' tidy LabName
    Name = Replace(rs1.Fields(7).value,"'","`")
  ' Lookup LabName
    If rs2.State = 1 Then rs2.Close
    rs2.Open "select LabID from eco.lab where LtyCD='T' and LabName  = ('" &Name &"')", dbconn
    If rs2.EOF Then
    ' Insert new LAB and get LabID 
      If rs3.State = 1 Then rs3.Close
      rs3.Open "select max(LabID) from eco.lab", dbconn
      NextID=CStr(CLng(rs3.Fields(0).Value)+1)
      dbconn.Execute "insert into eco.lab(RecCD, RecTM, UsrID, LabID, LtyCD, LabName, Sort)" &_
                     " values('" &RecCD &"','" &RecTM &"'," &UsrID &"," &NextID &",'T','" &Name & "',"&rs1.Fields(11).Value &")"
    ' Store LabID for TIN insert
      ID1 = NextID
    Else
      ID1 = rs2.Fields(0).Value  
    End If
    'ClientNote = rs1.Fields(9).value &" "
    'ClientNote = Replace(ClientNote,"'","`")
    StaffNote = rs1.Fields(8).value &" "
    StaffNote = Replace(StaffNote,"'","`")
  ' Get OldID  
    OldID=rs1.Fields(5).Value
  ' Get pID
    pID="Null"
'   MsgBox myquery
    dbconn.Execute "insert into eco.tin(RecCD, RecTM, UsrID, LtyCD, pTinID, TinID, LabID, Active1, Sort, StaffNote, OldTinID)" &_
                     " values('" &RecCD &"','" &RecTM &"'," &UsrID &",'T'," &pID &"," &rs1.Fields(5).Value &"," &ID1 &"," &rs1.Fields(10).Value &_
                     "," &rs1.Fields(11).Value &"," &rs1.Fields(11).Value &",'" &StaffNote &"'," &OldID &")"

    MyArray=Split(rs1.Fields(6).value,".")
    a = "x" &OldID &"x"
    b = "x" &MyArray(UBound(MyArray)) &"x"
  ' Test record is good
    If a<>b Then
    ' Log bad record
      WScript.Echo "Mismatched Topic_ID and Topic_CD=" &a &" " &b
    End if
                     
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
END SUB

Sub xLOAD_TIN2
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.topics where actflag <> 'X'", dbconn
  rs1.MoveFirst
  Do
  ' Split topic_cd into array 
    MyArray=Split(rs1.Fields(6).value,".")
  ' Get pID
    If UBound(MyArray)=0 Then
      ' Insert TIN node record already done but update rTinID
      dbconn.Execute "update eco.tin set rTinID=" &MyArray(0) &" where TinID=" &MyArray(0)
    Else
'      OldID=rs1.Fields(5).Value
'      pOldID=MyArray(UBound(MyArray)-1)
      'If rs2.State = 1 Then rs2.Close
      'rs2.Open "select TinID from eco.tin where LtyCD='T' and OldTinID = "&pOldID, dbconn
      'pID=rs2.Fields(0).Value
      dbconn.Execute "update eco.tin set pTinID=" &MyArray(UBound(MyArray)-1) &", rTinID=" &MyArray(0) &" where TinID=" &MyArray(UBound(MyArray))
    End If  
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
End SUB

Sub xLOAD_TIN3
Dim active3, TinID
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.list_series where actflag <> 'X'" &_
           " and afedivan.list_series.lbl_qck_id <> ''", dbconn
  rs1.MoveFirst
  Do
  ' Split lbl_qck_id in to array
    MyArray=Split(rs1.Fields(7).value,".")
  ' Get OldID  
    OldID=rs1.Fields(6).Value
  ' Get pID
    pID="Null"
    a = "x" &OldID &"x"
    b = "x" &MyArray(UBound(MyArray)) &"x"
  ' Test record is good
    If a=b Then
     'MsgBox "good record"
    ' Get RecCD
      If Not IsNull(rs1.Fields(0).Value) Then
        RecCD=rs1.Fields(0).Value
      Else 
        RecCD="I"
      End If
    ' Get RecTM
      If Not IsNull(rs1.Fields(3).Value) Then
        RecTM=FORMAT_TM(rs1.Fields(3).Value) 
      ElseIf Not IsNull(rs1.Fields(1).Value) Then
        RecTM=FORMAT_TM(rs1.Fields(1).Value) 
      Else
        RecTM=FixedRecTM
      End If
    ' Get UsrID
      If Not IsNull(rs1.Fields(4).Value) Then
        UsrID=rs1.Fields(4).Value
      ElseIf Not IsNull(rs1.Fields(2).Value) Then
        UsrID=rs1.Fields(2).Value
      Else
        UsrID="Null"
      End If
    ' tidy LabName
      Name = Replace(rs1.Fields(8).value,"'","`")
    ' Lookup LabName
      If rs2.State = 1 Then rs2.Close
      rs2.Open "select LabID from eco.lab where LtyCD='I' and LabName  = ('" &Name &"')", dbconn
      If rs2.EOF Then
      ' Insert new LAB and get LabID 
        If rs3.State = 1 Then rs3.Close
        rs3.Open "select max(LabID) from eco.lab", dbconn
        NextID=CStr(CLng(rs3.Fields(0).Value)+1)
        dbconn.Execute "insert into eco.lab(RecCD, RecTM, UsrID, LabID, LtyCD, LabName, Sort)" &_
                       " values('" &RecCD &"','" &RecTM &"'," &UsrID &"," &NextID &",'I','" &Name &"'," &rs1.Fields(11).Value &")"
      ' Save LabID  
        ID1 = NextID
      Else
        ID1 = rs2.Fields(0).Value)  
      End If
      'ClientNote = rs1.Fields(9).value &" "
      'ClientNote = Replace(ClientNote,"'","`")
      StaffNote = rs1.Fields(8).value &" "
      StaffNote = Replace(StaffNote,"'","`")
      If IsNull(rs1.Fields(19).Value) Then 
        active3="0" 
      Else      
        active3=rs1.Fields(19).Value 
      End If
      
      TinID=CStr(CLng(OldID)+10000)

      dbconn.Execute "insert into eco.tin(RecCD, RecTM, UsrID, LtyCD, pTinID, TinID, LabID, Active1, Active2, Active3, Sort, StaffNote, OldTinID)" &_
                     " values('" &RecCD &"','" &RecTM &"'," &UsrID &",'I'," &pID &"," &TinID &"," &ID1 &"," &rs1.Fields(12).Value &_
                     "," &rs1.Fields(17).Value &"," &active3 &"," &rs1.Fields(15).Value &",'" &StaffNote &"'," &OldID &")"
    Else
    ' Log bad record
      WScript.Echo "Mismatched Lbl_ID and Lbl_CD=" &a &" " &b
    End If
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
END SUB

Sub xLOAD_TIN4
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.list_series where actflag <> 'X'" &_
           " and afedivan.list_series.lbl_qck_id <> ''", dbconn
  rs1.MoveFirst
  Do
  ' Assign the OldID of the current ListSeries records
    OldID=rs1.Fields(6).value
  ' Split lbl_qck_id in to array
    MyArray=Split(rs1.Fields(7).value,".")
  ' If an I node
    If UBound(MyArray)=0 Then
    ' Split topic_cd into array 
      MyArray=Split(rs1.Fields(5).value,".")
    ' Get pID by locating next record up the I chain
      pID=MyArray(UBound(MyArray))
      dbconn.Execute "update eco.tin set pTinID=" &MyArray(UBound(MyArray))&", rTinID=" &MyArray(0) &" where LtyCD='I' and OldTinID=" &OldID
    Else
      pID=CStr(CLng(MyArray(UBound(MyArray)-1)+10000))
    ' Split topic_cd into array 
      MyArray=Split(rs1.Fields(5).value,".")
      dbconn.Execute "update eco.tin set pTinID=" &pID &", rTinID=" &MyArray(0) &" where LtyCD='I' and OldTinID=" &OldID
    End If
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
End SUB
