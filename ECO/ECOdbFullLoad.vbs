'**************************************************************************
'
'                             ECO Tables Loader
'
'**************************************************************************

Option Explicit
'****Variables****
Dim loadtbl, CONF, dbconn, rs1, rs2, rs3, rs4, rs5, oFSO, oFSO_File, WshShell, PreserveDate
Dim MyArray, ValID, ID1, ID2, pID, OldID, pOldID, TagID, Name, RecCD, RecTM, UsrID, MyArray2, Logfile, myquery, ClientNote, StaffNote, a, b, cd, type_cd, StartID, MaxID, CatID
'****Constants****
Const ForWriting = 2
'****Load Argument Data
LoadTbl = WScript.Arguments.Item(0)
'****Hard Coded Data****
CONF="o:\Auto\configs\dbservers.cfg"
logfile="o:\prodman\eco\log\loadlog1.txt"
PreserveDate="Yes"

'****Initialise Log****
Set oFSO=CreateObject("Scripting.FileSystemObject")
Set oFSO_File=oFSO.OpenTextFile(Logfile, ForWriting, True)
'****DataBase Connections****
Call OPENDBCON(conf)

Set WshShell = WScript.CreateObject("WScript.Shell")

'****Set Recordsets****
Set rs1=CreateObject("ADODB.Recordset")
Set rs2=CreateObject("ADODB.Recordset")
Set rs3=CreateObject("ADODB.Recordset")
Set rs4=CreateObject("ADODB.Recordset")
Set rs5=CreateObject("ADODB.Recordset")

' Set fixed time
' RecTM=FORMAT_TM(Now())

RecTM="2017-02-06 22:00:00"

'****Main****
If loadtbl="CAL" Then 
  Call LOAD_CAL
ElseIf loadtbl="CAT" Then
  Call LOAD_CAT
ElseIf loadtbl="CTS" Then
   Call LOAD_CTS_MTX_LOCALITY
ElseIf loadtbl="CTY" Then
  Call LOAD_CTY_CCY
ElseIf loadtbl="FRQ" Then
  Call LOAD_FRQ
ElseIf loadtbl="LGEO" Then
' Loads GEO,LOC
  WshShell.Run "O:\Prodman\ECO\bat\OPS\GEO_RESET.bat", 1, True
  Call LOAD_GEO_LOCALITY
ElseIf loadtbl="DGEO" Then
' Loads GEO,LOC
  Call LOAD_GEO_DISTRICT1
  Call LOAD_GEO_DISTRICT2
ElseIf loadtbl="LGEO_UP" Then
' Updates GEO,LOC
  Call UP_GEO_LOCALITY
ElseIf loadtbl="DGEO_UP" Then
' Updates GEO,LOC
  Call UP_GEO_DISTRICT1
  Call UP_GEO_DISTRICT2
ElseIf loadtbl="GRP" Then
  Call LOAD_GRP
ElseIf loadtbl="SCA" Then
  Call LOAD_SCA
ElseIf loadtbl="SRC" Then 
  Call LOAD_SRC
ElseIf loadtbl="SUB" Then
  Call LOAD_SUB
ElseIf loadtbl="SIN" Then
'  Call LOAD_SIN1
'  Call LOAD_SIN2
'  Call LOAD_SIN3
'  Call LOAD_SIN4
ElseIf loadtbl="TIN" Then
' Loads TIN, LAB
  WshShell.Run "O:\Prodman\ECO\bat\OPS\TIN_RESET.bat", 1, True
  Call LOAD_TIN1
  Call LOAD_TIN2
  Call LOAD_TIN3
  Call LOAD_TIN4
ElseIf loadtbl="UNT" Then
  Call LOAD_UNT
ElseIf loadtbl="VALUPDATE" Then
' Loads VAL,ORF,STG,VTY,VLX
'  Call UPDATE_VAL
ElseIf loadtbl="VAL" Then
' Loads VAL,ORF,STG,VTY,VLX
  'If rs1.State = 1 Then rs1.Close
  'rs1.Open "select max(value_id) from eco_data_prodman.lsv01", dbconn
  'MaxID = 242299036
  MaxID = 252007201
  StartID = 0
  ValID=0
  'ValID=6003528
  MsgBox "select * from eco_data_prodman.lsv" &cstr(ValID\1000000+1)
  Do
    'StartID = LOAD_VAL()
  Loop While MaxID>StartID
ElseIf loadtbl="VCL" Then
  Call LOAD_VCL
End if

oFSO_File.Close
WScript.Quit   

'*******************
'****SUBROUTINES****
'*******************

Sub LOAD_CAT
  RecCD="I"
  UsrID="Null"
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.source_category", dbconn
  rs1.MoveFirst
  Do
  ' Insert new CAT
    dbconn.Execute "insert into afed_data_prodman.cat(RecCD, RecTM, UsrID, CatID, CatName, Sort)" &_
                   " values('" &RecCD &"','" &RecTM &"'," &UsrID &", " &rs1.Fields(0).Value &", '" &rs1.Fields(1).Value &"'," &_
                   " 1)"  
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close                 
End Sub

Sub LOAD_FRQ
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.freq where actflag <> 'X'", dbconn
  rs1.MoveFirst
  Do
    'Get RecCD
    If Not IsNull(rs1.Fields(0).value) Then
      RecCD=rs1.Fields(0).Value
    Else
      RecCD="I"
    End If
    'Get RecTM
    If Not IsNull(rs1.Fields(3).Value) Then
      
      RecTM=FORMAT_TM(rs1.Fields(3).Value) 
    
    ElseIf Not IsNull(rs1.Fields(1).Value) Then
      
      RecTM=FORMAT_TM(rs1.Fields(1).Value) 
    
    End If
  
    'Get UsrID
    
    If Not IsNull(rs1.Fields(4).Value) Then
      
      UsrID=rs1.Fields(4).Value
    
    ElseIf Not IsNull(rs1.Fields(2).Value) Then
      
      UsrID=rs1.Fields(2).Value
    
    Else
      
      UsrID="Null"
    
    End If

    ClientNote = rs1.Fields(8).value &" "
    
    ClientNote = Replace(ClientNote,"'","`")
    
    StaffNote = rs1.Fields(9).value &" "
    
    StaffNote = Replace(StaffNote,"'","`")
  
    ' Insert new FRQ
    dbconn.Execute "insert into afed_data_prodman.frq(RecCD, RecTM, UsrID, FrqID, FrqName, Sort, ClientNote, StaffNote)"&_
                  " values('" &RecCD &"','" &RecTM &"'," &UsrID &"," &rs1.Fields(5).Value &",'" &rs1.Fields(6).Value &"',"&_
                  rs1.Fields(10).Value &",'" &ClientNote &"','" &StaffNote &"')"
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close                   
End Sub

Sub LOAD_GRP
  RecCD="I"
  UsrID="Null"
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.source_group", dbconn
  rs1.MoveFirst
  Do
  ' Insert new GRP
    dbconn.Execute "insert into afed_data_prodman.grp(RecCD, RecTM, UsrID, GrpCD, GrpName, Sort)"&_
                  " values('" &RecCD &"','" &RecTM &"'," &UsrID &",'" &rs1.Fields(0).Value &"','" &rs1.Fields(1).Value &"',"&_
                  rs1.Fields(3).Value &")"
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close              
End Sub

Sub LOAD_SCA
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.scales where actflag <> 'X'", dbconn
  rs1.MoveFirst
  Do
    'Get RecCD
    If Not IsNull(rs1.Fields(0).value) Then
      RecCD=rs1.Fields(0).Value
    Else
      RecCD="I"
    End If
    'Get RecTM
    If Not IsNull(rs1.Fields(3).Value) Then
      
      RecTM=FORMAT_TM(rs1.Fields(3).Value) 
    
    ElseIf Not IsNull(rs1.Fields(1).Value) Then
      
      RecTM=FORMAT_TM(rs1.Fields(1).Value) 
    
    End If
  
    'Get UsrID
    
    If Not IsNull(rs1.Fields(4).Value) Then
      
      UsrID=rs1.Fields(4).Value
    
    ElseIf Not IsNull(rs1.Fields(2).Value) Then
      
      UsrID=rs1.Fields(2).Value
    
    Else
      
      UsrID="Null"
    
    End If

    ClientNote = rs1.Fields(8).value &" "
    
    ClientNote = Replace(ClientNote,"'","`")
    
    StaffNote = rs1.Fields(9).value &" "
    
    StaffNote = Replace(StaffNote,"'","`")
  
    ' Insert new SCA
    dbconn.Execute "insert into afed_data_prodman.sca(RecCD, RecTM, UsrID, ScaID, ScaName, Sort, ClientNote, StaffNote)"&_
                  " values('" &RecCD &"','" &RecTM &"'," &UsrID &", "&rs1.Fields(5).Value &",'" &rs1.Fields(6).Value &"'," &_
                  rs1.Fields(10).Value &",'" &ClientNote &"','" &StaffNote &"')"
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
End Sub

Sub LOAD_SRC
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.source where actflag <> 'X'", dbconn
  rs1.MoveFirst
  Do
    'Get RecCD
    If Not IsNull(rs1.Fields(0).value) Then
      RecCD=rs1.Fields(0).Value
    Else
      RecCD="I"
    End If
    'Get RecTM
    If Not IsNull(rs1.Fields(3).Value) Then
      
      RecTM=FORMAT_TM(rs1.Fields(3).Value) 
    
    ElseIf Not IsNull(rs1.Fields(1).Value) Then
      
      RecTM=FORMAT_TM(rs1.Fields(1).Value) 
    
    End If
  
    'Get UsrID
    
    If Not IsNull(rs1.Fields(4).Value) Then
      
      UsrID=rs1.Fields(4).Value
    
    ElseIf Not IsNull(rs1.Fields(2).Value) Then
      
      UsrID=rs1.Fields(2).Value
    
    Else
      
      UsrID="Null"
    
    End If

    'When CatID is Null in afedivan.source the insert query below returns ,, instead of ,Null, and query crashes
    'so added the following code to deal with this  
    If Not IsNull(rs1.Fields(10).Value) Then 
      CatID=(rs1.Fields(10).Value)
    Else
      CatID="Null"
    End If
    ' Insert new SRC
    dbconn.Execute "insert into afed_data_prodman.src(RecCD, RecTM, UsrID, SrcID, SrcName, Sort, CatID, GrpCD, SrcCD)"&_
                  " values('" &RecCD &"','" &RecTM &"'," &UsrID &"," &rs1.Fields(5).Value &",'" &Replace(rs1.Fields(7).Value,"'","`") &"',"&_
                  rs1.Fields(13).Value &"," &CatID &",'" &rs1.Fields(9).Value &"','" &rs1.Fields(6).Value &"')"
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
End Sub

Sub LOAD_SUB
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.sub_source where actflag <> 'X'", dbconn
  rs1.MoveFirst
  Do
    'Get RecCD
    If Not IsNull(rs1.Fields(0).value) Then
      RecCD=rs1.Fields(0).Value
    Else
      RecCD="I"
    End If
    'Get RecTM
    If Not IsNull(rs1.Fields(3).Value) Then
      
      RecTM=FORMAT_TM(rs1.Fields(3).Value) 
    
    ElseIf Not IsNull(rs1.Fields(1).Value) Then
      
      RecTM=FORMAT_TM(rs1.Fields(1).Value) 
    
    End If
  
    'Get UsrID
    
    If Not IsNull(rs1.Fields(4).Value) Then
      
      UsrID=rs1.Fields(4).Value
    
    ElseIf Not IsNull(rs1.Fields(2).Value) Then
      
      UsrID=rs1.Fields(2).Value
    
    Else
      
      UsrID="Null"
    
    End If

    ' Insert new SUB
    dbconn.Execute "insert into afed_data_prodman.sub(RecCD, RecTM, UsrID, SubID, SubName, Sort, SrcID, SrcCD)"&_
                  " values('" &RecCD &"','" &RecTM &"'," &UsrID &"," &rs1.Fields(5).Value &",'" &Replace(rs1.Fields(7).Value,"'","`") &"',"&_
                  rs1.Fields(12).Value &"," &rs1.Fields(8).Value &",'" &rs1.Fields(6).Value &"')"
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close              
End Sub

Sub LOAD_UNT
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.units where actflag <> 'X'", dbconn
  rs1.MoveFirst
  Do
    'Get RecCD
    If Not IsNull(rs1.Fields(0).value) Then
      RecCD=rs1.Fields(0).Value
    Else
      RecCD="I"
    End If
    'Get RecTM
    If Not IsNull(rs1.Fields(3).Value) Then
      
      RecTM=FORMAT_TM(rs1.Fields(3).Value) 
    
    ElseIf Not IsNull(rs1.Fields(1).Value) Then
      
      RecTM=FORMAT_TM(rs1.Fields(1).Value) 
    
    End If
  
    'Get UsrID
    
    If Not IsNull(rs1.Fields(4).Value) Then
      
      UsrID=rs1.Fields(4).Value
    
    ElseIf Not IsNull(rs1.Fields(2).Value) Then
      
      UsrID=rs1.Fields(2).Value
    
    Else
      
      UsrID="Null"
    
    End If

    ClientNote = rs1.Fields(9).value &" "
    
    ClientNote = Replace(ClientNote,"'","`")
    
    StaffNote = rs1.Fields(10).value &" "
    
    StaffNote = Replace(StaffNote,"'","`")
  
    ' Insert new UNT
    dbconn.Execute "insert into afed_data_prodman.unt(RecCD, RecTM, UsrID, UntID, UntName, GrpCD, Sort, ClientNote, StaffNote)"&_
                  " values('" &RecCD &"','" &RecTM &"'," &UsrID &"," &rs1.Fields(5).Value &",'" &Replace(rs1.Fields(6).Value,"'","`") &"',"&_
                  rs1.Fields(8).Value &"," &rs1.Fields(11).Value &",'" &ClientNote &"','" &StaffNote &"')"
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close                
End Sub

Sub LOAD_TIN1
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.topics where actflag <> 'X'", dbconn
  rs1.MoveFirst
  Do
  ' Get RecCD
    If Not IsNull(rs1.Fields(0).Value) Then
      RecCD=rs1.Fields(0).Value
    Else 
      RecCD="I"
    End If
  ' Get RecTM
    If Not IsNull(rs1.Fields(3).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(3).Value) 
    ElseIf Not IsNull(rs1.Fields(1).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(1).Value) 
    End If
  ' Get UsrID
    If Not IsNull(rs1.Fields(4).Value) Then
      UsrID=rs1.Fields(4).Value
    ElseIf Not IsNull(rs1.Fields(2).Value) Then
      UsrID=rs1.Fields(2).Value
    Else
      UsrID="Null"
    End If
  ' tidy LabName
    Name = Replace(rs1.Fields(7).value,"'","`")
  ' Lookup LabName
    If rs2.State = 1 Then rs2.Close
    rs2.Open "select LabID from eco.lab where LtyCD='T' and LabName  = ('" &Name &"')", dbconn
    If rs2.EOF Then
    ' Insert new LAB and get LabID 
      dbconn.Execute "insert into eco.lab(RecCD, RecTM, UsrID, LtyCD, LabName, Sort)" &_
                     " values('" &RecCD &"','" &RecTM &"'," &UsrID &",'T','" &Name & "',"&rs1.Fields(11).Value &")"
    ' get LabID
      If rs2.State = 1 Then rs2.Close
      rs2.Open "select LabID from eco.lab where LtyCD='T' and LabName = '" &Name &"'", dbconn
    End If
  ' Save LabID  
    ID1 = rs2.Fields(0).Value
    'ClientNote = rs1.Fields(9).value &" "
    'ClientNote = Replace(ClientNote,"'","`")
    StaffNote = rs1.Fields(8).value &" "
    StaffNote = Replace(StaffNote,"'","`")
  ' Get OldID  
    OldID=rs1.Fields(5).Value
  ' Get pID
    pID="Null"
    myquery = "insert into eco.tin(RecCD, RecTM, UsrID, LtyCD, pTinID, TinID, LabID, Active1, Sort, StaffNote, OldTinID)" &_
                     " values('" &RecCD &"','" &RecTM &"'," &UsrID &",'T'," &pID &"," &rs1.Fields(5).Value &"," &rs1.Fields(10).Value &"," &rs1.Fields(11).Value &"," &rs1.Fields(11).Value &",'" &StaffNote &"'," &OldID &")"
'   MsgBox myquery
    dbconn.Execute "insert into eco.tin(RecCD, RecTM, UsrID, LtyCD, pTinID, TinID, LabID, Active1, Sort, StaffNote, OldTinID)" &_
                     " values('" &RecCD &"','" &RecTM &"'," &UsrID &",'T'," &pID &"," &rs1.Fields(5).Value &"," &rs1.Fields(10).Value &"," &rs1.Fields(11).Value &"," &rs1.Fields(11).Value &",'" &StaffNote &"'," &OldID &")"

    MyArray=Split(rs1.Fields(6).value,".")
    a = "x" &OldID &"x"
    b = "x" &MyArray(UBound(MyArray)) &"x"
  ' Test record is good
    If a<>b Then
    ' Log bad record
      WScript.Echo "Mismatched Topic_ID and Topic_CD=" &a &" " &b
    End if
                     
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
END SUB

Sub LOAD_TIN2
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.topics where actflag <> 'X'", dbconn
  rs1.MoveFirst
  Do
  ' Split topic_cd into array 
    MyArray=Split(rs1.Fields(6).value,".")
  ' Get pID
    If UBound(MyArray)=0 Then
      ' Insert TIN node record already done but update rTinID
      dbconn.Execute "update eco.tin set rTinID=" &MyArray(0) &" where TinID=" &MyArray(0)
    Else
'      OldID=rs1.Fields(5).Value
'      pOldID=MyArray(UBound(MyArray)-1)
      'If rs2.State = 1 Then rs2.Close
      'rs2.Open "select TinID from eco.tin where LtyCD='T' and OldTinID = "&pOldID, dbconn
      'pID=rs2.Fields(0).Value
      dbconn.Execute "update eco.tin set pTinID=" &MyArray(UBound(MyArray)-1) &", rTinID=" &MyArray(0) &" where TinID=" &MyArray(UBound(MyArray))
    End If  
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
End SUB

Sub LOAD_TIN3
Dim active3, TinID
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.list_series where actflag <> 'X'" &_
           " and afedivan.list_series.lbl_qck_id <> ''", dbconn
  rs1.MoveFirst
  Do
  ' Split lbl_qck_id in to array
    MyArray=Split(rs1.Fields(7).value,".")
  ' Get OldID  
    OldID=rs1.Fields(6).Value
  ' Get pID
    pID="Null"
    a = "x" &OldID &"x"
    b = "x" &MyArray(UBound(MyArray)) &"x"
  ' Test record is good
    If a=b Then
     'MsgBox "good record"
    ' Get RecCD
      If Not IsNull(rs1.Fields(0).Value) Then
        RecCD=rs1.Fields(0).Value
      Else 
        RecCD="I"
      End If
    ' Get RecTM
      If Not IsNull(rs1.Fields(3).Value) Then
        RecTM=FORMAT_TM(rs1.Fields(3).Value) 
      ElseIf Not IsNull(rs1.Fields(1).Value) Then
        RecTM=FORMAT_TM(rs1.Fields(1).Value) 
      End If
    ' Get UsrID
      If Not IsNull(rs1.Fields(4).Value) Then
        UsrID=rs1.Fields(4).Value
      ElseIf Not IsNull(rs1.Fields(2).Value) Then
        UsrID=rs1.Fields(2).Value
      Else
        UsrID="Null"
      End If
    ' tidy LabName
      Name = Replace(rs1.Fields(8).value,"'","`")
    ' Lookup LabName
      If rs2.State = 1 Then rs2.Close
      myquery = "select LabID from eco.lab where LtyCD='I' and LabName  = ('" &Name &"')"
      rs2.Open "select LabID from eco.lab where LtyCD='I' and LabName  = ('" &Name &"')", dbconn
      If rs2.EOF Then
      ' Insert new LAB and get LabID 
        dbconn.Execute "insert into eco.lab(RecCD, RecTM, UsrID, LtyCD, LabName, Sort)" &_
                       " values('" &RecCD &"','" &RecTM &"'," &UsrID &",'I','" &Name & "',"&rs1.Fields(15).Value &")"
      ' get LabID
        If rs2.State = 1 Then rs2.Close
        rs2.Open "select LabID from eco.lab where LtyCD='I' and LabName = '" &Name &"'", dbconn
      End If
    ' Save LabID  
      ID1 = rs2.Fields(0).Value
      'ClientNote = rs1.Fields(9).value &" "
      'ClientNote = Replace(ClientNote,"'","`")
      StaffNote = rs1.Fields(8).value &" "
      StaffNote = Replace(StaffNote,"'","`")
      If IsNull(rs1.Fields(19).Value) Then 
        active3="0" 
      else      
        active3=rs1.Fields(19).Value 
      End If
      
      TinID=CStr(CLng(OldID)+10000)

      myquery = "insert into eco.tin(RecCD, RecTM, UsrID, LtyCD, pTinID, TinID, LabID, Active1, Active2, Active3, Sort, StaffNote, OldTinID)" &_
                       " values('" &RecCD &"','" &RecTM &"'," &UsrID &",'I'," &pID &"," &TinID &"," &ID1 &"," &rs1.Fields(12).Value &"," &rs1.Fields(17).Value &"," &active3 &"," &rs1.Fields(15).Value &",'" &StaffNote &"'," &OldID &")"
  '    MsgBox myquery
      dbconn.Execute "insert into eco.tin(RecCD, RecTM, UsrID, LtyCD, pTinID, TinID, LabID, Active1, Active2, Active3, Sort, StaffNote, OldTinID)" &_
                       " values('" &RecCD &"','" &RecTM &"'," &UsrID &",'I'," &pID &"," &TinID &"," &ID1 &"," &rs1.Fields(12).Value &"," &rs1.Fields(17).Value &"," &active3 &"," &rs1.Fields(15).Value &",'" &StaffNote &"'," &OldID &")"
    Else
    ' Log bad record
      WScript.Echo "Mismatched Lbl_ID and Lbl_CD=" &a &" " &b
    End If
  rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
END SUB

Sub LOAD_TIN4
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.list_series where actflag <> 'X'" &_
           " and afedivan.list_series.lbl_qck_id <> ''", dbconn
  rs1.MoveFirst
  Do
  ' Assign the OldID of the current ListSeries records
    OldID=rs1.Fields(6).value
  ' Split lbl_qck_id in to array
    MyArray=Split(rs1.Fields(7).value,".")
  ' If an I node
    If UBound(MyArray)=0 Then
    ' Split topic_cd into array 
      MyArray=Split(rs1.Fields(5).value,".")
    ' Get pID by locating next record up the I chain
      pID=MyArray(UBound(MyArray))
      dbconn.Execute "update eco.tin set pTinID=" &MyArray(UBound(MyArray))&", rTinID=" &MyArray(0) &" where LtyCD='I' and OldTinID=" &OldID
    Else
    ' Get pID by locating next record up the I chain
      'pOldID=MyArray(UBound(MyArray)-1)
      pID=CStr(CLng(MyArray(UBound(MyArray)-1)+10000))
    ' Split topic_cd into array 
      MyArray=Split(rs1.Fields(5).value,".")
      dbconn.Execute "update eco.tin set pTinID=" &pID &", rTinID=" &MyArray(0) &" where LtyCD='I' and OldTinID=" &OldID
    End If
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
End SUB

Function LOAD_VAL()
Dim StgID, VtyID, GeoID, TinID, PstCD, PubDT, EffDT, Actby
' WScript.Echo (Hour(now) &":" &Minute(now) &":" &Second(now) &" Records: " & CStr(ValID))
' Load in subsets since very large
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from eco.lsv" &cstr((ValID)\1000000+1) &_
           " order by value_id limit " &ValID Mod 1000000 &",10000", dbconn
  rs1.MoveFirst
  Do
    ID1 = rs1.Fields(0).Value
    ValID = ValID+1
    If Not IsNull(rs1.Fields(2).Value) Then
      UsrID = rs1.Fields(2).Value
    ElseIf IsNull(rs1.Fields(23).Value) Then
      UsrID = rs1.Fields(23).Value
    Else
      UsrID="Null"
    End If
  ' Get RecTM
    If Not IsNull(rs1.Fields(3).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(3).Value) 
    End If
  ' Get EffDT
    If Not IsNull(rs1.Fields(31).Value) Then
      EffDT=RTrim(FORMAT_TM(rs1.Fields(31).Value)) 
    Else 
      EffDT="null"
    End If
  ' Get Actby
    If Not IsNull(rs1.Fields(20).Value) Then
      ActBy=rs1.Fields(20).Value
    Else
      ActBy="Null"
    End If

  ' Lookup ORF
    If rs2.State = 1 Then rs2.Close
    rs2.Open "select OrfID from eco.orf where OrfID  = " &rs1.Fields(29).Value, dbconn
    If rs2.EOF Then
    ' Set PstCD
      If rs1.Fields(22).Value = 1 Then
        PstCD="A"
      Else
        PstCD="I"
      End If
      If Not IsNull(rs1.Fields(30).Value) Then
        PubDT=RTrim(FORMAT_TM(rs1.Fields(30).Value)) 
      Else 
        PubDT="null"
      End If
    ' Insert new ORF
      dbconn.Execute "insert into eco.orf(RecCD, RecTM, UsrID, OrfID, OrfName, SrcID, SubID, PubDT, PstCD)" &_
                     " values('I','" &RecTM &"'," &UsrID &"," &rs1.Fields(29).Value &_
                     ",'" &Replace(rs1.Fields(34).Value &" ","'","`") &"'," &rs1.Fields(4).Value &"," &rs1.Fields(5).Value &",'" &PubDT &"','" &PstCD &"')"
    End If

  ' Lookup VTY
    If rs2.State = 1 Then rs2.Close
    rs2.Open "select VtyID from eco.vty where VclID  = " &rs1.Fields(17).Value &_
                                                      " and CalID  = " &rs1.Fields(32).Value &_
                                                      " and FrqID  = " &rs1.Fields(16).Value, dbconn
    If rs2.EOF Then
    ' Insert new VTY
      dbconn.Execute "insert into eco.vty(RecCD, RecTM, UsrID, VclID, CalID, FrqID)" &_
                     " values('I','" &RecTM &"'," &UsrID &"," &rs1.Fields(17).Value &"," &_
                                  rs1.Fields(32).Value &"," &rs1.Fields(16).Value &")"
    ' get VtyID
      If rs2.State = 1 Then rs2.Close
      rs2.Open "select Max(VtyID) from eco.vty", dbconn
    End If
    VtyID = rs2.Fields(0).Value

  ' Lookup SET
    If rs2.State = 1 Then rs2.Close
    rs2.Open "select StgID from eco.set where UntID = " &rs1.Fields(10).Value &_
                                                      " and ScaID = " &rs1.Fields(9).Value &_
                                                      " and Base = '" &rs1.Fields(15).Value &"'", dbconn
    If rs2.EOF Then
    ' Insert new SET
      dbconn.Execute "insert into eco.set(RecCD, RecTM, UsrID, UntID, ScaID, Base)" &_
                     " values('I','" &RecTM &"'," &UsrID &"," &rs1.Fields(10).Value &"," &_
                                  rs1.Fields(9).Value &",'" &rs1.Fields(15).Value &"')"
    ' get StgID
      If rs2.State = 1 Then rs2.Close
      rs2.Open "select Max(StgID) from eco.set", dbconn
    End If
    StgID = rs2.Fields(0).Value

  ' get GeoID
    If  rs1.Fields(26).Value = "0" Then
      If rs2.State = 1 Then rs2.Close
      If rs1.Fields(8).Value="EP" Or rs1.Fields(8).Value="EF" Then
        type_cd="S"
      ElseIf rs1.Fields(8).Value="ET" Then 
        type_cd="C"
      End If
      rs2.Open "select GeoID from eco.geo where GtyCD  = '" &type_cd &"' and CtsCD = '" &rs1.Fields(8).Value &"'", dbconn
      If rs2.EOF Then
        GeoID=88888
 '       GeoerrCt = GeoerrCt+1
      Else
        GeoID = rs2.Fields(0).Value
      End If
    Else
      If rs2.State = 1 Then rs2.Close
      rs2.Open "select GeoID from eco.geo where CtyCD  = '" &rs1.Fields(8).Value &"' and Dist_ID = " &rs1.Fields(26).Value, dbconn
      If rs2.EOF Then
        GeoID=99999
'        GeoerrCt = GeoerrCt+1
      Else
        GeoID = rs2.Fields(0).Value
      End If
    End If ' * * * Lookup GEO
   
  ' Get TinID
    If rs2.State = 1 Then rs2.Close
    rs2.Open "select TinID from eco.tin where LtyCD='I' and OldTinID = "&rs1.Fields(6).Value, dbconn
    If rs2.EOF Then
      TinID=99999
    Else
      TinID = rs2.Fields(0).Value
    End If

  ' Load VAL record
    If rs2.State = 1 Then rs2.Close
    dbconn.Execute "insert into eco.val(RecCD, RecTM, UsrID, Amount, EffDT, StgID, VtyID, OrfID, IndID, GeoID, Lbl_ID, Value_ID)" &_
                   " values('" &rs1.Fields(1).Value &"','" &RecTM &"'," &UsrID &"," &rs1.Fields(11).Value &",'" &EffDT &"'," &StgID &"," &_
                                VtyID &"," &rs1.Fields(29).Value &"," &TinID &"," &GeoID  &"," &rs1.Fields(6).Value &"," &rs1.Fields(0).Value &")"
  ' Load VLX record
    If rs2.State = 1 Then rs2.Close
    dbconn.Execute "insert into eco.vlx(RecCD, RecTM, UsrID, ValID, CreatedBy, CodedBy, CheckedBy, SubmitBy, ActionBy, SubmitNotes, ActionComment, Notes, Value_ID)" &_
                   " values('" &rs1.Fields(1).Value &"','" &RecTM &"'," &UsrID &"," &ValID &"," &rs1.Fields(23).Value &"," &rs1.Fields(24).Value &"," &_
                                rs1.Fields(25).Value &"," &rs1.Fields(18).Value &"," &Actby &",'" &Replace(rs1.Fields(19).Value&" ","'","`") &_
                                "','" &Replace(rs1.Fields(21).Value&" ","'","`") &"','" &Replace(rs1.Fields(14).Value&" ","'","`") &"'," &rs1.Fields(0).Value &")"

    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close

  WScript.Echo (Hour(now) &":" &Minute(now) &":" &Second(now) &" Records: " & CStr(ValID))
' Send back last value_id to calling loop
  LOAD_VAL_FULL=ID1
End Function

Sub LOAD_CAL 
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.cal_type where actflag <> 'X'", dbconn
  rs1.MoveFirst
  Do
  ' Get RecCD
    If Not IsNull(rs1.Fields(0).Value) Then
      RecCD=rs1.Fields(0).Value
    Else 
      RecCD="I"
    End If
  ' Get RecTM
    If Not IsNull(rs1.Fields(3).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(3).Value) 
    ElseIf Not IsNull(rs1.Fields(1).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(1).Value) 
    End If
  ' Get UsrID
    If Not IsNull(rs1.Fields(4).Value) Then
      UsrID=rs1.Fields(4).Value
    ElseIf Not IsNull(rs1.Fields(2).Value) Then
      UsrID=rs1.Fields(2).Value
    Else
      UsrID="Null"
    End If
    ' Remap bad cntrycd
    ID1=rs1.Fields(5).Value
    If rs2.State = 1 Then rs2.Close
    rs2.Open "select CalID from eco.cal where CalID  = " &ID1, dbconn
    If rs2.EOF Then
    ' Insert new CAL
      dbconn.Execute "insert into eco.cal(RecCD, RecTM, UsrID, CalID, CalName, Sort, CalDesc, Q1Month, Q2Month, Q3Month, Q4Month, StartMonth, PstCD)" &_
                     " values('" &RecCD &"','" &RecTM &"'," &UsrID &"," &ID1 &",'" &rs1.Fields(6).Value &"',"  &rs1.Fields(16).Value &",'" &rs1.Fields(7).Value &"'," &_
                     rs1.Fields(8).Value &"," &rs1.Fields(9).Value &"," &rs1.Fields(10).Value &"," &rs1.Fields(11).Value &"," &rs1.Fields(12).Value &",'P')"
    End If
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
End Sub

Sub LOAD_GEO_LOCALITY
' First load County and Special Country
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.locality where actflag <> 'D' and cd<>'EAZ' and cd<>'EAT' and (type_cd='C' or type_cd='S' or type_cd='AA') order by type_cd, cd", dbconn
  rs1.MoveFirst
  Do
  ' Get RecCD
    If Not IsNull(rs1.Fields(0).Value) Then
      RecCD=rs1.Fields(0).Value
    Else 
      RecCD="I"
    End If
  ' Get RecTM
    If Not IsNull(rs1.Fields(3).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(3).Value) 
    ElseIf Not IsNull(rs1.Fields(1).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(1).Value) 
    End If
  ' Get UsrID
    If Not IsNull(rs1.Fields(4).Value) Then
      UsrID=rs1.Fields(4).Value
    ElseIf Not IsNull(rs1.Fields(2).Value) Then
      UsrID=rs1.Fields(2).Value
    Else
      UsrID="Null"
    End If

    type_cd=rs1.Fields(5).value
    cd=rs1.Fields(6).value
  ' Check CD
    If cd="AA" Then
      type_cd="S"
    End if  
    
  ' tidy LocName
    Name = Replace(rs1.Fields(7).value,"'","`")
  ' Lookup LocName
    If rs2.State = 1 Then rs2.Close
    rs2.Open "select LocID from eco.loc where LocName  = ('" &Name &"')", dbconn
    If rs2.EOF Then
    ' Insert new LOC
      dbconn.Execute "insert into eco.loc(RecCD, RecTM, UsrID, LocName, Sort)" &_
                     " values('" &RecCD &"','" &RecTM &"'," &UsrID &",'" &Name &"',"  &rs1.Fields(12).Value &")"
    ' get LocID
      If rs2.State = 1 Then rs2.Close
      rs2.Open "select LocID from eco.loc where LocName = '" &Name &"'", dbconn
    End If
  ' Only create GEO for a country
    If Len(cd)=2 THEN
    ' Save LocID  
      ID1 = rs2.Fields(0).Value
      ClientNote = rs1.Fields(10).value &" "
      ClientNote = Replace(ClientNote,"'","`")
      StaffNote = rs1.Fields(11).value &" "
      StaffNote = Replace(StaffNote,"'","`")
    ' Get pID
      pID="Null"
      myquery = "insert into eco.geo(RecCD, RecTM, UsrID, pGeoID, LocID, Sort, GtyCD, CtsCD, CtyCD, ClientNote, StaffNote)" &_
                       " values('" &RecCD &"','" &RecTM &"'," &UsrID &"," &pID &"," &ID1 &"," &rs1.Fields(12).Value &",'" &type_cd &"','" &cd &"','" &cd &"','" &ClientNote &"','" &StaffNote &"')"
      dbconn.Execute "insert into eco.geo(RecCD, RecTM, UsrID, pGeoID, LocID, Sort, GtyCD, CtsCD, CtyCD, ClientNote, StaffNote)" &_
                       " values('" &RecCD &"','" &RecTM &"'," &UsrID &"," &pID &"," &ID1 &"," &rs1.Fields(12).Value &",'" &type_cd &"','" &cd &"','" &cd &"','" &ClientNote &"','" &StaffNote &"')"
    End If
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
 ' Now load Aggregates and Regions
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.locality where actflag <> 'D' and cd<>'EAZ' and cd<>'EAT' and (type_cd='A' or type_cd='R') order by type_cd, cd", dbconn
  rs1.MoveFirst
  Do
  ' Get RecCD
    If Not IsNull(rs1.Fields(0).Value) Then
      RecCD=rs1.Fields(0).Value
    Else 
      RecCD="I"
    End If
  ' Get RecTM
    If Not IsNull(rs1.Fields(3).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(3).Value) 
    ElseIf Not IsNull(rs1.Fields(1).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(1).Value) 
    End If
  ' Get UsrID
    If Not IsNull(rs1.Fields(4).Value) Then
      UsrID=rs1.Fields(4).Value
    ElseIf Not IsNull(rs1.Fields(2).Value) Then
      UsrID=rs1.Fields(2).Value
    Else
      UsrID="Null"
    End If

    type_cd=rs1.Fields(5).value
    cd=rs1.Fields(6).value

  ' Check CD
    If cd="AA" Then
      type_cd="S"
    End if  
  ' tidy LocName
    Name = Replace(rs1.Fields(7).value,"'","`")
  ' Lookup LocName
    If rs2.State = 1 Then rs2.Close
    rs2.Open "select LocID from eco.loc where LocName  = ('" &Name &"')", dbconn
    If rs2.EOF Then
    ' Insert new LOC
      dbconn.Execute "insert into eco.loc(RecCD, RecTM, UsrID, LocName, Sort)" &_
                     " values('" &RecCD &"','" &RecTM &"'," &UsrID &",'" &Name &"',"  &rs1.Fields(12).Value &")"
    ' get LocID
      If rs2.State = 1 Then rs2.Close
      rs2.Open "select LocID from eco.loc where LocName = '" &Name &"'", dbconn
    End If
  ' Save LocID  
    ID1 = rs2.Fields(0).Value
    ClientNote = rs1.Fields(10).value &" "
    ClientNote = Replace(ClientNote,"'","`")
    StaffNote = rs1.Fields(11).value &" "
    StaffNote = Replace(StaffNote,"'","`")
  ' Get pID
    pID="Null"
    myquery = "insert into eco.geo(RecCD, RecTM, UsrID, pGeoID, LocID, Sort, GtyCD, CtsCD, CtyCD, ClientNote, StaffNote)" &_
                     " values('" &RecCD &"','" &RecTM &"'," &UsrID &"," &pID &"," &ID1 &"," &rs1.Fields(12).Value &",'" &type_cd &"','" &cd &"','AA','" &ClientNote &"','" &StaffNote &"')"
    dbconn.Execute "insert into eco.geo(RecCD, RecTM, UsrID, pGeoID, LocID, Sort, GtyCD, CtsCD, CtyCD, ClientNote, StaffNote)" &_
                     " values('" &RecCD &"','" &RecTM &"'," &UsrID &"," &pID &"," &ID1 &"," &rs1.Fields(12).Value &",'" &type_cd &"','" &cd &"','AA','" &ClientNote &"','" &StaffNote &"')"
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
  dbconn.Execute "update eco.geo set rGeoID=GeoID where Dist_ID=0"
End Sub

Sub LOAD_CTS_MTX_LOCALITY
' Load Aggregate and Region GEO records
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select afedivan.mtx_locality.* from afedivan.mtx_locality" &_
           " where afedivan.mtx_locality.actflag <> 'D' and afedivan.mtx_locality.cd<>''" &_
           " and afedivan.mtx_locality.cd<>'EAZ' and afedivan.mtx_locality.cd<>'EAT'" &_
           " and (afedivan.mtx_locality.type_cd='A' or afedivan.mtx_locality.type_cd='R')" &_
           " and afedivan.mtx_locality.value<>'MH'" &_
           " order by afedivan.mtx_locality.type_cd, afedivan.mtx_locality.cd", dbconn
  rs1.MoveFirst
  Do
  ' Get RecCD
    If Not IsNull(rs1.Fields(0).Value) Then
      RecCD=rs1.Fields(0).Value
    Else 
      RecCD="I"
    End If
  ' Get RecTM
    If Not IsNull(rs1.Fields(3).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(3).Value) 
    ElseIf Not IsNull(rs1.Fields(1).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(1).Value) 
    End If
  ' Get UsrID
    If Not IsNull(rs1.Fields(4).Value) Then
      UsrID=rs1.Fields(4).Value
    ElseIf Not IsNull(rs1.Fields(2).Value) Then
      UsrID=rs1.Fields(2).Value
    Else
      UsrID="Null"
    End If

  ' Update Country Lists
    type_cd=rs1.Fields(6).value
    cd=rs1.Fields(5).Value
    myquery = "insert into eco.cts(RecCD, RecTM, UsrID, GtyCD, CtsCD, CtyCD)" &_
              " values('" &RecCD &"','" &RecTM &"'," &UsrID &",'" &type_CD &"','" &cd &"','" &rs1.Fields(7).Value &"')"
    dbconn.Execute "insert into eco.cts(RecCD, RecTM, UsrID, GtyCD, CtsCD, CtyCD)" &_
              " values('" &RecCD &"','" &RecTM &"'," &UsrID &",'" &type_CD &"','" &cd &"','" &rs1.Fields(7).Value &"')"

    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
End Sub

Sub LOAD_GEO_DISTRICT1
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.district where actflag <> 'X'", dbconn
  rs1.MoveFirst
  Do
  ' Get RecCD
    If Not IsNull(rs1.Fields(0).Value) Then
      RecCD=rs1.Fields(0).Value
    Else 
      RecCD="I"
    End If
  ' Get RecTM
    If Not IsNull(rs1.Fields(3).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(3).Value) 
    ElseIf Not IsNull(rs1.Fields(1).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(1).Value) 
    End If
  ' Get UsrID
    If Not IsNull(rs1.Fields(4).Value) Then
      UsrID=rs1.Fields(4).Value
    ElseIf Not IsNull(rs1.Fields(2).Value) Then
      UsrID=rs1.Fields(2).Value
    Else
      UsrID="Null"
    End If
  ' tidy LocName
    Name = Replace(rs1.Fields(8).value,"'","`")
  ' Lookup LocName
    If rs2.State = 1 Then rs2.Close
    rs2.Open "select LocID from eco.loc where LocName  = ('" &Name &"')", dbconn
    If rs2.EOF Then
    ' Insert new LOC
      dbconn.Execute "insert into eco.loc(RecCD, RecTM, UsrID, LocName, Sort)" &_
                     " values('" &RecCD &"','" &RecTM &"'," &UsrID &",'" &Name &"',"  &rs1.Fields(13).Value &")"
    ' get LocID
      If rs2.State = 1 Then rs2.Close
      rs2.Open "select LocID from eco.loc where LocName = '" &Name &"'", dbconn
    End If
  ' Save LocID  
    ID1 = rs2.Fields(0).Value
    ClientNote = rs1.Fields(12).value &" "
    ClientNote = Replace(ClientNote,"'","`")
    StaffNote = rs1.Fields(11).value &" "
    StaffNote = Replace(StaffNote,"'","`")
  ' Remap bad cntrycd
    If rs1.Fields(5).Value="EAZ" then
      cd="TZ"
    Else
      cd=rs1.Fields(5).Value
    End If
  ' Get OldID  
    OldID=rs1.Fields(6).Value
  ' Get pID
    pID="Null"
    myquery = "insert into eco.geo(RecCD, RecTM, UsrID, pGeoID, LocID, Sort, GtyCD, CtsCD, CtyCD, ClientNote, StaffNote, Dist_ID)" &_
                     " values('" &RecCD &"','" &RecTM &"'," &UsrID &"," &pID &"," &ID1 &"," &rs1.Fields(13).Value &",'D','" & cd &"','" & cd &"','" &ClientNote &"','" &StaffNote &"'," &OldID &")"
'    MsgBox myquery
    dbconn.Execute "insert into eco.geo(RecCD, RecTM, UsrID, pGeoID, LocID, Sort, GtyCD, CtsCD, CtyCD, ClientNote, StaffNote, Dist_ID)" &_
                     " values('" &RecCD &"','" &RecTM &"'," &UsrID &"," &pID &"," &ID1 &"," &rs1.Fields(13).Value &",'D','" & cd &"','" & cd &"','" &ClientNote &"','" &StaffNote &"'," &OldID &")"
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
End Sub

Sub LOAD_GEO_DISTRICT2
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.district where actflag <> 'X'", dbconn
  rs1.MoveFirst
  Do
    ' Remap bad cntrycd
    If rs1.Fields(5).Value="EAZ" then
      cd="TZ"
    Else
      cd=rs1.Fields(5).Value
    End If
  ' Split dist_cd into array 
    a=rs1.Fields(7).Value
    If Mid(a,1,1)="." Then
      a=Replace(a,".","")
    End If
    a=Replace(a,"..",".")
    MyArray=Split(a,".")
  ' Get OldID  
    OldID=rs1.Fields(6).Value
    If UBound(MyArray)=0 Then
      ' Insert TOP node record already done but assign rGeoID
      dbconn.Execute "update eco.geo set rGeoID=GeoID where Dist_ID=" &OldID &" and CtyCD='" &cd &"'"
    Else
      OldID=rs1.Fields(6).Value
      pOldID=MyArray(UBound(MyArray)-1)
      If rs2.State = 1 Then rs2.Close
      myquery = "select GeoID from eco.geo where Dist_ID = "&pOldID &" and CtyCD='" &cd&"'"
      rs2.Open "select GeoID from eco.geo where Dist_ID = "&pOldID &" and CtyCD='" &cd &"'" , dbconn
      If rs2.EOF Then
        WScript.Echo "Missing District for Dist_ID=" &OldID &" " &CD
      Else
        pID=rs2.Fields(0).Value
        If rs2.State = 1 Then rs2.Close
        rs2.Open "select GeoID from eco.geo where Dist_ID = "&MyArray(0) &" and CtyCD='" &cd &"'" , dbconn
        dbconn.Execute "update eco.geo set pGeoID=" &pID &", rGeoID=" & rs2.Fields(0).Value &" where Dist_ID=" &OldID &" and CtyCD='" &cd &"'"
      End If  
    End If  
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
End SUB

Sub LOAD_VCL
  If rs1.State = 1 Then rs1.Close
  rs1.Open "select * from afedivan.value_type where actflag <> 'X'", dbconn
  rs1.MoveFirst
  Do
  ' Get RecCD
    If Not IsNull(rs1.Fields(0).Value) Then
      RecCD=rs1.Fields(0).Value
    Else 
      RecCD="I"
    End If
  ' Get RecTM
    If Not IsNull(rs1.Fields(3).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(3).Value) 
    ElseIf Not IsNull(rs1.Fields(3).Value) Then
      RecTM=FORMAT_TM(rs1.Fields(1).Value) 
    End If
  ' Get UsrID
    If Not IsNull(rs1.Fields(4).Value) Then
      UsrID=rs1.Fields(4).Value
    ElseIf Not IsNull(rs1.Fields(2).Value) Then
      UsrID=rs1.Fields(2).Value
    Else
      UsrID="Null"
    End If
  ' Save VlcID  
    ID1 = rs1.Fields(5).Value
    If rs1.Fields(7).Value=0 Then
      cd="I"
    Else
      cd="P"
    End if  
    ClientNote = rs1.Fields(9).value &" "
    ClientNote = Replace(ClientNote,"'","`")
    StaffNote = rs1.Fields(10).value &" "
    StaffNote = Replace(StaffNote,"'","`")
    If rs2.State = 1 Then rs2.Close
    rs2.Open "select VclID from afed_data_prodman.vcl where VclID  = " &ID1, dbconn
    If rs2.EOF Then
    ' Insert new CAL
      dbconn.Execute "insert into afed_data_prodman.vcl(RecCD, RecTM, UsrID, VclID, VclName, VclColour, PstCD, Sort, ClientNote, StaffNote)" &_
                     " values('" &RecCD &"','" &RecTM &"'," &UsrID &"," &ID1 &",'" &rs1.Fields(6).Value &"','" &rs1.Fields(8).Value &"','" &cd &"'," &_
                                 rs1.Fields(11).Value &",'" &ClientNote &"','" &StaffNote &"')"
    End If
    rs1.MoveNext
  Loop While Not rs1.EOF
  rs1.Close
End Sub

Function FORMAT_TM(p_TM)
' Format datatime to YYYY-MM-DD 00:00:00
  If IsNull(p_TM) Then
  Else
    MyArray2=Split(p_TM,"/")
    FORMAT_TM=Mid(MyArray2(2),1,4) & "-" & Mid(MyArray2(1),1,2) & "-" & Mid(MyArray2(0),1,2) & " " & Mid(myarray2(2),6,8)
  End If  
END Function

Sub APPENDLOG(byval messagetxt)
  oFSO_File.Write MessageTxt & vbCrLf
END SUB

PRIVATE SUB OPENDBCON(p_conf)
  Dim fso, f1, ts, s, uname, pword, prov, pcata, dsource, MyArray
  Const ForReading = 1
  prov = "MySQL ODBC 5.1 Driver"
' Get connection details
  Set fso = CreateObject("Scripting.FileSystemObject")   
  Set ts = fso.OpenTextFile(p_conf, ForReading)
  s = ts.ReadLine
  MyArray = Split(s, vbTab)
  uname = MyArray(2)
  pword = MyArray(3)
  dsource = MyArray(4)			
  ts.Close
' connect to database
  On Error Resume Next
  Set dbconn = CreateObject("ADODB.Connection")
  dbconn.Open "Driver={"&prov &"}" &";Server=" &dsource &";" &_
  " Database=MY_AFED;" &_
  " User="&uname &";Password=" &pword &";"
  If Err.Number <> 0 Then
    AppendLog Err.Number & " " & Err.Description
    'MsgBox "no connection established"
  Else
    'msgbox "connection established"
  End If
  On Error Goto 0
END SUB
