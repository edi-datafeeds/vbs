'*************************************************************************
'
'                             ECO Tables Update Loader
'
'**************************************************************************

Option Explicit
'****Variables****
Dim CONF, dbconn, rs1, yyyymmdd, oFSO, oFSO_File, WshShell, Ziproot
'****Constants****
Const ForWriting = 2
'****DataBase Connections****
CONF="o:\Auto\configs\dbservers.cfg"
Call OPENDBCON(CONF, "MY_AFED")
'****Set Recordsets****
Set rs1=CreateObject("ADODB.Recordset")
'****Set Shell****
Set WshShell = WScript.CreateObject("WScript.Shell")
' Set FromDate
If rs1.State = 1 Then rs1.Close
rs1.Open "select max(RecTM) from eco.val", dbconn
If rs1.EOF Then
  MsgBox "Max(RecTM) could not be retrieved from eco.val"
Else  
  yyyymmdd=FORMAT_yyyymmdd(rs1.Fields(0).Value)
End If
Ziproot="ECO"

'****Main****
If Ziproot="ECO" Then
  Call ZipFolder("o:\datafeed\ECO\REF\HST\eco*.txt", "o:\datafeed\ECO\REF\ZIP\eco_" &yyyymmdd &"_REF_HST")
  Call ZipFolder("o:\datafeed\ECO\REF\CHG\eco*.txt", "o:\datafeed\ECO\REF\ZIP\eco_" &yyyymmdd &"_REF_CHG")
  Call ZipFolder("o:\datafeed\ECO\CTY\CHG\eco*.txt", "o:\datafeed\ECO\CTY\ZIP\eco_" &yyyymmdd &"_CTY_CHG")
ElseIf Ziproot="CAT" Then
'  Call UPDATE_CAT
End if

'oFSO_File.Close
WScript.Quit   

'*******************
'****SUBROUTINES****
'*******************
Sub ZipFolder(p_inpath,p_outpath)
  WshShell.Run "O:\Auto\Apps\ZIP\7-zip\7z.exe -y -tzip a " &p_outpath &" " &p_inpath, 1, True
End Sub 

Function FORMAT_yyyymmdd(p_TM)
Dim sMonth, sDay, sHour, sMinute, sSecond
' Format datatime to YYYY-MM-DD 00:00:00
  If IsNull(p_TM) Then
  Else
  ' Build custom output file name, ensuring that both sMonth and sDay are always two digits
    If Len(Month(p_TM))=1 Then sMonth="0" & month(p_TM) Else sMonth=month(p_TM) End if
    If Len(day(p_TM))=1 Then sDay="0" & Day(p_TM) Else sDay=Day(p_TM) End if
    'If Len(Hour(p_TM))=1 Then sHour="0" & Hour(p_TM) Else sHour=Hour(p_TM) End if
    'If Len(Minute(p_TM))=1 Then sMinute="0" & Minute(p_TM) Else sMinute=Minute(p_TM) End if
    'If Len(Second(p_TM))=1 Then sSecond="0" & Second(p_TM) Else sSecond=Second(p_TM) End if
    FORMAT_yyyymmdd=Year(p_TM)& sMonth & sDay
    'Mid(MyArray2(2),1,4) & "-" & Mid(MyArray2(1),1,2) & "-" & Mid(MyArray2(0),1,2) & " " & Mid(myarray2(2),6,8)
  End If  
END Function

Private SUB OPENDBCON(p_conf, p_svr)
  Dim fso, f1, ts, s, uname, pword, prov, pcata, dsource, MyArray
  Const ForReading = 1
  prov = "MySQL ODBC 5.1 Driver"
' Get connection details
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set ts = fso.OpenTextFile(p_conf, ForReading)
  Do 
    s = ts.ReadLine
    MyArray = Split(s, vbTab)
    If MyArray(0) = p_svr Then
      uname = MyArray(2)
      pword = MyArray(3)
      dsource = MyArray(4) 			
      Exit do
    End if		
  Loop while NOT ts.AtEndOfStream
  ts.Close
' connect to database
  On Error Resume Next
  Set dbconn = CreateObject("ADODB.Connection")
  dbconn.Open "Driver={"&prov &"}" &";Server=" &dsource &";" &_
  " Database=eco;" &_
  " User="&uname &";Password=" &pword &";"
  If Err.Number <> 0 Then
    AppendLog Err.Number & " " & Err.Description
  ' MsgBox "no connection established"
  Else
  ' msgbox "connection established"
  End If
  On Error Goto 0
END SUB
