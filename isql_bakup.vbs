option explicit


'**************************************************************************
' 		 GENERIC isql.exe Script		09/12/2010
' 	
' Script for parsing the connection details from the config file
'
' Arguments in order (0) Server Name
'		     (1) SQL Script location
'		     (2) Location of Config File	
'**************************************************************************

'***** MAIN *****

'***** VARIABLES *****

	dim fso, f, cmdline, SVR, SQL, CONF, WshShell, step1, numArgs, MyArray, s, ts, f1, OUT 
	CONST ForReading = 1, ForWriting = 2, ForAppending = 8


'***** Parsed Arguments *****


	numArgs = WScript.Arguments.Count

	SVR = WScript.Arguments.Item(0)
	SQL = WScript.Arguments.Item(1)
	CONF = WScript.Arguments.Item(2)
	'OUT = WScript.Arguments.Item(3)

  
'## Call ReadConfig ##   
  
ReadConfig


'## 
Sub ReadConfig
	Dim fso, f1, ts, s
	Const ForReading = 1
	Set fso = CreateObject("Scripting.FileSystemObject")   

	' Read the contents of the file.
	' Response.Write "Reading file <br>"

	Set ts = fso.OpenTextFile(CONF, ForReading)

	do 
		s = ts.ReadLine
		
		MyArray = Split(s, vbTab)	
		'MyArray = 
		
		'msgbox "|" & MyArray(0) & "|"
		
		if ucase(MyArray(0)) = ucase(SVR) Then 			
			cmdline = s
			exit do
			

		end if
				

	loop while NOT ts.AtEndOfStream
	cmdline = "-S " & MyArray(4) & " -U " & MyArray(2) & " -P " & MyArray(3) 
	'msgbox cmdline
	ts.Close

End Sub 

'cmdline = "-S 10.200.2.3 -U sa -P K376:lcnb"


'***** Processing *****

Set WshShell = WScript.CreateObject("WScript.Shell")

'msgbox "O:\AUTO\Apps\exe\isql.exe " & cmdline & " -i " & SQL
step1=WshShell.Run ("O:\AUTO\Apps\exe\isql.exe " & cmdline & " -i " & SQL, 1, true)

if step1 = "0" then
	'msgbox "isql Table Generated"
else
	msgbox "isql Table Failed"
end if
