'*****************************************************************************************************************************
'
'                                                          FeedTrans
'
'                                Script to Transfer Sample Feeds and Feed Documentation to FTP
'
'*****************************************************************************************************************************

Option Explicit

'** Variables **

Dim Drive, Wpath, sql
Dim WshShell, MyArray, oFSO, oFolder, oFiles, oFile, FileName, Arr, cmdline

'** Preparation **

'Load Argument Data
Drive = WScript.Arguments.Item(0)
sql = WScript.Arguments.Item(1)
Wpath = WScript.Arguments.Item(2)

Set WshShell = WScript.CreateObject("Wscript.Shell")

'** Main ***

'Choose whether to upload sample data or Documents and Specs
If sql = "docs" then 
'upload documents and specs only
Set oFSO = CreateObject("Scripting.FileSystemObject")
Set oFolder = oFSO.GetFolder(Drive&Wpath)
Set oFiles = oFolder.Files
  For Each oFile In oFiles 
  FileName = oFile.Name
  MyArray = Split(FileName,".")
  'Push files to DOTNET and DOTCOM
  WshShell.run "o:\auto\scripts\vbs\generic\ftp.vbs " &Replace(Drive,"\","/") &Replace(Wpath,"\","/") &" " &"/" &Replace(Wpath,"\","/") &" ."&MyArray(1) &" both upload scott scott"
  Next
  WScript.Quit
Else
  'Get File Extension
  Arr = Split(Wpath,"\")
  'Generate sample data
  WshShell.Run Drive &"AUTO\Apps\exe\prod.exe -m -s HAT_MS_Sqlsvr1 -f " &Drive &sql &" -d " &Drive &Wpath, 1, True
  'Push Sample to DOTNET and DOTCOM
  WshShell.Run "o:\auto\scripts\vbs\generic\ftp.vbs " &Replace(Drive,"\","/") &Replace(Wpath,"\","/") &" " &"/" &Replace(Wpath,"\","/") &" .txt" &" both upload scott scott"
  'cmdline = "o:\auto\scripts\vbs\generic\ftp.vbs " &Replace(Drive,"\","/") &Replace(Wpath,"\","/") &" " &"/" &Replace(Wpath,"\","/") &" .txt" &" both upload scott scott"
  'MsgBox cmdline
End If 
