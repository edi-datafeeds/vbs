'******************************************************************************************************************************
'
'                                            Script to Generate Price Feeds for Athena
'
'******************************************************************************************************************************

Option Explicit

'** Variables **
Dim sMonth, sDay, sYear, sFileName, sIcr, SVR, CONF, resultset, dbconn, oFSO, WshShell, oFS1, oFS1_File
'** Constants **
Const ForReading = 1, ForWriting = 2, ForAppending = 8, adOpenStatic = 3, adLockOptimistic = 3

'** Load Argument Data **
SVR = WScript.Arguments.Item(0)
Set oFSO = CreateObject("scripting.filesystemobject")
Set WshShell = WScript.CreateObject("WScript.Shell")

'** Hardcoded Information **
CONF="O:\Auto\Configs\DbServers.cfg"

'** Database Connections **
Call OpenDbCon(SVR, CONF)
Set resultset = CreateObject("ADODB.Recordset")

'** Build File Name
'Get Current Month
If Len(Month(Now))=1 then 
sMonth="0" & month(now)
Else
sMonth=month(now)
End If
'MsgBox sMonth
'Get Current Day		  
If Len(Day(now))=1 then 
sDay = "0" & day(Now)
Else
sDay = day(Now)
End If
'MsgBox sDay
'Get Year		
sYear = Year(Now)
'MsgBox sYear 
'Get Increment Number
resultset.Open "select time_format(date_sub(now(), interval 8 hour), '%h')", dbconn
sIcr = resultset.fields(0).value 
'Create File Name		  
sFileName = "EDI_" &sYear &sMonth &sDay &"_308_" &sIcr &".txt"
'MsgBox sFileName
'** Generate Price File
If Not oFSO.fileexists("o:\upload\acc\308\feed\" &sFileName) Then 
  Set oFS1 = CreateObject("Scripting.FileSystemObject")
  Set oFS1_File = oFS1.OpenTextFile("o:\upload\acc\308\feed\" &sFileName, ForWriting, True)
  oFS1_File.Close
'  MsgBox sFileName &" not created in o:\upload\acc\308\feed\"
'  WshShell.Run "o:\auto\apps\exe\mysql32.exe -h 192.168.2.63 -usa -pK376:lcnb < o:\auto\scripts\mysql\ops\ifeed\tp04_icr_308.sql"
  WshShell.Run "o:\prodman\dev\prices\athena\athena.bat"   
  WshShell.Run "O:\AUTO\Apps\ifeed\v01\iFeedV01.jar -m -s Prices_DB -f o:\auto\scripts\mysql\prices\ifeed\bespoke\308_athena_icr_p04.sql -d o:\upload\acc\308\feed\", 1, True
  WshShell.Run "o:\auto\scripts\vbs\generic\ftps.vbs o:/upload/acc/308/feed/ /Upload/Acc/308/feed/YYYYMMDD.txt both Upload Athena_308 EDI_:_308_01 Athena_308", 1, True 
Else
  'WScript.Echo "Already run in last hour!"
  WScript.Quit
End If  

WScript.Quit

Private Sub OpenDbCon (p_svr, p_conf)
Dim fso, f1, ts, s, uname, pword, prov, pcata, dsource, MyArray
Const ForReading = 1
prov = "MySQL ODBC 5.1 Driver"
'** Get connection details
Set fso = CreateObject("Scripting.FileSystemObject")   
Set ts = fso.OpenTextFile(p_conf, ForReading)
Do 
  s = ts.ReadLine
  MyArray = Split(s, vbTab)
  If MyArray(0) = p_svr Then
    uname = MyArray(2)
    pword = MyArray(3)
    dsource = MyArray(4) 			
    Exit do
  End if		
Loop while NOT ts.AtEndOfStream
ts.Close
'** connect to database
On Error Resume Next
Set dbconn = CreateObject("ADODB.Connection")
Dbconn.Open "Driver={"&prov &"}" &";Server=" &dsource &";" &_
" Database=client;" &_
" User="&uname &";Password=" &pword &";"
If Err.Number <> 0 Then
  MsgBox Err.Number & " " & Err.Description
End If
On Error Goto 0
End Sub