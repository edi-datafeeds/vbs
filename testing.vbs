'**************************************************************************
' 		 GENERIC APPENDFILE.VBS
' 	
' Script for copying the contents from one file to another file
'
' Arguments in order (0) ReadDirectory = C:\filepath\
'		     (1) ReadFile = file.txt
'		     (2) WriteDirectory = C:\filepath2\
'		     (3) WriteFile = if filename in argument use it but if argument "usedate" construct a filename
                        ' from todays date
 '                   (4) Fileext = .txt etc  
'**************************************************************************

'***** MAIN *****

'***** VARIABLES *****

Option Explicit

'Declare variables
Dim cunt, sExtension, strLine, i, fred, strFolder, strFile, colFiles, objFSO, objFolder, objShell, objTextFile, objFile, sDirectory, numArgs, sFile, dDirectory
Dim dFile, strText, objReadFile, contents, usedate, sdate, ddate, sfileext, dfileext, sMonth, sDay, sYear, DirList

		
	'Checks to see if the correct number of paramters have been set
	if WScript.Arguments.Count < 4 Then
		wscript.echo "There are less than 4 arguments"
		
		WScript.Quit	
		
	End if
		
		
		
'***** PREPERATION *****

' ********************************************** Main *****************************************************************
	
	'set 4 statuary arguments
	numArgs = WScript.Arguments.Count
	sDirectory = WScript.Arguments.Item(0)
	dDirectory = WScript.Arguments.Item(1)
	
	
	sFile = WScript.Arguments.Item(2)
	dFile = WScript.Arguments.Item(3)
	
	
	
	
	
			
	'assign variable arguments if they exist
	if wscript.arguments.count > 4 then
		sfileext = WScript.Arguments.Item(4)	
	
		Call CreateSdate()
	
	end if
	
	

	
	if wscript.arguments.count > 5 then
		dfileext = WScript.Arguments.Item(5)
	
		Call CreateDdate()
	
	end if
	
	
	'assign variable arguments if they exist
			
	If sfile = DirList Then	
			
		Call DirectoryList()
			
	end if
	
	
	Call DirectoryList()	
	'Call ReadFile()
	'Call CreateDirectory()
	'Call CreateFile()
	'Call WriteToFile()
	
	
	
	Sub CreateSdate()
	
	'Use this paramter to get the date name of the file
		if WScript.Arguments.Item(2)="sdate" Then
		
		
		
	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits
		  
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
		  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
	
		sYear = mid(Year(Now),1,4)
	
		'-- Create the Custom filename
		 
		sFile = sYear & sMonth & sDay & sfileext
	
	End if
		
	End Sub
	
	
	
			
	Sub CreateDdate()	


	
	'Use this paramter to get the date name of the file
	if WScript.Arguments.Item(3)="ddate" Then
	
	
	
	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits
	  
	if Len(Month(Now))=1 then 
	  	sMonth="0" & month(now)
	else
	  	sMonth=month(now)
	end if
	  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
	
		sYear = mid(Year(Now),1,4)
	  
		'-- Create the Custom filename without a extension  
	 
		dFile = sYear & sMonth & sDay & dfileext
	
	End if
	
	End Sub
	
	
	
'***** PROCESSING *****




Sub DirectoryList()

	Const ForReading = 1
	strFolder = sDirectory
	'strFile = sfileext
	
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	Set objFolder=objFSO.GetFolder(strFolder)
	'Set objFile=objFSO.GetFile(strFile)
	

	'WScript.Echo objFolder.Path
	
	Set colFiles=objFolder.Files
	
	For each objFile in colFiles		
			
	
	
	
	  Wscript.Echo objFile.Extension
	
	
	
	
	
	
	
		
'*********************************************************************************************	OLD	
		' if file ext is valid then sfileext`		
		
		'sExtension=lcase(objFSO.getxtensionname(objFile))
		  
		  'sfileext=sExtension
		  
		  
		  'if sExtension=sfileext then 
		  
		  'wscript.echo sfileext
		 		  
		
		'end if
		
'**********************************************************************************************		
				
		
		
		fred = fred & objFile.path & vbCrLf
		
		'WScript.Echo fred	
	Next
	
	fred = mid(fred, 1,len(fred)-2) 
	
	' OpenTextFile Method needs a Const value
	' ForAppending = 8 ForReading = 1, ForWriting = 2
	Const ForAppending = 8	
	Set objTextFile = objFSO.OpenTextFile("H:\y.laifa\dirlist\dirlist.txt", ForAppending, True)	
	' Writes to dFile every time you run this VBScript
	objTextFile.WriteLine(fred)
	objTextFile.Close
	
End Sub



'***** Read Directory/File *****

Sub ReadFile()
		
	' Create the File System Object
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	
	'wscript.echo fred
	
	Set objReadFile = objFSO.OpenTextFile("H:\y.laifa\dirlist\dirlist.txt")
	

	' OpenTextFile Method needs a Const value
	' ForAppending = 8 ForReading = 1, ForWriting = 2
	Const ForReading = 1

	
	Do Until ObjReadFile.AtEndOfStream
	i = i + 1
	strLine = ObjReadFile.ReadLine
	WScript.Echo "Line " & i & ": " & strLine
	
	Loop
	
	
	
	
	
	
	
	'Read file contents
	contents = objReadFile.ReadAll

	'Close file
	objReadFile.close

	'Display results
	'wscript.echo contents

	'Cleanup objects
	Set objFSO = Nothing
	'Set objReadFile = Nothing


	wscript.echo contents


End Sub





'***** Create Directory/File & Write to File *****

Sub CreateDirectory()


	' Create the File System Object
	Set objFSO = CreateObject("Scripting.FileSystemObject")

	' Check that the dDirectory folder exists
	If objFSO.FolderExists(dDirectory) Then
		Set objFolder = objFSO.GetFolder(dDirectory)
	Else
		 ' Create the dDirectory folder if folder does not exist
		Set objFolder = objFSO.CreateFolder(dDirectory)
		   'WScript.Echo "Just created " & dDirectory
	End If


End Sub



Sub CreateFile()


	If objFSO.FileExists(dDirectory & dFile) Then
		Set objFolder = objFSO.GetFolder(dDirectory)
	Else
		Set objFile = objFSO.CreateTextFile(dDirectory & dFile)
		   'Wscript.Echo "Just created " & dDirectory & dFile
	End If

	set objFile = nothing
	set objFolder = nothing

End Sub



Sub WriteToFile()

	' OpenTextFile Method needs a Const value
	' ForAppending = 8 ForReading = 1, ForWriting = 2
	Const ForAppending = 8

	Set objTextFile = objFSO.OpenTextFile _
	(dDirectory & dFile, ForAppending, True)

	' Writes to dFile every time you run this VBScript
	objTextFile.WriteLine(contents)
	objTextFile.Close

End Sub


WScript.Quit

'***** END MAIN *****