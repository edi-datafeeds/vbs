'****************************************************************************************************************************************
'
'                                                            YieldCalc Loader
'
'****************************************************************************************************************************************

Option Explicit

'** Variables **
Dim SVR
Dim Conf, dbconn, MyArray, SourceFolder, Datalist
Dim objFolder, objFilelist, objFile, oFSO, FileModDate, Filename
Dim Runmode, fso, ts, s, FileArray, r, i, fso1, fso1_file, resultset, Deletect
Const adVarChar=200, MaxCharacters=255, ForReading = 1, ForWriting = 2, ForAppending = 8, adOpenStatic = 3, adLockOptimistic = 3

'** Load Argument Data **
SVR = WScript.Arguments.Item(0)

'** Hardcoded Information **
Conf = "O:\Auto\Configs\DbServers.cfg"
SourceFolder = "O:\Upload\Acc\185\calcout\test\"
Set oFSO=CreateObject("Scripting.FileSystemObject")
Set resultset = CreateObject("ADODB.Recordset")

'** Initialise Log **
Set fso1 = CreateObject("Scripting.FileSystemObject")
Set fso1_file = fso1.OpenTextFile("o:\upload\acc\185\logs\Yield_LoadLog1.txt", ForAppending, True)
fso1_file.Write "*********************************************************" & vbCrLF & "Log Created At: " & Now &  vbCrLf &  vbCrLF

'** Database Connections **
'MsgBox svr
'MsgBox conf
Call OpenDbCon(SVR, Conf)

'** Create Datalist **
Set DataList = CreateObject("ADOR.Recordset")
DataList.Fields.Append "Filename", adVarChar, MaxCharacters
DataList.Fields.Append "FileModDate", adVarChar, MaxCharacters

'** Main **

'** Loop through Datalist and sort files in ascending order ** 
Datalist.Open

Set objFolder = oFSO.GetFolder(SourceFolder)
Set objFilelist = objFolder.Files

For Each objFile In objFilelist
  Datalist.AddNew
  DataList("Filename") = objFile.Name
  Datalist("FileModDate") = Mid(objFile.DateLastModified,7,4)&"/"&Mid(objFile.DateLastModified,4,2)&"/"&Mid(objFile.DateLastModified,1,2)
  DataList.Update
Next
If DataList.RecordCount>0 Then
  If Runmode<>"AUTO" then
  'WScript.Echo DataList.RecordCount
  End if
  DataList.Sort = "FileModDate ASC"
  DataList.MoveFirst
  Filename = Datalist.Fields(0).Value
  FileModDate = Datalist.Fields(1).Value
Else
  If Runmode<>"AUTO" then
  'WScript.Echo "No loadable files in " &oFSO.GetFolder(SourceFolder)
  AppendLog "No loadable files in "  &oFSO.GetFolder(SourceFolder)
  End if
  WScript.quit
End If

'** Clear out yieldcalc_temp before loading new file **
dbconn.Execute "delete from priceshist.yieldcalc_temp"

'** Loop to load calculated fields in file into yieldcalc_temp **
For i = 1 To DataList.RecordCount
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set ts = fso.OpenTextFile(SourceFolder &Filename, 1, True)
  r = ts.SkipLine
  Do
    s = ts.ReadLine
    FileArray = Split(s, vbTab)
    If UBound(FileArray) < 12 Then
      'WScript.Echo "File contains less than 12 fields"
      AppendLog "CANNOT LOAD!: File " &Filename &" contains less than 12 fields"
      Exit Do 
    Else
      If FileArray(4) = "" Then FileArray(4) = "''"
      If FileArray(5) = "" or FileArray(5) = "NaN" Then FileArray(5) = "''"
      If FileArray(6) = ""  or FileArray(6) = "NaN" Then FileArray(6) = "''"
      If FileArray(7) = ""  or FileArray(7) = "NaN" Then FileArray(7) = "''"
      If FileArray(8) = ""  or FileArray(8) = "NaN" Then FileArray(8) = "''"
      If FileArray(9) = ""  or FileArray(9) = "NaN" Then FileArray(9) = "''"
      If FileArray(10) = "" Or FileArray(10) = "NaN" Then FileArray(10) = "''"
      If FileArray(11) = "" Or FileArray(11) = "NaN" Then FileArray(11) = "''"
      If FileArray(12) = "" or FileArray(12) = "NaN" Then FileArray(12) = "''"
      dbconn.Execute "insert ignore into priceshist.yieldcalc_temp(isin, secid, exchgcd, mktclosedate, pricecurrency, accruedinterest, yieldtomaturity," &_
                     " yieldtocall, yieldtoput, modifiedduration, effectiveduration, macaulayduration, convexity) values (" &_
                     "'"&FileArray(4)&"'" &"," &FileArray(1) &"," &"'"&FileArray(3)&"'" &"," &"'"&FileArray(0)&"'" &"," &"'"&FileArray(2)&"'" &"," &FileArray(5) &"," &FileArray(6) &"," &_
                     FileArray(7) &"," &FileArray(8) &"," &FileArray(9) &"," &FileArray(10) &"," &FileArray(11) &"," &FileArray(12)&")"
    End if
  Loop While Not ts.AtEndOfStream
  ts.Close
  oFSO.DeleteFile(SourceFolder &Filename)
  
  If i<DataList.RecordCount Then
    DataList.MoveNext
    Filename=DataList.Fields(0).Value
    FileModDate=DataList.Fields(1).Value
  End If

Next

'** Delete everything from yieldcalc that exists in yieldcalc_temp ** 
dbconn.Execute "delete from priceshist.yieldcalc where secid in (select secid from priceshist.yieldcalc_temp)" &_
              " and mktclosedate in (select mktclosedate from priceshist.yieldcalc_temp)" &_
              " and exchgcd in (select exchgcd from priceshist.yieldcalc_temp)" &_
              " and pricecurrency in (select pricecurrency from priceshist.yieldcalc_temp)" 

'** Insert everything from yieldcalc_temp into yieldcalc ** 
dbconn.Execute "insert ignore into priceshist.yieldcalc select * from priceshist.yieldcalc_temp"

'** Datestamp new data in yieldcalc **
dbconn.Execute "update priceshist.yieldcalc" &_
              " set acttime = now()" &_
              " where acttime is null"

'** Subroutines **
Private Sub OpenDbCon (p_svr, p_conf)
Dim fso, ts, s, uname, pword, prov, dsource
Const ForReading = 1
prov = "MySQL ODBC 5.1 Driver"
'** Get connection details
Set fso = CreateObject("Scripting.FileSystemObject")   
Set ts = fso.OpenTextFile(p_conf, ForReading)
Do 
  s = ts.ReadLine
  MyArray = Split(s, vbTab)
  If MyArray(0) = p_svr Then
    uname = MyArray(2)
    pword = MyArray(3)
    dsource = MyArray(4) 			
    Exit do
  End if		
Loop while NOT ts.AtEndOfStream
ts.Close
'** connect to database
On Error Resume Next
Set dbconn = CreateObject("ADODB.Connection")
Dbconn.Open "Driver={"&prov &"}" &";Server=" &dsource &";" &_
" Database=priceshist;" &_
" User="&uname &";Password=" &pword &";"
If Err.Number <> 0 Then
  MsgBox Err.Number & " " & Err.Description
End If
On Error Goto 0
End Sub

Sub AppendLog(ByVal MessageTxt)
fso1_File.Write MessageTxt & vbCrLf & "*********************************************************"
End Sub