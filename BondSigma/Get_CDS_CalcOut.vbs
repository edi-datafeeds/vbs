' ****************************************************************
' GET_WCA_FEED.VBS
'
' ****************************************************************
option explicit


' **** SUB MAIN ****
dim fso,wso,CRLF,sTime,sExt,sMonth,sDay,sYear,sFilename,Y

	CRLF = Chr(13) & Chr(10)

	


	Set fso = CreateObject("Scripting.FileSystemObject")
	Set wso = Wscript.CreateObject("Wscript.Shell")


	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if

	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if

	sYear = Year(Now)

' Build filename

	sFilename = "CDS_FIPS_BondSigma_" & sYear & "-" & sMonth & "-" & sDay


' Build File Extension
	sTime = Time
	sTime = int(left(sTime,2))
	'msgbox sTime
	
	 
		sExt = "zip"
	 
	
	
	
 
	'Check download successfull and push to iCloud and exit script
  	'if fso.fileexists("o:\Datafeed\Bespoke\BondSigma\Calcout\" & sFilename &"." & sExt) then 


         'Y = wso.Run ("O:\AUTO\Apps\opsftp\dist\opsftp.jar -site ICLOUD3 -upload -local o:/upload/acc/185/calcout/ -remote /upload/acc/185/calcout/ -fileext " & sFilename & "." & sExt & " -XCRC on -log o:\AUTO\logs\Ftp.log", 1, true)
	 'Y = wso.Run ("O:\AUTO\Apps\opsftp\dist\opsftp.jar -site DOTCOM -upload -local O:/Datafeed/Bespoke/BondSigma/Calcout/ -remote /Bespoke/Beastapps/calcout/ -fileext " & sFilename & "." & sExt & " -XCRC on -log o:\AUTO\logs\Ftp.log", 1, true)
	 'WScript.quit(0)

        'else
             ' if doesnt exist delay for 4 mins and try again
             'Wscript.Sleep 0*1000    	
     	     
             ' download latest  file                      
             Y = wso.Run ("O:\AUTO\Apps\opsftp\dist\opsftp.jar -site DOTCOM -download -local O:/Datafeed/Debt/CDS/FIPS/CalcOut/ -remote /Bespoke/BondSigma/CDS_FIPS/CalcOut/ -fileext " & sFilename & "." & sExt & " -XCRC on -log o:\AUTO\logs\Ftp.log", 1, true)
             
             Do
             ' Check download successfull and push to iCloud and exit script 
             if fso.fileexists("O:/Datafeed/Debt/CDS/FIPS/CalcOut/" & sFilename &"." & sExt) then
                
		 	 WScript.quit(0)
             else
             	' if doesnt exist delay for 10 mins and try again
             	Wscript.Sleep 600*1000 
             	' download latest  file
                Y = wso.Run ("O:\AUTO\Apps\opsftp\dist\opsftp.jar -site DOTCOM -download -local O:/Datafeed/Debt/CDS/FIPS/CalcOut/ -remote /Bespoke/BondSigma/CDS_FIPS/CalcOut/ -fileext " & sFilename & "." & sExt & " -XCRC on -log o:\AUTO\logs\Ftp.log", 1, true)
                
                'Check download successfull and push to iCloud and exit script
                if fso.fileexists("O:/Datafeed/Debt/CDS/FIPS/CalcOut/" & sFilename &"." & sExt) then
                
				WScript.quit(0)
				
				
                else
                msgbox "CDS CalcOut Download Failed - Please notify Michael Beardmore"
		
                'end if
                             
           	
             end If
             
            
  	End If
  	
  	 Loop