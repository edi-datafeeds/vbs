'****************************************************************************************************************************************
'
'                                                            YieldCalc Loader
'
'****************************************************************************************************************************************

Option Explicit

'** Variables **
Dim SVR
Dim Conf, dbconn, MyArray, SourceFolder, Datalist
Dim objFolder, objFilelist, objFile, oFSO, FileModDate, Filename
Dim Runmode, fso, ts, s, FileArray, r, i, fso1, fso1_file, resultset, Deletect
Const adVarChar=200, MaxCharacters=255, ForReading = 1, ForWriting = 2, ForAppending = 8, adOpenStatic = 3, adLockOptimistic = 3

'** Load Argument Data **
SVR = WScript.Arguments.Item(0)

'** Hardcoded Information **
Conf = "O:\Auto\Configs\DbServers.cfg"
SourceFolder = "O:\Datafeed\Bespoke\BondSigma\CalcOut\"
Set oFSO=CreateObject("Scripting.FileSystemObject")
Set resultset = CreateObject("ADODB.Recordset")

'** Initialise Log **
Set fso1 = CreateObject("Scripting.FileSystemObject")
Set fso1_file = fso1.OpenTextFile("O:\Datafeed\Bespoke\BeastApps\Logs\Yield_LoadLog1.txt", ForAppending, True)
fso1_file.Write "*********************************************************" & vbCrLF & "Log Created At: " & Now &  vbCrLf &  vbCrLF

'** Database Connections **
'MsgBox svr
'MsgBox conf
Call OpenDbCon(SVR, Conf)

'** Create Datalist **
Set DataList = CreateObject("ADOR.Recordset")
DataList.Fields.Append "Filename", adVarChar, MaxCharacters
DataList.Fields.Append "FileModDate", adVarChar, MaxCharacters

'** Main **

'** Loop through Datalist and sort files in ascending order ** 
Datalist.Open

Set objFolder = oFSO.GetFolder(SourceFolder)
Set objFilelist = objFolder.Files

For Each objFile In objFilelist
  Datalist.AddNew
  DataList("Filename") = objFile.Name
  Datalist("FileModDate") = Mid(objFile.DateLastModified,7,4)&"/"&Mid(objFile.DateLastModified,4,2)&"/"&Mid(objFile.DateLastModified,1,2)
  DataList.Update
Next
If DataList.RecordCount>0 Then
  If Runmode<>"AUTO" then
  'WScript.Echo DataList.RecordCount
  End if
  DataList.Sort = "FileModDate ASC"
  DataList.MoveFirst
  Filename = Datalist.Fields(0).Value
  FileModDate = Datalist.Fields(1).Value
Else
  If Runmode<>"AUTO" then
  'WScript.Echo "No loadable files in " &oFSO.GetFolder(SourceFolder)
  AppendLog "No loadable files in "  &oFSO.GetFolder(SourceFolder)
  End if
  WScript.quit
End If

'** Clear out yieldcalc_temp before loading new file **
dbconn.Execute "delete from bondsigma.calcout"

'** Loop to load calculated fields in file into yieldcalc_temp **
For i = 1 To DataList.RecordCount
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set ts = fso.OpenTextFile(SourceFolder &Filename, 1, True)
  r = ts.SkipLine
  Do
    s = ts.ReadLine
    FileArray = Split(s, vbTab)
    If UBound(FileArray) < 12 Then
      'WScript.Echo "File contains less than 12 fields"
      AppendLog "CANNOT LOAD!: File " &Filename &" contains less than 12 fields"
      Exit Do 
    Else
      'If FileArray(4) = "" Then FileArray(4) = "''"
      'If FileArray(5) = "" or FileArray(5) = "NaN" Then FileArray(5) = "''"
      'If FileArray(6) = ""  or FileArray(6) = "NaN" Then FileArray(6) = "''"
      'If FileArray(7) = ""  or FileArray(7) = "NaN" Then FileArray(7) = "''"
      'If FileArray(8) = ""  or FileArray(8) = "NaN" Then FileArray(8) = "''"
      'If FileArray(9) = ""  or FileArray(9) = "NaN" Then FileArray(9) = "''"
      'If FileArray(10) = "" Or FileArray(10) = "NaN" Then FileArray(10) = "''"
      'If FileArray(11) = "" Or FileArray(11) = "NaN" Then FileArray(11) = "''"
      'If FileArray(12) = "" or FileArray(12) = "NaN" Then FileArray(12) = "''"
 

'MsgBox FileArray(35)
'MsgBox FileArray(36)
 
'     dbconn.Execute "insert ignore into BondSigma.calcout_new (secid, exchgcd, LocalCode, mktclosedate," &_
'                     " LastTradeDate, ClosingPrice, pricecurrency, accruedinterest, yieldtomaturity," &_
'                     " yieldtocall, yieldtoput, modifiedduration, effectiveduration, macaulayduration" &_ 
'                     " convexity, Yield, YieldToWorst, KeyRateDuration, Acttime) values (" &_
'                     FileArray(1) &"," &"'"&FileArray(34)&"'" &"," &FileArray(35)&"'" &"," &"'"&FileArray(36)&"'" &"," &"'"
'                     &FileArray(36)&"'" &"," &"'" &FileArray(37) &"'" &"," &FileArray(38) &"," &FileArray(49) &"," &_
'                     FileArray(50) &"," &FileArray(51) &"," &FileArray(52) &"," &FileArray(53) &"," &FileArray(54) &"," &_
'                     FileArray(55) &"," &FileArray(56) &"," &FileArray(57) &"," &FileArray(58 &"," &FileArray(59) &"," &_
'                     FileArray(60)&")"




'Currrent script To use

dbconn.Execute "insert ignore into bondsigma.calcout (secid, InterestRate, InterestPaymentFrequency, " &_
				" Callable, Puttable, Callput, CpType, CpoptID, FromDate, ToDate, CpCurrency, CpPrice, CpPriceAsPercent, " &_
				" SettlementDate, exchgcd, LocalCode, mktclosedate, LastTradeDate, ClosingPrice, Open, High, Low, Mid, Ask, Bid, BidSize, AskSize, TradedVolume, VolFlag, pricecurrency, accruedinterest, " &_
				" yieldtomaturity, yieldtocall, yieldtoput, modifiedduration, effectiveduration, macaulayduration, convexity, Yield, YieldToWorst, KeyRateDuration) values (" &_
				FileArray(1)&","  &"'"&FileArray(12)&"'" &"," &"'"&FileArray(13)&"'" &"," &_ 
				"'"&FileArray(20)&"'" &","  &"'"&FileArray(21)&"'" &"," &"'"&FileArray(22)&"'" &"," &"'"&FileArray(23)&"'" &"," &"'"&FileArray(24)&"'" &"," &"'"&FileArray(25)&"'" &"," &_
				"'"&FileArray(26)&"'" &"," &"'"&FileArray(27)&"'" &"," &"'"&FileArray(28)&"'" &"," &"'"&FileArray(29)&"'" &"," &"'"&FileArray(33)&"'" &"," &"'"&FileArray(34)&"'" &"," &_
				"'"&FileArray(35)&"'" &","  &"'"&FileArray(36)&"'" &","  &"'"&FileArray(37)&"'" &"," &_
				"'"&FileArray(38)&"'" &","  &"'"&FileArray(39)&"'" &","  &"'"&FileArray(40)&"'" &"," &"'"&FileArray(41)&"'" &"," &"'"&FileArray(42)&"'" &"," &"'"&FileArray(43)&"'" &"," &"'"&FileArray(44)&"'" &"," &"'"&FileArray(45)&"'" &"," &"'"&FileArray(46)&"'" &"," &"'"&FileArray(47)&"'" &"," &"'"&FileArray(48)&"'" &"," &"'"&FileArray(49)&"'" &"," &"'"&FileArray(50)&"'" &","  &"'"&FileArray(51)&"'" &","  &"'"&FileArray(52)&"'" &"," &_
				"'"&FileArray(53)&"'" &","  &"'"&FileArray(54)&"'" &","  &"'"&FileArray(55)&"'" &","  &"'"&FileArray(56)&"'" &","  &"'"&FileArray(57)&"'" &"," &_
				"'"&FileArray(58)&"'" &","  &"'"&FileArray(59)&"'" &","  &"'"&FileArray(60)&"'" &")"



'dbconn.Execute "insert ignore into BondSigma.calcout_new (secid, " &_
'				" SettlementDate, exchgcd, LocalCode, mktclosedate, LastTradeDate, ClosingPrice, pricecurrency, accruedinterest, " &_
'				" yieldtomaturity, yieldtocall, yieldtoput, modifiedduration, effectiveduration, macaulayduration, convexity, Yield, YieldToWorst, KeyRateDuration) values (" &_
'				FileArray(1)&"," &_ 
'				"'"&FileArray(36)&"'" &","  &"'"&FileArray(37)&"'" &","  &"'"&FileArray(38)&"'" &","  &"'"&FileArray(39)&"'" &"," &_
'				"'"&FileArray(40)&"'" &","  &"'"&FileArray(41)&"'" &","  &"'"&FileArray(52)&"'" &","  &"'"&FileArray(53)&"'" &","  &"'"&FileArray(54)&"'" &"," &_
'				"'"&FileArray(55)&"'" &","  &"'"&FileArray(56)&"'" &","  &"'"&FileArray(57)&"'" &","  &"'"&FileArray(58)&"'" &","  &"'"&FileArray(59)&"'" &"," &_
'				"'"&FileArray(60)&"'" &","  &"'"&FileArray(61)&"'" &","  &"'"&FileArray(62)&"'" &","  &"'"&FileArray(63)&"'" &")"
















'dbconn.Execute "insert ignore into BondSigma.calcout_new (Isin, SecID, UsCode, Issuername, DebtType, SecurityDesc, " &_
'			   "CntryofIncorp, SectyCD, DebtCurrency, NominalValue, OutstandingAmount, InterestType, InterestRate, " &_
'			   "InterestPaymentFrequency, InterestAccrualConvention, MaturityDate, MatPriceAsPercent, FrnIndexBenchmark, " &_
'			   "Markup, MaturityStructure, Callable, Puttable, Callput, CpType, CpoptID, FromDate, ToDate, CpCurrency, " &_
'			   "CpPrice, CpPriceAsPercent, IssuePrice, IssueDate, Perpetual, SettlementDate, ExchgCD, LocalCode, MktCloseDate, " &_
'			   "LastTradeDate, ClosingPrice, Open, High, Low, Mid, Ask, Bid, BidSize, AskSize, TradedVolume, VolFlag, PriceCurrency, " &_
'			   "AccruedInterest, YieldtoMaturity, YieldtoCall, YieldtoPut, ModifiedDuration, EffectiveDuration, MacaulayDuration, " &_
'			   "Convexity, Yield, YieldToWorst, KeyRateDuration) values (" &_
'				"'"&FileArray(0)&"'" &"," &FileArray(1) &"," &"'"&FileArray(2)&"'" &","  &"'"&FileArray(3)&"'" &"," &_
'				"'"&FileArray(4)&"'" &"," &"'"&FileArray(5)&"'" &"," &"'"&FileArray(6)&"'" &"," &"'"&FileArray(7)&"'" &"," &_
'				"'"&FileArray(8)&"'" &"," &"'"&FileArray(9)&"'" &"," &"'"&FileArray(10)&"'" &"," &"'"&FileArray(11)&"'" &"," &_
'				"'"&FileArray(12)&"'" &"," &"'"&FileArray(13)&"'" &"," &"'"&FileArray(14)&"'" &"," &"'"&FileArray(15)&"'" &"," &_
'				"'"&FileArray(16)&"'" &"," &"'"&FileArray(17)&"'" &"," &"'"&FileArray(18)&"'" &"," &"'"&FileArray(19)&"'" &"," &_
'				"'"&FileArray(20)&"'" &"," &"'"&FileArray(21)&"'" &"," &"'"&FileArray(22)&"'" &"," &"'"&FileArray(23)&"'" &"," &_
'				"'"&FileArray(24)&"'" &"," &"'"&FileArray(25)&"'" &"," &"'"&FileArray(26)&"'" &"," &"'"&FileArray(27)&"'" &"," &_
'				"'"&FileArray(28)&"'" &"," &"'"&FileArray(29)&"'" &"," &"'"&FileArray(30)&"'" &"," &"'"&FileArray(31)&"'" &"," &_
'				"'"&FileArray(32)&"'" &"," &"'"&FileArray(33)&"'" &"," &"'"&FileArray(34)&"'" &"," &"'"&FileArray(35)&"'" &"," &_
'				"'"&FileArray(36)&"'" &"," &"'"&FileArray(37)&"'" &"," &"'"&FileArray(38)&"'" &"," &"'"&FileArray(39)&"'" &"," &_
'				"'"&FileArray(40)&"'" &"," &"'"&FileArray(41)&"'" &"," &"'"&FileArray(42)&"'" &"," &"'"&FileArray(43)&"'" &"," &_
'				"'"&FileArray(44)&"'" &"," &"'"&FileArray(45)&"'" &"," &"'"&FileArray(46)&"'" &"," &"'"&FileArray(47)&"'" &"," &_
'				"'"&FileArray(48)&"'" &"," &"'"&FileArray(49)&"'" &"," &"'"&FileArray(50)&"'" &"," &"'"&FileArray(51)&"'" &"," &_
'				"'"&FileArray(52)&"'" &"," &"'"&FileArray(53)&"'" &"," &"'"&FileArray(54)&"'" &"," &"'"&FileArray(55)&"'" &"," &_
'				"'"&FileArray(56)&"'" &"," &"'"&FileArray(57)&"'" &"," &"'"&FileArray(58)&"'" &"," &"'"&FileArray(59)&"'" &"," &_
'				"'"&FileArray(60)&"'" &")"
			
'			   " ,  " &_
'			   " ,  " &_
'			   " , ,  " &_
'			   " , " &_
'			   " , , " &_
'			   " KeyRateDuration) values (" &_			   
				
				
'					
'				    &"," &_
'				  &"," &"'"&FileArray(20)&"'" &"," &_
'				
'				  &"," &_
'				 &"," &_
'				"'"&FileArray(33)&"'" &"," &"'"&FileArray(34)&"'" &"," &"'"&FileArray(35)&"'" &"," &"'"&FileArray(36)&"'" &"," &_
'				"'"&FileArray(37)&"'" &"," &"'"&FileArray(38)&"'" &"," &"'"&FileArray(39)&"'" &"," &"'"&FileArray(40)&"'" &"," &_
'				"'"&FileArray(41)&"'" &"," &"'"&FileArray(42)&"'" &"," &"'"&FileArray(43)&"'" &"," &"'"&FileArray(44)&"'" &"," &_
'				"'"&FileArray(45)&"'" &"," &"'"&FileArray(46)&"'" &"," &"'"&FileArray(47)&"'" &"," &"'"&FileArray(48)&"'" &"," &_
'				"'"&FileArray(49)&"'" &"," &"'"&FileArray(50)&"'" &"," &"'"&FileArray(51)&"'" &"," &"'"&FileArray(52)&"'" &"," &_
'				"'"&FileArray(53)&"'" &"," &"'"&FileArray(54)&"'" &"," &"'"&FileArray(55)&"'" &"," &"'"&FileArray(56)&"'" &"," &_
'				"'"&FileArray(57)&"'" &"," &"'"&FileArray(58)&"'" &"," &"'"&FileArray(59)&"'" &"," &"'"&FileArray(60)&"'" &")"













'dbconn.Execute "insert ignore into BondSigma.calcout_new (secid) values (" &_
'				"'"&FileArray(1)&"'" &")"    
    
    
    
    End if
  Loop While Not ts.AtEndOfStream
  ts.Close
  'oFSO.DeleteFile(SourceFolder &Filename)
  
  If i<DataList.RecordCount Then
    DataList.MoveNext
    Filename=DataList.Fields(0).Value
    FileModDate=DataList.Fields(1).Value
  End If

Next

'** Datestamp new data in yieldcalc **
dbconn.Execute "update bondsigma.calcout" &_
              " set acttime = now()" &_
              " where acttime is null"

'** Subroutines **
Private Sub OpenDbCon (p_svr, p_conf)
Dim fso, ts, s, uname, pword, prov, dsource
Const ForReading = 1
prov = "MySQL ODBC 5.1 Driver"
'** Get connection details
Set fso = CreateObject("Scripting.FileSystemObject")   
Set ts = fso.OpenTextFile(p_conf, ForReading)
Do 
  s = ts.ReadLine
  MyArray = Split(s, vbTab)
  If MyArray(0) = p_svr Then
    uname = MyArray(2)
    pword = MyArray(3)
    dsource = MyArray(4) 			
    Exit do
  End if		
Loop while NOT ts.AtEndOfStream
ts.Close
'** connect to database
On Error Resume Next
Set dbconn = CreateObject("ADODB.Connection")
Dbconn.Open "Driver={"&prov &"}" &";Server=" &dsource &";" &_
" Database=bondsigma;" &_
" User="&uname &";Password=" &pword &";"
If Err.Number <> 0 Then
  MsgBox Err.Number & " " & Err.Description
End If
On Error Goto 0
End Sub

Sub AppendLog(ByVal MessageTxt)
fso1_File.Write MessageTxt & vbCrLf & "*********************************************************"
End Sub