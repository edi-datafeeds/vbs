'********************************************************************************************************************
'
'                          Script to Load Hilstorical Price Data for Client Portfolio's
'
'********************************************************************************************************************

Option Explicit

'** Variables **
Dim Accid, Pftype, SVR, Offset
Dim CONF, dbconn, resultset, rs, TargetFolder, oFSO_Folder, oFSO, Mic, CntryCd
Dim InPath, WshShell, MyOffset, MyYear, MyArray, list, f, MyArray2, fdate, fdate2, mktclosedate
Const ForReading = 1, ForWriting = 2, ForAppending = 8, adOpenStatic = 3, adLockOptimistic = 3

'** Preparation **

'** Load Argument Data **
AccID = WScript.Arguments.Item(0)
Pftype = WScript.Arguments.Item(1)
SVR = WScript.Arguments.Item(2)
Offset = WScript.Arguments.Item(3)

'** Create FileSystemObject **
Set oFSO=CreateObject("Scripting.FileSystemObject")
Set WshShell = WScript.CreateObject("WScript.Shell")

'** Hardcoded Information **
CONF="O:\Auto\Configs\DbServers.cfg"
'TargetFolder="c:\pdat" &Accid &"\" 
TargetFolder="O:\Upload\Acc\" &Accid &"\pdat" &Accid &"\"

'** Database Connections **
Call OpenDbCon(SVR, CONF)
Set resultset = CreateObject("ADODB.Recordset")
Set rs = CreateObject("ADODB.Recordset")

'** Main **
'** Create client prid and pdat tables
If Pftype="comptk" Then
  dbconn.Execute "drop table if exists client.prid" &Accid  
  dbconn.Execute "create table `client`.`prid" &Accid &"` (" &_
                 " `MIC` CHAR(4) NOT NULL," &_
                 " `SecID` INT(10) NOT NULL," &_
                 " Primary Key (`MIC`, `SecID`))" &_
                 " ENGINE=MyISAM DEFAULT CHARSET=latin1;"
  dbconn.Execute "drop table if exists client.pdat" &Accid 
  dbconn.Execute " CREATE TABLE `client`.`pdat" &Accid &"` (" &_
                 " `MIC` varchar(6) DEFAULT NULL," &_
                 " `LocalCode` varchar(80) NOT NULL DEFAULT ''," &_
                 " `Isin` varchar(12) DEFAULT NULL," &_
                 " `Currency` char(3) NOT NULL DEFAULT ''," &_
                 " `PriceDate` date DEFAULT NULL," &_
                 " `Open` varchar(20) DEFAULT NULL," &_
                 " `High` varchar(20) DEFAULT NULL," &_
                 " `Low` varchar(20) DEFAULT NULL," &_
                 " `Close` varchar(20) DEFAULT NULL," &_
                 " `Mid` varchar(20) DEFAULT NULL," &_
                 " `Ask` varchar(20) DEFAULT NULL," &_
                 " `Last` varchar(20) DEFAULT NULL," &_
                 " `Bid` varchar(20) DEFAULT NULL," &_
                 " `BidSize` varchar(20) DEFAULT NULL," &_
                 " `AskSize` varchar(20) DEFAULT NULL," &_
                 " `TradedVolume` varchar(50) DEFAULT NULL," &_
                 " `SecID` int(11) NOT NULL DEFAULT '0'," &_
                 " `MktCloseDate` date DEFAULT NULL," &_
                 " `Volflag` char(1) DEFAULT NULL," &_
                 " `Issuername` varchar(70) DEFAULT NULL," &_
                 " `SectyCD` char(3) DEFAULT NULL," &_
                 " `SecurityDesc` varchar(100) DEFAULT NULL," &_
                 " `Sedol` varchar(7) DEFAULT NULL," &_
                 " `uscode` varchar(9) DEFAULT NULL," &_
                 " `PrimaryExchgCD` varchar(6) DEFAULT NULL," &_
                 " `ExchgCD` varchar(9) NOT NULL," &_
                 " `TradedValue` varchar(50) DEFAULT NULL," &_
                 " `TotalTrades` varchar(50) DEFAULT NULL," &_
                 " `Comment` varchar(255) DEFAULT NULL," &_
                 " PRIMARY KEY (`ExchgCD`,`SecID`,`Currency`,`LocalCode`,`MktCloseDate`) USING BTREE," &_
                 " KEY `IX_LocalCode` (`MIC`,`LocalCode`)," &_
                 " KEY `IX_SECID` (`SecID`)," &_
                 " KEY `IX_Sedol` (`Sedol`)," &_
                 " KEY `IX_ISIN` (`Isin`) USING BTREE," &_
                 " KEY `IX_Localcode_exch` (`LocalCode`,`ExchgCD`)," &_
                 " KEY `IX_Uscode` (`uscode`)," &_
                 " KEY `IX_SectyCD` (`SectyCD`)" &_
                 " ) ENGINE=MyISAM DEFAULT CHARSET=latin1;"   
'**Insert MIC and SecID's for client portfolio into prid
  dbconn.Execute "insert into client.prid" &Accid &" (MIC, SecID)" &_
                 " select distinct" &_
                 " prices.lasttrade.mic," &_
                 " prices.lasttrade.secid" &_
                 " from client.pfcomptk" &_
                 " left outer join wca.bbc on client.pfcomptk.code = wca.bbc.BbgComptk" &_
                 " left outer join wca.scexh on wca.bbc.cntrycd=substring(wca.scexh.exchgcd,1,2)" &_
      	     	 "                           and wca.bbc.secid=wca.scexh.secid" &_
                 "                           and 'D'<>wca.scexh.liststatus" &_
                 "                           and 'D'<>wca.scexh.actflag" &_
                 " Left outer join prices.lasttrade on client.pfcomptk.secid = prices.lasttrade.secid" &_
                 "                                  and (prices.lasttrade.exchgcd=wca.scexh.exchgcd or (wca.bbc.cntrycd='US' and prices.lasttrade.mic='OTCB'))" &_
                 "                                  and prices.lasttrade.pricedate>date_sub(now(), interval 30 day)" &_
                 " Where" &_
                 " client.pfcomptk.accid=" &Accid &_
                 " and client.pfcomptk.actflag='I'" &_
                 " and prices.lasttrade.mic is not null"
'**Create folder on C:\ to extract P04 files into                 
  Call CreateFolders(TargetFolder)
  If GetMic=True Then
'**Extract P04 files for current MIC and CntryCD   
    Do Until resultset.EOF
      MyOffset=Offset
      Call GetYear()
      Mic=resultset.Fields(0).Value
      'MsgBox Mic 
      CntryCd=resultset.Fields(1).Value
      Call ExtractP04Files(Mic, CntryCd, MyYear, TargetFolder)
'**Read file names and dates into a disconnected recordset and sort that by date    
      Set list = CreateObject("ADOR.Recordset")
      list.Fields.Append "name", 200, 255
      list.Open
      For Each f In oFSO.GetFolder(TargetFolder).Files
      list.AddNew
      list("name").value = f.Name
      list.Update
      Next 
      list.MoveFirst
'**Loop through list of files and load into pdat table if file date is >= the min hist date required      
      Do Until list.EOF
        Call CreateTempTbl() 
        MyArray2=Split(list.Fields("name"),"_")
        fdate=Mid(MyArray2(2),7,2) & "/" & Mid(MyArray2(2),5,2) &"/" & Mid(MyArray2(2),1,4)
        fdate2=Mid(MyArray2(2),1,4) & "-" & Mid(MyArray2(2),5,2) &"-" & Mid(MyArray2(2),7,2)
        If CDate(fdate)<DateAdd("d",-Offset, Date()) Then 
          'WScript.Echo "File Date: " &fdate &" is less than min hist date. No Load" 
        Else 
          'WScript.Echo "File Date: " &fdate &" is more than or equal to min hist date. Load File" 
          dbconn.Execute "load data local infile 'O:/Upload/Acc/" &Accid &"/pdat" &Accid &"/" &list.Fields("name") &"'" &_
                         " into table client.pdat" &Accid &"_temp" &_
                         " fields terminated by '\t'" &_
                         " lines terminated by '\n'" &_
                         " ignore 2 lines"
          If rs.State = 1 Then rs.Close
          rs.Open "select min(mktclosedate) from client.pdat" &Accid &"_temp",dbconn
          mktclosedate=rs.Fields(0).Value
          rs.Close                  
          dbconn.Execute "delete from client.pdat" &Accid &"_temp where mic = 'EDI_EN'"  
          dbconn.Execute "delete from client.pdat" &Accid &"_temp where mktclosedate<>'" &fdate2 &"'"               
'**Insert into pdat table everything from pdat_temp where secid and mic are in prid table       
          dbconn.Execute "insert ignore into client.pdat" &Accid &_
                       " (MIC, LocalCode, Isin, Currency, PriceDate, Open, High, Low, Close, Mid," &_
                       " Ask, Last, Bid, BidSize, AskSize, TradedVolume, SecID, MktCloseDate," &_
                       " Volflag, Issuername, SectyCD, SecurityDesc, Sedol, uscode, PrimaryExchgCD," &_ 
                       " ExchgCD, TradedValue, TotalTrades, Comment)" &_
                       " select" &_
                       " client.pdat" &Accid &"_temp.MIC," &_ 
                       " client.pdat" &Accid &"_temp.LocalCode," &_ 
                       " client.pdat" &Accid &"_temp.Isin," &_ 
                       " client.pdat" &Accid &"_temp.Currency," &_ 
                       " client.pdat" &Accid &"_temp.PriceDate," &_ 
                       " client.pdat" &Accid &"_temp.Open," &_ 
                       " client.pdat" &Accid &"_temp.High," &_ 
                       " client.pdat" &Accid &"_temp.Low," &_ 
                       " client.pdat" &Accid &"_temp.Close," &_ 
                       " client.pdat" &Accid &"_temp.Mid," &_
                       " client.pdat" &Accid &"_temp.Ask," &_ 
                       " client.pdat" &Accid &"_temp.Last," &_ 
                       " client.pdat" &Accid &"_temp.Bid," &_ 
                       " client.pdat" &Accid &"_temp.BidSize," &_ 
                       " client.pdat" &Accid &"_temp.AskSize," &_ 
                       " client.pdat" &Accid &"_temp.TradedVolume," &_ 
                       " client.pdat" &Accid &"_temp.SecID," &_ 
                       " client.pdat" &Accid &"_temp.MktCloseDate," &_
                       " client.pdat" &Accid &"_temp.Volflag," &_ 
                       " client.pdat" &Accid &"_temp.Issuername," &_ 
                       " client.pdat" &Accid &"_temp.SectyCD," &_ 
                       " client.pdat" &Accid &"_temp.SecurityDesc," &_ 
                       " client.pdat" &Accid &"_temp.Sedol," &_ 
                       " client.pdat" &Accid &"_temp.uscode," &_ 
                       " client.pdat" &Accid &"_temp.PrimaryExchgCD," &_ 
                       " client.pdat" &Accid &"_temp.ExchgCD," &_ 
                       " client.pdat" &Accid &"_temp.TradedValue," &_ 
                       " client.pdat" &Accid &"_temp.TotalTrades," &_ 
                       " client.pdat" &Accid &"_temp.Comment" &_
                       " from client.pdat" &Accid &"_temp" &_
                       " where concat(client.pdat" &Accid &"_temp.MIC, client.pdat" &Accid &"_temp.SecID)" &_ 
                       " In (select concat(client.prid" &Accid &".MIC, client.prid" &Accid &".SecID) from client.prid" &Accid &")"
        End If
        list.MoveNext
      Loop
      oFSO.DeleteFile(TargetFolder &"*.txt")   
      list.Close
      resultset.MoveNext
    Loop   
  End if  
End if  
WScript.Quit 

'**** Sub-Routines and Functions ****      

Private Sub OpenDbCon (p_svr, p_conf)
Dim fso, f1, ts, s, uname, pword, prov, pcata, dsource, MyArray
Const ForReading = 1
prov = "MySQL ODBC 5.1 Driver"
'** Get connection details
Set fso = CreateObject("Scripting.FileSystemObject")   
Set ts = fso.OpenTextFile(p_conf, ForReading)
Do 
  s = ts.ReadLine
  MyArray = Split(s, vbTab)
  If MyArray(0) = p_svr Then
    uname = MyArray(2)
    pword = MyArray(3)
    dsource = MyArray(4) 			
    Exit do
  End if		
Loop while NOT ts.AtEndOfStream
ts.Close
'** connect to database
On Error Resume Next
Set dbconn = CreateObject("ADODB.Connection")
Dbconn.Open "Driver={"&prov &"}" &";Server=" &dsource &";" &_
" Database=client;" &_
" User="&uname &";Password=" &pword &";"
If Err.Number <> 0 Then
  MsgBox Err.Number & " " & Err.Description
End If
On Error Goto 0
End Sub

Sub CreateFolders(p_targetfolder)
If oFSO.FolderExists(p_targetfolder) then
  '** delete contents
  If oFSO.GetFolder(p_targetfolder).Files.Count >0 Then 
    oFSO.DeleteFile(p_targetfolder &"*.txt")
  End If 
Else
  '** create target folder ****
  oFSO.CreateFolder(p_targetfolder)
End If
End Sub

Sub GetYear()
MyArray=Split(DateAdd("d",-MyOffset, Date()),"/")
MyYear=MyArray(2)
End Sub 

Function GetMic()
resultset.Open "select distinct client.prid" &Accid &".mic, prices.markets.cntrycd from client.prid" &Accid &_
               " inner join prices.markets on client.prid" &Accid &".mic = prices.markets.mic", dbconn              
' If resultset.Fields(0).Value<"1" Then
If resultset.EOF Then  
  GetMic=False
'  MsgBox GetMic
  WScript.Quit
Else 
  GetMic=True
End If
End Function 

Sub ExtractP04Files(p_mic, p_cntrycd, p_myyear, p_targetfolder)
Do While CDate(p_myyear)<=Year(Now())
  InPath="Y:\FeedArch\P04\" &p_cntrycd &"\" &p_cntrycd &"_" &p_mic &"_" &p_myyear &"*.zip"
  wshshell.Run "O:\Auto\Apps\ZIP\7-zip\7z.exe -y e -o" &p_targetfolder &" " &InPath, 1, True
  MyOffset=MyOffset-365 
  Call GetYear()
Loop  
End Sub

Sub CreateTempTbl()
dbconn.Execute "drop table if exists client.pdat" &Accid &"_temp"
dbconn.Execute " CREATE TABLE `client`.`pdat" &Accid &"_temp` (" &_
" `MIC` varchar(6) DEFAULT NULL," &_
" `LocalCode` varchar(80) NOT NULL DEFAULT ''," &_
" `Isin` varchar(12) DEFAULT NULL," &_
" `Currency` char(3) NOT NULL DEFAULT ''," &_
" `PriceDate` date DEFAULT NULL," &_
" `Open` varchar(20) DEFAULT NULL," &_
" `High` varchar(20) DEFAULT NULL," &_
" `Low` varchar(20) DEFAULT NULL," &_
" `Close` varchar(20) DEFAULT NULL," &_
" `Mid` varchar(20) DEFAULT NULL," &_
" `Ask` varchar(20) DEFAULT NULL," &_
" `Last` varchar(20) DEFAULT NULL," &_
" `Bid` varchar(20) DEFAULT NULL," &_
" `BidSize` varchar(20) DEFAULT NULL," &_
" `AskSize` varchar(20) DEFAULT NULL," &_
" `TradedVolume` varchar(50) DEFAULT NULL," &_
" `SecID` int(11) NOT NULL DEFAULT '0'," &_
" `MktCloseDate` date DEFAULT NULL," &_
" `Volflag` char(1) DEFAULT NULL," &_
" `Issuername` varchar(70) DEFAULT NULL," &_
" `SectyCD` char(3) DEFAULT NULL," &_
" `SecurityDesc` varchar(100) DEFAULT NULL," &_
" `Sedol` varchar(7) DEFAULT NULL," &_
" `uscode` varchar(9) DEFAULT NULL," &_
" `PrimaryExchgCD` varchar(6) DEFAULT NULL," &_
" `ExchgCD` varchar(9) NOT NULL," &_
" `TradedValue` varchar(50) DEFAULT NULL," &_
" `TotalTrades` varchar(50) DEFAULT NULL," &_
" `Comment` varchar(255) DEFAULT NULL," &_
" `Counter` int(11) NOT NULL AUTO_INCREMENT," &_
" PRIMARY KEY (`Counter`) USING BTREE," &_
" KEY `IX_SecID` (`SecID`)" &_
" ) ENGINE=MyISAM DEFAULT CHARSET=latin1;"   
End Sub 