'******************************************************************************************************************************
'
'                                            Script to Generate Price Feeds for Athena
'
'******************************************************************************************************************************

Option Explicit

'** Variables **
Dim sMonth, sDay, sYear, sFileName, sIcr, SVR, CONF, resultset, hrType, timeHour, dbconn, oFSO, WshShell, oFS1, oFS1_File
'** Constants **
Const ForReading = 1, ForWriting = 2, ForAppending = 8, adOpenStatic = 3, adLockOptimistic = 3

'** Load Argument Data **
SVR = WScript.Arguments.Item(0)
Set oFSO = CreateObject("scripting.filesystemobject")
Set WshShell = WScript.CreateObject("WScript.Shell")

'** Hardcoded Information **
CONF="O:\Auto\Configs\DbServers.cfg"

'** Database Connections **
Call OpenDbCon(SVR, CONF)
Set resultset = CreateObject("ADODB.Recordset")

'** Build File Name
'Get Current Month
If Len(Month(Now))=1 then 
sMonth="0" & month(now)
Else
sMonth=month(now)
End If
'MsgBox sMonth
'Get Current Day		  
If Len(Day(now))=1 then 
sDay = "0" & day(Now)
Else
sDay = day(Now)
End If
'MsgBox sDay
'Get Year		
sYear = Year(Now)
'MsgBox sYear 
'Get Increment Number
'resultset.Open "select time_format(date_sub(now(), interval 8 hour), '%h')", dbconn
'resultset.Open "SELECT time_format(date_sub(now(), interval 8 hour), '%h'), HOUR( MAX(CAST(todate as datetime)) ) AS hr FROM client.feedlog ORDER BY todate DESC ;", dbconn
resultset.Open "SELECT time_format(date_sub(now(), interval 8 hour), '%h'), HOUR( MAX(CAST(todate as datetime)) ) AS hr FROM client.feedlog WHERE clientname='Athena' ORDER BY todate DESC ;", dbconn

sIcr = resultset.fields(0).value 
hrType = RIGHT("0" & resultset.fields(1).value,2) 

timeHour = RIGHT("0" & Hour(Now())-1,2)

If timeHour = "-1" then
	timeHour = "23"
End If

If Not timeHour = hrType then 
	
	''WScript.Echo "dOES nOT eXIST"
	''WScript.Quit
	
	WshShell.Run "o:\prodman\dev\prices\athena\athena.bat"   
	WshShell.Run "O:\AUTO\Apps\ifeed\v01\iFeedV01.jar -m -s Prices_DB -f o:\auto\scripts\mysql\prices\ifeed\bespoke\308_athena_icr_p04.sql -d o:\upload\acc\308\feed\", 1, True
	WshShell.Run "o:\auto\scripts\vbs\generic\ftps.vbs o:/upload/acc/308/feed/ /Upload/Acc/308/feed/YYYYMMDD.txt both Upload Athena_308 EDI_:_308_01 Athena_308", 1, True 
	
Else
	
	''WScript.Echo "Noo"
	WScript.Quit
	
End If 

WScript.Quit

Private Sub OpenDbCon (p_svr, p_conf)
Dim fso, f1, ts, s, uname, pword, prov, pcata, dsource, MyArray
Const ForReading = 1
prov = "MySQL ODBC 5.1 Driver"
'** Get connection details
Set fso = CreateObject("Scripting.FileSystemObject")   
Set ts = fso.OpenTextFile(p_conf, ForReading)
Do 
  s = ts.ReadLine
  MyArray = Split(s, vbTab)
  If MyArray(0) = p_svr Then
    uname = MyArray(2)
    pword = MyArray(3)
    dsource = MyArray(4) 			
    Exit do
  End if		
Loop while NOT ts.AtEndOfStream
ts.Close
'** connect to database
On Error Resume Next
Set dbconn = CreateObject("ADODB.Connection")
Dbconn.Open "Driver={"&prov &"}" &";Server=" &dsource &";" &_
" Database=client;" &_
" User="&uname &";Password=" &pword &";"
If Err.Number <> 0 Then
  MsgBox Err.Number & " " & Err.Description
End If
On Error Goto 0
End Sub