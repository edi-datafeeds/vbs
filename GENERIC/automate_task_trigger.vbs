
'-----------------------------------------------------------------------------------------------------------------------------------------

'Para - f7fe4f30-c7cb-4a4b-976c-24efc35fc35a OPSVR001-DR MyWCA_t680i_FTP i

'programSrc = """C:\Program Files\AutoMate BPA Server 10\AMExecute.exe"""
'svrPara = " Server=192.168.2.144 User=administrator Pwd=password"
'taskId = " TaskID={41e3d4b5-054e-49bd-8bf4-5b0321782c5d}"
'taskAgent = " AgentName=DR-ARTEMIS"
'objShell.Run programSrc & svrPara & taskId & taskAgent

'-----------------------------------------------------------------------------------------------------------------------------------------

Option explicit

' **** MAIN ****
    
' **** VARIABLES ****

Dim para

	Set para = WScript.Arguments
	
Dim para_size : para_size = para.length

'-----------------------------------------------------------------------------------------------------------------------------------------

'If para_size > 3 Then 
	
	'WScript.Echo "false"
	
'Else
   
   'WScript.Echo "true"
   
   run_automate_taks( para )
   
   'Call logToOps( para, 1 )
   
'End If

'-----------------------------------------------------------------------------------------------------------------------------------------

Function run_automate_taks( para )

	Dim objShell, programSrc, svrPara, taskId, taskAgent, sInc, sDate
	
	Dim logArray(10)
	
	Set objShell = CreateObject ("WScript.Shell")
	
	'programSrc = """C:\Program Files\AutoMate BPA Server 10\AMExecute.exe"""
	programSrc = """O:\AUTO\Apps\BPA_Automate\AutoMate10\AMExecute.exe"""
	svrPara = " Server=192.168.2.144 User=administrator Pwd=password"
	taskId = " TaskID={" & para(0) & "}"
	taskAgent = " AgentName=" & para(1)
	
	objShell.Run programSrc & svrPara & taskId & taskAgent
	
	sInc = inc_yn( para )
	
	sDate = lDate
	
	logArray(0) = programSrc & svrPara & taskId & taskAgent 
	logArray(1) = lDate & "_" & para(2) &  sInc
	logArray(2) = para(2) &  sInc 
	
	'echoMsg( logArray(0) )
	'echoMsg( logArray(1) )
	'echoMsg( logArray(2) )
	
	logToFile( logArray )
	
End Function 

'-----------------------------------------------------------------------------------------------------------------------------------------

Function echoMsg( vMsg )
	
	'WScript.Echo vbCrlf & Now & vbTab & vMsg(0) & vbCrLf
	'WScript.Echo  Now & vbTab &  vMsg
	
End Function   

'-----------------------------------------------------------------------------------------------------------------------------------------

Function lDate()
	Dim varDate
	
	varDate = Year(Now) & RIGHT( "0" & Month(Now), 2) & RIGHT( "0" & Day(Now), 2)  
	
	'echoMsg( varDate )
	
	lDate = varDate

End Function

'-----------------------------------------------------------------------------------------------------------------------------------------

Function timeUTC()
	
	Dim dateTime, varHour, vTemp

	Set dateTime = CreateObject("WbemScripting.SWbemDateTime")    
	dateTime.SetVarDate (Now())
	'echoMsg(  "Local Time:  " & dateTime )
	'WScript.echo  "UTC Time: " & dateTime.GetVarDate (false)
	
	vTemp = Split( dateTime.GetVarDate (false), " ")
	
	varHour = Split( vTemp(1), ":")
	
	'WScript.echo varHour(0)
	
	timeUTC = varHour(0)
	
End Function  

'-----------------------------------------------------------------------------------------------------------------------------------------

'Function logToOps( para, valu )
Function inc_yn( para )
	
	Dim sInc, i, varTemp
	
	sInc = ""
	
	'WScript.Echo para.length
	
	For i = 0 to para.length - 1
      	
      	varTemp =  para(i)
      	
      	If para(i) = "i" Then 
      
      		sInc = GetInc()
      		
      		'WScript.Echo para(i) & " " & i & " " & sInc
      		
    	End If  
    Next
	
	inc_yn = sInc
	
End Function 

'-----------------------------------------------------------------------------------------------------------------------------------------

Function GetInc()
	
	Dim sTimeH, varInc
	
	varInc = "_1"
	
	sTimeH = Int( timeUTC() )
	
	If sTimeH >= 17 Then 
		
		'WScript.Echo "17"
		
		varInc = "_3"
	
	ElseIf  sTimeH >= 13 Then 
		
		'WScript.Echo "13"
		
		varInc = "_2"
		
	Else 
	
		'WScript.Echo "07"
	End If 

	GetInc = varInc
	
End Function

'-----------------------------------------------------------------------------------------------------------------------------------------

Function logToFile( logDetails )
	
	Dim LogFile, Dest, contents, objFSO, objFolder, objFile, objTextFile
	
	contents = "<p><span class=amazon>Automate Trigger <br/><br/> " & " <br/> " & logDetails(0) & " </span></p><br/>"
	
	LogFile = "O:\AUTO\logs\" & logDetails(2) & "\" & logDetails(1) & ".html"
	Dest = "O:\AUTO\logs\" & logDetails(2) & "\"
	
	echoMsg( contents )
	
	echoMsg( LogFile )
	
	' Create the File System Object
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		
		' Check that the WriteDirectory folder exists
		If objFSO.FolderExists(Dest) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		 ' Create the WriteDirectory folder if folder does not exist
		   Set objFolder = objFSO.CreateFolder(Dest)
		   'WScript.Echo "Just created " & Dest
		End If
		
		'msgbox "LogFile Test: " & LogFile
		If objFSO.FileExists(LogFile) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		   Set objFile = objFSO.CreateTextFile(LogFile)
		   'Wscript.Echo "Just created " & LogFile
		End If
		
		set objFile = nothing
		set objFolder = nothing
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		Const ForAppending = 8		
		
		Set objTextFile = objFSO.OpenTextFile _
		(LogFile, ForAppending, True)
		
		
		' Writes WriteFile every time you run this VBScript
		objTextFile.WriteLine(contents)
		objTextFile.Close
	
End Function