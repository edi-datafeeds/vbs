' 24/09/2014 | YM | Ticket ID: 2014091210000014
 'Original CMDLINE: O:\AUTO\Scripts\Java\FileSpliter\FileSpliter.jar -a -s O:\datafeed\srf\665i\20140924_1.665 -o O:\datafeed\srf\665i_Split\ -k Tablename �i
 'VBS CMDLINE: O:\AUTO\Scripts\vbs\generic\FileSpliter.vbs a O:\datafeed\srf\665i\YYYYMMDD.665 O:\datafeed\srf\665i_Split\ Tablename ignr : Youness i

Option Explicit
	
Dim fso, WshShell, numArgs
Dim sFile, sPath, wPath, sPathUrl, sExt, Source, Dest, log, sInc, LogFile
Dim stTime, sTime, lYear, sMonth, sDay, sYear, sDate
Dim Splitter, sSplit, Output, Mode, RunSplitter, header, sSource
Dim sSuf, sPre, x, y, Pre, i
Dim iCMDLINE, objFSO, objFolder, objFile, objTextFile, contents

	
' **** PREPARATION ****
	
	Set WshShell = WScript.CreateObject("WScript.Shell")
    Set fso = CreateObject("Scripting.FileSystemObject")	        
	Set numArgs = WScript.Arguments
	
	'Application to run
	Splitter = "O:\AUTO\Scripts\Java\FileSpliter\FileSpliter.jar "
	
	' Reset Inc
	sInc = ""

	'Date Function
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
	
	lYear = mid(Year(Now),1,4)
	sYear = mid(Year(Now),3,4)
	sTime = (now)
	stTime = (now)

	LogFile = "O:\AUTO\logs\" & log & "\" & lYear & sMonth & sDay & "_" & log & ".html"
	Dest = "O:\AUTO\logs\" & log & "\"		

' **** Arguments

	'Application to run


	'App Run Mode - Auto/Manual
	If numArgs(0)= "a" Then
		Mode = "-a "
	Else 
		Mode = "-m "
	End If 
	
	'Source File path and name
	Source = numArgs(1)
	
	'Dest File path
	Output = numArgs(2)
	
	'Split on XXXX
	sSplit = numArgs(3)
	
	'Ignore header options "ignr" or "show"
	If numArgs(4)= "ignr" Then
		header = "-i"
	Else 
		Mode = ""
	End If 
		
	'Any Prefix or Suffix 
	Pre = numArgs(5)
	
	'Log/Taskfile
	Log = numArgs(6)
	
	If numArgs.length = 7 Then
		Call splitPre()
		Call splitSource()
		Call Prod()
	ElseIf numArgs.length = 8 Then	
		If  numArgs(7)= "i" Then
			sInc = numArgs(7)
			Call getInc()
			Call splitPre()
			Call splitSource()
			Call Prod()
		Else
			i = numArgs(7)
			Call splitPre()
			Call getDate(i)
			Call splitSource()
			Call Prod()	
			
		End If 
	End If 
	
	
	
'****************************** GET INCREMENT NUMBER BASED ON TIME ****************************************************
 	
Sub getInc()	

	sTime = Time
	sTime = int(left(sTime,2))
	'msgbox sTime
	
		if sTime >= 12 and sTime <= 16  then 
			sInc = "_2"
			log = numArgs(6) & sInc
	
		elseif 	sTime >= 17 and  sTime <= 23  then 
			sInc = "_3"
			log = numArgs(6) & sInc
		else	
			sInc = "_1"
			log = numArgs(6) & sInc
		end If	


End Sub 

'******************************************** Split Prefix-Suffix String ******************************************************	

'FORMAT ANY PREFIX OR SUFFIX APPLIED ON THE COMMAND LINE. NOTE: THE HYPHEN "-" IS ALWAYS REQUIRED ON THE COMMAND LINE BUT IS NOT USED IF NOTHING ADDED BEFORE OR AFTER.
'ONLY CHARACTURES BEFORE AND FATER THE "-" IS USED. I.E. "AL-_1" WOULD YEILD "AL20110328_1.620"

	Sub splitPre()

	  sPre = Pre
	  x = Len(Pre)
	  for y = x to 1 step -1
		if mid(Pre, y, 1) = ":" then
			sPre = mid(Pre, 1, y-1)
				'msgbox "Pre: " & sPre
			sSuf = mid(Pre, y+1, len(pre) - y)
				'msgbox "Suf: " & sSuf
			exit for
		end if
	   next
	
	end Sub
	
'******************************************* Split Source String **********************************************************	

	'Split the Source string into fullpath,full filename and ext

	Sub splitSource()
	'msgbox "Source: " & Source
	x = Len(Source)
	for y = x to 1 step -1
		if mid(Source, y, 1) = "\" or mid(Source, y, 1) = "/" then
		    sFile = mid(Source, y+1)
   		   	'MsgBox "sFile: " & sFile
   		    sPath = mid(Source, 1, y-0)
   		    wPath = Replace(sPath,"\","/")
   		    sPathUrl = "<a href=file:///" & wPath & " target=new>"& wPath &"</a>" 
			'MsgBox "sPath: " & sPath
		    exit for
		end if
		
		if mid(Source, y, 1) = "." then
			sExt = mid(Source, y-0)
			'msgbox "sExt: " & sExt
		end if
		
	next 
 		
		if sFile = "YYYYMMDD" & sExt then
			Call sPreLongDateSuf()
		end if
		
		if sFile = "YYMMDD" & sExt then
			Call sPreShortDateSuf()
		end If
	
		if sFile = "YYYY-MM-DD" & sExt then
			Call sPreLDashedDateSuf()
		End If
		
		if sFile = "YY-MM-DD" & sExt then
			Call sPreSDashedDateSuf()
		
		end If
		
		sSource = "-s " & sPath & sFile & " "
 	end Sub	
 	
'****************************** SOURCE SHORT Prefix & SUFFIX DATE **********************************************************	

	
	Sub sPreShortDateSuf()			  
	
		sFile = sPre & sYear & sMonth & sDay & sSuf & sInc & sExt
	End Sub

'******************************* SOURCE LONG PREFIX & SUFFIX DATE **********************************************************	
	
	Sub sPreLongDateSuf()
		
		sFile = sPre & lYear & sMonth & sDay & sSuf & sInc & sExt
	End Sub
	
	
'******************************* SOURCE Long DASHED PREFIX & SUFFIX DATE **********************************************************	

	Sub sPreLDashedDateSuf()
	
		sFile = sPre & lYear & "-" & sMonth & "-" & sDay & sSuf & sInc & sExt
	End Sub

'******************************* SOURCE Long DASHED PREFIX & SUFFIX DATE **********************************************************	
	
	Sub sPreSDashedDateSuf()
		
		sFile = sPre & sYear & "-" & sMonth & "-" & sDay & sSuf &  sInc & sExt
	End Sub
 	

'******* Production *****************

Sub Prod()

iCMDLINE = Splitter & Mode & sSource & "-o " & Output & " -k " & sSplit & " " & header

RunSplitter = WshShell.Run (Splitter & Mode & sSource & "-o " & Output & " -k " & sSplit & " " & header, 1, true)

If Err.Number <> 0 Then
  'MsgBox Err.Number & " " & Err.Description
  	ErrMsg = MsgBox ("Error: " & Err.Number & (Chr(13)) & " " & (Chr(10)) & Err.Description & (Chr(13)) & " " & (Chr(10)) & "What would you like to do?",5,"VBS Error Message")
	'MsgBox ErrMsg
	If ErrMsg = "4" Then
		contents ="<p><span class=note>" & sTime & " |  <b>User Retried Splitter:</b>" & sPathUrl & sFile & "</span></p><p><span class=note> <b>Command Line:</b> " & iCMDLINE & "</span></p>"
		call sAppend()
		
		'sSql = "INSERT INTO opslog(taskfile, lpath, file, status, Diff, bytesize, Seq, CMDLINE) VALUES ('" & log & "', '" & wPath &"', '" & sFile & "', 'Retried', '" & tDiff & "', '" & iByteSize & "', '" & sInc & "', '" & iCMDLINE & "')"
		'Call OpenDbCon(CONF, sSql)
		
		Call Prod()
		
	ElseIf ErrMsg = "2" Then	
		contents ="<p><span class=note>" & sTime & " |  <b>User Aborted Splitter:</b>" & sPathUrl & sFile & "</span></p><p><span class=note> <b>Command Line:</b> " & iCMDLINE & "</span></p>"
		call sAppend()
		
		'sSql = "INSERT INTO opslog(taskfile, lpath, file, status, Diff, bytesize, Seq, CMDLINE) VALUES ('" & log & "', '" & wPath &"', '" & sFile & "', 'Cancelled', '" & tDiff & "', '" & iByteSize & "', '" & sInc & "', '" & iCMDLINE & "')"
		'Call OpenDbCon(CONF, sSql)
	
	End If
Else 
	sTime = (now)
		contents ="<p><span class=note>" & sTime & " |  <b>Splitter Worked: </b>" & sPathUrl & sFile & "</span></p><p><span class=note> <b>Command Line:</b> " & iCMDLINE & "</span></p>"
	call sAppend()
		
End If

'WScript.Quit 


end Sub
 	
'******************************************************************************************************

Sub sAppend()
	LogFile = "O:\AUTO\logs\" & log & "\" & lYear & sMonth & sDay & "_" & log & ".html"
	Dest = "O:\AUTO\logs\" & log & "\"

		
		' Create the File System Object
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		
		' Check that the WriteDirectory folder exists
		If objFSO.FolderExists(Dest) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		 ' Create the WriteDirectory folder if folder does not exist
		   Set objFolder = objFSO.CreateFolder(Dest)
		   'WScript.Echo "Just created " & Dest
		End If
		
		'msgbox "LogFile Test: " & LogFile
		If objFSO.FileExists(LogFile) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		   Set objFile = objFSO.CreateTextFile(LogFile)
		   'Wscript.Echo "Just created " & LogFile
		End If
		
		set objFile = nothing
		set objFolder = nothing
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		Const ForAppending = 8		
		
		Set objTextFile = objFSO.OpenTextFile _
		(LogFile, ForAppending, True)
		
		
		' Writes WriteFile every time you run this VBScript
		objTextFile.WriteLine(contents)
		objTextFile.Close
		
		
	end sub	

 	