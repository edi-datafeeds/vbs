Option explicit

' **** MAIN ****
    
' **** VARIABLES ****

CONST ForReading = 1, ForWriting = 2, ForAppending = 8
Dim CountCheck,objRecordSet,objConnection, strConnectString

Dim Feeddate, smfDate, wca_cnt, valueHR, splitData, db_i 'declaing varibles

Dim ms_Array_Kentish(7), ms_Array_Master(7), sqlStmt 'declaring array

Dim schmaRst(3,2)
Dim schmaRst2

Dim dbConnDetail(3,2)	' Which has 4rows and 4 columns
	dbConnDetail(0,0) = "192.168.2.60" 
	dbConnDetail(0,1) = "sa"
	dbConnDetail(0,2) = "K376:lcnb"           
	dbConnDetail(1,0) = "192.168.2.63"           
	dbConnDetail(1,1) = "sa"           
	dbConnDetail(1,2) = "K376:lcnb"           
	dbConnDetail(2,0) = "109.235.145.14"             
	dbConnDetail(2,1) = "edisa77"            
	dbConnDetail(2,2) = "K376:lcnb"             
	dbConnDetail(3,0) = "185.3.164.40"             
	dbConnDetail(3,1) = "autoops"            
	dbConnDetail(3,2) = "0p3r4t10n5"
	
'iCloud_MySql	mysql	edisa77	K376:lcnb	109.235.145.14
'M2_UK	mysql	autoops	0p3r4t10n5	185.3.164.40



For db_i = 0 To UBound(dbConnDetail)
	
	schmaRst2 = mySQL( dbConnDetail(db_i,0), dbConnDetail(db_i,1), dbConnDetail(db_i,2) )

	schmaRst(db_i,0) = schmaRst2(0,0) 
	schmaRst(db_i,1) = schmaRst2(0,1) 
	schmaRst(db_i,2) = ""
	
	'echoMsg( schmaRst(0,0) )
		
Next

'-------------------------------------------------------------------------------------------------------------------------------
schmaRst(0,2) = "Diesel"
schmaRst(1,2) = "Prices"
schmaRst(2,2) = "Icloud"
schmaRst(3,2) = "Gen2"

'splitData = Split( valueHR )

'checkfile_isloaded( splitData )

'-------------------------------------------------------------------------------------------------------------------------------

testArray( schmaRst )

          
Function mySQL( v_ip, v_user, v_passw )
    
    Const adOpenStatic = 3
    Const adLockOptimistic = 3
    
    Dim dbRslt(0,2)
    
    ' i did it like this as i has trouble with connection
    Dim db_server 
    db_server = v_ip '"192.168.2.60"
    Dim db_user 
    db_user = v_user '"sa"
    Dim db_pass
    db_pass = v_passw '"K376:lcnb"
    Dim db_name
    db_name = "wca"
    
    Set objConnection = CreateObject("ADODB.Connection")
    Set objRecordSet = CreateObject("ADODB.Recordset")
    
    'strConnectString = "DRIVER={MySQL ODBC 5.3 ANSI Driver};" & "SERVER=" & db_server & ";" _
	strConnectString = "DRIVER={MySQL ODBC 5.1 Driver};" & "SERVER=" & db_server & ";" _
                    & " DATABASE=" & db_name & ";" & "UID=" & db_user & ";PWD=" & db_pass & "; OPTION=3"
                    
    
    objConnection.Open strConnectString
    
    If objConnection.State = 1 Then
        'WScript.Echo "connected MY SQL"
    Else
        MsgBox "connection failed on MY Diesel"
        'Wscript.Echo "connection failed"
    End If
 	
 	objRecordSet.Open "SELECT " & _	
 						"( insert_cnt + update_cnt + error_cnt ) AS wca_cnt, file_name " & _
                      "FROM wca.tbl_opslog  ORDER BY acttime DESC LIMIT 1" , _
                        objConnection, adOpenStatic, adLockOptimistic                    
        
    
    objRecordSet.MoveFirst
    
    'WScript.Echo objRecordSet.RecordCount
    
    Do Until objRecordset.EOF
    
        wca_cnt = objRecordset.Fields.Item("wca_cnt")
        
        dbRslt(0,0) = objRecordset.Fields.Item("wca_cnt")
        dbRslt(0,1) = objRecordset.Fields.Item("file_name")
        dbRslt(0,2) = objRecordset.Fields.Item("file_name")
    	objRecordSet.MoveNext
    Loop
    
    objRecordSet.Close
    objConnection.Close
    
    Set objRecordSet = NOTHING
    Set objConnection =  Nothing
    
    'mySQL = wca_cnt
    mySQL = dbRslt
    
    'WScript.Quit 
    
End Function

'-------------------------------------------------------------------------------------------------------------------------------

Function testArray( array1  )

	'echoMsg( "Works " )
	'Dim sFnd : sFnd = "ID"
	Dim i, j, TorF, compareValue(2), holder
	
	TorF = True
	
	holder = "The Database are not aligned please have a look :" & (Chr(13)) & " " & (Chr(10))
	
	compareValue(0) = array1(0, 0 )
	compareValue(1) = array1(0, 1 )
	compareValue(2) = array1(0, 2 )
	
	For j = 0 To UBound(array1)
		
		holder  = holder & "Count: " & array1(j, 0) & " -> DB Server: " & array1(j, 2) & _
		"   --> File Name: " & array1(j, 1) &(Chr(13)) & " " & (Chr(10)) 
		
		If CLng (compareValue(0)) <> CLng (array1(j, 0)) Then 
		
			'WScript.Echo array1(j, 0) & " " & array1(j, 1 )
			TorF = False
				
		End If
		
	Next
	
	holder = holder & "If you need to reload the tbl_opslog table - log on to db server and use the following sql statement" &_ 
					  (Chr(13)) & " " & (Chr(10)) & " DELETE FROM wca.tbl_opslog WHERE file_name = '" & compareValue(1) & "'"
	
	
	holder = holder & (Chr(13)) & " " & (Chr(10)) & " Then run the loader"
	
	If TorF = False Then
	
		echoMsg( holder )
	End If 	
	
	'echoMsg( holder )
	
End Function

'-------------------------------------------------------------------------------------------------------------------------------


Function checkfile_isloaded( valu )

	Dim splitValu, valueDate, valueTime 
	
	splitValu = valu
		
	valueDate = dateStamp( 0 )
	valueTime = timeStamp()
	
	If splitValu(0) = valueDate Then 
			
		If splitValu(1) <> CStr(valueTime) Then 	
			
			echoMsg( "SMF has not been loaded to Current time on Diesel, please investigate!!!!     last load was Date: " & splitValu(0) & " Time: " & splitValu(1) & ":00" ) 
			
		Else
		
			WScript.Quit 
		End If
		
	Else 	
		echoMsg( "SMF has not been loaded to Current Date on Diesel, please investigate!!!!     last load was Date: " & splitValu(0) & " Time: " & splitValu(1) & ":00" )
	End	If
	
End Function 


'-------------------------------------------------------------------------------------------------------------------------------

Function dateStamp( dateBack )
    
    Dim t 
    
    t = Now - dateBack
    
    dateStamp = Year(t) & _
			    Right("0" & Month(t),2) & _
			    Right("0" & Day(t),2) 
    
End Function

Function timeStamp()
	
	Dim iTime
	
	iTime = Time
	iTime = int(left(iTime,2)) 
	
	If iTime >= 12 and iTime < 18  then 
		iTime = 12
	ElseIf iTime >= 18 and  iTime <= 23  then 
		iTime = 18
	Else
		iTime = "06"
	End If 
		
	timeStamp = iTime
	
End Function 

'-------------------------------------------------------------------------------------------------------------------------------

Function echoMsg( vMsg )
	
	WScript.Echo vbCrlf & Now & vbTab & vMsg & vbCrLf
	
	'MsgBox Now & vbTab & vMsg
	
End Function           



'-------------------------------------------------------------------------------------------------------------------------------

