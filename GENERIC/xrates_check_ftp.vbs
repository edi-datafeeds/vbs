Dim fso, FTPfolder, LocalBackupfolder, LOCALfolder,FTPDir,FTPHost,FTPPass,FTPUser, NameOfFile ,ftpFolderString,strFTP,SH, stringsrc, oFile, fileSearchStr, item, item2, item3, count2, count3, count4, oShell, objFSO, Path, FormattedDate, objRE
Set fso = CreateObject("Scripting.FileSystemObject") 
Set LOCALfolder = fso.GetFolder("O:\Datafeed\Xrates\NewFeeds")
Set LocalBackupfolder = fso.GetFolder("O:\Datafeed\Xrates\dailyFeedBackup\NewFeeds")
Set SH = CreateObject("Shell.Application") 

Set oShell = CreateObject("Shell.Application")
Set objFSO = CreateObject("Scripting.FileSystemObject")



FormattedDate= Year(Now) & "-" & Right("0" & Month(Now),2) & "-" & Right("0" & Day(Now),2)

Set oFile = Fso.OpenTextFile("O:\Datafeed\Xrates\NewFeeds\error_report\RateSent" & FormattedDate & ".txt",2,True)

'MsgBox FormattedDate

If WScript.Arguments(0) = "NA" Then 
	fileSearchStr = Right("0" & Hour(Now),2)
Else
	fileSearchStr = WScript.Arguments(0)
End If


stringsrc = WScript.Arguments(1)
'MsgBox fileSearchStr

FTPUser = "opsRate"
FTPPass = "Fwlr6l8f"
FTPHost = "ftp.exchange-data.net"
FTPDir = "/" & fileSearchStr & "/" & stringsrc
strFTP = "ftp://" & FTPUser & ":" & FTPPass & "@" & FTPHost & FTPDir



'MsgBox fileSearchStr  
'MsgBox stringsrc
Set FTPfolder = SH.NameSpace(strFTP)

NameOfFile = FormattedDate & "_" & stringsrc & "_" & fileSearchStr & "00.csv"

'NameOfFile = "2015-09-23_GBP_1800.csv"

'MsgBox FormattedDate & "_" & stringsrc & "_" & fileSearchStr & "00.csv"

'MsgBox NameOfFile

count2=0
count3=0
count4=0

For Each item In FTPfolder.Items
	
	'WScript.Echo item.Name
	
    If InStr(item.Name,NameOfFile) > 0 Then
        'WScript.Echo item.Name & "  exists in the ftp folder"
        'MsgBox item.Name & "  exists in the ftp folder"
        oFile.Write item.Name & "  exists in the ftp folder" & vbCrLf & vbCrLf
        count2=1
    End If
Next



'If count2=2 Then
If count2=0 Then    
    
    For Each item2 In LOCALfolder.Files
    
        If InStr(item2.Name,NameOfFile) > 0 Then
        
            'WScript.Echo LOCALfolder & "\" & item2.Name & " exists in local folder but not in the ftp folder"
            MsgBox item2.Name & " exists in local folder but not in the ftp folder"
            oFile.Write item2.Name & " exists in local folder but not in the ftp folder"  & vbCrLf & vbCrLf
            Path = LOCALfolder & "\" & item2.Name
            FTPUpload(Path)
            count3=1
            
        End If
        
    Next
                            
    
    If count3=0 Then
        
        For Each item3 In LocalBackupfolder.Files
            If InStr(item3.Name,NameOfFile) > 0 Then
            
                MsgBox item3.Name & " exists in local backup folder but not in the ftp or local folders"
                'WScript.Echo LocalBackupfolder & "\" & item3.Name & " exists in local folder but not in the ftp folder"
                oFile.Write item3.Name & " exists in local backup folder but not in the ftp or local folders"  & vbCrLf & vbCrLf
                
                fso.CopyFile LocalBackupfolder & "\" & item3.Name, LOCALfolder & "\"
                'MsgBox "File copied to Local Folder"
                Path = LocalBackupfolder & "\" & item3.Name
                FTPUpload(Path)
                count4=1
                
            End If
        Next
        
        If count4=0 then
            MsgBox "Either the search argument was incorrect, it was being searched for in the wrong folder or the file doesn't exist",64,"The file could not be found."
            oFile.Write "There was an error" & vbCrLf & "The file could not be found." & vbCrLf & "Either the file doesn't exist or the search argument was incorrect"
        End If        
        
    End If 
End If

Set SH = Nothing
oFile.Close 
'WScript.Echo "Done"


Sub FTPUpload(path)

    Const copyType = 16
    
    waitTime = 80000
     
    If objFSO.FileExists(path) Then
    
        Set objFile = objFSO.getFile(path)
        
        strParent = objFile.ParentFolder
        
        Set objFolder = oShell.NameSpace(strParent)
        
        Set objItem = objFolder.ParseName(objFile.Name)
        
        Set objFTP = oShell.NameSpace(strFTP)
        
        'Wscript.Echo "Uploading file " & objItem.Name & " to " & strFTP
    
        objFTP.CopyHere objItem, copyType


    End If


    If Err.Number <> 0 Then
        Wscript.Echo "Error: " & Err.Description
        oFile.Write "Error: " & Err.Description
    End If

    WScript.Sleep waitTime

End Sub