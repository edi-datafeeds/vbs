
Dim fso, FTPfolder, LocalBackupfolder, LOCALfolder,FTPDir
Dim FTPHost,FTPPass,FTPUser, NameOfFile ,ftpFolderString,strFTP,SH, currency_TYP, oFile, current_HR, item, item2, item3, count2, count3, count4, oShell, objFSO, Path, FormattedDate, objRE
Dim writeToFile, contents, sTime, inc, emailObj, ftpBox

'--------------------------------------------------------------------------------------------------------

If WScript.Arguments(0) = "NA" Then 
	current_HR = Right("0" & Hour(Now),2)
Else
	current_HR = WScript.Arguments(0)
End If

currency_TYP = WScript.Arguments(1)

ftpBox = WScript.Arguments(2)

'--------------------------------------------------------------------------------------------------------

FormattedDate= Year(Now) & "-" & Right("0" & Month(Now),2) & "-" & Right("0" & Day(Now),2)

FormattedDate2= Year(Now) & Right("0" & Month(Now),2) & Right("0" & Day(Now),2)

'--------------------------------------------------------------------------------------------------------

sTime = (now)

inc = Right("0" & Hour(Now),2)

If inc = "18" Then 
	inc = "_2"
Else 
	inc = ""
End If

'--------------------------------------------------------------------------------------------------------

Set fso = CreateObject("Scripting.FileSystemObject") 

Set LOCALfolder = fso.GetFolder("O:\Datafeed\Xrates\NewFeeds\" & current_HR & "\" & currency_TYP & "\")
Set LocalBackupfolder = fso.GetFolder("O:\Datafeed\Xrates\dailyFeedBackup\NewFeeds\" & current_HR & "\" & currency_TYP & "\")
Set SH = CreateObject("Shell.Application") 

Set oShell = CreateObject("Shell.Application")
Set objFSO = CreateObject("Scripting.FileSystemObject")

Set oFile = Fso.OpenTextFile("O:\AUTO\logs\Xrates" & inc & "\" & FormattedDate2 & "_xrates" & inc & ".html",8,True)

'--------------------------------------------------------------------------------------------------------

FTPUser = "opsRate"
FTPPass = "Fwlr6l8f"
FTPHost = "ftp.exchange-data." & ftpBox
FTPDir = "/" & current_HR & "/" & currency_TYP
strFTP = "ftp://" & FTPUser & ":" & FTPPass & "@" & FTPHost & FTPDir

'--------------------------------------------------------------------------------------------------------

Set FTPfolder = SH.NameSpace(strFTP)
NameOfFile = FormattedDate & "_" & currency_TYP & "_" & current_HR & "00.csv"

'NameOfFile = "2015-09-23_GBP_1800.csv"
'MsgBox FormattedDate & "_" & currency_TYP & "_" & current_HR & "00.csv"
'MsgBox NameOfFile

'--------------------------------------------------------------------------------------------------------

count2=0
count3=0
count4=0

'--------------------------------------------------------------------------------------------------------

For Each item In FTPfolder.Items
	
	If InStr(item.Name,NameOfFile) > 0 Then
        
        'WScript.Echo item.Name & "  exists in the ftp folder"
        oFile.WriteLine "<p><span style='color:orange; font-size:15px;'>" & sTime & " | " & item.Name & "  exists on FTP box " & ftpBox & "</span></p>" & vbCrLf & vbCrLf
        count2=1
        
        email_OPS( "been upload by Automate! on FTP box " & ftpBox )
        
    End If
Next

'--------------------------------------------------------------------------------------------------------

If count2=0 Then    
    
    For Each item2 In LOCALfolder.Files
    	
        If InStr(item2.Name,NameOfFile) > 0 Then
        
            'WScript.Echo LOCALfolder & "\" & item2.Name & " exists in local folder but not in the ftp folder"
            oFile.WriteLine "<p><span class=note>" & sTime & " | " & item2.Name & " exists in local folder but not in the ftp folder </span></p>" & vbCrLf & vbCrLf
            Path = LOCALfolder & "\" & item2.Name
            FTPUpload(Path)
            count3=1
            
        End If
        
    Next

'--------------------------------------------------------------------------------------------------------                           
    
    If count3=0 Then
        
        For Each item3 In LocalBackupfolder.Files
            If InStr(item3.Name,NameOfFile) > 0 Then
            
                'WScript.Echo LocalBackupfolder & "\" & item3.Name & " exists in local backup folder but not in the ftp or local folders"
                oFile.WriteLine "<p><span class=note>" & sTime & " | " & item3.Name & " exists in local backup folder but not in the ftp or local folders </span></p>"  & vbCrLf & vbCrLf
                
                fso.CopyFile LocalBackupfolder & "\" & item3.Name, LOCALfolder & "\"
                'MsgBox "File copied to Local Folder"
                Path = LocalBackupfolder & "\" & item3.Name
                FTPUpload(Path)
                count4=1
                
            End If
        Next

'--------------------------------------------------------------------------------------------------------   
        
        If count4=0 Then
        	
        	email_OPS( " Fail, there is no file!" )
        	
            MsgBox "Either the search argument was incorrect, it was being searched for in the wrong folder or the file doesn't exist",64,"The file could not be found."
            oFile.WriteLine "<p><span class=failed> " & sTime & " | There was an error" & vbCrLf & "The file could not be found." & vbCrLf & "Either the file doesn't exist or the search argument was incorrect  </span></p>"
        End If        

'--------------------------------------------------------------------------------------------------------           
    End If 
    
End If

'--------------------------------------------------------------------------------------------------------   

Set SH = Nothing
oFile.Close 
'WScript.Echo "Done"


Sub FTPUpload(path)

    Const copyType = 16
    
    waitTime = 80000
     
    If objFSO.FileExists(path) Then
    
        Set objFile = objFSO.getFile(path)
        
        strParent = objFile.ParentFolder
        
        Set objFolder = oShell.NameSpace(strParent)
        
        Set objItem = objFolder.ParseName(objFile.Name)
        
        Set objFTP = oShell.NameSpace(strFTP)
        
        'Wscript.Echo "Uploading file " & objItem.Name & " to " & strFTP
    
        objFTP.CopyHere objItem, copyType
		
		oFile.WriteLine "<p><span class=note>" & sTime & " | " & " File is copied to FTP </span></p>"  & vbCrLf & vbCrLf
		
		email_OPS( " has been pushed up by nates script! Please check Automate" )
		
    End If


    If Err.Number <> 0 Then
        Wscript.Echo "Error: " & Err.Description
        oFile.Write "Error: " & Err.Description
    End If

    WScript.Sleep waitTime

End Sub

'--------------------------------------------------------------------------------------------------------   

'wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER ops@exchange-data.com -PASS BLOCH1 -TO a.gupta@exchange-data.com -SUB test script ")

Sub email_OPS( status )
	
	Set emailObj = Wscript.CreateObject("Wscript.Shell")
	
    'emailObj.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER ops@exchange-data.com -PASS BLOCH1 -TO a.gupta@exchange-data.com;y.migou@exchange-data.com;m.elberkati@exchange-data.com -SUB Xrates has " & status )
	emailObj.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER ops@exchange-data.com -PASS BLOCH1 -TO a.gupta@exchange-data.com;m.elberkati@exchange-data.com -SUB Xrates has " & status )
End Sub