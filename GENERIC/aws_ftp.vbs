'-------------------------------------------------------------------------

'INFORMATION PLEASE READ 

'-------------------------------------------------------------------------

'Parameter Mandatory
'v_loc = o:\test\yyyymmdd.txt 'the usual format for ops
'v_ftp = bucketName | bucketName/folder
'v_log = name of log

'Sample of para = o:/datafeed/wca/680i/ al-kumar-g/test123/YYYYMMDD.680 : My_Wca_Webload_FTP i

'full command line "c:\Program Files\S3 Browser\s3browser-con.exe" upload Ops o:/datafeed/wca/680i/ al-kumar-g/test123/YYYYMMDD.680 : My_Wca_Webload_FTP i

'Parameter Optional -  any of the following can be added after the Mandatory Para, 
'Make sure you add the following after the log name 
'i = Ops way Incremental 
': = Ops way colon
'-id? = As long as the ID is added to the s3 Browser you can overwrite the default so -idTMX or -idTEST
'-ud? =  - -uddownload, by default it's upload
'-min? = number of day(s) back  -min5
'-ap? =	-apYYYY/MM/DD | YYYY/MM | YYYY | -apYYYYMMDD | YYYYMM | YYYY-MM-DD | YYYY-MM
'Sample of para = o:/datafeed/wca/680i/ al-kumar-g/test123/YYYYMMDD.680 : My_Wca_Webload_FTP -uddownload i -idName -min1

'-------------------------------------------------------------------------

Option explicit

' **** MAIN ****
    
' **** VARIABLES ****

Dim para, db_NameConn

	Set para = WScript.Arguments
	
Dim para_size : para_size = para.length

db_NameConn = "HAT_MY_Diesel"

run_aws_ftp( para )

Function run_aws_ftp( para )

	Dim objShell, programSrc, accID, upDown, v_loc, v_ftp, v_file, v_tmp, v_colon, v_pre, v_suf, v_inc, v_log, v_localPath, v_awsPath, minValu, v_error
	
	Dim logArray(10), sqlArray(10), v_logDate, runCMD, v_dynDir
	
	Set objShell = CreateObject ("WScript.Shell")
	
	programSrc = """c:\Program Files\S3 Browser\s3browser-con.exe"""
	'programSrc = """O:\AUTO\Apps\S3_Browser\s3browser-con.exe"""
	
	v_loc = para(0)
	v_ftp = para(1)
	v_log = para(3)
	
'----------------------------------------------------------------------------	
	
	accID = overWriteID( para ) ' function to overwrite default user
	'MsgBox accID
'----------------------------------------------------------------------------	
	
	upDown = overWriteUpDown( para ) 'function to overwrite from upload to download
	
'----------------------------------------------------------------------------	
	
	v_inc = incExist( para ) 'check if its incremental or not 

'----------------------------------------------------------------------------	
	
	v_tmp = colonExist( para ) 
	
	v_pre = v_tmp(0)
	v_suf = v_tmp(1)

'----------------------------------------------------------------------------

	v_dynDir = dynamicDateDirExist( para )

'----------------------------------------------------------------------------
	
	v_localPath = localFileDir( v_loc )
	
	'MsgBox v_localPath
	minValu = minusExist( para ) 
	
	'MsgBox minValu
	
	v_file = fileFormat( v_ftp, minValu )
	
	'MsgBox v_file
	
	v_awsPath = amazonFileDir( v_ftp )
	
	'MsgBox v_ftp
'----------------------------------------------------------------------------
	
	If upDown = "download" Then 
		
		runCMD = programSrc & " " & upDown & " " & accID & " " & v_awsPath & v_dynDir & v_pre & v_file & v_suf & v_inc & Right(v_ftp, 4 )  & " " & v_localPath
			
	Else 
		
		runCMD = programSrc & " " & upDown & " " & accID & " " & v_localPath & v_pre & v_file & v_suf & v_inc & Right(v_ftp, 4 )  & " " & v_awsPath & v_dynDir
		
	End If 
	
	'WScript.Echo programSrc & " " & upDown & " " & accID & " " &  v_localPath & v_pre & v_file & v_suf & v_inc & Right(v_ftp, 4 )  & " " & v_awsPath
		
	'v_error = objShell.Run ( programSrc & " " & upDown & " " & accID & " " &  v_localPath & v_pre & v_file & v_suf & v_inc & Right(v_ftp, 4 )  & " " & v_awsPath, 1, TRUE )
	
	'WScript.Echo runCMD
	
	'WScript.Quit 
	
	v_error = objShell.Run ( runCMD, 1, TRUE )
	
	'MsgBox v_error
	
	Set objShell = Nothing
	
'----------------------------------------------------------------------------	

	v_logDate = fileFormat( "yyyymmdd", "0" )
	
	'logArray(0) = programSrc & " " & upDown & " " & accID & " " &  v_localPath & v_pre & v_file & v_suf & v_inc & Right(v_loc, 4 )  & " " & v_awsPath
	
	programSrc = """O:\AUTO\Apps\S3_Browser\s3browser-con.exe"""
	
	runCMD = programSrc & " " & upDown & " " & accID & " " & v_localPath & v_pre & v_file & v_suf & v_inc & Right(v_ftp, 4 )  & " " & v_awsPath & v_dynDir

'----------------------------------------------------------------------------	
	
	logArray(0) = runCMD
	
	logArray(1) = v_logDate & "_" & v_log &  v_inc
	
	logArray(2) = v_log &  v_inc 
	
	logToFile( logArray )

'----------------------------------------------------------------------------	
	
	sqlArray(0) = v_log &  v_inc 	'taskfile
	sqlArray(1) = "AWS-Up"			'direction
	sqlArray(2) = v_localPath		'lpath
	sqlArray(3) = v_awsPath & v_dynDir 'rpath
	sqlArray(4) = v_pre & v_file & v_suf & v_inc & Right(v_ftp, 4 ) 'file
	sqlArray(5) = v_inc				'Seq
	sqlArray(6) = runCMD			'CMDLINE
	sqlArray(7) = v_pre				'v_pre
	sqlArray(8) = v_suf				'v_suf
	sqlArray(9) = "AWS_S3"			'Server

'----------------------------------------------------------------------------		
	
	logToSQL( sqlArray )
	
'----------------------------------------------------------------------------	
		
End Function 

'---------------------------------------------------------------------------------------------------

Function fileFormat( para, minV )
	
	Dim v_tmp, v_fileDate, oRE, oRE_Match, varDate, oRE_Count, dteNow
	
	para = Replace( para, "/", "\" )
	
	Set oRE = New RegExp 
	With oRE
        .pattern="YY"
        .IgnoreCase = True
        .Global = True
    End With
	
	'---------------------------------------------------
	
	dteNow = Date()
	dteNow = DateAdd("d", minV, dteNow)
	
	dteNow = Split(dteNow, "/")
	
	'---------------------------------------------------
	
	v_tmp = Split( para, "\" )
	
	v_fileDate = v_tmp( UBound( v_tmp ) )
	
	v_tmp = Split( v_fileDate, "." )
	
	v_fileDate = v_tmp(0)
	
	'---------------------------------------------------
	
	Set oRE_Count = oRE.execute( v_fileDate )
	
	oRE_Count  = oRE_Count.count
	
	'MsgBox oRE_Count
	
	oRE_Match = oRE.Test( v_fileDate )
	
	If oRE_Match = True Then
		
		'-------------------------------------------------------------------
		'Prep date value YY-MM-DD or YYYY-MM-DD
		
		If oRE_Count = 2 Then 'count the number YY rather than YYYY,
		
			varDate = dteNow(2) & "-" & dteNow(1) & "-" & dteNow(0) 
		Else 
			
			varDate = Mid(dteNow(2),3,4) & "-" & dteNow(1) & "-" & dteNow(0) 
		End If
		
		'-------------------------------------------------------------------
			
		v_tmp = Split( v_fileDate, "-" ) 	'Split the date para, YYYY-MM-DD | YYYYMMDD, value to see if hyphen exist or not 
		
		If UBound( v_tmp ) = 2 Then 		'number of split will detemine if hyphen exist or not
		
			v_fileDate = varDate 			'If hyphen exist then assign varDate value 
			
		ElseIf UBound( v_tmp ) = 0 Then 
		
			v_fileDate = Replace( varDate, "-", "") 'If hyphen does not exist then remove hypen from varDate 
		
		Else 
		
			MsgBox "please add the conditon to the if statement!"
			
		End If 
		
		
	End If 
	
	'MsgBox varDate
		
	fileFormat = v_fileDate
	
End Function 

'---------------------------------------------------------------------------------------------------

Function localFileDir( para )
	
	Dim v_tmp, v_fileDir, oRE_Count, i 
	
	para = Replace( para, "/", "\" )
	
	v_tmp = Split( para, "\" )
	
	oRE_Count = UBound( v_tmp )
		
	v_fileDir = ""
	
	For i = 0 to oRE_Count - 1
      	
      	v_fileDir =  v_fileDir & "" & v_tmp(i) & "\"
      	
      	'MsgBox v_fileDir
    
    Next
	
	'MsgBox varDate
		
	localFileDir = v_fileDir
	
End Function 

'---------------------------------------------------------------------------------------------------

Function amazonFileDir( para )
	
	Dim v_tmp, v_fileDir, oRE_Count, i 
	
	para = Replace( para, "\", "/" )
	
	v_tmp = Split( para, "/" )
	
	oRE_Count = UBound( v_tmp )
		
	v_fileDir = ""
	
	For i = 0 to oRE_Count - 1
      	
      	v_fileDir =  v_fileDir & "" & v_tmp(i) & "/" 
      	
      	'MsgBox v_fileDir
    
    Next
	
	'MsgBox v_fileDir
		
	amazonFileDir = v_fileDir
	
End Function 

'---------------------------------------------------------------------------------------------------

Function minusExist( para )
	
	Dim minValu, i, varTemp
	
	minValu = 0
	
	'WScript.Echo para.length
	
	For i = 0 to para.length - 1
      	
      	varTemp =  para(i)
      	
      	If Left( varTemp, 4) = "-min" Then 
      		
      		minValu = Replace( varTemp, "min", "")
      		
      		'WScript.Echo para(i) & " " & i & " " & sInc
      		
    	End If  
    Next
	
	minusExist = minValu
	
End Function 

'---------------------------------------------------------------------------------------------------

Function colonExist( para )
	
	Dim oRE, bMatch, varTemp, i, varTempArr(2)
	
	varTemp = ""
	
	Set oRE = New RegExp
	
	oRE.Pattern = ":"
	
	For i = 1 to para.length - 1
      	
      	varTemp =  para(i)
      	
      	'MsgBox varTemp
      	
      	bMatch = oRE.Test( varTemp )
      	
      	If bMatch = True Then 
      		
      		'MsgBox varTemp
      		
      		varTemp = Split( varTemp, ":" ) ' get prefix and suffix
			
			varTempArr(0) = varTemp(0)
			varTempArr(1) = varTemp(1)
			
			Exit For 
			
    	End If  
    Next
	
	'MsgBox "pause"
	
	colonExist = varTempArr
	
End Function 

'---------------------------------------------------------------------------------------------------

Function dynamicDateDirExist( para )
	
	Dim v_dynValu, i, j, varTemp, v_dynDate, dteNow, v_replace, v_len
		
	v_dynValu = ""
	
	v_dynDate = ""
	
	v_replace = "N"
	
	'---------------------------------------------------
	
	dteNow = Date()
	dteNow = DateAdd("d", 0, dteNow)
	
	dteNow = Split(dteNow, "/")
	
	'---------------------------------------------------
	
	For i = 0 to para.length - 1
      	
      	varTemp =  para(i)
      	
      	If Left( varTemp, 3) = "-ap" Then 
      		
      		v_dynDate = Replace( varTemp, "-ap", "")
      		
    '---------------------------------------------------  		
    
      		varTemp = Split( v_dynDate, "/" )
    		
    		'MsgBox UBound(varTemp)
    		
    		If UBound(varTemp) = 0 Then 
    		
				varTemp = Split( v_dynDate, "-" )
    		
    			'MsgBox UBound(varTemp)
    			v_replace = "-"
    			
    			v_len = 1
    			
    			If UBound(varTemp) = 0 Then 
    		
					varTemp = Split( v_dynDate, "MM" )
	    		
	    			'MsgBox UBound(varTemp)
	    			'MsgBox varTemp(0)
	    			
	    			If UBound(varTemp) > 0 Then 
	    				
	    				v_replace = ""
	    				
	    				v_len = 0
	    				
	    				If varTemp(1) = "" Then 	
	    					
	    					varTemp(1) = "MM"
	    				
	    					'MsgBox varTemp(1) & " Empty"
	    					
	    				Else 
	    					
	    					ReDim Preserve varTemp(3)
	    					varTemp(2) = varTemp(1)
	    					varTemp(1) = "MM"
	    						
	    				End If 		
	    			End If 	 
	    			
	    		End If 	
	    		
    		End If 	
    		    		
    '---------------------------------------------------
    		
    		For j = 0 to UBound(varTemp)
    			
    			'MsgBox varTemp(j)
    			
    			If LCase( varTemp(j)) = "yyyy" Then 
    				
    				'Mid(dteNow(2),3,4) & "-" & dteNow(1) & "-" & dteNow(0) 
    				
    				v_dynValu = v_dynValu & dteNow(2) & "/" 
    				
    			ElseIf LCase( varTemp(j)) = "yy" Then 
    				
    				v_dynValu = v_dynValu & Mid(dteNow(2),3,4) & "/" 
    				
    			ElseIf LCase( varTemp(j)) = "mm" Then 
    				
    				v_dynValu = v_dynValu & dteNow(1) & "/" 
    				
    			ElseIf LCase( varTemp(j)) = "dd" Then 
    				
    				v_dynValu = v_dynValu & dteNow(0) & "/" 
    			Else
    			
    			End If 
    			
    		Next 
    		
      		Exit For 
      				
    	End If  
    Next
	
	If v_replace <> "N" Then 
	
		v_dynValu = Replace(v_dynValu, "/", v_replace)
		
		v_dynValu = Mid( v_dynValu, 1, Len(v_dynValu)- v_len ) & "/"
		
	End If 	
	
	'MsgBox v_dynValu
	
	dynamicDateDirExist = v_dynValu
	
End Function 

'-----------------------------------------------------------------------------------------------------------------------------------------

Function incExist( para )
	
	Dim sInc, i, varTemp
	
	sInc = ""
	
	'WScript.Echo para.length
	
	For i = 0 to para.length - 1
      	
      	varTemp =  para(i)
      	
      	If para(i) = "i" Then 
      
      		sInc = GetInc()
      		
      		'WScript.Echo para(i) & " " & i & " " & sInc
      		
    	End If  
    Next
	
	incExist = sInc
	
End Function 

'-----------------------------------------------------------------------------------------------------------------------------------------

Function overWriteID( para )
	
	Dim accID, i, varTemp
	
	accID = "ops_ftp"
	
	'WScript.Echo para.length
	
	For i = 0 to para.length - 1
      	
      	varTemp =  para(i)
      	
      	If Left( varTemp, 3) = "-id" Then 
      		
      		accID = Replace( varTemp, "-id", "")
      		
      		'WScript.Echo para(i) & " " & i & " " & sInc
      		
    	End If  
    Next
	
	overWriteID = accID
	
End Function 

'-----------------------------------------------------------------------------------------------------------------------------------------

Function overWriteUpDown( para )
	
	Dim v_upDown, i, varTemp
	
	v_upDown = "upload"
	
	'WScript.Echo para.length
	
	For i = 0 to para.length - 1
      	
      	varTemp =  para(i)
      	
      	If Left( varTemp, 3) = "-ud" Then 
      		
      		v_upDown = Replace( varTemp, "-ud", "")
      		
      		'WScript.Echo para(i) & " " & i & " " & sInc
      		
    	End If  
    Next
	
	overWriteUpDown = v_upDown
	
End Function 

'-----------------------------------------------------------------------------------------------------------------------------------------

Function timeUTC()
	
	Dim dateTime, varHour, vTemp

	Set dateTime = CreateObject("WbemScripting.SWbemDateTime")    
	dateTime.SetVarDate (Now())
	'echoMsg(  "Local Time:  " & dateTime )
	'WScript.echo  "UTC Time: " & dateTime.GetVarDate (false)
	
	vTemp = Split( dateTime.GetVarDate (false), " ")
	
	varHour = Split( vTemp(1), ":")
	
	'WScript.echo varHour(0)
	
	timeUTC = varHour(0)
	
End Function  

'-----------------------------------------------------------------------------------------------------------------------------------------

Function getTimeLog

	Dim hrMinSec
	
	hrMinSec =  Right( "0" & Hour( Now ), 2 ) & ":" & _
			 	Right( "0" & Minute( Now ), 2 ) & ":" & _ 
			 	Right( "0" & Second( Now ), 2 ) 
	
	getTimeLog = hrMinSec
	
End Function

'-----------------------------------------------------------------------------------------------------------------------------------------

Function GetInc()
	
	Dim sTimeH, varInc
	
	varInc = "_1"
	
	sTimeH = Int( timeUTC() )
	
	If sTimeH >= 17 Then 
		
		'WScript.Echo "17"
		
		varInc = "_3"
	
	ElseIf  sTimeH >= 13 Then 
		
		'WScript.Echo "13"
		
		varInc = "_2"
		
	ElseIf  sTimeH >= 7 Then 
		
		'WScript.Echo "13"
		
		varInc = "_1"
	
	ElseIf  sTimeH >= 3 Then 
		
		'WScript.Echo "13"
		
		varInc = "_0"
			
	Else 
	
		'WScript.Echo "07"
	End If 

	GetInc = varInc
	
End Function

'-----------------------------------------------------------------------------------------------------------------------------------------

Function logToFile( logDetails )
	
	Dim LogFile, Dest, contents, objFSO, objFolder, objFile, objTextFile
	
	Dim v_logDate, v_logTime
	
	v_logDate = fileFormat( "yyyy-mm-dd", "0" )
	
	v_logTime = getTimeLog
	
	'WScript.Echo v_logDate & " " & v_logTime & " | "
	
	'contents = "<p><span class=amazon>S3 Browser Trigger <br/><br/> " & " <br/> " & logDetails(0) & " </span></p><br/>"
	
	contents = "<p><span class=amazon>" & v_logDate & " | " & v_logTime & " | " & logDetails(0) & " </span></p><br/>"
	
	
	LogFile = "O:\AUTO\logs\" & logDetails(2) & "\" & logDetails(1) & ".html"
	Dest = "O:\AUTO\logs\" & logDetails(2) & "\"
	
	
	' Create the File System Object
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		
		' Check that the WriteDirectory folder exists
		If objFSO.FolderExists(Dest) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		 ' Create the WriteDirectory folder if folder does not exist
		   Set objFolder = objFSO.CreateFolder(Dest)
		   'WScript.Echo "Just created " & Dest
		End If
		
		'msgbox "LogFile Test: " & LogFile
		If objFSO.FileExists(LogFile) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		   Set objFile = objFSO.CreateTextFile(LogFile)
		   'Wscript.Echo "Just created " & LogFile
		End If
		
		set objFile = nothing
		set objFolder = nothing
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		Const ForAppending = 8		
		
		Set objTextFile = objFSO.OpenTextFile _
		(LogFile, ForAppending, True)
		
		
		' Writes WriteFile every time you run this VBScript
		objTextFile.WriteLine(contents)
		objTextFile.Close
	
End Function

'-------------------------------------------------------------------------------------------------------------------------------

Function logToSQL( sqlDetails )
	
	Dim dbConnDetail, insertCmd
	
	sqlDetails(2) = Replace(sqlDetails(2),"\","\\")
	sqlDetails(6) = Replace(sqlDetails(6),"\","\\")
	insertCmd = "INSERT INTO AUTOOPS.opslog(taskfile, direction, lpath, rpath, file, Seq, CMDLINE, Prefix, Suffix, Server, status ) VALUES " & _
					"('" & sqlDetails(0) & "','" & sqlDetails(1) & "', '" & sqlDetails(2) &"','" & sqlDetails(3) &"','" & sqlDetails(4) & "', '" & sqlDetails(5) & "', '" & sqlDetails(6) & "', '" & sqlDetails(7) & "', '" & sqlDetails(8) & "', 'AWS_S3', 'OK');"
	
	dbConnDetail = dbConfig_reader
	
	Call mySQL( dbConnDetail,  insertCmd )
	
End Function

'-------------------------------------------------------------------------------------------------------------------------------

Function dbConfig_reader

	Dim fso
	Dim oFile
	Dim arrline
	Dim arrItem
	Dim i, j, k
	Dim arrMain()
	Dim sFileLocation, strResults
	
'--------------------------------------------------------------------------------------------
	
	Dim db_conn()
	
	ReDim db_conn(para_size, 4)

'--------------------------------------------------------------------------------------------
	
	Const forReading = 1

	Set fso = CreateObject("Scripting.FileSystemObject")
		sFileLocation = "O:\AUTO\Configs\DbServers.cfg"
		
		Set oFile = fso.OpenTextFile(sFileLocation, forReading, False)
		
	Do While oFile.AtEndOfStream <> True
		strResults = oFile.ReadAll
	Loop
	
' Close the file
	oFile.Close
	
' Release the object from memory
	Set oFile = Nothing
	
' Return the contents of the file if not Empty
	If Trim(strResults) <> "" Then
		
		' Create an Array of the Text File
		arrline = Split(strResults, vbNewLine)
	End If
 
	For i = 0 To UBound(arrline)
		If arrline(i) = "" Then
			' checks for a blank line at the end of stream
			Exit For
		End If 
		
		ReDim Preserve arrMain(i)
		
		arrMain(i) = Split(arrline(i), vbTab)

	Next
 	
	'For j = 0 To para_size  
		
		For k = 0 To i - 1
			
			'WScript.Echo k & ",0 contains: " & arrMain(k)(0)
			
			If arrMain(k)(0) = db_NameConn Then 
				
				db_conn(j,0 ) = arrMain(k)(4)
				db_conn(j,1 ) = arrMain(k)(2)
				db_conn(j,2 ) = arrMain(k)(3)
				db_conn(j,3 ) = arrMain(k)(0)
				db_conn(j,4 ) = arrMain(k)(1)
				
				'WScript.Echo k & ",0 contains: " & db_conn(j,0 ) & " " & db_conn(j,1 ) & " " & db_conn(j,2 ) & " " & arrMain(k)(0)' & " " & arrMain(k)(4)
				
			End If 
				
		Next
	'Next
	
	dbConfig_reader = db_conn
 
End Function 

'-------------------------------------------------------------------------------------------------------------------------------

Function mySQL( dbConnDtl,  instCmd )
    
    'CONST ForReading = 1, ForWriting = 2, ForAppending = 8
	Dim CountCheck,objRecordSet,objConnection, strConnectString
   
    'WScript.Quit 
    
    Const adOpenStatic = 3
    Const adLockOptimistic = 3
    
    Dim dbRslt(0,2)
    
    ' i did it like this as i has trouble with connection
    Dim db_server 
    db_server = dbConnDtl(0,0) '"192.168.2.60"
    Dim db_user 
    db_user = dbConnDtl(0,1) '"sa"
    Dim db_pass
    db_pass = dbConnDtl(0,2) '"K376:lcnb"
    Dim db_name
    db_name = "wca"
    
    Set objConnection = CreateObject("ADODB.Connection")
    'Set objRecordSet = CreateObject("ADODB.Recordset")
    
    'strConnectString = "DRIVER={MySQL ODBC 5.3 ANSI Driver};" & "SERVER=" & db_server & ";" _
	strConnectString = "DRIVER={MySQL ODBC 5.1 Driver};" & "SERVER=" & db_server & ";" _
                    & " DATABASE=" & db_name & ";" & "UID=" & db_user & ";PWD=" & db_pass & "; OPTION=3"
                    
    
    objConnection.Open strConnectString
    
    If objConnection.State = 1 Then
        'WScript.Echo "connected MY SQL"
    Else
        MsgBox "connection failed on MY Diesel"
        'Wscript.Echo "connection failed"
    End If
 	
 	
 	
 	objConnection.execute instCmd                  

    objConnection.Close
    
    Set objConnection =  Nothing
    
    'mySQL = wca_cnt
    mySQL = dbRslt
    
    'WScript.Quit 
    
End Function

'-------------------------------------------------------------------------------------------------------------------------------
