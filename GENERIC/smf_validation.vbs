Option explicit

' **** MAIN ****
    
' **** VARIABLES ****

CONST ForReading = 1, ForWriting = 2, ForAppending = 8
Dim CountCheck,objRecordSet,objConnection, strConnectString

Dim Feeddate, smfDate, smfTime, valueHR, splitData 'declaing varibles

Dim ms_Array_Kentish(7), ms_Array_Master(7), sqlStmt 'declaring array

'-------------------------------------------------------------------------------------------------------------------------------

valueHR = mySQL()

splitData = Split( valueHR )

checkfile_isloaded( splitData )

'-------------------------------------------------------------------------------------------------------------------------------
          
Function mySQL()
    
    Const adOpenStatic = 3
    Const adLockOptimistic = 3
    
    ' i did it like this as i has trouble with connection
    Dim db_server 
    db_server = "192.168.2.60"
    Dim db_user 
    db_user = "sa"
    Dim db_pass
    db_pass = "K376:lcnb"
    Dim db_name
    db_name = "wca"
    
    Set objConnection = CreateObject("ADODB.Connection")
    Set objRecordSet = CreateObject("ADODB.Recordset")
    
    'strConnectString = "DRIVER={MySQL ODBC 5.3 ANSI Driver};" & "SERVER=" & db_server & ";" _
	strConnectString = "DRIVER={MySQL ODBC 5.1 Driver};" & "SERVER=" & db_server & ";" _
                    & " DATABASE=" & db_name & ";" & "UID=" & db_user & ";PWD=" & db_pass & "; OPTION=3"
                    
    
    objConnection.Open strConnectString
    
    If objConnection.State = 1 Then
        'WScript.Echo "connected MY SQL"
    Else
        MsgBox "connection failed on MY Diesel"
        'Wscript.Echo "connection failed"
    End If
 	
 	objRecordSet.Open "SELECT Feeddate, " & _ 
 							"SUBSTRING_INDEX(Feeddate, ' ',1) AS smfDate," & _
 							"REPLACE(SUBSTRING_INDEX(Feeddate, ':',1), '-', '') AS smfDateTime," & _
 							"SUBSTRING_INDEX(SUBSTRING_INDEX(Feeddate, ':',1), ' ',-1) AS `smfTime`" & _
                      "FROM smf4.tbl_opslog  ORDER BY acttime DESC LIMIT 1" , _
                        objConnection, adOpenStatic, adLockOptimistic                    
        
    
    objRecordSet.MoveFirst
    
    'WScript.Echo objRecordSet.RecordCount
    
    Do Until objRecordset.EOF
    
        smfTime = objRecordset.Fields.Item("smfDateTime")
    	objRecordSet.MoveNext
    Loop
    
    objRecordSet.Close
    objConnection.Close
    
    Set objRecordSet = NOTHING
    Set objConnection =  Nothing
    
    mySQL = smfTime
    
    'WScript.Quit 
    
End Function

'-------------------------------------------------------------------------------------------------------------------------------

Function checkfile_isloaded( valu )

	Dim splitValu, valueDate, valueTime 
	
	splitValu = valu
		
	valueDate = dateStamp( 0 )
	valueTime = timeStamp()
	
	'echoMsg( splitValu(0) )
	'echoMsg( splitValu(1) )
	
	'echoMsg( valueDate )
	'echoMsg( valueTime )
	
	'valueDate = "20170428"
	'valueTime = "18"
	
	If splitValu(0) = valueDate Then 
			
		If splitValu(1) <> CStr(valueTime) Then 	
			
			echoMsg( "SMF has not been loaded to Current time on Diesel, please investigate!!!!     last load was Date: " & splitValu(0) & " Time: " & splitValu(1) & ":00" ) 
			
		Else
		
			WScript.Quit 
		End If
		
	Else 	
		echoMsg( "SMF has not been loaded to Current Date on Diesel, please investigate!!!!     last load was Date: " & splitValu(0) & " Time: " & splitValu(1) & ":00" )
	End	If
	
End Function 


'-------------------------------------------------------------------------------------------------------------------------------

Function dateStamp( dateBack )
    
    Dim t 
    
    t = Now - dateBack
    
    dateStamp = Year(t) & _
			    Right("0" & Month(t),2) & _
			    Right("0" & Day(t),2) 
    
End Function

Function timeStamp()
	
	Dim iTime
	
	iTime = Time
	iTime = int(left(iTime,2)) 
	
	If iTime >= 12 and iTime < 18  then 
		iTime = 12
	ElseIf iTime >= 18 and  iTime <= 23  then 
		iTime = 18
	Else
		iTime = "06"
	End If 
		
	timeStamp = iTime
	
End Function 

'-------------------------------------------------------------------------------------------------------------------------------

Function echoMsg( vMsg )
	
	WScript.Echo vbCrlf & Now & vbTab & vMsg & vbCrlf
	
End Function           



'-------------------------------------------------------------------------------------------------------------------------------

