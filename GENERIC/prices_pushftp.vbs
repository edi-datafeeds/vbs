' ****************************************************************
' PRICES PUSHFTP.VBS
'
'     Script for pushing files to EDI ftp boxes
' 
' Arguments in order (0) Local path
'                    (1) Remote path
'                    (2) File extension
'
' ****************************************************************

' **** MAIN ****
	
' **** VARIABLES ****

	dim WshShell,sLocalPath,sRemotePath,sRemotePath1,sFileext,sOpsFtp,sDirection,sXRC,server,database 

	

' **** PREPARATION ****
	
	Set colArgs = WScript.Arguments
 
	sLocalPath = "-local " & replace(colArgs(0),"\","/") &" "
	sRemotePath = "-remote " & replace(colArgs(1),"\","/") &" "
	sRemotePath1 = "-remote /Prices/P04/ALLPrices/ "
	sFileext = "-fileext " & colArgs(2) & " "
	sOpsFtp = "J:\java\Prog\h.patel\opsftp\dist\opsftp.jar "
	sDirection = "-upload "
	sXRC = "-XCRC on "
	server = "-svr HAT_MY_Diesel4 "
	database = "-db prices "
	
	Set WshShell = WScript.CreateObject("WScript.Shell")


' **** PROCESSING ****

	
	' PUSH FILE TO DOTCOM
	sSite = "-site dotcom "
	ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & server & database, 1, true)	
	
	' PUSH FILE TO DOTCOM(to allprices folder)
	sSite = "-site dotcom "
	ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath1 & sXRC & server & database, 1, true)
	
	' PUSH FILE TO DOTNET
	sSite = "-site dotnet "
	ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & server & database, 1, true)
	
	' PUSH FILE TO DOTNET(to allprices folder)
	sSite = "-site dotnet "
	ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath1 & sXRC & server & database, 1, true)
	
' **** END MAIN **** 