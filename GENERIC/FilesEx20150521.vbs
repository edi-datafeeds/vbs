option explicit

' **** VARIABLES ****

	
	dim fso, WshShell, numArgs, sFile, sPath, sExt, sMonth, sDay, sYear, Source, Dest, x, y, Pre, sDate,iByteSize,log,LogFile,sInc,i
	Dim SVR, step1, Mode, sProd, SQL, sAction, sPre, sSuf, fs,  objFSO,objReadFile,contents,objFolder,objFile,objTextFile,lYear,inc 
	Dim stTime,sTime,tDiff,ts,intHours,intMinutes,intSeconds,sPathUrl,wPath,sSql,CONF,ForReading
	Dim objRS,objConn,test,ErrMsg,iCMDLINE
' **** PREPARATION ****

	
	Set WshShell = WScript.CreateObject("WScript.Shell")
    Set fso = CreateObject("Scripting.FileSystemObject")	        

	'Arguments
	Set numArgs = WScript.Arguments

sInc = ""
	'Config
	CONF="O:\Auto\Configs\DbServers.cfg"
	'Server
	'Date Function
	if Len(Month(Now))=1 then 
	sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
	
	lYear = mid(Year(Now),1,4)
	sYear = mid(Year(Now),3,4)
	sTime = (now)
	stTime = (now)

	'Checks to see if the correct number of paramters have been set

	if numArgs.length = 2 then 
		sAction = numArgs(0)
		log = numArgs(1)
		sTime = (now)
		iCMDLINE = "O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs " & numArgs(0)& " " & numArgs(1)
		iCMDLINE = Replace(iCMDLINE,"\","/")
		LogFile = "O:\AUTO\logs\" & log & "\" & lYear & sMonth & sDay & "_" & log & ".html"
		Dest = "O:\AUTO\logs\" & log & "\"		
		if sAction = "Finish" then
			contents = "<p><span class=note>" & sTime & " | " & sAction & " " & Log & " </span></p>"
			contents = contents & " <br/> "
		elseif 	sAction = "Start" then
			contents = "<META HTTP-EQUIV=REFRESH CONTENT=60><link rel=stylesheet href=../style.css type=text/css media=screen /><p><span class=note >" & sTime & " | " & sAction & " " & Log & "  </span></p>"
		else
			contents = "<p><span class=note>" & sTime & " | " & sAction & " " & Log & " </span></p>"
		end if
		
		call sAppend()
		sSql = "INSERT INTO opslog(taskfile, status, CMDLINE) VALUES ('" & log & "', '" & sAction & "', '" & iCMDLINE & "')"
		'Call OpenDbCon(CONF, sSql)
		wscript.Quit
	elseif numArgs.length = 3 then 
		If numArgs(2)= "i" then
			iCMDLINE = "O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs " & numArgs(0)& " " & numArgs(1)& " " & numArgs(2)
			iCMDLINE = Replace(iCMDLINE,"\","/")
			sAction = numArgs(0)
			log = numArgs(1)
			sInc = numArgs(2)
			Call getInc()
			sTime = (now)
			'contents = "<META HTTP-EQUIV=REFRESH CONTENT=60><p><span class=note>" & sTime & " | " & sAction & " " & Log & " </span></p>"
			LogFile = "O:\AUTO\logs\" & log & "\" & lYear & sMonth & sDay & "_" & log & ".html"
			Dest = "O:\AUTO\logs\" & log & "\"		
			if sAction = "Finish" then
				contents = "<p><span class=note>" & sTime & " | " & sAction & " " & Log & " </span></p>"
				contents = contents & " <br/> "
			elseif 	sAction = "Start" then
				contents = "<META HTTP-EQUIV=REFRESH CONTENT=60><link rel=stylesheet href=../style.css type=text/css media=screen /><p><span class=note >" & sTime & " | " & sAction & " " & Log & "  </span></p>"
			else
				contents = "<p><span class=note>" & sTime & " | " & sAction & " " & Log & " </span></p>"
			end If
			'MsgBox log
			call sAppend()
		sSql = "INSERT INTO opslog(taskfile, status, Seq, CMDLINE) VALUES ('" & log & "', '" & sAction & "', '" & sInc & "', '" & iCMDLINE & "')"
		'Call OpenDbCon(CONF, sSql)
			wscript.Quit
		Else
			Source = numArgs(0)	
			'Any Prefix or Suffix 
			Pre = numArgs(1)
			'Log/Taskfile
			Log = numArgs(2)
			iCMDLINE = "O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs " & numArgs(0)& " " & numArgs(1)& " " & numArgs(2)
			iCMDLINE = Replace(iCMDLINE,"\","/")
			'CALL PROCESSING FUNCTIONS
			Call splitPre()
			Call splitSource()
			Call FileExists()
	
		End If 
	ElseIf numArgs.length = 4 then
	iCMDLINE = "O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs " & numArgs(0)& " " & numArgs(1)& " " & numArgs(2)& " " & numArgs(3)
	iCMDLINE = Replace(iCMDLINE,"\","/")
			Source = numArgs(0)	
			'Any Prefix or Suffix 
			Pre = numArgs(1)
			'Log/Taskfile
			Log = numArgs(2)
		If numArgs(3)= "i" Then
			'assign variable arguments if they exist
			Call splitPre()
			Call getInc()
			Call splitSource()		
			Call FileExists()
		Else
			i = numArgs(3)
			Call splitPre()
			Call getDate(i)
			Call splitSource()		
			Call FileExists()
		End if		
		
		'wscript.quit
	
	ElseIf numArgs.length = 5 then
	iCMDLINE= "O:\AUTO\Scripts\vbs\GENERIC\test\Val.vbs " & numArgs(0)& " " & numArgs(1)& " " & numArgs(2)& " " & numArgs(3)& " " & numArgs(4)
	iCMDLINE = Replace(iCMDLINE,"\","/")
		Source = numArgs(0)	
		'Any Prefix or Suffix 
		Pre = numArgs(1)
		'Log/Taskfile
		Log = numArgs(2)
			sInc = numArgs(3)
			i = numArgs(4)
			Call getInc()
			Call getDate(i)
			Call splitPre()
			Call splitSource()
			Call FileExists()
	Else 
		msgbox "Syntax Err: Incorrect Number of Arguments"
		wscript.quit
	
	end if
	
'******************************************* Split Source String **********************************************************	
	

	'Split the Source string into fullpath,full filename and ext

	Sub splitSource()
	'msgbox "Source: " & Source
	x = Len(Source)
	for y = x to 1 step -1
		if mid(Source, y, 1) = "\" or mid(Source, y, 1) = "/" then
		    sFile = mid(Source, y+1)
   		   	'MsgBox "sFile: " & sFile
   		    sPath = mid(Source, 1, y-0)
   		    wPath = Replace(sPath,"\","/")
   		    sPathUrl = "<a href=file:///" & wPath & " target=new>"& wPath &"</a>" 
			'MsgBox "sPath: " & sPath
		    exit for
		end if
		
		if mid(Source, y, 1) = "." then
			sExt = mid(Source, y-0)
			'msgbox "sExt: " & sExt
		end if
		
	next 
 		
		if sFile = "YYYYMMDD" & sExt then
			Call sPreLongDateSuf()
		end if
		
		if sFile = "YYMMDD" & sExt then
			Call sPreShortDateSuf()
		end If
	
		if sFile = "YYYY-MM-DD" & sExt then
			Call sPreLDashedDateSuf()
		End If
		
		if sFile = "YY-MM-DD" & sExt then
			Call sPreSDashedDateSuf()
		
		end If
					 
 	end Sub	

'******************************************** Split Prefix-Suffix String ******************************************************	

'FORMAT ANY PREFIX OR SUFFIX APPLIED ON THE COMMAND LINE. NOTE: THE HYPHEN "-" IS ALWAYS REQUIRED ON THE COMMAND LINE BUT IS NOT USED IF NOTHING ADDED BEFORE OR AFTER.
'ONLY CHARACTURES BEFORE AND FATER THE "-" IS USED. I.E. "AL-_1" WOULD YEILD "AL20110328_1.620"

	Sub splitPre()

	  sPre = Pre
	  x = Len(Pre)
	  for y = x to 1 step -1
		if mid(Pre, y, 1) = ":" then
			sPre = mid(Pre, 1, y-1)
				'msgbox "Pre: " & sPre
			sSuf = mid(Pre, y+1, len(pre) - y)
				'msgbox "Suf: " & sSuf
			exit for
		end if
	   next
	
	end Sub
	
	
'****************************** GET INCREMENT NUMBER BASED ON TIME ****************************************************
 
'o:\Datafeed\Equity\620i\PreLDateSuf.620 - eod i
'Increment number based on time of day. Will need to change this if you want to run an inc out of sequence. 
'The "i" will insert the "_" and inc number i.e "_1"

Sub getInc()	

	sTime = Time
	sTime = int(left(sTime,2))
	'msgbox sTime
	
	if numArgs(1)= "123Trans" Or numArgs(1)= "123Trans_FTP" then
		if sTime >= 12 and sTime <= 14  then 
			sInc = "_1"
			log = numArgs(1) & sInc
		elseif sTime >= 17 and  sTime <= 23  then 
			sInc = "_2"
			log = numArgs(1) & sInc
		else	
			sInc = "_1"
			log = numArgs(1) & sInc
		end If
		
	ElseIf numArgs(1)= "CABTrans" Or numArgs(1)= "CABTrans_FTP" then
		if sTime >= 11 and sTime <= 13  then 
			sInc = "_1"
			log = numArgs(1) & sInc
	
		elseif 	sTime >= 14 and  sTime <= 15  then 
			sInc = "_2"
			log = numArgs(1) & sInc
		elseif 	sTime >= 16 and  sTime <= 17  then 
			sInc = "_3"
			log = numArgs(1) & sInc
		elseif 	sTime >= 18 and  sTime <= 19  then 
			sInc = "_4"
			log = numArgs(1) & sInc
		else	
			sInc = "_5"
			log = numArgs(1) & sInc
		end If
	ElseIf numArgs(1)= "SMF4" Or numArgs(1)= "SMF4_FTP" then
		if sTime >= 11 and sTime <= 16  then 
			sInc = "_1"
			log = numArgs(1) & sInc
	
		elseif 	sTime >= 17 and  sTime <= 22  then 
			sInc = "_2"
			log = numArgs(1) & sInc
		
		else	
			sInc = "_1"
			log = numArgs(1) & sInc
		end If
	Else
		if sTime >= 12 and sTime <= 16  then 
			sInc = "_2"
			If numArgs(1) = "t15022_Inc" or numArgs(1) = "2012_Nasdaq_sedol" or numArgs(1) = "t15022_Inc_FTP" or numArgs(1) = "2012_Nasdaq_sedol_FTP" Then
				
				Pre = "edi_:_153000"
				log = numArgs(1) & sInc
			ElseIf numArgs(2) = "t15022_Inc" or numArgs(2) = "2012_Nasdaq_sedol" or numArgs(2) = "2012_Nasdaq_sedol" or numArgs(2) = "2012_Nasdaq_sedol_FTP" Then
				Pre = "edi_:_153000"
				log = numArgs(2) & sInc	
				sInc = ""
			Else
				log = log & sInc	
			End if
	
		elseif 	sTime >= 17 and  sTime <= 23  then 
			sInc = "_3"
			If numArgs(1) = "t15022_Inc" or numArgs(1) = "2012_Nasdaq_sedol" or numArgs(1) = "t15022_Inc_FTP" or numArgs(1) = "2012_Nasdaq_sedol_FTP" Then
				
				Pre = "edi_:_203000"
				log = numArgs(1) & sInc
			ElseIf numArgs(2) = "t15022_Inc" or numArgs(2) = "2012_Nasdaq_sedol" or numArgs(2) = "2012_Nasdaq_sedol" or numArgs(2) = "2012_Nasdaq_sedol_FTP" Then
				Pre = "edi_:_203000"
				log = numArgs(2) & sInc	
				sInc = ""
			Else
				log = log & sInc	
			End if
		else	
			sInc = "_1"
			If numArgs(1) = "t15022_Inc" or numArgs(1) = "2012_Nasdaq_sedol" or numArgs(1) = "2012_Nasdaq_sedol" or numArgs(1) = "2012_Nasdaq_sedol_FTP" Then
				
				Pre = "edi_:_083000"
				log = numArgs(1) & sInc
			ElseIf numArgs(2) = "t15022_Inc" or numArgs(2) = "2012_Nasdaq_sedol" or numArgs(2) = "2012_Nasdaq_sedol" or numArgs(2) = "2012_Nasdaq_sedol_FTP" Then
				Pre = "edi_:_083000"
				log = numArgs(2) & sInc	
				sInc = ""
			Else
				log = log & sInc	
			End if
			
		end If	
	End If 

			Call splitPre()
End Sub 

	
'****************************** SOURCE SHORT Prefix & SUFFIX DATE **********************************************************	

	
	Sub sPreShortDateSuf()			  
	
		sFile = sPre & sYear & sMonth & sDay & sSuf & sInc & sExt
	End Sub

'******************************* SOURCE LONG PREFIX & SUFFIX DATE **********************************************************	
	
	Sub sPreLongDateSuf()
		
		sFile = sPre & lYear & sMonth & sDay & sSuf & sInc & sExt
	End Sub
	
	
'******************************* SOURCE Long DASHED PREFIX & SUFFIX DATE **********************************************************	

		Sub sPreLDashedDateSuf()
	
		sFile = sPre & lYear & "-" & sMonth & "-" & sDay & sSuf & sInc & sExt
	End Sub

'******************************* SOURCE Long DASHED PREFIX & SUFFIX DATE **********************************************************	
	
	Sub sPreSDashedDateSuf()
		
		sFile = sPre & sYear & "-" & sMonth & "-" & sDay & sSuf &  sInc & sExt
	End Sub



				
'************************ VALIDATE FILES FUNCTION *******************************************************************



'Return 0 if a file exists else -1

Sub FileExists()

	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	End If 
		
'	LogFile = "O:\AUTO\logs\" & log & "\" & lYear & "\" & sMonth & "\" & lYear & sMonth & sDay & "_" & log & ".txt"
'	Dest = "O:\AUTO\logs\" & log & "\" & lYear & "\" & sMonth & "\"
	LogFile = "O:\AUTO\logs\" & log & "\" & lYear & sMonth & sDay & "_" & log & ".html"
	Dest = "O:\AUTO\logs\" & log & "\"

  	Set fs = CreateObject("Scripting.FileSystemObject")

	sTime = (now)
	
	'th = (DateDiff("h", stTime, sTime))
	'tm = (DateDiff("n", stTime, sTime))
	ts = (DateDiff("s", stTime, sTime))
	intHours = ts \ 3600
	intMinutes = (ts Mod 3600) \ 60
	intSeconds = ts Mod 60
	tDiff =  intHours & ":" & intMinutes & ":" &  intSeconds  
	'MsgBox tDiff

	if fs.FileExists(sPath & sFile) = False then
	  	
		'msgbox sTime & " | " & sPath & sFile & " File Failed to Generate, Please Try Again"
		contents ="<p><span class=failed>" & sTime & " | " & tDiff & " | <b>Failed to Generate File:</b> " & sPathUrl & sFile & " </span></p><p><span class=failed><b>Command Line:</b> " & iCMDLINE & "</span></p>"
		call sAppend()
	
		sSql = "INSERT INTO opslog(taskfile, lpath, file, status, Diff, bytesize, Seq, CMDLINE) VALUES ('" & log & "', '" & wPath &"', '" & sFile & "', 'Failed', '" & tDiff & "', '" & iByteSize & "', '" & sInc & "', '" & iCMDLINE & "')"
		'Call OpenDbCon(CONF, sSql)

	Else 
	   	iByteSize=GetFileSize
		contents ="<p><span class=ok>" & sTime & " | " & tDiff & " | <b>Bytesize:</b> " & iByteSize & " | <b>File:</b> " & sPathUrl & sFile & " </span></p><p><span class=ok><b>Command Line:</b> " & iCMDLINE & "</span></p>"
		'msgbox sPath & sFile & " | Bytesize = " & iByteSize & " | Log File = " & LogFile & sTime
		Call sAppend()
		sSql = "INSERT INTO opslog(taskfile, lpath, file, status, Diff, bytesize, Seq, CMDLINE) VALUES ('" & log & "', '" & wPath &"', '" & sFile & "', 'OK', '" & tDiff & "', '" & iByteSize & "', '" & sInc & "', '" & iCMDLINE & "')"
		'Call OpenDbCon(CONF, sSql)
	End if

	

end Sub	


'***************

'*********************************

'Return the length of a file or -1 if it does not exist

function GetFileSize()
Dim f
  GetFileSize = -1

  Set fs = CreateObject("Scripting.FileSystemObject")

  if fs.FileExists(sPath & sFile) = True then
    set f = fs.GetFile(sPath & sFile)
    GetFileSize = f.size
  end if
  if fs.FolderExists(sPath & sFile) = True then
    set f = fs.GetFolder(sPath & sFile)
    GetFileSize = f.size
  end if

Set f = Nothing
Set fs = Nothing
end function


'********************************
    	
    	
  	'WScript.Echo sFile
  	
  		

Sub sAppend()
	LogFile = "O:\AUTO\logs\" & log & "\" & lYear & sMonth & sDay & "_" & log & ".html"
	Dest = "O:\AUTO\logs\" & log & "\"
'MsgBox Dest

'MsgBox "In Act - Append Function"



    	'*******************
    	
		'wscript.echo ("Source: " & sPath & sFile), ("Dest: " & dPath & dFile ), ("Action: " & action), ("Log" & log)
		'************
		'MsgBox sPath & sFile
		
		'***** PROCESSING *****
		
		
		'Read file contents
		
		'Display results
		'wscript.echo contents
			
		
		' Create the File System Object
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		
		' Check that the WriteDirectory folder exists
		If objFSO.FolderExists(Dest) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		 ' Create the WriteDirectory folder if folder does not exist
		   Set objFolder = objFSO.CreateFolder(Dest)
		   'WScript.Echo "Just created " & Dest
		End If
		
		'msgbox "LogFile Test: " & LogFile
		If objFSO.FileExists(LogFile) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		   Set objFile = objFSO.CreateTextFile(LogFile)
		   'Wscript.Echo "Just created " & LogFile
		End If
		
		set objFile = nothing
		set objFolder = nothing
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		Const ForAppending = 8		
		
		Set objTextFile = objFSO.OpenTextFile _
		(LogFile, ForAppending, True)
		
		
		' Writes WriteFile every time you run this VBScript
		objTextFile.WriteLine(contents)
		objTextFile.Close
		
		
	end sub	

		'*************		
			
	
			'msgbox LogFile & " Appended to " & sPath
	
	
	
	
Sub getDate(i)

sDay = datepart("w", now)

if sDay = 2 then
	i=3
	
Else
	i=i
	
end If

if Len(Month(Now-i))=1 then 
	sMonth="0" & month(now -i)
else
	sMonth=month(now -i)
end if

if Len(Day(Now-i))=1 then 
	sDay = "0" & day(Now -i)
else
	sDay = day(Now -i)
end if

sYear = Year(Now -i)

' Build filename
sDate = sYear & sMonth & sDay




'wScript.echo "Day: " & sDay	
'WScript.echo "Month: " & sMonth	
'wScript.echo "Year: " & sYear    	
'wScript.echo "Date: " & sDate	

end sub
	
	

' **** END MAIN ****


Private Sub OpenDbCon (p_conf, sSql)
Dim fso, f1, ts, s, uname, pword, prov, pcata, dsource,MyArray,SVRL,ErrMsg
SVRL = "HAT_MY_Diesel"
Const ForReading = 1
prov = "MySQL ODBC 5.1 Driver"
'** Get connection details
Set fso = CreateObject("Scripting.FileSystemObject")   
Set ts = fso.OpenTextFile(p_conf, ForReading)
Do 
  s = ts.ReadLine
  MyArray = Split(s, vbTab)
  If MyArray(0) = SVRL Then
    uname = MyArray(2)
    pword = MyArray(3)
    dsource = MyArray(4) 			
    Exit do
  End if		
Loop while NOT ts.AtEndOfStream
ts.Close
'MsgBox uname & pword & dsource

'** connect to database
On Error Resume Next


set objConn = CreateObject("ADODB.Connection")
  objConn.Open "Driver={"&prov &"};Server=" &dsource &";Database=AutoOps;UID="&uname &";PWD=" &pword &";OPTION=133121;"
  set objRS = CreateObject("ADODB.Recordset")
  objRS.Open "opslog", objConn


        objConn.Execute sSql
		set objRS = Nothing
		objConn.Close
		set objConn = Nothing



If Err.Number <> 0 Then
  'MsgBox Err.Number & " " & Err.Description
  	'ErrMsg = MsgBox ("Error: " & Err.Number & (Chr(13)) & " " & (Chr(10)) & Err.Description & (Chr(13)) & " " & (Chr(10)) & "What would you like to do?",5,"DB Log Error Message")
	'MsgBox ErrMsg
	If ErrMsg = "4" Then
		contents ="<p><span class=note>" & sTime & " | " & tDiff & " | <b>User Retried DB Log Entry:</b> " & sPathUrl & sFile & " </span></p><p><span class=note><b>Command Line:</b> " & iCMDLINE & "</span></p>"
		call sAppend()
		Call OpenDbCon (p_conf, sSql)
	ElseIf ErrMsg = "2" Then	
		contents ="<p><span class=failed>" & sTime & " | " & tDiff & " | <b>User Aborted DB Log Entry:</b> " & sPathUrl & sFile & " </span></p><p><span class=failed><b>Command Line:</b> " & iCMDLINE & "</span></p>"
		call sAppend()

	WScript.Quit(0)
	End if
End If
On Error Goto 0
'WScript.Quit
End Sub
 


