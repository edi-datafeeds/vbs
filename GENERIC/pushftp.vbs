' ****************************************************************
' GENERIC PUSHFTP.VBS
'
'     Script for pushing files to EDI ftp boxes
' 
' Arguments in order (0) Local path
'                    (1) Remote path
'                    (2) File extension
'
' ****************************************************************

' **** MAIN ****
	
' **** VARIABLES ****

	dim WshShell,sLocalPath,sRemotePath,sFileext,sOpsFtp,sDirection,sXRC,server,database,sLog 

	

' **** PREPARATION ****
	
	Set colArgs = WScript.Arguments
 
	sLocalPath = "-local " & replace(colArgs(0),"\","/") &" "
	sRemotePath = "-remote " & replace(colArgs(1),"\","/") &" "
	sFileext = "-fileext " & colArgs(2) & " "
	sOpsFtp = "O:\AUTO\Apps\opsftp\dist\opsftp.jar "
	sDirection = "-upload "
	sXRC = "-XCRC on "
	sLog = "-log o:\AUTO\logs\Ftp.log"
	
	
	Set WshShell = WScript.CreateObject("WScript.Shell")


' **** PROCESSING ****

	
	' PUSH FILE TO DOTCOM
	sSite = "-site dotcom "
	ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog)

	' PUSH FILE TO DOTNET
	sSite = "-site dotnet "
	ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)
	
	
' **** END MAIN **** 