
Option explicit


Dim objWMIService, colMonitoredEvents, objLatestEvent

Dim sPath, sComputer, sDrive, sFolders, sfileName, sfileDate

'----------------------------------------------------------------------------

sPath = "\\.\Y$\FeedProc\P04\LivePrice"
'sPath = "\\.\O$\Nate\price-vms"

sComputer = split(sPath,"\")(2)
sDrive = split(sPath,"\")(3)
sDrive = REPLACE(sDrive, "$", ":")
sFolders = split(sPath,"$")(1)
sFolders = REPLACE(sFolders, "\", "\\") & "\\"

sfileDate = dateStamp( 0 )

sfileName = Array( "US_USCOMP_" & sfileDate, "US_USCOMP_UC" & Right( sfileDate, 6) )

'----------------------------------------------------------------------------

'WScript.Echo sDrive
'WScript.Echo sFolders
'WScript.Echo sfileName

'----------------------------------------------------------------------------

MonitorPrices( sfileName )
'clientFileExist()

'----------------------------------------------------------------------------

Function MonitorPrices( vfileName )
 	
 	Dim clientFiles
 	
 	Set objWMIService = GetObject("winmgmts:\\" & sComputer & "\root\cimv2")
	
	Set colMonitoredEvents = objWMIService.ExecNotificationQuery _
		( "SELECT * FROM __InstanceOperationEvent " _
			& "WITHIN 1 WHERE Targetinstance ISA 'CIM_DataFile' " _ 
			& "AND TargetInstance.Drive='" & sDrive & "' " _ 
			& "AND TargetInstance.Path='" & sFolders & "'" )
	 
	echoMsg ( "Begin Monitoring for a Folder Change Event..." )
	
	Do
		
		Set objLatestEvent = colMonitoredEvents.NextEvent
		
		Select Case objLatestEvent.Path_.Class
	 
			Case "__InstanceCreationEvent"
				
				echoMsg ( objLatestEvent.TargetInstance.FileName & " was created"  )
				
				quitMonitoring( "N" )
				
			Case "__InstanceDeletionEvent"
				
				echoMsg ( objLatestEvent.TargetInstance.FileName & " was deleted"  )
				
				If  objLatestEvent.TargetInstance.FileName  = vfileName(0) THEN 
				
					echoMsg ( objLatestEvent.TargetInstance.FileName & " was deleted main one"   )
					
					echoMsg ( "Started 5 min wait time "   )
					
					clientFiles = Array( sfileDate & "_P04.txt", "C" )
					
					echoMsg( clientFiles(0) & " will be the file to check for!")
					
					WScript.Sleep 9*60*1000 'timer to check if the script is there 
					
					echoMsg ( "Wait timer has finish now checking file "   )
					
					clientFileExistAll( clientFiles )
					
					quitMonitoring( "Y" )
					
				ElseIf objLatestEvent.TargetInstance.FileName  = vfileName(1) THEN 
				
					echoMsg ( objLatestEvent.TargetInstance.FileName & " is the unconfirmed client needs"   )
					
					echoMsg ( "Started 9 min wait time "   )
					
					clientFiles = Array( sfileDate & "_UC_P04.txt", "UC" )
					
					echoMsg( clientFiles(0) & " will be the file to check for!")
					
					WScript.Sleep 9*60*1000 'timer to check if the script is there 
					
					echoMsg ( "Wait timer has finish now checking file "   )
					
					clientFileExistAll( clientFiles )
					
				End If 
	 
		End Select
	Loop
	 
	Set objWMIService = Nothing
	Set colMonitoredEvents = Nothing
	Set objLatestEvent = Nothing

End Function  

'----------------------------------------------------------------------------

Function clientFileExistAll( clientFiles )

	Dim clientPath, clientFile, objFS_file, objFiles,  listFiles
	Dim FSO, sRunCMD, WshShell, step1
		
	clientPath = "O:\Upload\Acc\298\feed\prices\"
	'clientPath = "O:\Upload\Acc\298\feed\prices\test\"
	clientFile =  clientFiles
	
	Set WshShell = WScript.CreateObject("WScript.Shell")
	Set FSO = CreateObject("Scripting.FileSystemObject")
	
	echoMsg ( clientFile(0) & " check if the file is there "  )
	
	echoMsg ( clientPath & clientFile(0)  )
	
	If fso.FileExists( clientPath & clientFile(0) ) Then
	    
	    echoMsg ( clientFile(0) & " File Exist no need to create"  )
	    
	Else
		
		echoMsg ( clientFile(0) & " File does not exist - trigger file task "  )
		
		If clientFile(1) = "UC" Then 
			
			'sRunCMD = "O:\AUTO\Scripts\vbs\GENERIC\Trig.vbs \Morning\vms_p04_uc_confirmed.tsk auxilliary3 ""test"" My_Wca_Webload i"
			sRunCMD = "O:\AUTO\Scripts\vbs\GENERIC\Trig.vbs \Prices\VMS\vms_p04_uc_confirmed.tsk auxilliary3 ""test"" My_Wca_Webload i"
		Else
			
			'sRunCMD = "O:\AUTO\Scripts\vbs\GENERIC\Trig.vbs \Morning\vms_p04_confirmed.tsk auxilliary3 ""test"" My_Wca_Webload i"
			sRunCMD = "O:\AUTO\Scripts\vbs\GENERIC\Trig.vbs \Prices\VMS\vms_p04_confirmed.tsk auxilliary3 ""test"" My_Wca_Webload i"
		 	
		End If  	
		
		echoMsg ( sRunCMD )
		
		step1 = WshShell.Run (sRunCMD, 1, True)
					    
	End If

End Function 

'----------------------------------------------------------------------------

Function dateStamp( dateBack )
    
    Dim t 
    
    t = Now - dateBack
    
    dateStamp = Year(t) & _
			    Right("0" & Month(t),2) & _
			    Right("0" & Day(t),2) 
    
    
'    dateStamp = Year(t) & "-" & _
'    Right("0" & Month(t),2)  & "-" & _
'    Right("0" & Day(t),2)  
    
End Function

'----------------------------------------------------------------------------

Function dateTimeStamp()
    
    Dim t 
    t = Now
    dateTimeStamp = Year(t) & "-" & _
    Right("0" & Month(t),2)  & "-" & _
    Right("0" & Day(t),2)  & "_" & _  
    Right("0" & Hour(t),2) & _
    Right("0" & Minute(t),2) '  
    
    
'    dateStamp = Year(t) & "-" & _
'    Right("0" & Month(t),2)  & "-" & _
'    Right("0" & Day(t),2)  
    
End Function

'----------------------------------------------------------------------------

Function quitMonitoring( vValue )
	
	Dim sTime, logDateTime
	
	logDateTime = dateTimeStamp()
	
	sTime = Hour(time)
	
'	If sTime > 10 Then 
		
'		echoMsg ( " - Monitoring program has ended! condition has been met"  )
	
'		WScript.Quit
		
'	ElseIf	vValue = "Y" Then 
	
'		echoMsg ( " - Monitoring program has ended! - the file should be there "  )
	
'		WScript.Quit
		
'	Else 
		'do nothing 
		
'	End If 	


	If vValue = "Y" Then 
	
		echoMsg ( " - Monitoring program has ended! - the file should be there "  )
	
		WScript.Quit
		
	Else 
		'do nothing 
		
	End If 
	
End Function 


'----------------------------------------------------------------------------

Function echoMsg( vMsg )
	
	createLog( vMsg )
	'WScript.Echo vbCrlf & Now & vbTab & vMsg & vbCrlf
	
End Function 

'----------------------------------------------------------------------------

Function createLog( vMsg )
	
	Dim sLog, fileDate, LogFile, Dest, contents, logDateTime
	Dim objFSO, objFolder, objFile, objTextFile
	
	'WScript.Echo vbCrlf & Now & vbTab & vMsg & vbCrlf
	
	sLog = "VMS_confirmed"
	
	'fileDate = dateStamp( 0 )
	fileDate = sfileDate
	
	logDateTime = dateTimeStamp()
	
	LogFile = "O:\AUTO\logs\" & sLog & "\" & fileDate & "_" & sLog & ".html"
	
	Dest = "O:\AUTO\logs\" & sLog & "\"

	contents ="<p><span class=note> <b>Last Files </b> " & logDateTime & " | " & vMsg & "</span></p>"
			
		' Create the File System Object
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		
		' Check that the WriteDirectory folder exists
		If objFSO.FolderExists(Dest) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		 ' Create the WriteDirectory folder if folder does not exist
		   Set objFolder = objFSO.CreateFolder(Dest)
		   'WScript.Echo "Just created " & Dest
		End If
		
		'msgbox "LogFile Test: " & LogFile
		If objFSO.FileExists(LogFile) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		   Set objFile = objFSO.CreateTextFile(LogFile)
		   'Wscript.Echo "Just created " & LogFile
		End If
		
		set objFile = nothing
		set objFolder = nothing
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		Const ForAppending = 8		
		
		Set objTextFile = objFSO.OpenTextFile _
		(LogFile, ForAppending, True)
		
		
		' Writes WriteFile every time you run this VBScript
		objTextFile.WriteLine(contents)
		objTextFile.Close
		
End Function 

'----------------------------------------------------------------------------

Function write2Log

End Function 

'----------------------------------------------------------------------------