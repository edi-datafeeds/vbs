' ****************************************************************
' GENERIC FileTidy.VBS
'
'     Script for Deleting Files in the Directory
' 
' Arguments in order (0) sPath: H:\folder1\
'                    (1) sFilename: Full File Name or File Extension: file1.txt or .txt
'		     (2) 	
'		     
' ****************************************************************
option explicit

' **** MAIN ****
	
' **** VARIABLES ****

	

	dim fso, WshShell, numArgs, sPath, sFullFilename, sFilename, sdate, sFileext, sMonth, sDay, sYear
	

' **** PREPARATION ****
	
	
	'Checks to see if the correct number of paramters have been set
	
	Set numArgs = WScript.Arguments
	if numArgs.length <2 then
		msgbox "Syntax:Missing Arguments"
		wscript.quit
	end if
	
	
	
	'Arguments
	
	sPath = numArgs(0)
	sFullFilename = numArgs(1)
		
	Set WshShell = WScript.CreateObject("WScript.Shell")
        Set fso = CreateObject("Scripting.FileSystemObject")	        


	'assign variable arguments if they exist
	if wscript.arguments.count > 2 then
		
		sFileext = WScript.Arguments.Item(2)
		
		'Use this paramter to get the date name of the file
		if WScript.Arguments.Item(1)="sdate" Then
		
			Call CreateSdate1()
		
		End if	
		
		
		
	end if
	
	
	

	'assign variable arguments if they exist
	if wscript.arguments.count > 3 then
				
		sFilename = WScript.Arguments.Item(1)
		sFullFilename = WScript.Arguments.Item(2)
		sFileext = WScript.Arguments.Item(3)
		
		'Use this paramter to get the date name of the file
		if WScript.Arguments.Item(2)="sdate" Then
		
			Call CreateSdate2()
		
		End if	
		
				
				
	end if
	
			
	
	Sub CreateSdate1()
		
					
		'-- Build custom output file name, ensuring that
		' both sMonth and sDay are always two digits
			  
		if Len(Month(Now))=1 then 
			sMonth="0" & month(now)
		else
			sMonth=month(now)
		end if
			  
		if Len(Day(now))=1 then 
			sDay = "0" & day(Now)
		else
			sDay = day(Now)
		end if
		
			sYear = mid(Year(Now),1,4)
		
			'-- Create the Custom filename
			 
			sFullFilename = sYear & sMonth & sDay & sFileext
		
		
			
	End Sub
	
	
	Sub CreateSdate2()
			
						
			'-- Build custom output file name, ensuring that
			' both sMonth and sDay are always two digits
				  
			if Len(Month(Now))=1 then 
				sMonth="0" & month(now)
			else
				sMonth=month(now)
			end if
				  
			if Len(Day(now))=1 then 
				sDay = "0" & day(Now)
			else
				sDay = day(Now)
			end if
			
				sYear = mid(Year(Now),1,4)
			
				'-- Create the Custom filename
				 
				sFullFilename = sFilename & sYear & sMonth & sDay & sFileext
			
			
				
	End Sub
	
	
	
' **** PROCESSING ****


	'
	if sFullFilename <> "*" then
	fso.DeleteFile(sPath & sFullFilename)
	
	else 
	
	fso.DeleteFile(sPath & "*" & sFileext)
	
	End if	
		
' **** END MAIN **** 


WScript.Quit