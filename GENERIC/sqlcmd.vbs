option explicit


'**************************************************************************
' 		 GENERIC isql.exe Script		09/12/2010
' 	
' Script for parsing the connection details from the config file
'
' Arguments in order (0) Server Name
'		     (1) SQL Script location
'		     (2) Location of Config File	
'**************************************************************************

'***** MAIN *****

'***** VARIABLES *****

	dim fso, f, cmdline, SVR, SQL, CONF, WshShell, step1, numArgs, MyArray, s, ts, f1, OUT, TSK, STP,sMonth
	Dim sDay,lYear,sYear,sTime,LogFile,Dest,contents, objFSO,objFolder,objFile,objTextFile,sInc 
	CONST ForReading = 1, ForWriting = 2, ForAppending = 8


'***** Parsed Arguments *****


	numArgs = WScript.Arguments.Count

	SVR = WScript.Arguments.Item(0)
	SQL = WScript.Arguments.Item(1)
	CONF = "o:\auto\configs\dbservers.cfg "
	TSK = WScript.Arguments.Item(2)
	STP = WScript.Arguments.Item(3)


	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		lYear = mid(Year(Now),1,4)
		sYear = mid(Year(Now),1,4)
		sTime = (now)
	
	if WScript.Arguments.length = 5 then 
		sInc = WScript.Arguments.Item(4)
		Call getInc() 
	End If 
	
'****************************** GET INCREMENT NUMBER BASED ON TIME ****************************************************
 
'o:\Datafeed\Equity\620i\PreLDateSuf.620 - eod i
'Increment number based on time of day. Will need to change this if you want to run an inc out of sequence. 
'The "i" will insert the "_" and inc number i.e "_1"

Sub getInc()	

	sTime = Time
	sTime = int(left(sTime,2))
	'msgbox sTime
	
	if TSK = "123Trans" then
		if sTime >= 12 and sTime <= 14  then 
			sInc = "_1"
			
		elseif sTime >= 17 and  sTime <= 23  then 
			sInc = "_2"
			
		else	
			sInc = "_1"
		end if
	ElseIf TSK = "CABTrans" then
		if sTime >= 11 and sTime <= 13  then 
			sInc = "_1"
	
		elseif 	sTime >= 14 and  sTime <= 15  then 
			sInc = "_2"
		elseif 	sTime >= 16 and  sTime <= 17  then 
			sInc = "_3"
		elseif 	sTime >= 18 and  sTime <= 19  then 
			sInc = "_4"
		else	
			sInc = "_5"
		end If	
	Else
		if sTime >= 12 and sTime <= 16  then 
			sInc = "_2"
	
		elseif 	sTime >= 17 and  sTime <= 21  then 
			sInc = "_3"
		else	
			sInc = "_1"
		end If	
	End If 
sTime = (now)

End Sub 	
	
	
	LogFile = "O:\AUTO\logs\" & TSK & sInc & "\" & sYear & sMonth & sDay & "_" & TSK & sInc & ".html"
	Dest = "O:\AUTO\logs\" & TSK & sInc & "\"

'## Call ReadConfig ##   
  
ReadConfig

'## 
Sub ReadConfig
	Dim fso, f1, ts, s
	Const ForReading = 1
	Set fso = CreateObject("Scripting.FileSystemObject")   

	' Read the contents of the file.
	' Response.Write "Reading file <br>"

	Set ts = fso.OpenTextFile(CONF, ForReading)

	do 
		s = ts.ReadLine
		
		MyArray = Split(s, vbTab)	
		'MyArray = 
		
		'msgbox "|" & MyArray(0) & "|"
		
		if MyArray(0) = SVR Then 			
			cmdline = s			
			exit do

		end if
				
	loop while NOT ts.AtEndOfStream
	cmdline = "-S " & MyArray(4) & " -U " & MyArray(2) & " -P " & MyArray(3) 
	'msgbox cmdline
	ts.Close

End Sub 

'cmdline = "-S 10.200.2.3 -U sa -P K376:lcnb"


'***** Processing *****

Set WshShell = WScript.CreateObject("WScript.Shell")
'msgbox "O:\AUTO\Apps\exe\isql.exe " & cmdline & " -i " & SQL & " -o " & TSK
'msgbox "O:\AUTO\Apps\exe\isql.exe " & cmdline & " -i " & SQL
step1=WshShell.Run ("sqlcmd " & cmdline & " -i " & SQL, 1, true)
'sqlcmd -U sa -P K376:lcnb -S 89.187.67.59 -Q "delete from IPO.dbo.IPO"

if step1 = "0" Then
 	contents = "<p><span class=ok>" & sTime & " | Generated Sucessfully: " & STP & " | isql to " & SVR & "</span></p>"
	call sAppend()
	'msgbox "isql Table Generated"	
Else
 	contents ="<p><span class=failed>" & sTime & " | Failed to Generate: " & STP & " | isql to " & SVR & "</span></p>"
	Call sAppend()	
	msgbox STP & " sqlcmd Table Failed"
	
end if	



Sub sAppend()


'MsgBox "In Act - Append Function"



    	'*******************
    	
		'wscript.echo ("Source: " & sPath & sFile), ("Dest: " & dPath & dFile )
		'************
		'MsgBox sPath & sFile
		
		'***** PROCESSING *****
		
		
		'Read file contents
		
		'Display results
		'wscript.echo contents
			
		
		' Create the File System Object
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		
		' Check that the WriteDirectory folder exists
		If objFSO.FolderExists(Dest) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		 ' Create the WriteDirectory folder if folder does not exist
		   Set objFolder = objFSO.CreateFolder(Dest)
		   'WScript.Echo "Just created " & Dest
		End If
		'msgbox "LogFile Test: " & LogFile
		If objFSO.FileExists(LogFile) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		   Set objFile = objFSO.CreateTextFile(LogFile)
		   'Wscript.Echo "Just created " & LogFile
		End If
		
		set objFile = nothing
		set objFolder = nothing
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		Const ForAppending = 8		
		
		Set objTextFile = objFSO.OpenTextFile _
		(LogFile, ForAppending, True)
		
		
		' Writes WriteFile every time you run this VBScript
		objTextFile.WriteLine(contents)
		objTextFile.Close
		
		
	end sub	