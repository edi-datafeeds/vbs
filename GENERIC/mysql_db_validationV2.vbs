Option explicit

' **** MAIN ****
    
' **** VARIABLES ****

	CONST ForReading = 1, ForWriting = 2, ForAppending = 8
	Dim CountCheck,objRecordSet,objConnection, strConnectString
	
	Dim Feeddate, smfDate, wca_cnt, valueHR, splitData, db_i 'declaing varibles
	
	Dim schmaRst(3,2)
	
	Dim schmaRst2

	Dim numArgs
	Set numArgs = WScript.Arguments
	
	Dim para_size : para_size = numArgs.length
	
	Dim dbConnDetail, inc 
	
	dbConnDetail = dbConfig_reader
	
	
'	For inc = 0 To para_size - 1
	
'		WScript.Echo inc & ",0 contains: " & dbConnDetail(inc,0 ) & " " & dbConnDetail(inc,1 ) & " " & dbConnDetail(inc,2 ) & " " & dbConnDetail(inc,3 )
'	Next

For db_i = 0 To para_size - 1
	
	schmaRst2 = mySQL( dbConnDetail(db_i,0), dbConnDetail(db_i,1), dbConnDetail(db_i,2) )

	schmaRst(db_i,0) = schmaRst2(0,0) 
	schmaRst(db_i,1) = schmaRst2(0,1) 
	schmaRst(db_i,2) = dbConnDetail(db_i,3 )
	
	'echoMsg( schmaRst(0,0) )
		
Next

'WScript.Quit

'-------------------------------------------------------------------------------------------------------------------------------


'splitData = Split( valueHR )

'checkfile_isloaded( splitData )

'-------------------------------------------------------------------------------------------------------------------------------

testArray( schmaRst )

          
Function mySQL( v_ip, v_user, v_passw )
    
    'WScript.Echo inc & ",0 contains: " & v_ip & " " & v_user & " " & v_passw
    
    Const adOpenStatic = 3
    Const adLockOptimistic = 3
    
    Dim dbRslt(0,2)
    
    ' i did it like this as i has trouble with connection
    Dim db_server 
    db_server = v_ip '"192.168.2.60"
    Dim db_user 
    db_user = v_user '"sa"
    Dim db_pass
    db_pass = v_passw '"K376:lcnb"
    Dim db_name
    db_name = "wca"
    
    Set objConnection = CreateObject("ADODB.Connection")
    Set objRecordSet = CreateObject("ADODB.Recordset")
    
    'strConnectString = "DRIVER={MySQL ODBC 5.3 ANSI Driver};" & "SERVER=" & db_server & ";" _
	strConnectString = "DRIVER={MySQL ODBC 5.1 Driver};" & "SERVER=" & db_server & ";" _
                    & " DATABASE=" & db_name & ";" & "UID=" & db_user & ";PWD=" & db_pass & "; OPTION=3"
                    
    
    objConnection.Open strConnectString
    
    If objConnection.State = 1 Then
        'WScript.Echo "connected MY SQL"
    Else
        MsgBox "connection failed on MY Diesel"
        'Wscript.Echo "connection failed"
    End If
 	
 	objRecordSet.Open "SELECT " & _	
 						"( insert_cnt + update_cnt + error_cnt ) AS wca_cnt, file_name " & _
                      "FROM wca.tbl_opslog  ORDER BY acttime DESC LIMIT 1" , _
                        objConnection, adOpenStatic, adLockOptimistic                    
        
    
    objRecordSet.MoveFirst
    
    'WScript.Echo objRecordSet.RecordCount
    
    Do Until objRecordset.EOF
    
        wca_cnt = objRecordset.Fields.Item("wca_cnt")
        
        dbRslt(0,0) = objRecordset.Fields.Item("wca_cnt")
        dbRslt(0,1) = objRecordset.Fields.Item("file_name")
        dbRslt(0,2) = objRecordset.Fields.Item("file_name")
    	objRecordSet.MoveNext
    Loop
    
    objRecordSet.Close
    objConnection.Close
    
    Set objRecordSet = NOTHING
    Set objConnection =  Nothing
    
    'mySQL = wca_cnt
    mySQL = dbRslt
    
    'WScript.Quit 
    
End Function

'-------------------------------------------------------------------------------------------------------------------------------

Function testArray( array1  )

	'echoMsg( "Works " )
	'Dim sFnd : sFnd = "ID"
	Dim i, j, TorF, compareValue(2), holder
	
	TorF = True
	
	holder = "The Database are not aligned please have a look :" & (Chr(13)) & " " & (Chr(10))
	
	compareValue(0) = array1(0, 0 )
	compareValue(1) = array1(0, 1 )
	compareValue(2) = array1(0, 2 )
	
	For j = 0 To UBound(array1)
	'For j = 0 To 2
		
		holder  = holder & "Count: " & array1(j, 0) & " -> DB Server: " & array1(j, 2) & _
		"   --> File Name: " & array1(j, 1) &(Chr(13)) & " " & (Chr(10)) 
		
		If CLng (compareValue(0)) <> CLng (array1(j, 0)) Then 
		
			'WScript.Echo array1(j, 0) & " " & array1(j, 1 )
			TorF = False
				
		End If
		
	Next
	
	holder = holder & "If you need to reload the tbl_opslog table - log on to db server and use the following sql statement" &_ 
					  (Chr(13)) & " " & (Chr(10)) & " DELETE FROM wca.tbl_opslog WHERE file_name = '" & compareValue(1) & "'"
	
	
	holder = holder & (Chr(13)) & " " & (Chr(10)) & " Then run the loader"
	
	If TorF = False Then
	
		echoMsg( holder )
	End If 	
	
	'echoMsg( holder )
	
End Function

'-------------------------------------------------------------------------------------------------------------------------------


Function checkfile_isloaded( valu )

	Dim splitValu, valueDate, valueTime 
	
	splitValu = valu
		
	valueDate = dateStamp( 0 )
	valueTime = timeStamp()
	
	If splitValu(0) = valueDate Then 
			
		If splitValu(1) <> CStr(valueTime) Then 	
			
			echoMsg( "SMF has not been loaded to Current time on Diesel, please investigate!!!!     last load was Date: " & splitValu(0) & " Time: " & splitValu(1) & ":00" ) 
			
		Else
		
			WScript.Quit 
		End If
		
	Else 	
		echoMsg( "SMF has not been loaded to Current Date on Diesel, please investigate!!!!     last load was Date: " & splitValu(0) & " Time: " & splitValu(1) & ":00" )
	End	If
	
End Function 


'-------------------------------------------------------------------------------------------------------------------------------

Function dateStamp( dateBack )
    
    Dim t 
    
    t = Now - dateBack
    
    dateStamp = Year(t) & _
			    Right("0" & Month(t),2) & _
			    Right("0" & Day(t),2) 
    
End Function

Function timeStamp()
	
	Dim iTime
	
	iTime = Time
	iTime = int(left(iTime,2)) 
	
	If iTime >= 12 and iTime < 18  then 
		iTime = 12
	ElseIf iTime >= 18 and  iTime <= 23  then 
		iTime = 18
	Else
		iTime = "06"
	End If 
		
	timeStamp = iTime
	
End Function 

'-------------------------------------------------------------------------------------------------------------------------------

Function echoMsg( vMsg )
	
	WScript.Echo vbCrlf & Now & vbTab & vMsg & vbCrLf
	
	'MsgBox Now & vbTab & vMsg
	
End Function           

'-------------------------------------------------------------------------------------------------------------------------------

Function dbConfig_reader

	Dim fso
	Dim oFile
	Dim arrline
	Dim arrItem
	Dim i, j, k
	Dim arrMain()
	Dim sFileLocation, strResults
	
'--------------------------------------------------------------------------------------------

	Dim para_valu()
	
	ReDim para_valu( para_size )
	
	For k = 0 To para_size - 1
		
		para_valu(k) = numArgs(k)
		
	Next
	
'--------------------------------------------------------------------------------------------
	
	Dim db_conn()
	
	ReDim db_conn(para_size, 4)

'--------------------------------------------------------------------------------------------
	
	Const forReading = 1

	Set fso = CreateObject("Scripting.FileSystemObject")
		sFileLocation = "O:\AUTO\Configs\DbServers.cfg"
		
		Set oFile = fso.OpenTextFile(sFileLocation, forReading, False)
		
	Do While oFile.AtEndOfStream <> True
		strResults = oFile.ReadAll
	Loop
	
' Close the file
	oFile.Close
	
' Release the object from memory
	Set oFile = Nothing
	
' Return the contents of the file if not Empty
	If Trim(strResults) <> "" Then
		
		' Create an Array of the Text File
		arrline = Split(strResults, vbNewLine)
	End If
 
	For i = 0 To UBound(arrline)
		If arrline(i) = "" Then
			' checks for a blank line at the end of stream
			Exit For
		End If 
		
		ReDim Preserve arrMain(i)
		
		arrMain(i) = Split(arrline(i), vbTab)

	Next
 	
	For j = 0 To para_size  
		
		For k = 0 To i - 1
			
			If arrMain(k)(0) = para_valu(j) Then 
				
				db_conn(j,0 ) = arrMain(k)(4)
				db_conn(j,1 ) = arrMain(k)(2)
				db_conn(j,2 ) = arrMain(k)(3)
				db_conn(j,3 ) = arrMain(k)(0)
				db_conn(j,4 ) = arrMain(k)(1)
				
				'WScript.Echo k & ",0 contains: " & db_conn(j,0 ) & " " & db_conn(j,1 ) & " " & db_conn(j,2 ) '& " " & arrMain(k)(3) & " " & arrMain(k)(4)
				
			End If 
				
		Next
	Next
	
	dbConfig_reader = db_conn
 
End Function 
