
Dim sformatted,sCoded,sChecked

Const FOR_READING = 1
Set WshShell = WScript.CreateObject("WScript.Shell")

Set objFSO = CreateObject("Scripting.FileSystemObject")
Set fso = CreateObject("Scripting.FileSystemObject")
	CONF="O:\Auto\Configs\DbServers.cfg"

Set numArgs = WScript.Arguments


	if Len(Month(Now))=1 then 
	sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
	
	lYear = mid(Year(Now),1,4)
	sYear = mid(Year(Now),3,4)
	sTime = Time()
	stTime = Time()
	TheDate = Date()
	TheTime = Time()
	sFolder = numArgs(0)	
	strFolder = "C:/Users/y.migou.WS124B/Desktop/" & sFolder
	x = ""
	'Report = "\\192.168.2.163\EcoData\Africa\Import\Reports\"  & lYear & "-" & sMonth & "-" & sDay & "_" & sFolder & x & ".xls"
	rPath = "C:\Users\y.migou.WS124B\Desktop\" & numArgs(1)
'	rFile = lYear & "-" & sMonth & "-" & sDay & "_" & x & ".xls"
	rFile = lYear & "-" & sMonth & "-" & sDay & "_" & sFolder & x & ".xls"
	rLog = rPath & "\" & rFile
	'MsgBox rLog

Set objFolder = objFSO.GetFolder(strFolder)
sStatus = ""
Call sAppend(objFolder.Path,n,m,sStatus)
'WScript.Echo objFolder.Path

Set colFiles = objFolder.Files

For Each objFile In colFiles
		sSource = objFolder.Path & "\" &objFile.Name
		sSource = Replace(sSource,"\","/")
		'MsgBox sSource
		sSql = "load data local infile '" & sSource & "' into table afed_data.list_series_value fields terminated by '\t'  lines terminated by '\n' (lbl_id,cntrycd,source_indicator_name,effective_date,sc_id,unt_id,Value,src_id,sbsrc_id,frq_id,vt_id,submitBy,Actflag,Base,Notes,type_cd,codedBy,checkedBy,Dist_ID,period_id,original_file_id,Publication_date);"		
		'MsgBox sSql
		Call OpenDbCon(CONF,sSql)
		Call sAppend(objFolder.Path,objFile.Name,objFile.DateLastModified,sStatus)
	  	'WScript.Echo objFile.Path

Next

fCount = colFiles.Count
Call sAppendCnt(objFolder.Path,fCount)
Call ShowSubFolders(objFolder)
call RenameSheets()
 

Sub ShowSubFolders(objFolder)

  Set colFolders = objFolder.SubFolders
	'fCount = colFiles.Count
	'Call sAppendCnt(fCount)

  For Each objSubFolder In colFolders

   ' WScript.Echo objSubFolder.Path
    Call sAppend(objSubFolder.Path,n,m,sStatus)

    Set colFiles = objSubFolder.Files
	
    For Each objFile In colFiles
		sSource = objSubFolder.Path & "\" &objFile.name
		'MsgBox sSource
		sSql = "load data local infile '" & sSource & "' into table list_series_value fields terminated by '\t' enclosed by '" & chr(34) & "' lines terminated by '\n' (lbl_id,cntrycd,source_indicator_name,effective_date,sc_id,unt_id,Value,src_id,sbsrc_id,frq_id,vt_id,submitBy,Actflag,Base,Notes,type_cd,codedBy,checkedBy,Dist_ID,period_id,original_file_id,Publication_date);"		
		Call OpenDbCon(CONF, sSql)
		Call sAppend(objSubFolder.Path,objFile.name,objFile.DateLastModified,sStatus)
      	'WScript.Echo objFile.Path
      
    Next
	fCount = colFiles.Count
	Call sAppendCnt(objSubFolder.Path,fCount)

    Call ShowSubFolders(objSubFolder)

  Next

End Sub

Sub sAppend(f,n,m,sStatus)
'	sSource = path & "" & file
sTime = Time()		
	path = f
	if n = "n" then
		file = " "
	Else
		file = n
	End If
	if m = "m" then
		modd = " "
	Else
		modd = m
	End If
	

	
	
    Set xl = CreateObject("Excel.application")
    ' if my destination folder doesn't need creating, i create it
    if not fso.folderexists(rPath) then
        fso.CreateFolder(rPath)
    end if
    ' create xl file with columns if it's not there
    if not fso.fileexists(rLog) then
        xl.application.workbooks.add
        xl.application.save(rLog)
        set objSheet = xl.ActiveWorkbook.Worksheets(1)
        sTime = Time()
        
        i = 2
        objSheet.Columns("A:A").ColumnWidth = "60"
        objSheet.Columns("B:B").ColumnWidth = "16"
        objSheet.Columns("C:C").ColumnWidth = "16"
        objSheet.Columns("D:D").ColumnWidth = "11"
        objSheet.Cells(1, 1).Font.ColorIndex = 11
        objSheet.Cells(1, 1).value = sFolder & " Folder Report for: " & TheDate
        objSheet.Cells(i, 1).value = "File Path"
        objSheet.Cells(i, 1).Font.Bold = True
        objSheet.Cells(i, 2).value = "File Name"
        objSheet.Cells(i, 2).Font.Bold = True
        objSheet.Cells(i, 3).value = "Date Modified"
        objSheet.Cells(i, 3).Font.Bold = True
        objSheet.Cells(i, 4).value = "Time: "
        objSheet.Cells(i, 4).Font.Bold = True
        objSheet.Cells(i, 5).value = "Status"
        objSheet.Cells(i, 5).Font.Bold = True
        xl.DisplayAlerts = False
        objSheet.saveas(rLog)
        xl.application.quit
     
    End if
    ' open up the xl sheet
    xl.Application.Workbooks.Open rLog
    xl.Application.Visible = FALSE
    set objSheet = xl.ActiveWorkbook.Worksheets(1)
    ' get todays date and place in a variable
    
    ' get the last row in the spreadsheet
    LastRow = objSheet.UsedRange.Rows.Count
    ' just an if statement in case it's the first entry
    ' i leave a line between updates by incrementing lastrow by 2
    ' if you want a bigger gap you could change this to 3, 4, 5 etc
    if LastRow = 1 then
        i = 2
    else
        i = LastRow + 1
    End if
    ' iterate through the files and write details to spreadsheet
    
        fullpath = file
        	objSheet.Cells(1, 1).value = sFolder & " Folder Report for: " & TheDate & " Start Time: " & stTime & " Finish Time: " & sTime & " Run Time: " & tDiff
            objSheet.Cells(i, 1).value = path
            objSheet.Cells(i, 2).value = file
            objSheet.Cells(i, 3).value = modd
            ' add a line to say when update to spreadsheet was ran
    		objSheet.Cells(i,4).Font.ColorIndex = 11
            objSheet.Cells(i,4).Value = sTime
    		objSheet.Cells(i,5).Font.ColorIndex = 11
            objSheet.Cells(i,5).Value = sStatus
            i = i + 1
        
  
     
    ' save & close
    xl.DisplayAlerts = FALSE
    objSheet.saveas(rLog)
    xl.application.quit
End Sub

Sub sAppendCnt(f,c)
	sTime = Time()
	path = f
	rLog = rPath & "\" & rFile
	'MsgBox rLog
	sCount = c
	
	
    Set xl = CreateObject("Excel.application")
    ' if my destination folder doesn't need creating, i create it
    if not fso.folderexists(rPath) then
        fso.CreateFolder(rPath)
    end if
    ' create xl file with columns if it's not there
    if not fso.fileexists(rLog) then
        xl.application.workbooks.add
        xl.application.save(rLog)
        set objSheet2 = xl.ActiveWorkbook.Worksheets(2)
        i = 2
        objSheet2.Columns("A:A").ColumnWidth = "70"
        objSheet2.Columns("B:B").ColumnWidth = "10"
        objSheet2.Columns("C:C").ColumnWidth = "16"
        objSheet2.Columns("D:D").ColumnWidth = "11"
        objSheet2.Cells(1, 1).Font.ColorIndex = 11
        objSheet2.Cells(1, 1).value = sFolder & " Folder Report for: " & TheDate
        objSheet2.Cells(i, 1).value = "Folder"
        objSheet2.Cells(i, 1).Font.Bold = True
        objSheet2.Cells(i, 2).value = "Count"
        objSheet2.Cells(i, 2).Font.Bold = True
        xl.DisplayAlerts = FALSE
        objSheet2.saveas(rLog)
        xl.application.quit
    Else
     	xl.Application.Workbooks.Open rLog
    	xl.Application.Visible = FALSE 
    	set objSheet2 = xl.ActiveWorkbook.Worksheets(2)
    	i = 2
        objSheet2.Columns("A:A").ColumnWidth = "10"
        objSheet2.Columns("B:B").ColumnWidth = "100"
        objSheet2.Columns("C:C").ColumnWidth = "16"
        objSheet2.Columns("D:D").ColumnWidth = "11"
        objSheet2.Cells(1, 1).Font.ColorIndex = 11
        objSheet2.Cells(1, 1).value = sFolder & " Folder Report for: " & TheDate & " Start Time: " & stTime & " Finish Time: " & sTime
        objSheet2.Cells(i, 2).value = "Folder"
        objSheet2.Cells(i, 2).Font.Bold = True
        objSheet2.Cells(i, 1).value = "File Count"
        objSheet2.Cells(i, 1).Font.Bold = True
        xl.DisplayAlerts = FALSE 
        objSheet2.saveas(rLog)
        xl.application.quit

    End if
    ' open up the xl sheet
    xl.Application.Workbooks.Open rLog
    xl.Application.Visible = FALSE
    set objSheet2 = xl.ActiveWorkbook.Worksheets(2)
    ' get todays date and place in a variable
    
    ' get the last row in the spreadsheet
    LastRow = objSheet2.UsedRange.Rows.Count
    ' just an if statement in case it's the first entry
    ' i leave a line between updates by incrementing lastrow by 2
    ' if you want a bigger gap you could change this to 3, 4, 5 etc
    if LastRow = 1 then
        i = 2
    else
        i = LastRow + 1
    End if
    ' iterate through the files and write details to spreadsheet
    
        
            objSheet2.Cells(i, 2).value = path
            objSheet2.Cells(i, 1).value = sCount
            
            i = i + 1
    ' save & close
    xl.DisplayAlerts = FALSE
    objSheet2.saveas(rLog)
    xl.application.quit
End Sub 

    
    
Sub RenameSheets()
    Set xl = CreateObject("Excel.application")
    xl.Application.Workbooks.Open rLog
    xl.Application.Visible = False
	set objSheet1 = xl.ActiveWorkbook.Worksheets(1)
	set objSheet2 = xl.ActiveWorkbook.Worksheets(2)
	objSheet1.Name = "Inventory"
	objSheet2.Name = "File Counts"
 	xl.DisplayAlerts = FALSE
    xl.ActiveWorkbook.saveas(rLog)
    xl.application.Quit
End Sub


Sub OpenDbCon(p_conf,sSql)
Dim f1, ts, s, uname, pword, prov, pcata, dsource,MyArray,SVRL,ErrMsg
SVRL = "MyDev"
Const ForReading = 1
'** Get connection details
Set ts = fso.OpenTextFile(p_conf, ForReading)
Do 
  s = ts.ReadLine
  MyArray = Split(s, vbTab)
  If MyArray(0) = SVRL Then
    uname = MyArray(2)
    pword = MyArray(3)
    dsource = MyArray(4) 			
    Exit do
  End if		
Loop while NOT ts.AtEndOfStream
	cmdline = "-h" & MyArray(4) & " -u" & MyArray(2) & " -p" & MyArray(3) 

ts.Close
'MsgBox uname & pword & dsource
step1=WshShell.Run ("cmd /C o:\auto\apps\exe\mysql32.exe -v -C -B " & cmdline & " -e " & chr(34) & sSql & chr(34), 1, true)
'run = "cmd /C o:\auto\apps\exe\mysql32.exe -v -C " & cmdline & " < " & SQL
'MsgBox run


If step1 <> 0 Then
  sStatus = "Failed"
 'MsgBox step1
  
else   
 sStatus = "Successful"
		'MsgBox "sSQL Successful"
 
End If
On Error Goto 0
'WScript.Quit
End Sub
