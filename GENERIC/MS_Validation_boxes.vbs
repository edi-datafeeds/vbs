Option explicit

' **** MAIN ****
    
' **** VARIABLES ****

Dim iTime, Seqnce

CONST ForReading = 1, ForWriting = 2, ForAppending = 8
Dim CountCheck,objRecordSet,objConnection, strConnectString

Dim seq, actTime, fileName, insertCnt, updateCnt, cntInserts, cntUpdates, cntError, total_cnt 'declaing varibles

Dim ms_Array_Kentish(7), ms_Array_Master(7), sqlStmt 'declaring array

iTime = Time
iTime = int(left(iTime,2)) 

'------------------------------------------------------------------------------------------------
'Get the Sequence Number
'------------------------------------------------------------------------------------------------

If iTime >= 12 and iTime <= 15  then 
	Seqnce = 2
ElseIf iTime >= 16 and  iTime <= 21  then 
	Seqnce = 3
Else
	Seqnce = 1
End If 


sqlStmt = "SELECT TOP 1 acttime, " & _ 
                        "file_name, seq, insert_cnt, update_cnt, error_cnt, insert_cnt + update_cnt + error_cnt AS total_cnt " & _ 
                      "FROM wca.dbo.tbl_opslog " & _ 
                      "WHERE acttime > DATEADD(day, DATEDIFF(day, 0, GETDATE()), 0) " & _ 
                      "AND seq =  " & Seqnce & _ 
                      " ORDER BY acttime DESC "

'back date, testing purpose
'sqlStmt = "SELECT TOP 1 acttime, " & _ 
'                        "file_name, seq, insert_cnt, update_cnt, error_cnt, insert_cnt + update_cnt + error_cnt AS total_cnt " & _ 
'                      "FROM wca.dbo.tbl_opslog " & _ 
'                      "WHERE acttime < DATEADD(day, DATEDIFF(day, 0, GETDATE()), -10) " & _ 
'                      "AND seq = 1 " & _ 
'                      " ORDER BY acttime DESC "

'------------------------------------------------------------------------------------------------

Call msSQL_Master()

Call msSQL_Kentish()'check MS count

Call checkValues()'validate both counts

'Call checkArrOutput()'Check Array Output, test purpose 

'------------------------------------------------------------------------------------------------

Sub msSQL_Master()
    
    Const adOpenStatic = 3
    Const adLockOptimistic = 3
    
    Set objConnection = CreateObject("ADODB.Connection")
    Set objRecordSet = CreateObject("ADODB.Recordset")
    
    'this is SQL back connection
    objConnection.Open "Provider=SQLOLEDB;Data Source=192.168.2.80;" & _
                       "Trusted_Connection=true;Initial Catalog=client;" & _
                       "User ID=sa;Password=K376:lcnb;"
    
    
    If objConnection.State = 1 Then
        'MsgBox "connected"
        'Wscript.Echo "connected MS SQL"
    Else
        MsgBox "connection failed on MS"
        'Wscript.Echo "connection failed"
    End If
                        
    objRecordSet.Open sqlStmt, objConnection, adOpenStatic, adLockOptimistic                    
    
    If objRecordSet.RecordCount = 0 Then 
      
      MsgBox "File has not been Loaded! On MS SQL Master" 
	
    Else
	    objRecordSet.MoveFirst
	    
	    'WScript.Echo objRecordSet.RecordCount
	    
	    Do Until objRecordset.EOF
	        
	        ms_Array_Master(0) = objRecordset.Fields.Item("seq")
	        ms_Array_Master(1) = objRecordset.Fields.Item("acttime")
	        ms_Array_Master(2) = objRecordset.Fields.Item("file_name")
	        ms_Array_Master(3) = objRecordset.Fields.Item("insert_cnt")
	        ms_Array_Master(4) = objRecordset.Fields.Item("update_cnt")
	        ms_Array_Master(5) = objRecordset.Fields.Item("error_cnt")
	        ms_Array_Master(6) = objRecordset.Fields.Item("total_cnt")
	    
	        objRecordset.MoveNext
	        
	    Loop
	    
	    objRecordSet.Close
	    objConnection.Close
	    
	    Set objRecordSet = NOTHING
	    Set objConnection = NOTHING
	
	End If
	
End Sub

'------------------------------------------------------------------------------------------------

Sub msSQL_Kentish()
    
    Const adOpenStatic = 3
    Const adLockOptimistic = 3
    
    Set objConnection = CreateObject("ADODB.Connection")
    Set objRecordSet = CreateObject("ADODB.Recordset")
    
    'this is SQL back connection
    objConnection.Open "Provider=SQLOLEDB;Data Source=192.168.12.172;" & _
                       "Trusted_Connection=true;Initial Catalog=client;" & _
                       "User ID=sa;Password=K376:lcnb;"
    
    
    If objConnection.State = 1 Then
        'MsgBox "connected"
        'Wscript.Echo "connected MS SQL"
    Else
        MsgBox "connection failed on MS"
        'Wscript.Echo "connection failed"
    End If
                        
    objRecordSet.Open sqlStmt, objConnection, adOpenStatic, adLockOptimistic                    
    
    If objRecordSet.RecordCount = 0 Then 
      
      MsgBox "File has not been Loaded! On MS SQL Kentish" 
	
    Else
	    objRecordSet.MoveFirst
	    
	    'WScript.Echo objRecordSet.RecordCount
	    
	    Do Until objRecordset.EOF
	        
	        ms_Array_Kentish(0) = objRecordset.Fields.Item("seq")
	        ms_Array_Kentish(1) = objRecordset.Fields.Item("acttime")
	        ms_Array_Kentish(2) = objRecordset.Fields.Item("file_name")
	        ms_Array_Kentish(3) = objRecordset.Fields.Item("insert_cnt")
	        ms_Array_Kentish(4) = objRecordset.Fields.Item("update_cnt")
	        ms_Array_Kentish(5) = objRecordset.Fields.Item("error_cnt")
	        ms_Array_Kentish(6) = objRecordset.Fields.Item("total_cnt")
	    
	        objRecordset.MoveNext
	        
	    Loop
	    
	    objRecordSet.Close
	    objConnection.Close
	    
	    Set objRecordSet = NOTHING
	    Set objConnection = NOTHING
	
	End If
	
End Sub

'---------------------------------------------------------------------------------------------------------------------

Sub checkValues()
    
    
    'checkArrOutput()
    
    Dim totalMS, totalMY
    
    If iTime >= 12 and iTime <= 15  then 
    
        If ms_Array_Kentish(0)= 2 And ms_Array_Kentish(0) = ms_Array_Master(0) Then 
            Call CheckCount
        Else    
            Call output_Sequence
        End If
        
    ElseIf iTime >= 16 and  iTime <= 21  then 
    
        If ms_Array_Kentish(0)= 3 And ms_Array_Kentish(0) = ms_Array_Master(0) Then 
            Call CheckCount
        Else
            Call output_Sequence
        End If
    Else
    
        If ms_Array_Kentish(0)= 1 And ms_Array_Kentish(0) = ms_Array_Master(0) Then 
            Call CheckCount
        Else
            Call output_Sequence
        End If    
     
    End If 
    
End Sub

'---------------------------------------------------------------------------------------------------------------------

Sub CheckCount()
    
    CountCheck = CInt(ms_Array_Master(6) - ms_Array_Kentish(6))
    
    'WScript.Echo CountCheck
    
    If ms_Array_Master(6) > ms_Array_Kentish(6) Then 
    	
	    'threshold count
	    If CountCheck >= 100 Then 
	    
	        WScript.Echo "PLEASE run the loader again - The difference between the two Total Counts is outside of the acceptable range"  & vbCrLf & vbLf & "MS Master Total Count " & ms_Array_Master(6) & vbCrLf & vbLf & "MS Kentish Total Count " & ms_Array_Kentish(6)   
	        
	        
		ElseIf CountCheck >= 90 Then 
	    
	        WScript.Echo " PLEASE CALL IVAN - The difference between the two Total Counts is outside of the acceptable range"  & vbCrLf & vbLf & "MS Master Total Count " & ms_Array_Master(6) & vbCrLf & vbLf & "MS Kentish Total Count " & ms_Array_Kentish(6)   
	        
	        
		Else
		
			'checkArrOutput()    
	    
	    End If 
	    
	Else
		
		'checkArrOutput()    
	    
	End If 

End Sub 

'---------------------------------------------------------------------------------------------------------------------

Sub output_Sequence()

    MsgBox "Sequence Does NOT Match" & vbCr & vbLf & vbCr & vbLf& "MS Master increment " & ms_Array_Master(0) & " - MS Kentish increment " & ms_Array_Kentish(0) & vbCrLf & vbCrLf & vbLf &  "MS Total Count " & ms_Array_Master(6) & vbCr & vbLf & vbCr & vbLf & "MY Total Count " & ms_Array_Kentish(6)   
	'Call checkValues()
	
End Sub

'---------------------------------------------------------------------------------------------------------------------

Sub checkArrOutput() 'Check Array Output, test purpose 
	
	MsgBox "Horsham  " & vbTab & vbTab &  vbTab & "Kentish " & _
	vbCr & vbLf & vbCr & vbLf & _
	"Seq " & ms_Array_Master(0) & vbTab & vbTab &  vbTab & "Seq " & ms_Array_Kentish(0) & _
	vbCr & vbLf & vbCr & vbLf & _
	"File " & ms_Array_Master(2) & vbTab & vbTab & "File " & ms_Array_Kentish(2) & _
	vbCr & vbLf & vbCr & vbLf & _
	"Insert " & ms_Array_Master(3) & vbTab & vbTab & "Insert " & ms_Array_Kentish(3) & _
	vbCr & vbLf & vbCr & vbLf & _
	"Update " & ms_Array_Master(4) & vbTab & vbTab & "Update " & ms_Array_Kentish(4) & _
	vbCr & vbLf & vbCr & vbLf & _
	"Error " & ms_Array_Master(5) & vbTab & vbTab &  vbTab & "Error " & ms_Array_Kentish(5) & _
	vbCr & vbLf & vbCr & vbLf & _
	"Total " & ms_Array_Master(6) & vbTab & vbTab & "Total " & ms_Array_Kentish(6) & _
	vbCr & vbLf & vbCr & vbLf & _
	"Difference "  & vbTab & CountCheck
	
End Sub

Sub checkArrOutput1() 'Check Array Output, test purpose 
	
	MsgBox "Horsham  " & _
		vbTab & " Seq " & ms_Array_Master(0) & _
		vbTab & " Total " & ms_Array_Master(6) & _
		vbCr & vbLf & vbCr & vbLf & "Kentish " & _
	    vbTab & " Seq " & ms_Array_Kentish(0) & _
	    vbTab & " Total " & ms_Array_Kentish(6)
	
End Sub