' ****************************************************************
' GENERIC FileTidy.VBS
'
'     Script for Deleting Files in the Directory
' 
' Arguments in order (0) sPath: H:\folder1\
'                    (1) sFilename: Full File Name or File Extension: file1.txt or .txt
'		     (2) 	
'		     
' ****************************************************************
option explicit

' **** MAIN ****
	
' **** VARIABLES ****

	
	dim fso, WshShell, numArgs, sFile, sPath, sExt, dFile, dPath, dExt, sMonth, sDay, sYear, Source, Dest, x, y, Pre, sDate,fSource
	dim lDate, sPre, dPre, PreSdate, PreLdate, SdateSuf, SdateLuf, sSuf, fs, dFileEx, sFileEx,objFSO,objReadFile,contents,objFolder,objFile,objTextFile,Act 

' **** PREPARATION ****
	
	
	'Checks to see if the correct number of paramters have been set
	
	Set numArgs = WScript.Arguments
	if numArgs.length <2 then
		msgbox "Syntax:Missing Arguments"
		wscript.quit
	end if
		
	
	'Arguments
	
	Source = numArgs(0)
	Dest = numArgs(1)

	Set WshShell = WScript.CreateObject("WScript.Shell")
    Set fso = CreateObject("Scripting.FileSystemObject")	        

	'assign variable arguments if they exist
	if wscript.arguments.count = 3 then
		Pre = numArgs(2)
		Call splitPre()
		Call splitSource()
		Call splitDest()
		Call Action()
		Call FileExists()
	else	
		Call splitSource()
		Call splitDest()
		Call Action(Act)
		Call FileExists()

	end if
	if wscript.arguments.count = 4 then
		Pre = numArgs(2)
		Act = numArgs(3)
		Call splitPre()
		Call splitSource()
		Call splitDest()
		Call Action(Act)
		Call FileExists()
	else	
		Call splitSource()
		Call splitDest()
		Call Action()
		'Call FileExists()

	end if
	if wscript.arguments.count > 4 then

		msgbox "Syntax:Too Many Arguments you Nob"
		wscript.quit
	end if


	
'******************************************* Split Source String **********************************************************	
	

	'Split the Source string into fullpath,full filename and ext

	Sub splitSource()
	msgbox "Source: " & Source
	x = Len(Source)
	for y = x to 1 step -1
		if mid(Source, y, 1) = "\" or mid(Source, y, 1) = "/" then
		    sFile = mid(Source, y+1)
   		   	'msgbox "sFile: " & sFile
   		    sPath = mid(Source, 1, y-0)
		'	msgbox "sPath: " & sPath
		    exit for
		end if
		
		if mid(Source, y, 1) = "." then
			sExt = mid(Source, y-0)
			'msgbox "sExt: " & sExt
		end if
		
	next
  
  	if sFile = "ldate" & sExt then
		Call slongDate()
	end if
	
	if sFile = "sdate" & sExt then
		Call sshortDate()
	end if

	if sFile = "Presdate" & sExt then
		Call sPreShortDate()
	end if
	
	if sFile = "sdateSuf" & sExt then
		Call sShortDateSuf()
	end if
	
	if sFile = "PresdateSuf" & sExt then
		Call sPreShortDateSuf()
	end if

	if sFile = "Preldate" & sExt then
		Call sPreLongDate()
	end if
	
	if sFile = "ldateSuf" & sExt then
		Call sLongDateSuf()
	end if
	
	if sFile = "PreldateSuf" & sExt then
		Call sPreLongDateSuf()
	end if
	
	if sFile = "PresdateSuf" & sExt then
		Call sPreShortDateSuf()
	end if
	
    
 	end Sub
	
	
'******************************************** Split Destination String ******************************************************	

	Sub splitDest()

	  dFile = Dest
	  x = Len(Dest)
	  for y = x to 1 step -1
		if mid(Dest, y, 1) = "\" or mid(Dest, y, 1) = "/" then
		    dFile = mid(Dest, y+1)
   		   '	msgbox "dFile: " & dFile
   		    dPath = mid(Dest, 1, y-0)
		'	msgbox "dPath: " & dPath
		    exit for
		end if
		
		if mid(Dest, y, 1) = "." then
			dExt = mid(Dest, y-0)
		'	msgbox "dExt: " & dExt
		end if
	next
	if dFile = "ldate" & dExt then
		Call dlongDate()
	end if
	
	if dFile = "sdate" & dExt then
		Call dshortDate()
	end if

	if dFile = "Presdate" & dExt then
		Call dPreShortDate()
	end if
	
	if dFile = "sdateSuf" & dExt then
		Call dShortDateSuf()
	end if
	
	if dFile = "PresdateSuf" & dExt then
		Call dPreShortDateSuf()
	end if

	if dFile = "Preldate" & dExt then
		Call dPreLongDate()
	end if
	
	if dFile = "ldateSuf" & dExt then
		Call dLongDateSuf()
	end if
	
	if dFile = "PreldateSuf" & dExt then
		Call dPreLongDateSuf()
	end if
	
	if dFile = "PresdateSuf" & dExt then
		Call dPreShortDateSuf()
	end if

	end Sub

	
'******************************************** Split Prefix-Suffix String ******************************************************	

	Sub splitPre()

	  sPre = Pre
	  x = Len(Pre)
	  for y = x to 1 step -1
		if mid(Pre, y, 1) = "-" then
			sPre = mid(Pre, 1, y-1)
				'msgbox "Pre: " & sPre
			sSuf = mid(Pre, y+1, len(pre) - y)
				'msgbox "Suf: " & sSuf
			exit for
		end if
	   next
	
	end Sub
	
	
'*************************************************** SOURCE LONG DATE **********************************************************	
	
	
	Sub slongDate()
		
	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits
			  
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		sYear = mid(Year(Now),1,4)
		
		'-- Create the Custom filename
		 
		sFile = sYear & sMonth & sDay & sExt
	End Sub
	
	
'************************************************* SOURCE SHORT DATE **********************************************************	
	
	
	Sub sshortDate()
		
	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits
			  
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		sYear = mid(Year(Now),3,4)
		
		'-- Create the Custom filename
		 
		sFile = sYear & sMonth & sDay & sExt
	End Sub
		
'****************************************** SOURCE PREFIX SHORT DATE **********************************************************	
	
	
	Sub sPreShortDate()
		
	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits
			  
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		sYear = mid(Year(Now),3,4)
		
		'-- Create the Custom filename
		'msgbox sYear & " syear" 
		sFile = sPre & sYear & sMonth & sDay & sExt
		'msgbox sPre & "= sPre"
	End Sub

'****************************************** SOURCE PREFIX LONG DATE **********************************************************	
	
	
	Sub sPreLongDate()
		
	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits
			  
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		sYear = mid(Year(Now),1,4)
		
		'-- Create the Custom filename
		'
		'msgbox sYear & " syear" 
		sFile = sPre & sYear & sMonth & sDay & sExt
		'msgbox sPre & "= sPre"
	End Sub

'***************************************** SOURCE LONG SUFFIX DATE **********************************************************	
	
	
	Sub sLongDateSuf()
		
	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits
			  
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		sYear = mid(Year(Now),1,4)
		
		'-- Create the Custom filename
		'msgbox sSuf & "= sSuf"
		sFile = sYear & sMonth & sDay & sSuf & sExt
	End Sub

'************************************ SOURCE SHORT SUFFIX DATE **********************************************************	
	
	
	Sub sShortDateSuf()
		
	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits
			  
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		sYear = mid(Year(Now),3,4)
		
		'-- Create the Custom filename
		'msgbox sSuf & "= sSuf"
		sFile = sYear & sMonth & sDay & sSuf & sExt
	End Sub
	
'****************************** SOURCE SHORT Prefix & SUFFIX DATE **********************************************************	
	
	
	Sub sPreShortDateSuf()
		
	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits
			  
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		sYear = mid(Year(Now),3,4)
		
		'-- Create the Custom filename
		'msgbox sPre & "= sPre"
		'msgbox sSuf & "= sSuf"
		sFile = sPre & sYear & sMonth & sDay & sSuf & sExt
	End Sub

'******************************* SOURCE LONG PREFIX & SUFFIX DATE **********************************************************	
	
	
	Sub sPreLongDateSuf()
		
	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits
			  
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		sYear = mid(Year(Now),1,4)
		
		'-- Create the Custom filename
		'msgbox sPre & "= sPre"
		'msgbox sSuf & "= sSuf"
		sFile = sPre & sYear & sMonth & sDay & sSuf & sExt
	End Sub
	
'******************************************** DESTINATION LONG DATE **********************************************************	
	
	
	Sub dlongDate()
		
	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits
			  
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		sYear = mid(Year(Now),1,4)
		
		'-- Create the Custom filename
		 
		dFile = sYear & sMonth & sDay & dExt
	End Sub
	
	
'******************************************* DESTINATION SHORT DATE **********************************************************	
	
	
	Sub dshortDate()
		
	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits
			  
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		sYear = mid(Year(Now),3,4)
		
		'-- Create the Custom filename
		 
		dFile = sYear & sMonth & sDay & dExt
	End Sub
		
'*********************************** DESTINATION PREFIX SHORT DATE **********************************************************	
	
	
	Sub dPreShortDate()
		
	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits
			  
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		sYear = mid(Year(Now),3,4)
		
		'-- Create the Custom filename
		'msgbox sYear & " syear" 
		'msgbox sPre & "= sPre"
		dFile = sPre & sYear & sMonth & sDay & dExt
	End Sub

'*********************************** DESTINATION PREFIX LONG DATE **********************************************************	
	
	
	Sub dPreLongDate()
		
	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits
			  
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		sYear = mid(Year(Now),1,4)
		
		'-- Create the Custom filename
		'msgbox sYear & " syear" 
		'msgbox sPre & "= sPre"
		dFile = sPre & sYear & sMonth & sDay & dExt
	End Sub

'********************************** DESTINATION LONG SUFFIX DATE **********************************************************	
	
	
	Sub dLongDateSuf()
		
	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits
			  
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		sYear = mid(Year(Now),1,4)
		
		'-- Create the Custom filename
		'msgbox sSuf & "= sSuf"
		dFile = sYear & sMonth & sDay & sSuf & dExt
	End Sub

'*********************************** DESTINATION SHORT SUFFIX DATE **********************************************************	
	
	
	Sub dShortDateSuf()
		
	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits
			  
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		sYear = mid(Year(Now),3,4)
		
		'-- Create the Custom filename
		'msgbox sSuf & "= sSuf"
		dFile = sYear & sMonth & sDay & sSuf & dExt
	End Sub
	
'***************************** DESTINATION SHORT Prefix & SUFFIX DATE **********************************************************	
	
	
	Sub dPreShortDateSuf()
		
	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits
			  
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		sYear = mid(Year(Now),3,4)
		
		'-- Create the Custom filename
		'msgbox sPre & "= sPre"
		'msgbox sSuf & "= sSuf"
		dFile = sPre & sYear & sMonth & sDay & sSuf & dExt
	End Sub

'**************************** DESTINATION LONG PREFIX & SUFFIX DATE **********************************************************	
	
	
	Sub dPreLongDateSuf()
		
	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits
			  
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		sYear = mid(Year(Now),1,4)
		
		'-- Create the Custom filename
		'msgbox sPre & "= sPre"
		'msgbox sSuf & "= sSuf"
		dFile = sPre & sYear & sMonth & sDay & sSuf & dExt
	End Sub
	
'************************ Multi FILES FUNCTION *******************************************************************


Sub all()
  	Wscript.Echo "In the ALL Function"

'Multifile Array (*.*) Reads all files in a folder and stores them in an array which can be passed to other functions


  Dim files, NewFile,folder,folderIdx
  
  Set fso = CreateObject("Scripting.FileSystemObject")
  Wscript.Echo sPath
  If sPath = "*" Then
      Wscript.Echo "No Source Folder was passed"
      Wscript.Quit
  End If
  If sFile = "*.*" Then
  Set NewFile = fso.CreateTextFile(dPath&"\FileListNew.txt", True)
  Set folder = fso.GetFolder(sPath)
  Set files = folder.Files
  
  For each folderIdx In files
    NewFile.WriteLine(folderIdx.Name)
    sFile = folderIdx.Name
Exit For
  	'WScript.Echo sFile
  Next  
  NewFile.Close
 
  End If 
End Sub  
'***END******************
	
'************************ VALIDATE FILES FUNCTION *******************************************************************



'Return 0 if a file exists else -1

Sub FileExists()
  Set fs = CreateObject("Scripting.FileSystemObject")

  if fs.FileExists(dPath & dFile) = False then
    dFileEx = -1
  else
    dFileEx = 0
  end if
  
  if fs.FileExists(sPath & sFile) = False then
    sFileEx = -1
  else
    sFileEx = 0
  end if

end Sub		
'***END******************


'************************ PROCESS & VALIDATE FILES *******************************************************************



Sub Action(Act)
Dim fname
Dim files, NewFile,folder,folderIdx
  
  Set fso = CreateObject("Scripting.FileSystemObject")
If Act = "append" Then
  If sFile = "*.*" Then
	  Set folder = fso.GetFolder(sPath)
	  Set files = folder.Files
  '	MsgBox folder
  	 
	  For each fname In files
	    MsgBox fname
	    sFile = fname
	    Call sAppend(sFile) 	
	    
		Next
		'WScript.Quit
	Else 
		Call sAppend(sPath & sFile)
  End If 
  
End If   


End Sub

'****************************************
Sub sFilecopy(sFile)
	if  sFileEx = 0 then
		if  dFileEx = -1 then
			wscript.echo ("Source: " & sFile), ("Dest: " & dPath & dFile )
			fso.CopyFile (sFile), (dPath & dFile) 
			msgbox dFile & " Copied to " & dPath
				Call FileExists
			if  dFileEx = -1 then
				msgbox sFile & " File Failed to Copy, Please Try Again"
				WScript.Quit(0)
			else
				WScript.Quit(1)
	
			end if
	
		else 
			msgbox dFile & " File Already Exists in " & dPath & "This File Will Be Overwritten"
			fso.CopyFile (sFile), (dPath & dFile) 
			msgbox dFile & " Overwritten to " & dPath
				Call FileExists
			if  dFileEx = -1 then
				msgbox sFile & " File Failed to Overwrite, Please Try Again"
				WScript.Quit(0)
			else
				WScript.Quit(1)
	
			end if
	
		end if
	else		
	
	
		msgbox sFile & " File Failed to Generate, Please Try Again"
		WScript.Quit(0)
	
	End if
End Sub
'***********
Sub sAppend(sFile)
'MsgBox "In Act - Append Function"
    	'*******************
    	  	
		'wscript.echo ("Source: " & sPath & sFile), ("Dest: " & dPath & dFile )
		'************
	'	MsgBox sPath & sFile
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		Set objReadFile = objFSO.OpenTextFile(sFile)
		
		
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		Const ForReading = 1
		
		
		'***** PROCESSING *****
		
		
		'Read file contents
		contents = objReadFile.ReadAll
		
		'Close file
		objReadFile.close
		
		'Display results
		'wscript.echo contents
		
		'Cleanup objects
		Set objFSO = Nothing
		'Set objReadFile = Nothing
		
		
		' Create the File System Object
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		
		' Check that the WriteDirectory folder exists
		If objFSO.FolderExists(dPath) Then
		   Set objFolder = objFSO.GetFolder(dPath)
		Else
		 ' Create the WriteDirectory folder if folder does not exist
		   Set objFolder = objFSO.CreateFolder(dPath)
		   'WScript.Echo "Just created " & dPath
		End If
		
		If objFSO.FileExists(dPath & dFile) Then
		   Set objFolder = objFSO.GetFolder(dPath)
		Else
		   Set objFile = objFSO.CreateTextFile(dPath & dFile)
		   'Wscript.Echo "Just created " & dPath & dFile
		End If
		
		set objFile = nothing
		set objFolder = nothing
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		Const ForAppending = 8
		
		
		
		Set objTextFile = objFSO.OpenTextFile _
		(dPath & dFile, ForAppending, True)
		
		
		' Writes WriteFile every time you run this VBScript
		objTextFile.WriteLine(contents)
		objTextFile.Close
		
		
		

		'*************		
			
	
			'MsgBox dFile & " Appended to " & dPath
				'Call FileExists  

	

'***************
    	
    	
  	'WScript.Echo sFile
  	
  		







End Sub
' **** END MAIN **** 