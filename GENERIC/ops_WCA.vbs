' Connect to a SQL Server Database

Dim seq, actTime, fileName, insertCnt, updateCnt, cntInserts, cntUpdates, cntError 'declaing varibles

Dim ms_Array(7), my_Array(7) 'declaring array

'---------------------------------------------------------------------------------------------------------------------

Call msSQL()

Call mySQL()

Call checkValues()

'---------------------------------------------------------------------------------------------------------------------
	
Private Sub msSQL()
	
	Const adOpenStatic = 3
	Const adLockOptimistic = 3
	
	Set objConnection = CreateObject("ADODB.Connection") ' create conection object
	Set objRecordSet = CreateObject("ADODB.Recordset")	 ' create record object 
	
	
	'objConnection.Open "Provider=SQLOLEDB;Data Source=192.168.12.172;" & _
	'        		   "Trusted_Connection=true;Initial Catalog=client;" & _
	'             	   "User ID=sa;Password=K376:lcnb;"
	
	'this is SQL back connection
	objConnection.Open "Provider=SQLOLEDB;Data Source=192.168.12.206;" & _
	        		   "Trusted_Connection=true;Initial Catalog=client;" & _
	             	   "User ID=sa;Password=K376:lcnb;"
	
	
	If objConnection.State = 1 Then
		'MsgBox "connected"
		Wscript.Echo "connected MS SQL"
	Else
		'MsgBox "connection failed"
		Wscript.Echo "connection failed"
	End If
	        			
	objRecordSet.Open "SELECT TOP 1 acttime, " & _ 
					  	"file_name, seq, insert_cnt, update_cnt, error_cnt " & _ 
					  "FROM wca.dbo.tbl_opslog " & _ 
					  "ORDER BY acttime DESC ", _
	        			objConnection, adOpenStatic, adLockOptimistic        			
	
	objRecordSet.MoveFirst
	
	'WScript.Echo objRecordSet.RecordCount
	
	Do Until objRecordset.EOF
	
		seq = objRecordset.Fields.Item("seq")
		actTime = objRecordset.Fields.Item("acttime")
		fileName = objRecordset.Fields.Item("file_name")
		insertCnt = objRecordset.Fields.Item("insert_cnt")
		updateCnt =	objRecordset.Fields.Item("update_cnt")
		cntError = objRecordset.Fields.Item("error_cnt")
		
		ms_Array(0) = seq
		ms_Array(1) = actTime
		ms_Array(2) = fileName
		ms_Array(3) = insertCnt
		ms_Array(4) = updateCnt
		ms_Array(5) = cntError
		
		ms_Array(6) = insertCnt + updateCnt + cntError
		
			
	    Wscript.Echo actTime & _
	         vbTab & fileName & _
	         vbTab & seq & _
		     vbTab & " Inserts " & insertCnt & _
		     vbTab & " Update " & updateCnt & _
		   	 vbTab & " Errors " & cntError
	
	    objRecordset.MoveNext
	Loop
	
	objRecordSet.Close
	objConnection.Close
	
	Set objRecordSet = NOTHING
	Set objConnection = NOTHING

End Sub

'---------------------------------------------------------------------------------------------------------------------

Private Sub mySQL()
	
	Const adOpenStatic = 3
	Const adLockOptimistic = 3
	
	' i did it like this as i has trouble with connection
	db_server = "192.168.12.160"
	db_user = "sa"
	db_pass = "K376:lcnb"
	db_name = "wca"
	
	
	Set objConnection = CreateObject("ADODB.Connection")
	Set objRecordSet = CreateObject("ADODB.Recordset")
	
	strConnectString = "DRIVER={MySQL ODBC 5.1 Driver};" & "SERVER=" & db_server & ";" _
					& " DATABASE=" & db_name & ";" & "UID=" & db_user & ";PWD=" & db_pass & "; OPTION=3"
	
	objConnection.Open strConnectString
	
	If objConnection.State = 1 Then
		WScript.Echo "connected MY SQL"
	Else
		Wscript.Echo "connection failed"
	End If
	        			
	objRecordSet.Open "SELECT acttime, " & _ 
					  	"file_name, seq, insert_cnt, update_cnt, error_cnt " & _ 
					  "FROM wca.tbl_opslog " & _ 
					  " WHERE file_name = '" & ms_Array(2) & "' AND seq = '" & ms_Array(0) & "'" & _ 
					  " ORDER BY acttime DESC LIMIT 1", _
	        			objConnection, adOpenStatic, adLockOptimistic        			
	
	objRecordSet.MoveFirst
	
	'WScript.Echo objRecordSet.RecordCount
	
	Do Until objRecordset.EOF
	
		seq = objRecordset.Fields.Item("seq")
		actTime = objRecordset.Fields.Item("acttime")
		fileName = objRecordset.Fields.Item("file_name")
		insertCnt = objRecordset.Fields.Item("insert_cnt")
		updateCnt =	objRecordset.Fields.Item("update_cnt")
		cntError = objRecordset.Fields.Item("error_cnt")
		
		my_Array(0) = seq
		my_Array(1) = actTime
		my_Array(2) = fileName
		my_Array(3) = insertCnt
		my_Array(4) = updateCnt
		my_Array(5) = cntError
		
		
		my_Array(6) = insertCnt + updateCnt + cntError
			
	    Wscript.Echo actTime & _
	       vbTab & fileName & _
	       vbTab & seq & _
		   vbTab & " Inserts " & insertCnt & _
		   vbTab & " Update " & updateCnt & _
		   vbTab & " Errors " & cntError
	
	    objRecordset.MoveNext
	Loop
		
	objRecordSet.Close
	objConnection.Close
	
	Set objRecordSet = NOTHING
	Set objConnection =  NOTHING
	
End Sub

'---------------------------------------------------------------------------------------------------------------------

Private Sub checkValues()
	
	Dim totalMS, totalMY
	
	WScript.Echo vbLf & "MS Total Count " & ms_Array(6) & vbTab & "MY Total Count " & my_Array(6) & vbCr & vbLf
	
	
	If ms_Array(0) >= my_Array(0) Then 'check the sequence
	
		WScript.Echo "Sequence Does Match"	
		'MsgBox "Sequence Does  Match; vbCr & vbLf & vbCr & vbLf& "MS Total Count " & ms_Array(6) & vbCr & vbLf & vbCr & vbLf & "MY Total Count " & my_Array(6)
		
	Else 
		
		WScript.Echo "Sequence Does not Match"
		MsgBox "Sequence Does NOT Match" & vbCr & vbLf & vbCr & vbLf& "MS Total Count " & ms_Array(6) & vbCr & vbLf & vbCr & vbLf & "MY Total Count " & my_Array(6)	
	End If 
	
End Sub

'---------------------------------------------------------------------------------------------------------------------
