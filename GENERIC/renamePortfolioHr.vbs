Dim rootFolder, objFS, objShell, objFolder, listDir, objRE 'declaring folders objects

Dim objFS_file, objFiles, listFiles, dateConCat, iTime 'declaring files objects

Dim numArgs


Set numArgs = WScript.Arguments

'rootFolder = "O:\Upload\Acc\274"

rootFolder = numArgs(0)

portName = numArgs(1)

searchFile( rootFolder )

Function searchFile( rootFolder )

	Set objFS_file = CreateObject("Scripting.FileSystemObject")

	Set objFiles = objFS_file.GetFolder( rootFolder )
	Set listFiles = objFiles.Files
	
	
	iTime = Time
	
	'MsgBox iTime
	
	iTime = left(iTime,2) & Left( Right(iTime,5), 2)
	
	'MsgBox iTime
	
	dateConCat = Year(Now) & Right("0" & Month(Now),2) & Right("0" & Day(Now),2) & "_" & iTime
	
	If NOT (objFS_file.FolderExists(rootFolder & "\isin_hist")) Then
	    ' Delete this if you don't want the MsgBox to show
	    'MsgBox("Local folder doesn't exists, creating...")
	    
	    objFS_file.CreateFolder(rootFolder & "\isin_hist" )
	End If
	
	
	For Each objFile In listFiles
		
		'WScript.Echo objFile.Name
		
		a=Split( objFile.Name, "_")
		
		objFS_file.CopyFile rootFolder & "\" & objFile.Name, rootFolder & "\isin_hist\" & "Report_" & dateConCat & "_" & a(0) 
		objFS_file.CopyFile rootFolder & "\" & objFile.Name, rootFolder & "\isin.txt" 
		
		objFS_file.DeleteFile rootFolder & "\" & objFile.Name 
		
	Next

End Function