
Dim sformatted,sCoded,sChecked

Const FOR_READING = 1

Set objFSO = CreateObject("Scripting.FileSystemObject")
Set fso = CreateObject("Scripting.FileSystemObject")

Set numArgs = WScript.Arguments


	if Len(Month(Now))=1 then 
	sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
	
	lYear = mid(Year(Now),1,4)
	sYear = mid(Year(Now),3,4)
	sTime = Time()
	stTime = Time()
	TheDate = Date()
	TheTime = Time()
	sFolder = numArgs(0)	
	strFolder = "\\192.168.2.163\EcoData\Africa\" & sFolder
	x = ""
	'Report = "\\192.168.2.163\EcoData\Africa\Import\Reports\"  & lYear & "-" & sMonth & "-" & sDay & "_" & sFolder & x & ".xls"
	rPath = "\\192.168.2.163\EcoData\Africa\Import\Reports\" & numArgs(1)
'	rFile = lYear & "-" & sMonth & "-" & sDay & "_" & x & ".xls"
	rFile = lYear & "-" & sMonth & "-" & sDay & "_" & sFolder & x & ".xls"
	rLog = rPath & "\" & rFile
	'MsgBox rLog

Set objFolder = objFSO.GetFolder(strFolder)

Call sAppend(objFolder.Path,n,m)
'WScript.Echo objFolder.Path

Set colFiles = objFolder.Files

For Each objFile In colFiles
		sSource = objFolder.Path & "\" &objFile.name
			'MsgBox sSource
			Call User(sSource)
	Call sAppend(objFolder.Path,objFile.name,objFile.DateLastModified)
  	'WScript.Echo objFile.Path

Next

fCount = colFiles.Count
Call sAppendCnt(objFolder.Path,fCount)
Call ShowSubFolders(objFolder)
call RenameSheets()
 

Sub ShowSubFolders(objFolder)

  Set colFolders = objFolder.SubFolders
	'fCount = colFiles.Count
	'Call sAppendCnt(fCount)

  For Each objSubFolder In colFolders

   ' WScript.Echo objSubFolder.Path
    Call sAppend(objSubFolder.Path,n,m)

    Set colFiles = objSubFolder.Files
	
    For Each objFile In colFiles
			sSource = objSubFolder.Path & "\" &objFile.name
			'MsgBox sSource
			Call User(sSource)
		Call sAppend(objSubFolder.Path,objFile.name,objFile.DateLastModified)
      	'WScript.Echo objFile.Path
      
    Next
	fCount = colFiles.Count
	Call sAppendCnt(objSubFolder.Path,fCount)

    Call ShowSubFolders(objSubFolder)

  Next

End Sub

Sub sAppend(f,n,m)
'	sSource = path & "" & file
		
	path = f
	if n = "n" then
		file = " "
	Else
		file = n
	End If
	if m = "m" then
		modd = " "
	Else
		modd = m
	End If
	

	
	
    Set xl = CreateObject("Excel.application")
    ' if my destination folder doesn't need creating, i create it
    if not fso.folderexists(rPath) then
        fso.CreateFolder(rPath)
    end if
    ' create xl file with columns if it's not there
    if not fso.fileexists(rLog) then
        xl.application.workbooks.add
        xl.application.save(rLog)
        set objSheet = xl.ActiveWorkbook.Worksheets(1)
        sTime = Time()
        
        i = 2
        objSheet.Columns("A:A").ColumnWidth = "60"
        objSheet.Columns("B:B").ColumnWidth = "70"
        objSheet.Columns("C:C").ColumnWidth = "16"
        objSheet.Columns("D:D").ColumnWidth = "11"
        objSheet.Cells(1, 1).Font.ColorIndex = 11
        objSheet.Cells(1, 1).value = sFolder & " Folder Report for: " & TheDate
        objSheet.Cells(i, 1).value = "File Path"
        objSheet.Cells(i, 1).Font.Bold = True
        objSheet.Cells(i, 2).value = "File Name"
        objSheet.Cells(i, 2).Font.Bold = True
        objSheet.Cells(i, 3).value = "Date Modified"
        objSheet.Cells(i, 3).Font.Bold = True
        objSheet.Cells(i, 4).value = "Time: "
        objSheet.Cells(i, 4).Font.Bold = True
        objSheet.Cells(i, 5).value = "Created By"
        objSheet.Cells(i, 5).Font.Bold = True
        objSheet.Cells(i, 6).value = "Coded By"
        objSheet.Cells(i, 6).Font.Bold = True
        objSheet.Cells(i, 7).value = "Checked By"
        objSheet.Cells(i, 7).Font.Bold = True
        xl.DisplayAlerts = False
        objSheet.saveas(rLog)
        xl.application.quit
     
    End if
    ' open up the xl sheet
    xl.Application.Workbooks.Open rLog
    xl.Application.Visible = FALSE
    set objSheet = xl.ActiveWorkbook.Worksheets(1)
    ' get todays date and place in a variable
    
    ' get the last row in the spreadsheet
    LastRow = objSheet.UsedRange.Rows.Count
    ' just an if statement in case it's the first entry
    ' i leave a line between updates by incrementing lastrow by 2
    ' if you want a bigger gap you could change this to 3, 4, 5 etc
    if LastRow = 1 then
        i = 2
    else
        i = LastRow + 1
    End if
    ' iterate through the files and write details to spreadsheet
    
        fullpath = file
        	objSheet.Cells(1, 1).value = sFolder & " Folder Report for: " & TheDate & " Start Time: " & stTime & " Finish Time: " & sTime & " Run Time: " & tDiff
            objSheet.Cells(i, 1).value = path
            objSheet.Cells(i, 2).value = file
            objSheet.Cells(i, 3).value = modd
            ' add a line to say when update to spreadsheet was ran
    		objSheet.Cells(i,4).Font.ColorIndex = 11
            objSheet.Cells(i,4).Value = TheTime
    		objSheet.Cells(i,5).Font.ColorIndex = 11
            objSheet.Cells(i,5).Value = sFormatted
    		objSheet.Cells(i,6).Font.ColorIndex = 11
            objSheet.Cells(i,6).Value = sCoded
    		objSheet.Cells(i,7).Font.ColorIndex = 11
            objSheet.Cells(i,7).Value = sChecked
            i = i + 1
        
  
     
    ' save & close
    xl.DisplayAlerts = FALSE
    objSheet.saveas(rLog)
    xl.application.quit
End Sub

Sub sAppendCnt(f,c)
	sTime = Time()
	path = f
	rLog = rPath & "\" & rFile
	'MsgBox rLog
	sCount = c
	
	
    Set xl = CreateObject("Excel.application")
    ' if my destination folder doesn't need creating, i create it
    if not fso.folderexists(rPath) then
        fso.CreateFolder(rPath)
    end if
    ' create xl file with columns if it's not there
    if not fso.fileexists(rLog) then
        xl.application.workbooks.add
        xl.application.save(rLog)
        set objSheet2 = xl.ActiveWorkbook.Worksheets(2)
        i = 2
        objSheet2.Columns("A:A").ColumnWidth = "70"
        objSheet2.Columns("B:B").ColumnWidth = "10"
        objSheet2.Columns("C:C").ColumnWidth = "16"
        objSheet2.Columns("D:D").ColumnWidth = "11"
        objSheet2.Cells(1, 1).Font.ColorIndex = 11
        objSheet2.Cells(1, 1).value = sFolder & " Folder Report for: " & TheDate
        objSheet2.Cells(i, 1).value = "Folder"
        objSheet2.Cells(i, 1).Font.Bold = True
        objSheet2.Cells(i, 2).value = "Count"
        objSheet2.Cells(i, 2).Font.Bold = True
        xl.DisplayAlerts = FALSE
        objSheet2.saveas(rLog)
        xl.application.quit
    Else
     	xl.Application.Workbooks.Open rLog
    	xl.Application.Visible = FALSE 
    	set objSheet2 = xl.ActiveWorkbook.Worksheets(2)
    	i = 2
        objSheet2.Columns("A:A").ColumnWidth = "10"
        objSheet2.Columns("B:B").ColumnWidth = "100"
        objSheet2.Columns("C:C").ColumnWidth = "16"
        objSheet2.Columns("D:D").ColumnWidth = "11"
        objSheet2.Cells(1, 1).Font.ColorIndex = 11
        objSheet2.Cells(1, 1).value = sFolder & " Folder Report for: " & TheDate & " Start Time: " & stTime & " Finish Time: " & sTime
        objSheet2.Cells(i, 2).value = "Folder"
        objSheet2.Cells(i, 2).Font.Bold = True
        objSheet2.Cells(i, 1).value = "File Count"
        objSheet2.Cells(i, 1).Font.Bold = True
        xl.DisplayAlerts = FALSE 
        objSheet2.saveas(rLog)
        xl.application.quit

    End if
    ' open up the xl sheet
    xl.Application.Workbooks.Open rLog
    xl.Application.Visible = FALSE
    set objSheet2 = xl.ActiveWorkbook.Worksheets(2)
    ' get todays date and place in a variable
    
    ' get the last row in the spreadsheet
    LastRow = objSheet2.UsedRange.Rows.Count
    ' just an if statement in case it's the first entry
    ' i leave a line between updates by incrementing lastrow by 2
    ' if you want a bigger gap you could change this to 3, 4, 5 etc
    if LastRow = 1 then
        i = 2
    else
        i = LastRow + 1
    End if
    ' iterate through the files and write details to spreadsheet
    
        
            objSheet2.Cells(i, 2).value = path
            objSheet2.Cells(i, 1).value = sCount
            
            i = i + 1
    ' save & close
    xl.DisplayAlerts = FALSE
    objSheet2.saveas(rLog)
    xl.application.quit
End Sub 

    
    
Sub RenameSheets()
    Set xl = CreateObject("Excel.application")
    xl.Application.Workbooks.Open rLog
    xl.Application.Visible = False
	set objSheet1 = xl.ActiveWorkbook.Worksheets(1)
	set objSheet2 = xl.ActiveWorkbook.Worksheets(2)
	objSheet1.Name = "Inventory"
	objSheet2.Name = "File Counts"
 	xl.DisplayAlerts = FALSE
    xl.ActiveWorkbook.saveas(rLog)
    xl.application.Quit
End Sub


Function User(s)
sSource = s
Set objExcel = CreateObject("Excel.Application")
Set objWorkbook = objExcel.Workbooks.Open(sSource)

intRow = 3
	sformatted = objExcel.Cells(intRow, 12).Value
	sCoded = objExcel.Cells(intRow, 17).Value
	sChecked = objExcel.Cells(intRow, 18).Value
	
	If sformatted = "7" Then
		sformatted = "Krum"
	ElseIf sformatted = "10" Then
		sformatted = "Iris"
	ElseIf sformatted = "9" Then
		sformatted = "Nadia"
	ElseIf sformatted = "21" Then
		sformatted = "Jonatha"
	ElseIf sformatted = "22" Then
		sformatted = "Yuri"
	ElseIf sformatted = "23" Then
		sformatted = "Maria"
	ElseIf sformatted = "14" Then
		sformatted = "Zsofia"
	ElseIf sformatted = "5" Then
		sformatted = "Mitch"
	ElseIf sformatted = "3" Then
		sformatted = "Youness"
	Else 
		sformatted = "Error"
	End If 	

	If sCoded = "7" Then
		sCoded = "Krum"
	ElseIf sCoded = "10" Then
		sCoded = "Iris"
	ElseIf sCoded = "9" Then
		sCoded = "Nadia"
	ElseIf sCoded = "21" Then
		sCoded = "Jonatha"
	ElseIf sCoded = "22" Then
		sCoded = "Yuri"
	ElseIf sCoded = "23" Then
		sCoded = "Maria"
	ElseIf sCoded = "14" Then
		sCoded = "Zsofia"
	ElseIf sCoded = "5" Then
		sCoded = "Mitch"
	ElseIf sCoded = "3" Then
		sCoded = "Youness"
	Else 
		sCoded = "Error"
	End If 	
	
	If sChecked = "7" Then
		sChecked = "Krum"
	ElseIf sChecked = "10" Then
		sChecked = "Iris"
	ElseIf sChecked = "9" Then
		sChecked = "Nadia"
	ElseIf sChecked = "21" Then
		sChecked = "Jonatha"
	ElseIf sChecked = "22" Then
		sChecked = "Yuri"
	ElseIf sChecked = "23" Then
		sChecked = "Maria"
	ElseIf sChecked = "14" Then
		sChecked = "Zsofia"
	ElseIf sChecked = "5" Then
		sChecked = "Mitch"
	ElseIf sChecked = "3" Then
		sChecked = "Youness"
	Else 
		sChecked = "Error"
	End If 	
    'Wscript.Echo "Formatted By: " & objExcel.Cells(intRow, 12).Value
    'Wscript.Echo "Coded By: " & objExcel.Cells(intRow, 17).Value
    'Wscript.Echo "Checked By: " & objExcel.Cells(intRow, 18).Value
    'Wscript.Echo "LastName: " & objExcel.Cells(intRow, 4).Value

objExcel.Quit

End Function 

