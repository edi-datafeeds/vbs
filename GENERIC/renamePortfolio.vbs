Dim rootFolder, objFS, objShell, objFolder, listDir, objRE 'declaring folders objects

Dim objFS_file, objFiles, listFiles, dateConCat, iTime 'declaring files objects

Dim numArgs


Set numArgs = WScript.Arguments

'rootFolder = "O:\Upload\Acc\274"

rootFolder = numArgs(0)

searchFile( rootFolder )

Function searchFile( rootFolder )

	Set objFS_file = CreateObject("Scripting.FileSystemObject")

	Set objFiles = objFS_file.GetFolder( rootFolder )
	Set listFiles = objFiles.Files
	
	
	iTime = Time
	iTime = int(left(iTime,2))
	
	if iTime >= 13 and iTime <= 15  then 
        iTime = "_153000" & ".000.txt"

    elseif iTime >= 17 and  iTime <= 21  then 
        iTime = "_203000" & ".000.txt"
    else    
        iTime = "_083000" & ".000.txt"
    end if
	
	dateConCat = Year(Now) & Right("0" & Month(Now),2) & Right("0" & Day(Now),2) & iTime
	
	If NOT (objFS_file.FolderExists(rootFolder & "\report_hist")) Then
	    ' Delete this if you don't want the MsgBox to show
	    'MsgBox("Local folder doesn't exists, creating...")
	    
	    objFS_file.CreateFolder(rootFolder & "\report_hist" )
	End If
	
	
	For Each objFile In listFiles
		
		'WScript.Echo objFile.Name
		
		a=Split( objFile.Name, "_")
		
		objFS_file.CopyFile rootFolder & "\" & objFile.Name, rootFolder & "\" & a(0) & "_Report_" & dateConCat
		objFS_file.CopyFile rootFolder & "\" & objFile.Name, rootFolder & "\report_hist\" & a(0) & "_Report_" & dateConCat
		objFS_file.DeleteFile rootFolder & "\" & objFile.Name 
		
	Next

End Function