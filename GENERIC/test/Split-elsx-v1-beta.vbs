Public Sub ar_ServiceChargeDumpProcess() 
     '//Declarations
    Dim wbBook As Workbook 
    Dim wsData As Worksheet 
    Dim rng As Range 
    Dim lngRows As Long 
    Dim rngCopyFrom As Range 
    Dim wbnewBook As Workbook 
     
     '//Environment
    With Application 
        .ScreenUpdating = True 
        .Calculation = xlCalculationAutomatic 
        .DisplayAlerts = True 
    End With 
     
     '//Objects
    Set wbBook = ThisWorkbook 
    Set wsData = wbBook.Worksheets("Data") 
     
     '//Processes
    With wsData 
        lngRows = .Range("$A65536").End(xlUp).Row 
    End With 
     
    With wsData 
        Set rng = .Range("A1:K" & lngRows) 
    End With 
     
    With rng 
        .AutoFilter Field:=6, Criteria1:="Service Charge Invoice" 
    End With 
     
    With wsData 
        Set rngCopyFrom = .Range("A1:K" & lngRows).SpecialCells(xlCellTypeVisible) 
    End With 
     
    Set wbnewBook = Workbooks.Add 
    wbnewBook.SaveAs "C:\Delete\AR_ServiceCharge" _ 
    & Format(Date, "mmddyyyy") 
    wbnewBook.Sheets(1).Name = "Data" 
     
    rngCopyFrom.Copy _ 
    wbnewBook.Worksheets("Data").Range("A1") 
     
    With rng 
        .AutoFilter 
    End With 
     
    ActiveWindow.Zoom = 75 
    wbnewBook.Save 
    wbnewBook.Close 
     
     
     '//Cleanup
    Set wbBook = Nothing 
    Set wbnewBook = Nothing 
    Set wsData = Nothing 
    Set rng = Nothing 
    Set rngCopyFrom = Nothing 
     
    Application.ScreenUpdating = True 
    Application.Calculation = xlCalculationAutomatic 
    Application.DisplayAlerts = True 
     
End Sub 