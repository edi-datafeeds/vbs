// Configuration
 
// Remote file search for
var FILEPATH = "/home/user/myfile.dat";
// Session to connect to
var SESSION = "session";
// Path to winscp.com
var WINSCP = "c:\\program files\\winscp\\winscp.com";
 
var filesys = WScript.CreateObject("Scripting.FileSystemObject");
var shell = WScript.CreateObject("WScript.Shell");
   
var logfilepath = filesys.GetSpecialFolder(2) + "\\" + filesys.GetTempName() + ".xml";
 
var p = FILEPATH.lastIndexOf('/');
var path = FILEPATH.substring(0, p);
var filename = FILEPATH.substring(p + 1);
 
var exec;
 
// run winscp to check for file existence
exec = shell.Exec("\"" + WINSCP + "\" /log=\"" + logfilepath + "\"");
exec.StdIn.Write(
    "option batch abort\n" +
    "open \"" + SESSION + "\"\n" +
    "ls \"" + path + "\"\n" +
    "exit\n");
 
// wait until the script finishes
while (exec.Status == 0)
{
    WScript.Sleep(100);
    WScript.Echo(exec.StdOut.ReadAll());
}
 
if (exec.ExitCode != 0)
{
    WScript.Echo("Error checking for file existence");
    WScript.Quit(1);
}
 
// look for log file
var logfile = filesys.GetFile(logfilepath);
 
if (logfile == null)
{
    WScript.Echo("Cannot find log file");
    WScript.Quit(1);
}
 
// parse XML log file
var doc = new ActiveXObject("MSXML2.DOMDocument");
doc.async = false;
doc.load(logfilepath);
 
doc.setProperty("SelectionNamespaces", 
    "xmlns:w='http://winscp.net/schema/session/1.0'");
 
var nodes = doc.selectNodes("//w:file/w:filename[@value='" + filename + "']");
 
if (nodes.length > 0)
{
    WScript.Echo("File found");
    // signalize file existence to calling process;
    // you can also continue with processing (e.g. downloading the file)
    // directly from the script here
    WScript.Quit(0);
}
else
{
    WScript.Echo("File not found");
    WScript.Quit(1);
}