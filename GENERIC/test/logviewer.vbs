option explicit


'**************************************************************************
' 		 GENERIC isql.exe Script		09/12/2010
' 	
' Script for parsing the connection details from the config file
'
' Arguments in order (0) Server Name
'		     (1) SQL Script location
'		     (2) Location of Config File	
'**************************************************************************

'***** MAIN *****

'***** VARIABLES *****

	dim fso, f, cmdline, SVR, SQL, CONF, WshShell, step1, numArgs, MyArray, s, ts, f1, OUT, TSK, STP,sMonth
	Dim sDay,lYear,sYear,sTime,LogFile,Dest,contents, objFSO,objFolder,objFile,objTextFile,TBL,DIR,LOC 
	


'***** Parsed Arguments *****


	numArgs = WScript.Arguments.Count

	TSK = WScript.Arguments.Item(0)



'		MsgBox TBL & DIR & LOC & SVR & CONF & TSK & STP

	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		lYear = mid(Year(Now),1,4)
		sYear = mid(Year(Now),1,4)
		sTime = (now)
	
	LogFile = "O:\AUTO\logs\" & TSK & "\" & sYear & sMonth & sDay & "_" & TSK & ".txt"
	
  


'***** Processing *****

Set WshShell = WScript.CreateObject("WScript.Shell")


step1=WshShell.Run("O:\AUTO\Apps\Logs\KiwiLogViewer.exe " & LogFile)
'step1=WshShell.Run("O:\AUTO\Apps\LogViewer\monkeyLogViewer.exe " & LogFile)


WScript.Quit
