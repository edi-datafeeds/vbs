' ****************************************************************
' GENERIC PUSHFTP.VBS
'
'     Script for pushing files to EDI ftp boxes
' 
' Arguments in order (0) Local path
'                    (1) Remote path
'                    (2) File extension
'
' ****************************************************************

' **** MAIN ****
	
' **** VARIABLES ****

	dim WshShell,sLocalPath,sRemotePath,sFileext,sOpsFtp,sDirection,sXRC,sLog 

	

' **** PREPARATION ****
	
	Set colArgs = WScript.Arguments
 
	sLocalPath = "-local " & replace(colArgs(0),"\","/") &" "
	sRemotePath = "-remote " & replace(colArgs(1),"\","/") &" "
	sFileext = "-fileext " & colArgs(2) & " "
	sOpsFtp = "O:\AUTO\Apps\opsftp\dist\opsftp.jar "
	sDirection = "-" & colArgs(4) & " "
	sXRC = "-XCRC on "
	sSite = "-site " & colArgs(3) & " "
	If colArgs(3) = "WALLSTR" Then
		sXRC = "-XCRC on "
	ElseIf colArgs(3) = "MERGENT" Then 
		sXRC = "-XCRC off "	
	Else 	
		sXRC = "-XCRC on "	
	End If 
	TSK = colArgs(5)
			
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		sYear = mid(Year(Now),1,4)

	'sLog = "-log o:\AUTO\logs\Ftp.log"
	sLog = "-log O:\AUTO\logs\" & TSK & "\" & sYear & sMonth & sDay & "_" & TSK & ".txt"
	
	Set WshShell = WScript.CreateObject("WScript.Shell")


' **** PROCESSING ****

	If colArgs(3) = "both" Then
	
	' PULL FILE DOTCOM
	sSite = "-site dotcom "
	ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog)
		If ftp1 = "1" Then
		ftp2=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)	
		End If

	' PUSH FILE TO DOTNET
	sSite = "-site dotnet "
	ftp2=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)
		If ftp2 = "1" Then
		ftp2=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)	
		End If
		 
	Else 
	
		' PULL FILE 
	'sSite = User Defined
	ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)
		If ftp1 = "1" Then
		ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)	
		End If

	End if
' **** END MAIN **** 