' Connect to a SQL Server Database

Dim seq, actTime, fileName, insertCnt, updateCnt, errorCnt, recTotal

Dim ms_Array(5), my_Array(5)


Call msSQL()
Call mySQL()

Private Sub msSQL()
	
	Const adOpenStatic = 3
	Const adLockOptimistic = 3
	
	Set objConnection = CreateObject("ADODB.Connection")
	Set objRecordSet = CreateObject("ADODB.Recordset")
	
	objConnection.Open "Provider=SQLOLEDB;Data Source=192.168.12.172;" & _
	        		   "Trusted_Connection=true;Initial Catalog=client;" & _
	             	   "User ID=sa;Password=K376:lcnb;"
	
	
	If objConnection.State = 1 Then
		'MsgBox "connected"
		Wscript.Echo "connected MS SQL"
	Else
		'MsgBox "connection failed"
		Wscript.Echo "connection failed"
	End If
	        			
	objRecordSet.Open "SELECT TOP 1 acttime, " & _ 
					  	"file_name, seq, insert_cnt, update_cnt, error_cnt " & _ 
					  "FROM wca.dbo.tbl_opslog " & _ 
					  "ORDER BY acttime DESC ", _
	        			objConnection, adOpenStatic, adLockOptimistic        			
	
	objRecordSet.MoveFirst
	
	'WScript.Echo objRecordSet.RecordCount
	
	Do Until objRecordset.EOF
	
		seq = objRecordset.Fields.Item("seq")
		actTime = objRecordset.Fields.Item("acttime")
		fileName = objRecordset.Fields.Item("file_name")
		insertCnt = objRecordset.Fields.Item("insert_cnt")
		updateCnt =	objRecordset.Fields.Item("update_cnt")
		errorCnt =	objRecordset.Fields.Item("error_cnt")
		
		ms_Array(0) = seq
		ms_Array(1) = actTime
		ms_Array(2) = fileName
		ms_Array(3) = insertCnt
		ms_Array(4) = updateCnt
		ms_Array(5) = errorCnt
		MSrecTotal = insertCnt + updateCnt + errorCnt
			
'	    Wscript.Echo "MS Time: " & ms_Array(1) & Chr(13) & _
'          	Chr(13) & "MS File: " & fileName & Chr(13) & _
'	      	Chr(13) & "MS Inserts: " & insertCnt & _
'	      	Chr(13) & "MS Updates: " & updateCnt & _
'	      	Chr(13) & "MS Errors: " & errorCnt & Chr(13) & _
'	      	Chr(13) & "MS Total: " & recTotal
	
	    objRecordset.MoveNext
	Loop
	
	objRecordSet.Close
	objConnection.Close
	
	Set objRecordSet = NOTHING
	Set objConnection = NOTHING

End Sub


Private Sub mySQL()
	
	Const adOpenStatic = 3
	Const adLockOptimistic = 3
	
	db_server = "192.168.12.160"
	db_user = "sa"
	db_pass = "K376:lcnb"
	db_name = "wca"
	
	
	Set objConnection = CreateObject("ADODB.Connection")
	Set objRecordSet = CreateObject("ADODB.Recordset")
	
	strConnectString = "DRIVER={MySQL ODBC 5.1 Driver};" & "SERVER=" & db_server & ";" _
					& " DATABASE=" & db_name & ";" & "UID=" & db_user & ";PWD=" & db_pass & "; OPTION=3"
	
	objConnection.Open strConnectString
	
	If objConnection.State = 1 Then
		WScript.Echo "connected MY SQL"
	Else
		Wscript.Echo "connection failed"
	End If
	        			
	objRecordSet.Open "SELECT acttime, " & _ 
					  	"file_name, seq, insert_cnt, update_cnt " & _ 
					  "FROM wca.tbl_opslog " & _ 
					  "ORDER BY acttime DESC LIMIT 1", _
	        			objConnection, adOpenStatic, adLockOptimistic        			
	
	objRecordSet.MoveFirst
	
	'WScript.Echo objRecordSet.RecordCount
	
	Do Until objRecordset.EOF
	
		seq = objRecordset.Fields.Item("seq")
		actTime = objRecordset.Fields.Item("acttime")
		fileName = objRecordset.Fields.Item("file_name")
		insertCnt = " Inserts " & objRecordset.Fields.Item("insert_cnt")
		updateCnt =	" Update " & objRecordset.Fields.Item("update_cnt")
		
		my_Array(0) = seq
		my_Array(1) = actTime
		my_Array(2) = fileName
		my_Array(3) = insertCnt
		my_Array(4) = updateCnt
	    my_Array(5) = errorCnt
	    MYrecTotal = my_Array(3) + my_Array(4) + my_Array(5)
	    
	    Wscript.Echo "MS Time: " & ms_Array(1) & vbTab & "MY Time: " & my_Array(1) & Chr(13) & _
          	Chr(13) & "MS File: " & ms_Array(2) & vbTab & "MY File: " & my_Array(2) & Chr(13) & _
	      	Chr(13) & "MS Inserts: " & ms_Array(3) & vbTab & "MY Inserts: " & my_Array(3) & _
	      	Chr(13) & "MS Updates: " & ms_Array(4) & vbTab & "MY Updates: " & my_Array(4) & _
	      	Chr(13) & "MS Errors: " & ms_Array(5) & vbTab & "MY Errors: " & my_Array(5) & Chr(13) & _
	      	Chr(13) & "MS Total: " & MSrecTotal & vbTab & "MY Total: " & MYrecTotal
			
	    Wscript.Echo actTime & _
	       vbTab & fileName & _
	       vbTab & seq & _
		   vbTab & insertCnt & _
		   vbTab & updateCnt
	
	    objRecordset.MoveNext
	Loop
	
	objRecordSet.Close
	objConnection.Close
	
	Set objRecordSet = NOTHING
	Set objConnection = NOTHING
	
End Sub