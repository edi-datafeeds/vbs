option explicit
' ****************************************************************
' GENERIC FilesEx.VBS
'
'    This Script is used to check files exist and appends to the named log the file path and byte size to be used for comparison.
'    The log files are stored here: "O:\AUTO\logs\" and the final folder name is based on what you parse from the cmdline

' EXAMPLE CMDLINE
' Long Date 20110302| O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618i_not_US_CA\PreLDateSuf.618 -_2 WcaWebload_2
' Short Date 110302 | O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618i_not_US_CA\PreSDateSuf.618 -_2 WcaWebload_2
' Dashed Long Date 2011-04-05 | O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618\PreDLDateSuf.618 - WcaWebload_2
' Dashed Short Date 11-04-05 | O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618\PreDSDateSuf.618 - WcaWebload_2
' Incremental Files yymmdd_2.ext| O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618i_not_US_CA\PreDSDateSuf.618 - WcaWebload_2 i

' CMDLINE BREAKDOWN
' O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs is this file, must be called and fed the following parameters
' Param0: Source File, can be a real word and/or date or use a pseudonym ie, PreLDateSuf (20110209) or PreSDateSuf (110209) to get the date eg, O:\Datafeed\WCA\618\PreldateSuf.618
' Param1: Prefix & Suffix part of the file name. Prefix should be before the "-" and Suffix after (Pre-Suf) If no pre or suf you must include the "-"
' Param2: Task File Name without Ext. This will also become the folder created in "O:\AUTO\logs\". eg, WcaWebload_2 would yeild O:\AUTO\logs\WcaWebload_2\yyyymmdd_WcaWebload_2.txt
' Param3: If the file is an incremental and add "i" as the final argument will get you the increment number based on the time of day. 
'         12:00 to 15:00 = Inc 2 
'	  16:00 to 21:00 = Inc 3
'	  All other hours = Inc 1
' ****************************************************************



' **** MAIN ****
	
' **** VARIABLES ****

	
	dim fso, WshShell, numArgs, sFile, sPath, sExt, sMonth, sDay, sYear, Source, Dest, x, y, Pre, Log,LogFile,sTime,sInc,sDate
	Dim lDate, sPre, dPre, PreSdate, PreLdate, SdateSuf, SdateLuf, sSuf, fs, objFSO,objReadFile,contents,objFolder,objFile,objTextFile,lYear,inc 
	Dim filesys, filetxt, getname, path, rPath, tmpFile, FName,s,ftpcmd,oexec,sSite,i,D,fPath,fPathUrl,cmdline,MyArray,CONF,SVR,Server,User,Pass
' **** PREPARATION ****

	if Len(Month(Now))=1 then 
	sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
	
	lYear = mid(Year(Now),1,4)
	sYear = mid(Year(Now),3,4)
	sTime = (now)
	D=0
	
	
	'Checks to see if the correct number of paramters have been set
	
	Set numArgs = WScript.Arguments
	Set WshShell = WScript.CreateObject("WScript.Shell")
    Set fso = CreateObject("Scripting.FileSystemObject")	        

	'Arguments
	
	Source = numArgs(0)	
	sSite = numArgs(1)
	tmpFile = numArgs(2)
	Pre = numArgs(3)
	Log = numArgs(4)
	CONF = "o:\auto\configs\FTPServers.cfg"

	If numArgs.length = 5 then
		
		Call splitPre()
		Call splitSource()
		Call Prod()
			
			'wscript.quit
	
	ElseIf numArgs.length = 6 then
		
		If numArgs(5)= "i" Then
			sInc = numArgs(5)
			Call getInc()
			Call splitPre()
			Call splitSource()
			Call Prod()
		Else
			i=numArgs(5)
			Call getDate(D)
			Call splitPre()
			Call splitSource()
			Call Prod()
		End If

	ElseIf numArgs.length = 7 then
		
	
		sInc = numArgs(5)
		i=numArgs(6)
		Call getDate(D)
		Call getInc()
		Call splitPre()
		Call splitSource()
		Call Prod()
	Else 
		msgbox "Syntax Err:Incorrect Number of Arguments"
		wscript.quit
	
	end if
	
'******************************************* Split Source String **********************************************************	
	

	'Split the Source string into fullpath,full filename and ext

	Sub splitSource()
	'msgbox "Source: " & Source
	x = Len(Source)
	for y = x to 1 step -1
		if mid(Source, y, 1) = "\" or mid(Source, y, 1) = "/" then
		    sFile = mid(Source, y+1)
   		   	'MsgBox "sFile: " & sFile
   		    sPath = mid(Source, 1, y-0)
			fPath = Replace(sPath,"\","/")
			
			'MsgBox "sPath: " & sPath
		    exit for
		end if
		
		if mid(Source, y, 1) = "." then
			sExt = mid(Source, y-0)
			'msgbox "sExt: " & sExt
		end if
		
	next 
 		
		if sFile = "PreLDateSuf" & sExt then
			Call sPreLongDateSuf()
		end if
		
		if sFile = "PreSDateSuf" & sExt then
			Call sPreShortDateSuf()
		end If
	
		if sFile = "PreDLDateSuf" & sExt then
			Call sPreLDashedDateSuf()
		End If
		
		if sFile = "PreDSDateSuf" & sExt then
			Call sPreSDashedDateSuf()
		
		end If
					 
 	end Sub	

'******************************************** Split Prefix-Suffix String ******************************************************	

'FORMAT ANY PREFIX OR SUFFIX APPLIED ON THE COMMAND LINE. NOTE: THE HYPHEN "-" IS ALWAYS REQUIRED ON THE COMMAND LINE BUT IS NOT USED IF NOTHING ADDED BEFORE OR AFTER.
'ONLY CHARACTURES BEFORE AND FATER THE "-" IS USED. I.E. "AL-_1" WOULD YEILD "AL20110328_1.620"

	Sub splitPre()

	  sPre = Pre
	  x = Len(Pre)
	  for y = x to 1 step -1
		if mid(Pre, y, 1) = ":" then
			sPre = mid(Pre, 1, y-1)
				'msgbox "Pre: " & sPre
			sSuf = mid(Pre, y+1, len(pre) - y)
				'msgbox "Suf: " & sSuf
			exit for
		end if
	   next
	
	end Sub
	
	
'****************************** GET INCREMENT NUMBER BASED ON TIME ****************************************************
 
'o:\Datafeed\Equity\620i\PreLDateSuf.620 - eod i
'Increment number based on time of day. Will need to change this if you want to run an inc out of sequence. 
'The "i" will insert the "_" and inc number i.e "_1"

Sub getInc()	

	sTime = Time
	sTime = int(left(sTime,2))
	'msgbox sTime
	
	if numArgs(1)= "123Trans" then
		if sTime >= 12 and sTime <= 14  then 
			sInc = "_1"
			log = numArgs(1) & sInc
		elseif sTime >= 17 and  sTime <= 23  then 
			sInc = "_2"
			log = numArgs(1) & sInc
		else	
			sInc = "_1"
			log = numArgs(1) & sInc
		end if
	ElseIf numArgs(1)= "CABTrans" then
		if sTime >= 11 and sTime <= 13  then 
			sInc = "_1"
			log = numArgs(1) & sInc
	
		elseif 	sTime >= 14 and  sTime <= 15  then 
			sInc = "_2"
			log = numArgs(1) & sInc
		elseif 	sTime >= 16 and  sTime <= 17  then 
			sInc = "_3"
			log = numArgs(1) & sInc
		elseif 	sTime >= 18 and  sTime <= 19  then 
			sInc = "_4"
			log = numArgs(1) & sInc
		else	
			sInc = "_5"
			log = numArgs(1) & sInc
		end If	
	Elseif numArgs(4)= "SMF4" And numArgs(4)= "SMF4_FTP" then		
		sInc = "_2"
		log = numArgs(4)
	Elseif numArgs(4)= "smf_inc" then	
		sInc = "_1"
		log = numArgs(4)	
	Else
		if sTime >= 14 and sTime <= 16  then 
			sInc = "_2"
			log = numArgs(4) & sInc
			If tmpFile = "15022_Inc" Then
				Pre = "edi_:_153000"
			End if	
	
		elseif 	sTime >= 17 and  sTime <= 23  then 
			sInc = "_3"
			log = numArgs(4) & sInc
			If tmpFile = "15022_Inc" Then
				Pre = "edi_:_203000"
			End if	
		else	
			sInc = "_1"
			log = numArgs(4) & sInc
			If tmpFile = "15022_Inc" Then
				Pre = "edi_:_083000"
			End if	
		end If	
	End If 
sTime = (now)

End Sub 

	
'****************************** SOURCE SHORT Prefix & SUFFIX DATE **********************************************************	

	
	Sub sPreShortDateSuf()

		'-- Create the Custom filename
		'msgbox sPre & "= sPre"
		'msgbox sSuf & "= sSuf"
		sFile = sPre & sYear & sMonth & sDay & sSuf & sInc & sExt
		If tmpFile = "15022_Inc" Then
			sFile = sPre & sYear & sMonth & sDay & sSuf & sExt
		End if	
		If tmpFile = "smf_inc" Then
			sFile = sPre & sYear & sMonth & sDay & sSuf & sExt
		End if	
		If tmpFile = "smf4" Then
			sFile = sPre & sYear & sMonth & sDay & sSuf & sExt
		End if	

	End Sub

'******************************* SOURCE LONG PREFIX & SUFFIX DATE **********************************************************	
	
	Sub sPreLongDateSuf()
		

		'-- Create the Custom filename
		'msgbox sPre & "= sPre"
		'msgbox sSuf & "= sSuf"
		sFile = sPre & lYear & sMonth & sDay & sSuf & sInc & sExt
		If tmpFile = "15022_Inc" Then
			sFile = sPre & lYear & sMonth & sDay & sSuf & sExt
		End if	
		If tmpFile = "smf_inc" Then
			sFile = sPre & lYear & sMonth & sDay & sSuf & sExt
		End if	
		If tmpFile = "smf4" Then
			sFile = sPre & lYear & sMonth & sDay & sSuf & sExt
		End if	
	End Sub
	
	
'******************************* SOURCE Long DASHED PREFIX & SUFFIX DATE **********************************************************	

	'o:\Upload\Acc\185\feed\PreDLDateSuf.txt STANDING_FULL_- eod
		
		Sub sPreLDashedDateSuf()
	
	
		'-- Create the Custom filename
		'msgbox sPre & "= sPre"
		'msgbox sSuf & "= sSuf"
		sFile = sPre & lYear & "-" & sMonth & "-" & sDay & sSuf & sInc & sExt
		If tmpFile = "15022_Inc" Then
			sFile = sPre & lYear & "-" & sMonth & "-" & sDay & sSuf & sExt
		End if	
		If tmpFile = "smf_inc" Then
			sFile = sPre & lYear & "-" & sMonth & "-" & sDay & sSuf & sExt
		End if	
		If tmpFile = "smf4" Then
			sFile = sPre & lYear & "-" & sMonth & "-" & sDay & sSuf & sExt
		End if	
	End Sub

'******************************* SOURCE Long DASHED PREFIX & SUFFIX DATE **********************************************************	
	
	
	'o:\Upload\Acc\185\feed\PreDSDateSuf.txt STANDING_FULL_- eod
	
	Sub sPreSDashedDateSuf()
			
	
		'-- Create the Custom filename
		'msgbox sPre & "= sPre"
		'msgbox sSuf & "= sSuf"
		sFile = sPre & sYear & "-" & sMonth & "-" & sDay & sSuf &  sInc & sExt
		If tmpFile = "15022_Inc" Then
			sFile = sPre & sYear & "-" & sMonth & "-" & sDay & sSuf & sExt
		End if	
		If tmpFile = "smf_inc" Then
			sFile = sPre & sYear & "-" & sMonth & "-" & sDay & sSuf & sExt
		End if	
		If tmpFile = "smf4" Then
			sFile = sPre & sYear & "-" & sMonth & "-" & sDay & sSuf & sExt
		End if	
	End Sub


'******* Read Config *****************

Sub ReadConfig(a)
	Dim fso, f1, ts, s
	Const ForReading = 1
	Set fso = CreateObject("Scripting.FileSystemObject")   

	' Read the contents of the file.
	' Response.Write "Reading file <br>"
	
	SVR = a
	
	Set ts = fso.OpenTextFile(CONF, ForReading)

	do 
		s = ts.ReadLine
		
		MyArray = Split(s, vbTab)	
		'MyArray = 
		
		'msgbox "|" & MyArray(0) & "|"
		
		if MyArray(0) = SVR Then 			
			cmdline = s			
			exit do

		end if
				
	loop while NOT ts.AtEndOfStream
	cmdline = "-S " & MyArray(1) & " -U " & MyArray(2) & " -P " & MyArray(3) 
	Server = MyArray(1)
	User = MyArray(2)
	Pass = MyArray(3)
	'msgbox cmdline
	ts.Close

End Sub 


'******* Production *****************

Sub Prod()
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	End If 
	LogFile = "O:\AUTO\logs\" & log & "\" & lYear & sMonth & sDay & "_" & log & ".html"
	Dest = "O:\AUTO\logs\" & log & "\"



If sSite = "both" Then
	Call ftp()
	sSite = "DOTCOM"
	Call ReadConfig(sSite)
	Call CheckFTP()
	sSite = "DOTNET"
	Call ReadConfig(sSite)
	Call CheckFTP()
	sSite = "OPS_COM"
	Call ReadConfig(sSite)
	Call CheckFTP()	
	
ElseIf sSite = "NET" Then
	sSite = "DOTNET"
	Call ReadConfig(sSite)
	Call CheckFTP()

ElseIf sSite = "OPS_COM" Then
	sSite = "OPS_COM"
	Call ReadConfig(sSite)
	Call CheckFTP()

ElseIf sSite = "COM" Then
	sSite = "DOTCOM"
	Call ReadConfig(sSite)
	Call CheckFTP()

ElseIf sSite = "EMTS" Then
	sSite = "EMTS"
	Call ReadConfig(sSite)
	Call CheckFTP()
	sSite = "EMTSNET"
	Call ReadConfig(sSite)
	Call CheckFTP()
	sSite = "OPS_COM_EMTS"
	Call ReadConfig(sSite)
	Call CheckFTP()
ElseIf sSite = "EMTSNET" Then
	sSite = "EMTSNET"
	Call ReadConfig(sSite)
	Call CheckFTP()
ElseIf sSite = "OPS_COM_EMTS" Then
	sSite = "OPS_COM_EMTS"
	Call ReadConfig(sSite)
	Call CheckFTP()

End If 



end Sub


'***************

Sub sAppend()


		
		' Create the File System Object
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		
		' Check that the WriteDirectory folder exists
		If objFSO.FolderExists(Dest) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		 ' Create the WriteDirectory folder if folder does not exist
		   Set objFolder = objFSO.CreateFolder(Dest)
		   'WScript.Echo "Just created " & Dest
		End If
		
		'msgbox "LogFile Test: " & LogFile
		If objFSO.FileExists(LogFile) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		   Set objFile = objFSO.CreateTextFile(LogFile)
		   'Wscript.Echo "Just created " & LogFile
		End If
		
		set objFile = nothing
		set objFolder = nothing
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		Const ForAppending = 8		
		
		Set objTextFile = objFSO.OpenTextFile _
		(LogFile, ForAppending, True)
		
		
		' Writes WriteFile every time you run this VBScript
		objTextFile.WriteLine(contents)
		objTextFile.Close
		
		
	end sub	

		'*************		
			
	
			'msgbox LogFile & " Appended to " & sPath
	




Sub getDate(D)

sDay = datepart("w", now)

if sDay = 2 then
	D=3
	
Else
	If numArgs.length = 7 Then
		D=numArgs(6)
		

	Else
		D=numArgs(5)
	End if	
	
end If

if Len(Month(Now -D))=1 then 
	sMonth="0" & month(now -D)
else
	sMonth=month(now -D)
end if

if Len(Day(Now-D))=1 then 
	sDay = "0" & day(Now -D)
else
	sDay = day(Now -D)
end if

'sYear = Year(Now -D)
	lYear = mid(Year(Now -D),1,4)
	sYear = mid(Year(Now -D),3,4)

' Build filename
sDate = sYear & sMonth & sDay




'wScript.echo "Day: " & sDay	
'WScript.echo "Month: " & sMonth	
'wScript.echo "Year: " & sYear    	
'wScript.echo "Date: " & sDate	

end Sub

' **** END MAIN ****

Sub CheckFTP()
Set filesys = CreateObject("Scripting.FileSystemObject")
Set filetxt = filesys.CreateTextFile("O:\AUTO\Temp\ftp\" & tmpFile & ".txt", True)
path = filesys.GetAbsolutePathName("O:\AUTO\Temp\ftp\" & tmpFile & ".txt")
getname = filesys.GetFileName(path)

	filetxt.WriteLine("open " & Server)
	filetxt.WriteLine(User)
	filetxt.WriteLine(Pass)
	filetxt.WriteLine("cd " & sPath)
	filetxt.WriteLine("ls")
	filetxt.WriteLine("close")
	filetxt.WriteLine("bye")
	filetxt.Close
	
	If filesys.FileExists(path) Then
   'wscript.Echo ("Your file, '" & getname & "', has been created.")
  
	End If
	
    ftpcmd="O:\AUTO\Temp\ftp\" & tmpFile & ".txt"    'add full path is necessary
	sFile=sFile    'at root of ftp site
	set wshshell=createobject("wscript.shell")
	set oexec=wshshell.exec("ftp -s:" & ftpcmd)
	'wscript.Echo ftpcmd
	Do while oexec.status=0 And i < 1 : i = i +1 : loop
	s=oexec.stdout.readall
	'wscript.Echo s
	Set wshshell=nothing
	'sSite = "COM"
	sTime = (now)
	fPathUrl = "<a href=ftp://" & User & ":" & Pass & "@" & Server & "/" & fPath & " target=new>"& sPath &"</a>" 
	if instr(1,s,sFile,1)<>0 Then
		
		contents ="<p><span class=ok>" & sTime & " | File: " & fPathUrl & sFile & " | Server: " & sSite & "</span></p>"
		'msgbox sPath & sFile & " | Bytesize = " & iByteSize & " | Log File = " & LogFile & sTime
	    'wscript.Echo contents
	    'WScript.echo sTime & " | Successfully FTP'd " & sFile& " to " & sPath & " on " & sSite & " server"
	    call sAppend()
	Else
		sTime = (now)
	    'wscript.echo sTime & " | Failed to FTP File: " &  sPath & sFile & " to " & sSite
		contents ="<p><span class=failed>" & sTime & " | File: " & fPathUrl & sFile & " | Server: " & sSite & "</span></p>"
		call sAppend()
	End If
End Sub 

 


