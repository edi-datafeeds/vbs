option explicit

' **** VARIABLES ****

	
	dim fso, WshShell, numArgs, sFile, sPath, sExt, sMonth, sDay, sYear, Source, Dest, x, y, Pre, sDate,iByteSize,log,LogFile,sInc,i
	Dim SVR, step1, Mode, sProd, SQL, sAction, sPre, sSuf, fs,  objFSO,objReadFile,contents,objFolder,objFile,objTextFile,lYear,inc 
	Dim stTime,sTime,tDiff,ts,intHours,intMinutes,intSeconds,sPathUrl,wPath,sSql,CONF,ForReading,dPath,dPathUrl
	Dim objRS,objConn,test,ErrMsg,iCMDLINE,strComputerName,wshNetwork,NIC1, Nic, StrIP,Server,Destination
	server = ""
	'Arguments
	Set numArgs = WScript.Arguments
	Set WshShell = WScript.CreateObject("WScript.Shell")
    Set fso = CreateObject("Scripting.FileSystemObject")	        
    Set fs = CreateObject("Scripting.FileSystemObject")	        
	Set wshNetwork = WScript.CreateObject( "WScript.Network" )
	
	strComputerName = wshNetwork.ComputerName
	'WScript.Echo "Computer Name: " & strComputerName
	
	Set NIC1 = GetObject("winmgmts:").InstancesOf("Win32_NetworkAdapterConfiguration")
	
	For Each Nic in NIC1
	
		if Nic.IPEnabled then
		
		StrIP = Nic.IPAddress(i)	
		
		end If
	
	Next
	
	'assign variable arguments if they exist	
	
	CONF="O:\Auto\Configs\DbServers.cfg"
	'Date Function
	if Len(Month(Now))=1 then 
	sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
	
	lYear = mid(Year(Now),1,4)
	sYear = mid(Year(Now),3,4)
	sTime = (now)
	stTime = (now)

	'Checks to see if the correct number of paramters have been set
		Source = numArgs(0)	
		Destination = "O:\Datafeed\Prices\EOD_Prices\inbound\" & numArgs(1) & "\"
		Pre = numArgs(2)
		Log = numArgs(3)

	If numArgs.length = 4 then
		iCMDLINE = "O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs " & numArgs(0)& " " & numArgs(1)& " " & numArgs(2)& " " & numArgs(3)
		iCMDLINE = Replace(iCMDLINE,"\","/")
		Call splitPre()
		Call splitSource()		
		Call sCopyPricesFiles()
	ElseIf numArgs.length = 5 then
		i = numArgs(3)

	Else 
		msgbox "Syntax Err: Incorrect Number of Arguments. Requires Source File, type, Prefix:Suffix and Taskfile"
		wscript.quit
	end if
	
'******************************************* Split Source String **********************************************************	
	

	'Split the Source string into fullpath,full filename and ext

	Sub splitSource()
	'msgbox "Source: " & Source
	x = Len(Source)
	for y = x to 1 step -1
		if mid(Source, y, 1) = "\" or mid(Source, y, 1) = "/" then
		    sFile = mid(Source, y+1)
   		   	'MsgBox "sFile: " & sFile
   		    sPath = mid(Source, 1, y-0)
   		    wPath = Replace(sPath,"\","/")
   		    dPath = Replace(Destination,"\","/")
   		    sPathUrl = "<a href=file:///" & wPath & " target=new>"& wPath &"</a>" 
   		    dPathUrl = "<a href=file:///" & dPath & " target=new>"& dPath &"</a>" 
			'MsgBox "sPath: " & sPath
		    exit for
		end if
		
		if mid(Source, y, 1) = "." then
			sExt = mid(Source, y-0)
			'msgbox "sExt: " & sExt
		end if
		
	next 
 		
		if sFile = "YYYYMMDD" & sExt then
			Call sPreLongDateSuf()
		end if
		
		if sFile = "YYMMDD" & sExt then
			Call sPreShortDateSuf()
		end If
	
		if sFile = "YYYY-MM-DD" & sExt then
			Call sPreLDashedDateSuf()
		End If
		
		if sFile = "YY-MM-DD" & sExt then
			Call sPreSDashedDateSuf()
		
		end If
					 
 	end Sub	

'******************************************** Split Prefix-Suffix String ******************************************************	

'FORMAT ANY PREFIX OR SUFFIX APPLIED ON THE COMMAND LINE. NOTE: THE HYPHEN "-" IS ALWAYS REQUIRED ON THE COMMAND LINE BUT IS NOT USED IF NOTHING ADDED BEFORE OR AFTER.
'ONLY CHARACTURES BEFORE AND FATER THE "-" IS USED. I.E. "AL-_1" WOULD YEILD "AL20110328_1.620"

	Sub splitPre()

	  sPre = Pre
	  x = Len(Pre)
	  for y = x to 1 step -1
		if mid(Pre, y, 1) = ":" then
			sPre = mid(Pre, 1, y-1)
				'msgbox "Pre: " & sPre
			sSuf = mid(Pre, y+1, len(pre) - y)
				'msgbox "Suf: " & sSuf
			exit for
		end if
	   next
	
	end Sub
	
	
'****************************** SOURCE SHORT Prefix & SUFFIX DATE **********************************************************	

	
	Sub sPreShortDateSuf()			  
		sFile = sPre & sYear & sMonth & sDay & sSuf & sInc & sExt
	End Sub

'******************************* SOURCE LONG PREFIX & SUFFIX DATE **********************************************************	
	
	Sub sPreLongDateSuf()
		sFile = sPre & lYear & sMonth & sDay & sSuf & sInc & sExt
	End Sub
	
	
'******************************* SOURCE Long DASHED PREFIX & SUFFIX DATE **********************************************************	

	Sub sPreLDashedDateSuf()
		sFile = sPre & lYear & "-" & sMonth & "-" & sDay & sSuf & sInc & sExt
	End Sub

'******************************* SOURCE Long DASHED PREFIX & SUFFIX DATE **********************************************************	
	
	Sub sPreSDashedDateSuf()
		sFile = sPre & sYear & "-" & sMonth & "-" & sDay & sSuf &  sInc & sExt
	End Sub



'************************ VALIDATE & Copy Price Files **************************************************************************

	Sub sCopyPricesFiles()
		
		if fs.FileExists(sPath & sFile) = False then
		  	
			'msgbox sTime & " | " & sPath & sFile & " File Failed to Generate, Please Try Again"
			contents ="<p><span class=failed>" & sTime & " | " & tDiff & " | <b>Failed to Generate File:</b> " & sPathUrl & sFile & " </span></p><p><span class=failed><b>Command Line:</b> " & iCMDLINE & "</span></p>"
			call sAppend()
		
			sSql = "INSERT INTO opslog(taskfile, lpath, file, status, Diff, bytesize, Seq, CMDLINE, Server, Machine, errormsg) VALUES ('" & log & "', '" & wPath &"', '" & sFile & "', 'Failed', '" & tDiff & "', '" & iByteSize & "', '" & sInc & "', '" & iCMDLINE & "', '" & Server & "', '" & StrIP & "', '" & Err.Description & "')"
			Call OpenDbCon(CONF, sSql)
	
		Else 
			' Check that the WriteDirectory folder exists
			If fs.FolderExists(Destination) Then
				Set objFolder = fs.GetFolder(Destination)
			   	fs.CopyFile sPath & sFile, Destination & "\"
			Else
			 ' Create the WriteDirectory folder if folder does not exist
			   Set objFolder = fs.CreateFolder(Destination)
			   'WScript.Echo "Just created " & Dest
				If fs.FolderExists(Destination) Then
					Set objFolder = fs.GetFolder(Destination)
				   	fs.CopyFile sPath & sFile, Destination & "\"
					if fs.FileExists(Destination & sFile) = False then
					  	
						'msgbox sTime & " | " & sPath & sFile & " File Failed to Generate, Please Try Again"
						contents ="<p><span class=failed>" & sTime & " | " & tDiff & " | <b>Failed to Generate File:</b> " & dPathUrl & sFile & " </span></p><p><span class=failed><b>Command Line:</b> " & iCMDLINE & "</span></p>"
						call sAppend()
					
						sSql = "INSERT INTO opslog(taskfile, lpath, file, status, Diff, bytesize, Seq, CMDLINE, Server, Machine, errormsg) VALUES ('" & log & "', '" & dPath &"', '" & sFile & "', 'Failed', '" & tDiff & "', '" & iByteSize & "', '" & sInc & "', '" & iCMDLINE & "', '" & Server & "', '" & StrIP & "', '" & Err.Description & "')"
						Call OpenDbCon(CONF, sSql)
				
					Else 
					   	iByteSize=GetFileSize
						contents ="<p><span class=ok>" & sTime & " | " & tDiff & " | <b>Bytesize:</b> " & iByteSize & " | <b>File:</b> " & dPathUrl & sFile & " </span></p><p><span class=ok><b>Command Line:</b> " & iCMDLINE & "</span></p>"
						'msgbox sPath & sFile & " | Bytesize = " & iByteSize & " | Log File = " & LogFile & sTime
						Call sAppend()
						sSql = "INSERT INTO opslog(taskfile, lpath, file, status, Diff, bytesize, Seq, CMDLINE, Server, Machine, errormsg) VALUES ('" & log & "', '" & dPath &"', '" & sFile & "', 'OK', '" & tDiff & "', '" & iByteSize & "', '" & sInc & "', '" & iCMDLINE & "', '" & Server & "', '" & StrIP & "', '" & Err.Description & "')"
						Call OpenDbCon(CONF, sSql)
					
					End If 
				   	
				Else
				 ' Create the WriteDirectory folder if folder does not exist
				   Set objFolder = fs.CreateFolder(Destination)
				   'WScript.Echo "Just created " & Dest
				   
				End If
			   
			End If
		End if
	End Sub
				


'*********************************

'Return the length of a file or -1 if it does not exist

function GetFileSize()
Dim f
  GetFileSize = -1

  Set fs = CreateObject("Scripting.FileSystemObject")

  if fs.FileExists(sPath & sFile) = True then
    set f = fs.GetFile(sPath & sFile)
    GetFileSize = f.size
  end if
  if fs.FolderExists(sPath & sFile) = True then
    set f = fs.GetFolder(sPath & sFile)
    GetFileSize = f.size
  end if

Set f = Nothing
Set fs = Nothing
end function


'********************************
    	
    	
  	'WScript.Echo sFile
  	
  		

Sub sAppend()
	LogFile = "O:\AUTO\logs\" & log & "\" & lYear & sMonth & sDay & "_" & log & ".html"
	Dest = "O:\AUTO\logs\" & log & "\"
'MsgBox Dest

'MsgBox "In Act - Append Function"



    	'*******************
    	
		'wscript.echo ("Source: " & sPath & sFile), ("Dest: " & dPath & dFile ), ("Action: " & action), ("Log" & log)
		'************
		'MsgBox sPath & sFile
		
		'***** PROCESSING *****
		
		
		'Read file contents
		
		'Display results
		'wscript.echo contents
			
		
		' Create the File System Object
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		
		' Check that the WriteDirectory folder exists
		If objFSO.FolderExists(Dest) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		 ' Create the WriteDirectory folder if folder does not exist
		   Set objFolder = objFSO.CreateFolder(Dest)
		   'WScript.Echo "Just created " & Dest
		End If
		
		'msgbox "LogFile Test: " & LogFile
		If objFSO.FileExists(LogFile) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		   Set objFile = objFSO.CreateTextFile(LogFile)
		   'Wscript.Echo "Just created " & LogFile
		End If
		
		set objFile = nothing
		set objFolder = nothing
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		Const ForAppending = 8		
		
		Set objTextFile = objFSO.OpenTextFile _
		(LogFile, ForAppending, True)
		
		
		' Writes WriteFile every time you run this VBScript
		objTextFile.WriteLine(contents)
		objTextFile.Close
		
		
	end sub	

		'*************		
			
	
			'msgbox LogFile & " Appended to " & sPath
	
	
	
	
Sub getDate(i)

sDay = datepart("w", now)

if sDay = 2 then
	i=3
	
Else
	i=i
	
end If

if Len(Month(Now-i))=1 then 
	sMonth="0" & month(now -i)
else
	sMonth=month(now -i)
end if

if Len(Day(Now-i))=1 then 
	sDay = "0" & day(Now -i)
else
	sDay = day(Now -i)
end if

sYear = Year(Now -i)

' Build filename
sDate = sYear & sMonth & sDay




'wScript.echo "Day: " & sDay	
'WScript.echo "Month: " & sMonth	
'wScript.echo "Year: " & sYear    	
'wScript.echo "Date: " & sDate	

end sub
	
	

' **** END MAIN ****



Private Sub OpenDbCon (p_conf, sSql)
Dim fso, f1, ts, s, uname, pword, prov, pcata, dsource,MyArray,SVRL,ErrMsg
SVRL = "HAT_MY_Diesel"
Const ForReading = 1
prov = "MySQL ODBC 5.1 Driver"
'** Get connection details
Set fso = CreateObject("Scripting.FileSystemObject")   
Set ts = fso.OpenTextFile(p_conf, ForReading)
Do 
  s = ts.ReadLine
  MyArray = Split(s, vbTab)
  If MyArray(0) = SVRL Then
    uname = MyArray(2)
    pword = MyArray(3)
    dsource = MyArray(4) 			
    Exit do
  End if		
Loop while NOT ts.AtEndOfStream
ts.Close
'MsgBox uname & pword & dsource

'** connect to database
On Error Resume Next


set objConn = CreateObject("ADODB.Connection")
  objConn.Open "Driver={"&prov &"};Server=" &dsource &";Database=AutoOps;UID="&uname &";PWD=" &pword &";OPTION=133121;"
  set objRS = CreateObject("ADODB.Recordset")
  objRS.Open "opslog", objConn


        objConn.Execute sSql
		set objRS = Nothing
		objConn.Close
		set objConn = Nothing



If Err.Number <> 0 Then
  'MsgBox Err.Number & " " & Err.Description
  	'ErrMsg = MsgBox ("Error: " & Err.Number & (Chr(13)) & " " & (Chr(10)) & Err.Description & (Chr(13)) & " " & (Chr(10)) & "What would you like to do?",5,"DB Log Error Message")
	'MsgBox ErrMsg
	If ErrMsg = "4" Then
		contents ="<p><span class=note>" & sTime & " | " & tDiff & " | <b>User Retried DB Log Entry:</b> " & sPathUrl & sFile & " </span></p><p><span class=note><b>Command Line:</b> " & iCMDLINE & "</span></p>"
		call sAppend()
		Call OpenDbCon (p_conf, sSql)
	ElseIf ErrMsg = "2" Then	
		contents ="<p><span class=failed>" & sTime & " | " & tDiff & " | <b>User Aborted DB Log Entry:</b> " & sPathUrl & sFile & " </span></p><p><span class=failed><b>Command Line:</b> " & iCMDLINE & "</span></p>"
		call sAppend()

	WScript.Quit(0)
	End if
End If
On Error Goto 0
'WScript.Quit
End Sub