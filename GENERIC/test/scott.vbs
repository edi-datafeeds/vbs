'**************************************************************************
'
' 		     Generic File Archiving Programme
'
' Author Scott Kitchingham
' 	
' Script for ..
'
' Arguments in order (0) SourceFolder e.g. n:\prices\
'		     (1) TargetFolder e.g. r:\prices\
'		     (2) FileAge = 40
'                    (3) Mode = Arch or NoArch
'                    
'
'**************************************************************************

'***** VARIABLES *****

option Explicit

'Script Arguments
Dim SourceFolder, TargetFolder, Fileage, Mode

'General use FileSystemObject
Dim oFS1, oFS1_Folder, oFS1_File

'Log FileSystemObject
Dim oFS2, oFS2_File 

'Strings
Dim sMonth, sDay, sYear, CRLF, Response, sYYYYMMDD

'Counters
Dim iFilesCopied,iFoldersCreated,iAlreadyCopied,iBadCopies,iFilesDeleted,iByteSize,tByteSize, f, fs

Const ForReading = 1, ForWriting = 2, ForAppending = 8

'***** PREPARATION *****

'Checks to see if the correct number of paramters have been set
if WScript.Arguments.Count < 4 Then
  wscript.echo "There are less than 4 Arguments, CANNOT CONTINUE."
  WScript.Quit	
End if

'Load Argument data
SourceFolder = WScript.Arguments.Item(0)
TargetFolder = WScript.Arguments.Item(1)
FileAge = WScript.Arguments.Item(2)
Mode = WScript.Arguments.Item(3)

'Zero counters
iFilesCopied = 0
iFoldersCreated = 0
iAlreadyCopied = 0
iBadCopies = 0
iFilesDeleted = 0
iByteSize = 0
tByteSize = 0

'Set carriage return + line feed string
CRLF = Chr(13) & Chr(10)

'Checks to see if root folders exists
Set oFS1= CreateObject("Scripting.FileSystemObject")
If oFS1.FolderExists(SourceFolder) Then
Else
  wscript.echo "The Source folder doesn't exist, CANNOT CONTINUE."
  WScript.Quit
End If
'If oFS1.FolderExists(TargetFolder) Then
'Else
'  wscript.echo "The Target folder doesn't exist, CANNOT CONTINUE."
'  WScript.Quit	
'End If

'Setup log file datestring
if Len(Month(Now))=1 then 
    sMonth="0" & month(now)
else
    sMonth=month(now)
end if
if Len(Day(now))=1 then 
    sDay = "0" & day(Now)
else
    sDay = day(Now)
end if
sYear = Year(Now)
sYYYYMMDD = sYear & sMonth & sDay

'Initialise log
Set oFS2 = CreateObject("Scripting.FileSystemObject")
Set oFS2_File = oFS2.OpenTextFile("O:\AUTO\Scripts\vbs\GENERIC\" & replace(sourcefolder,":\","_") & "_" & fileage & ".txt", ForAppending, True)
AppendLog "Potential deletions from root path = " & sourcefolder
AppendLog "For files older than days = " & fileage
AppendLog ""

'***** MAIN *****
 
Set oFS1_Folder = oFS1.GetFolder(SourceFolder)
If Mode = "arch" Then
    ArchLevel oFS1_Folder
Else
    CullLevel oFS1_Folder
    
End If

'***** FINISH *****

'Display file stats
Response = MsgBox(CRLF & "------------------------------------------------------------" & CRLF & CRLF & _
                         "              Files copied   : " & iFilesCopied & CRLF & CRLF & _
                         "              Folders created: " & iFoldersCreated & CRLF & CRLF & _
                         "              Already Copied : " & iAlreadyCopied & CRLF & CRLF & _
                         "              Bad Copies     : " & iBadCopies & CRLF & CRLF & _
                         "              Files Deleted  : " & iFilesDeleted & CRLF & CRLF & _
                         "              ByteSize       : " & tByteSize & CRLF & CRLF & _
                         "------------------------------------------------------------" & CRLF, vbOk)
                         
appendLog "------------------------------------------------------------"
appendLog "              Files copied   : " & iFilesCopied
appendLog "              Folders created: " & iFoldersCreated
appendLog "              Already Copied : " & iAlreadyCopied
appendLog "              Bad Copies     : " & iBadCopies
appendLog "              Files Deleted  : " & iFilesDeleted
appendLog "              ByteSize       : " & tByteSize
appendLog "------------------------------------------------------------"

set oFS1_File = nothing
set oFS1_Folder = nothing
set oFS1 = nothing

'Shut log file
oFS2_File.Close
set oFS2 = nothing

WScript.Quit







'***** SUBROUTINES *****

Sub CullLevel(Folder)
DIM Subfolder, oFS3_Folder, oFS3_File, oFS3_Files
    For Each Subfolder in Folder.SubFolders
        Set oFS3_Folder = oFS1.GetFolder(Subfolder.Path)
        Set oFS3_Files = oFS3_Folder.Files
        'Wscript.Echo Subfolder
        For Each oFS3_File in oFS3_Files
            If dateDiff("d", oFS3_File.DateLastModified, Date) > int(fileage) Then
            	iByteSize=GetFileSize
                Wscript.Echo "Deleting file from source archive " & oFS3_File.Path
                iFilesDeleted = iFilesDeleted + 1
                
                AppendLog oFS3_File.Path & iByteSize
                tByteSize=tByteSize+iByteSize
            End If
        Next
        CullLevel Subfolder
    Next
End Sub


Sub ArchLevel(Folder)
DIM Subfolder, oFS3_Folder, oFS3_File, oFS3_Files, TargetFile

    For Each Subfolder in Folder.SubFolders
        Set oFS3_Folder = oFS1.GetFolder(Subfolder.Path)
        Set oFS3_Files = oFS3_Folder.Files
        'Wscript.Echo Subfolder
        'create target folder if not exist
        If oFS1.FolderExists(replace(Subfolder.Path, SourceFolder, TargetFolder)) Then
        Else
            oFS1.CreateFolder(replace(Subfolder.Path, SourceFolder, TargetFolder))
            iFoldersCreated = iFoldersCreated +1
        End If
        For Each oFS3_File in oFS3_Files
            If dateDiff("d", oFS3_File.DateLastModified, Date) > int(fileage) Then
                 TargetFile = replace(oFS3_File.Path, SourceFolder, TargetFolder)
                 If oFS1.FileExists(TargetFile) Then
                     AppendLog "File has already been copied: " & oFS3_File.Path
                     iAlreadyCopied = iAlreadyCopied +1
                 Else
                     oFS1.CopyFile oFS3_File.Path, TargetFile
                     iFilesCopied = iFilesCopied +1
                     On Error Resume Next
                     If Err.Number<>0 Then
                         AppendLog "File copy problem " & err.number, err.description & " " & oFS3_File.Path
                         iBadCopies = iBadCopies +1
                     Else
                         If oFS1.FileExists(TargetFile) Then
                             ' every OK, delete file from source archive
                             AppendLog "Deleting file from source archive " & oFS3_File.Path
                         Else    
                             AppendLog "Target File does not exist: "  & TargetFile
                             iBadCopies = iBadCopies +1
                         End If
                        'On Error Goto 0
                    End If
                End If
            End If
        Next
        ArchLevel Subfolder
   Next
End Sub

'*********************************

'Return the length of a file or -1 if it does not exist

function GetFileSize()

  GetFileSize = -1

  Set fs = CreateObject("Scripting.FileSystemObject")

  if fs.FileExists(oFS3_File.Path) = True then
    set f = fs.GetFile(oFS3_File.Path)
    GetFileSize = f.size
  end if
  if fs.FolderExists(oFS3_File.Path) = True then
    set f = fs.GetFolder(oFS3_File.Path)
    GetFileSize = f.size
  end if

Set f = Nothing
Set fs = Nothing
end function


'********************************

Sub AppendLog(ByVal MessageTxt)
oFS2_File.Write MessageTxt & vbcrlf
End Sub
