' ****************************************************************
'Generic Feed Loader Script		07/04/2011
'
'    This Script is used to check Feed Loaders have worked and appends the outcome to the named log. Can be used for both SMF & WCA
'    The log files are stored here: "O:\AUTO\logs\" and the final folder name is based on what you parse as the 4th argument on the cmdline

' EXAMPLE CMDLINE
' O:\AUTO\Scripts\vbs\GENERIC\Loader.vbs WCA a 1 SQLSVR1
' 

' CMDLINE BREAKDOWN

'Options 
'Typ: "WCA" or "SMF"
'MODE: "a" for Auto - "m" for Manual - Note; Manual mode never exits with errors because you have to close the program

' ****************************************************************

Option explicit

' **** MAIN ****
	
' **** VARIABLES ****

	
	Dim fso, WshShell, numArgs,sMonth, sDay, sYear,  Dest, LogFile,sTime,sInc,Prd1
	Dim objFSO,contents,objFolder,objFile,objTextFile,lYear,Mode,sLog 
	Dim TSK,step1,Typ,wso,Emailer,Prd,pTyp,oInc,rInc
	CONST ForReading = 1, ForWriting = 2, ForAppending = 8
' **** PREPARATION ****

	Set fso = CreateObject("Scripting.FileSystemObject")
	Set wso = Wscript.CreateObject("Wscript.Shell")

	'Checks to see if the correct number of paramters have been set
	
	Set numArgs = WScript.Arguments
	
	'Emailer = "O:\AUTO\Apps\Emailers\edi_port_emailer3\edi_port_emailer.jar -config O:\AUTO\Configs\DbServers.cfg -s iCloud_MySql -l J:\java\Prog\h.patel\EmailAlert\log\ -a -atype ann -prdcd WCA -incr 3
	Emailer = "O:\AUTO\Apps\Emailers\edi_port_emailer3\edi_port_emailer.jar -config O:\AUTO\Configs\DbServers.cfg -s iCloud_MySql -l J:\java\Prog\h.patel\EmailAlert\log\ "

sInc = ""

	'Mode	
	If numArgs(0)= "a" Then
		Mode = "-a "
	Else 
		Mode = "-m "
	End If 
	
	pTyp = numArgs(1)
	Typ = "-atype " & numArgs(1)
	Prd1 = numArgs(2)
	Prd = " -prdcd " & numArgs(2)
	'User= numArgs(4)		
	'User = " -username " & numArgs(4)

	'Log File
	TSK = numArgs(3)
	
	If numArgs.length = 5 Then
		Call getInc()
		oInc = sInc
		rInc = " -incr " & sInc
	Else
		oInc = ""
		rInc = ""
	end if
	
	'Server
		
	
	
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		lYear = mid(Year(Now),1,4)
		sYear = mid(Year(Now),1,4)
		sTime = (now)
	
	LogFile = "O:\AUTO\logs\" & TSK & "\" & sYear & sMonth & sDay & "_" & TSK & ".html"
	Dest = "O:\AUTO\logs\" & TSK & "\"



'***** Processing *****

Set WshShell = WScript.CreateObject("WScript.Shell")

step1=WshShell.Run (Emailer & Mode & Typ & Prd & rInc, 1, true)
'step1=WshShell.Run (Emailer & Mode & Typ & Prd & " -dt 2014-10-21", 1, true)
'step1=WshShell.Run (Emailer & Mode & Typ & Prd & " -username towfik", 1, true)
'tep1=WshShell.Run (Emailer & Mode & Typ & Prd & " -dt 2015-04-30, -username comdirect2", 1, true)


'msgbox Emailer & Mode & Typ& Prd & user
' Validate Load
if step1 = "0" Then
 	contents = sTime & " | " & pTyp & " Emailer for " & Prd1 & " " & oInc & " ran Sucessfully"
	call sAppend()
	'msgbox Typ & " Emailer to " & Prod & " Loaded Sucessfully"	
Else
 	contents = sTime & " | " & pTyp & " Emailer for " & Prd1 & " " & oInc & " Failed to run"
 	 step1 = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER ops@exchange-data.com -PASS BLOCH1 -TO y.migou@exchange-data.com -SUB " & pTyp & " Emailer Error Alert for " & Prd1 & " " & oInc)
 	 'y =     wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1 -TO webmaster@exchange-data.com;ukcorporateactions@citigroup.com;phil.c.davies@citigroup.com -SUB EDI Wincab docs are available") 
	Call sAppend()	
	msgbox TSK & " " & numArgs(2) & numArgs(1) & " Emailer Failed to Send"
	
end if	



Sub sAppend()
	LogFile = "O:\AUTO\logs\" & TSK & "\" & lYear & sMonth & sDay & "_" & TSK & ".html"
	Dest = "O:\AUTO\logs\" & TSK & "\"


'MsgBox "Append Function"

		
		'***** PROCESSING *****
				
		
		' Create the File System Object
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		
		' Check that the WriteDirectory folder exists
		If objFSO.FolderExists(Dest) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		 ' Create the WriteDirectory folder if folder does not exist
		   Set objFolder = objFSO.CreateFolder(Dest)
		   'WScript.Echo "Just created " & Dest
		End If
		'msgbox "LogFile Test: " & LogFile
		If objFSO.FileExists(LogFile) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		   Set objFile = objFSO.CreateTextFile(LogFile)
		   'Wscript.Echo "Just created " & LogFile
		End If
		
		set objFile = nothing
		set objFolder = nothing
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		Const ForAppending = 8		
		
		Set objTextFile = objFSO.OpenTextFile _
		(LogFile, ForAppending, True)
		
		
		' Writes WriteFile every time you run this VBScript
		objTextFile.WriteLine(contents)
		objTextFile.Close
		
	'	WScript.Quit(0)
End sub	

Sub getInc()	

	sTime = Time
	sTime = int(left(sTime,2))
	'msgbox sTime
	

		if sTime >= 12 and sTime <= 17  then 
			sInc = "2"
			TSK = numArgs(3) & "_" & sInc
	
		elseif 	sTime >= 18 and  sTime <= 23  then 
			sInc = "3"
			TSK = numArgs(3) & "_" & sInc
		else	
			sInc = "1"
			TSK = numArgs(3) & "_" & sInc
		end If	


End Sub 
