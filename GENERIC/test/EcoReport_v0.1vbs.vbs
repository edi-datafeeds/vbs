    Option Explicit 'thrown this in to keep track of my variables
     
    ' declare my variables
    Dim fso, objFolder, obFileList, folderpath, fullpath, xl, i
    Dim TheDate, LastRow, objFileList, objSheet, File
    ' Path to where my files are
    folderpath = "\\192.168.2.163\EcoData\Africa\Coded\Multilaterals\AfDB\Data-Portal\"
    ' set up some objects
    Set fso = CreateObject("Scripting.FileSystemObject")
    Set objFolder = fso.GetFolder(folderpath)
    Set objFileList = objFolder.Files
    Set xl = CreateObject("Excel.application")
    ' if my destination folder doesn't need creating, i create it
    if not fso.folderexists("\\192.168.2.163\EcoData\Africa\Import\Reports") then
        fso.CreateFolder("\\192.168.2.163\EcoData\Africa\Import\Reports")
    end if
    ' create xl file with columns if it's not there
    if not fso.fileexists("\\192.168.2.163\EcoData\Africa\Import\Reports\Test.xls") then
        xl.application.workbooks.add
        xl.application.save("\\192.168.2.163\EcoData\Africa\Import\Reports\Test.xls")
        set objSheet = xl.ActiveWorkbook.Worksheets(1)
        i = 1
        objSheet.Columns("A:A").ColumnWidth = "20"
        objSheet.Columns("B:B").ColumnWidth = "15"
        objSheet.Cells(i, 1).value = "File Name"
        objSheet.Cells(i, 1).Font.Bold = True
        objSheet.Cells(i, 2).value = "Date Modified"
        objSheet.Cells(i, 2).Font.Bold = True
        xl.DisplayAlerts = FALSE
        objSheet.saveas("\\192.168.2.163\EcoData\Africa\Import\Reports\Test.xls")
        xl.application.quit
     
    End if
    ' open up the xl sheet
    xl.Application.Workbooks.Open "\\192.168.2.163\EcoData\Africa\Import\Reports\Test.xls"
    xl.Application.Visible = True
    set objSheet = xl.ActiveWorkbook.Worksheets(1)
    ' get todays date and place in a variable
    TheDate = Date()
    ' get the last row in the spreadsheet
    LastRow = objSheet.UsedRange.Rows.Count
    ' just an if statement in case it's the first entry
    ' i leave a line between updates by incrementing lastrow by 2
    ' if you want a bigger gap you could change this to 3, 4, 5 etc
    if LastRow = 1 then
        i = 2
    else
        i = LastRow + 2
    End if
    ' iterate through the files and write details to spreadsheet
    For Each File In objFileList
        fullpath = folderpath & "\" & file.name
        if fso.getextensionname(fullpath) = "xls" then
            objSheet.Cells(i, 1).value = file.name
            objSheet.Cells(i, 2).value = file.DateLastModified
            ' add a line to say when update to spreadsheet was ran
    		objSheet.Cells(i,3).Font.ColorIndex = 11
            objSheet.Cells(i,3).Value = "Updated " & TheDate
            i = i + 1
        end if
    Next
     
    ' save & close
    xl.DisplayAlerts = FALSE
    objSheet.saveas("\\192.168.2.163\EcoData\Africa\Import\Reports\Test.xls")
    xl.application.quit