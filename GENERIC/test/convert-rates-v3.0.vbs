Set WshShell = WScript.CreateObject("WScript.Shell")
'WScript.Echo "Processing information. This might take several minutes."


' Date Function

	if Len(Month(Now))=1 then 
	sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 

		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
	
	lYear = mid(Year(Now),1,4)
	sYear = mid(Year(Now),3,4)
	sTime = (now)
	stTime = (now)

	sDate = lYear & "-" & sMonth & "-" & sDay
	
' End Date Function


Const ForReading = 1

'Count number of rows in file

	Set objFSO = CreateObject("Scripting.FileSystemObject")
	Set objTextFile = objFSO.OpenTextFile _
	    ("O:\Datafeed\Xrates\Feed\Daily\" & sDate & "_Rates.txt", ForReading)
	
	Do While objTextFile.AtEndOfStream <> True
	    strTemp = objTextFile.SkipLine ' or .ReadLine
	Loop
	
	rCount = objTextFile.Line-1
	
	
	objTextFile.Close

' End Row Count


'CInt(rCount)

' Build Array

Set objTextFile = objFSO.OpenTextFile _
    ("O:\Datafeed\Xrates\Feed\Daily\" & sDate & "_Rates.txt", ForReading)

ReDim mArray(CInt(rCount),1) 

j=0

Do Until objTextFile.AtEndOfStream
    
    strNextLine = objTextFile.Readline
    rates = Split(strNextLine , vbTab)
    
    For i = 0 to Ubound(rates)
 
    	If i= 0  Then
    	 'Wscript.Echo  rates(i)
    	 mArray(j,0) = rates(i)
    	 'Wscript.Echo  rates(i)
    	End If 
     	If i= 2  Then
    	 'Wscript.Echo  rates(i)
    	 mArray(j,1) = rates(i)
    	End If 
   	
    Next
    
  	j=j+1  
  	
  	If j = CInt(rCount)+1 Then
  		j=j-1
    End If
  	'Wscript.Echo Ubound(rates) 
Loop

'Wscript.Echo mArray(99,1)
 
'End Build Array 
 
 y = 0
l = 0

For l = 2 To CInt(rCount)-2
y = y+1

	For j = 2 To CInt(rCount)-2
	
			 mCrossH = mArray(l,0)
			 mCrossx = mArray(l,1)
			 base = mArray(l,0)
			 
			 
			 mDown = mArray(j,1)
			 mDownH = mArray(j,0)
			 
			 
			If FormatNumber(mCrossx,,-1) < 1 Then  
			 
				cx =  FormatNumber(mDown,,-1) * FormatNumber(mCrossx,,-1)
			
			Else 
			
				cx =  FormatNumber(mDown,,-1) / FormatNumber(mCrossx,,-1)
			
			End If 
			
			If j = 2 Then
				contents = "Curr,Rate,Feeddate,base,Src"
				Call sAppend(contents,mCrossH)
				Wscript.StdOut.Write(y)
			End If 
			
			contents = mDownH & "," & cx & "," & sDate & "," & base & "," & "ECB-CALC"
			
			Call sAppend(contents,mCrossH)
			'MsgBox mCrossH & "/"& mDownH &" - " &  mCrossx & "/"& mDown &" = " & cx
			 
			 Set mCrossH = Nothing
			 Set mCrossx = Nothing
			 Set mDown = Nothing
			 Set mDownH = Nothing
			 Set cx = Nothing 
		
	Next 
Next 
'Wscript.Echo mArray(99,1)



Sub sAppend(contents,mCrossH)
	LogFile = "O:\Datafeed\Xrates\test\"& mCrossH & "_" & sDate &".csv"
	Dest = "O:\Datafeed\Xrates\test\"
	
		' Create the File System Object
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		
		' Check that the WriteDirectory folder exists
		If objFSO.FolderExists(Dest) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		 ' Create the WriteDirectory folder if folder does not exist
		   Set objFolder = objFSO.CreateFolder(Dest)
		   'WScript.Echo "Just created " & Dest
		End If
		
		'msgbox "LogFile Test: " & LogFile
		If objFSO.FileExists(LogFile) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		   Set objFile = objFSO.CreateTextFile(LogFile)
		   'Wscript.Echo "Just created " & LogFile
		End If
		
		set objFile = nothing
		set objFolder = nothing
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		Const ForAppending = 8		
		
		Set objTextFile = objFSO.OpenTextFile _
		(LogFile, ForAppending, True)
		
		
		' Writes WriteFile every time you run this VBScript
		objTextFile.WriteLine(contents)
		objTextFile.Close
		
		Set contents = Nothing 
	end sub	
	