Sub SplitTable()
Dim wks As Worksheet
Dim c As Range
Dim rngTable As Range
Dim varRowsPerSheet As Variant
Dim lngTotalRows As Variant
Dim intTotalColumns As Integer
Dim strMsg As String
Dim varSheets As Variant
Dim i As Integer
Const cstrTITLE As String = "Split Table"
'--
With ThisWorkbook.Worksheets("Sheet1")
Set c = .Range("A1")
Set rngTable = .Range("A1").CurrentRegion
lngTotalRows = rngTable.Rows.Count
intTotalColumns = rngTable.Columns.Count
'--
' Get nomber of rows per worksheet
LabEnterRows:
strMsg = "Your table contains " & _
lngTotalRows & " rows" & _
IIf(lngTotalRows = _
.Rows.Count, "(Max rows)", "") & _
"." & vbLf & _
"Enter how many rows per worksheet?"
varRowsPerSheet = InputBox( _
Prompt:=strMsg, _
Title:=cstrTITLE)
If varRowsPerSheet = "" Or _
Not IsNumeric(varRowsPerSheet) Then
Exit Sub '>>>
Else
varRowsPerSheet = CLng(varRowsPerSheet)
End If
'--
If varRowsPerSheet > lngTotalRows Then
strMsg = "Entered number of rows per worksheet" & _
"(" & varRowsPerSheet & ")" & vbLf & _
"is greater then total rows (" & _
lngTotalRows & ")." & vbLf & _
"Please enter correct number of rows."
MsgBox strMsg, vbExclamation, cstrTITLE
GoTo LabEnterRows '>>>
End If
End With
'--
' Split Table in several worksheets
varSheets = lngTotalRows / varRowsPerSheet
If CInt(varSheets) < lngTotalRows / varRowsPerSheet Then
varSheets = CInt(varSheets) + 1
End If
strMsg = "Yout Table will be split on " & _
varSheets & " sheets." & vbLf & _
IIf(varSheets > 50, _
"<GREAT NUMBER of sheets!>", "") & vbLf & _
"Do you want to continue?"
If MsgBox(strMsg, vbQuestion + vbYesNo + vbDefaultButton2, _
cstrTITLE) = vbNo Then
Exit Sub '>>>
End If
'--
For i = 1 To varSheets
Set wks = ThisWorkbook.Worksheets.Add
wks.Name = "Part " & i & " of " & _
varSheets & Format(Now(), " yymmdd hhmm")
c.Interior.ColorIndex = 3 ' Mark split point
c.Resize(varRowsPerSheet, intTotalColumns).Copy _
wks.Range("A1")
Set c = c.Offset(varRowsPerSheet, 0)
Next i
End Sub 