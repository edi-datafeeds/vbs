'****************************** GET INCREMENT NUMBER BASED ON TIME ****************************************************
 
'o:\Datafeed\Equity\620i\PreLDateSuf.620 - eod i
'Increment number based on time of day. Will need to change this if you want to run an inc out of sequence. 
'The "i" will insert the "_" and inc number i.e "_1"

Call getInc

Sub getInc()	

	sTime = Time
	sTime = int(left(sTime,2))
	'msgbox sTime
	
	if numArgs(1)= "123Trans" then
		if sTime >= 12 and sTime <= 14  then 
			sInc = "_1"
			log = numArgs(1) & sInc
		elseif sTime >= 17 and  sTime <= 23  then 
			sInc = "_2"
			log = numArgs(1) & sInc
		else	
			sInc = "_1"
			log = numArgs(1) & sInc
		end if
	ElseIf numArgs(1)= "CABTrans" then
		if sTime >= 11 and sTime <= 13  then 
			sInc = "_1"
			log = numArgs(1) & sInc
	
		elseif 	sTime >= 14 and  sTime <= 15  then 
			sInc = "_2"
			log = numArgs(1) & sInc
		elseif 	sTime >= 16 and  sTime <= 17  then 
			sInc = "_3"
			log = numArgs(1) & sInc
		elseif 	sTime >= 18 and  sTime <= 19  then 
			sInc = "_4"
			log = numArgs(1) & sInc
		else	
			sInc = "_5"
			log = numArgs(1) & sInc
		end If	
	Elseif numArgs(4)= "SMF4" And numArgs(4)= "SMF4_FTP" then		
		sInc = "_2"
		log = numArgs(4)
	Elseif numArgs(4)= "smf_inc" then	
		sInc = "_1"
		log = numArgs(4)	
	Else
		if sTime >= 14 and sTime <= 16  then 
			sInc = "_2"
			log = numArgs(6) & sInc
			If numArgs(4) = "t15022_inc" or numArgs(4) = "2012_Nasdaq_sedol" or numArgs(4) = "t15022_Inc_FTP" or numArgs(4) = "2012_Nasdaq_sedol_FTP" Then
				Pre = "edi_:_153000"
			End if	
	
		elseif 	sTime >= 17 and  sTime <= 23  then 
			sInc = "_3"
			log = numArgs(6) & sInc
			If numArgs(4) = "t15022_inc" or numArgs(4) = "2012_Nasdaq_sedol" or numArgs(4) = "t15022_Inc_FTP" or numArgs(4) = "2012_Nasdaq_sedol_FTP" Then
			
				Pre = "edi_:_203000"
			End if	
		else	
			sInc = "_1"
			log = numArgs(6) & sInc
			If numArgs(4) = "t15022_inc" or numArgs(4) = "2012_Nasdaq_sedol" or numArgs(4) = "t15022_Inc_FTP" or numArgs(4) = "2012_Nasdaq_sedol_FTP" Then
				Pre = "edi_:_083000"
			End if	
		end If	
	End If 
sTime = (now)

End Sub 
