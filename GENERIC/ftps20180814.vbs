' **** MAIN ****
	
' **** VARIABLES ****

	dim fso, WshShell, numArgs, sFile, sPath, sExt, sMonth, sDay, sYear, Source, Dest, x, y, Pre, Log,LogFile,sTime,sInc,sDate
	Dim lDate, sPre, dPre, PreSdate, PreLdate, SdateSuf, SdateLuf, sSuf, fs, objFSO,objReadFile,contents,objFolder,objFile,objTextFile,lYear,inc 
	Dim filesys, filetxt, getname, path, rPath, tmpFile, FName,s,ftpcmd,oexec,sSite,i,D,fPath,fPathUrl,cmdline,MyArray,CONF,SVR,Server,User,Pass
	Dim sLocalPath,sDirection,sLog,sOpsFtp,sXRC,sfileext,sRemotePath,ftp2,Remote,sPathUrl,sWait,iByteSize,f,tmpFile1,tmpFile2
	Dim DBCONF,sSql,iCMDLINE,objConn,objRS,cLocalPath,stmpFile,lSite,strComputerName,wshNetwork,NIC1, Nic, StrIP,tmpFile3,ConvertSizeResult
	Dim AWS_Path,AWS_US,AWS_Local,AWS_US_Url, azureTrys
	
' **** PREPARATION ****

	count = 0

	Const ForReading = 1
	Set numArgs = WScript.Arguments
	Set WshShell = WScript.CreateObject("WScript.Shell")
    Set fso = CreateObject("Scripting.FileSystemObject")	        
	Set wshNetwork = WScript.CreateObject( "WScript.Network" )
	Set NIC1 = GetObject("winmgmts:").InstancesOf("Win32_NetworkAdapterConfiguration")

	strComputerName = wshNetwork.ComputerName
	'WScript.Echo "Computer Name: " & strComputerName
	
	For Each Nic in NIC1
	
		if Nic.IPEnabled then
		
		StrIP = Nic.IPAddress(i)	
		
		end If
	
	next
	
'Increment
	sInc = ""
	
'Config
	DBCONF="O:\Auto\Configs\DbServers.cfg"
	
'******** DATE **********
	
	if Len(Month(Now))=1 then 
	sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
	
	lYear = mid(Year(Now),1,4)
	sYear = mid(Year(Now),3,4)
	sTime = (now)
	D=0
	

'Arguments

	Source = numArgs(0)
	cLocalPath = replace(numArgs(0),"\","/") &" "
	sLocalPath = "-local " & replace(numArgs(0),"\","/") &" "
	Remote = numArgs(1)	
	sSite = numArgs(2)
	sDirection = "-" & numArgs(3) & " "
	tmpFile = numArgs(4)
	Pre = numArgs(5)
	Log = numArgs(6)
	sLog = "-log o:\AUTO\logs\" & sYear & "\"& sYear & sMonth & sDay &"_Ftp.log"
	CONF = "o:\auto\configs\FTPServers.cfg"
	sOpsFtp = "O:\AUTO\Apps\opsftp\dist\opsftp.jar "
	sXRC = "-XCRC on "
	
 	If numArgs(2) = "WALLSTR" Then
		sXRC = "-XCRC off "
	ElseIf numArgs(2) = "MERGENT" Then 
		sXRC = "-XCRC off "	
	ElseIf numArgs(2) = "ALACRA" Then 
		sXRC = "-XCRC off "	
	ElseIf numArgs(2) = "MARKETXS" Then 
		sXRC = "-XCRC off "		
	ElseIf numArgs(2) = "Cantors" Then 
		sXRC = "-XCRC off "		
	Else 	
		sXRC = "-XCRC on "	
	End If 


	'Checks to see if the correct number of paramters have been set

	If numArgs.length = 7 then
		iCMDLINE= "O:\AUTO\Scripts\vbs\GENERIC\ftps.vbs " & numArgs(0)& " " & numArgs(1)& " " & numArgs(2)& " " & numArgs(3)& " " & numArgs(4)& " " & numArgs(5)& " " & numArgs(6)	
		iCMDLINE = Replace(iCMDLINE,"\","/")
		Call splitPre()
		Call splitSource()
		Call Prod()
			
			'wscript.quit
	
	ElseIf numArgs.length = 8 then
		iCMDLINE= "O:\AUTO\Scripts\vbs\GENERIC\ftps.vbs " & numArgs(0)& " " & numArgs(1)& " " & numArgs(2)& " " & numArgs(3)& " " & numArgs(4)& " " & numArgs(5)& " " & numArgs(6)& " " & numArgs(7)
		iCMDLINE = Replace(iCMDLINE,"\","/")
		
		If numArgs(7)= "i" Then
			sInc = numArgs(6)
			Call getInc()
			Call splitPre()
			Call splitSource()
			Call Prod()
		Else
			i=numArgs(7)
			Call getDate(D)
			Call splitPre()
			Call splitSource()
			Call Prod()
		End If

	ElseIf numArgs.length = 9 then
		iCMDLINE= "O:\AUTO\Scripts\vbs\GENERIC\ftps.vbs " & numArgs(0)& " " & numArgs(1)& " " & numArgs(2)& " " & numArgs(3)& " " & numArgs(4)& " " & numArgs(5)& " " & numArgs(6)& " " & numArgs(7)& " " & numArgs(8)
		iCMDLINE = Replace(iCMDLINE,"\","/")
	
		sInc = numArgs(7)
		i=numArgs(8)
		Call getDate(D)
		Call getInc()
		Call splitPre()
		Call splitSource()
		Call Prod()
	Else 
		msgbox "Syntax Err:Incorrect Number of Arguments"
		wscript.quit
	
	end if
	
'******************************************* Split Source String **********************************************************	
	

	'Split the Source string into fullpath,full filename and ext

	Sub splitSource()
	'msgbox "Source: " & Source
	x = Len(Remote)
	for y = x to 1 step -1
		if mid(Remote, y, 1) = "\" or mid(Remote, y, 1) = "/" then
		    sFile = mid(Remote, y+1)
   		   	'MsgBox "sFile: " & sFile
   		    sPath = mid(Remote, 1, y-0)
			fPath = Replace(sPath,"\","/")
			
			'MsgBox "sPath: " & sPath
		    exit for
		end if
		
		if mid(Remote, y, 1) = "." then
			sExt = mid(Remote, y-0)
			'msgbox "sExt: " & sExt
		end if
		
	next 
 	sRemotePath = "-remote " & fPath & " "
	sFileext = "-fileext " & sExt & " "
	'msgbox "sExt: " & sExt

		if sFile = "YYYYMMDD" & sExt then
			Call sPreLongDateSuf()
		end if
		
		if sFile = "YYMMDD" & sExt then
			Call sPreShortDateSuf()
		end If
	
		if sFile = "YYYY-MM-DD" & sExt then
			Call sPreLDashedDateSuf()
		End If
		
		if sFile = "YY-MM-DD" & sExt then
			Call sPreSDashedDateSuf()
		
		end If
					 
 	end Sub	

'******************************************** Split Prefix-Suffix String ******************************************************	

'FORMAT ANY PREFIX OR SUFFIX APPLIED ON THE COMMAND LINE. NOTE: THE HYPHEN "-" IS ALWAYS REQUIRED ON THE COMMAND LINE BUT IS NOT USED IF NOTHING ADDED BEFORE OR AFTER.
'ONLY CHARACTURES BEFORE AND FATER THE "-" IS USED. I.E. "AL-_1" WOULD YEILD "AL20110328_1.620"

	Sub splitPre()

	  sPre = Pre
	  x = Len(Pre)
	  for y = x to 1 step -1
		if mid(Pre, y, 1) = ":" then
			sPre = mid(Pre, 1, y-1)
				'msgbox "Pre: " & sPre
			sSuf = mid(Pre, y+1, len(pre) - y)
				'msgbox "Suf: " & sSuf
			exit for
		end if
	   next
	
	end Sub
	
	
'****************************** GET INCREMENT NUMBER BASED ON TIME ****************************************************
 
'o:\Datafeed\Equity\620i\PreLDateSuf.620 - eod i
'Increment number based on time of day. Will need to change this if you want to run an inc out of sequence. 
'The "i" will insert the "_" and inc number i.e "_1"

Sub getInc()	

	sTime = Time
	sTime = int(left(sTime,2))
	'msgbox sTime
	
	if numArgs(1)= "123Trans" then
		if sTime >= 12 and sTime <= 14  then 
			sInc = "_1"
			log = numArgs(1) & sInc
		elseif sTime >= 17 and  sTime <= 23  then 
			sInc = "_2"
			log = numArgs(1) & sInc
		else	
			sInc = "_1"
			log = numArgs(1) & sInc
		end if
	ElseIf numArgs(1)= "CABTrans" then
		if sTime >= 11 and sTime <= 13  then 
			sInc = "_1"
			log = numArgs(1) & sInc
	
		elseif 	sTime >= 14 and  sTime <= 15  then 
			sInc = "_2"
			log = numArgs(1) & sInc
		elseif 	sTime >= 16 and  sTime <= 17  then 
			sInc = "_3"
			log = numArgs(1) & sInc
		elseif 	sTime >= 18 and  sTime <= 19  then 
			sInc = "_4"
			log = numArgs(1) & sInc
		else	
			sInc = "_5"
			log = numArgs(1) & sInc
		end If	
	Elseif numArgs(4)= "SMF4" or numArgs(4)= "SMF4_FTP" then		
		sInc = "_2"
		log = numArgs(4)
	Elseif numArgs(4)= "smf_inc" then	
		sInc = "_1"
		log = numArgs(4)	
	Else
		if sTime >= 13 and sTime <= 16  then 
			sInc = "_2"
			log = numArgs(6) & sInc
			If numArgs(4) = "t15022_inc" or numArgs(4) = "2012_Nasdaq_sedol" or numArgs(4) = "t15022_Inc_FTP" or numArgs(4) = "2012_Nasdaq_sedol_FTP" or numArgs(4) = "Markit_FTP" or numArgs(4) = "MarkitIHS_ftp" or numArgs(4) = "MarkitIHS" Then
				Pre = "edi_:_153000"
				sInc = ""
			End if	
			If numArgs(4) = "Accenture" or numArgs(4) = "Accenture_FTP" or numArgs(4) = "sgcib_355_ftp" or numArgs(4) = "sgcib" or numArgs(4) = "sgcib_357_ftp" or numArgs(4) = "sgcib_357" Then
				Pre = "aptp_tlmca_ca_camkt_:_153000.000"
				sInc = ""
			End if	
			If numArgs(4)= "acc_355_t" or numArgs(4)= "acc_357_t" Then
				Pre = "sedol_Report_:_153000.000"
				sInc = ""
			End if	
			If numArgs(4)= "acc_355" or numArgs(4)= "acc_357" Then
				sInc = ""
			End if	
	
		elseif 	sTime >= 17 and  sTime <= 23  then 
			sInc = "_3"
			log = numArgs(6) & sInc
			If numArgs(4) = "t15022_inc" or numArgs(4) = "2012_Nasdaq_sedol" or numArgs(4) = "t15022_Inc_FTP" or numArgs(4) = "2012_Nasdaq_sedol_FTP" or numArgs(4) = "Markit_FTP" or numArgs(4) = "MarkitIHS_ftp" or numArgs(4) = "MarkitIHS" Then
				Pre = "edi_:_203000"
				sInc = ""
			End if	
			If numArgs(4) = "Accenture" or numArgs(4) = "Accenture_FTP" or numArgs(4) = "sgcib_355_ftp" or numArgs(4) = "sgcib" or numArgs(4) = "sgcib_357" or numArgs(4) = "sgcib_357_ftp" Then
				Pre = "aptp_tlmca_ca_camkt_:_203000.000"
				sInc = ""
			End if	
			If numArgs(4)= "acc_355_t" or numArgs(4)= "acc_357_t" Then
				Pre = "sedol_Report_:_203000.000"
				sInc = ""
			End if				
			If numArgs(4)= "acc_355" or numArgs(4)= "acc_357" Then
				sInc = ""
			End if	
		else	
			sInc = "_1"
			log = numArgs(6) & sInc
			If numArgs(4) = "t15022_inc" or numArgs(4) = "2012_Nasdaq_sedol" or numArgs(4) = "t15022_Inc_FTP" or numArgs(4) = "2012_Nasdaq_sedol_FTP" or numArgs(4) = "Markit_FTP" or numArgs(4) = "MarkitIHS_ftp" or numArgs(4) = "MarkitIHS" Then
				Pre = "edi_:_083000"
				sInc = ""
			End if	
			If numArgs(4) = "Accenture" or numArgs(4) = "Accenture_FTP" or numArgs(4) = "sgcib_355_ftp" or numArgs(4) = "sgcib" or numArgs(4) = "sgcib_357_ftp" or numArgs(4) = "sgcib_357" Then
				Pre = "aptp_tlmca_ca_camkt_:_083000.000"
				sInc = ""
			End if	
			If numArgs(4)= "acc_355_t" or numArgs(4)= "acc_357_t" Then
				Pre = "sedol_Report_:_083000.000"
				sInc = ""
			End if				
			If numArgs(4)= "acc_355" or numArgs(4)= "acc_357" Then
				sInc = ""
			End if	
		end If	
	End If 
sTime = (now)

End Sub 

	
'****************************** SOURCE SHORT Prefix & SUFFIX DATE **********************************************************	

	
	Sub sPreShortDateSuf()

		'-- Create the Custom filename
		'msgbox sPre & "= sPre"
		'msgbox sSuf & "= sSuf"
		sFile = sPre & sYear & sMonth & sDay & sSuf & sInc & sExt
		If numArgs(4) = "t15022_Inc" or numArgs(4) = "2012_Nasdaq_sedol" Then
			sFile = sPre & sYear & sMonth & sDay & sSuf & sExt
		End if	
		If tmpFile = "smf_inc" Then
			sFile = sPre & sYear & sMonth & sDay & sSuf & sExt
		End if	
		If tmpFile = "smf4" Then
			sFile = sPre & sYear & sMonth & sDay & sSuf & sExt
		End if	

	End Sub

'******************************* SOURCE LONG PREFIX & SUFFIX DATE **********************************************************	
	
	Sub sPreLongDateSuf()
		

		'-- Create the Custom filename
		'msgbox sPre & "= sPre"
		'msgbox sSuf & "= sSuf"
		sFile = sPre & lYear & sMonth & sDay & sSuf & sInc & sExt
		If numArgs(4) = "t15022_inc" or numArgs(4) = "2012_Nasdaq_sedol" Then
			sFile = sPre & lYear & sMonth & sDay & sSuf & sExt
		End if	
		If tmpFile = "smf_inc" Then
			sFile = sPre & lYear & sMonth & sDay & sSuf & sExt
		End if	
		If tmpFile = "smf4" Then
			sFile = sPre & lYear & sMonth & sDay & sSuf & sExt
		End if	
	End Sub
	
	
'******************************* SOURCE Long DASHED PREFIX & SUFFIX DATE **********************************************************	

	'o:\Upload\Acc\185\feed\PreDLDateSuf.txt STANDING_FULL_- eod
		
	Sub sPreLDashedDateSuf()
	
	
		'-- Create the Custom filename
		'msgbox sPre & "= sPre"
		'msgbox sSuf & "= sSuf"
		sFile = sPre & lYear & "-" & sMonth & "-" & sDay & sSuf & sInc & sExt
		If numArgs(4) = "t15022_inc" or numArgs(4) = "2012_Nasdaq_sedol" Then
			sFile = sPre & lYear & "-" & sMonth & "-" & sDay & sSuf & sExt
		End if	
		If tmpFile = "smf_inc" Then
			sFile = sPre & lYear & "-" & sMonth & "-" & sDay & sSuf & sExt
		End if	
		If tmpFile = "smf4" Then
			sFile = sPre & lYear & "-" & sMonth & "-" & sDay & sSuf & sExt
		End if	
	End Sub

'******************************* SOURCE Long DASHED PREFIX & SUFFIX DATE **********************************************************	
	
	
	'o:\Upload\Acc\185\feed\PreDSDateSuf.txt STANDING_FULL_- eod
	
	Sub sPreSDashedDateSuf()
			
	
		'-- Create the Custom filename
		'msgbox sPre & "= sPre"
		'msgbox sSuf & "= sSuf"
		sFile = sPre & sYear & "-" & sMonth & "-" & sDay & sSuf &  sInc & sExt
		If numArgs(4) = "t15022_inc" or numArgs(4) = "2012_Nasdaq_sedol" Then
			sFile = sPre & sYear & "-" & sMonth & "-" & sDay & sSuf & sExt
		End if	
		If tmpFile = "smf_inc" Then
			sFile = sPre & sYear & "-" & sMonth & "-" & sDay & sSuf & sExt
		End if	
		If tmpFile = "smf4" Then
			sFile = sPre & sYear & "-" & sMonth & "-" & sDay & sSuf & sExt
		End if	
	End Sub


'******* Read Config *****************

Sub ReadConfig(a)
	Dim fso, f1, ts, s
	Const ForReading = 1
	Set fso = CreateObject("Scripting.FileSystemObject")   

	' Read the contents of the file.
	' Response.Write "Reading file <br>"
	
	SVR = a
	'MsgBox SVR
	
	Set ts = fso.OpenTextFile(CONF, ForReading)

	do 
		s = ts.ReadLine
		
		MyArray = Split(s, vbTab)	
		'MyArray = 
		
		'msgbox "|" & MyArray(0) & "|"
		
		if MyArray(0) = SVR Then 			
			cmdline = s			
			exit do

		end if
				
	loop while NOT ts.AtEndOfStream
	cmdline = "-S " & MyArray(1) & " -U " & MyArray(2) & " -P " & MyArray(3)
	Server = MyArray(1)
	User = MyArray(2)
	Pass = MyArray(3)
	'msgbox cmdline
	ts.Close

End Sub 
'O:/AUTO/Scripts/vbs/GENERIC/test/ftps.vbs o:/upload/acc/274/feed/ /Upload/Acc/274/feed/YYYYMMDD.txt both Upload Accenture edi_: Accenture_ftp i
'******* AWS COPY *******************
Sub AWS_Copy_US()
	sPathUrl = "<a href=file:///" & Source & " target=new>"& Source &"</a>" 

	AWS_US = "\\10.200.12.142\d$\INETPUB\FTPROOT" & replace(sPath,"/","\") & sFile
	AWS_Path = "\\10.200.12.142\d$\INETPUB\FTPROOT" & replace(sPath,"/","\")
	AWS_US_Url = "<a href=file:///" & replace(AWS_Path,"\","/") & " target=new>"& replace(AWS_Path,"\","/") &"</a>" 
	AWS_Local = replace(numArgs(0),"/","\") & sFile
	
	'msgbox AWS_US & "|-|-|" & AWS_Local

	If fso.FileExists(AWS_Local) Then
	fso.CopyFile AWS_Local, AWS_US
	iByteSize = GetFileSize
   'wscript.Echo ("Your file, '" & getname & "', has been created.")
  		contents ="<p><span class=ok>" & sTime & " | " & GetFileSize & " | Local: " & sPathUrl & " | Remote: " & AWS_US_Url & sFile & " | Server: AWS_US_FTP | Action: Copy Over VPN  </span></p>"
		call sAppend() 
	    sSql = "INSERT INTO opslog(taskfile, direction, lpath, rpath, file, status, Seq, CMDLINE, Server, Machine, errormsg) VALUES ('" & log & "','Copy Over VPN', '" & cLocalPath &"','" & sPath &"', '" & sFile & "', 'OK', '" & sInc & "', '" & iCMDLINE & "','AWS_US_FTP','" & StrIP & "', '" & Err.Description & "')"
		'Call OpenDbCon(DBCONF, sSql)
	Else

		sTime = (now)
	    'wscript.echo sTime & " | Failed to FTP File: " &  sPath & sFile & " to " & sSite
		contents ="<p><span class=failed>" & sTime & " | " & GetFileSize & " | Local: " & sPathUrl & sFile & " | Server: AWS_US_FTP | Action: Copy Over VPN </span></p><p><span class=note> <b>Command Line:</b> " & iCMDLINE & "</span></p>"
		call sAppend()

	    sSql = "INSERT INTO opslog(taskfile, direction, lpath, rpath, file, status, Seq, CMDLINE, Server, Machine, errormsg) VALUES ('" & log & "','Copy Over VPN', '" & cLocalPath &"','" & sPath &"', '" & sFile & "', 'Failed', '" & sInc & "', '" & iCMDLINE & "', 'AWS_US_FTP', '" & StrIP & "', '" & Err.Description & "')"
		Call OpenDbCon(DBCONF, sSql)

		sSite = "AWS_US_FTP"
		Call Prod()
	End If
End Sub 

'******* Production *****************

Sub Prod()
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	End If 
	LogFile = "O:\AUTO\logs\" & log & "\" & lYear & sMonth & sDay & "_" & log & ".html"
	Dest = "O:\AUTO\logs\" & log & "\"
	'MsgBox Dest

If sSite = "both" Then

 'MsgBox numArgs(3)
	sSite = "DOTCOM"
	sWait = "No"
	tmpFile1 = tmpFile & "-Com"
	Call FTP(sSite,sWait,tmpFile1)

	sSite = "DOTNET"
	sWait = "Yes"
	tmpFile2 = tmpFile&"-Net"
	sXRC = "-XCRC off "	
	Call FTP(sSite,sWait,tmpFile2)
		
	'sSite = "DOTNETGEN2"
	'sWait = "No"
	'tmpFile3 = tmpFile&"-GEN2"
	'sXRC = "-XCRC off "	
	'Call FTP(sSite,sWait,tmpFile3)
	'Call CheckFTP(tmpFile3)
	
	sSite = "DOTCOM"
	Call ReadConfig(sSite)
	Call CheckFTP(tmpFile1)
	
	azureFTP_winscp( "AZUREFTP_LB" )
	
'	sSite = "Gen2"
'	Call ReadConfig(sSite)
'	Call CheckFTP(tmpFile3)

	
ElseIf sSite = "DOTNET" Then
	sWait = "Yes"
	tmpFile = tmpFile&"-Net"
	Call FTP(sSite,sWait,tmpFile)
	'sSite = "DOTNET"
	'Call ReadConfig(sSite)
	'Call CheckFTP()

ElseIf sSite = "DOTNETGEN2" Then
	sWait = "Yes"
	tmpFile = tmpFile&"-Net"
	Call FTP(sSite,sWait,tmpFile)
	'sSite = "DOTNET"
	'Call ReadConfig(sSite)
	'Call CheckFTP()
	
ElseIf sSite = "DOTCOM" Then
	sWait = "Yes"
	tmpFile = tmpFile&"-COM"
	Call FTP(sSite,sWait,tmpFile)
	'sSite = "DOTCOM"
	'Call ReadConfig(sSite)
	Call CheckFTP(tmpFile)
	
ElseIf sSite = "EMTS" Then
	sWait = "No"
	sSite = "EMTSNET"
	tmpFile1 = tmpFile & "-Net"
	Call FTP(sSite,sWait,tmpFile1)
	
	sSite = "OPS_COM_EMTS"
	sWait = "Yes"
	tmpFile2 = tmpFile & "-Com"
	Call FTP(sSite,sWait,tmpFile2)
	
	sSite = "EMTSNET"
	Call ReadConfig(sSite)
	Call CheckFTP(tmpFile)
	
	
ElseIf sSite = "EMTSNET" Then
	sWait = "Yes"
	tmpFile1 = tmpFile & "-Com"
	Call FTP(sSite,sWait,tmpFile1)
	'Call ReadConfig(sSite)
	'Call CheckFTP()
ElseIf sSite = "OPS_COM_EMTS" Then
	sWait = "Yes"
	tmpFile2 = tmpFile & "-Com"
	Call FTP(sSite,sWait,tmpFile2)
	'Call ReadConfig(sSite)
	'Call CheckFTP()
ElseIf sSite = "ALACRA" Then
	sWait = "Yes"
	Call FTP(sSite,sWait,tmpFile)
	'Call ReadConfig(sSite)
	'Call CheckFTP()
ElseIf sSite = "MARKETXS" Then
	sWait = "Yes"
	Call FTP(sSite,sWait,tmpFile)
	'Call ReadConfig(sSite)
	'Call CheckFTP()	
ElseIf sSite = "Cantors" Then
	sWait = "Yes"
	Call FTP(sSite,sWait,tmpFile)
	'Call ReadConfig(sSite)
	'Call CheckFTP()
ElseIf sSite = "FUNDATA" Then
	sWait = "Yes"
	Call FTP(sSite,sWait,tmpFile)
	'Call ReadConfig(sSite)
	'Call CheckFTP()		
ElseIf sSite = "FUNDATA2" Then
	sWait = "Yes"
	Call FTP(sSite,sWait,tmpFile)
	'Call ReadConfig(sSite)
	'Call CheckFTP()		
ElseIf sSite = "AWS_US_FTP" Then
	sWait = "Yes"
	Call FTP(sSite,sWait,tmpFile)
	'Call ReadConfig(sSite)
	'Call CheckFTP()		
End If 





end Sub


'***************

Sub sAppend()


		
		' Create the File System Object
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		
		' Check that the WriteDirectory folder exists
		If objFSO.FolderExists(Dest) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		 ' Create the WriteDirectory folder if folder does not exist
		   Set objFolder = objFSO.CreateFolder(Dest)
		   'WScript.Echo "Just created " & Dest
		End If
		
		'msgbox "LogFile Test: " & LogFile
		If objFSO.FileExists(LogFile) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		   Set objFile = objFSO.CreateTextFile(LogFile)
		   'Wscript.Echo "Just created " & LogFile
		End If
		
		set objFile = nothing
		set objFolder = nothing
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		Const ForAppending = 8		
		
		Set objTextFile = objFSO.OpenTextFile _
		(LogFile, ForAppending, True)
		
		
		' Writes WriteFile every time you run this VBScript
		objTextFile.WriteLine(contents)
		objTextFile.Close
		
		
	end sub	

		'*************		
			
	
			'msgbox LogFile & " Appended to " & sPath
	
Sub getDate(D)

sDay = datepart("w", now)

if sDay = 2 then
	D=3
	
Else
	If numArgs.length = 8 Then
		D=numArgs(7)
		

	Else
		D=numArgs(6)
	End if	
	
end If

if Len(Month(Now -D))=1 then 
	sMonth="0" & month(now -D)
else
	sMonth=month(now -D)
end if

if Len(Day(Now-D))=1 then 
	sDay = "0" & day(Now -D)
else
	sDay = day(Now -D)
end if

'sYear = Year(Now -D)
	lYear = mid(Year(Now -D),1,4)
	sYear = mid(Year(Now -D),3,4)

' Build filename
sDate = sYear & sMonth & sDay




'wScript.echo "Day: " & sDay	
'WScript.echo "Month: " & sMonth	
'wScript.echo "Year: " & sYear    	
'wScript.echo "Date: " & sDate	

end Sub

' **** END MAIN ****

Sub CheckFTP(t)
	Call GetFileSize()
		Set filesys = CreateObject("Scripting.FileSystemObject")
		stmpFile=t
		sFile=sFile    'at root of ftp site
		'sSite = "COM"
		sTime = (now)
		sPathUrl = "<a href=file:///" & Source & " target=new>"& Source &"</a>" 

		If numArgs(3) = "Upload" then	
			If sSite = "-site DOTNET " Then
				Call AWS_Copy_US()
				'If taskfile = "15022_Accenture" Then
					'MsgBox sSite			
				'Else
				'	Call AWS_Copy_US()
				'end if
			End If 
			Set filetxt = filesys.CreateTextFile("O:\AUTO\Temp\ftp\" & stmpFile & ".txt", True)
			path = filesys.GetAbsolutePathName("O:\AUTO\Temp\ftp\" & stmpFile & ".txt")
			getname = filesys.GetFileName(path)
			
			filetxt.WriteLine("open " & Server)
			filetxt.WriteLine(User)
			filetxt.WriteLine(Pass)
			filetxt.WriteLine("cd " & sPath)
			filetxt.WriteLine("ls")
			filetxt.WriteLine("close")
			filetxt.WriteLine("bye")
			filetxt.Close
			
		'	If filesys.FileExists(path) Then
		   		'wscript.Echo ("Your file, '" & getname & "', has been created.")
		  
		'	End If
			
		    ftpcmd="O:\AUTO\Temp\ftp\" & stmpFile & ".txt"    'add full path is necessary
			
			set wshshell=createobject("wscript.shell")
			set oexec=wshshell.exec("ftp -s:" & ftpcmd)
			'wscript.Echo ftpcmd
			Do while oexec.status=0 And i < 1 : i = i +1 : Loop
			s=oexec.stdout.readall
			'wscript.Echo s
			'Set wshshell=nothing
			fPathUrl = "<a href=ftp://" & User & ":" & Pass & "@" & Server & "/" & fPath & " target=new>"& sPath &"</a>" 
			
			if instr(1,s,sFile,1)<>0 Then
				sTime = (now)
				contents ="<p><span class=ok>" & sTime & " | " & GetFileSize & " | Local: " & sPathUrl & " | Remote: " & fPathUrl & sFile & " | Server: " & SVR & " | Action: " & numArgs(3) & " </span></p>"
			    call sAppend()
			    sSql = "INSERT INTO opslog(taskfile, direction, lpath, rpath, file, status, Seq, CMDLINE, Server, Machine, errormsg) VALUES ('" & log & "','" & numArgs(3) & "', '" & cLocalPath &"','" & sPath &"', '" & sFile & "', 'OK', '" & sInc & "', '" & iCMDLINE & "', '" & SVR & "', '" & StrIP & "', '" & Err.Description & "')"
				'Call OpenDbCon(DBCONF, sSql)
			Else
				'MsgBox "Number: " & Err.Number & " Message: " & Err.Description
				sTime = (now)
			    'wscript.echo sTime & " | Failed to FTP File: " &  sPath & sFile & " to " & sSite
				contents ="<p><span class=failed>" & sTime & " | " & GetFileSize & " | Local: " & sPathUrl & " | Remote: " & fPathUrl & sFile & " | Server: " & SVR & " | Action: " & numArgs(3) & " </span></p><p><span class=note> <b>Command Line:</b> " & iCMDLINE & "</span></p><p><span class=note> <b>Command Line:</b> " & iCMDLINE & "</span></p>"
				call sAppend()
			    sSql = "INSERT INTO opslog(taskfile, direction, lpath, rpath, file, status, Seq, CMDLINE, Server, Machine, errormsg) VALUES ('" & log & "','" & numArgs(3) & "', '" & cLocalPath &"','" & sPath &"', '" & sFile & "', 'Failed', '" & sInc & "', '" & iCMDLINE & "', '" & SVR & "', '" & StrIP & "', '" & Err.Description & "')"
				Call OpenDbCon(DBCONF, sSql)
			End If
		
		ElseIf numArgs(3) = "Download" then
		'MsgBox Source
			If filesys.FileExists(Source & sFile) Then
			iByteSize = GetFileSize
		   'wscript.Echo ("Your file, '" & getname & "', has been created.")
		  		contents ="<p><span class=ok>" & sTime & " | " & GetFileSize & " | Local: " & sPathUrl & sFile & " | Remote: " & fPathUrl & " | Server: " & SVR & " | Action: " & numArgs(3) & " </span></p>"
				call sAppend() 
			    sSql = "INSERT INTO opslog(taskfile, direction, lpath, rpath, file, status, Seq, CMDLINE, Server, Machine, errormsg) VALUES ('" & log & "','" & numArgs(3) & "', '" & cLocalPath &"','" & sPath &"', '" & sFile & "', 'OK', '" & sInc & "', '" & iCMDLINE & "', '" & SVR & "', '" & StrIP & "', '" & Err.Description & "')"
				'Call OpenDbCon(DBCONF, sSql)
			Else
				sTime = (now)
			    'wscript.echo sTime & " | Failed to FTP File: " &  sPath & sFile & " to " & sSite
				contents ="<p><span class=failed>" & sTime & " | " & GetFileSize & " | Local: " & sPathUrl & sFile & " | Server: " & SVR & " | Action: " & numArgs(3) & " </span></p><p><span class=note> <b>Command Line:</b> " & iCMDLINE & "</span></p>"
				call sAppend()
			    sSql = "INSERT INTO opslog(taskfile, direction, lpath, rpath, file, status, Seq, CMDLINE, Server, Machine, errormsg) VALUES ('" & log & "','" & numArgs(3) & "', '" & cLocalPath &"','" & sPath &"', '" & sFile & "', 'Failed', '" & sInc & "', '" & iCMDLINE & "', '" & SVR & "', '" & StrIP & "', '" & Err.Description & "')"
				Call OpenDbCon(DBCONF, sSql)
			End If
		Else 
			Set filetxt = filesys.CreateTextFile("O:\AUTO\Temp\ftp\" & stmpFile & ".txt", True)
			path = filesys.GetAbsolutePathName("O:\AUTO\Temp\ftp\" & stmpFile & ".txt")
			getname = filesys.GetFileName(path)
			
			filetxt.WriteLine("open " & Server)
			filetxt.WriteLine(User)
			filetxt.WriteLine(Pass)
			filetxt.WriteLine("cd " & sPath)
			filetxt.WriteLine("ls")
			filetxt.WriteLine("close")
			filetxt.WriteLine("bye")
			filetxt.Close
			
			
		    ftpcmd="O:\AUTO\Temp\ftp\" & stmpFile & ".txt"    'add full path is necessary
			
			set wshshell=createobject("wscript.shell")
			set oexec=wshshell.exec("ftp -s:" & ftpcmd)
			'wscript.Echo ftpcmd
			Do while oexec.status=0 And i < 1 : i = i +1 : loop
			s=oexec.stdout.readall
			'wscript.Echo s
			'Set wshshell=nothing
			fPathUrl = "<a href=ftp://" & User & ":" & Pass & "@" & Server & "/" & fPath & " target=new>"& sPath &"</a>" 
			
			if instr(1,s,sFile,1)<>0 Then
				sTime = (now)
				contents ="<p><span class=ok>" & sTime & " | " & GetFileSize & " | Local: " & sPathUrl & " | Remote: " & fPathUrl & sFile & " | Server: " & SVR & " | Action: " & numArgs(3) & " </span></p>"
			    call sAppend()
			    sSql = "INSERT INTO opslog(taskfile, direction, lpath, rpath, file, status, Seq, CMDLINE, Server, Machine, errormsg) VALUES ('" & log & "','" & numArgs(3) & "', '" & cLocalPath &"','" & sPath &"', '" & sFile & "', 'OK', '" & sInc & "', '" & iCMDLINE & "', '" & SVR & "', '" & StrIP & "', '" & Err.Description & "')"
				'Call OpenDbCon(DBCONF, sSql)
			Else
				'MsgBox "Number: " & Err.Number & " Message: " & Err.Description
				sTime = (now)
			    'wscript.echo sTime & " | Failed to FTP File: " &  sPath & sFile & " to " & sSite
				contents ="<p><span class=failed>" & sTime & " | " & GetFileSize & " | Local: " & sPathUrl & " | Remote: " & fPathUrl & sFile & " | Server: " & SVR & " | Action: " & numArgs(3) & " </span></p><p><span class=note> <b>Command Line:</b> " & iCMDLINE & "</span></p><p><span class=note> <b>Command Line:</b> " & iCMDLINE & "</span></p>"
				call sAppend()
			    sSql = "INSERT INTO opslog(taskfile, direction, lpath, rpath, file, status, Seq, CMDLINE, Server, Machine, errormsg) VALUES ('" & log & "','" & numArgs(3) & "', '" & cLocalPath &"','" & sPath &"', '" & sFile & "', 'Failed', '" & sInc & "', '" & iCMDLINE & "', '" & SVR & "', '" & StrIP & "', '" & Err.Description & "')"
				Call OpenDbCon(DBCONF, sSql)
			End If
		
		End If 		
End Sub 


'##################################################

Sub FTP(x,z,t)
Dim ErrMsg,sRun
'MsgBox "sSite: " & x
	Call ReadConfig(x)
	
	If z = "Yes" Then
		sWait = "True"
	Else
		sWait = "False"
	End If 		
	lSite = x
	sSite = "-site " & x & " "
	sPathUrl = "<a href=file:///" & Source & " target=new>"& Source &"</a>" 
	fPathUrl = "<a href=ftp://" & User & ":" & Pass & "@" & Server & "/" & fPath & " target=new>"& sPath &"</a>" 
	
	'MsgBox numArgs(3)
	If numArgs(3) = "Check" Then
		
		Call CheckFTP(t)
		
	Else
		sRun = sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog
	
		'msgbox sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog & ", 1," & sWait
		ftp2=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, sWait)
	
		If ftp2 => 1 Then
		    sSql = "INSERT INTO opslog(taskfile, direction, lpath, rpath, file, status, Seq, CMDLINE, Server, Machine, errormsg) VALUES ('" & log & "','" & numArgs(3) & "', '" & cLocalPath &"','" & sPath &"', '" & sFile & "', 'OK', '" & sInc & "', '" & iCMDLINE & "', '" & SVR & "', '" & StrIP & "', '" & Err.Description & "')"
			'Call OpenDbCon(DBCONF, sSql)
	
		ElseIf ftp2 < 0 Then
		    sSql = "INSERT INTO opslog(taskfile, direction, lpath, rpath, file, status, Seq, CMDLINE, Server, Machine, errormsg) VALUES ('" & log & "','" & numArgs(3) & "', '" & cLocalPath &"','" & sPath &"', '" & sFile & "', 'OK', '" & sInc & "', '" & iCMDLINE & "', '" & SVR & "', '" & StrIP & "', '" & Err.Description & "')"
			'Call OpenDbCon(DBCONF, sSql)
		End If 
	
		If sSite = "-site DOTCOM " Then 
	'	MsgBox "Do nothing"
		else 
			Call CheckFTP(t)	
		End If 
		
	End If 
		'MsgBox sSite & " Exit Code: " & ftp2
'		If ftp2 = 1 Then
'			sWait = "True"
			'MsgBox "Back Up FTP"
'			ftp2=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, sWait)	
'				If ftp2 = "1" Then
'					If sDirection = "-Download " Then
'							contents ="<p><span class=failed>" & sTime & " | Failed to Download Twice | Local: " & sPathUrl & " | Remote: " & fPathUrl & sFile & " | Server: " & sSite & " | Direction: " & numArgs(3) & " " & sRun & "  </span></p>"
'							call sAppend()					
'						    sSql = "INSERT INTO opslog(taskfile, direction, lpath, rpath, file, status, Seq, CMDLINE) VALUES ('" & log & "','" & numArgs(3) & "', '" & cLocalPath &"','" & sPath &"', '" & sFile & "', 'Failed'" & sPath &"', '" & sFile & "' Twice', '" & sInc & "', '" & iCMDLINE & "')"
							'Call OpenDbCon(DBCONF, sSql)
'					  	ErrMsg = MsgBox ("Error: " & Err.Number & (Chr(13)) & " " & (Chr(10)) & "Download Failed from " & x & (Chr(13)) & " " & (Chr(10)) & "What would you like to do?",5,"FTP Download Error Message")
'						If ErrMsg = "4" Then
'							contents ="<p><span class=note>" & sTime & " | USER RETRIED | Local: " & sPathUrl & " | Remote: " & fPathUrl & sFile & " | Server: " & sSite & " | Direction: " & numArgs(3) & " <input type=hidden name=but id=but> " & sRun & "</input> </script><button onclick=RunProgram()>Run</button> </span></p>"
'							call sAppend()					
'						    sSql = "INSERT INTO opslog(taskfile, direction, lpath, rpath, file, status, Seq, CMDLINE) VALUES ('" & log & "','" & numArgs(3) & "', '" & cLocalPath &"','" & sPath &"', '" & sFile & "', 'USER RETRIED', '" & sInc & "', '" & iCMDLINE & "')"
							'Call OpenDbCon(DBCONF, sSql)
				
							'MsgBox ErrMsg
'							Call FTP(x,z)
'						ElseIf ErrMsg = "2" Then	
							'MsgBox "Path: "&sPathUrl
'							contents ="<p><span class=note>" & sTime & " | USER CANCELLED | Local: " & sPathUrl & " | Remote: " & fPathUrl & sFile & " | Server: " & sSite & " | Direction: " & numArgs(3) & " <input type=button name=but id=but value= """ & sRun & """ onclick=RunProgram></input>  </span></p>"
'							call sAppend()
'						    sSql = "INSERT INTO opslog(taskfile, direction, lpath, rpath, file, status, Seq, CMDLINE) VALUES ('" & log & "','" & numArgs(3) & "', '" & cLocalPath &"','" & sPath &"', '" & sFile & "', 'USER CANCELLED', '" & sInc & "', '" & iCMDLINE & "')"
							'Call OpenDbCon(DBCONF, sSql)
'						End if
'					Else
'						contents ="<p><span class=failed>" & sTime & " | Failed to Upload Twice | Local: " & sPathUrl & " | Remote: " & fPathUrl & sFile & " | Server: " & sSite & " | Direction: " & numArgs(3) & " " & sRun & "  </span></p>"
'						call sAppend()					
'					    sSql = "INSERT INTO opslog(taskfile, direction, lpath, rpath, file, status, Seq, CMDLINE) VALUES ('" & log & "','" & numArgs(3) & "', '" & cLocalPath &"','" & sPath &"', '" & sFile & "', 'Failed', '" & sInc & "', '" & iCMDLINE & "')"
						'Call OpenDbCon(DBCONF, sSql)
'					End If	
			'	msgbox " Failed"	
'				End If
'		Else 
'			If sWait = "False" Then 

'			Else 
				'MsgBox "Check FTP True"
'				Call CheckFTP(t)	
'			End if 	
'		End If	

End Sub
	 

function GetFileSize()
Dim f
  GetFileSize = -1

  Set fs = CreateObject("Scripting.FileSystemObject")

'MsgBox Source & sFile
  if fs.FileExists(Source & sFile) = True then
    set f = fs.GetFile(Source & sFile)
    'GetFileSize = f.size
    Call ConvertSize(f.Size)
    GetFileSize = ConvertSizeResult
  end if
  if fs.FolderExists(Source & sFile) = True then
    set f = fs.GetFolder(Source & sFile)
    'GetFileSize = f.size
    Call ConvertSize(f.Size)
    GetFileSize = ConvertSizeResult
  end if

Set f = Nothing
Set fs = Nothing
end function

Function ConvertSize(byteSize) 
	dim Size, SizeType, CommaLocate
	Size = byteSize
	
	Do While InStr(Size,",") 'Remove commas from size 
		CommaLocate = InStr(Size,",") 
		Size = Mid(Size,1,CommaLocate - 1) & _ 
		Mid(Size,CommaLocate + 1,Len(Size) - CommaLocate) 
	Loop
	
	SizeType = " Bytes" 
	If Size >= 1024 Then SizeType = " KB" 
	If Size >= 1048576 Then SizeType = " MB" 
	If Size >= 1073741824 Then SizeType = " GB" 
	If Size >= 1099511627776 Then SizeType = " TB" 
	
	Select Case SizeType 
		Case " KB" Size = Round(Size / 1024, 1) 
		Case " MB" Size = Round(Size / 1048576, 1) 
		Case " GB" Size = Round(Size / 1073741824, 1) 
		Case " TB" Size = Round(Size / 1099511627776, 1) 
	End Select
	
	ConvertSizeResult = Size & SizeType 
End Function


Private Sub OpenDbCon (p_conf, sSql)
Dim fso, f1, ts, s, uname, pword, prov, pcata, dsource,MyArray,SVRL,ErrMsg
SVRL = "HAT_MY_Diesel"
Const ForReading = 1
prov = "MySQL ODBC 5.1 Driver"
'** Get connection details
Set fso = CreateObject("Scripting.FileSystemObject")   
Set ts = fso.OpenTextFile(p_conf, ForReading)
Do 
  s = ts.ReadLine
  MyArray = Split(s, vbTab)
  If MyArray(0) = SVRL Then
    uname = MyArray(2)
    pword = MyArray(3)
    dsource = MyArray(4) 			
    Exit do
  End if		
Loop while NOT ts.AtEndOfStream
ts.Close
'MsgBox uname & pword & dsource

'** connect to database
On Error Resume Next


set objConn = CreateObject("ADODB.Connection")
  objConn.Open "Driver={"&prov &"};Server=" &dsource &";Database=AutoOps;UID="&uname &";PWD=" &pword &";OPTION=133121;"
  set objRS = CreateObject("ADODB.Recordset")
  objRS.Open "opslog", objConn


        objConn.Execute sSql
		set objRS = Nothing
		objConn.Close
		set objConn = Nothing



If Err.Number <> 0 Then
  'MsgBox Err.Number & " " & Err.Description
  	'ErrMsg = MsgBox ("Error: " & Err.Number & (Chr(13)) & " " & (Chr(10)) & Err.Description & (Chr(13)) & " " & (Chr(10)) & "What would you like to do?",5,"DB Log Error Message")
	'MsgBox ErrMsg
	If ErrMsg = "4" Then
		contents ="<p><span class=note>" & sTime & " | " & tDiff & " | <b>User Retried DB Log Entry:</b> " & sPathUrl & sFile & " </span></p><p><span class=note><b>Command Line:</b> " & iCMDLINE & "</span></p>"
		call sAppend()
		'Call OpenDbCon (p_conf, sSql)
	ElseIf ErrMsg = "2" Then	
		contents ="<p><span class=failed>" & sTime & " | " & tDiff & " | <b>User Aborted DB Log Entry:</b> " & sPathUrl & sFile & " </span></p><p><span class=failed><b>Command Line:</b> " & iCMDLINE & "</span></p>"
		call sAppend()

	WScript.Quit(0)
	End if
End If
On Error Goto 0
'WScript.Quit
End Sub

'-----------------------------------------------------------------------------------------------

Function azureFTP_winscp( cnfs )
	
	Dim azureSite, cmdAzure, WshShell, ftpPush
	
	'azureSite = "o:\\datafeed\\wca\\680i\\"
	
	Set WshShell = WScript.CreateObject("WScript.Shell")
	
	ReadConfig( cnfs )
	
	'MsgBox Server & " " & User & " " & Pass
	
	azureSite = Replace(numArgs(0),"\","\\")
	azureSite = Replace(numArgs(0),"/","\\")
	
	'WScript.Echo azureSite & sFile & " " & fPath
	
'	cmdAzure = "O:\AUTO\Apps\WinSCP\winscp.com /command"
'	cmdAzure = cmdAzure & " ""open sftp://channel7:1nc3p:T1oN@ftp3.exchange-data.com/"" " 
'	cmdAzure = cmdAzure & """put o:\\datafeed\\wca\\680i\\20180717_1.680 /wca/680i/"" " '
	
	cmdAzure = "O:\AUTO\Apps\WinSCP\winscp.com /command"
	cmdAzure = cmdAzure & " ""open sftp://" & User & ":" & Pass & "@" & Server & "/"" " 
	cmdAzure = cmdAzure & """put " & azureSite & sFile & " " & fPath & """ " 
	
	cmdAzure = cmdAzure & """exit""" 
	
	'WScript.Echo cmdAzure
	
	iCMDLINE = cmdAzure
	
	'O:\AUTO\Apps\WinSCP\winscp.com /command "open sftp://channel7:1nc3p:T1oN@ftp3.exchange-data.com/" "put o:\\datafeed\\wca\\680i\\20180716_3.680 /wca/680i/" "exit"
		
	ftpPush = WshShell.Run ( cmdAzure, 1, TRUE )
	
	'writeFTP_batchFILE( cmdAzure )
	'MsgBox ftpPush
	
	If ftpPush = 0 Then
		
		'WScript.Echo "Success file pushed up"
		
		count = 0
		
		writeToLog( 1 )
		
		validateAzureFTP()
		
	Else
	
		'WScript.Echo cmdOutput
		
		'MsgBox "Error file not pushed up - no connection"
		
		If count < 3 Then 
			
			'WScript.Echo "Error file not pushed up - no connection"
			
			WScript.Sleep 5000
			
			count = count + 1	
			
			azureFTP_winscp( cnfs )
		Else
			
			'WScript.Echo "Error file not pushed up - tried 3 times connection"
			
			writeFTP_batchFILE( cmdAzure )
			
			count = 0
			
			writeToLog( 0 )
			
			'validateAzureFTP()
			
		End If 	
		
	End If
	
End Function

'-----------------------------------------------------------------------------------------------

Function validateAzureFTP( )

	Dim cmdShell, cmdExec, cmdOutput, cmdValid
	
	Set cmdShell = WScript.CreateObject("WScript.Shell")
	set cmdExec = cmdShell.Exec("O:\AUTO\Apps\WinSCP\winscp.com")
	' set up our session
	cmdExec.StdIn.Write("option batch continue" + vbCRLF & _
	   "option batch continue" + vbCRLF & _
	   "option confirm off" + vbCRLF & _
	   "option transfer binary" + vbCRLF & _
	   "option reconnecttime 300" + vbCRLF& _
	   "open sftp://channel7:1nc3p:T1oN@ftp3.exchange-data.com"  + vbCRLF& _
	   "cd " & fPath + vbCrLf & _
	   "ls"+ vbCrLf & _
	   "close"+ vbCrLf & _
	   "bye"+ vbCrLf & _
	   "exit")
	   
	   '"cd /wca/680i/" + vbCrLf & _
	   
	cmdOutput = cmdExec.StdOut.ReadAll()
	
	'cmdValid = instr(1,cmdOutput,"20180716_680_2.txt",1)
	
	cmdValid = instr(1,cmdOutput,sFile,1)
	
	If cmdValid <> 0 Then
		
		'WScript.Echo "Success file exist"
		
		'contents ="<p><span class=ok>" & sTime & " | " & GetFileSize & " | Local: " & sPathUrl & " | Remote: " & fPathUrl & sFile & " | Server: " & SVR & " | Action: " & numArgs(3) & " </span></p>"
			    
	Else
	
		'WScript.Echo cmdOutput
		
		If count < 3 Then 
			
			count = count + 1	
			
			validateAzureFTP()
			
		End If 	
		
	End If
	
End Function

'----------------------------------------------------------------------------------------------- 	

Function writeFTP_batchFILE( azureCMD )
	
	Dim batchFSO, batchFolder, batchFile, batchTextFile
	
	Dim azure_dteNow, azure_varDate, varInc
	
	varInc = GetIncLog()
	
	'------------------------------------------------------------------
	
	azure_dteNow = Date()
	azure_dteNow = DateAdd("d", minV, azure_dteNow)
	
	azure_dteNow = Split(azure_dteNow, "/")
		
	azure_varDate = azure_dteNow(2) & azure_dteNow(1) & azure_dteNow(0) 
	
	'------------------------------------------------------------------
	
	Set batchFSO = CreateObject("Scripting.FileSystemObject")
	
	' Check that the WriteDirectory folder exists
	If batchFSO.FolderExists("o:\AUTO\logs\batchFTP\") Then
	   Set batchFolder = batchFSO.GetFolder("o:\AUTO\logs\batchFTP\")
	Else
	 ' Create the WriteDirectory folder if folder does not exist
	   Set batchFolder = batchFSO.CreateFolder("o:\AUTO\logs\batchFTP\")
	   'WScript.Echo "Just created " & Dest
	End If
	
	'msgbox "LogFile Test: " & LogFile
	If batchFSO.FileExists("o:\AUTO\logs\batchFTP\" & azure_varDate & varInc & "_batch.bat") Then
	   Set batchFolder = batchFSO.GetFolder("o:\AUTO\logs\batchFTP\")
	Else
	   Set batchFile = batchFSO.CreateTextFile("o:\AUTO\logs\batchFTP\" & azure_varDate & varInc & "_batch.bat")
	   'Wscript.Echo "Just created " & LogFile
	End If
	
	set batchFile = nothing
	set batchFolder = nothing
	
	Const ForAppending = 8		
	
	'------------------------------------------------------------------
	
	Set batchTextFile = batchFSO.OpenTextFile _
	("o:\AUTO\logs\batchFTP\" & azure_varDate & varInc & "_batch.bat", ForAppending, True)
	
	batchTextFile.WriteLine( azureCMD )
	batchTextFile.Close
	
End Function 		

'----------------------------------------------------------------------------------------------- 	

Function writeToLog( valuTF )
	
	fPathUrl = "<a href=ftp://" & User & ":" & Pass & "@" & Server & "/" & fPath & " target=new>"& sPath &"</a>" 
	
	'WScript.Echo fPathUrl
	
	
	If valuTF > 0 Then  
		contents = "<p><span class=ok>" & sTime & " | " & GetFileSize & " | Local: " & sPathUrl & " | Remote: " & fPathUrl & sFile & " | Server: " & SVR & " | Action: " & numArgs(3) & " </span></p>"
	Else 
		contents ="<p><span class=failed>" & sTime & " | " & GetFileSize & " | Local: " & sPathUrl & " | Remote: " & fPathUrl & sFile & " | Server: " & SVR & " | Action: " & numArgs(3) & " </span></p>" & _
				  "<p><span class=note> <b>Command Line:</b> " & iCMDLINE & "</span></p><p><span class=note> <b>Command Line:</b> " & iCMDLINE & "</span></p>"
	End If 			
	
	call sAppend() 
	
End Function

'-----------------------------------------------------------------------------------------------------------------------------------------

Function GetIncLog()
	
	Dim sTimeH, varInc
	
	varInc = "_1"
	
	sTimeH = Int( timeUTC() )
	
	If sTimeH >= 17 Then 
		
		'WScript.Echo "17"
		
		varInc = "_3"
	
	ElseIf  sTimeH >= 13 Then 
		
		'WScript.Echo "13"
		
		varInc = "_2"
		
	Else 
	
		'WScript.Echo "07"
	End If 

	GetIncLog = varInc
	
End Function

'-----------------------------------------------------------------------------------------------------------------------------------------

Function timeUTC()
	
	Dim dateTime, varHour, vTemp

	Set dateTime = CreateObject("WbemScripting.SWbemDateTime")    
	dateTime.SetVarDate (Now())
	'echoMsg(  "Local Time:  " & dateTime )
	'WScript.echo  "UTC Time: " & dateTime.GetVarDate (false)
	
	vTemp = Split( dateTime.GetVarDate (false), " ")
	
	varHour = Split( vTemp(1), ":")
	
	'WScript.echo varHour(0)
	
	timeUTC = varHour(0)
	
End Function  