'03/08/2012 - DJ - Simple script for generic file copy from Source to destination
'-------------------------------------------------------------------------------

'Option Explicit

Set numArgs = WScript.Arguments

'numArgs(0)
'numArgs(1)

 'Generic source & destination argument pass
		Dim source
		source = numArgs(0)
		Dim Destination
		Destination = numArgs(1)

'Copy specific file in source directory
	'Dim source
	'source = "C:\users\d.johnson\desktop\Testfile\MyTestFile.txt"
	'Dim Destination
	'Destination = "C:\users\d.johnson\desktop\TestFolder\MyTestFile"

'Copy Any file in source folder
	'Dim source
	'source = "C:\users\d.johnson\desktop\testfile\*.*"
	'Dim Destination
	'Destination = "C:\users\d.johnson\desktop\TestFolder\"

Dim FSO
Set FSO = CreateObject("Scripting.FileSystemObject")
FSO.CopyFile source, Destination
Set FSO = nothing
