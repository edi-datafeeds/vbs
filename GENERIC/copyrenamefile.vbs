option explicit


'**************************************************************************
' 		 GENERIC COPYRENAMEFILE.VBS
' 	
' Script for copying files from one directory to another
'
' Arguments in order (0) Source Path
'		     (1) Destination Path
'		     (2) sfName
'		     (3) dfName
'**************************************************************************

'***** MAIN *****

'***** VARIABLES *****

	dim fso, WshShell, sPath, dPath, sfName, dfName, numArgs, sMonth, sDay, sYear, sCustomName 
'***** PREPERATION *****


	numArgs = WScript.Arguments.Count

	sPath = WScript.Arguments.Item(0)
	sfName = WScript.Arguments.Item(1)
	dPath = WScript.Arguments.Item(2)
	dfName = WScript.Arguments.Item(3)
	

	Set WshShell = WScript.CreateObject("WScript.Shell")
	Set fso = CreateObject("Scripting.FileSystemObject")




'***** PROCESSING *****


		
		
		
		'-- Build custom output file name, ensuring that
		  '		both sMonth and sDay are always two digits
		  
		 ' if Len(Month(Now))=1 then 
		  	sMonth="0" & month(now)
		 ' else
		 ' 	sMonth=month(now)
		 ' end if
		  
		 ' if Len(Day(now))=1 then 
		 ' 	sDay = "0" & day(Now)
		 ' else
		 ' 	sDay = day(Now)
		 ' end if
		'
		 ' sYear = mid(Year(Now),3,4)
		  
		  '-- Create the Custom filename
		 
		 '   sCustomName = "AL" & sYear & sMonth & sDay
	
			
		




	'wscript.echo sPath & sCustomName & sfName ,  dPath & dfName

	fso.MoveFile sPath & sfName ,  dPath & dfName
	
	
'***** END MAIN *****