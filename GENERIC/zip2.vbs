' ****************************************************************
' GENERIC FilesEx.VBS
'
'    This Script is used to check files exist and appends to the named log the file path and byte size to be used for comparison.
'    The log files are stored here: "O:\AUTO\logs\" and the final folder name is based on what you parse from the cmdline

' EXAMPLE CMDLINE
' Long Date 20110302| O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618i_not_US_CA\PreLDateSuf.618 -_2 WcaWebload_2
' Short Date 110302 | O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618i_not_US_CA\PreSDateSuf.618 -_2 WcaWebload_2
' Dashed Long Date 2011-04-05 | O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618\PreDLDateSuf.618 - WcaWebload_2
' Dashed Short Date 11-04-05 | O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618\PreDSDateSuf.618 - WcaWebload_2
' Incremental Files yymmdd_2.ext| O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618i_not_US_CA\PreDSDateSuf.618 - WcaWebload_2 i

' CMDLINE BREAKDOWN
' O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs is this file, must be called and fed the following parameters
' Param0: Source File, can be a real word and/or date or use a pseudonym ie, PreLDateSuf (20110209) or PreSDateSuf (110209) to get the date eg, O:\Datafeed\WCA\618\PreldateSuf.618
' Param1: Prefix & Suffix part of the file name. Prefix should be before the "-" and Suffix after (Pre-Suf) If no pre or suf you must include the "-" eg, AL- or -AL
' Param2: Task File Name without Ext. This will also become the folder created in "O:\AUTO\logs\". eg, WcaWebload_2 would yeild O:\AUTO\logs\WcaWebload_2\yyyymmdd_WcaWebload_2.txt
' Param3: If the file is an incremental and add "i" as the final argument will get you the increment number based on the time of day. 
'         12:00 to 15:00 = Inc 2 
'	  16:00 to 21:00 = Inc 3
'	  All other hours = Inc 1
' ****************************************************************

option explicit

' **** MAIN ****
	
' **** VARIABLES ****

	
	dim fso, WshShell, numArgs, sFile, sPath, sExt, dFile, dPath, dExt, sMonth, sDay, sYear, Source, Dest, x, y, Pre, sDate,fSource,iByteSize,log,LogFile,sTime,sInc
	Dim lDate, sPre, dPre, PreSdate, PreLdate, SdateSuf, SdateLuf, sSuf, fs, dFileEx, sFileEx,objFSO,objReadFile,contents,objFolder,objFile,objTextFile,Act,lYear,inc 
	Dim procesing, step1,STP
' **** PREPARATION ****
	
	
	'Checks to see if the correct number of paramters have been set
	
	Set numArgs = WScript.Arguments
	
	
	'Arguments

	Dest = numArgs(0)
	Source = numArgs(1)
	Pre = numArgs(2)
'	log = numArgs(3)
	
		if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		lYear = mid(Year(Now),1,4)
		sYear = mid(Year(Now),3,4)
		sTime = (now)
	
'	MsgBox numArgs.length
	
	if numArgs.length = 3 Then
	Set WshShell = WScript.CreateObject("WScript.Shell")
        Set fso = CreateObject("Scripting.FileSystemObject")	        
	
	'assign variable arguments if they exist
		Call splitPre()
		Call splitDest()
		Call process()

		'wscript.quit
	
	ElseIf numArgs.length <> 3 Then
		msgbox "Incorrect Number of Command line Arguments, there should only be 3" & Chr(10) & Chr(10) & "Available command Line Options" & Chr(10) & Chr(10) & "Long Date:" & Chr(10) & "\zip.vbs Destination\PreLDateSuf.zip \Path\to\Source\Files\*.txt Pre-Suf" & Chr(10) & Chr(10) & "Short Date:" & Chr(10) & "\zip.vbs Destination\PreSDateSuf.zip \Path\to\Source\Files\*.txt Pre-Suf" & Chr(10) & Chr(10) & "Dashed Long Date:" & Chr(10) & "\zip.vbs Destination\PreDLDateSuf.zip \Path\to\Source\Files\*.txt Pre-Suf" & Chr(10) & Chr(10) & "Dashed Short Date:" & Chr(10) &"\zip.vbs Destination\PreDSDateSuf.zip \Path\to\Source\Files\*.txt Pre-Suf"
		WScript.quit	
	
	end if
	
'******************************************* Split Source String **********************************************************	
	

	'Split the Source string into fullpath,full filename and ext

	Sub splitDest()
	'msgbox "Source: " & Source
	x = Len(Dest)
	for y = x to 1 step -1
		if mid(Dest, y, 1) = "\" or mid(Dest, y, 1) = "/" then
		    dFile = mid(Dest, y+1)
   		   	'msgbox "sFile: " & sFile
   		    dPath = mid(Dest, 1, y-0)
			'msgbox "sPath: " & sPath
		    exit for
		end if
		
		if mid(Dest, y, 1) = "." then
			sExt = mid(Dest, y-0)
			'msgbox "sExt: " & sExt
		end if
		
	next 
 		
		if dFile = "PreLDateSuf" & sExt then
			Call sPreLongDateSuf()
		end if
		
		if dFile = "PreSDateSuf" & sExt then
			Call sPreShortDateSuf()
		end If
	
		if dFile = "PreDLDateSuf" & sExt then
			Call sPreLDashedDateSuf()
		End If
		
		if dFile = "PreDSDateSuf" & sExt then
			Call sPreSDashedDateSuf()
		
		end If
		
			 
 	end Sub	

'******************************************** Split Prefix-Suffix String ******************************************************	

'FORMAT ANY PREFIX OR SUFFIX APPLIED ON THE COMMAND LINE. NOTE: THE HYPHEN "-" IS ALWAYS REQUIRED ON THE COMMAND LINE BUT IS NOT USED IF NOTHING ADDED BEFORE OR AFTER.
'ONLY CHARACTURES BEFORE AND FATER THE "-" IS USED. I.E. "AL-_1" WOULD YEILD "AL20110328_1.620"

	Sub splitPre()

	  sPre = Pre
	  x = Len(Pre)
	  for y = x to 1 step -1
		if mid(Pre, y, 1) = "-" then
			sPre = mid(Pre, 1, y-1)
				'msgbox "Pre: " & sPre
			sSuf = mid(Pre, y+1, len(pre) - y)
				'msgbox "Suf: " & sSuf
			exit for
		end if
	   next
	
	end Sub
	
	
'****************************** GET INCREMENT NUMBER BASED ON TIME ****************************************************
 
'o:\Datafeed\Equity\620i\PreLDateSuf.620 - eod i
'Increment number based on time of day. Will need to change this if you want to run an inc out of sequence. 
'The "i" will insert the "_" and inc number i.e "_1"

Sub getInc()	

	sTime = Time
	sTime = int(left(sTime,2))
	'msgbox sTime
	
	if sTime >= 12 and sTime <= 15  then 
		sInc = "_2"
		log = numArgs(2) & sInc

	elseif sTime >= 16 and  sTime <= 21  then 
		sInc = "_3"
		log = numArgs(2) & sInc
	else	
		sInc = "_1"
		log = numArgs(2) & sInc
	end if



End Sub 

	
'****************************** SOURCE SHORT Prefix & SUFFIX DATE **********************************************************	

	
	Sub sPreShortDateSuf()
		
	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits
			  
		'-- Create the Custom filename
		'msgbox sPre & "= sPre"
		'msgbox sSuf & "= sSuf"
		dFile = sPre & sYear & sMonth & sDay & sSuf & sInc & sExt
	End Sub

'******************************* SOURCE LONG PREFIX & SUFFIX DATE **********************************************************	
	
	Sub sPreLongDateSuf()

		dFile = sPre & lYear & sMonth & sDay & sSuf & sInc & sExt
	End Sub
	
	
'******************************* SOURCE Long DASHED PREFIX & SUFFIX DATE **********************************************************	

	'o:\Upload\Acc\185\feed\PreDLDateSuf.txt STANDING_FULL_- eod
		
		Sub sPreLDashedDateSuf()
		sYear = mid(Year(Now),1,4)
		sTime = (now)
		dFile = sPre & lYear & "-" & sMonth & "-" & sDay & sSuf & sInc & sExt
	End Sub

'******************************* SOURCE Long DASHED PREFIX & SUFFIX DATE **********************************************************	
	
	
	'o:\Upload\Acc\185\feed\PreDSDateSuf.txt STANDING_FULL_- eod
	
	Sub sPreSDashedDateSuf()
		dFile = sPre & sYear & "-" & sMonth & "-" & sDay & sSuf &  sInc & sExt
	End Sub

'******************************* FILEProcessing *********************************************************
Sub Process

Set WshShell = WScript.CreateObject("WScript.Shell")
'msgbox "O:\AUTO\Apps\exe\isql.exe " & cmdline & " -i " & SQL & " -o " & TSK
'msgbox "O:\AUTO\Apps\exe\isql.exe " & cmdline & " -i " & SQL

'	O:\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a o:\datafeed\UDR\302\302_FULL.ZIP o:\datafeed\UDR\302\*.302

step1=WshShell.Run ("\\192.168.2.163\Ops\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a " & dPath & dFile & " " & Source, 1, true)


End Sub 
				

'***************

'*********************************

'Return the length of a file or -1 if it does not exist

function GetFileSize()
Dim f
  GetFileSize = -1

  Set fs = CreateObject("Scripting.FileSystemObject")

  if fs.FileExists(sPath & sFile) = True then
    set f = fs.GetFile(sPath & sFile)
    GetFileSize = f.size
  end if
  if fs.FolderExists(sPath & sFile) = True then
    set f = fs.GetFolder(sPath & sFile)
    GetFileSize = f.size
  end if

Set f = Nothing
Set fs = Nothing
end function


'********************************
    	
    	
  	'WScript.Echo sFile


' **** END MAIN **** 