' ****************************************************************
' GENERIC PUSHFTP.VBS
'
'     Script for pushing files to EDI ftp boxes
' 
' Arguments in order (0) Local path
'                    (1) Remote path
'                    (2) File extension
'
' ****************************************************************

' **** MAIN ****
	
' **** VARIABLES ****

	dim WshShell,sLocalPath,sRemotePath,sFileext,sOpsFtp,sDirection,sXRC,sLog 

	

' **** PREPARATION ****
	
	Set colArgs = WScript.Arguments
 
	sLocalPath = "-local " & replace(colArgs(0),"\","/") &" "
	sRemotePath = "-remote " & replace(colArgs(1),"\","/") &" "
	sFileext = "-fileext " & colArgs(2) & " "
	sOpsFtp = "O:\AUTO\Apps\opsftp\dist\opsftp.jar "
	sDirection = "-" & colArgs(4) & " "
	sXRC = "-XCRC on "
	sSite = "-site " & colArgs(3) & " "
	sLog = "-log o:\AUTO\logs\Ftp.log"
	
	Set WshShell = WScript.CreateObject("WScript.Shell")


' **** PROCESSING ****

	If colArgs(3) = "both" Then
	
	' PULL FILE DOTCOM
	sSite = "-site dotcom "
	ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog)

	' PUSH FILE TO DOTNET
	sSite = "-site dotnet "
	ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)

	Else 
		' PULL FILE 
	'sSite = "-site dotcom "
	ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC, 1, true)
	End if
' **** END MAIN **** 