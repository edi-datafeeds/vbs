'-------------------------------------------------------------------------

'INFORMATION PLEASE READ 

'-------------------------------------------------------------------------

'Parameter Mandatory
'v_loc = o:\test\yyyymmdd.txt 'the usual format for ops
'v_ftp = bucketName | bucketName/folder
'v_colon = : | test_|_test ' the usual ops way
'v_log = name of log

'Sample of para = o:/datafeed/wca/680i/ al-kumar-g/test123/YYYYMMDD.680 : My_Wca_Webload_FTP i

'full command line "c:\Program Files\S3 Browser\s3browser-con.exe" upload Ops o:/datafeed/wca/680i/ al-kumar-g/test123/YYYYMMDD.680 : My_Wca_Webload_FTP i

'Parameter Optional -  any of the following can be added after the Mandatory Para, Make sure you add the following after the log name 
'-ud? =  - -uddownload, by default it's upload
'i = Ops way Incremental 
'-id? = As long as the ID is added to the s3 Browser you can overwrite the default so -idTMX or -idTEST
'-min? = number of day(s) back  -min5
'Sample of para = o:/datafeed/wca/680i/ al-kumar-g/test123/YYYYMMDD.680 : My_Wca_Webload_FTP -uddownload i -idName -min1

'-------------------------------------------------------------------------

Option explicit

' **** MAIN ****
    
' **** VARIABLES ****

Dim para

	Set para = WScript.Arguments
	
Dim para_size : para_size = para.length

run_automate_taks( para )


Function run_automate_taks( para )

	Dim objShell, programSrc, accID, upDown, v_loc, v_ftp, v_file, v_tmp, v_colon, v_pre, v_suf, v_inc, v_log, v_localPath, v_awsPath, minValu, v_error
	
	Dim logArray(10), v_logDate
	
	Set objShell = CreateObject ("WScript.Shell")
	
	programSrc = """c:\Program Files\S3 Browser\s3browser-con.exe"""
	'programSrc = """O:\AUTO\Apps\S3 Browser\s3browser-con.exe"""
	
	v_loc = para(0)
	v_ftp = para(1)
	v_colon = para(2)
	v_log = para(3)
	
'----------------------------------------------------------------------------	
	
	accID = overWriteID( para ) ' function to overwrite default user

'----------------------------------------------------------------------------	
	
	upDown = overWriteUpDown( para ) 'function to overwrite from upload to download
	
'----------------------------------------------------------------------------	
	
	v_inc = inc_yn( para ) 'check if its incremental or not 

'----------------------------------------------------------------------------	
	
	v_tmp = Split( v_colon, ":" ) ' get prefix and suffix
	
	v_pre = v_tmp(0)
	v_suf = v_tmp(1)

'----------------------------------------------------------------------------
	
	v_localPath = localFileDir( v_loc )
	
	'MsgBox v_localPath
	minValu = minusExist( para ) 
	
	'MsgBox minValu
	
	v_file = fileFormat( v_ftp, minValu )
	
	'MsgBox v_file
	
	v_awsPath = amazonFileDir( v_ftp )
	
	'MsgBox v_ftp
'----------------------------------------------------------------------------
	
	'WScript.Echo programSrc & " " & upDown & " " & accID & " " &  v_localPath & v_pre & v_file & v_suf & v_inc & Right(v_ftp, 4 )  & " " & v_awsPath
	
	'objShell.Run programSrc & " " & upDown & " " & accID & " " &  v_localPath & v_pre & v_file & v_suf & v_inc & Right(v_ftp, 4 )  & " " & v_awsPath
	
	v_error = objShell.Run ( programSrc & " " & upDown & " " & accID & " " &  v_localPath & v_pre & v_file & v_suf & v_inc & Right(v_ftp, 4 )  & " " & v_awsPath, 1, TRUE )
	
	'MsgBox v_error
	
	Set objShell = Nothing
	
'----------------------------------------------------------------------------	

	v_logDate = fileFormat( "yyyymmdd", "0" )
	
	logArray(0) = programSrc & " " & upDown & " " & accID & " " &  v_localPath & v_pre & v_file & v_suf & v_inc & Right(v_loc, 4 )  & " " & v_awsPath
	
	logArray(1) = v_logDate & "_" & v_log &  v_inc
	
	logArray(2) = v_log &  v_inc 
	
	logToFile( logArray )
	
'----------------------------------------------------------------------------	
		
End Function 

'---------------------------------------------------------------------------------------------------

Function fileFormat( para, minV )
	
	Dim v_tmp, v_fileDate, oRE, oRE_Match, varDate, oRE_Count, dteNow
	
	para = Replace( para, "/", "\" )
	
	Set oRE = New RegExp 
	With oRE
        .pattern="YY"
        .IgnoreCase = True
        .Global = True
    End With
	
	'---------------------------------------------------
	
	dteNow = Date()
	dteNow = DateAdd("d", minV, dteNow)
	
	dteNow = Split(dteNow, "/")
	
	'---------------------------------------------------
	
	v_tmp = Split( para, "\" )
	
	v_fileDate = v_tmp( UBound( v_tmp ) )
	
	v_tmp = Split( v_fileDate, "." )
	
	v_fileDate = v_tmp(0)
	
	'---------------------------------------------------
	
	Set oRE_Count = oRE.execute( v_fileDate )
	
	oRE_Count  = oRE_Count.count
	
	'MsgBox oRE_Count
	
	oRE_Match = oRE.Test( v_fileDate )
	
	If oRE_Match = True Then
		
		'-------------------------------------------------------------------
		'Prep date value YY-MM-DD or YYYY-MM-DD
		
		If oRE_Count = 2 Then 'count the number YY rather than YYYY,
		
			varDate = dteNow(2) & "-" & dteNow(1) & "-" & dteNow(0) 
		Else 
			
			varDate = Mid(dteNow(2),3,4) & "-" & dteNow(1) & "-" & dteNow(0) 
		End If
		
		'-------------------------------------------------------------------
			
		v_tmp = Split( v_fileDate, "-" ) 'Split the date para, YYYY-MM-DD | YYYYMMDD, value to see if hyphen exist or not 
		
		If UBound( v_tmp ) = 2 Then 'number of split will detemine if hyphen exist or not
		
			v_fileDate = varDate 'If hyphen exist then assign varDate value 
			
		ElseIf UBound( v_tmp ) = 0 Then 
		
			v_fileDate = Replace( varDate, "-", "") 'If hyphen does not exist then remove hypen from varDate 
		
		Else 
		
			MsgBox "please add the conditon to the if statement!"
			
		End If 
		
		
	End If 
	
	'MsgBox varDate
		
	fileFormat = v_fileDate
	
End Function 

'---------------------------------------------------------------------------------------------------

Function localFileDir( para )
	
	Dim v_tmp, v_fileDir, oRE_Count, i 
	
	para = Replace( para, "/", "\" )
	
	v_tmp = Split( para, "\" )
	
	oRE_Count = UBound( v_tmp )
		
	v_fileDir = ""
	
	For i = 0 to oRE_Count - 1
      	
      	v_fileDir =  v_fileDir & "" & v_tmp(i) & "\"
      	
      	'MsgBox v_fileDir
    
    Next
	
	'MsgBox varDate
		
	localFileDir = v_fileDir
	
End Function 

'---------------------------------------------------------------------------------------------------

Function amazonFileDir( para )
	
	Dim v_tmp, v_fileDir, oRE_Count, i 
	
	para = Replace( para, "\", "/" )
	
	v_tmp = Split( para, "/" )
	
	oRE_Count = UBound( v_tmp )
		
	v_fileDir = ""
	
	For i = 0 to oRE_Count - 1
      	
      	v_fileDir =  v_fileDir & "" & v_tmp(i) & "/" 
      	
      	'MsgBox v_fileDir
    
    Next
	
	'MsgBox v_fileDir
		
	amazonFileDir = v_fileDir
	
End Function 

'---------------------------------------------------------------------------------------------------

Function minusExist( para )
	
	Dim minValu, i, varTemp
	
	minValu = 0
	
	'WScript.Echo para.length
	
	For i = 0 to para.length - 1
      	
      	varTemp =  para(i)
      	
      	If Left( varTemp, 4) = "-min" Then 
      		
      		minValu = Replace( varTemp, "min", "")
      		
      		'WScript.Echo para(i) & " " & i & " " & sInc
      		
    	End If  
    Next
	
	minusExist = minValu
	
End Function 

'-----------------------------------------------------------------------------------------------------------------------------------------

Function inc_yn( para )
	
	Dim sInc, i, varTemp
	
	sInc = ""
	
	'WScript.Echo para.length
	
	For i = 0 to para.length - 1
      	
      	varTemp =  para(i)
      	
      	If para(i) = "i" Then 
      
      		sInc = GetInc()
      		
      		'WScript.Echo para(i) & " " & i & " " & sInc
      		
    	End If  
    Next
	
	inc_yn = sInc
	
End Function 

'-----------------------------------------------------------------------------------------------------------------------------------------

Function overWriteID( para )
	
	Dim accID, i, varTemp
	
	accID = "Ops"
	
	'WScript.Echo para.length
	
	For i = 0 to para.length - 1
      	
      	varTemp =  para(i)
      	
      	If Left( varTemp, 3) = "-id" Then 
      		
      		accID = Replace( varTemp, "-id", "")
      		
      		'WScript.Echo para(i) & " " & i & " " & sInc
      		
    	End If  
    Next
	
	overWriteID = accID
	
End Function 

'-----------------------------------------------------------------------------------------------------------------------------------------

Function overWriteUpDown( para )
	
	Dim v_upDown, i, varTemp
	
	v_upDown = "upload"
	
	'WScript.Echo para.length
	
	For i = 0 to para.length - 1
      	
      	varTemp =  para(i)
      	
      	If Left( varTemp, 3) = "-ud" Then 
      		
      		v_upDown = Replace( varTemp, "-ud", "")
      		
      		'WScript.Echo para(i) & " " & i & " " & sInc
      		
    	End If  
    Next
	
	overWriteUpDown = v_upDown
	
End Function 

'-----------------------------------------------------------------------------------------------------------------------------------------

Function timeUTC()
	
	Dim dateTime, varHour, vTemp

	Set dateTime = CreateObject("WbemScripting.SWbemDateTime")    
	dateTime.SetVarDate (Now())
	'echoMsg(  "Local Time:  " & dateTime )
	'WScript.echo  "UTC Time: " & dateTime.GetVarDate (false)
	
	vTemp = Split( dateTime.GetVarDate (false), " ")
	
	varHour = Split( vTemp(1), ":")
	
	'WScript.echo varHour(0)
	
	timeUTC = varHour(0)
	
End Function  

'-----------------------------------------------------------------------------------------------------------------------------------------

Function GetInc()
	
	Dim sTimeH, varInc
	
	varInc = "_1"
	
	sTimeH = Int( timeUTC() )
	
	If sTimeH >= 17 Then 
		
		'WScript.Echo "17"
		
		varInc = "_3"
	
	ElseIf  sTimeH >= 13 Then 
		
		'WScript.Echo "13"
		
		varInc = "_2"
		
	Else 
	
		'WScript.Echo "07"
	End If 

	GetInc = varInc
	
End Function

'-----------------------------------------------------------------------------------------------------------------------------------------

Function logToFile( logDetails )
	
	Dim LogFile, Dest, contents, objFSO, objFolder, objFile, objTextFile
	
	contents = "<p><span class=amazon>S3 Browser Trigger <br/><br/> " & " <br/> " & logDetails(0) & " </span></p><br/>"
	
	LogFile = "O:\AUTO\logs\" & logDetails(2) & "\" & logDetails(1) & ".html"
	Dest = "O:\AUTO\logs\" & logDetails(2) & "\"
	
	
	' Create the File System Object
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		
		' Check that the WriteDirectory folder exists
		If objFSO.FolderExists(Dest) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		 ' Create the WriteDirectory folder if folder does not exist
		   Set objFolder = objFSO.CreateFolder(Dest)
		   'WScript.Echo "Just created " & Dest
		End If
		
		'msgbox "LogFile Test: " & LogFile
		If objFSO.FileExists(LogFile) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		   Set objFile = objFSO.CreateTextFile(LogFile)
		   'Wscript.Echo "Just created " & LogFile
		End If
		
		set objFile = nothing
		set objFolder = nothing
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		Const ForAppending = 8		
		
		Set objTextFile = objFSO.OpenTextFile _
		(LogFile, ForAppending, True)
		
		
		' Writes WriteFile every time you run this VBScript
		objTextFile.WriteLine(contents)
		objTextFile.Close
	
End Function