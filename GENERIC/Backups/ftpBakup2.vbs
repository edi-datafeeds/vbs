' ****************************************************************
' GENERIC FTP.VBS
'
'     Script for pushing files to EDI ftp boxes
' 
' Arguments in order (0) Local path
'                    (1) Remote path
'                    (2) File extension
'					 (3) Server
'					 (4) Direction
'					 (5) Task File
'					 (6) Ops Step
'
'### Example CMDLINE ###
' o:\auto\scripts\vbs\generic\ftp.vbs o:/Datafeed/Warrant/609/ /Warrant/609/ .txt both Upload WCAWebload_1 609_Warrants

'### CMDLINE Breakdown ###

'# o:\auto\scripts\vbs\generic\ftp.vbs \LocalPath\ \RemotePath\ .Ext SVR Direction Taskfile OpsStep 

'### Options ###

'# SVR 
'	"both" = dotcom & dotnet
'	"dotcom" = dotcom
'	"dotnet" = dotnet
'	"MarkitDivCon" = Wall Street Horizons
'	"MERGENT" = MERGENT

'# Direction | "Upload"
'      	       "Download"

'# Taskfile |  Name of the Task File the ftp belongs to, ie, WCAWebload_1

'# OpsStep |   Name of the step the ftp belongs to, ie, 609_Warrants

'##############

'# If the ftp returns a crc failure it will try to run it again and if still it fails it will append to log 

'# XCRC is turned on by default but off for Mergent and Wall Street Horizons. If you need to add a new download from a
'# servers that do not belong to us, you will need to turn off the XCRC in this scriptby adding another Else If colArgs(3)
'# with the server name from the config file.
' ****************************************************************

' **** MAIN ****
	
' **** VARIABLES ****

	dim WshShell,sLocalPath,sRemotePath,sFileext,sOpsFtp,sDirection,sXRC,sLog 
	CONST ForReading = 1, ForWriting = 2, ForAppending = 8
	

' **** PREPARATION ****
	
	Set colArgs = WScript.Arguments
 
	sLocalPath = "-local " & replace(colArgs(0),"\","/") &" "
	sRemotePath = "-remote " & replace(colArgs(1),"\","/") &" "
	sFileext = "-fileext " & colArgs(2) & " "
	sOpsFtp = "O:\AUTO\Apps\opsftp\dist\opsftp.jar "
	sSite = "-site " & colArgs(3) & " "
	sDirection = "-" & colArgs(4) & " "
	If colArgs(3) = "WALLSTR" Then
		sXRC = "-XCRC off "
	ElseIf colArgs(3) = "MERGENT" Then 
		sXRC = "-XCRC off "	
	Else 	
		sXRC = "-XCRC on "	
	End If 
	TSK = colArgs(5)
	STP = colArgs(6)
		
	
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		lYear = mid(Year(Now),1,4)
		sYear = mid(Year(Now),1,4)
		sTime = (now)

	sLog = "-log o:\AUTO\logs\Ftp.log"
	'sLog = "-log O:\AUTO\logs\" & TSK & "\" & sYear & sMonth & sDay & "_" & TSK & ".txt"

	
	LogFile = "O:\AUTO\logs\" & TSK & "\" & sYear & sMonth & sDay & "_" & TSK & ".txt"
	Dest = "O:\AUTO\logs\" & TSK & "\"

	Set WshShell = WScript.CreateObject("WScript.Shell")

' **** PROCESSING ****

	If colArgs(3) = "both" Then
	
	' FTP FILE DOTCOM
	sSite = "-site dotcom "
	ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)
		if ftp1 = "0" Then
		 	contents = sTime & " | " & STP & " | " & TSK & " | " & colArgs(4) & "ed Sucessfully | " & sSite & " | " &  sLocalPath & " | " &  sRemotePath
			call sAppend()
		'msgbox "isql Table Generated"	
		Elseif ftp1 = "2" then
		 	ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)
				if ftp1 = "0" Then
			 		contents = sTime & " | " & STP & " | " & TSK & " | " & colArgs(4) & "ed Sucessfully | " & sSite & " | " &  sLocalPath & " | " &  sRemotePath
					call sAppend()
					'msgbox "isql Table Generated"	
				Elseif ftp1 = "2" then
					contents = sTime & " | " & STP & " | " & TSK & " | " & colArgs(4) & " | CRC Failed | " & sSite & " | " &  sLocalPath & " | " &  sRemotePath
					Call sAppend()	
					'msgbox "CRC Failed"		
				Else
			 		contents = sTime & " | " & STP & " | " & TSK & " | " & colArgs(4) & " Failed | " & sSite & " | " &  sLocalPath & " | " &  sRemotePath
					Call sAppend()	
					'msgbox "FTP Failed"
		
				end if	
					
		Else
		 	contents =sTime & " | " & STP & " | " & TSK & " | " & colArgs(4) & " Failed | " & sSite & " | " &  sLocalPath & " | " &  sRemotePath
			Call sAppend()	
		'msgbox "FTP Failed"
	
		end if	

		
	' FTP FILE TO DOTNET
	sSite = "-site dotnet "
	ftp2=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1,true)
		if ftp2 = "0" Then
		 	contents = sTime & " | " & STP & " | " & TSK & " | " & colArgs(4) & "ed Sucessfully | " & sSite & " | " &  sLocalPath & " | " &  sRemotePath
			call sAppend()
		'msgbox "isql Table Generated"	
		Elseif ftp2 = "2" then
			ftp2=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)
				if ftp2 = "0" Then
			 		contents = sTime & " | " & STP & " | " & TSK & " | " & colArgs(4) & "ed Sucessfully | " & sSite & " | " &  sLocalPath & " | " &  sRemotePath
					call sAppend()
					'msgbox "isql Table Generated"	
				Elseif ftp2 = "2" then
					contents = sTime & " | " & STP & " | " & TSK & " | " & colArgs(4) & " | CRC Failed | " & sSite & " | " &  sLocalPath & " | " &  sRemotePath
					Call sAppend()	
					'msgbox "CRC Failed"		
				Else
			 		contents = sTime & " | " & STP & " | " & TSK & " | " & colArgs(4) & " Failed | " & sSite & " | " &  sLocalPath & " | " &  sRemotePath
					Call sAppend()	
					'msgbox "FTP Failed"
		
				end if	
					
		Else
		 	contents = sTime & " | " & STP & " | " & TSK & " | " & colArgs(4) & " Failed | " & sSite & " | " &  sLocalPath & " | " &  sRemotePath
			Call sAppend()	
			'msgbox "FTP Failed"
	
		end if	

	Else 
		' FTP FILE OTHER
		'sSite = "-site (User Defined) "
		ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)
			if ftp1 = "0" Then
			 	contents = sTime & " | " & STP & " | " & TSK & " | " & colArgs(4) & "ed Sucessfully | " & sSite & " | " &  sLocalPath & " | " &  sRemotePath
				call sAppend()
			'msgbox "isql Table Generated"	
			Elseif ftp1 = "2" Then
				ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC, 1, true)
					if ftp1 = "0" Then
				 		contents = sTime & " | " & STP & " | " & TSK & " | " & colArgs(4) & "ed Sucessfully | " & sSite & " | " &  sLocalPath & " | " &  sRemotePath
						call sAppend()
						'msgbox "isql Table Generated"	
					Elseif ftp1 = "2" then
						contents = sTime & " | " & STP & " | " & TSK & " | " & colArgs(4) & " | CRC Failed | " & sSite & " | " &  sLocalPath & " | " &  sRemotePath
						Call sAppend()	
						'msgbox "CRC Failed"		
					Else
				 		contents = sTime & " | " & STP & " | " & TSK & " | " & colArgs(4) & " Failed | " & sSite & " | " &  sLocalPath & " | " &  sRemotePath
						Call sAppend()	
						'msgbox "FTP Failed"
			
					end if	
						
			Else
			 	contents = sTime & " | " & STP & " | " & TSK & " | " & colArgs(4) & " Failed | " & sSite & " | " &  sLocalPath & " | " &  sRemotePath
				Call sAppend()	
			'msgbox "FTP Failed"
		
			end if	
End if
' **** END MAIN **** 


Sub sAppend()


'MsgBox "In Act - Append Function"



    	'*******************
    	
		'wscript.echo ("Source: " & sPath & sFile), ("Dest: " & dPath & dFile )
		'************
		'MsgBox sPath & sFile
		
		'***** PROCESSING *****
		
		
		'Read file contents
		
		'Display results
		'wscript.echo contents
			
		
		' Create the File System Object
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		
		' Check that the WriteDirectory folder exists
		If objFSO.FolderExists(Dest) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		 ' Create the WriteDirectory folder if folder does not exist
		   Set objFolder = objFSO.CreateFolder(Dest)
		   'WScript.Echo "Just created " & Dest
		End If
		'msgbox "LogFile Test: " & LogFile
		If objFSO.FileExists(LogFile) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		   Set objFile = objFSO.CreateTextFile(LogFile)
		   'Wscript.Echo "Just created " & LogFile
		End If
		
		set objFile = nothing
		set objFolder = nothing
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		Const ForAppending = 8		
		
		Set objTextFile = objFSO.OpenTextFile _
		(LogFile, ForAppending, True)
		
		
		' Writes WriteFile every time you run this VBScript
		objTextFile.WriteLine(contents)
		objTextFile.Close
		
		
	end sub	