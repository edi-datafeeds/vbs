' ****************************************************************
'Generic Feed Loader Script		07/04/2011
'
'    This Script is used to check Feed Loaders have worked and appends the outcome to the named log. Can be used for both SMF & WCA
'    The log files are stored here: "O:\AUTO\logs\" and the final folder name is based on what you parse as the 4th argument on the cmdline

' EXAMPLE CMDLINE
' O:\AUTO\Scripts\vbs\GENERIC\Loader.vbs WCA a 1 SQLSVR1
' 

' CMDLINE BREAKDOWN
' O:\AUTO\Scripts\vbs\GENERIC\Loader.vbs is this file, must be called and fed the following parameters
' Param0: Type of Loader... WCA, SMF, etc
' Param1: Mode to run... "a" for Auto or "m" for Manual 
' Param2: Server Number... 1, 2, 3, etc from loader specific config (in the same folder as exe)
' Param3: Task File Name... ie, WcaWebload_1, WcaWebload_2, etc. Used to create the Log File and Folder
' Param4: Server Name... ie, SQLSVR1, Deisel, Deisel4, ect. Used for better Server Identification in the Log File

'Options 
'Typ: "WCA" or "SMF"
'MODE: "a" for Auto - "m" for Manual - Note; Manual mode never exits with errors because you have to close the program
'SVR: You will have to check the config file specific to the loader you would like to you. The config can usually be found in the same folder as the exe
'TSK: Name of the Task File this VBS is being run from. Used to build a Log File and append the results of the operation to
'SVRN: SQLSVR1, Deisel, Deisel4, etc. This is not used in the processing, only for the logging of results.
' ****************************************************************

Option explicit

' **** MAIN ****
	
' **** VARIABLES ****

	
	dim fso, WshShell, numArgs, sFile, sPath, sExt, dFile, dPath, dExt, sMonth, sDay, sYear, Source, Dest, x, y, Pre, sDate,fSource,iByteSize,log,LogFile,sTime,sInc
	Dim lDate, sPre, dPre, PreSdate, PreLdate, SdateSuf, SdateLuf, sSuf, fs, dFileEx, sFileEx,objFSO,objReadFile,contents,objFolder,objFile,objTextFile,Act,lYear,inc,Mode,sLog 
	Dim TSK, SVRN,SVR,step1,Loader,Typ,wso,iTime
	CONST ForReading = 1, ForWriting = 2, ForAppending = 8
' **** PREPARATION ****
	
			Set fso = CreateObject("Scripting.FileSystemObject")
			Set wso = Wscript.CreateObject("Wscript.Shell")

	'Checks to see if the correct number of paramters have been set
	
	Set numArgs = WScript.Arguments
	
	
	'Arguments
	'Loader Type
		Typ = numArgs(0)
			If numArgs(0) = "WCA" Then
				Loader = "O:\AUTO\Apps\exe\WCALoader\wcaloader.exe -cfgfile O:\AUTO\Configs\DbServers.cfg "
				Source = "-i o:\datafeed\bahar\wcafeed\ "
				If numArgs(2)= "5" then
					sLog = " -l o:\datafeed\bahar\wcaerr\ -tablst o:\apps\exe\wcaloader\diesel4\tablist.txt -alert -errcd 1" 
				else
					sLog = " -l o:\datafeed\bahar\wcaerr\ -alert -errcd 1"
				End If	
				
		'@' SMF Loader - If you need to run loader out of sequence change .jar to .exe	
				
			ElseIf numArgs(0) = "SMF" Then
				Loader = "O:\AUTO\Apps\exe\SMFLoader\SMFLoader.jar -cfgfile O:\AUTO\Configs\DbServers.cfg "
				Source = "-i o:\datafeed\smf\smffeed\ "
				sLog = " -l o:\datafeed\smf\smffeed\logsqlsvr1\ -alert -errcd 1"
			Else
				MsgBox "Loader not Found, Please talk to Ops about adding another Loader"	
				WScript.Quit(1)
			End If 
	'Mode	
		If numArgs(1)= "a" Then
			Mode = "-a "
		Else 
			Mode = "-m "
		End If 
	'Server
		SVR = "-s " & numArgs(2)
	'Log File
	TSK = WScript.Arguments.Item(3)
	SVRN = WScript.Arguments.Item(4)	
	if numArgs.length = 6 then 
		sInc = numArgs(5)
	Else
		sInc =""
	End If
	
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	End if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		lYear = mid(Year(Now),1,4)
		sYear = mid(Year(Now),1,4)
		sTime = (now)
	
	If sInc = "i" Then
		getInc
	Else 
		sInc = ""
	End If

Sub getInc()	
	
	iTime = Time
	iTime = int(left(iTime,2))
	'msgbox sTime
	
	if iTime >= 12 and iTime <= 15  then 
		sInc = "_2"

	elseif iTime >= 16 and  iTime <= 21  then 
		sInc = "_3"
	else	
		sInc = "_1"
	end if



End Sub 

	LogFile = "O:\AUTO\logs\" & TSK &  sInc &"\" & sYear & sMonth & sDay & "_" & TSK & sInc & ".html"
	Dest = "O:\AUTO\logs\" & TSK & sInc & "\"


'***** Processing *****

Set WshShell = WScript.CreateObject("WScript.Shell")
'msgbox Loader & Mode & Source & SVR & sLog
step1=WshShell.Run (Loader & Mode & Source & SVR & sLog, 1, true)

' Validate Load
if step1 = "0" Then
 	contents ="<p><span class=ok>" &  sTime & " | " & Typ & " Loader to " & SVRN & " Loaded Sucessfully</span></p>"
	call sAppend()
	'msgbox Typ & " Loader to " & SVRN & " Loaded Sucessfully"	
Else
 	contents ="<p><span class=failed>" & sTime & " | " & Typ & " Loader to " & SVRN & " Failed to Load </span></p>"
 	 'step1 = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER ops@exchange-data.com -PASS BLOCH1 -TO y.laifa@exchange-data.com;y.migou@exchange-data.com;m.amaning@exchange-data.com; -SUB " & Typ & " Loader Error Alert to " & SVRN)
 	 'y =     wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1 -TO webmaster@exchange-data.com;ukcorporateactions@citigroup.com;phil.c.davies@citigroup.com -SUB EDI Wincab docs are available") 
	Call sAppend()	
	msgbox TSK & " " & Typ & " Loader to " & SVRN & " Failed to Load"
	
end if	



Sub sAppend()


'MsgBox "Append Function"

		
		'***** PROCESSING *****
				
		
		' Create the File System Object
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		
		' Check that the WriteDirectory folder exists
		If objFSO.FolderExists(Dest) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		 ' Create the WriteDirectory folder if folder does not exist
		   Set objFolder = objFSO.CreateFolder(Dest)
		   'WScript.Echo "Just created " & Dest
		End If
		'msgbox "LogFile Test: " & LogFile
		If objFSO.FileExists(LogFile) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		   Set objFile = objFSO.CreateTextFile(LogFile)
		   'Wscript.Echo "Just created " & LogFile
		End If
		
		set objFile = nothing
		set objFolder = nothing
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		Const ForAppending = 8		
		
		Set objTextFile = objFSO.OpenTextFile _
		(LogFile, ForAppending, True)
		
		
		' Writes WriteFile every time you run this VBScript
		objTextFile.WriteLine(contents)
		objTextFile.Close
		
	'	WScript.Quit(0)
End sub	