' ****************************************************************
' GENERIC FilesEx.VBS
'
'    This Script is used to check files exist and appends to the named log the file path and byte size to be used for comparison.
'    The log files are stored here: "O:\AUTO\logs\" and the final folder name is based on what you parse from the cmdline

' EXAMPLE CMDLINE
' Long Date 20110302| O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618i_not_US_CA\PreLDateSuf.618 -_2 WcaWebload_2
' Short Date 110302 | O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618i_not_US_CA\PreSDateSuf.618 -_2 WcaWebload_2
' Dashed Long Date 2011-04-05 | O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618\PreDLDateSuf.618 - WcaWebload_2
' Dashed Short Date 11-04-05 | O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618\PreDSDateSuf.618 - WcaWebload_2
' Incremental Files yymmdd_2.ext| O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs O:\Datafeed\WCA\618i_not_US_CA\PreDSDateSuf.618 - WcaWebload_2 i

' CMDLINE BREAKDOWN
' O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs is this file, must be called and fed the following parameters
' Param0: Source File, can be a real word and/or date or use a pseudonym ie, PreLDateSuf (20110209) or PreSDateSuf (110209) to get the date eg, O:\Datafeed\WCA\618\PreldateSuf.618
' Param1: Prefix & Suffix part of the file name. Prefix should be before the "-" and Suffix after (Pre-Suf) If no pre or suf you must include the "-"
' Param2: Task File Name without Ext. This will also become the folder created in "O:\AUTO\logs\". eg, WcaWebload_2 would yeild O:\AUTO\logs\WcaWebload_2\yyyymmdd_WcaWebload_2.txt
' Param3: If the file is an incremental and add "i" as the final argument will get you the increment number based on the time of day. 
'         12:00 to 15:00 = Inc 2 
'	  16:00 to 21:00 = Inc 3
'	  All other hours = Inc 1
' ****************************************************************

option explicit

' **** MAIN ****
	
' **** VARIABLES ****

	
	dim fso, WshShell, numArgs, sFile, sPath, sExt, dFile, dPath, dExt, sMonth, sDay, sYear, Source, Dest, x, y, Pre, sDate,fSource,iByteSize,log,LogFile,sInc
	Dim lDate, sAction, sPre, dPre, PreSdate, PreLdate, SdateSuf, SdateLuf, sSuf, fs, dFileEx, sFileEx,objFSO,objReadFile,contents,objFolder,objFile,objTextFile,Act,lYear,inc 
	Dim stTime,sTime,sMins,tDiff,th,tm,ts,intHours,intMinutes,intSeconds,i
' **** PREPARATION ****
	
	
	'Checks to see if the correct number of paramters have been set
	
	Set numArgs = WScript.Arguments
	Set WshShell = WScript.CreateObject("WScript.Shell")
	Set fso = CreateObject("Scripting.FileSystemObject")	        

	
	'Arguments

	Source = numArgs(0)
	Pre = numArgs(1)

	'-- Build custom output file name, ensuring that
	' both sMonth and sDay are always two digits
	
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		lYear = mid(Year(Now),1,4)
		sYear = mid(Year(Now),3,4)
		sTime = (now)
		stTime = (now)
	if numArgs.length = 2 then 
		sAction = numArgs(0)
		log = numArgs(1)
		contents = "<META HTTP-EQUIV=REFRESH CONTENT=60><link rel=stylesheet href=../style.css /><p><span class=note>" & sTime & " | " & sAction & "ed " & Log & " Task File</span></p>"
		LogFile = "O:\AUTO\logs\" & log & "\" & lYear & sMonth & sDay & "_" & log & ".html"
		Dest = "O:\AUTO\logs\" & log & "\"		
		call sAppend()
		wscript.Quit
		
	ElseIf numArgs.length = 3 then
		
		if numArgs(2) = "i" then
			Call getInc()
			sAction = numArgs(0)
			log = numArgs(1) & sInc
			sTime = (now)

			contents = "<META HTTP-EQUIV=REFRESH CONTENT=60><link rel=stylesheet href=../style.css /><p><span class=note>" & sTime & " | " & sAction & "ed " & Log & " Task File</span></p>"
			LogFile = "O:\AUTO\logs\" & log & "\" & lYear & sMonth & sDay & "_" & log & ".html"
			Dest = "O:\AUTO\logs\" & log & "\"		
			call sAppend()
			wscript.Quit
		else

			log = numArgs(2)
			'assign variable arguments if they exist
			Call splitPre()
			Call splitSource()
			Call FileExists()
			'wscript.quit
		end if
	ElseIf numArgs.length = 4 then
	log = numArgs(2)
		If numArgs(3)= "i" Then
			'assign variable arguments if they exist
			Call splitPre()
			Call getInc()
			Call splitSource()		
			Call FileExists()
		Else
			i = numArgs(3)
			Call splitPre()
			Call getDate(i)
			Call splitSource()		
			Call FileExists()
		End if		
	Else 
		msgbox "Syntax Err:Incorrect Number of Arguments"
		wscript.quit
	
	end if
	
'******************************************* Split Source String **********************************************************	
	

	'Split the Source string into fullpath,full filename and ext

	Sub splitSource()
	'msgbox "Source: " & Source
	x = Len(Source)
	for y = x to 1 step -1
		if mid(Source, y, 1) = "\" or mid(Source, y, 1) = "/" then
		    sFile = mid(Source, y+1)
   		   	'msgbox "sFile: " & sFile
   		    sPath = mid(Source, 1, y-0)
			'msgbox "sPath: " & sPath
		    exit for
		end if
		
		if mid(Source, y, 1) = "." then
			sExt = mid(Source, y-0)
			'msgbox "sExt: " & sExt
		end if
		
	next 
 		
		if sFile = "PreLDateSuf" & sExt then
			Call sPreLongDateSuf()
		end if
		
		if sFile = "PreSDateSuf" & sExt then
			Call sPreShortDateSuf()
		end If
	
		if sFile = "PreDLDateSuf" & sExt then
			Call sPreLDashedDateSuf()
		End If
		
		if sFile = "PreDSDateSuf" & sExt then
			Call sPreSDashedDateSuf()
		
		end If
					 
 	end Sub	

'******************************************** Split Prefix-Suffix String ******************************************************	

'FORMAT ANY PREFIX OR SUFFIX APPLIED ON THE COMMAND LINE. NOTE: THE HYPHEN "-" IS ALWAYS REQUIRED ON THE COMMAND LINE BUT IS NOT USED IF NOTHING ADDED BEFORE OR AFTER.
'ONLY CHARACTURES BEFORE AND FATER THE "-" IS USED. I.E. "AL-_1" WOULD YEILD "AL20110328_1.620"

	Sub splitPre()

	  sPre = Pre
	  x = Len(Pre)
	  for y = x to 1 step -1
		if mid(Pre, y, 1) = ":" then
			sPre = mid(Pre, 1, y-1)
				'msgbox "Pre: " & sPre
			sSuf = mid(Pre, y+1, len(pre) - y)
				'msgbox "Suf: " & sSuf
			exit for
		end if
	   next
	
	end Sub
	
	
'****************************** GET INCREMENT NUMBER BASED ON TIME ****************************************************
 
'o:\Datafeed\Equity\620i\PreLDateSuf.620 - eod i
'Increment number based on time of day. Will need to change this if you want to run an inc out of sequence. 
'The "i" will insert the "_" and inc number i.e "_1"

Sub getInc()	

	sTime = Time
	sTime = int(left(sTime,2))
	'msgbox sTime
	
	if sTime >= 12 and sTime <= 16  then 
		sInc = "_2"
		log = numArgs(2) & sInc

	elseif sTime >= 17 and  sTime <= 21  then 
		sInc = "_3"
		log = numArgs(2) & sInc
	else	
		sInc = "_1"
		log = numArgs(2) & sInc
	end if



End Sub 

	
'****************************** SOURCE SHORT Prefix & SUFFIX DATE **********************************************************	

	
	Sub sPreShortDateSuf()
		
			  
		'-- Create the Custom filename
		'msgbox sPre & "= sPre"
		'msgbox sSuf & "= sSuf"
		sFile = sPre & sYear & sMonth & sDay & sSuf & sInc & sExt
	End Sub

'******************************* SOURCE LONG PREFIX & SUFFIX DATE **********************************************************	
	
	Sub sPreLongDateSuf()

		'-- Create the Custom filename
		'msgbox sPre & "= sPre"
		'msgbox sSuf & "= sSuf"
		sFile = sPre & lYear & sMonth & sDay & sSuf & sInc & sExt
	End Sub
	
	
'******************************* SOURCE Long DASHED PREFIX & SUFFIX DATE **********************************************************	

	'o:\Upload\Acc\185\feed\PreDLDateSuf.txt STANDING_FULL_- eod
		
		Sub sPreLDashedDateSuf()
	

		'-- Create the Custom filename
		'msgbox sPre & "= sPre"
		'msgbox sSuf & "= sSuf"
		sFile = sPre & lYear & "-" & sMonth & "-" & sDay & sSuf & sInc & sExt
	End Sub

'******************************* SOURCE Long DASHED PREFIX & SUFFIX DATE **********************************************************	
	
	
	'o:\Upload\Acc\185\feed\PreDSDateSuf.txt STANDING_FULL_- eod
	
	Sub sPreSDashedDateSuf()

		'-- Create the Custom filename
		'msgbox sPre & "= sPre"
		'msgbox sSuf & "= sSuf"
		sFile = sPre & sYear & "-" & sMonth & "-" & sDay & sSuf &  sInc & sExt
	End Sub

				
'************************ VALIDATE FILES FUNCTION *******************************************************************



'Return 0 if a file exists else -1

Sub FileExists()

	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	End If 
		
'	LogFile = "O:\AUTO\logs\" & log & "\" & lYear & "\" & sMonth & "\" & lYear & sMonth & sDay & "_" & log & ".txt"
'	Dest = "O:\AUTO\logs\" & log & "\" & lYear & "\" & sMonth & "\"
	LogFile = "O:\AUTO\logs\" & log & "\" & lYear & sMonth & sDay & "_" & log & ".html"
	Dest = "O:\AUTO\logs\" & log & "\"

  	Set fs = CreateObject("Scripting.FileSystemObject")

	sTime = (now)
	
	'th = (DateDiff("h", stTime, sTime))
	'tm = (DateDiff("n", stTime, sTime))
	ts = (DateDiff("s", stTime, sTime))
	intHours = ts \ 3600
	intMinutes = (ts Mod 3600) \ 60
	intSeconds = ts Mod 60
	tDiff =  intHours & ":" & intMinutes & ":" &  intSeconds  
	'MsgBox tDiff

	if fs.FileExists(sPath & sFile) = False then
	  	
		'msgbox sTime & " | " & sPath & sFile & " File Failed to Generate, Please Try Again"
		contents ="<p><span class=failed>" & sTime & " | " & tDiff & " | Failed to Generate File: " & sPath & sFile & "</span></p>"
		call sAppend()
	Else 
	   	iByteSize=GetFileSize
		contents ="<p><span class=ok>" & sTime & " | " & tDiff & " | Bytesize = " & iByteSize & " | File: " & sPath & sFile & "</span></p>"
		'msgbox sPath & sFile & " | Bytesize = " & iByteSize & " | Log File = " & LogFile & sTime
		call sAppend()
	End if

	

end Sub	

'***************

'*********************************

'Return the length of a file or -1 if it does not exist

function GetFileSize()
Dim f
  GetFileSize = -1

  Set fs = CreateObject("Scripting.FileSystemObject")

  if fs.FileExists(sPath & sFile) = True then
    set f = fs.GetFile(sPath & sFile)
    GetFileSize = f.size
  end if
  if fs.FolderExists(sPath & sFile) = True then
    set f = fs.GetFolder(sPath & sFile)
    GetFileSize = f.size
  end if

Set f = Nothing
Set fs = Nothing
end function


'********************************
    	
    	
  	'WScript.Echo sFile
  	
  		

Sub sAppend()


'MsgBox "In Act - Append Function"



    	'*******************
    	
		'wscript.echo ("Source: " & sPath & sFile), ("Dest: " & dPath & dFile ), ("Action: " & action), ("Log" & log)
		'************
		'MsgBox sPath & sFile
		
		'***** PROCESSING *****
		
		
		'Read file contents
		
		'Display results
		'wscript.echo contents
			
		
		' Create the File System Object
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		
		' Check that the WriteDirectory folder exists
		If objFSO.FolderExists(Dest) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		 ' Create the WriteDirectory folder if folder does not exist
		   Set objFolder = objFSO.CreateFolder(Dest)
		   'WScript.Echo "Just created " & Dest
		End If
		
		'msgbox "LogFile Test: " & LogFile
		If objFSO.FileExists(LogFile) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		   Set objFile = objFSO.CreateTextFile(LogFile)
		   'Wscript.Echo "Just created " & LogFile
		End If
		
		set objFile = nothing
		set objFolder = nothing
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		Const ForAppending = 8		
		
		Set objTextFile = objFSO.OpenTextFile _
		(LogFile, ForAppending, True)
		
		
		' Writes WriteFile every time you run this VBScript
		objTextFile.WriteLine(contents)
		objTextFile.Close
		
		
	end sub	

		'*************		
			
	
			'msgbox LogFile & " Appended to " & sPath
	



'***************************************************

Sub getDate(i)

sDay = datepart("w", now)

if sDay = 2 then
	i=3
	
Else
	i=numArgs(3)
	
end If

if Len(Month(Now-i))=1 then 
	sMonth="0" & month(now -i)
else
	sMonth=month(now -i)
end if

if Len(Day(Now-i))=1 then 
	sDay = "0" & day(Now -i)
else
	sDay = day(Now -i)
end if

sYear = Year(Now -i)

' Build filename
sDate = sYear & sMonth & sDay




'wScript.echo "Day: " & sDay	
'WScript.echo "Month: " & sMonth	
'wScript.echo "Year: " & sYear    	
'wScript.echo "Date: " & sDate	

end Sub

' **** END MAIN **** 