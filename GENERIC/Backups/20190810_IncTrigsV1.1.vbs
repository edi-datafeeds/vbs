option explicit

' **** MAIN ****
	
' **** VARIABLES ****

	
	dim fso, WshShell, numArgs, sFile, sPath, sExt, sMonth, sDay, sYear, Source, Dest, x, y, Pre, sDate,iByteSize,log,LogFile,sInc,i
	Dim SVR, step1, Mode, sProd, SQL, sAction, sPre, sSuf, fs,  objFSO,objReadFile,contents,objFolder,objFile,objTextFile,lYear,inc 
	Dim stTime,sTime,tDiff,ts,intHours,intMinutes,intSeconds,sPathUrl,wPath,sSql,CONF,ForReading
	Dim objRS,objConn,test,ErrMsg,iCMDLINE,sCMD,sLog
' **** PREPARATION ****

	
	Set WshShell = WScript.CreateObject("WScript.Shell")
    Set fso = CreateObject("Scripting.FileSystemObject")	        

	'Arguments
	Set numArgs = WScript.Arguments

	sInc = ""

	'Checks to see if the correct number of paramters have been set
	
	If numArgs.length = 1 then
		'Log/Taskfile
		Log = numArgs(0)
		Call getInc()
	Else 
		msgbox "Syntax Err: Incorrect Number of Arguments - Pass taskfile name as parameter"
		wscript.quit
	
	end if

'****************************** GET INCREMENT NUMBER BASED ON TIME ****************************************************
 
Sub getInc()	

	sTime = Time
	sTime = int(left(sTime,2))
	
	if 	sTime >= 17 and  sTime <= 23  then 
		sInc = "3"
		sLog = Log & "_" & sInc
		Call third(sInc,Log,sLog)
		
	elseif sTime >= 12 and sTime <= 16  then 
		sInc = "2"
		sLog = Log & "_" & sInc
		Call second(sInc,Log,sLog)
		'Call Prod(sInc)
		
	elseif sTime >= 7 and sTime <= 11  then 	
		sInc = "1"
		sLog = Log & "_" & sInc
		Call first(sInc,Log,sLog)
		'Call Prod(sInc)
	
	elseif sTime >= 3 and sTime <= 6  then 	
		sInc = "0"
		sLog = Log & "_" & sInc
		Call zero(sInc,Log,sLog)
		'Call Prod(sInc)	
		
	else
		sInc = "0"
		sLog = Log & "_" & sInc
		
	end If	


End Sub 

'******* Production *****************

Sub third(sInc,Log,sLog)
	
	Set WshShell = WScript.CreateObject("WScript.Shell")
	
	sCMD = "" 'intialise to nothing
	
	Select Case Log
		Case "MyWCA_t680i"
			'WCA_t680i= "O:\AUTO\Scripts\vbs\GENERIC\Trig.vbs \MySql\WCA\MyWCAEOD_t680.tsk auxilliary3 "MyWCAEOD t680" WCA_t680i i"
			sCMD= "O:\AUTO\Scripts\vbs\GENERIC\Trig.vbs \MySql\WCA\MyWCAEOD_t680.tsk auxilliary " & chr(34) & "MyWCAEOD t680" & chr(34) & " " & Log & " i"
		'	sCMD= "O:\AUTO\Scripts\vbs\GENERIC\Trig.vbs \TEST.tsk test " & chr(34) & "\MySql\WCA\MyWCAEOD_t680" & chr(34) & " " & Log & " i"
			'msgbox "O:\AUTO\Scripts\vbs\GENERIC\Trig.vbs \MySql\WCA\MyWCAEOD_t680.tsk auxilliary3 " & chr(34) & "MyWCAEOD t680" & chr(34) & " " & Log & " i"
		Case "MyWCA_Statpro"
			sCMD= "O:\AUTO\Scripts\vbs\GENERIC\Trig.vbs \MySql\WCA\statpro_EOD.tsk auxilliary " & chr(34) & "Statpro t690 EOD" & chr(34) & " " & Log & " i"

		Case "15022_steps_EOD"
			sCMD= "O:\AUTO\Scripts\vbs\GENERIC\Trig.vbs \15022_STEPS\15022_steps.tsk auxilliary2 " & chr(34) & "15022 EOD tsk" & chr(34) & " loader_all i"	

		Case "Regen_MySQL_EOD"
			sCMD= "O:\AUTO\Scripts\vbs\GENERIC\Trig.vbs \EODEmailers\Regen_MySQL_iCloud_Emailer.tsk auxilliary3 " & chr(34) & "Regen MySQL EOD" & chr(34) & " loader_all i"	
		
		Case "15022_USBank"
			sCMD= "O:\AUTO\Scripts\vbs\GENERIC\Trig.vbs \15022_STEPS\15022_usbank_i.tsk default " & chr(34) & "15022_US_Bank" & chr(34) & " UsBank i"	
		
		Case "FixedIncome_EOD"
			sCMD= "O:\AUTO\Scripts\vbs\GENERIC\Trig.vbs \Fixed_Income\FixedIncome_EOD.tsk default " & chr(34) & "FixedIncome_EOD" & chr(34) & " loader_all i"	
		
		Case "2"

		Case "3"
		
	End Select


	'step1=WshShell.Run (sCMD, 1, true)
	
	If sCMD <> "" Then 
		step1=WshShell.Run (sCMD, 1, true)
	End If 	
	
	 'msgbox step1

	'WScript.Quit 


End Sub

'******* Production Second INC Production*****************

Sub second(sInc,Log,sLog)
	Set WshShell = WScript.CreateObject("WScript.Shell")
	
	sCMD = "" 'intialise to nothing
	
	Select Case Log
	
		Case "Regen_MySQL"
			sCMD= "O:\AUTO\Scripts\vbs\GENERIC\Trig.vbs \EODEmailers\Regen_MySQL_iCLOUD.tsk auxilliary2 " & chr(34) & "Regen_MySQL_iCLOUD" & chr(34) & " loader_all i"	
	
	End Select

	'step1=WshShell.Run (sCMD, 1, true)
	
	If sCMD <> "" Then 
		step1=WshShell.Run (sCMD, 1, true)
	End If 	
	'msgbox step1

	'WScript.Quit 

End Sub

'******* Production First INC Production*****************

Sub first(sInc,Log,sLog)
	Set WshShell = WScript.CreateObject("WScript.Shell")
	
	sCMD = "" 'intialise to nothing
	
	Select Case Log
		
		Case "Regen_MySQL"
			sCMD= "O:\AUTO\Scripts\vbs\GENERIC\Trig.vbs \EODEmailers\Regen_MySQL_iCLOUD.tsk auxilliary2 " & chr(34) & "Regen_MySQL_iCLOUD" & chr(34) & " loader_all i"	
		
		Case "15022_daiwa_smartstream"
			sCMD= "O:\AUTO\Scripts\vbs\GENERIC\Trig.vbs \15022_STEPS\15022_daiwa_smartstream.tsk auxilliary2 " & chr(34) & "15022_daiwa_smartstream" & chr(34) & " loader_all i"	
		
		Case "15022_USBank"
			sCMD= "O:\AUTO\Scripts\vbs\GENERIC\Trig.vbs \15022_STEPS\15022_usbank_i.tsk default " & chr(34) & "15022_US_Bank" & chr(34) & " UsBank i"	
		
		
	End Select

	'step1=WshShell.Run (sCMD, 1, true)
	
	If sCMD <> "" Then 
		step1=WshShell.Run (sCMD, 1, true)
	End If 	
	'msgbox step1

	'WScript.Quit 

End Sub

'******* Production zero INC Production*****************

Sub zero(sInc,Log,sLog)
	Set WshShell = WScript.CreateObject("WScript.Shell")
	
	sCMD = "" 'intialise to nothing
	
	Select Case Log
		
		Case "15022_USBank"
			sCMD = ""
		
			'sCMD= "O:\AUTO\Scripts\vbs\GENERIC\Trig.vbs \15022_STEPS\15022_usbank_i.tsk default " & chr(34) & "15022_US_Bank" & chr(34) & " UsBank i"	
		
	End Select

	'step1=WshShell.Run (sCMD, 1, true)
	
	If sCMD <> "" Then 
		step1=WshShell.Run (sCMD, 1, true)
	End If 	
	'msgbox step1

	'WScript.Quit 

End Sub

