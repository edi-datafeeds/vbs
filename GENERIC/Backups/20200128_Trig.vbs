
Option explicit 

' **** VARIABLES ****

	
	dim fso, WshShell, numArgs, sMonth, sDay, sYear, Source, Dest, sLog,LogFile,sInc,i
	Dim SVR, step1, fs, objFSO,objReadFile,contents,objFolder,objFile,objTextFile,lYear,inc 
	Dim stTime,sTime,tDiff,ts,intHours,intMinutes,intSeconds,CONF,ForReading,sDate
	Dim objRS,objConn,test,ErrMsg,iCMDLINE,sTask ,sRunCMD
	Dim sTrig,sConf, sTaskfile, sQueue, lTaskfile
' **** PREPARATION ****

	
	Set WshShell = WScript.CreateObject("WScript.Shell")
    Set fso = CreateObject("Scripting.FileSystemObject")	        

	'Arguments
	Set numArgs = WScript.Arguments
	
	'Trigger Application
	sTrig = "j:\java\prog\i.cornish\nb6\j2se\ops\OpsTaskTrigger\dist\OpsTaskTrigger.jar"
	
	'Config used by Trigger App
	sConf = " --config=o:\AUTO\configs\global.cfg"
	
	'Taskfile to be Triggered
	sTaskfile = " -taskfile o:\AUTO\Tasks" & numArgs(0)
	
	'Taskfile for Log display
	lTaskfile = "o:\AUTO\Tasks\" & numArgs(0)
	
	'Queue the Taskfile triggers on
	sQueue = " -" & numArgs(1)
	
	'Name of the Job being triggered
	sTask = " " & Chr(34) & numArgs(2) & chr(34)
	
	'Taskfile for HTML & DB Logs
	slog = numArgs(3)
	
	'Command line ready to run
	sRunCMD = sTrig & sConf & sTaskfile & sQueue & sTask
	
	'Command line for Logging 
	iCMDLINE = Replace(sRunCMD,"\","/")
	
		
	'Logs Config
	CONF="O:\Auto\Configs\DbServers.cfg"

	'Set Incremental NULL
	sInc = ""
	
	'Global Date Function
	i = 0 
	'Date Function
	if Len(Month(Now))=1 then 
	sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
	
	lYear = mid(Year(Now),1,4)
	sYear = mid(Year(Now),3,4)
	sTime = (now)
	stTime = (now)


	'Checks to see if the correct number of paramters have been set

	if numArgs.length = 4 then
		
		
		'CALL PROCESSING FUNCTIONS
		Call Prod()
		
		'wscript.quit
	
	ElseIf numArgs.length = 5 then

		If numArgs(4)= "i" Then
			sInc = numArgs(4)
			Call getInc()
			Call Prod()
		Else
			i = numArgs(4)
			Call getDate(i)
			Call Prod()	
			
		End If 
	ElseIf numArgs.length = 6 then

		If numArgs(4)= "i" Then
			sInc = numArgs(4)
			Call getInc()
		Else
			i = numArgs(4)
			Call getDate(i)
			
		End If 
		If numArgs(5)= "i" Then
			sInc = numArgs(5)
			Call getInc()
		Else
			i = numArgs(5)
			Call getDate(i)
			
		End If 

		Call Prod()

	Else 
		msgbox "Syntax Err: Incorrect Number of Arguments"
		wscript.quit
	
	end if

	
	
'****************************** GET INCREMENT NUMBER BASED ON TIME ****************************************************

Sub getInc()	

	sTime = Time
	sTime = int(left(sTime,2))
	'msgbox sTime
	
	if sLog= "123Trans" Or sLog = "123Trans_FTP" then
		if sTime >= 12 and sTime <= 14  then 
			sInc = "_1"
			sLog = sLog & sInc
		elseif sTime >= 17 and  sTime <= 23  then 
			sInc = "_2"
			sLog = sLog & sInc
		else	
			sInc = "_1"
			sLog = sLog & sInc
		end If
		
	ElseIf sLog= "CABTrans" Or sLog= "CABTrans_FTP" then
		if sTime >= 11 and sTime <= 13  then 
			sInc = "_1"
			sLog = sLog & sInc
	
		elseif 	sTime >= 14 and  sTime <= 15  then 
			sInc = "_2"
			sLog = sLog & sInc
		elseif 	sTime >= 16 and  sTime <= 17  then 
			sInc = "_3"
			sLog = sLog & sInc
		elseif 	sTime >= 18 and  sTime <= 19  then 
			sInc = "_4"
			sLog = sLog & sInc
		else	
			sInc = "_5"
			sLog = sLog & sInc
		end If
	ElseIf sLog = "SMF4" Or sLog = "SMF4_FTP" then
		if sTime >= 11 and sTime <= 16  then 
			sInc = "_1"
			sLog = sLog & sInc
	
		elseif 	sTime >= 17 and  sTime <= 22  then 
			sInc = "_2"
			sLog = sLog & sInc
		
		else	
			sInc = "_1"
			sLog = sLog & sInc
		end If
	Else
		'if sTime >= 12 and sTime <= 17  then 
		if sTime >= 12 and sTime <= 16  then 
			sInc = "_2"
			If sLog = "t15022_Inc" or sLog = "2012_Nasdaq_sedol" Then
				sInc = ""
				Pre = "edi_:_153000"
			End if
			sLog = sLog & sInc
	
		'elseif 	sTime >= 18 and  sTime <= 23  then 
		elseif 	sTime >= 17 and  sTime <= 23  then 
			sInc = "_3"
			If sLog = "t15022_Inc" or sLog = "2012_Nasdaq_sedol" Then
				sInc = ""
				Pre = "edi_:_203000"
			End if
			sLog = sLog & sInc
		else	
			sInc = "_1"
			If sLog = "t15022_Inc" or sLog = "2012_Nasdaq_sedol" Then
				sInc = ""
				Pre = "edi_:_083000"
			End if
			sLog = sLog & sInc
		end If	
	End If 


End Sub 


'******* Production *****************

Sub Prod()

sTime = Time

step1=WshShell.Run (sRunCMD, 1, true)
 'msgbox step1
If Err.Number <> 0 Then
  'MsgBox Err.Number & " " & Err.Description
  	ErrMsg = MsgBox ("Error: " & Err.Number & (Chr(13)) & " " & (Chr(10)) & Err.Description & (Chr(13)) & " " & (Chr(10)) & "What would you like to do?",5,"VBS Error Message")
	'MsgBox ErrMsg
	If ErrMsg = "4" Then
		contents ="<p><span class=note>" & sTime & " | " & tDiff & " | <b>User Retried Production Generation: </b> </span></p><p><span class=note> <b>Command Line:</b> " & iCMDLINE & "</span></p><br/>"
		call sAppend()
		
		'sSql = "INSERT INTO opslog(taskfile, lpath, file, status, Diff, bytesize, Seq, CMDLINE) VALUES ('" & sLog & "', '" & wPath &"', '" & sFile & "', 'Retried', '" & tDiff & "', '" & iByteSize & "', '" & sInc & "', '" & iCMDLINE & "')"
		'Call OpenDbCon(CONF, sSql)
		
		Call Prod()
	ElseIf ErrMsg = "2" Then	
		contents ="<p><span class=failed>" & sTime & " | " & tDiff & " | <b>User Cancelled Production Generation:</b> " & sPathUrl & sFile & "</span></p><p><span class=failed> <b>Command Line:</b> " & iCMDLINE & "</span></p><br/>"
		call sAppend()
		
		'sSql = "INSERT INTO opslog(taskfile, lpath, file, status, Diff, bytesize, Seq, CMDLINE) VALUES ('" & sLog & "', '" & wPath &"', '" & sFile & "', 'Cancelled', '" & tDiff & "', '" & iByteSize & "', '" & sInc & "', '" & iCMDLINE & "')"
		'Call OpenDbCon(CONF, sSql)
	
	End If
Else 
		contents ="<p><span class=note>" & sTime & " | <b>" & lTaskfile & " Triggered to </b> " &  numArgs(1) & " Queue Successfully</span></p><p><span class=note> <b>CMDLINE:</b> " & sRunCMD & "</span></p><br/>"
		call sAppend()
		
		'sSql = "INSERT INTO opslog(taskfile, lpath, file, status, Diff, bytesize, Seq, CMDLINE) VALUES ('" & sLog & "', '" & wPath &"', '" & sFile & "', 'Cancelled', '" & tDiff & "', '" & iByteSize & "', '" & sInc & "', '" & iCMDLINE & "')"
		'Call OpenDbCon(CONF, sSql)
End If

'WScript.Quit 


end Sub

  		

Sub sAppend()
	LogFile = "O:\AUTO\logs\" & sLog & "\" & lYear & sMonth & sDay & "_" & sLog & ".html"
	Dest = "O:\AUTO\logs\" & sLog & "\"

		
		' Create the File System Object
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		
		' Check that the WriteDirectory folder exists
		If objFSO.FolderExists(Dest) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		 ' Create the WriteDirectory folder if folder does not exist
		   Set objFolder = objFSO.CreateFolder(Dest)
		   'WScript.Echo "Just created " & Dest
		End If
		
		'msgbox "LogFile Test: " & LogFile
		If objFSO.FileExists(LogFile) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		   Set objFile = objFSO.CreateTextFile(LogFile)
		   'Wscript.Echo "Just created " & LogFile
		End If
		
		set objFile = nothing
		set objFolder = nothing
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		Const ForAppending = 8		
		
		Set objTextFile = objFSO.OpenTextFile _
		(LogFile, ForAppending, True)
		
		
		' Writes WriteFile every time you run this VBScript
		objTextFile.WriteLine(contents)
		objTextFile.Close
		
		
	end sub	

		'*************		
			
	
			'msgbox LogFile & " Appended to " & sPath
	
	
	
	
Sub getDate(i)

	sDay = datepart("w", now)
	
	if sDay = 2 then
		i=3
		
	Else
		i=i
		
	end If
	
	if Len(Month(Now-i))=1 then 
		sMonth="0" & month(now -i)
	else
		sMonth=month(now -i)
	end if
	
	if Len(Day(Now-i))=1 then 
		sDay = "0" & day(Now -i)
	else
		sDay = day(Now -i)
	end if
	
	lYear = mid(Year(Now),1,4)
	sYear = mid(Year(Now),3,4)
	
	' Build filename
	sDate = lYear & sMonth & sDay

	'Time Now
	sTime = (now)

	'Start Time
	stTime = (now)

end sub
	
	

' **** END MAIN ****


Private Sub OpenDbCon (p_conf, sSql)
Dim fso, f1, ts, s, uname, pword, prov, pcata, dsource,MyArray,SVRL,ErrMsg
SVRL = "HAT_MY_Diesel"
Const ForReading = 1
prov = "MySQL ODBC 5.1 Driver"
'** Get connection details
Set fso = CreateObject("Scripting.FileSystemObject")   
Set ts = fso.OpenTextFile(p_conf, ForReading)
Do 
  s = ts.ReadLine
  MyArray = Split(s, vbTab)
  If MyArray(0) = SVRL Then
    uname = MyArray(2)
    pword = MyArray(3)
    dsource = MyArray(4) 			
    Exit do
  End if		
Loop while NOT ts.AtEndOfStream
ts.Close
'MsgBox uname & pword & dsource

'** connect to database
On Error Resume Next


set objConn = CreateObject("ADODB.Connection")
  objConn.Open "Driver={"&prov &"};Server=" &dsource &";Database=AutoOps;UID="&uname &";PWD=" &pword &";OPTION=133121;"
  set objRS = CreateObject("ADODB.Recordset")
  objRS.Open "opslog", objConn


        objConn.Execute sSql
		set objRS = Nothing
		objConn.Close
		set objConn = Nothing



If Err.Number <> 0 Then
  'MsgBox Err.Number & " " & Err.Description
  	ErrMsg = MsgBox ("Error: " & Err.Number & (Chr(13)) & " " & (Chr(10)) & Err.Description & (Chr(13)) & " " & (Chr(10)) & "What would you like to do?",5,"DB Log Error Message")
	'MsgBox ErrMsg
	If ErrMsg = "4" Then
		contents ="<p><span class=note>" & sTime & " | " & tDiff & " | <b>User Retried DB Log Entry:</b> " & sPathUrl & sFile & " </span></p><p><span class=note><b>Command Line:</b> " & iCMDLINE & "</span></p><br/>"
		call sAppend()
		Call OpenDbCon (p_conf, sSql)
	ElseIf ErrMsg = "2" Then	
		contents ="<p><span class=failed>" & sTime & " | " & tDiff & " | <b>User Aborted DB Log Entry:</b> " & sPathUrl & sFile & " </span></p><p><span class=failed><b>Command Line:</b> " & iCMDLINE & "</span></p><br/>"
		call sAppend()

	WScript.Quit(0)
	End if
End If
On Error Goto 0
'WScript.Quit
End Sub
 
