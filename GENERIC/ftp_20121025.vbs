' ****************************************************************
' GENERIC PUSHFTP.VBS
'
'     Script for pushing files to EDI ftp boxes
' 
' Arguments in order (0) Local path
'                    (1) Remote path
'                    (2) File extension
'		     (3) SERVER
'		     (4) FTP Direction
'		     (5) Task File
'
' ****************************************************************

' **** MAIN ****
	
' **** VARIABLES ****

	dim WshShell,sLocalPath,sRemotePath,sFileext,sOpsFtp,sDirection,sXRC,sLog 

' **** PREPARATION ****
	
	Set colArgs = WScript.Arguments
 
	sLocalPath = "-local " & replace(colArgs(0),"\","/") &" "
	sRemotePath = "-remote " & replace(colArgs(1),"\","/") &" "
	sFileext = "-fileext " & colArgs(2) & " "
	sOpsFtp = "O:\AUTO\Apps\opsftp\dist\opsftp.jar "
	sDirection = "-" & colArgs(4) & " "
	sXRC = "-XCRC on "
	sSite = "-site " & colArgs(3) & " "
	If colArgs(3) = "WALLSTR" Then
		sXRC = "-XCRC off "
	ElseIf colArgs(3) = "MERGENT" Then 
		sXRC = "-XCRC off "	
	Else 	
		sXRC = "-XCRC on "	
	End If 
	TSK = colArgs(5)
			
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		sYear = mid(Year(Now),1,4)

	sLog = "-log o:\AUTO\logs\Ftp.log"
	'sLog = "-log O:\AUTO\logs\" & TSK & "\" & sYear & sMonth & sDay & "_" & TSK & ".txt"
	
	Set WshShell = WScript.CreateObject("WScript.Shell")


' **** PROCESSING ****

	If colArgs(3) = "both" Then

	' FTP FILES TO ActiveHost
	sSite = "-site dotcom "
	ftp2=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog)
		If ftp2 = "1" Then
			ftp2=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)	
				If ftp2 = "1" Then
			'	msgbox ".net Failed"	
				End If
		End If


	' FTP FILES TO LIQUID
	sSite = "-site LIQUID "
	ftp3=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog)
		If ftp3 = "1" Then
			ftp3=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)	
				If ftp3 = "1" Then
			'	msgbox "LIQUID Failed"	
				End If
		End If
	
	
	' FTP FILES TO ICLOUD
	sSite = "-site dotnet "
	ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)
		If ftp1 = "1" Then
		ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)	
			If ftp1 = "1" Then
		'msgbox ".com Failed"	
			End If
		End If
	
	
	ElseIf colArgs(3) = "both2" Then

	' FTP FILES TO DOTNET
	sSite = "-site dotnet "
	ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog)
		If ftp1 = "1" Then
		ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)	
			If ftp1 = "1" Then
		'msgbox ".com Failed"	
			End If
		End If
		
		
	
	ElseIf colArgs(3) = "EMTS" Then
	
	' FTP FILES TO EMTSCOM
	
	sSite = "-site EMTS "
	ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog)
		If ftp1 = "1" Then
		ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)	
			If ftp1 = "1" Then
			'msgbox ".com Failed"	
			End If
		End If

	' FTP FILES TO EMTSNET
	sSite = "-site EMTSNET "
	ftp2=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog)
		If ftp2 = "1" Then
			ftp2=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)	
				If ftp2 = "1" Then
			'	msgbox ".net Failed"	
				End If
		End If
	
	Else 
	
		' FTP FILES TO USER DEFINED SERVER
	'sSite = User Defined
	ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)
		If ftp1 = "1" Then
			ftp1=WshShell.Run (sOpsFtp & sSite & sDirection & sfileext & sLocalPath & sRemotePath & sXRC & sLog, 1, true)	
				If ftp1 = "1" Then
				'	msgbox sSite & " Failed"	
				End If
		End If
	End if
' **** END MAIN **** 