' ****************************************************************
'Generic Feed Loader Script		07/04/2011
'
'    This Script is used to check Feed Loaders have worked and appends the outcome to the named log. Can be used for both SMF & WCA
'    The log files are stored here: "O:\AUTO\logs\" and the final folder name is based on what you parse as the 4th argument on the cmdline

' EXAMPLE CMDLINE
' O:\AUTO\Scripts\vbs\GENERIC\Loader.vbs WCA a 1 SQLSVR1
' 

' CMDLINE BREAKDOWN
' O:\AUTO\Scripts\vbs\GENERIC\Loader.vbs is this file, must be called and fed the following parameters
' Param0: Type of Loader... WCA, SMF, etc
' Param1: Mode to run... "a" for Auto or "m" for Manual 
' Param2: Server Number... 1, 2, 3, etc from loader specific config (in the same folder as exe)
' Param3: Task File Name... ie, WcaWebload_1, WcaWebload_2, etc. Used to create the Log File and Folder
' Param4: Server Name... ie, SQLSVR1, Deisel, Deisel4, ect. Used for better Server Identification in the Log File

'Options 
'Typ: "WCA" or "SMF"
'MODE: "a" for Auto - "m" for Manual - Note; Manual mode never exits with errors because you have to close the program
'SVR: You will have to check the config file specific to the loader you would like to you. The config can usually be found in the same folder as the exe
'TSK: Name of the Task File this VBS is being run from. Used to build a Log File and append the results of the operation to
'SVRN: SQLSVR1, Deisel, Deisel4, etc. This is not used in the processing, only for the logging of results.
' ****************************************************************

Option explicit

' **** MAIN ****
	
' **** VARIABLES ****

	
	dim fso, WshShell, numArgs, sFile, sPath, sExt, dFile, dPath, dExt, sMonth, sDay, sYear, Source, Dest, x, y, Pre, sDate,fSource,iByteSize,log,LogFile,sTime,sInc
	Dim lDate, sPre, dPre, PreSdate, PreLdate, SdateSuf, SdateLuf, sSuf, fs, dFileEx, sFileEx,objFSO,objReadFile,contents,objFolder,objFile,objTextFile,Act,lYear,inc,Mode,sLog 
	Dim TSK, SVRN,SVR,step1,Loader,Typ,wso,Emailer,Prd,User
	CONST ForReading = 1, ForWriting = 2, ForAppending = 8
' **** PREPARATION ****

	Set fso = CreateObject("Scripting.FileSystemObject")
	Set wso = Wscript.CreateObject("Wscript.Shell")

	'Checks to see if the correct number of paramters have been set
	
	Set numArgs = WScript.Arguments
	
	Emailer = "O:\AUTO\Apps\Emailers\edi_port_emailer3\edi_port_emailer.jar -config O:\AUTO\Configs\DbServers.cfg -s Liquid_MySql -l J:\java\Prog\h.patel\EmailAlert\log\ "

	'Mode	
	If numArgs(0)= "a" Then
		Mode = "-a "
	Else 
		Mode = "-m "
	End If 
	Typ = numArgs(1)
	Typ = "-atype " & numArgs(1)
	Prd = numArgs(2)
	Prd = " -prdcd " & numArgs(2)
	'User= numArgs(4)		
	'User = " -username " & numArgs(4)
	
	'Server
		
	'Log File
	TSK = WScript.Arguments.Item(3)
	
	
	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if
			  
			  
	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if
		
		lYear = mid(Year(Now),1,4)
		sYear = mid(Year(Now),1,4)
		sTime = (now)
	
	LogFile = "O:\AUTO\logs\" & TSK & "\" & sYear & sMonth & sDay & "_" & TSK & ".html"
	Dest = "O:\AUTO\logs\" & TSK & "\"



'***** Processing *****

Set WshShell = WScript.CreateObject("WScript.Shell")
'msgbox "O:\AUTO\Apps\exe\isql.exe " & cmdline & " -i " & SQL & " -o " & TSK
'msgbox Loader & Mode & Source & SVR & sLogk

step1=WshShell.Run (Emailer & Mode & Typ & Prd & " -dt 2014-11-30", 1, true)
'step1=WshShell.Run (Emailer & Mode & Typ & Prd, 1, true)
'step1=WshShell.Run (Emailer & Mode & Typ & Prd & " -dt 2012-11-20", 1, true)
'step1=WshShell.Run (Emailer & Mode & Typ & Prd & " -username Youness", 1, true)


'msgbox Emailer & Mode & Typ& Prd & user
' Validate Load
if step1 = "0" Then
 	contents = sTime & " | " & Typ & " Emailer to " & Prd & " Loaded Sucessfully"
	call sAppend()
	'msgbox Typ & " Emailer to " & Prod & " Loaded Sucessfully"	
Else
 	contents = sTime & " | " & Typ & " Emailer to " & Prd & " Failed to Load"
 	 step1 = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER ops@exchange-data.com -PASS BLOCH1 -TO y.migou@exchange-data.com -SUB " & Typ & " Emailer Error Alert to " & Prd)
 	 'y =     wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1 -TO webmaster@exchange-data.com;ukcorporateactions@citigroup.com;phil.c.davies@citigroup.com -SUB EDI Wincab docs are available") 
	Call sAppend()	
	msgbox TSK & " " & numArgs2() & numArgs(1) & " Emailer Failed to Send"
	
end if	



Sub sAppend()


'MsgBox "Append Function"

		
		'***** PROCESSING *****
				
		
		' Create the File System Object
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		
		' Check that the WriteDirectory folder exists
		If objFSO.FolderExists(Dest) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		 ' Create the WriteDirectory folder if folder does not exist
		   Set objFolder = objFSO.CreateFolder(Dest)
		   'WScript.Echo "Just created " & Dest
		End If
		'msgbox "LogFile Test: " & LogFile
		If objFSO.FileExists(LogFile) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		   Set objFile = objFSO.CreateTextFile(LogFile)
		   'Wscript.Echo "Just created " & LogFile
		End If
		
		set objFile = nothing
		set objFolder = nothing
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		Const ForAppending = 8		
		
		Set objTextFile = objFSO.OpenTextFile _
		(LogFile, ForAppending, True)
		
		
		' Writes WriteFile every time you run this VBScript
		objTextFile.WriteLine(contents)
		objTextFile.Close
		
	'	WScript.Quit(0)
End sub	