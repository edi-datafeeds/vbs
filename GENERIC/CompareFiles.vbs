Option Explicit

Dim objFSO, strFileList, objFileList, strFile
Dim strFolder1, strFolder2, objShell, strCmd
Dim strTemp, objTempFile, blnOK

Const ForReading = 1

' Specify the list of file names.
strFileList = "c:\Scripts\FileList.txt"
' Specify the two folders. Files will be compared in these folders.
strFolder1 = "c:\Scripts\Temp1"
strFolder2 = "c:\Scripts\Temp2"
' Specify a temporary file.
strTemp = "c:\Scripts\Temp.dif"

' Open the list of file names
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objFileList = objFSO.OpenTextFile(strFileList, ForReading)

Set objShell = CreateObject("Wscript.Shell")

' Read the file names.
blnOK = True
Do Until objFileList.AtEndOfStream
strFile = Trim(objFileList.ReadLine)
' Skip blank lines.
If (strFile <> "") Then
' Compare the files in the two folders using the fc command.
' Check for string "no differences encountered".
strCmd = "%comspec% /c fc """ & strFolder1 & "\" & strFile _
& """ """ & strFolder2 & "\" & strFile _
& """ | find ""no differences encountered"" > """ _
& strTemp & """"
objShell.Run strCmd, 0, True
' Check if file has zero bytes.
Set objTempFile = objFSO.GetFile(strTemp)
If (objTempFile.Size = 0) Then
Wscript.Echo "File " & strFile & " not the same in both folders"
blnOK = False
End If
End If
Loop
objFileList.Close

If (blnOK = True) Then
Wscript.Echo "All files identical"
Else
Wscript.Echo "All files NOT identical"
End If
=You can echo the value of the string variable strCmd to the command prompt
to see the command used to compare the two files. For a file called
Example.vbs, it would be similar to (one line):

%comspec% /c fc "c:\Scripts\Temp1\Example.vbs" "c:\Scripts\Temp2\Example.vbs
| find "no differences encountered" > "c:\Scripts\Temp.dif"