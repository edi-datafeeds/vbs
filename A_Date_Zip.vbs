' ****************************************************************
' GENERIC DATE_ZIP.VBS
'
'     Script for zipping and producing custom zip file
' 
' Arguments in order (0) Client_ID	         e.g. 1
'                    (1) Source		         e.g. c:\autoexec.bat
'
' ****************************************************************

' **** SUB MAIN ****
	dim fso,f,L1,wso,colArgs,fldr,fcol,lastday,Seclast,CRLF,sSource,sOpspath,sTxtfile,sTargfile,sDatename,sArchpath

	CRLF = Chr(13) & Chr(10)

	Set colArgs = WScript.Arguments
 
	sClientID = colArgs(0)
	sSource = colArgs(1)
	sOpspath = "o:\worldequ\wsoexp\"
	
 
	'Response = MsgBox(CRLF & "------------------------------------------------------------" & CRLF & CRLF & _
                        ' "              Custom DateZip" & CRLF & CRLF & _
                        ' "------------------------------------------------------------" & CRLF, vbOkCancel)

	'If Response = vbOK Then  
		Set fso = CreateObject("Scripting.FileSystemObject")
		Set wso = Wscript.CreateObject("Wscript.Shell")
	
		'-- Call custom routine by using the sClientID
		'		which was passed via command Line argument(0)
		Select case sClientID
  		case "1"
				Call valorInform
  		case "2"
				Call FactSet
  		case "3"
				Call Deutsche
  		case "4"
				Call Factudr
  		case "5"
				Call Xcitek
  		case "6"
				Call Uklse
  		case "7"
				Call GrabWca
  		case "8"
				Call Smf2Wca
  		case "9"
				Call JapZip
  		case "10"
				Call FidWca
  		case "11"
				Call FidWdi
		case "12"
				Call Msciwca
		case "13"
				Call schroders
		case "14"
 				Call zip188
		case "15"
 				Call ziprbc
		case "16"
 				Call zip645
		case "17"
 				Call zip620
 		case "18"
 				Call zip101
 		case "19"
		 		Call zipGBsos
 		case "20"
		 		Call zipAIGudr
		case "21"
		 		Call zipAIGsmf
		case "22" 		
		 		Call Carryquote_653
 		case "23" 		
		 		Call fnz_209
		case "24"
				Call Factset_212
		case "25"
				Call rbc
		end select	
	'End If

' **** END SUB MAIN ****

'**************************************************************
'	Remove all reference to any object created early in this
' script
'
Sub TidyUp()
 	msg = "Operation has completed"
 	msgbox msg,,"DateZip.vbs"
 	if fso.fileexists(sOpspath & sCustomName & ".TXT") then
 		fso.DeleteFile(sOpspath & sCustomName & ".TXT")
 	End If
  Set fso = Nothing
  Set wso = Nothing
End Sub


'**************************************************************
'	Custom routine for Client number 1 (valorinform)
'		1) Check that the day is a tuesday
'		2) Make a copy of the source file specified in command line
'		   and name it 'VALMMDD.TXT'
'		3) Add the file specified in 2 to a zip file of the
'			 same name 'VALMMDD.ZIP'
'
Sub valorInform()  
   
 	'-- Exit if Today is NOT Tuesday
 	if weekday(date)<>vbTuesday then
 		msg = "The specified client can only be run on a Tuesday" & CRLF & _
 					"and will not run today"
 		msgbox msg,vbinformation
 		call TidyUp
 		Exit Sub 		
 	end if
 			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  
  '-- Create the Custom filename without a extension  
  sCustomName = "VAL" & sMonth & sDay 

  '-- CopyFile the source file to the new custom name & .TXT
  fso.CopyFile sSource, sOpspath & sCustomName & ".TXT",true
	
  '- Zip copied file to Custom name & .ZIP  
  Y = wso.Run ("O:\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a " & sOpspath & sCustomName & ".ZIP " & sOpspath & sCustomName & ".TXT", 1, TRUE)
    
  ' Send File via SendFTP.exe (FTP)
  Y = wso.Run ("O:\utilities\SendFTP.exe UP valorftp.ft.com ediftp ed1ftp " & sOpspath & sCustomName & ".ZIP " & sCustomName & ".ZIP")
  
  if fso.fileexists(sOpspath & sCustomName & ".TXT") then
 	fso.DeleteFile(sOpspath & sCustomName & ".TXT")
  End If
  
end sub


'**************************************************************
'	Custom routine for Client number 2 (Factset)
'		1) Check that the day is a Tuesday
'		2) Make a copy of the source file specified in command line
'		   and name it 'DDMMYYYY.TXT'
'		3) Add the file specified in 2 to a zip file of the
'			 same name 'DDMMYYYY.ZIP'
'**************************************************************
Sub FactSet()  
  
 	'-- Exit if Today is NOT Tuesday
' 	if weekday(date)<>vbTuesday then
' 		msg = "The specified client can only be run on a Tuesday" & CRLF & _
' 					"and will not run today"
' 		msgbox msg,vbinformation
' 		call TidyUp
' 		Exit Sub 		
' 	end if
 			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  
  sYear = Year(Now)
  
  
  '-- Create the Custom filename without a extension  
  sCustomName = sDay &  sMonth & sYear
  
  '-- CopyFile the source file to the new custom name & .TXT
  msgbox sSource
  msgbox sCustomName & ".TXT"
  
  fso.CopyFile sSource, sOpspath & sCustomName & ".TXT",True
	
  '- Zip copied file to Custom name & .ZIP  
  Y = wso.Run ("O:\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a " & sOpspath & sCustomName & ".ZIP " & sOpspath & sCustomName & ".TXT", 1, TRUE)
  Y = wso.Run ("O:\utilities\SendFTP.exe UP www.exchange-data.net barrel K376:lcnb " & sOpspath & sCustomName & ".ZIP " & "Wso/Hist/" & sCustomName & ".ZIP")
  Y = wso.Run ("O:\utilities\SendFTP.exe UP www.exchange-data.com barrel K376:lcnb " & sOpspath & sCustomName & ".ZIP " & "Wso/Hist/" & sCustomName & ".ZIP")
  

  ' Send File via o:\utilities\sendfile.exe (EMain)
  'Y = wso.Run ("o:\utilities\sendfile " & sCustomName & ".ZIP  <Address1;Address2> <Subject text>" 
  
	if fso.fileexists(sOpspath & sCustomName & ".TXT") then
		fso.DeleteFile(sOpspath & sCustomName & ".TXT")
 	End If
end sub


'**************************************************************
'	Custom routine for Client number 3 (Deutsche)
'		1) Add the file specified to a zip file of the
'			 same name 'ALYYMMDD.ZIP'
'
Sub Deutsche()  
   
 			
	sOpspath= "O:\Datafeed\UDR\db\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = mid(Year(Now),3,2)
  
  '-- Create the Custom filename without a extension  
  sCustomName = "AL" & sYear & sMonth & sDay 

  '- Zip copied file to Custom name & .ZIP  
  Y = wso.Run ("O:\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a " & sOpspath & sCustomName & ".ZIP " & sOpspath & sCustomName & ".*", 1, TRUE)
  
  
  ' Send File via SendFTP.exe (FTP)
  'Y = wso.Run ("O:\Apps\vbe\SendFTP.exe UP www.exchange-data.net barrel K376:lcnb " & sOpspath & sCustomName & ".ZIP " & "d:/inetpub/ftproot/UDR/AL/" & sCustomName & ".ZIP")
  'Y = wso.Run ("O:\Apps\vbe\SendFTP.exe UP www.exchange-data.com barrel K376:lcnb " & sOpspath & sCustomName & ".ZIP " & "UDR/AL/" & sCustomName & ".ZIP")
  
	if fso.fileexists(sOpspath & sCustomName & ".DR1") then
 		fso.DeleteFile(sOpspath & sCustomName & ".DR1")
 	End If
 	if fso.fileexists(sOpspath & sCustomName & ".DR2") then
 		fso.DeleteFile(sOpspath & sCustomName & ".DR2")
 	End If
 	if fso.fileexists(sOpspath & sCustomName & ".DR3") then
 		fso.DeleteFile(sOpspath & sCustomName & ".DR3")
 	End If
end sub

'**************************************************************
'	Custom routine for Client number 2 (Factset)
'		1) Check that the day is a Tuesday
'		2) Make a copy of the source file specified in command line
'		   and name it 'DDMMYYYY.TXT'
'		3) Add the file specified in 2 to a zip file of the
'			 same name 'DDMMYYYY.ZIP'
'**************************************************************

Sub FactUdr()  
  
'**************************************************************
'	Custom routine for Client number 4 (fACTSET udr)
'		1) Add the file specified to a zip file of the
'			 sent uncompressed 'UDYYMMDD.txt'
'
   
  sOpspath= "O:\udrsql\OUTPUTS\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = mid(Year(Now),3,2)
  
  '-- Create the Custom filename without a extension  
  sCustomName = "UD" & sYear & sMonth & sDay 

    
  ' Send File via o:\utilities\sendfile.exe (EMain)
  Y = wso.Run ("o:\utilities\sendfile " & sOpspath & sCustomName & ".TXT vendor_mail@factset.com Daily UDR datafeed") 

  ' Send File via SendFTP.exe (FTP)
  'Y = wso.Run ("O:\utilities\SendFTP.exe UP gateway1.factset.com vendor_edi@datafeed-ct bounkert " & sOpspath & sCustomName & ".TXT /" & sCustomName & ".TXT")
  
'  if fso.fileexists(sOpspath & sCustomName & ".TXT") then
' 	fso.DeleteFile(sOpspath & sCustomName & ".TXT")
'  End If
end sub


Sub Xcitek()  
  
'**************************************************************
'	Custom routine for opsid number 5 (xcitek wca raw feed)
'		1) Rename file to date based file name from first line 
'          data file
'
   
  sOpspath= "O:\wcasql\xcitek\"			
  sTargfile = "MARSC11H"
  sTxtfile = "XCITEK.TXT"

  ' Grab File via SendFTP.exe (FTP)
  Y = wso.Run ("O:\utilities\SendFTP.exe DOWN ftp.xcitek.com exch0404 exch0404 " & sOpspath & sTargfile&".zip /XCITEK/CORPORATE/" & sTargfile&".ZIP",,true)

  if fso.fileexists(sOpspath & sTxtfile) then
    fso.DeleteFile(sOpspath & sTxtfile)
  End If  

  '- Unpack file
  'Y = wso.Run (sOpspath & sTargfile, 1, TRUE)
  '- UnZip copied file to Custom name & .ZIP  
'  Y = wso.Run ("pkunzip " & sOpspath & sTargfile & ".ZIP ", 1, TRUE)
  Y = wso.Run ("o:\scripts\unzipxci.bat", 1, TRUE)
    
  
  Set f = fso.OpenTextFile(sOpspath & sTxtfile, 1)
  L1= f.ReadLine
  
  sMonth = mid(L1,8,2)
  sday = mid(L1,11,2)
  sYear = mid(L1,14,4)
  
  Response = MsgBox(CRLF & "------------------------------------------------------------" & CRLF & CRLF & _
                         "      Packing fixed feed for "& sYear&sMonth&sDay & CRLF & CRLF & _
                         "------------------------------------------------------------" & CRLF, vbOkCancel)
  
  If Response = vbOK Then  

    sDatename="XF"&mid(sYear,3,2)&sMonth&sDay
    sArchpath=sOpspath&"archive\"&sYear&sMonth&"\"

   
    If not (fso.FolderExists(sArchpath)) Then
      fso.CreateFolder(sArchpath)
    End If
   
'    If (fso.FolderExists(sArchpath)) Then
      ' nothing to do
	'Else
'       fso.CreateFolder(sArchpath)
'    End If

    if fso.fileexists(sArchpath & sDatename & ".TXT") then
      fso.DeleteFile(sArchpath & sDatename&".TXT")
	End If  
    fso.CopyFile sOpspath & sTxtfile, sOpspath & sDatename & ".TXT", true
    fso.CopyFile sOpspath & sTxtfile, sArchpath & sDatename & ".TXT", true


    '- Zip copied file to Custom name & .ZIP  
    Y = wso.Run ("O:\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a " & sOpspath & sDatename & ".ZIP " & sOpspath & sDatename & ".TXT", 1, TRUE)

    ' Grab File via SendFTP.exe (FTP)
    Y = wso.Run ("O:\utilities\SendFTP.exe UP www.exchange-data.net barrel K376:lcnb " & sOpspath & sDatename & ".zip /Custom/bahar/XCITEK/" & sDatename & ".ZIP",,true)
    Y = wso.Run ("O:\utilities\SendFTP.exe UP www.exchange-data.com barrel K376:lcnb " & sOpspath & sDatename & ".zip /Custom/bahar/XCITEK/" & sDatename & ".ZIP",,true)
    'fso.DeleteFile(sOpspath & sTargfile & ".zip")
	'fso.DeleteFile(sOpspath & sTxtfile)


  End If

call tidyup  
  
end sub


Sub uklse()  
  
'**************************************************************
'	Custom routine for opsid number 6 (UKLSE wincab for Janak)
'
	sOpspath= "O:\dayedit\cem\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = mid(Year(Now),3,2)
  
  '-- Create the Custom filename without a extension  
  sCustomName = "UK" & sYear & sMonth & sDay 

  '- Zip copied file to Custom name & .ZIP  
  Y = wso.Run ("O:\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a " & sOpspath & sCustomName & ".ZIP " & sOpspath & "*.TXT", 1, TRUE)
  
  
  ' Send File via SendFTP.exe (FTP)
  Y = wso.Run ("O:\utilities\SendFTP.exe UP www.exchange-data.net barrel K376:lcnb " & sOpspath & sCustomName & ".zip /Custom/bahar/UKLSE/" & sCustomName & ".ZIP",,true)
  Y = wso.Run ("O:\utilities\SendFTP.exe UP www.exchange-data.com barrel K376:lcnb " & sOpspath & sCustomName & ".zip /Custom/bahar/UKLSE/" & sCustomName & ".ZIP",,true)
  
	if fso.fileexists(sOpspath & sCustomName & ".zip") then
 		fso.DeleteFile(sOpspath & sCustomName & ".zip")
 	End If

end sub



Sub Smf2Wca()  
  
'**************************************************************
'	Custom routine for opsid number 8 (SMF daily for Janak)
'
	sOpspath= "O:\smfsql\wca\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = mid(Year(Now),3,2)
  
  '-- Create the Custom filename without a extension  
  sCustomName = "LS" & sYear & sMonth & sDay 

  '- Zip copied file to Custom name & .ZIP  
  Y = wso.Run ("O:\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a " & sOpspath & sCustomName & ".ZIP " & sOpspath & "*.TXT", 1, TRUE)
  
  
  ' Send File via SendFTP.exe (FTP)
  Y = wso.Run ("O:\utilities\SendFTP.exe UP www.exchange-data.net barrel K376:lcnb " & sOpspath & sCustomName & ".zip /Custom/bahar/UKLSE/" & sCustomName & ".ZIP",,true)
  Y = wso.Run ("O:\utilities\SendFTP.exe UP www.exchange-data.com barrel K376:lcnb " & sOpspath & sCustomName & ".zip /Custom/bahar/UKLSE/" & sCustomName & ".ZIP",,true)
  
	if fso.fileexists(sOpspath & sCustomName & ".zip") then
 		fso.DeleteFile(sOpspath & sCustomName & ".zip")
 		fso.DeleteFile(sOpspath & sCustomName & ".txt")
 	End If

end sub


Sub GrabWca()  
  
'**************************************************************
'	Custom routine for opsid number 7
'   1) Rename file to date based file name from first line 
'      data file
'
   
  sOpspath= "O:\wcasql\feeds\"			

  call genfile
  
  '- Unpack file
  'Y = wso.Run (sOpspath & sTargfile, 1, TRUE)
  '- UnZip copied file to Custom name & .ZIP  
'  Y = wso.Run ("pkunzip " & sOpspath & sTargfile & ".ZIP ", 1, TRUE)
  'Y = wso.Run ("o:\scripts\unzipxci.bat", 1, TRUE)
    
  
  'Response = MsgBox(CRLF & "------------------------------------------------------------" & CRLF & CRLF & _
'                         "      Packing fixed feed for "& sYear&sMonth&sDay & CRLF & CRLF & _
'                         "------------------------------------------------------------" & CRLF, vbOkCancel)
  
'  If Response = vbOK Then  
'
'    sDatename="XF"&mid(sYear,3,2)&sMonth&sDay
'    sArchpath=sOpspath&"archive\"&sYear&sMonth&"\"
'
   
'    If not (fso.FolderExists(sArchpath)) Then
'      fso.CreateFolder(sArchpath)
'    End If
   
'    If (fso.FolderExists(sArchpath)) Then
      ' nothing to do
	'Else
'       fso.CreateFolder(sArchpath)
'    End If

'    if fso.fileexists(sArchpath & sDatename & ".TXT") then
'      fso.DeleteFile(sArchpath & sDatename&".TXT")
'	End If  
'    fso.CopyFile sOpspath & sTxtfile, sOpspath & sDatename & ".TXT", true
'    fso.CopyFile sOpspath & sTxtfile, sArchpath & sDatename & ".TXT", true


    '- Zip copied file to Custom name & .ZIP  
'    Y = wso.Run ("O:\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a " & sOpspath & sDatename & ".ZIP " & sOpspath & sDatename & ".TXT", 1, TRUE)

    ' Grab File via SendFTP.exe (FTP)
'    Y = wso.Run ("O:\utilities\SendFTP.exe UP eurotracker.com edi_19 vpg_9508 " & sOpspath & sDatename & ".zip FEEDS/XCITEK/" & sDatename & ".ZIP",,true)
    'fso.DeleteFile(sOpspath & sTargfile & ".zip")
	'fso.DeleteFile(sOpspath & sTxtfile)


'  End If

'call tidyup  
  
end sub


sub genfile()

  if Len(Month(Now-2))=1 then
  	sMonth="0" & month(now-2)
  else
  	sMonth=month(now-2)
  end if
  
  if Len(Day(now-2))=1 then 
  	sDay = "0" & day(Now-2)
  else
  	sDay = day(Now-2)
  end if

  sYear = mid(Year(Now-2),1,4)
  
  '-- Create the Custom filename
  sCustomName = sYear & sMonth & sDay & ".zip"

  ' Grab File via SendFTP.exe (FTP)
  Y = wso.Run ("O:\utilities\SendFTP.exe DOWN eurotracker.com ediadmin inter " & sOpspath & sCustomname & " /inetpub/eurotracker/data/data_19/feeds/" & sCustomname,,true)

end sub


'**************************************************************
'	Custom routine for Client number 3 (Deutsche)
'		1) Add the file specified to a zip file of the
'			 same name 'ALYYMMDD.ZIP'
'
Sub JapZip()  
			
	sOpspath= "O:\WCASQL\JPCA\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = mid(Year(Now),3,2)
  
  '-- Create the Custom filename without a extension  
  sCustomName = "JP" & sYear & sMonth & sDay 

  '- Zip copied file to Custom name & .ZIP  
  Y = wso.Run ("o:\support\wzzip\wzzip " & sOpspath & sCustomName & ".ZIP " & sOpspath & "*.*", 1, TRUE)
  
  
  ' Send File via SendFTP.exe (FTP)
  Y = wso.Run ("O:\utilities\SendFTP.exe UP eurotracker.com ediadmin inter " & sOpspath & sCustomName & ".ZIP data/data_19/Feeds/Japan_z/" & sCustomName & ".ZIP")
  
'	if fso.fileexists(sOpspath & sCustomName & ".DR1") then
' 		fso.DeleteFile(sOpspath & sCustomName & ".DR1")
' 	End If
' 	if fso.fileexists(sOpspath & sCustomName & ".DR2") then
' 		fso.DeleteFile(sOpspath & sCustomName & ".DR2")
' 	End If
' 	if fso.fileexists(sOpspath & sCustomName & ".DR3") then
' 		fso.DeleteFile(sOpspath & sCustomName & ".DR3")
' 	End If
end sub


Sub fidwca()  
  
'**************************************************************
'	Custom routine for Client number 10 
'	   send uncompressed yyyymmdd.608
'
   
  sOpspath= "O:\wcasql\608_not_us_ca\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay
    
  ' Send File via o:\utilities\sendfile.exe (EMain)
  Y = wso.Run ("o:\utilities\sendfile " & sOpspath & sCustomName & ".608 robert.nyman@FMR.com Backup1 WCA 608 feed") 
  ' Y = wso.Run ("o:\utilities\sendfile " & sOpspath & sCustomName & ".608 germain.seac@FMR.com Backup2 WCA 608 feed") 

end sub


Sub fidwdi()  
  
'**************************************************************
'	Custom routine for Client number 11 
'	   send uncompressed yyyymmdd.513
'
   
  sOpspath= "O:\worldequ\fdi\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay
    
  ' Send File via o:\utilities\sendfile.exe (EMain)
  Y = wso.Run ("o:\utilities\sendfile " & sOpspath & sCustomName & ".513 robert.nyman@FMR.com Backup1 WDI 513 feed") 
  ' Y = wso.Run ("o:\utilities\sendfile " & sOpspath & sCustomName & ".513 germain.seac@FMR.com Backup2 WDI 513 feed") 

end sub

Sub msciwca()  
' *trial 3/11/2003
  
'**************************************************************
'	Custom routine for Client number 12
'	   send uncompressed csv version yyyymmdd.514
'
   
  sOpspath= "O:\wcasql\610\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay
    
  ' Send File via o:\utilities\sendfile.exe (EMain)
  Y = wso.Run ("o:\utilities\sendfile " & sOpspath & sCustomName & ".610 pierre.laugeri@msci.com 610 feed") 
  Y = wso.Run ("o:\utilities\sendfile " & sOpspath & sCustomName & ".610 violet.lentz@msci.com 610 feed")

end sub


Sub schroders()  
  
'**************************************************************
'	Custom routine for Client number 13
'	   send zipped LSTAT 620 yyyymmdd.zip
   
  sOpspath= "O:\Datafeed\Equity\620_LSTAT\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay
    
  ' Send File via o:\utilities\sendfile.exe
  'Y = wso.Run ("o:\utilities\sendfile " & sOpspath & sCustomName & ".zip *UKSIM-CorporateActions@schroders.com EDI LSTAT extract") 
  'Y = wso.Run ("o:\utilities\sendfile " & sOpspath & sCustomName & ".zip David.Dobinson@schroders.com EDI LSTAT extract")
  Y = wso.Run ("o:\utilities\sendfile " & sOpspath & sCustomName & ".620 b.james@exchange-data.com EDI LSTAT extract test email")
  Y = wso.Run ("o:\utilities\sendfile " & sOpspath & sCustomName & ".620 *UKSIM-CorporateActions@schroders.com EDI LSTAT extract test email")

end sub


Sub zip188()  
  
'**************************************************************
   
  sOpspath= "O:\Upload\Acc\188\feed\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & ".zip"
    
  Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub ziprbc()  
  
'**************************************************************
   
  sOpspath= "O:\Datafeed\Debt\rbc\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & ".zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub zip645()  
  
'**************************************************************
   
  sOpspath= "O:\Datafeed\Equity\645_loose\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & "_645.zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub



Sub zip620()  
  
'**************************************************************
   
  sOpspath= "O:\Datafeed\Equity\620_loose\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & "_620.zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub


Sub zip101()  
  
'**************************************************************
   
  sOpspath= "O:\smfsql\align\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = "101_FULL.zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf 101 -notidy",,true)

end sub

Sub zipGBsos()  
  
'**************************************************************
   
  sOpspath= "O:\datafeed\wso\gbsos\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = "GB_SOS.ZIP"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf 501 -notidy",,true)

end sub

Sub zipAIGudr()  
  
'**************************************************************
   
  sOpspath= "O:\udrsql\aigdup\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = "AIGUDR.zip"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub zipAIGsmf()  
  
'**************************************************************
   
  sOpspath= "O:\DATAFEED\SMF\aigsmf\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = "AIGSMF.ZIP"
    
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & sOpspath & " -m " & sCustomName & " -suf TXT -notidy",,true)

end sub

Sub Carryquote_653()  
  
'**************************************************************
'	Custom routine for opsid number 22
'
	sOpspath= "O:\datafeed\equity\653_full\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = mid(Year(Now),3,2)
  
  '-- Create the Custom filename without a extension  
  sCustomName = "AL" & sYear & sMonth & sDay 
  
  'wscript.echo sCustomName 

  'fso.DeleteFile(sOpspath & ".zip")

  '- Zip copied file to Custom name & .ZIP  
  Y = wso.Run ("O:\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a " & sOpspath & sCustomName & ".ZIP " & sOpspath & "*.653", 1, TRUE)
  
   	

end sub

Sub fnz_209()  
  
'**************************************************************
'	Custom routine for opsid number 23
'
	sOpspath= "o:\datafeed\debt\fnz\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)
  
  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & "_209" 
  
  'wscript.echo sCustomName 

  fso.DeleteFile(sOpspath & "*.zip")

  '- Zip copied file to Custom name & .ZIP  
  Y = wso.Run ("O:\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a " & sOpspath & sCustomName & ".zip " & sOpspath & "*.txt", 1, TRUE)
  
  
end sub


Sub Factset_212()  
  
'**************************************************************
'	Custom routine for opsid number 24
'
	sOpspath= "o:\upload\acc\212\feed\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)
  
  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & "_212" 
  
  'wscript.echo sCustomName 

  'fso.DeleteFile(sOpspath & "*.zip")

  '- Zip copied file to Custom name & .ZIP  
  Y = wso.Run ("O:\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a " & sOpspath & sCustomName & ".zip " & sOpspath & "*.txt", 1, TRUE)
  
  
 	

end sub

Sub rbc()  
  
'**************************************************************
'	Custom routine for opsid number 25
'
	sOpspath= "O:\Datafeed\Debt\rbc\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
    	sMonth="0" & month(now)
    else
    	sMonth=month(now)
    end if
    
    if Len(Day(now))=1 then 
    	sDay = "0" & day(Now)
    else
    	sDay = day(Now)
    end if
  
    sYear = Year(Now)
  
  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay 
  
  'wscript.echo sCustomName 

  'fso.DeleteFile(sOpspath & "*.zip")

  '- Zip copied file to Custom name & .ZIP  
  Y = wso.Run ("O:\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a " & sOpspath & sCustomName & ".zip " & sOpspath & "*.txt", 1, TRUE)
  
  
 	

end sub
