option explicit


'**************************************************************************
' 		 GENERIC CopyFileOnly.VBS
' 	
' Script for copying files from one directory to another
'
' Arguments in order (0) Source Path
'		     (1) Destination Path
'		     (2) sFileext
'		     (3) Sdate 	
'**************************************************************************

'***** MAIN *****

'***** VARIABLES *****

	dim fso, WshShell, sPath, dPath, sFileext, sFileext2, sdate, numArgs, sYear, sMonth, sDay

'***** PREPERATION *****


	numArgs = WScript.Arguments.Count

	sPath = WScript.Arguments.Item(0)
	dPath = WScript.Arguments.Item(1)
	sFileext = WScript.Arguments.Item(2)
	
	
	'assign variable arguments if they exist
		if wscript.arguments.count > 3 then
			sFileext2 = WScript.Arguments.Item(3)	
	end if
	
	
	

	Set WshShell = WScript.CreateObject("WScript.Shell")
	Set fso = CreateObject("Scripting.FileSystemObject")


'***** PROCESSING *****



'Use this paramter to get the date name of the file
		if WScript.Arguments.Item(2)="sdate" Then
		
		
		
		'-- Build custom output file name, ensuring that
		  '		both sMonth and sDay are always two digits
		  
		  if Len(Month(Now))=1 then 
		  	sMonth="0" & month(now)
		  else
		  	sMonth=month(now)
		  end if
		  
		  if Len(Day(now))=1 then 
		  	sDay = "0" & day(Now)
		  else
		  	sDay = day(Now)
		  end if
		
		  sYear = mid(Year(Now),1,4)
		  
		  '-- Create the Custom filename
		 
		    sFileext = sYear & sMonth & sDay & sFileext2
	
			
		End if
		
		
		


'msgbox sFileext
	if sFileext <> "*" then
		copyfile(sPath & "*" & sFileext)
	
	
	
		
	End if	


Private Sub copyfile(sFilename)

wscript.echo sFilename

'wscript.echo sPath & sFileext	
	'fso.CopyFile(sFilename), dPath
	
on error resume next
fso.CopyFile(sFilename), dPath
if err.number > 0 then
	'msgbox(err.number & " there are no files to copy cnt")
	on error goto 0 
end if	
	
	
	

End Sub
	
'***** END MAIN *****