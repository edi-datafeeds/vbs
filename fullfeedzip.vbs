'********************************************************************************************
'
'                                               pfFeedZipper
'
'********************************************************************************************

Option Explicit

'**** VARIABLES ****

Dim SourcePath, FileExt, oFileExt, SVR, rs, FileDate, Dbconn, WshShell, oFSO, oFolder, oFiles, oFile, CONF,  MyArray, MyArr
Const ForReading = 1, ForWriting = 2, ForAppending = 8, adOpenStatic = 3, adLockOptimistic = 3
Dim ziperr

'**** PREPARATION ****

'Load Argument Data
SourcePath = WScript.Arguments.Item(0)
FileExt = WScript.Arguments.Item(1)

'**** MAIN ****

'Delete existing zip files in sourcefolder
Set oFSO = CreateObject("Scripting.FileSystemObject")
Set oFolder = oFSO.GetFolder(SourcePath)
Set oFiles = oFolder.Files

For Each oFile In oFiles
  If InStr(oFile.Name,FileExt &".zip",1) Then
    oFile.Delete
  End if  
Next

Set WshShell = WScript.CreateObject("Wscript.Shell")

Set oFiles = oFolder.Files
For Each oFile In oFiles
  If InStr(oFile.Name,"." &FileExt,1) Then
    ziperr = WshShell.Run("O:\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a " &SourcePath &Mid(oFile.Name,1,8)&"_" &FileExt &".zip" &" " &SourcePath &oFile.Name, 1, True)
    If ziperr =0 Then
      If InStr(oFile.Name,"." &FileExt,1) Then
        oFile.Delete
      End if
    End if  
  Next
End If




