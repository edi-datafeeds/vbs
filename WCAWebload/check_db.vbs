'IMPORTANT - IP is hardcoded in the exe, if LSE change their ip Cornish will need to change the jar and Himadri will need to recompile the exe.
'03/08/2010 - YM - Script updated to handle new LSE 18:00 bank holiday files and moved to this location

option explicit
Const adOpenStatic = 3
Const adLockOptimistic = 3

dim aDate, sFilename, wso, sTime, sDate, fso, sMonth, sDay, sYear, Y, i, strWavFile, objShell,sMon,sFri,sResult,result,DbCon,filedate,offSet,newDOM,DOM
Set fso = CreateObject("Scripting.FileSystemObject")

' Build File Extension
sTime = Time
sTime = int(left(sTime,2))
sMon = day(now-1)
sFri = day(now-4)

'Connection Information
Private Sub OpenDbCon ()
    on error resume next
    	set DbCon = createobject("ADODB.Connection")
    	DbCon.Open "Provider=SQLOLEDB;Data Source=sqlsvr1;" & _
	       "Trusted_Connection=No;Initial Catalog=smf4;" & _
               "User ID=sa;Password=;"
    	if err.number <> 0 Then
    		AppendLog("OpenDbCon: " & Err.Number & " " & Err.Description)
    		GetStub 'generate stub on error	
    	end if
    on error goto 0
End Sub

Private Sub GetHols ()
    
    Dim resultset,result 
    
    on error resume next
    	set resultset = CreateObject("ADODB.Recordset")             
    
    	resultset.Open "select CONVERT(VARCHAR(25),max(FeedDate),121) from tbl_OPSLOG where CONVERT(VARCHAR(25),FeedDate,121) like '%18:00:00.000'", _
            DbCon, adOpenStatic, adLockOptimistic   
    
    	If resultset.EOF Then 
        	Wscript.Echo "CNT!, there is a problem with SQLSVR1, Check you can connect to the DB manually"
    	Else
    		result = resultset.Fields(0).Value            	      
    		'result1 = resultset.Fields(1).Value 
    	
    		'Wscript.Echo "Last Loaded: " &  result
    		'Wscript.Echo "result1: " &  result1
    	End If        
    sResult = result

    	resultset.close  
    if err.number <> 0 Then
       	AppendLog("GetSqlDateTime: " & Err.Number & " " & Err.Description)
       	GetStub 'generate stub on error
    end if
    on error goto 0
    
End Sub

Private Sub GetDOM()
    
    Dim resultset,result, DOM

    on error resume next
    	set resultset = CreateObject("ADODB.Recordset")             
    
    	resultset.Open "select datePart(d,FeedDate) as dom from tbl_OPSLOG where CONVERT(VARCHAR(25),FeedDate,121) = '" & sResult & "'", _
            DbCon, adOpenStatic, adLockOptimistic   
    
    	If resultset.EOF Then 
		Wscript.Echo "CNT!, there is a problem with SQLSVR1, Check you can connect to the DB manually"
    	Else
		DOM = resultset.Fields(0).Value            	      
    		'Wscript.Echo "DOM: " &  DOM
    		
    	End If       
    	
    newDOM = DOM
    	resultset.close  
    
    if err.number <> 0 Then
       	AppendLog("GetSqlDateTime: " & Err.Number & " " & Err.Description)
       	GetStub 'generate stub on error
    end if
    on error goto 0
    

    
End Sub

OpenDbCon()
GetHols()
GetDOM()

if sTime >= 12 and sTime <= 19  then 
	sFilename = "IDC_1_TAB.zip"
	getDate(0)
	getFiles()
	'loadData()
else	
	sFilename = "FDC_1_TAB.zip"

	offSet=day(Now)-newDOM
		'Wscript.Echo "New DOM: " &  newDOM
		'Wscript.Echo "Offset: " &  offSet
	
	Do While (offSet>0)
	
		getdate(offSet)
    		'Wscript.Echo "Offset Loop: " &  offSet
		getFiles()
	
		offSet = offSet -1
		
	loop
	
	
	'wScript.echo datepart("w", now)
end if	
	

sub getDate(i)
	'Set fso = CreateObject("Scripting.FileSystemObject")



	if Len(Month(Now))=1 then 
		sMonth="0" & month(now -i)
	else
		sMonth=month(now -i)
	end if

	if Len(Day(now))=1 then 
		sDay = "0" & day(Now -i)
	else
		sDay = day(Now -i)
	end if

	sYear = Year(Now -i)

	' Build filename
	sDate = sYear & sMonth & sDay
	aDate = sDate
'wScript.echo sDate	
end sub

sub getFiles()

'Wscript.Sleep 1*1000

set wso = Wscript.CreateObject("Wscript.Shell")
Y = wso.run("o:\auto\apps\LSESMF\LSESMFDownloader.exe -host 194.169.1.31 -user lsestaging\sedolprod_edi -pass sEp3t3mB3r -initdir 'SEDOL\Changes\Module 1\Daily' -date " + aDate + " -local 'O:/datafeed/smf/smffeed/' -endswith " + sFilename + "", 1, TRUE)
'wScript.echo aDate & sFilename
end sub



'IMPORTANT - IP is hardcoded in the exe, if LSE change their ip Cornish will need to change the jar and Himadri will need to recompile the exe.
