option explicit


'**************************************************************************
' 		 GENERIC COPYRENAMEFILE.VBS
' 	
' Script for copying files from one directory to another
'
' Arguments in order (0) Source Path
'		     (1) Destination Path
'		     (2) Source file Name
'		     (3) Destination file Name
'**************************************************************************

'***** MAIN *****

'***** VARIABLES *****

	dim fso, WshShell, sPath, dPath, sfName, sext, dfName, numArgs, sMonth, sDay, sYear, sCustomName


'***** PREPERATION *****


	numArgs = WScript.Arguments.Count

	sPath = WScript.Arguments.Item(0)
	sfName = WScript.Arguments.Item(1)
	sext= WScript.Arguments.Item(2)
	dPath = WScript.Arguments.Item(3)
	dfName = WScript.Arguments.Item(4)
	

	Set WshShell = WScript.CreateObject("WScript.Shell")
	Set fso = CreateObject("Scripting.FileSystemObject")


'***** PROCESSING *****

'-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = mid(Year(Now),1,4)
  
  '-- Create the Custom filename without a extension  
 
    sCustomName = sYear & sMonth & sDay 


wscript.echo sPath & sfName & sCustomName & sext	
	fso.CopyFile sPath & sfName & sCustomName & sext,  dPath & dfName
	
	
'***** END MAIN *****