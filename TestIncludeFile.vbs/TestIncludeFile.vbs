Option Explicit

Dim fso: set fso = CreateObject("Scripting.FileSystemObject")

' Includes a file in the global namespace of the current script.
' The file can contain any VBScript source code.
' The path of the file name must be specified relative to the
' directory of the main script file.
Private Sub IncludeFile (ByVal RelativeFileName)
   Dim ScriptDir: ScriptDir = fso.GetParentFolderName(WScript.ScriptFullName)
   Dim FileName: FileName = fso.BuildPath(ScriptDir,RelativeFileName)
   IncludeFileAbs FileName
   End Sub

' Includes a file in the global namespace of the current script.
' The file can contain any VBScript source code.
' The path of the file name must be specified absolute (or
' relative to the current directory).
Private Sub IncludeFileAbs (ByVal FileName)
   Const ForReading = 1
   Dim f: set f = fso.OpenTextFile(FileName,ForReading)
   Dim s: s = f.ReadAll()
   ExecuteGlobal s
End Sub

' Includes the configuration file.
' The configiguration file has the name of the main script
' with the extension ".config".
Private Sub ReadConfigFile
   Dim ConfigFileName
   ConfigFileName = fso.GetBaseName(WScript.ScriptName) & ".config"   
   IncludeFile ConfigFileName
   Dim ConfigFileName1
   ConfigFileName1 = "Test1.config"   
   IncludeFile ConfigFileName1
End Sub

ReadConfigFile

WScript.Echo "ConfigParm1=" & ConfigParm1
WScript.Echo "ConfigParm2=" & ConfigParm2
WScript.Echo "ConfigParm3=" & ConfigParm3
WScript.Echo "ConfigParm4=" & ConfigParm4

Sub1

Sub2
