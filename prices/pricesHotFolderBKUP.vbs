
Option explicit


Dim objWMIService, colMonitoredEvents, objLatestEvent

Dim sPath, sComputer, sDrive, sFolders, sfileName, sfileDate

'----------------------------------------------------------------------------

sPath = "\\.\Y$\FeedProc\P04\LivePrice"
'sPath = "\\.\O$\Nate\price-vms"

sComputer = split(sPath,"\")(2)
sDrive = split(sPath,"\")(3)
sDrive = REPLACE(sDrive, "$", ":")
sFolders = split(sPath,"$")(1)
sFolders = REPLACE(sFolders, "\", "\\") & "\\"

sfileDate = dateStamp( 0 )

sfileName = Array( "US_USCOMP_" & sfileDate, "US_USCOMP_UC" & Right( sfileDate, 6) )

'----------------------------------------------------------------------------

'WScript.Echo sDrive
'WScript.Echo sFolders
'WScript.Echo sfileName

'----------------------------------------------------------------------------

MonitorPrices()

'pushFileUP( "SE_XSTO_20180621" )
'pushFileUP( "SE_XSTO_IX180621" )
'pushFileUP( "SE_XSTO_FI180621" )

'clientFileExist()

'----------------------------------------------------------------------------

Function MonitorPrices()
 	
 	Dim clientFiles
 	
 	Set objWMIService = GetObject("winmgmts:\\" & sComputer & "\root\cimv2")
	
	Set colMonitoredEvents = objWMIService.ExecNotificationQuery _
		( "SELECT * FROM __InstanceOperationEvent " _
			& "WITHIN 1 WHERE Targetinstance ISA 'CIM_DataFile' " _ 
			& "AND TargetInstance.Drive='" & sDrive & "' " _ 
			& "AND TargetInstance.Path='" & sFolders & "'" )
	 
	echoMsg ( "Begin Monitoring for a Folder Change Event..." )
	
	Do
		
		Set objLatestEvent = colMonitoredEvents.NextEvent
		
		Select Case objLatestEvent.Path_.Class
	 
			Case "__InstanceCreationEvent"
				
				echoMsg ( objLatestEvent.TargetInstance.FileName & " was created"  )
				
				clientFiles =  objLatestEvent.TargetInstance.FileName 				
				
				pushFileUP( clientFiles )
				
				'quitMonitoring( "N" )
				
			Case "__InstanceDeletionEvent"
				
				'echoMsg ( objLatestEvent.TargetInstance.FileName & " was deleted"  )

				'clientFiles =  objLatestEvent.TargetInstance.FileName 				
								
				'pushFileUP( clientFiles )
				
		End Select
	Loop
	 
	Set objWMIService = Nothing
	Set colMonitoredEvents = Nothing
	Set objLatestEvent = Nothing

End Function  

'----------------------------------------------------------------------------

Function pushFileUP( poFile )
	
	Dim Y_dir, cntryCD, exChg, folderType, fileName
	
	fileName = poFile
	
	poFile = Split( poFile, "_")
	
	'echoMsg ( poFile(0) & " " & poFile(1) & " " & poFile(2) )
	
	Y_dir =  poFile(0)& "\" 
	
	cntryCD = poFile(0)
	
	exChg = poFile(1)
	
	folderType = Left( poFile(2), 2 )
	
	Select Case folderType
		
		Case "IX"
		
			'echoMsg ( poFile(0) & " " & poFile(1) & " " & folderType )
			
			Y_dir = Y_dir & "Indices\"
			
		Case "UX"
			
			'echoMsg ( poFile(0) & " " & poFile(1) & " " & folderType )
			
			Y_dir = Y_dir & "Indices\"
			
		Case "FI"

			'echoMsg ( poFile(0) & " " & poFile(1) & " " & folderType )		
		
			Y_dir = Y_dir & "Bond\"
		
		Case "UF"
			
			'echoMsg ( poFile(0) & " " & poFile(1) & " " & folderType )
			
			Y_dir = Y_dir & "Bond\"
		
		Case "MF"
			
			Y_dir = Y_dir & "Fund\"
			
		Case "UC"	
		
			'echoMsg ( poFile(0) & " " & poFile(1) & " " & folderType )
					
		Case Else
		
			'echoMsg ( poFile(0) & " " & poFile(1) & " " & folderType )
	End Select
	
	
	Y_dir = Y_dir & fileName & ".zip"
	
	
	echoMsg ( Y_dir )
	
	AWS_Copy_US( Y_dir )
	
	
End Function

'----------------------------------------------------------------------------

Function AWS_Copy_US( Y_dir )
	
	Dim AWS_US, AWS_Local, fso, chkDir, tmp
	
	Set fso = CreateObject("Scripting.FileSystemObject")  
	
	AWS_US = "\\10.200.12.142\d$\INETPUB\FTPROOT\Prices\P04\" & Y_dir
	AWS_Local = "Y:\FeedArch\P04\" & Y_dir
	
	'MsgBox AWS_US
	
	echoMsg ( AWS_Local )
	
	echoMsg ( AWS_US )
	
	chkDir = Split(Y_dir, "\")
	
	tmp =  chkDir ( UBound( chkDir ) )
	
	echoMsg ( tmp )
	
	
	chkDir = Replace( AWS_US, tmp, "" )
	
	echoMsg ( chkDir  )
	If fso.FolderExists( chkDir ) Then 
	
		If fso.FileExists(AWS_Local) Then
		
			'fso.CopyFile AWS_Local, AWS_US
			
			If fso.FileExists(AWS_US) Then
				
			Else
			
				fso.CopyFile AWS_Local, AWS_US
				
			End If
			
		Else
			'msgbox AWS_US & "|-|-|" & AWS_Local
			'fso.CopyFile AWS_Local, AWS_US
		End If
		
	End If 
End Function 

'----------------------------------------------------------------------------

Function dateStamp( dateBack )
    
    Dim t 
    
    t = Now - dateBack
    
    dateStamp = Year(t) & _
			    Right("0" & Month(t),2) & _
			    Right("0" & Day(t),2) 
    
    
'    dateStamp = Year(t) & "-" & _
'    Right("0" & Month(t),2)  & "-" & _
'    Right("0" & Day(t),2)  
    
End Function

'----------------------------------------------------------------------------

Function dateTimeStamp()
    
    Dim t 
    t = Now
    dateTimeStamp = Year(t) & "-" & _
    Right("0" & Month(t),2)  & "-" & _
    Right("0" & Day(t),2)  & "_" & _  
    Right("0" & Hour(t),2) & _
    Right("0" & Minute(t),2) '  
    
    
'    dateStamp = Year(t) & "-" & _
'    Right("0" & Month(t),2)  & "-" & _
'    Right("0" & Day(t),2)  
    
End Function

'----------------------------------------------------------------------------

Function quitMonitoring( vValue )
	
	Dim sTime, logDateTime
	
	logDateTime = dateTimeStamp()
	
	sTime = Hour(time)
	
'	If sTime > 10 Then 
		
'		echoMsg ( " - Monitoring program has ended! condition has been met"  )
	
'		WScript.Quit
		
'	ElseIf	vValue = "Y" Then 
	
'		echoMsg ( " - Monitoring program has ended! - the file should be there "  )
	
'		WScript.Quit
		
'	Else 
		'do nothing 
		
'	End If 	


	If vValue = "Y" Then 
	
		echoMsg ( " - Monitoring program has ended! - the file should be there "  )
	
		WScript.Quit
		
	Else 
		'do nothing 
		
	End If 
	
End Function 


'----------------------------------------------------------------------------

Function echoMsg( vMsg )
	
	createLog( vMsg )
	WScript.Echo vbCrlf & Now & vbTab & vMsg & vbCrlf
	
End Function 

'----------------------------------------------------------------------------

Function createLog( vMsg )
	
	Dim sLog, fileDate, LogFile, Dest, contents, logDateTime
	Dim objFSO, objFolder, objFile, objTextFile, newFileDate
	
	'WScript.Echo vbCrlf & Now & vbTab & vMsg & vbCrlf
	
	sLog = "Prices_newBox"
	
	'fileDate = dateStamp( 0 )
	
	
	newFileDate = dateStamp( 0 )
	
	logDateTime = dateTimeStamp()
	
	LogFile = "O:\AUTO\logs\" & sLog & "\" & fileDate & "_" & sLog & ".html"
	
	Dest = "O:\AUTO\logs\" & sLog & "\"

	contents ="<p><span class=note> <b>Last Files </b> " & logDateTime & " | " & vMsg & "</span></p>"
			
		' Create the File System Object
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		
		' Check that the WriteDirectory folder exists
		If objFSO.FolderExists(Dest) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		 ' Create the WriteDirectory folder if folder does not exist
		   Set objFolder = objFSO.CreateFolder(Dest)
		   'WScript.Echo "Just created " & Dest
		End If
		
		'msgbox "LogFile Test: " & LogFile
		If objFSO.FileExists(LogFile) Then
		   Set objFolder = objFSO.GetFolder(Dest)
		Else
		   Set objFile = objFSO.CreateTextFile(LogFile)
		   'Wscript.Echo "Just created " & LogFile
		End If
		
		set objFile = nothing
		set objFolder = nothing
		' OpenTextFile Method needs a Const value
		' ForAppending = 8 ForReading = 1, ForWriting = 2
		Const ForAppending = 8		
		
		Set objTextFile = objFSO.OpenTextFile _
		(LogFile, ForAppending, True)
		
		
		' Writes WriteFile every time you run this VBScript
		objTextFile.WriteLine(contents)
		objTextFile.Close
		
End Function 

'----------------------------------------------------------------------------

Function write2Log

End Function 

'----------------------------------------------------------------------------