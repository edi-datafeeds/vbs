'********************************************************************
'					
'					DATABASE IMPORTER
'
' Author Scott Kitchingham
'
' Script for importing tab delimited files into mssql and mysql
' Arguments in order (0) AccID (Client Acount id Number)
'					 (1) Pftype (Isin, Uscode etc)                    
'
'********************************************************************

Option Explicit

'**** VARIABLES ****

Dim DbServer, DbName, dbconn, resultset

Dim fso, ts, s, textfile1, oFSO_file, fname, objFS2, WshShell, objFSO, oFS1, oFS1_File, oFSO, oFS2
Dim oFSO_Fromfolder

Dim myquery, myapppath, MyArray

Dim AccID, Pftype

Dim Loadfile, Logfile, CONF, SVR

Dim Runpath, ReportPath, SourceDrive, TargetDrive

Dim acttime, startdate, accountname, limitdown, limitup, lastloadmessage, loadfiledate
Dim MatchedCode, totalct, insertct, japdate

Dim noloadflag

Dim loadfilect, blankrowct, badrowct, updatect, deletedct

Const ForReading = 1, ForWriting = 2, ForAppending = 8, adOpenStatic = 3, adLockOptimistic = 3

'** PREPARATION **

'Load Argument Data
AccID = WScript.Arguments.Item(0)
Pftype = WScript.Arguments.Item(1)
SVR = WScript.Arguments.Item(2)
Loadfile = pftype & ".txt"
Set oFSO = CreateObject("scripting.filesystemobject")

'** Hardcoded stuff
Runpath=Replace(WScript.ScriptFullName,WScript.ScriptName,"")
Loadfile="C:\upload\acc\" &AccID &"\port\" &Loadfile
'MsgBox loadfile
ReportPath="\upload\acc\" &AccID
SourceDrive="C:"
TargetDrive="N:"
CONF="O:\Auto\Configs\DbServers.cfg"

japdate=Year(Now) &"-" &Mid("0" &Month(now),Len(Month(Now)),2) &"-" &Mid("0" &day(now),Len(day(Now)),2)

'** check not run already today - if it has, quit task
If oFSO.FileExists("C:\upload\acc\" &AccID &"\" &pftype &"_"  &japdate &"_Report.txt") Then
  WScript.Quit
End If
If oFSO.FileExists("C:\upload\acc\" &AccID &"\" &pftype &"_"  &japdate &"_Noload.txt") Then
  WScript.Quit
End If

Set oFSO_FromFolder = oFSO.GetFolder("C:" &ReportPath)
Archive_Old_Reports oFSO_FromFolder
  
'** Database Connections
Call OpenDbCon(SVR, CONF)
Set resultset = CreateObject("ADODB.Recordset")

'**** MAIN ****

'** clear valid row counter
loadfilect=0
Blankrowct=0
badrowct=0

'** Does account exist?
resultset.open "select accountname, limitdown," &_
               " limitup, lastloadmessage, convert(varchar(10),loadfiledate,111), insertct, totalct from client.dbo.pfacclog" &_
               " where accid =" &AccID &" and  pftype = '" &pftype &"'" _ 
               , dbconn, adOpenStatic, adLockOptimistic

'** No, so create
If resultset.EOF Then
  dbconn.Execute "insert into client.dbo.pfacclog(accid, pftype, acttime, actflag, startdate, limitup, limitdown) values (" _
                 &AccID &", '" &pftype &"', getdate(), 'I', getdate(), 0, 0)"
  limitdown=0
  limitup=0
  loadfiledate = japdate
  totalct=0
  insertct=0
Else
  '** Yes, get needed values
  accountname=resultset.Fields(0).value
  limitdown=resultset.Fields(1).value
  limitup=resultset.Fields(2).Value
  lastloadmessage=resultset.Fields(3).Value
  loadfiledate=resultset.Fields(4).Value
  insertct=resultset.Fields(5).Value
  totalct=resultset.Fields(6).Value
End if  
resultset.Close

If oFSO.FileExists(loadfile) Then
  Set oFS2 = oFSO.GetFile(loadfile)
  '** possible new pf, load in <pftype>_<accid> table, drop and create
  If (Pftype="issid" Or Pftype="secid") Then  
    dbconn.Execute "use client if exists (select * from sysobjects where name = '" &Pftype &"_" &accid &"')" &_
                   " drop table client.dbo." &Pftype &"_" &accid &_
                   " CREATE TABLE client.dbo." &Pftype &"_" &accid &" (mycode int NOT NULL, PRIMARY KEY (mycode))"

  Else
    dbconn.Execute "use client if exists (select * from sysobjects where name = '" &Pftype &"_" &accid &"')" &_
                   " drop table client.dbo." &Pftype &"_" &accid &_
                   " CREATE TABLE client.dbo." &Pftype &"_" &accid &" (mycode varchar(20) NOT NULL, PRIMARY KEY (mycode))"

  End if
  '** loop to load file into appropriate temp file
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set ts = fso.OpenTextFile(loadfile, ForReading)
  Do
    s = Replace(Replace(Replace(Replace(ts.ReadLine,Chr(34),""),Chr(39),"")," ",""),Chr(13),"")
    'MsgBox len(s)
    If s = "" Then
      blankrowct=blankrowct+1
    ElseIf (Pftype="issid" Or Pftype="secid") And Not IsNumeric(s) Or s = "0" Then  
       badrowct=badrowct+1
    ElseIf Pftype="isin" And Not len(s)=12 Then  
       badrowct=badrowct+1
    ElseIf Pftype="uscode" And Not len(s)=9 Then  
       badrowct=badrowct+1
    ElseIf Pftype="sedol" And Not len(s)=7 Then  
       badrowct=badrowct+1
    Else
      If (Pftype="issid" Or Pftype="secid") Then  
        loadfilect=loadfilect+1
        dbconn.execute "insert into client.dbo." &Pftype &"_" &AccID &" select '" & s &_
                   "' where " & s &" not in (select mycode from client.dbo." &Pftype &"_" &AccID &")"
      Else                   
        loadfilect=loadfilect+1
        dbconn.execute "insert into client.dbo." &Pftype &"_" &AccID &" select '" & s &_
                       "' where '" & s &"' not in (select mycode from client.dbo." &Pftype &"_" &AccID &")"
      End If
    End If
  Loop while NOT ts.AtEndOfStream
  ts.Close
Else
  noloadflag="T"
  lastloadmessage = "File not found: " &Pftype &".txt"
  Call update_pfacclog
  Call write_pf_stats
  Call clearobjects
  WScript.Quit
End If

If loadfilect=0 Then
  lastloadmessage = "Client portfolio has zero records: "  &Pftype &".txt"
  noloadflag="T"
  Call update_pfacclog
  Call write_pf_stats
  Call clearobjects
  WScript.Quit
End if


' compare totct with count id join between temp and master pf (doesn't include actflag<>'D')
resultset.Open "select count(code) as MatchedCode from client.dbo.pf" &Pftype &_
             " inner join client.dbo." &Pftype &"_" &AccID &" on client.dbo.pf" &Pftype &".code = client.dbo." &Pftype &"_" &AccID &".mycode" &_
             " and client.dbo.pf" &Pftype &".accid =" &AccID &_
             " where client.dbo.pf" &Pftype &".actflag<>'D'"
MatchedCode=resultset.Fields(0).Value
resultset.Close

If totalct=0 Then
  noloadflag="F"
  lastloadmessage="Loaded OK - Firsttime load"
  '** query to load temp table into pf table
  If Pftype="issid" or pftype="secid" then
    dbconn.Execute "insert into pf" &Pftype &_
                   " select distinct " &AccID &", mycode, 'I', getdate(), null" &_
                   " from " &Pftype &"_" &Accid &_
                   " where mycode not in (select code from pf" &Pftype &" where accid =" &AccID &")"
  Else
    dbconn.Execute "insert into pf" &Pftype &_
                   " select distinct " &AccID &", mycode, 'I', getdate(), null" &_
                   " from " &Pftype &"_" &Accid &_
                   " where mycode<>'' and mycode not in (select code from pf" &Pftype &" where accid =" &AccID &")"
  End if                   
  resultset.Open "select count(code) from client.dbo.pf" &Pftype &_
                 " where client.dbo.pf" &Pftype &".actflag = 'I'" &_
                 " and client.dbo.pf" &Pftype &".accid =" &accid
  insertct=resultset.Fields(0).Value
  resultset.Close
ElseIf MatchedCode = totalct And MatchedCode = loadfilect And insertct = 0 Then
   '** log only - leave loadfiledate and counts alone
   noloadflag="F"
   lastloadmessage="Loaded OK - No changes"
ElseIf MatchedCode = totalct And MatchedCode = loadfilect And insertct > 0 Then
   '** log only - leave loadfiledate and counts alone
   noloadflag="F"
   lastloadmessage="Loaded OK - No changes"
   insertct=0
   dbconn.Execute "update client.dbo.pf" &Pftype &_
                  " set actflag='U'" &_
                  " where actflag='I' and accid =" &Accid
ElseIf outside_limits() Then
  '** log only - leave loadfiledate and counts alone
  noloadflag="T"
Else
  noloadflag="F"
  lastloadmessage="Loaded OK - With changes"
  loadfiledate=japdate
  'loadfiledate=Now()
  '** set yesterday's inserts, present today also to updates
  dbconn.Execute "update client.dbo.pf" &Pftype &_
                " set actflag='U'" &_
                " where actflag='I' and accid =" &AccID &_
                " and code in (select code from" &Pftype &"_" &AccID &")"
  '** set existing deleted records, present today to inserts
  dbconn.Execute "update client.dbo.pf" &Pftype &_
                " set actflag='I'" &_
                " where actflag='D' and accid =" &AccID &_
                " and code in (select mycode from client.dbo." &Pftype &"_" &AccID &")"
   '** set any non-deleted records to deleted if not present today
'   msgbox " update pf" &Pftype &_
  dbconn.Execute "update client.dbo.pf" &Pftype &_
                " set actflag='D', lastdeleted=getdate()" &_
                " where accid =" &AccID &" and actflag<>'D'" &_
                " and code not in (select mycode from client.dbo. " &Pftype &"_" &AccID &")"
   '** insert any new codes
  dbconn.Execute "insert into client.dbo.pf" &Pftype &_
                " select distinct " &AccID & ", mycode, 'I', getdate(), null" &_
                " from client.dbo." &Pftype &"_" &AccID &_
                " where mycode<>''" &_
                " and mycode not in (select code from client.dbo.pf" &Pftype &" where accid =" &AccID &")"
End If

'** update query for pfacclog
Call write_pf_stats
Call update_pfacclog
Call clearobjects

WScript.quit

' **** SUBROUTINES ****
Sub write_pf_stats()
resultset.Open "select count(code) from client.dbo.pf" &Pftype &_
               " where client.dbo.pf" &Pftype &".actflag = 'I'" &_
               " and client.dbo.pf" &Pftype &".accid =" &AccID, dbconn
insertct=resultset.Fields(0).Value
resultset.Close

resultset.open "select count(code) from client.dbo.pf" &Pftype &_
               " where client.dbo.pf" &Pftype &".actflag = 'U'" &_
               " and client.dbo.pf" &Pftype &".accid =" &AccID
updatect=resultset.Fields(0).Value
resultset.Close

resultset.Open "select count(code) from client.dbo.pf" &Pftype &_
               " where client.dbo.pf" &Pftype &".actflag = 'D'" &_
               " and client.dbo.pf" &Pftype &".accid =" &AccID &_
               " and client.dbo.pf" &Pftype &".lastdeleted > getdate()-0.5"
deletedct=resultset.Fields(0).Value
resultset.Close

totalct=insertct+updatect

If noloadflag="T" then
  Logfile="C:\upload\acc\" &AccID &"\" &pftype &"_"  &japdate &"_Noload.txt"
Else
  Logfile="C:\upload\acc\" &AccID &"\" &pftype &"_"  &japdate &"_Report.txt"
End if

'** Initialise Log
Set oFS1 = CreateObject("Scripting.FileSystemObject")
Set oFS1_File = oFS1.OpenTextFile(Logfile, ForWriting, True)

AppendLog "*************************************************" &vbCrLf
AppendLog "Client portfolio report for: " &japdate &vbCrLf
AppendLog "Client portfolio last changed on: " &Replace(Replace(loadfiledate,"'",""),"/","-") &vbCrLf
AppendLog lastloadmessage &vbCrLf
AppendLog "Number of new codes added: " &insertct &vbCrLf
AppendLog "Number of code reloaded: " &updatect &vbcrlf
AppendLog "Number of codes removed: "&deletedct &vbcrlf
AppendLog "Total codes contained in current portfoliO: " &totalct &vbcrlf
AppendLog "***************** End of Report *****************" &vbCrLf
oFS1_File.Close
End Sub

Function outside_limits()
If limitup=0 And limitdown=0 Then
  outside_limits=False
Else
  If loadfilect<totalct and loadfilect*100/totalct<limitdown then
    lastloadmessage = loadfilect*100/totalct &"% decrease exceeded current limit of " &limitdown
    outside_limits=True
  ElseIf loadfilect>totalct and (loadfilect-totalct)*100/totalct>limitup then
    lastloadmessage = (loadfilect-totalct)*100/totalct &"% increase exceeded current limit of " &limitup
    outside_limits=True
  Else
    outside_limits=False
  End If
End if
End Function


Sub update_pfacclog()
If loadfiledate  = "" Then
  loadfiledate = "null"
Else
  loadfiledate = "'" & loadfiledate &"'"
End if    
dbconn.Execute "update client.dbo.pfacclog" &_
             " set acttime = getdate(), actflag = 'U'" &_
             ", loadfiledate =" &loadfiledate &_ 
             ", insertct = " &Insertct &", totalct = " &Totalct &_
             ", blankrowct = " &blankrowct &", badrowct = " &badrowct &_
             ", noloadflag = '" &noloadflag &"', lastloadmessage = '" &lastloadmessage &_
             "' where accid = " &AccID &" and pftype = '" &pftype &"'"
End Sub

Sub AppendLog(ByVal MessageTxt)
oFS1_File.Write MessageTxt & vbCrLf
End Sub 


Sub clearobjects ()
Set dbconn = nothing
Set oFSO = nothing
Set fso = nothing
Set ts = nothing
Set resultset = nothing
End Sub


Sub Archive_Old_Reports(FromFolder)
DIM ReportFilelist, ReportFile, TargetFile

Set ReportFilelist = FromFolder.Files

If oFSO.FolderExists(replace(FromFolder.Path, SourceDrive, TargetDrive)) then
  '** do nothing
Else
  '** create target folder if not exist
  oFSO.CreateFolder(replace(FromFolder.Path, SourceDrive, TargetDrive))
End If

For Each ReportFile in ReportFilelist
  If InStr(ReportFile.path,Pftype) <> 0 Then 
    'MsgBox ReportFile.path
    TargetFile = replace(ReportFile.path, SourceDrive, TargetDrive)
    'MsgBox ReportFile 
    If oFSO.FileExists(TargetFile) Then
      oFSO.DeleteFile(TargetFile)
    End If
    oFSO.CopyFile ReportFile.path, TargetFile
    ReportFile.Delete
  End if
Next
End Sub


'******************************************************************
'*
'* Database Library Code
'*
'******************************************************************


Sub run_cmdline(p_DbServer, p_sql)
'** returns fully formed command for the specified dbtype

Dim fso, f1, ts, s, foundserver, dbconfigfile, MyArray, dbtype, sql_cmdline, tfile
Const ForReading = 1

Set fso = CreateObject("Scripting.FileSystemObject")
dbconfigfile = "O:\auto\configs\dbservers.cfg" 
dbtype = mid(DbServer,5,2)

If fso.FileExists(dbconfigfile)Then
else
  MsgBox "ERROR - DB config file not found :" &dbconfigfile
  WScript.Quit
End if 

Set ts = fso.OpenTextFile(dbconfigfile, ForReading)

foundserver=false
Do 
  s = ts.ReadLine
  MyArray = Split(s, vbTab)	
  if MyArray(0) = dbserver Then 			
    foundserver=True
    If MyArray(3)<>"" Then
      MyArray(3)=" -p " &myarray(3)
    Else
      MyArray(3)=" "  
    End If
    exit do
  end If
Loop while NOT ts.AtEndOfStream
ts.Close

If foundserver=true then
  If lcase(dbtype)="my" Then
    'test id file or literal q text
    If LCase(Mid(p_sql,Len(p_sql)-3,4))=".sql" Then
      'commandline using filepath and name of p_sql
      sql_cmdline = "O:\auto\apps\exe\mysql32.exe" & " -h " & MyArray(4) & " -u " & MyArray(2) & " " & MyArray(3) & " < " & p_sql
      'Call WriteCmndLine()
    Else  
      'commandline using literal p_sql
      sql_cmdline = "O:\auto\apps\exe\mysql32.exe" & " -h " & MyArray(4) & " -u " & MyArray(2) & " " & MyArray(3) & " -e " & """" & p_sql &""""
    End if 
  ElseIf lcase(dbtype)="ms" Then 
    'test id file or literal q text
    If LCase(Mid(p_sql,Len(p_sql)-3,4))=".sql" Then
      'commandline using filepath and name of p_sql
      sql_cmdline = "O:\auto\apps\exe\isql.exe" & " -S " & MyArray(4) & " -U " & MyArray(2) & MyArray(3) & " < " & p_sql
      'Call WriteCmndLine()
    Else  
      'commandline using literal p_sql
      sql_cmdline = "O:\auto\apps\exe\isql.exe" & " -S " & MyArray(4) & " -U " & MyArray(2) & MyArray(3) & " -Q " & """" & p_sql &""""
    End if 
  Else  
    MsgBox "ERROR - DB server not found :" &DbServer 
    WScript.Quit
  End If  
End If
   
Set WshShell = WScript.CreateObject("WScript.Shell")
Set objFSO = CreateObject("scripting.filesystemobject")
fname = myapppath &"LoadFile.bat"
If objFSO.FileExists(fname) Then
  objFSO.DeleteFile(fname)
End If
objFSO.CreateTextFile(fname), ForWriting
Set tfile = objFSO.OpenTextFile(fname, Forwriting)
tfile.Write(sql_cmdline)
tfile.Close
End Sub

Private Sub OpenDbCon (p_svr, p_conf)
Dim fso, f1, ts, s, uname, pword, prov, pcata, dsource
Const ForReading = 1
prov = "SQLOLEDB"
'** Get connection details
Set fso = CreateObject("Scripting.FileSystemObject")   
Set ts = fso.OpenTextFile(p_conf, ForReading)
Do 
  s = ts.ReadLine
  MyArray = Split(s, vbTab)
  If MyArray(0) = p_svr Then
    uname = MyArray(2)
    pword = MyArray(3)
    dsource = MyArray(4) 			
    Exit do
  End if		
Loop while NOT ts.AtEndOfStream
ts.Close
'** connect to database
On Error Resume Next
Set dbconn = CreateObject("ADODB.Connection")

dbconn.Open "Provider="&prov &";Data Source=" &dsource &";" &_
" Trusted_Connection=No;Initial Catalog=client;" &_
" User ID="&MyArray(2) &";Password=" &pword &";"
'dbconn.open "Provider=SQLOLEDB;Data Source=192.168.12.206;" &_
'" Trusted_Connection=No;Initial Catalog=portfolio;" &_
'" User ID=sa;Password=K376:lcnb;"
If Err.Number <> 0 Then
  MsgBox Err.Number & " " & Err.Description
End If
On Error Goto 0
'WScript.Quit
End Sub