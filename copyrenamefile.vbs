option explicit


'**************************************************************************
' 		 GENERIC COPYRENAMEFILE.VBS
' 	
' Script for copying files from one directory to another
'
' Arguments in order (0) Source Path
'		     (1) Destination Path
'		     (2) Source file Name
'		     (3) Destination file Name
'**************************************************************************

'***** MAIN *****

'***** VARIABLES *****

	dim fso, WshShell, sPath, dPath, sfName, dfName, numArgs


'***** PREPERATION *****


	numArgs = WScript.Arguments.Count

	sPath = WScript.Arguments.Item(0)
	sfName = WScript.Arguments.Item(1)
	dPath = WScript.Arguments.Item(2)
	dfName = WScript.Arguments.Item(3)
	

	Set WshShell = WScript.CreateObject("WScript.Shell")
	Set fso = CreateObject("Scripting.FileSystemObject")


'***** PROCESSING *****

	fso.CopyFile sPath & sfName,  dPath & dfName
	
	
'***** END MAIN *****