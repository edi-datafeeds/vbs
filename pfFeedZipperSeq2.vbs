'********************************************************************************************
'
'                                               pfFeedZipper
'
'********************************************************************************************

Option Explicit

'**** VARIABLES ****

Dim SourcePath, FileExt, Accid, SVR, rs, FileDate, Dbconn, WshShell, oFSO, oFolder, oFiles, oFile, CONF,  MyArray
Const ForReading = 1, ForWriting = 2, ForAppending = 8, adOpenStatic = 3, adLockOptimistic = 3

'**** PREPARATION ****

'Load Argument Data
SourcePath = WScript.Arguments.Item(0)
FileExt = WScript.Arguments.Item(1)
Accid = WScript.Arguments.Item(2)
SVR = WScript.Arguments.Item(3)

'Hardcoded Info
CONF="O:\Auto\Configs\DbServers.cfg"

'Database Connections
Call OpenDbCon(SVR, CONF)
Set rs = CreateObject("ADODB.Recordset")

'**** MAIN ****

'Get max FeedDate into variable
If rs.State = 1 Then rs.Close
rs.Open "select max(feeddate) from wca.dbo.tbl_opslog" &_
       " where seq = 2", Dbconn
FileDate = rs.Fields(0).Value
rs.Close

'Delete existing zip files in sourcefolder
Set oFSO = CreateObject("Scripting.FileSystemObject")
Set oFolder = oFSO.GetFolder(SourcePath)
Set oFiles = oFolder.Files

For Each oFile In oFiles
  If LCase(Right(CStr(oFile.Name),4))=".zip" Then
    oFile.Delete
  End If
Next 

'zip up entire contents of sourcepath
Set WshShell = WScript.CreateObject("Wscript.Shell")
WshShell.Run "O:\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a " &SourcePath &Mid(FileDate,7,4) &Mid(FileDate,4,2) &Mid(FileDate,1,2) &"_" &accid &" " &SourcePath &FileExt, 1, True

'**** SUBROUTINES ****

Sub OpenDbCon (SVR, CONF)
Dim fso, f1, ts, s, uname, pword, prov, pcata, dsource
Const ForReading = 1
prov = "SQLOLEDB"
'** Get connection details
Set fso = CreateObject("Scripting.FileSystemObject")   
Set ts = fso.OpenTextFile(CONF, ForReading)
Do 
  s = ts.ReadLine
  MyArray = Split(s, vbTab)
  If MyArray(0) = SVR Then
    uname = MyArray(2)
    pword = MyArray(3)
    dsource = MyArray(4) 			
    Exit do
  End if		
Loop while NOT ts.AtEndOfStream
ts.Close
'** connect to database
On Error Resume Next
Set dbconn = CreateObject("ADODB.Connection")
dbconn.open "Provider="&prov &";Data Source=" &dsource &";" &_
" Trusted_Connection=No;Initial Catalog=wca;" &_
" User ID="&MyArray(2) &";Password=" &pword &";"
If Err.Number <> 0 Then
  MsgBox Err.Number & " " & Err.Description
End If
On Error Goto 0
End Sub