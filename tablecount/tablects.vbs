'Attribute VB_Name = "Module1"
'**************************************************************************
'
'                 TABLE COUNTER
'
' Author Shah Syed
'
' Script for counting table records in given databases
'
' Arguments in order (1) Folder path for input files and output(log) file
'                    (2) Prefix of files to look in the folder
'
'**************************************************************************
Option Explicit

'***** VARIABLES *****
Dim auto, CrLF
'**arguments
Dim SourceFolder, Prefix

'** script control settings
auto = False
CrLF = Chr(13) & Chr(10)

'**File
Dim tfTbl, tfDB, tflog, output, write_flg

Const ForReading = 1, ForAppending = 8


'**FileSystemObject
Dim objFile

'**Counters
Dim i, idb, itbl

'**database variables
Dim Dbconn, rs

'**Load Argument data
SourceFolder = WScript.Arguments.Item(0)
Prefix = WScript.Arguments.Item(1)

' Arrays to store Database and Table names/values
Dim arrDB(), arrTbl()


'***** MAIN *****

'**Open source file of DB names
Set objFile = CreateObject("Scripting.FileSystemObject")
Set tfDB = objFile.OpenTextFile(SourceFolder & Prefix & "db.cfg", ForReading, True)

'**Read the DB file and create an array of DB names
i = 0
Do Until tfDB.AtEndOfStream
ReDim Preserve arrDB(i)
arrDB(i) = tfDB.ReadLine
i = i + 1
Loop
tfDB.close

'** check here if DB array is empty exit with message **

'**Open source file of DB names
Set tfTbl = objFile.OpenTextFile(SourceFolder & Prefix & "tbl.cfg", ForReading, True)

'** Read the table file and create an array of table names
i = 0
Do Until tfTbl.AtEndOfStream
    ReDim Preserve arrTbl(i)
    arrTbl(i) = tfTbl.ReadLine
    i = i + 1
Loop
tfTbl.Close

'** check here if Table array is empty exit with message **

'** create two dimensional array - no. of DB's row, no. of tables columns
'Dim arrdbTbl(szDB,szTbl )
Dim arrCur(), arrLast
output = Chr(9) & Join(arrTbl) & CrLF
write_flg = False
'** compare table counts
Set Dbconn = CreateObject("ADODB.Connection")
Set rs = CreateObject("ADODB.Recordset")

For idb = 0 To UBound(arrDB)

    Dbconn.Open "DSN=" & arrDB(idb)
	For itbl = 0 To UBound(arrTbl)
		rs.open "Select count(*) from " & arrTbl(itbl),Dbconn
        ReDim Preserve arrCur(itbl)
		arrCur(itbl) = rs.fields(0)
		rs.Close
    Next
    output = output & arrDB(idb) & Chr(9) & Join(arrCur, Chr(9)) & CrLF
    If IsEmpty(arrLast) Then
        arrLast = arrCur
    Else
       
	   If (discrepancy(arrCur, arrLast)) Then
            write_flg = True
	   End If
		arrLast = arrCur
	End If
	Erase arrCur
    Dbconn.Close
    
Next


If (write_flg) Then
    Call write_log(output)
	MsgBox ("Discrepancies found. See the log")
Else
    MsgBox ("No Discrepancies found")
End If

Function discrepancy(arrA, arrB)
Dim x, ret
    For x = 0 To UBound(arrA)
       ' MsgBox "Records in A:" & arrA(x) & " - Records in B:" & arrB(x)
		If(CInt(arrA(x)) <> CInt(arrB(x))) Then
            discrepancy = True
			Exit Function
		End If	
    Next
	discrepancy = False
End Function

Sub write_log(ByVal content)
Dim objF, tflog
   Set objF = CreateObject("Scripting.FileSystemObject")
   Set tflog = objF.OpenTextFile(SourceFolder & Prefix & "log.txt", 2, True)
   tflog.write "*********************************************************" & CrLF & "Log Created At: " & Now &  CrLF & CrLF & content &  CrLF & "*********************************************************"
   tflog.Close
   Set tflog = Nothing
   Set objF = Nothing
End Sub




















