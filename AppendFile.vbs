'**************************************************************************
' 		 GENERIC APPENDFILE.VBS
' 	
' Script for copying the contents from one file to another file
'
' Arguments in order (0) ReadDirectory = C:\filepath\
'		     (1) ReadFile = file.txt
'		     (2) WriteDirectory = C:\filepath2\
'		     (3) WriteFile = if filename in argument use it but if argument "usedate" construct a filename
                        ' from todays date
 '                   (4) Fileext = .txt etc  
'**************************************************************************

'***** MAIN *****

'***** VARIABLES *****

Option Explicit

'Declare variables
Dim objFSO, objFolder, objShell, objTextFile, objFile, ReadDirectory, numArgs, ReadFile, WriteDirectory
Dim WriteFile, strText, objReadFile, contents, usedate, sdate, ddate, fileext1, fileext2, sMonth, sDay, sYear

		
	'Checks to see if the correct number of paramters have been set
	if WScript.Arguments.Count < 4 Then
		wscript.echo "There are less than 4 arguments"
		
		WScript.Quit	
		
	End if
		
		
		
'***** PREPERATION *****


	
	'set 4 statuary arguments
	numArgs = WScript.Arguments.Count
	ReadDirectory = WScript.Arguments.Item(0)
	WriteDirectory = WScript.Arguments.Item(1)
	ReadFile = WScript.Arguments.Item(2)
	WriteFile = WScript.Arguments.Item(3)
	
	
	
	'assign variable arguments if they exist
	if wscript.arguments.count > 4 then
		fileext1 = WScript.Arguments.Item(4)	
	end if
	if wscript.arguments.count > 5 then
		fileext2 = WScript.Arguments.Item(5)
	end if
	
	'wscript.echo wscript.arguments.count
	

	
	
	'Use this paramter to get the date name of the file
		if WScript.Arguments.Item(2)="sdate" Then
		
		
		
		'-- Build custom output file name, ensuring that
		  '		both sMonth and sDay are always two digits
		  
		  if Len(Month(Now))=1 then 
		  	sMonth="0" & month(now)
		  else
		  	sMonth=month(now)
		  end if
		  
		  if Len(Day(now))=1 then 
		  	sDay = "0" & day(Now)
		  else
		  	sDay = day(Now)
		  end if
		
		  sYear = mid(Year(Now),1,4)
		  
		  '-- Create the Custom filename
		 
		    ReadFile = sYear & sMonth & sDay & fileext1
	
		
		
		End if


	
	'Use this paramter to get the date name of the file
	if WScript.Arguments.Item(3)="ddate" Then
	
	
	
	'-- Build custom output file name, ensuring that
	  '		both sMonth and sDay are always two digits
	  
	  if Len(Month(Now))=1 then 
	  	sMonth="0" & month(now)
	  else
	  	sMonth=month(now)
	  end if
	  
	  if Len(Day(now))=1 then 
	  	sDay = "0" & day(Now)
	  else
	  	sDay = day(Now)
	  end if
	
	  sYear = mid(Year(Now),1,4)
	  
	  '-- Create the Custom filename without a extension  
	 
	    WriteFile = sYear & sMonth & sDay & fileext2

	
	
	End if
	
	
		
' Create the File System Object
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objReadFile = objFSO.OpenTextFile(ReadDirectory & ReadFile)


' OpenTextFile Method needs a Const value
' ForAppending = 8 ForReading = 1, ForWriting = 2
Const ForReading = 1


'***** PROCESSING *****


'Read file contents
contents = objReadFile.ReadAll

'Close file
objReadFile.close

'Display results
'wscript.echo contents

'Cleanup objects
Set objFSO = Nothing
'Set objReadFile = Nothing


' Create the File System Object
Set objFSO = CreateObject("Scripting.FileSystemObject")

' Check that the WriteDirectory folder exists
If objFSO.FolderExists(WriteDirectory) Then
   Set objFolder = objFSO.GetFolder(WriteDirectory)
Else
 ' Create the WriteDirectory folder if folder does not exist
   Set objFolder = objFSO.CreateFolder(WriteDirectory)
   'WScript.Echo "Just created " & WriteDirectory
End If

If objFSO.FileExists(WriteDirectory & WriteFile) Then
   Set objFolder = objFSO.GetFolder(WriteDirectory)
Else
   Set objFile = objFSO.CreateTextFile(WriteDirectory & WriteFile)
   'Wscript.Echo "Just created " & WriteDirectory & WriteFile
End If





set objFile = nothing
set objFolder = nothing
' OpenTextFile Method needs a Const value
' ForAppending = 8 ForReading = 1, ForWriting = 2
Const ForAppending = 8



Set objTextFile = objFSO.OpenTextFile _
(WriteDirectory & WriteFile, ForAppending, True)


' Writes WriteFile every time you run this VBScript
objTextFile.WriteLine(contents)
objTextFile.Close


WScript.Quit

'***** END MAIN *****