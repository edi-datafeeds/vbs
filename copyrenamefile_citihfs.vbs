option explicit


'**************************************************************************
' 		 GENERIC COPYRENAMEFILE.VBS
' 	
' Script for copying files from one directory to another
'
' Arguments in order (0) Source Path
'		     (1) Destination Path
'		     (2) Source file Name
'		     (3) Destination file Name
'**************************************************************************

'***** MAIN *****

'***** VARIABLES *****

	dim fso, WshShell, sPath, dPath, sfName, sext, dfName, numArgs, sMonth, sDay, sYear, sCustomName


'***** PREPERATION *****


	numArgs = WScript.Arguments.Count

	sPath = WScript.Arguments.Item(0)
	sext= WScript.Arguments.Item(1)
	dPath = WScript.Arguments.Item(2)
	dfName = WScript.Arguments.Item(3)
	

	Set WshShell = WScript.CreateObject("WScript.Shell")
	Set fso = CreateObject("Scripting.FileSystemObject")


'***** PROCESSING *****


	if sext <> "*" then

	wscript.echo sPath & "*" & sext, dPath & " " & dfName
	
	fso.copyfile sPath & "*" & sext, dPath & " " & dfName
	
	end if
	

	
	'fso.CopyFile "O:\upload\acc\190\feed\*.txt", "H:\y.laifa\citihfs\"
		

	
	
'***** END MAIN *****