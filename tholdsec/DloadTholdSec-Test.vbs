' Created for Sanjeet 12/05/2014

Option Explicit 

Dim sDay, sMonth, lYear, sDate, sYear, TSK, sTime,contents, LogFile, Dest,URL
Dim objFSO,objReadFile,objFolder,objFile,objTextFile,Datafeed,TholdSec,D,MKT,rDay,rDate,dDate


'Set command line arguments
	Datafeed = "\\192.168.12.4\ops\Datafeed\TholdSec\raw\" & dDate & "_" & MKT & ".txt"
	MKT = WScript.Arguments(0)
	TSK = WScript.Arguments(1)
	D = WScript.Arguments(2)
	Call GetDate(D)
	
	Select Case MKT
		Case "NYSEth"
			'URL = "http://www1.nyse.com/threshold/" & MKT & sDate &".txt"
			msgbox "Download ALL MARKETS file from https://www.nyse.com/regulation/threshold-securities/ to " & Datafeed
			WScript.quit
		Case "NYSEAMEXth"
			'URL = "http://www1.nyse.com/threshold/" & MKT & sDate &".txt"
			msgbox "Download ALL MARKETS file from https://www.nyse.com/regulation/threshold-securities/ to " & Datafeed
			WScript.quit
		Case "NYSEARCAth"
			'URL = "http://www1.nyse.com/thresholdnysearca/" & MKT & sDate &".txt"
			msgbox "Download NYSEarch file from https://www.nyse.com/regulation/threshold-securities/"
			WScript.quit
		Case "nasdaqth"
			URL = "http://www.nasdaqtrader.com/dynamic/symdir/regsho/" & MKT & sDate &".txt"
		Case "FINRA"
			
			URL = "http://otce.finra.org/RegSHO/DownloadFileStream?fileId=259"
	End Select 		
	
' Create the File System Object
	Set objFSO = CreateObject("Scripting.FileSystemObject")

'Call functions
	Call Prod()
	Call Validate()


'Discover what today is
Sub GetDate(D)
		
	sDay = datepart("w", now)
	
	if sDay = 2 then
		D=3
		
	Else
		D=WScript.Arguments(2)
		
	end If
	
	if Len(Month(Now-D))=1 then 
		sMonth="0" & month(now -D)
	else
		sMonth=month(now -D)
	end if
	
	if Len(Day(Now-D))=1 then 
		sDay = "0" & day(Now -D)
		rDay = "0" & day(Now)
	Else
		sDay = day(Now -D)
		rDay = day(Now)
	
	end if
			
		lYear = mid(Year(Now),1,4)
		sYear = mid(Year(Now),3,4)
		sTime = (now)
		sDate = lYear & sMonth & sDay
		dDate = lYear & "-" & sMonth & "-" & sDay
		rDate = lYear & sMonth & rDay

End Sub 

'Discovery what Time it is

'Build Download parameters
	Sub Prod()
	
	
				' Check that the WriteDirectory folder exists
			If objFSO.FolderExists(Datafeed) Then
			   Set objFolder = objFSO.GetFolder(Datafeed)
			Else
			 ' Create the WriteDirectory folder if folder does not exist
			   Set objFolder = objFSO.CreateFolder(Datafeed)
			   'WScript.Echo "Just created " & Dest
			End If

	'http://www.nyse.com/threshold/NYSEth20140318.txt
		get_html _
		URL, _
		"\\192.168.12.4\ops\Datafeed\TholdSec\raw\" & dDate & "_" & MKT & ".txt"
		TholdSec = "\\192.168.12.4\ops\Datafeed\TholdSec\raw\" & dDate & "_" & MKT & ".txt"
	
	End Sub 

'Run Download Step
	Sub get_html (up_http, down_http)
	
		dim xmlhttp : set xmlhttp = createobject("msxml2.xmlhttp.3.0")
		xmlhttp.open "get", up_http, false
		xmlhttp.send
	
		dim fso : set fso = createobject ("scripting.filesystemobject")
	
		dim newfile : set newfile = fso.createtextfile(down_http, true)
		newfile.write (xmlhttp.responseText)
	
		newfile.close
	
		set newfile = nothing
		set xmlhttp = nothing
	End Sub

'Validate correct files downloaded
	Sub Validate()

		sTime = (now)
	
		if objFSO.FileExists(TholdSec) = False Then
			contents ="<p><span class=failed>" & sTime & " | <b>Failed to Download:</b> " & TholdSec & " </span></p><p><span class=failed><b>Please try again...</span></p>"
			call sAppend(contents)
		Else
			contents ="<p><span class=ok>" & sTime & " | <b>Downloaded </b> " & TholdSec & " Rates Feed Sucessfully</span></p>"
			call sAppend(contents)
		End If 
	
	
	End Sub 

'Append results To HTML log
	Sub sAppend(contents)
		LogFile = "O:\AUTO\logs\" & TSK & "\" & lYear & sMonth & rDay & "_" & TSK & ".html"
		Dest = "O:\AUTO\logs\" & TSK & "\"
	    	
					
			' Check that the WriteDirectory folder exists
			If objFSO.FolderExists(Dest) Then
			   Set objFolder = objFSO.GetFolder(Dest)
			Else
			 ' Create the WriteDirectory folder if folder does not exist
			   Set objFolder = objFSO.CreateFolder(Dest)
			   'WScript.Echo "Just created " & Dest
			End If
			
			'msgbox "LogFile Test: " & LogFile
			If objFSO.FileExists(LogFile) Then
			   Set objFolder = objFSO.GetFolder(Dest)
			Else
			   Set objFile = objFSO.CreateTextFile(LogFile)
			   'Wscript.Echo "Just created " & LogFile
			End If
			
			set objFile = nothing
			set objFolder = nothing
			' OpenTextFile Method needs a Const value
			' ForAppending = 8 ForReading = 1, ForWriting = 2
			Const ForAppending = 8		
			
			Set objTextFile = objFSO.OpenTextFile _
			(LogFile, ForAppending, True)
			
			
			' Writes WriteFile every time you run this VBScript
			objTextFile.WriteLine(contents)
			objTextFile.Close		
	end sub	

