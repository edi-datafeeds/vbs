' ****************************************************************
' GENERIC OPS_GET.VBS
'
'     Script for gettting Daily Datafeed and from BBS
' 
' Arguments in order (0) Number of input targets e.g. 2
'                    (1) Operations ID           e.g. CRE
'                    (2) BBS Download Directory  e.g. CAB
'                    (3) Input target Directory1 e.g. O:\DIARY\
'                    (4) Input target Directory1 e.g. U:\DIARY\
' ****************************************************************

  Dim wso,colArgs,CRLF

  CRLF = Chr(13) & Chr(10)

  Set colArgs = WScript.Arguments
  opsid = colArgs(1)
  bbs1path = "O:\MODEM\" & colArgs(2) & "\"
  If opsid <> "CW" Then
    inp1path = colArgs(3) & "INPUT\"
  Else
    inp1path = colArgs(3)
  End If

  If colArgs(0) = "2" Then
    inp2path = colArgs(4) & "INPUT\"
  End If

  
  
    Set wso = Wscript.CreateObject("Wscript.Shell")
    ' Unpack BBS1 datafeed
    Y = wso.Run (bbs1path & opsid & "_DAY -o " & inp1path, 1, TRUE)
    If colArgs(0) = "2" Then
      Y = wso.Run (bbs1path & opsid & "_DAY -o " & inp2path, 1, TRUE)
    
end if
 

' **************************************************************

 
' **************************************************************
