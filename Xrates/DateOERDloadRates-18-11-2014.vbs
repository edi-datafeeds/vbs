sub get_html (up_http, down_http)
dim xmlhttp : set xmlhttp = createobject("msxml2.xmlhttp.3.0")
xmlhttp.open "get", up_http, false
xmlhttp.send

dim fso : set fso = createobject ("scripting.filesystemobject")

dim newfile : set newfile = fso.createtextfile(down_http, true)
newfile.write (xmlhttp.responseText)

newfile.close

set newfile = nothing
set xmlhttp = nothing
end sub

Dim sDay, sMonth, lYear, sDate, sYear

If Len(Month(Now))=1 then 
	sMonth="0" & month(now)
else
	sMonth=month(now)
end if
	  
if Len(Day(now))=1 then 
	sDay = "0" & day(Now)
else
	sDay = day(Now)
end if

lYear = mid(Year(Now),1,4)
sYear = mid(Year(Now),3,4)
sTime = (now)
sDate = lYear & "-" & sMonth & "-" & sDay

get_html _
"http://openexchangerates.org/historical/2009-07-27.json?app_id=c1c1cf3b4216437f869e6f2b66f7a306", _
"o:\Datafeed\Xrates\OER_2009-07-27.csv"
get_html _
"http://openexchangerates.org/historical/2009-08-28.json?app_id=c1c1cf3b4216437f869e6f2b66f7a306", _
"o:\Datafeed\Xrates\OER_2009-08-28.csv"
get_html _
"http://openexchangerates.org/historical/2009-12-31.json?app_id=c1c1cf3b4216437f869e6f2b66f7a306", _
"o:\Datafeed\Xrates\OER_2009-12-31.csv"
get_html _
"http://openexchangerates.org/historical/2010-07-05.json?app_id=c1c1cf3b4216437f869e6f2b66f7a306", _
"o:\Datafeed\Xrates\OER_2010-07-05.csv"
get_html _
"http://openexchangerates.org/historical/2010-08-03.json?app_id=c1c1cf3b4216437f869e6f2b66f7a306", _
"o:\Datafeed\Xrates\OER_2010-08-03.csv"
get_html _
"http://openexchangerates.org/historical/2010-08-17.json?app_id=c1c1cf3b4216437f869e6f2b66f7a306", _
"o:\Datafeed\Xrates\OER_2010-08-17.csv"
get_html _
"http://openexchangerates.org/historical/2010-12-31.json?app_id=c1c1cf3b4216437f869e6f2b66f7a306", _
"o:\Datafeed\Xrates\OER_2010-12-31.csv"
get_html _
"http://openexchangerates.org/historical/2011-02-18.json?app_id=c1c1cf3b4216437f869e6f2b66f7a306", _
"o:\Datafeed\Xrates\OER_2011-02-18.csv"
get_html _
"http://openexchangerates.org/historical/2011-07-04.json?app_id=c1c1cf3b4216437f869e6f2b66f7a306", _
"o:\Datafeed\Xrates\OER_2011-07-04.csv"
get_html _
"http://openexchangerates.org/historical/2011-08-02.json?app_id=c1c1cf3b4216437f869e6f2b66f7a306", _
"o:\Datafeed\Xrates\OER_2011-08-02.csv"
get_html _
"http://openexchangerates.org/historical/2011-08-09.json?app_id=c1c1cf3b4216437f869e6f2b66f7a306", _
"o:\Datafeed\Xrates\OER_2011-08-09.csv"
get_html _
"http://openexchangerates.org/historical/2011-08-21.json?app_id=c1c1cf3b4216437f869e6f2b66f7a306", _
"o:\Datafeed\Xrates\OER_2011-08-21.csv"
get_html _
"http://openexchangerates.org/historical/2011-08-26.json?app_id=c1c1cf3b4216437f869e6f2b66f7a306", _
"o:\Datafeed\Xrates\OER_2011-08-26.csv"
get_html _
"http://openexchangerates.org/historical/2011-08-28.json?app_id=c1c1cf3b4216437f869e6f2b66f7a306", _
"o:\Datafeed\Xrates\OER_2011-08-28.csv"
get_html _
"http://openexchangerates.org/historical/2011-08-29.json?app_id=c1c1cf3b4216437f869e6f2b66f7a306", _
"o:\Datafeed\Xrates\OER_2011-08-29.csv"
get_html _
"http://openexchangerates.org/historical/2011-11-29.json?app_id=c1c1cf3b4216437f869e6f2b66f7a306", _
"o:\Datafeed\Xrates\OER_2011-11-29.csv"
get_html _
"http://openexchangerates.org/historical/2011-12-31.json?app_id=c1c1cf3b4216437f869e6f2b66f7a306", _
"o:\Datafeed\Xrates\OER_2011-12-31.csv"
get_html _
"http://openexchangerates.org/historical/2012-02-07.json?app_id=c1c1cf3b4216437f869e6f2b66f7a306", _
"o:\Datafeed\Xrates\OER_2012-02-07.csv"
get_html _
"http://openexchangerates.org/historical/2012-02-08.json?app_id=c1c1cf3b4216437f869e6f2b66f7a306", _
"o:\Datafeed\Xrates\OER_2012-02-08.csv"
get_html _
"http://openexchangerates.org/historical/2012-02-28.json?app_id=c1c1cf3b4216437f869e6f2b66f7a306", _
"o:\Datafeed\Xrates\OER_2012-02-28.csv"
