Option Explicit 

Dim sDay, sMonth, lYear, sDate, sYear, TSK, sTime, sInc, ECB, OER, contents, LogFile, Dest
Dim objFSO,objReadFile,objFolder,objFile,objTextFile
Dim OER_OLD, ECB_OLD

'Set command line arguments
	TSK = WScript.Arguments(0)
	
	
' Create the File System Object
	Set objFSO = CreateObject("Scripting.FileSystemObject")

'Call functions
	Call GetDate()
	Call GetInc()
	Call Prod()
	Call Validate()


'Discover what today is
	Sub GetDate()
		
		If Len(Month(Now))=1 then 
			sMonth="0" & month(now)
		else
			sMonth=month(now)
		end if
			  
		if Len(Day(now))=1 then 
			sDay = "0" & day(Now)
		else
			sDay = day(Now)
		end if
		
		lYear = mid(Year(Now),1,4)
		sYear = mid(Year(Now),3,4)
		sTime = (now)
		sDate = lYear & "-" & sMonth & "-" & sDay
	
	End Sub 

'Discovery what Time it is
	Sub GetInc()
	
		if sTime >= 00 and sTime <= 13  then 
			'sInc = "180000"
			sInc = "060000"
		else	
			'sInc = "060000"
			sInc = "180000"

			
		end If	
	
	End Sub 

'Build Download parameters
	Sub Prod()
	
		get_html _
		"http://openexchangerates.org/historical/" & sDate & ".json?app_id=c1c1cf3b4216437f869e6f2b66f7a306&base=GBP", _
		"o:\Datafeed\Xrates\GBP\OER_GBP_" & sDate & "_" & sInc & ".csv"
		OER = "o:\Datafeed\Xrates\GBP\OER_GBP_"  & sInc & "_" & sDate & ".csv"
	
	End Sub 

'Run Download Step
	Sub get_html (up_http, down_http)
	
		dim xmlhttp : set xmlhttp = createobject("msxml2.xmlhttp.3.0")
		xmlhttp.open "get", up_http, false
		xmlhttp.send
	
		dim fso : set fso = createobject ("scripting.filesystemobject")
	
		dim newfile : set newfile = fso.createtextfile(down_http, true)
		newfile.write (xmlhttp.responseText)
	
		newfile.close
	
		set newfile = nothing
		set xmlhttp = nothing
	End Sub

'Validate correct files downloaded
	Sub Validate()

		sTime = (now)
	
		if objFSO.FileExists(ECB_OLD) = False Then
			contents ="<p><span class=failed>" & sTime & " | <b>Failed to Download Feed:</b> " & ECB_OLD & " </span></p><p><span class=failed><b>Please try again...</span></p>"
			call sAppend(contents)
		Else
			contents ="<p><span class=ok>" & sTime & " | <b>Downloaded </b> " & ECB_OLD & " Rates Feed Sucessfully</span></p>"
			call sAppend(contents)
		End If 
	
		if objFSO.FileExists(OER_OLD) = False Then
			contents ="<p><span class=failed>" & sTime & " | <b>Failed to Download Feed:</b> " & OER_OLD & " </span></p><p><span class=failed><b>Please try again...</span></p>"
			call sAppend(contents)
		Else
			contents ="<p><span class=ok>" & sTime & " | <b>Downloaded </b> " & ECB & " Rates Feed Sucessfully</span></p>"
			call sAppend(contents)
		End If 
	
		if objFSO.FileExists(ECB) = False Then
			contents ="<p><span class=failed>" & sTime & " | <b>Failed to Download Feed:</b> " & ECB & " </span></p><p><span class=failed><b>Please try again...</span></p>"
			call sAppend(contents)
		Else
			contents ="<p><span class=ok>" & sTime & " | <b>Downloaded </b> " & ECB & " Rates Feed Sucessfully</span></p>"
			call sAppend(contents)
		End If 
	
		if objFSO.FileExists(OER) = False Then
			contents ="<p><span class=failed>" & sTime & " | <b>Failed to Download Feed:</b> " & OER & " </span></p><p><span class=failed><b>Please try again...</span></p>"
			call sAppend(contents)
		Else
			contents ="<p><span class=ok>" & sTime & " | <b>Downloaded </b> " & OER & " Rates Feed Sucessfully</span></p>"
			call sAppend(contents)
		End If 
	
		
	End Sub 

'Append results To HTML log
	Sub sAppend(contents)
		LogFile = "O:\AUTO\logs\" & TSK & "\" & lYear & sMonth & sDay & "_" & TSK & ".html"
		Dest = "O:\AUTO\logs\" & TSK & "\"
	    	
					
			' Check that the WriteDirectory folder exists
			If objFSO.FolderExists(Dest) Then
			   Set objFolder = objFSO.GetFolder(Dest)
			Else
			 ' Create the WriteDirectory folder if folder does not exist
			   Set objFolder = objFSO.CreateFolder(Dest)
			   'WScript.Echo "Just created " & Dest
			End If
			
			'msgbox "LogFile Test: " & LogFile
			If objFSO.FileExists(LogFile) Then
			   Set objFolder = objFSO.GetFolder(Dest)
			Else
			   Set objFile = objFSO.CreateTextFile(LogFile)
			   'Wscript.Echo "Just created " & LogFile
			End If
			
			set objFile = nothing
			set objFolder = nothing
			' OpenTextFile Method needs a Const value
			' ForAppending = 8 ForReading = 1, ForWriting = 2
			Const ForAppending = 8		
			
			Set objTextFile = objFSO.OpenTextFile _
			(LogFile, ForAppending, True)
			
			
			' Writes WriteFile every time you run this VBScript
			objTextFile.WriteLine(contents)
			objTextFile.Close		
	end sub	

