Set colArgs = WScript.Arguments

Sub get_html (up_http, down_http)
dim xmlhttp : set xmlhttp = createobject("msxml2.xmlhttp.3.0")
xmlhttp.open "get", up_http, false
xmlhttp.send

dim fso : set fso = createobject ("scripting.filesystemobject")

dim newfile : set newfile = fso.createtextfile(down_http, true)
newfile.write (xmlhttp.responseText)

newfile.close

set newfile = nothing
set xmlhttp = nothing
end sub

Dim sDay, sMonth, lYear, sDate, sYear

If Len(Month(Now))=1 then 
	sMonth="0" & month(now)
else
	sMonth=month(now)
end if
	  
if Len(Day(now))=1 then 
	sDay = "0" & day(Now)
else
	sDay = day(Now)
end if
If colArgs(0)="" then
	lYear = mid(Year(Now),1,4)
	sYear = mid(Year(Now),3,4)
	sTime = (now)
	sDate = sDay & sMonth & lYear
Else
	sDate = colArgs(0)
End if	
get_html _
"http://openexchangerates.org/historical/2001-10-09.json", _
"\\192.168.2.163\ops\Datafeed\Xrates\OER-" & sDate & ".csv"
