sub get_html (up_http, down_http)
dim xmlhttp : set xmlhttp = createobject("msxml2.xmlhttp.3.0")
xmlhttp.open "get", up_http, false
xmlhttp.send

dim fso : set fso = createobject ("scripting.filesystemobject")

dim newfile : set newfile = fso.createtextfile(down_http, true)
newfile.write (xmlhttp.responseText)

newfile.close

set newfile = nothing
set xmlhttp = nothing
end sub

Dim sDay, sMonth, lYear, sDate, sYear

If Len(Month(Now))=1 then 
	sMonth="0" & month(now)
else
	sMonth=month(now)
end if
	  
if Len(Day(now))=1 then 
	sDay = "0" & day(Now)
else
	sDay = day(Now)
end if

lYear = mid(Year(Now),1,4)
sYear = mid(Year(Now),3,4)
sTime = (now)
sDate = lYear & "-" & sMonth & "-" & sDay

sInc = "180000"


get_html _
"http://openexchangerates.org/historical/2015-01-18.json?app_id=c1c1cf3b4216437f869e6f2b66f7a306", _
"o:\Datafeed\Xrates\USD\OER_USD_2015-01-18_" & sInc & ".csv"
get_html _
"http://openexchangerates.org/historical/2015-01-04.json?app_id=c1c1cf3b4216437f869e6f2b66f7a306", _
"o:\Datafeed\Xrates\USD\OER_USD_2015-01-04_" & sInc & ".csv"

