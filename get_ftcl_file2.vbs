' ****************************************************************
' GET_FTCL_FILE.VBS
'
' ****************************************************************

' **** SUB MAIN ****
dim fso,f,L1,wso,colArgs,fldr,fcol,lastday,Seclast,CRLF,sSource,sOpspath,sTxtfile,sTargfile,sDatename,sArchpath,sTime,sExt

	CRLF = Chr(13) & Chr(10)

	Set colArgs = WScript.Arguments

	sFtclScript = colArgs(0)


	Set fso = CreateObject("Scripting.FileSystemObject")
	Set wso = Wscript.CreateObject("Wscript.Shell")


	if Len(Month(Now))=1 then 
		sMonth="0" & month(now)
	else
		sMonth=month(now)
	end if

	if Len(Day(now))=1 then 
		sDay = "0" & day(Now)
	else
		sDay = day(Now)
	end if

	sYear = Year(Now)

' Build filename

	sFilename = sYear & sMonth & sDay


' Build File Extension
	sTime = Time
	sTime = int(left(sTime,2))
	'msgbox sTime
	
	if sTime >= 12 and sTime <= 15  then 
		sExt = "2"
	elseif sTime >= 16 and  sTime <= 21  then 
		sExt = "3"
	else	
		sExt = "1"
		
	end if
	
 ' Testing
 	sTest = "Success"
	
'msgbox sExt,vbinformation

' Run Ftcl script to download the file

  'Y = wso.Run ("O:\utilities\SendFTP.exe UP www.exchange-data.com barrel FTP:K376:lcnb " & sOpspath & sCustomName & ".ZIP " & "Wso/Hist/" & sCustomName & ".ZIP")
  
 'msg = "File name = " & sFilename
'msgbox msg,vbinformation

  	if fso.fileexists("o:\datafeed\bahar\wcafeed\" & sFilename &".z0" & sExt) then 
	'msgbox "file exists",vbinformation
     	     
        else
             ' if exists delay for 4 mins
             Wscript.Sleep 240*1000    	
     	     'msgbox sTest
             ' download latest zip file
             Y = wso.Run ("o:\Apps\exe\ediftp -f " & sFtclScript &  " -s false -h www.exchange-data.com -u barrel -p FTP:K376:lcnb -MyLocalPath 'o:\datafeed\bahar\wcafeed\' -MyRemotePath '\Custom\Bahar\WCAfeed\' -MyFilename " & sFilename &".z0" & sExt, 1, TRUE)
     	     'msgbox sTest
             ' unzip to archive folder
             if fso.fileexists("o:\datafeed\bahar\wcafeed\" & sFilename &".z0" & sExt) then
		'msgbox "file exists",vbinformation
             else
                Wscript.Sleep 60*1000    	
                Y = wso.Run ("o:\Apps\exe\ediftp -f " & sFtclScript &  " -s false -h www.exchange-data.com -u barrel -p FTP:K376:lcnb -MyLocalPath 'o:\datafeed\bahar\wcafeed\' -MyRemotePath '\Custom\Bahar\WCAfeed\' -MyFilename " & sFilename &".z0" & sExt, 1, TRUE)
                if fso.fileexists("o:\datafeed\bahar\wcafeed\" & sFilename &".z0" & sExt) then
  	'msgbox "file exists",vbinformation
              	
			WScript.quit(0)
                else
                  	msgbox "Download Failed",vbinformation

		WScript.quit(0)
                end if
                              
           	'Y = wso.Run ("c:\progra~1\winzip\winzip32.exe -e -o -j o:\datafeed\bahar\wcafeed\" & sFilename & ".z0" & sExt & " N:\Bahar\Wcafeed\", 1, TRUE)
             end if
  	End If
  	

	call tidyup()

' **** END SUB MAIN ****

'**************************************************************
'	Remove all reference to any object created early in this
' script
'
Sub TidyUp()
	Set fso = Nothing
	Set wso = Nothing
End Sub


'**************************************************************
'	Custom routine for Client number xxx (valorinform)
'		1) Check that the day is a tuesday
'		2) Make a copy of the source file specified in command line
'		   and name it 'VALMMDD.TXT'
'		3) Add the file specified in 2 to a zip file of the
'			 same name 'VALMMDD.ZIP'
'
Sub valorInform()  
   
 	'-- Exit if Today is NOT Tuesday
 	if weekday(date)<>vbTuesday then
 		msg = "The specified client can only be run on a Tuesday" & CRLF & _
 					"and will not run today"
 		msgbox msg,vbinformation
 		call TidyUp
 		Exit Sub 		
 	end if
 			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  
  '-- Create the Custom filename without a extension  
  sCustomName = "VAL" & sMonth & sDay 

  '-- CopyFile the source file to the new custom name & .TXT
  fso.CopyFile sSource, sOpspath & sCustomName & ".TXT",true
	
  '- Zip copied file to Custom name & .ZIP  
  Y = wso.Run ("O:\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a " & sOpspath & sCustomName & ".ZIP " & sOpspath & sCustomName & ".TXT", 1, TRUE)
    
  ' Send File via SendFTP.exe (FTP)
  Y = wso.Run ("O:\utilities\SendFTP.exe UP valorftp.ft.com ediftp ed1ftp " & sOpspath & sCustomName & ".ZIP " & sCustomName & ".ZIP")
  
  if fso.fileexists(sOpspath & sCustomName & ".TXT") then
 	fso.DeleteFile(sOpspath & sCustomName & ".TXT")
  End If
  
end sub


'**************************************************************
'	Custom routine for Client number 2 (Factset)
'		1) Check that the day is a Tuesday
'		2) Make a copy of the source file specified in command line
'		   and name it 'DDMMYYYY.TXT'
'		3) Add the file specified in 2 to a zip file of the
'			 same name 'DDMMYYYY.ZIP'
'**************************************************************
Sub FactSet()  
  
 	'-- Exit if Today is NOT Tuesday
' 	if weekday(date)<>vbTuesday then
' 		msg = "The specified client can only be run on a Tuesday" & CRLF & _
' 					"and will not run today"
' 		msgbox msg,vbinformation
' 		call TidyUp
' 		Exit Sub 		
' 	end if
 			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  sOpspath = "o:\Datafeed\WSO\wsomain\"

  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  
  sYear = Year(Now)
  
  
  '-- Create the Custom filename without a extension  
  sCustomName = sDay &  sMonth & sYear
  
  '-- CopyFile the source file to the new custom name & .TXT
  msgbox sSource
  msgbox sCustomName & ".TXT"
  
  fso.CopyFile sSource, sOpspath & sCustomName & ".TXT",True
	
  '- Zip copied file to Custom name & .ZIP  
  Y = wso.Run ("O:\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a " & sOpspath & sCustomName & ".ZIP " & sOpspath & sCustomName & ".TXT", 1, TRUE)
  Y = wso.Run ("O:\utilities\SendFTP.exe UP www.exchange-data.com barrel FTP:K376:lcnb " & sOpspath & sCustomName & ".ZIP " & "Wso/Hist/" & sCustomName & ".ZIP")
  Y = wso.Run ("O:\utilities\SendFTP.exe UP 193.189.140.66 barrel FTP:K376:lcnb " & sOpspath & sCustomName & ".ZIP " & "/Wso/Hist/" & sCustomName & ".ZIP")
  

  ' Send File via W:\VBPackages\SendFile\sendfile.exe (EMain)
  'Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sCustomName & ".ZIP  <Address1;Address2> <Subject text>" 
  
	if fso.fileexists(sOpspath & sCustomName & ".TXT") then
		fso.DeleteFile(sOpspath & sCustomName & ".TXT")
 	End If
end sub


'**************************************************************
'	Custom routine for Client number 3 (Deutsche)
'		1) Add the file specified to a zip file of the
'			 same name 'ALYYMMDD.ZIP'
'
Sub Deutsche()  
   
 			
	sOpspath= "O:\udrsql\db\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = mid(Year(Now),3,2)
  
  '-- Create the Custom filename without a extension  
  sCustomName = "AL" & sYear & sMonth & sDay 

  '- Zip copied file to Custom name & .ZIP  
  Y = wso.Run ("O:\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a " & sOpspath & sCustomName & ".ZIP " & sOpspath & sCustomName & ".*", 1, TRUE)
  
  
  ' Send File via SendFTP.exe (FTP)
  'Y = wso.Run ("O:\utilities\SendFTP.exe UP 62.231.129.157 barrel FTP:K376:lcnb " & sOpspath & sCustomName & ".ZIP " & "UDR/AL/" & sCustomName & ".ZIP")
  Y = wso.Run ("O:\utilities\SendFTP.exe UP www.exchange-data.com barrel FTP:K376:lcnb " & sOpspath & sCustomName & ".ZIP " & "UDR/AL/" & sCustomName & ".ZIP")
  Y = wso.Run ("O:\utilities\SendFTP.exe UP 193.189.140.66 barrel FTP:K376:lcnb " & sOpspath & sCustomName & ".ZIP " & "/UDR/AL/" & sCustomName & ".ZIP")
  
  
	if fso.fileexists(sOpspath & sCustomName & ".DR1") then
 		fso.DeleteFile(sOpspath & sCustomName & ".DR1")
 	End If
 	if fso.fileexists(sOpspath & sCustomName & ".DR2") then
 		fso.DeleteFile(sOpspath & sCustomName & ".DR2")
 	End If
 	if fso.fileexists(sOpspath & sCustomName & ".DR3") then
 		fso.DeleteFile(sOpspath & sCustomName & ".DR3")
 	End If
end sub

'**************************************************************
'	Custom routine for Client number 2 (Factset)
'		1) Check that the day is a Tuesday
'		2) Make a copy of the source file specified in command line
'		   and name it 'DDMMYYYY.TXT'
'		3) Add the file specified in 2 to a zip file of the
'			 same name 'DDMMYYYY.ZIP'
'**************************************************************

Sub FactUdr()  
  
'**************************************************************
'	Custom routine for Client number 4 (fACTSET udr)
'		1) Add the file specified to a zip file of the
'			 sent uncompressed 'UDYYMMDD.txt'
'
   
  sOpspath= "O:\udrsql\OUTPUTS\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = mid(Year(Now),3,2)
  
  '-- Create the Custom filename without a extension  
  sCustomName = "UD" & sYear & sMonth & sDay 

    
  ' Send File via W:\VBPackages\SendFile\sendfile.exe (EMain)
  Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sOpspath & sCustomName & ".TXT vendor_mail@factset.com Daily UDR datafeed") 
  ' Send File via SendFTP.exe (FTP)
  'Y = wso.Run ("O:\utilities\SendFTP.exe UP gateway1.factset.com vendor_edi@datafeed-ct bounkert " & sOpspath & sCustomName & ".TXT /" & sCustomName & ".TXT")
  
'  if fso.fileexists(sOpspath & sCustomName & ".TXT") then
' 	fso.DeleteFile(sOpspath & sCustomName & ".TXT")
'  End If
end sub


Sub Xcitek()  
  
'**************************************************************
'	Custom routine for opsid number 5 (xcitek wca raw feed)
'		1) Rename file to date based file name from first line 
'          data file
'
   
  sOpspath= "O:\wcasql\xcitek\"			
  sTargfile = "MARSC11H"
  sTxtfile = "XCITEK.TXT"

  ' Grab File via SendFTP.exe (FTP)
  Y = wso.Run ("O:\utilities\SendFTP.exe DOWN ftp.xcitek.com exch0404 exch0404 " & sOpspath & sTargfile&".zip /XCITEK/CORPORATE/" & sTargfile&".ZIP",,true)

  if fso.fileexists(sOpspath & sTxtfile) then
    fso.DeleteFile(sOpspath & sTxtfile)
  End If  

  '- Unpack file
  'Y = wso.Run (sOpspath & sTargfile, 1, TRUE)
  '- UnZip copied file to Custom name & .ZIP  
'  Y = wso.Run ("pkunzip " & sOpspath & sTargfile & ".ZIP ", 1, TRUE)
  Y = wso.Run ("o:\scripts\unzipxci.bat", 1, TRUE)
    
  
  Set f = fso.OpenTextFile(sOpspath & sTxtfile, 1)
  L1= f.ReadLine
  
  sMonth = mid(L1,8,2)
  sday = mid(L1,11,2)
  sYear = mid(L1,14,4)
  
  Response = MsgBox(CRLF & "------------------------------------------------------------" & CRLF & CRLF & _
                         "      Packing fixed feed for "& sYear&sMonth&sDay & CRLF & CRLF & _
                         "------------------------------------------------------------" & CRLF, vbOkCancel)
  
  If Response = vbOK Then  

    sDatename="XF"&mid(sYear,3,2)&sMonth&sDay
    sArchpath=sOpspath&"archive\"&sYear&sMonth&"\"

   
    If not (fso.FolderExists(sArchpath)) Then
      fso.CreateFolder(sArchpath)
    End If
   
'    If (fso.FolderExists(sArchpath)) Then
      ' nothing to do
	'Else
'       fso.CreateFolder(sArchpath)
'    End If

    if fso.fileexists(sArchpath & sDatename & ".TXT") then
      fso.DeleteFile(sArchpath & sDatename&".TXT")
	End If  
    fso.CopyFile sOpspath & sTxtfile, sOpspath & sDatename & ".TXT", true
    fso.CopyFile sOpspath & sTxtfile, sArchpath & sDatename & ".TXT", true


    '- Zip copied file to Custom name & .ZIP  
    Y = wso.Run ("O:\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a " & sOpspath & sDatename & ".ZIP " & sOpspath & sDatename & ".TXT", 1, TRUE)

    ' Grab File via SendFTP.exe (FTP)
    'Y = wso.Run ("O:\utilities\SendFTP.exe UP 62.231.129.157 barrel FTP:K376:lcnb " & sOpspath & sDatename & ".zip /Custom/bahar/XCITEK/" & sDatename & ".ZIP",,true)
    Y = wso.Run ("O:\utilities\SendFTP.exe UP www.exchange-data.com barrel FTP:K376:lcnb " & sOpspath & sDatename & ".zip /Custom/bahar/XCITEK/" & sDatename & ".ZIP",,true)
    Y = wso.Run ("O:\utilities\SendFTP.exe UP 193.189.140.66 barrel FTP:K376:lcnb " & sOpspath & sDatename & ".zip /Custom/bahar/XCITEK/" & sDatename & ".ZIP",,true)

    'fso.DeleteFile(sOpspath & sTargfile & ".zip")
	'fso.DeleteFile(sOpspath & sTxtfile)


  End If

call tidyup  
  
end sub


Sub uklse()  
  
'**************************************************************
'	Custom routine for opsid number 6 (UKLSE wincab for Janak)
'
	sOpspath= "O:\dayedit\cem\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = mid(Year(Now),3,2)
  
  '-- Create the Custom filename without a extension  
  sCustomName = "UK" & sYear & sMonth & sDay 

  '- Zip copied file to Custom name & .ZIP  
  Y = wso.Run ("O:\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a " & sOpspath & sCustomName & ".ZIP " & sOpspath & "*.TXT", 1, TRUE)
  
  
  ' Send File via SendFTP.exe (FTP)
  ' Removed as now run as a separate ftcl step following in OPS
  'Y = wso.Run ("O:\utilities\SendFTP.exe UP www.exchange-data.com barrel FTP:K376:lcnb " & sOpspath & sCustomName & ".zip /Custom/bahar/UKLSE/" & sCustomName & ".ZIP",,true)
  'Y = wso.Run ("O:\utilities\SendFTP.exe UP 193.189.140.66 barrel FTP:K376:lcnb " & sOpspath & sCustomName & ".zip /Custom/bahar/UKLSE/" & sCustomName & ".ZIP",,true)

  
'	if fso.fileexists(sOpspath & sCustomName & ".zip") then
 '		fso.DeleteFile(sOpspath & sCustomName & ".zip")
 '	End If

end sub



Sub Smf2Wca()  
  
'**************************************************************
'	Custom routine for opsid number 8 (SMF daily for Janak)
'
	sOpspath= "O:\smfsql\wca\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = mid(Year(Now),3,2)
  
  '-- Create the Custom filename without a extension  
  sCustomName = "LS" & sYear & sMonth & sDay 

  '- Zip copied file to Custom name & .ZIP  
  Y = wso.Run ("O:\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a " & sOpspath & sCustomName & ".ZIP " & sOpspath & "*.TXT", 1, TRUE)
  
  
  ' Send File via SendFTP.exe (FTP)
  'Y = wso.Run ("O:\utilities\SendFTP.exe UP 62.231.129.157 barrel FTP:K376:lcnb " & sOpspath & sCustomName & ".zip /Custom/bahar/UKLSE/" & sCustomName & ".ZIP",,true)
  Y = wso.Run ("O:\utilities\SendFTP.exe UP www.exchange-data.com barrel FTP:K376:lcnb " & sOpspath & sCustomName & ".zip /Custom/bahar/UKLSE/" & sCustomName & ".ZIP",,true)
  Y = wso.Run ("O:\utilities\SendFTP.exe UP 193.189.140.66 barrel FTP:K376:lcnb " & sOpspath & sCustomName & ".zip /Custom/bahar/UKLSE/" & sCustomName & ".ZIP",,true)
  
  
	if fso.fileexists(sOpspath & sCustomName & ".zip") then
 		fso.DeleteFile(sOpspath & sCustomName & ".zip")
 		fso.DeleteFile(sOpspath & sCustomName & ".txt")
 	End If

end sub


Sub GrabWca()  
  
'**************************************************************
'	Custom routine for opsid number 7
'   1) Rename file to date based file name from first line 
'      data file
'
   
  sOpspath= "O:\wcasql\feeds\"			

  call genfile
  
  '- Unpack file
  'Y = wso.Run (sOpspath & sTargfile, 1, TRUE)
  '- UnZip copied file to Custom name & .ZIP  
'  Y = wso.Run ("pkunzip " & sOpspath & sTargfile & ".ZIP ", 1, TRUE)
  'Y = wso.Run ("o:\scripts\unzipxci.bat", 1, TRUE)
    
  
  'Response = MsgBox(CRLF & "------------------------------------------------------------" & CRLF & CRLF & _
'                         "      Packing fixed feed for "& sYear&sMonth&sDay & CRLF & CRLF & _
'                         "------------------------------------------------------------" & CRLF, vbOkCancel)
  
'  If Response = vbOK Then  
'
'    sDatename="XF"&mid(sYear,3,2)&sMonth&sDay
'    sArchpath=sOpspath&"archive\"&sYear&sMonth&"\"
'
   
'    If not (fso.FolderExists(sArchpath)) Then
'      fso.CreateFolder(sArchpath)
'    End If
   
'    If (fso.FolderExists(sArchpath)) Then
      ' nothing to do
	'Else
'       fso.CreateFolder(sArchpath)
'    End If

'    if fso.fileexists(sArchpath & sDatename & ".TXT") then
'      fso.DeleteFile(sArchpath & sDatename&".TXT")
'	End If  
'    fso.CopyFile sOpspath & sTxtfile, sOpspath & sDatename & ".TXT", true
'    fso.CopyFile sOpspath & sTxtfile, sArchpath & sDatename & ".TXT", true


    '- Zip copied file to Custom name & .ZIP  
'    Y = wso.Run ("O:\AUTO\Apps\ZIP\7-zip\7z.exe -tzip a " & sOpspath & sDatename & ".ZIP " & sOpspath & sDatename & ".TXT", 1, TRUE)

    ' Grab File via SendFTP.exe (FTP)
'    Y = wso.Run ("O:\utilities\SendFTP.exe UP eurotracker.com edi_19 vpg_9508 " & sOpspath & sDatename & ".zip FEEDS/XCITEK/" & sDatename & ".ZIP",,true)
    'fso.DeleteFile(sOpspath & sTargfile & ".zip")
	'fso.DeleteFile(sOpspath & sTxtfile)


'  End If

'call tidyup  
  
end sub


sub genfile()

  if Len(Month(Now-2))=1 then
  	sMonth="0" & month(now-2)
  else
  	sMonth=month(now-2)
  end if
  
  if Len(Day(now-2))=1 then 
  	sDay = "0" & day(Now-2)
  else
  	sDay = day(Now-2)
  end if

  sYear = mid(Year(Now-2),1,4)
  
  '-- Create the Custom filename
  sCustomName = sYear & sMonth & sDay & ".zip"

  ' Grab File via SendFTP.exe (FTP)
  Y = wso.Run ("O:\utilities\SendFTP.exe DOWN eurotracker.com ediadmin inter " & sOpspath & sCustomname & " /inetpub/eurotracker/data/data_19/feeds/" & sCustomname,,true)

end sub


'**************************************************************
'	Custom routine for Client number 3 (Deutsche)
'		1) Add the file specified to a zip file of the
'			 same name 'ALYYMMDD.ZIP'
'
Sub JapZip()  
			
	sOpspath= "O:\WCASQL\JPCA\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = mid(Year(Now),3,2)
  
  '-- Create the Custom filename without a extension  
  sCustomName = "JP" & sYear & sMonth & sDay 

  '- Zip copied file to Custom name & .ZIP  
  Y = wso.Run ("o:\support\wzzip\wzzip " & sOpspath & sCustomName & ".ZIP " & sOpspath & "*.*", 1, TRUE)
  
  
  ' Send File via SendFTP.exe (FTP)
  Y = wso.Run ("O:\utilities\SendFTP.exe UP eurotracker.com ediadmin inter " & sOpspath & sCustomName & ".ZIP data/data_19/Feeds/Japan_z/" & sCustomName & ".ZIP")
  
'	if fso.fileexists(sOpspath & sCustomName & ".DR1") then
' 		fso.DeleteFile(sOpspath & sCustomName & ".DR1")
' 	End If
' 	if fso.fileexists(sOpspath & sCustomName & ".DR2") then
' 		fso.DeleteFile(sOpspath & sCustomName & ".DR2")
' 	End If
' 	if fso.fileexists(sOpspath & sCustomName & ".DR3") then
' 		fso.DeleteFile(sOpspath & sCustomName & ".DR3")
' 	End If
end sub


Sub fidwca()  
  
'**************************************************************
'	Custom routine for Client number 10 
'	   send uncompressed yyyymmdd.608
'
   
  sOpspath= "O:\wcasql\608_not_us_ca\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay
    
  ' Send File via W:\VBPackages\SendFile\sendfile.exe (EMain)
  Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sOpspath & sCustomName & ".608 robert.nyman@FMR.com Backup1 WCA 608 feed") 
  ' Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sOpspath & sCustomName & ".608 germain.seac@FMR.com Backup2 WCA 608 feed") 

end sub


Sub fidwdi()  
  
'**************************************************************
'	Custom routine for Client number 11 
'	   send uncompressed yyyymmdd.513
'
   
  sOpspath= "O:\worldequ\fdi\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay
    
  ' Send File via W:\VBPackages\SendFile\sendfile.exe (EMain)
  Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sOpspath & sCustomName & ".513 robert.nyman@FMR.com Backup1 WDI 513 feed") 
  ' Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sOpspath & sCustomName & ".513 germain.seac@FMR.com Backup2 WDI 513 feed") 

end sub

Sub msciwca()  
' *trial 3/11/2003 changed 23/02/2004 defunct 2004/03/31
  
'**************************************************************
'	Custom routine for Client number 12
'
   
  sOpspath= "O:\wcasql\610\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay
    
  ' Send File via W:\VBPackages\SendFile\sendfile.exe (EMain)
  Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sOpspath & sCustomName & ".610 jennifer.soliman@msci.com 610 feed") 
  
end sub


Sub ubswca()  
' *trial 23/02/2004
  
'**************************************************************
'	Custom routine for Client number 12
'	   send uncompressed csv version yyyymmdd.514
'
   
  sOpspath= "O:\wcasql\610ubs\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay
    
  ' Send File via W:\VBPackages\SendFile\sendfile.exe (EMain)
  'Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sOpspath & sCustomName & ".610 i.grimer@exchange-data.com 610 feed")
  Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sOpspath & sCustomName & ".610 scott.fuller@ubs.com 610 feed")

end sub

Sub cazenove()  
  
'**************************************************************
'	Custom routine for Client number 15
'	   send uncompressed yyyymmdd.611
'
   
  sOpspath= "O:\Datafeed\Wca\611_EOD\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay
    
  ' Send File via W:\VBPackages\SendFile\sendfile.exe (EMain)
  'Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sOpspath & sCustomName & ".611 soven.amatya@jpmorgancazenove.com;andy.norton@jpmorgancazenove.com;fred.wong@jpmorgancazenove.com 611 feed")
  Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sOpspath & sCustomName & ".611 andy.norton@cazenove.com 611 feed")
    Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sOpspath & sCustomName & ".611 fred.wong@jpmorgancazenove.com;soven.amatya@jpmorgancazenove.com 611 feed")
  'Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sOpspath & sCustomName & ".611 soven.amatya@jpmorgancazenove.com 611 feed")
  Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sOpspath & sCustomName & ".611 genshpoll1@jpmorgancazenove.com;genshpoll2@jpmorgancazenove.com 611 feed")
  'Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sOpspath & sCustomName & ".611 genshpoll2@jpmorgancazenove.com 611 feed")

end sub

Sub pfpc()  
  
'**************************************************************
'	Custom routine for Client number 16
'	   send uncompressed yyyymmdd.617
'
   
  sOpspath= "O:\datafeed\equity\617\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay
    
  ' Send File via W:\VBPackages\SendFile\sendfile.exe (EMain)
  Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sOpspath & sCustomName & ".617 pncgis.ias.support@bnymellon.com 617 contingency email")
  
end sub


Sub sumit()  
  
'**************************************************************
'	Custom routine for Client number 17
'	   send uncompressed yyyymmdd.610
'
   
  sOpspath= "O:\Datafeed\Wca\610_sumit\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay
    
  ' Send File via W:\VBPackages\SendFile\sendfile.exe (EMain)
Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sOpspath & sCustomName & "_1.610 MSweeney@sumitomotrustusa.com;KCarney@sumitomotrustusa.com 610 daily from EDI")
Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sOpspath & sCustomName & "_2.610 MSweeney@sumitomotrustusa.com;KCarney@sumitomotrustusa.com 610 daily from EDI")
Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sOpspath & sCustomName & "_3.610 MSweeney@sumitomotrustusa.com;KCarney@sumitomotrustusa.com 610 daily from EDI")
'Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sOpspath & sCustomName & ".610 t.aidarus@exchange-data.com 610 daily from EDI")
  
end sub


Sub reuters()  
  
'**************************************************************
'	Custom routine for Client number 1
'	   send uncompressed yyyymmdd.611
'
   
  sOpspath= "O:\Datafeed\wca\605\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without an extension  
  sCustomName = sYear & sMonth & sDay
    
  ' Send File via W:\VBPackages\SendFile\sendfile.exe (EMain)
  'Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sOpspath & sCustomName & ".605 Robert.Taylor@reuters.com 605 feed")
  
end sub



Sub ipotrial()  
  
'**************************************************************
'	Custom routine for Client number 1
'	   send uncompressed yyyymmdd.611
'
   
  sOpspath= "O:\Datafeed\IPO\100\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without an extension  
  sCustomName = sYear & sMonth & sDay
    
  ' Send File via W:\VBPackages\SendFile\sendfile.exe (EMain)
  Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sOpspath & sCustomName & ".100 sbuhre@russell.com 100 IPO feed")
'  Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sOpspath & sCustomName & ".100 Lakshminarayana.Venkatesan@morganstanley.com;Louis.Marsillo@morganstanley.com;Vaughn.Sinclair@morganstanley.com;Ling.Jiang@morganstanley.com 100 IPO feed")
'  Y = wso.Run ("W:\VBPackages\SendFile\sendfile.exe " & sOpspath & sCustomName & ".100 dave.fisher@morganstanley.com;michael.hornef@morganstanley.com;Lwanga.Phillip@morganstanley.com;b.hopcroft@exchange-data.com 100 IPO feed")  
end sub
