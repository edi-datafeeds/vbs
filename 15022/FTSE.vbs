option explicit

Const adOpenStatic = 3
Const adLockOptimistic = 3

Private Sub OpenDbCon ()
    on error resume next
    	set DbCon = createobject("ADODB.Connection")
    	DbCon.Open "Provider=SQLOLEDB;Data Source=sqlsvr1;" & _
	       "Trusted_Connection=No;Initial Catalog=wca;" & _
               "User ID=sa;Password=;"
    	if err.number <> 0 Then
    		AppendLog("OpenDbCon: " & Err.Number & " " & Err.Description)
    		GetStub 'generate stub on error
    	end if
    on error goto 0
End Sub

Private Sub GetSqlDateTime ()
    
    Dim resultset 
    
    on error resume next
    	set resultset = CreateObject("ADODB.Recordset")             
    
    	resultset.Open "SELECT CONVERT(varchar,GETDATE(),103) AS SqlDate, CONVERT(varchar,GETDATE(),108) AS SqlTime", _
            DbCon, adOpenStatic, adLockOptimistic
    
    
    	If resultset.EOF Then 
        	Wscript.Echo "Record cannot be found."
    	Else
    		SqlDate = resultset.Fields(0).Value            	      
    		SqlTime = resultset.Fields(1).Value 
    	
    		'Wscript.Echo "SqlDate: " &  SqlDate
    		'Wscript.Echo "SqlTime: " &  SqlTime
    	End If        
    
    	resultset.close  
    if err.number <> 0 Then
       	AppendLog("GetSqlDateTime: " & Err.Number & " " & Err.Description)
       	GetStub 'generate stub on error
    end if
    on error goto 0
    
End Sub

Private Sub GetSysDateTime()
    
    Dim Tseq
    on error resume next
    	SysDate = Date    
    	SysTime = Time  
        
    	If  DateDiff("n",Date & " " & "08:50", Now) >= 0 and  DateDiff("n",Date & " " & "08:50", Now) <= 40 Then
    		SysSeq = 1
    		Fname = "15022_" & Right(SysDate, 4) & Mid(SysDate, 4,2) & Left(SysDate, 2) & "0930.txt"
    		
    		InsertFeedLog 'Do Feedlog entry
    	ElseIf  DateDiff("n",Date & " " & "14:20", Now) >= 0 and  DateDiff("n",Date & " " & "14:20", Now) <= 40 Then
    		SysSeq = 2
    		Fname = "15022_" & Right(SysDate, 4) & Mid(SysDate, 4,2) & Left(SysDate, 2) & "1500.txt"
    		
    		InsertFeedLog 'Do Feedlog entry
    	ElseIf  DateDiff("n",Date & " " & "19:50", Now) >= 0 and  DateDiff("n",Date & " " & "19:50", Now) <= 40 Then
    		SysSeq = 3
    		Fname = "15022_" & Right(SysDate, 4) & Mid(SysDate, 4,2) & Left(SysDate, 2) & "2030.txt"
    		
    		InsertFeedLog 'Do Feedlog entry
    	Else
    		SysSeq = 0
    		Fname = ""
    		AppendLog("Time out")    		
    	End If
    
    	'Wscript.Echo "SysSeq: " &  SysSeq
    	'Wscript.Echo "SysDate: " &  SysDate
    	'Wscript.Echo "SysTime: " &  SysTime
    if err.number <> 0 Then
        AppendLog("GetSysDateTime: " & Err.Number & " " & Err.Description)
        'GetStub 'generate stub on error        
    end if
    on error goto 0
End Sub

Private Sub InsertFeedLog()
	
     Dim Seq_rs , sqlcmd
     on error resume next
     	set Seq_rs = CreateObject("ADODB.Recordset")             
	    
     	'Get FromDate from feedlog
     	Seq_rs.Open "SELECT MAX(ToDate) FROM feedlog WHERE FeedFreq = '15022INCR'", _
	          DbCon, adOpenStatic, adLockOptimistic
	         	   
     	If Seq_rs.EOF Then 
        	FromDate = "Not Found"
        	'Wscript.Echo "FromDate cannot be found."
     	Else
        	FromDate = Seq_rs.Fields(0).Value            	      	   	 	   
		If FromDate <> "" Then		        
			'Wscript.Echo "FromDate: " &  FromDate
			FromDate = Mid(FromDate, 7,4) & "-" & Mid(FromDate, 4,2) & "-" & Left(FromDate,2) & " " & Right(FromDate, 8)  
     			'Wscript.Echo "FromDate: " &  FromDate     	
		Else
		     	FromDate = "Not Found"
     	        End If
     	End If
     	Seq_rs.close
     
     
     	'Get ToDate from wcalog     	
     	Seq_rs.Open "SELECT MAX(acttime) FROM tbl_opslog WHERE acttime > '" & FromDate & "'", _
	          DbCon, adOpenStatic, adLockOptimistic
	          
     	If Seq_rs.EOF Then 
     	     ToDate = "Not Found"
             'Wscript.Echo "ToDate cannot be found."
     	Else
             ToDate = Seq_rs.Fields(0).Value 
             If ToDate <> "" Then
             	'Wscript.Echo "ToDate: " &  ToDate
             	ToDate = Mid(ToDate, 7,4) & "-" & Mid(ToDate, 4,2) & "-" & Left(ToDate,2) & " " & Right(ToDate, 8)  
     	     	'Wscript.Echo "ToDate: " &  ToDate               
     	     Else
     	     	ToDate = "Not Found"
     	     End If
     	End If          
     	Seq_rs.close  
     
     	'Insert Date in to FeedLog
     	If FromDate <> "Not Found" and ToDate <> "Not Found" and FromDate <> "Null" and ToDate <> "Null" Then          	      
      		sqlcmd = "INSERT INTO feedlog (FeedFreq, FromDate, ToDate ) VALUES('15022INCR', '" & FromDate & "', '" & ToDate & "')"
      		DbCon.execute sqlcmd
      		isDel = 1
      		Run_t_EQ_incr 'Run t_EQ_inct batch file
      
     	Else    
     		isDel = 0  
     		AppendLog("Could not insert the FeedLog")
     		GetStub 'generate stub on error     	     
       End If
    if err.number <> 0 Then
    	     AppendLog("InsertFeedLog: " & Err.Number & " " & Err.Description)
    	     GetStub 'generate stub on error	
                     
    end if
    on error goto 0

End Sub


Private Sub Run_t_EQ_incr()
	on error resume next    
		Dim fso
	        Set fso = CreateObject("Scripting.FileSystemObject")	        
        	If fso.FileExists("O:\AUTO\Scripts\bat\15022\t_EQ_incr.bat") Then
    	    		t_EQ_incr = WsShell.Run ("O:\AUTO\Scripts\bat\15022\t_EQ_incr.bat", 1, TRUE)
	    		'Wscript.Echo "t_EQ_incr: " &  t_EQ_incr
	    		If t_EQ_incr = 0 Then  
	    			Run_ftse_15022 'Run ftse_15022 batch file
	    		Else
	    			AppendLog("Error generated while running t_EQ_incr.bat")
	    			GetStub 'generate stub on error    			
	    		End If
	        Else
	        	AppendLog("Script not found: O:\AUTO\Scripts\bat\15022\t_EQ_incr.bat")
	    		GetStub 'generate stub on error
	        End If
	        set fso = nothing
        if err.number <> 0 Then
             AppendLog("Run_t_EQ_incr: " & Err.Number & " " & Err.Description)
             GetStub 'generate stub on error             
        end if
    	on error goto 0

End Sub

Private Sub Run_ftse_15022()
    on error resume next
         Dim fso
	 Set fso = CreateObject("Scripting.FileSystemObject")	        
         If fso.FileExists("O:\AUTO\Scripts\bat\15022\ftse_15022.bat") Then
    		 ftse_15022 = WsShell.Run ("O:\AUTO\Scripts\bat\15022\ftse_15022.bat", 1, TRUE)
    	 	'Wscript.Echo "ftse_15022: " &  ftse_15022
    	 	If ftse_15022 = 0 Then  
	     		MergeFiles 'Merge files
	 	Else
	        	AppendLog("Error generated while running ftse_15022.bat")
	        	GetStub 'generate stub on error	   		     
    	 	End If
    	 Else
    	 	AppendLog("Script not found: O:\AUTO\Scripts\bat\15022\ftse_15022.bat")
	    	GetStub 'generate stub on error
    	 End If
    	 set fso = nothing
    if err.number <> 0 Then
         AppendLog("Run_ftse_15022: " & Err.Number & " " & Err.Description)
         GetStub 'generate stub on error
    end if
    on error goto 0
End Sub

Private Sub CloseDbCon()
	on error resume next
    	     DbCon.close    
    	     set DbCon = nothing
    	if err.number <> 0 Then
	     AppendLog("CloseDbCon: " & Err.Number & " " & Err.Description)
	     WScript.Quit(1)
	end if
    	on error goto 0
End Sub

Private Sub MergeFiles()

   on error resume next	           
	   Const ForReading = 1
	   Dim fso, f, ReadAllTextFile, MyFile, objFolderA, file ,i, arr,Filecnt
	   i =-1
	   Filecnt = -1
	   
	   DeleteFiles 'DeleteFiles
	   
	   Set fso = CreateObject("Scripting.FileSystemObject")
	   
	   Set objFolderA=fso.GetFolder("O:\Datafeed\15022\a_ftse\")
	   	   
	   
	   Set MyFile = fso.CreateTextFile("O:\Datafeed\15022\a_ftse\combined\" & Fname,true)	   	 	   
	   
	   'Filecnt = objFolderA.Files.count	   	     
	
	   'Getting file count
	   For Each file in objFolderA.Files
	   	Dim sf
	   	Set sf = fso.GetFile(file.Path)
	   	
	   	If sf.size > 0 Then 	
	   		Filecnt=Filecnt+1	   			   			   		   		
	   	End If
	   	
	   	Set sf = Nothing
	   Next	   	  
	   	   
	   
	   'Getting filenames into array
	   arr = array(Filecnt)
	   ReDim Preserve arr(ubound(arr) + Filecnt)
	   Wscript.Echo ubound(arr)
	   
	   For Each file in objFolderA.Files
	   	   	Dim sf1
	   	   	Set sf1 = fso.GetFile(file.Path)	   	   	
	   	   	If sf1.size > 0 Then 	
	   	   		i=i+1	   		
	   	   		arr(i) = file.Path	   			   			   		   	   		
	   	   	End If
	   	   	
	   	   	Set sf1 = Nothing
	   Next	 	   	   	  	   	
	   

	 'sorting array
	  dim si, j, temp, SortArray
	  for si = UBound(arr) - 1 To 0 Step -1
	  for j= 0 to si
	  if arr(j)>arr(j+1) then
	  temp=arr(j+1)
	  arr(j+1)=arr(j)
	  arr(j)=temp
	  end if
	  next
	  next
	  SortArray = arr 
	  
	  'Writing into file
	  dim s, x1, FinalFile	   
	  for x1=0 to ubound(SortArray)
	  	s=SortArray(x1)
	   	'Wscript.Echo s
	   	Set FinalFile = fso.OpenTextFile(s, ForReading)	   
                ReadAllTextFile =  FinalFile.ReadAll	                
	        MyFile.WriteLine(ReadAllTextFile)
	  next
	   
	  MyFile.Close
	  Set fso = Nothing
	   
	  'upload file
	  UploadFile 'Upload file on FTP
	   
	  WScript.Quit(0)
   if err.number <> 0 Then
   	 AppendLog("MergeFiles: " & Err.Number & " " & Err.Description)
   	 GetStub 'generate stub on error   	    	 
   end if
   on error goto 0

End Sub


Private Sub DeleteFiles()

	Wscript.Echo "Delete"
	
	Dim Dfso,objFolderB,file1
	
	Set Dfso = CreateObject("Scripting.FileSystemObject")
		   
        Set objFolderB=Dfso.GetFolder("O:\Datafeed\15022\a_ftse\combined\")
	
        For Each file1 in objFolderB.Files
		Dim sf
		Set sf = Dfso.GetFile(file1.Path)
		Wscript.Echo  file1.Path
		sf.delete() 		  
		Set sf = Nothing		
	Next	   
	
	Set Dfso = Nothing

End Sub

Private Sub DelFeedLogEntry()

   on error resume next
	Dim sql 
	Set sql = "DELETE FROM FeedLog WHERE FromDate = '" & FromDate & "' and ToDate = '" & ToDate & "'"
	DbCon.execute sql
   if err.number <> 0 Then
      	 AppendLog("DelFeedLogEntry: " & Err.Number & " " & Err.Description)
      	 WScript.Quit(1)
   end if
   on error goto 0
   
End Sub

Private Sub GetStub()
   on error resume next
	Dim fso, LogFile
        Set fso = CreateObject("Scripting.FileSystemObject")        
        Set LogFile = fso.CreateTextFile("O:\Datafeed\15022\a_ftse\combined\" & Fname,true)
	LogFile.WriteLine("Message")
	LogFile.close
	Set fso = Nothing
	
	If isDel > 0 Then
		DelFeedLogEntry 'Delete log entry
	End If
	
	UploadFile 'Upload file on FTP
	
	WScript.Quit(1)
   if err.number <> 0 Then
      	 AppendLog("GetStub: " & Err.Number & " " & Err.Description)
      	 WScript.Quit(1)
   end if
   on error goto 0
End Sub

Private Sub UploadFile()

    on error resume next
    	Dim y,z
	y = WsShell.Run ("o:\Apps\exe\ftcl.exe -f o:\Apps\scr_ftcl\Feed_generic.scr -s false -h www.exchange-data.net -u barrel -p FTP:K376:lcnb -MyLocalPath 'O:/Datafeed/15022/a_ftse/combined/' -MyRemotePath '/15022/ftse'",1,true)
	z = WsShell.Run ("o:\Apps\exe\ftcl.exe -f o:\Apps\scr_ftcl\Feed_generic.scr -s false -h www.exchange-data.com -u barrel -p FTP:K376:lcnb -MyLocalPath 'O:/Datafeed/15022/a_ftse/combined/' -MyRemotePath '/15022/ftse'",1,true)
    if err.number <> 0 Then
        AppendLog("UploadFile: " & Err.Number & " " & Err.Description)
        WScript.Quit(1)
    end if
    on error goto 0
	
End Sub

Private Sub AppendLog(ByVal msg)

   Dim fso, aLogFile
   'on error resume next
        
        Set fso = CreateObject("Scripting.FileSystemObject")
        
        If fso.FileExists("O:\AUTO\logs\" & "FTSE.log") Then
        	'Wscript.Echo "open"
        	Set aLogFile = fso.OpenTextFile("O:\AUTO\logs\" & "FTSE.log" ,8)
		aLogFile.WriteLine(FormatDateTime(Time(),4) & " " & FormatDateTime(Now(),1) & " " & msg)
		aLogFile.close
        
        Else
        	'Wscript.Echo "Create"
        	Set aLogFile = fso.CreateTextFile("O:\AUTO\logs\" & "FTSE.log" ,false)
		aLogFile.WriteLine(FormatDateTime(Time(),4) & " " & FormatDateTime(Now(),1) & " " & msg)
		aLogFile.close
        End If                
	Set fso = Nothing
	
   'if err.number <> 0 Then
        'AppendLog("AppendLog: " & Err.Number & " " & Err.Description)
        'WScript.Quit(1)
   'end if
   'on error goto 0

End Sub

Private Sub SendEmail()
	dim x
	x = WsShell.Run ("j:/java/prog/release/jsendfile/sendfile.jar -file O:/AUTO/Scripts/vbs/15022/FTSE.vbs -email i.grimer@exchange-data.com -subject FTSE",1,true)
End Sub
    
dim DbCon, SqlDate, SqlTime, SysDate, SysTime, WCAActTime, SysSeq, isRun, WsShell, t_EQ_incr, ftse_15022, Fname, isDel
dim FromDate, ToDate

Set WsShell = Wscript.CreateObject("Wscript.Shell")
WScript.Interactive = false
isDel = 0 'Set varaible for delete feedlog entry

OpenDbCon
GetSysDateTime
CloseDbCon
SendEmail


