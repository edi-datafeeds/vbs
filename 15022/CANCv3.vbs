'** extended for MT568
Option Explicit

'**** VARIABLES ****
Dim Clienttag, SVR, Feedpath
Dim Resultset, MyArray, dbconn
Dim CONF, canccorp, cancmtflag, cancseme, cancprev, canccaevmain, canct3g, camv, PrevSEME, tfb, wherestr, joinstr
Dim prev, seme, isohdr, LF, prep, headersql, isohdr568, messtxt
Dim MaxSEME, oFSO, Targfile, Archfile, rs
Const adOpenStatic = 3, adLockOptimistic = 3, adOpenDynamic = 2
Const ForReading = 1, ForWriting = 2, ForAppending = 8

'**** PREPARATION ****
Clienttag = WScript.Arguments.Item(0)
SVR = WScript.Arguments.Item(1)
Feedpath = WScript.Arguments.Item(2)

'** Hardcoded Information
CONF="O:\Auto\Configs\DbServers.cfg"
Set oFSO=CreateObject("Scripting.FileSystemObject")
LF = Chr(13) &Chr(10)

'** Database Connections
Call OpenDbCon(SVR, CONF)
Set Resultset = CreateObject("ADODB.Recordset")

'** Clear seme counter
MaxSEME=0

'** Setup output are archive file for CANC message
Resultset.Open "select CONVERT(varchar,GETDATE(),112) + replace(convert(varchar,GETDATE(), 08),':','')"_
               , dbconn, adOpenStatic, adLockOptimistic
prep = Resultset.Fields(0).Value
Resultset.Close
Set Targfile = oFSO.OpenTextFile(Feedpath &"canc\canc.txt", 2, True)
Set Archfile = oFSO.OpenTextFile(Feedpath &"cancarch\edi_" &Mid(prep,1,8) &"_" & Mid(prep,9,6) &".can", 2, True)

'** Get max seme from table and store in variable
Resultset.Open "select max(seme) from client.dbo." &Clienttag _
               , dbconn, adOpenStatic, adLockOptimistic
MaxSEME = Clng(Resultset.Fields(0).Value)
Resultset.Close

'************************************************************************************************************* MAIN ****
'** Construct shared query strings
joinstr= " from client.dbo."&Clienttag &" as canc" &_
         " inner join client.dbo."&Clienttag &" as newm on canc.caref2=newm.caref2" &_
                                                " and canc.caref3=newm.caref3" &_
                                                " and canc.mtflag=newm.mtflag"   '&_
'         " inner join wca.dbo.v54f_620_dividend_reinvestment_plan as vdrip on newm.caref2=vdrip.eventid" &_
'                          " and '2014/05/20'<vdrip.exdate"
wherestr=" where" &_
         " canc.corp<>newm.corp" &_
         " And canc.acttime<newm.acttime" &_
         " And canc.t3g<>'SYSC'" &_
         " And canc.t3g<>'CANC'" &_
         " And canc.caevmain<>'SPLF'" &_
         " And canc.caevmain<>'SPLR'" &_
         " And canc.tfb<>''" &_
         " And canc.tfb is not null" &_
         " And canc.caevmain<>'' and newm.caevmain<>''" &_
         " And ((canc.caevmain=newm.caevmain and (canc.camv<>newm.camv or canc.tfb<>newm.tfb))" &_
         " or (canc.caevmain<>newm.caevmain" &_
         "      and (canc.caevmain='CAPD' or canc.caevmain='CAPG' or canc.caevmain='SHPR')" &_
         "      and (newm.caevmain='CAPD' or newm.caevmain='CAPG' or newm.caevmain='SHPR'))" &_
         " or (canc.caevmain<>newm.caevmain" &_
         "      and (canc.caevmain='PRIO' or canc.caevmain='RHTS')" &_
         "      and (newm.caevmain='PRIO' or newm.caevmain='RHTS'))" &_
         " or (canc.caevmain<>newm.caevmain" &_
         "      and (canc.caevmain='DVCA' or canc.caevmain='DVSE' or canc.caevmain='DVOP' or canc.caevmain='DRIP')" &_
         "      and (newm.caevmain='DVCA' or newm.caevmain='DVSE' or newm.caevmain='DVOP' or newm.caevmain='DRIP'))" &_
         " or (canc.caevmain<>newm.caevmain" &_
         "      and (canc.caevmain='CMET' or canc.caevmain='MEET' or canc.caevmain='OMET' or canc.caevmain='XMET')" &_
         "      and (newm.caevmain='CMET' or newm.caevmain='MEET' or newm.caevmain='OMET' or newm.caevmain='XMET'))" &_
         " or (canc.caevmain<>newm.caevmain and canc.caevmain='MRGR' and newm.caevmain='TEND'))" &_
         " And newm.acttime>(select max(acttime)-1.1 from wca.dbo.tbl_opslog)"

'** Open resultset to get records to loop through
Resultset.Open " select canc.corp, canc.mtflag, canc.seme, canc.semedate" &_
             joinstr &_
             wherestr _
             , dbconn, adOpenStatic, adLockOptimistic

'** Loop through resultset updating prev and seme for records to be cancelled
If Resultset.EOF Then 
  WScript.Quit
End If


Resultset.MoveFirst
Do 
  ' Only process blank semedates since if populated the message has already be written
  ' This allows vbs to crash half way and be rerun and finish unprocessed CANCs only
  If IsNull(Resultset.Fields(3).Value) then
    MaxSEME=MaxSEME+1
    PrevSEME=Resultset.Fields(2).Value
    canccorp=Resultset.Fields(0).Value
    cancmtflag=Resultset.Fields(1).Value
    dbconn.Execute "update client.dbo."&Clienttag &_
                   " set prev = " &PrevSEME & ", seme = " &MaxSEME &_
                   ", semedate = (select max(acttime) from wca.dbo.tbl_opslog)" &_
                   " where" &_
                   " corp='" &canccorp &"'" &_
                   " and mtflag='" &cancmtflag &"'"
  End If               
  Resultset.MoveNext
Loop While Not Resultset.EOF

Resultset.Close

'** Get headersql from messhead
Resultset.Open " select headersql" &_
               " from client.dbo.canc_messhead" &_
               " where clienttag ='" &Clienttag &"'" &_
               " and clienttag ='" &Clienttag &"'" _
               , dbconn, adOpenStatic, adLockOptimistic
headersql=Replace(Resultset.Fields(0).Value,"`","'")
Resultset.Close

'** run headersql and store result in variable
Resultset.Open headersql, dbconn, adOpenStatic, adLockOptimistic
isohdr=Resultset.Fields(0).Value
isohdr568=Replace(isohdr,"O564","O568")
Resultset.Close

'** Re-query to get new values and write to file
Resultset.Open "select canc.corp, canc.mtflag, canc.seme, canc.prev, newm.corp, canc.caevmain, newm.caevmain, canc.t3g, canc.camv, canc.tfb" &_
              joinstr &_
              wherestr &_
              " order by canc.tfb" _
                , dbconn, adOpenStatic, adLockOptimistic

Resultset.MoveFirst
'** Loop through resultset and write out CANC messages               
Do
  canccorp=Resultset.Fields(0).Value
  cancmtflag=Resultset.Fields(1).Value
  cancseme=Resultset.Fields(2).Value
  cancprev=Resultset.Fields(3).Value
  canccaevmain=Resultset.Fields(5).Value
  canct3g=Resultset.Fields(7).Value
  camv=Resultset.Fields(8).Value
  If Trim(camv)="" And (canccaevmain="DRIP" Or canccaevmain="DVOP") Then camv="CHOS"
  If Trim(camv)="" And (canccaevmain="EXWA") Then camv="VOLU"
  If Trim(camv)="" Then camv="MAND" 
  tfb=Resultset.Fields(9).Value
  If cancmtflag=564 Then
    Call SelectMessType
    Archfile.write isohdr &LF &":16R:GENL" &LF &":20C::CORP//" &canccorp &LF &":20C::SEME//" &cancseme _
                          &LF &":23G:CANC" &LF &":22F::CAEV//" &canccaevmain _
                          &LF &":22F::CAMV//" &camv &LF &":98C::PREP//" &prep _ 
                          &LF &":25D::PROC//COMP" &LF &":16R:LINK" &LF &":13A::LINK//564" _ 
                          &LF &":20C::PREV//" &cancprev &LF &":16S:LINK" &LF &":16S:GENL" &LF &":16R:USECU" _
                          &LF &tfb &LF &":16R:FIA" &LF &":94B::PLIS//SECM" &LF &":16S:FIA" &LF &":16R:ACCTINFO" _
                          &LF &":97A::SAFE//NONREF" &LF &":16S:ACCTINFO" &LF &":16S:USECU" &LF &messtxt _
                          &"-}$" &LF
    Targfile.write isohdr &LF &":16R:GENL" &LF &":20C::CORP//" &canccorp &LF &":20C::SEME//" &cancseme _
                          &LF &":23G:CANC" &LF &":22F::CAEV//" &canccaevmain _
                          &LF &":22F::CAMV//" &camv &LF &":98C::PREP//" &prep _ 
                          &LF &":25D::PROC//COMP" &LF &":16R:LINK" &LF &":13A::LINK//564" _ 
                          &LF &":20C::PREV//" &cancprev &LF &":16S:LINK" &LF &":16S:GENL" &LF &":16R:USECU" _
                          &LF &tfb &LF &":16R:FIA" &LF &":94B::PLIS//SECM" &LF &":16S:FIA" &LF &":16R:ACCTINFO" _
                          &LF &":97A::SAFE//NONREF" &LF &":16S:ACCTINFO" &LF &":16S:USECU" &LF &messtxt _
                          &"-}$" &LF
  Else
    Archfile.write isohdr568 &LF &":16R:GENL" &LF &":20C::CORP//" &canccorp &LF &":20C::SEME//" &cancseme _
                          &LF &":23G:CANC" &LF &":22F::CAEV//" &canccaevmain _
                          &LF &":98C::PREP//" &prep _ 
                          &LF &":16R:LINK" &LF &":13A::LINK//568" _ 
                          &LF &":20C::PREV//" &cancprev &LF &":16S:LINK" &LF &":16S:GENL" &LF &":16R:USECU" _
                          &LF &":97C::SAFE//GENR" &LF &tfb &LF &":16R:FIA" &LF &":94B::PLIS//SECM" _
                          &LF &":16S:FIA" &LF &":16S:USECU" &LF &":16R:ADDINFO" &LF &":70F::ADTX//dummy text" _
                          &LF &":16S:ADDINFO" &LF &"-}$" &LF
    Targfile.write isohdr568 &LF &":16R:GENL" &LF &":20C::CORP//" &canccorp &LF &":20C::SEME//" &cancseme _
                          &LF &":23G:CANC" &LF &":22F::CAEV//" &canccaevmain _
                          &LF &":98C::PREP//" &prep _ 
                          &LF &":16R:LINK" &LF &":13A::LINK//568" _ 
                          &LF &":20C::PREV//" &cancprev &LF &":16S:LINK" &LF &":16S:GENL" &LF &":16R:USECU" _
                          &LF &":97C::SAFE//GENR" &LF &tfb &LF &":16R:FIA" &LF &":94B::PLIS//SECM" _
                          &LF &":16S:FIA" &LF &":16S:USECU" &LF &":16R:ADDINFO" &LF &":70F::ADTX//dummy text" _
                          &LF &":16S:ADDINFO" &LF &"-}$" &LF
  End if                          
  Resultset.MoveNext
Loop While Not Resultset.EOF

Resultset.Close

'** Open resultset to mark SYSC and update
Resultset.Open " select canc.corp, canc.mtflag" &_
               joinstr &_
               wherestr _
               , dbconn, adOpenStatic, adLockOptimistic

'** Update the SYSC
Resultset.MoveFirst
Do 
  canccorp=Resultset.Fields(0).Value
  cancmtflag=Resultset.Fields(1).Value
  dbconn.Execute "update client.dbo."&Clienttag &_
                " set t3g = 'SYSC'" &_
                " where" &_
                " corp='" &canccorp &"'" &_
                " and mtflag='" &cancmtflag &"'"
  Resultset.MoveNext
Loop While Not Resultset.EOF
Resultset.Close


'**** SUBROUTINES ****

'** Database Connection
Private Sub OpenDbCon (p_svr, p_conf)
Dim fso, f1, ts, s, uname, pword, prov, pcata, dsource
Const ForReading = 1
prov = "SQLOLEDB"
'** Get connection details
Set fso = CreateObject("Scripting.FileSystemObject")   
Set ts = fso.OpenTextFile(p_conf, ForReading)
Do 
  s = ts.ReadLine
  MyArray = Split(s, vbTab)
  If MyArray(0) = p_svr Then
    uname = MyArray(2)
    pword = MyArray(3)
    dsource = MyArray(4) 			
    Exit do
  End if		
Loop while NOT ts.AtEndOfStream
ts.Close
'** connect to database
On Error Resume Next
Set dbconn = CreateObject("ADODB.Connection")
dbconn.Open "Provider="&prov &";Data Source=" &dsource &";" &_
" Trusted_Connection=No;Initial Catalog=client;" &_
" User ID="&MyArray(2) &";Password=" &pword &";"
If Err.Number <> 0 Then
  MsgBox Err.Number & " " & Err.Description
End If
On Error Goto 0
End Sub

'** Select message text from canc_messtype depending on message type
Sub SelectMessType
Set rs = CreateObject("ADODB.Recordset")

'MsgBox Clienttag
'MsgBox canccaevmain
'MsgBox camv


rs.Open "Select messtext from client.dbo.canc_messtype" &_
       " where clienttag ='" &Clienttag &"'" &_
       " and caevmain ='" &canccaevmain &"'" &_
       " and camv ='" &camv &"'" _
       , dbconn, adOpenStatic, adLockOptimistic
  messtxt = Replace(rs.Fields(0).Value,"`",Chr(13) &Chr(10))
rs.close
End Sub