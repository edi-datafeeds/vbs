' ****************************************************************
' SSN EMAILER.VBS
'
'     Script for emailing Notifications Without Attachments
' 
' Arguments in order (0) Client_ID	         e.g. 1
'                    (1) Source		         e.g. c:\autoexec.bat
'
' ****************************************************************

' **** SUB MAIN ****
	dim fso,f,L1,wso,colArgs,fldr,fcol,lastday,Seclast,CRLF,sSource,sOpspath,sOpspath2,sOpspath3,sTxtfile,sTargfile,sDatename,sArchpath

	CRLF = Chr(13) & Chr(10)

	Set colArgs = WScript.Arguments
 
	sClientID = colArgs(0)
	sAttach = colArgs(1)
	'sExt = colArgs(2)
	sOpspath = "o:\worldequ\wsoexp\"
	sYear = "00"
	sDay = "00"
	sMonth = "00"
	
 '*********************************************************
 'Removed for Automation
	'Response = MsgBox(CRLF & "------------------------------------------------------------" & CRLF & CRLF & _
        '                 "              Custom DateZip" & CRLF & CRLF & _
        '                 "------------------------------------------------------------" & CRLF, vbOkCancel)

'*********************************************************
 'Removed for Automation
'	If Response = vbOK Then  
		Set fso = CreateObject("Scripting.FileSystemObject")
		Set wso = Wscript.CreateObject("Wscript.Shell")
	
		'-- Call custom routine by using the sClientID
		'		which was passed via command Line argument(0)
		
				
		Select case sClientID
			case "1"
					Call SSN_EmailAlert1
			case "2"
					Call ipotrial
			case "3"
					Call distributionlist
			case "4"
					Call dresdner
			case "5"
					Call schroders
			case "6"
					Call emts
			case "7"
					Call Missing_Prices_PDES
			case "8"
					Call Missing_Prices
			case "9"
					Call cazenove
			case "10"
					Call pfpc
			case "11"
					Call Missing_Index_Prices
			case "12"
					Call Price_File_FTP_Failure
			case "13"
					Call Missing_Prices_new
			case "14"
					Call fortis_167_620
			case "15"
					Call fortis_167_630
			case "16"
					Call wfi_sample_300
			case "17"			
					Call LSTAT_weekly
			case "18"	
					Call NLIST_weekly
			case "19"		
					Call UnmatchedBondPrice
			case "20"		
					Call FTP_Dir
			case "21"		
					Call schroders_2
					
			case "25"		
					Call schroders_3
					
			case "22"		
					Call GBDataTeam
			case "23"		
					Call tHoldSec
			case "24"		
					Call WFI_Markit_Russian_New_Issuance
			end select
		
	
	
	'*********************************************************
 'Removed for Automation
	'End If



' **** END SUB MAIN ****

'**************************************************************
'	Remove all reference to any object created early in this
' script
'
Sub TidyUp()


'*********************************************************
 'Removed for Automation
 	'msg = "Operation has completed"
'msgbox msg,,"DateZip.vbs"
 	if fso.fileexists(sOpspath & sCustomName & ".TXT") then
 		fso.DeleteFile(sOpspath & sCustomName & ".TXT")
 	End If
  Set fso = Nothing
  Set wso = Nothing
End Sub


Sub SSN_EmailAlert1()  
  
'**************************************************************
'	Custom routine for Client number 1
'	   
   
    
'Y = wso.Run ("J:\java\Prog\Y.laifa\NB6\J2SE\SendFile\dist\SendFile.jar -email y.laifa@exchange-data.com;ukcorporateactions@citigroup.com;phil.c.davies@citigroup.com -subject EDI Wincab docs are available")
 
 Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com;ukcorporateactions@citigroup.com;phil.c.davies@citigroup.com -SUB EDI Wincab docs are available") 
' Y = wso.Run ("J:\java\Prog\I.Cornish\NB6\J2SE\gmailer\dist\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com -SUB EDI Wincab docs are available") 

end sub


Sub ipotrial()  
  
'**************************************************************
'	Custom routine for Client number 2
'	   send uncompressed yyyymmdd.100
'
   
  sOpspath= "O:\Datafeed\IPO\100\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without an extension  
  sCustomName = sYear & sMonth & sDay
    
  
  'Y = wso.Run ("J:\java\Prog\Y.laifa\NB6\J2SE\SendFilenew\dist\SendFile.jar -file " & sOpspath & sCustomName & ".100 -email y.laifa@exchange-data.com;sbuhre@russell.com -subject EDI 100 IPO feed")
 
 Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com;sbuhre@russell.com -ATTACH " & sOpspath & sCustomName & ".100 -SUB EDI 100 IPO feed") 
 
 end sub
 
 Sub distributionlist()  
   
 '**************************************************************
 '	Distribution list routine for ssn 3
 '	   
 '
    
   sOpspath= "o:\modem\cab\"			
   '-- Build custom output file name, ensuring that
   '		both sMonth and sDay are always two digits
   
   
   '-- Create the Custom filename without a extension  
   sCustomName = "dil_day"
     
   
   'Y = wso.Run ("J:\java\Prog\Y.laifa\NB6\J2SE\SendFilenew\dist\SendFile.jar -file " & sOpspath & sCustomName & ".zip -email y.laifa@exchange-data.com;manju.vishwanathan@reuters.com;arun.alphonse@reuters.com;bhargavi.kumari@reuters.com;Malavika.swamy@reuters.com;Shilpa.Rajan@reuters.com;Janardhan.yalapalli@reuters.com;Hozefa.Saifuddin@reuters.com -subject EDI UK Distribution List")
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com;manju.vishwanathan@reuters.com;arun.alphonse@reuters.com;bhargavi.kumari@reuters.com;Malavika.swamy@reuters.com;Shilpa.Rajan@reuters.com;Janardhan.yalapalli@reuters.com;Hozefa.Saifuddin@reuters.com -ATTACH " & sOpspath & sCustomName & ".zip -SUB EDI UK Distribution List") 
 
end sub

Sub dresdner()  
  
'**************************************************************
'	Custom routine for Client number 4
'	   send cem_day.zip
'
   
  sOpspath= "O:\modem\cab\cem_day"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without an extension  
  'sCustomName = sYear & sMonth & sDay
    
  
  'Y = wso.Run ("J:\java\Prog\Y.laifa\NB6\J2SE\SendFilenew\dist\SendFile.jar -file " & sOpspath & ".zip -email y.laifa@exchange-data.com;corp.events@dkib.com -subject EDI Daily Wincab data")

  Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com;corp.events@dkib.com -ATTACH " & sOpspath & ".zip -SUB EDI Daily Wincab data") 

end sub
 
Sub schroders()  
  
'**************************************************************
'	Custom routine for Client number 5
'
   
  'sOpspath= "O:\Datafeed\Equity\620_LSTAT\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without an extension  
  'sCustomName = sYear & sMonth & sDay
  sOpspath= "O:\Datafeed\Equity\620_LSTAT\" & sYear & sMonth & sDay & ".620"			
  'sOpspath= "O:\Datafeed\Equity\620_LSTAT\20140318.620"			
  'msgbox sOpspath  
  
 Set fs = CreateObject("Scripting.FileSystemObject")

  if fs.FileExists(sOpspath) = False then
    msgbox "No File has been generated for today, Please Regenerate The File!"
  else
    'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO *UKSIM-CorporateActions@schroders.com;b.james@exchange-data.com;webmaster@exchange-data.com -ATTACH " & sOpspath & " -SUB EDI LSTAT 620 Daily") 
    
    Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO SI-SIMCOACControl@Schroders.com;b.james@exchange-data.com;webmaster@exchange-data.com -ATTACH " & sOpspath & " -SUB EDI LSTAT 620 Daily")
    '2016-05-24 'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO *UKSIM-CorporateActions@schroders.com;SI-SIMCOACControl@Schroders.com;b.james@exchange-data.com;webmaster@exchange-data.com -ATTACH " & sOpspath & " -SUB EDI LSTAT 620 Daily")
    'Y = wso.Run ("J:\java\Prog\I.Cornish\NB6\J2SE\gmailer\dist\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.migou@exchange-data.com -ATTACH " & sOpspath & " -SUB EDI LSTAT 620 Daily") 
    'if y = 0 then
    '	msgbox "Check LSTAT Email is in Webmaster"
    if y = 1 then
    	msgbox "LSTAT Email Failed. Check Webmaster to Confirm Failure"
    End if	
 end if
  
  
end sub

Sub schroders_2()  
  
'**************************************************************
'	Custom routine for Client number 21
'
   
  'sOpspath= "O:\Datafeed\Equity\620_LSTAT\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without an extension  
  'sCustomName = sYear & sMonth & sDay
  sOpspath= "O:\Datafeed\Equity\620_LSTAT_LIVE\" & sYear & sMonth & sDay & "_620.txt"
  'sOpspath= "O:\Datafeed\Equity\620_LSTAT_LIVE\20140404_620.txt"		
  'msgbox sOpspath  
  
 Set fs = CreateObject("Scripting.FileSystemObject")

  if fs.FileExists(sOpspath) = False then
    msgbox "No File has been generated for today, Please Regenerate The File!"
  else
    'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO *UKSIM-CorporateActions@schroders.com;b.james@exchange-data.com;webmaster@exchange-data.com -ATTACH " & sOpspath & " -SUB EDI LSTAT 620 Daily") 
    
    Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO SI-SIMCOACControl@Schroders.com;b.james@exchange-data.com;webmaster@exchange-data.com -ATTACH " & sOpspath & " -SUB EDI LSTAT 620 Live Daily")
    '2016-05-24 'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO gordon.more@schroders.com;SI-SIMCOACControl@Schroders.com;b.james@exchange-data.com;webmaster@exchange-data.com -ATTACH " & sOpspath & " -SUB EDI LSTAT 620 Live Daily")
    'Y = wso.Run ("J:\java\Prog\I.Cornish\NB6\J2SE\gmailer\dist\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.migou@exchange-data.com -ATTACH " & sOpspath & " -SUB EDI LSTAT 620 Daily") 
    'if y = 0 then
    '	msgbox "Check LSTAT Email is in Webmaster"
    if y = 1 then
    	msgbox "LSTAT LIVE Email Failed. Check Webmaster to Confirm Failure"
    End if	
 end if
  
  
end sub

Sub schroders_3()  
  
'**************************************************************
'	Custom routine for Client number 21
'
   
  'sOpspath= "O:\Datafeed\Equity\620_LSTAT\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without an extension  
  'sCustomName = sYear & sMonth & sDay
  sOpspath= "O:\Datafeed\Equity\620_LSTAT_Alert\" & sYear & sMonth & sDay & "_620_LSTAT_Alert.txt"
  'sOpspath= "O:\Datafeed\Equity\620_LSTAT_LIVE\20140404_620.txt"		
  'msgbox sOpspath  
  
 Set fs = CreateObject("Scripting.FileSystemObject")

  if fs.FileExists(sOpspath) = False then
    msgbox "No File has been generated for today, Please Regenerate The File!"
  else
    'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO *UKSIM-CorporateActions@schroders.com;b.james@exchange-data.com;webmaster@exchange-data.com -ATTACH " & sOpspath & " -SUB EDI LSTAT 620 Daily") 
    
    Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO SI-SIMCOACControl@Schroders.com;b.james@exchange-data.com;webmaster@exchange-data.com -ATTACH " & sOpspath & " -SUB EDI LSTAT 620 Live Daily")
    '2016-05-24 'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO gordon.more@schroders.com;SI-SIMCOACControl@Schroders.com;b.james@exchange-data.com;webmaster@exchange-data.com -ATTACH " & sOpspath & " -SUB EDI LSTAT 620 Live Daily")
    'Y = wso.Run ("J:\java\Prog\I.Cornish\NB6\J2SE\gmailer\dist\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.migou@exchange-data.com -ATTACH " & sOpspath & " -SUB EDI LSTAT 620 Daily") 
    'if y = 0 then
    '	msgbox "Check LSTAT Email is in Webmaster"
    if y = 1 then
    	msgbox "LSTAT LIVE Email Failed. Check Webmaster to Confirm Failure"
    End if	
 end if
  
  
end sub

Sub emts()  
  
'**************************************************************
'	Custom routine for Client number 6
'
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)

  'sOpspath= "O:\Datafeed\EMTS\Prices\YB" & sYear & sMonth & sDay & "-PRICES11AM.CSV"			
  'Y = wso.Run ("J:\java\Prog\Y.laifa\NB6\J2SE\SendFilenew\dist\SendFile.jar -file " & sOpspath & " -email ops@exchange-data.com;christine.sheeka@euromts-ltd.com -subject Claudio Baraldi: Euro MTS 11am Price File")
  'sOpspath= "O:\Datafeed\EMTS\Prices\YB" & sYear & sMonth & sDay & "-PRICES04PM.CSV"			
  ''hard codeded test to send specific file sOpspath= "O:\Datafeed\EMTS\Prices\YB20100419-PRICES04PM.CSV"			
  'Y = wso.Run ("J:\java\Prog\Y.laifa\NB6\J2SE\SendFilenew\dist\SendFile.jar -file " & sOpspath & " -email ops@exchange-data.com;christine.sheeka@euromts-ltd.com -subject Claudio Baraldi: Euro MTS 4pm Price File")
  'sOpspath= "O:\Datafeed\EMTS\Prices_IT_5DP\YB" & sYear & sMonth & sDay & "-PRICES5DP.CSV"			
  'Y = wso.Run ("J:\java\Prog\Y.laifa\NB6\J2SE\SendFilenew\dist\SendFile.jar -file " & sOpspath & " -email ops@exchange-data.com;christine.sheeka@euromts-ltd.com -subject Claudio Baraldi: Euro MTS 5dp Price File")
  
  
  
  sOpspath= "O:\Datafeed\EMTS\Prices\YB" & sYear & sMonth & sDay & "-PRICES11AM.CSV"
'  sOpspath= "O:\Datafeed\EMTS\Prices\test.txt"
  sOpspath2= "O:\Datafeed\EMTS\Prices\YB" & sYear & sMonth & sDay & "-PRICES04PM.CSV"
  sOpspath3= "O:\Datafeed\EMTS\Prices_IT_5DP\YB" & sYear & sMonth & sDay & "-PRICES5DP.CSV"
  'sOpspath3= "O:\Datafeed\EMTS\Prices_IT_5DP\YB20121130-PRICES5DP.CSV"
  
  
  
  'Y = wso.Run ("J:\java\Prog\Y.laifa\NB6\J2SE\SendFilenew\dist\SendFile.jar -file "& sOpspath &";"& sOpspath2 &";"& sOpspath3 &" -email ops@exchange-data.com;christine.sheeka@euromts-ltd.com -subject Euro MTS Price Files Back Up")
 	
 	
 If fso.fileexists(sOpspath) then
  	Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com;christine.sheeka@euromts-ltd.com -ATTACH "& sOpspath &" -SUB Euro MTS Price Files Back Up")
 '  	Y = wso.Run ("J:\java\Prog\I.Cornish\NB6\J2SE\gmailer\dist\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com -ATTACH "& sOpspath &" -SUB Euro MTS Price Files Back Up")
 
 
  	'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com -ATTACH "& sOpspath &" -SUB Euro MTS Price Files Back Up")
 
 
 
 Elseif fso.fileexists(sOpspath2) then
 	Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com;christine.sheeka@euromts-ltd.com -ATTACH "& sOpspath2 &" -SUB Euro MTS Price Files Back Up")
 
 Elseif fso.fileexists(sOpspath3) then
 	Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com;christine.sheeka@euromts-ltd.com -ATTACH "& sOpspath3 &" -SUB Euro MTS Price Files Back Up")
 
 else	
 	  msgbox "File to be emailed does not exist! Please re-run the process before this"
 
 End If
 
   
end sub

Sub Missing_Prices_PDES()  
   
 '**************************************************************
 '	Missing Prices routine for number 7
 '	   
 '
     
   'Y = wso.Run ("W:\VBPackages\sendfile\sendfile.exe O:\datafeed\prices\PDES\PDES_missing.txt h.patel@exchange-data.com;a.mohammed@exchange-data.com;j.bloch@exchange-data.com;grade@btinternet.com;ops@exchange-data.com;t.elliott@exchange-data.com Vietnam Missing Price files")
   
   'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO h.patel@exchange-data.com;a.mohammed@exchange-data.com;j.bloch@exchange-data.com;webmaster@exchange-data.com;y.vasilev@exchange-data.com;d.hanzenova@exchange-data.com;s.barry@exchange-data.com; -ATTACH O:\datafeed\prices\PDES\PDES_missing.txt -SUB PDES Missing Price files")
   
   '2015-06-02 - akg - removed j.bloch name from the email as requested
   'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO h.patel@exchange-data.com;a.mohammed@exchange-data.com;j.bloch@exchange-data.com;webmaster@exchange-data.com;y.vasilev@exchange-data.com;d.hanzenova@exchange-data.com; -ATTACH O:\datafeed\prices\PDES\PDES_missing.txt -SUB PDES Missing Price files")
	
	Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO h.patel@exchange-data.com;a.mohammed@exchange-data.com;d.hanzenova@exchange-data.com; -ATTACH O:\datafeed\prices\PDES\PDES_missing.txt -SUB PDES Missing Price files")
	
end sub

Sub Missing_Prices()  
   
 '**************************************************************
 '	Missing Prices routine for number 8
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   ''WScript.Echo sDate
   ''WScript.Quit
   'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO h.patel@exchange-data.com;a.mohammed@exchange-data.com;d.hanzenova@exchange-data.com;a.kitchingham@exchange-data.com;s.bhuskute@exchange-data.com;k.martins@exchange-data.com;y.vasilev@exchange-data.com;webmaster@exchange-data.com -ATTACH O:\datafeed\prices\missing_files\" & sDate & "_prices_missing.txt -SUB Missing Price files")
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO h.patel@exchange-data.com;t.dhokre@exchange-data.com;support@exchange-data.com;s.bhuskute@exchange-data.com;webmaster@exchange-data.com -ATTACH O:\datafeed\prices\missing_files\" & sDate & "_prices_missing.txt -SUB Missing Price files")
   'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO h.patel@exchange-data.com;s.gwira@exchange-data.com;t.dhokre@exchange-data.com;support@exchange-data.com;s.bhuskute@exchange-data.com;webmaster@exchange-data.com -ATTACH O:\datafeed\prices\missing_files\" & sDate & "_prices_missing.txt -SUB Missing Price files")
   'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com -ATTACH O:\datafeed\prices\missing_files\" & sDate & "_prices_missing.txt -SUB Missing Price files")
   
 
end sub


Sub cazenove()
   
'**************************************************************
'	Custom routine for Client number 9
'	   send uncompressed yyyymmdd.611
'
   
  sOpspath= "O:\Datafeed\Wca\611_EOD\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & ".611"
    
  'Y = wso.Run ("j:\java\prog\release\jsendfile\sendfile.jar -file " & sOpspath & sCustomName & ".611 -email andy.norton@cazenove.com -subject 611 feed")
  'Y = wso.Run ("j:\java\prog\release\jsendfile\sendfile.jar -file " & sOpspath & sCustomName & ".611 -email fred.wong@jpmorgancazenove.com;soven.amatya@jpmorgancazenove.com -subject 611 feed")
  'Y = wso.Run ("j:\java\prog\release\jsendfile\sendfile.jar -file " & sOpspath & sCustomName & ".611 -email genshpoll1@jpmorgancazenove.com;genshpoll2@jpmorgancazenove.com -subject 611 feed")
  
  Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO andy.norton@cazenove.com;fred.wong@jpmorgancazenove.com;soven.amatya@jpmorgancazenove.com;genshpoll1@jpmorgancazenove.com;webmaster@exchange-data.com -ATTACH " & sOpspath & sCustomName & " -SUB 611 feed")
  'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO ops@exchange-data.com -ATTACH " & sOpspath & sCustomName & " -SUB 611 feed")
  	
end sub
 
Sub pfpc()  
  
'**************************************************************
'	Custom routine for Client number 10
'	   send uncompressed yyyymmdd.617
'
   
  sOpspath= "O:\datafeed\equity\617\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without a extension  
  sCustomName = sYear & sMonth & sDay & ".617"
    
    
    'msgbox sCustomName
    
    
  ' Send File via W:\VBPackages\SendFile\sendfile.exe (EMain)
  'Y = wso.Run ("j:\java\prog\release\jsendfile\sendfile.jar -file " & sOpspath & sCustomName & ".617 -email pncgis.ias.support@bnymellon.com -subject 617 contingency email")
  'Y = wso.Run ("j:\java\prog\release\jsendfile\sendfile.jar -file " & sOpspath & sCustomName & ".617 -email youcef_laifa@hotmail.com -subject 617 contingency email")
  
  Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO bnym.gis.iax.production.support@bnymellon.com;webmaster@exchange-data.com -ATTACH " & sOpspath & sCustomName & " -SUB 617 contingency email")
  
  'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO youcef_laifa@hotmail.com -ATTACH " & sOpspath & sCustomName & " -SUB test")
  
end sub

Sub Missing_Index_Prices()  
   
 '**************************************************************
 '	Missing Index Prices routine for number 11
 '	Manasi's Feed   
 '
     
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO h.patel@exchange-data.com;a.mohammed@exchange-data.com;j.bloch@exchange-data.com;s.bhuskute@exchange-data.com;support@exchange-data.com;webmaster@exchange-data.com;a.aarar@exchange-data.com -ATTACH O:\Datafeed\Prices\MissingIDXNmList\MissingIDXNmList.txt -SUB Missing Index Name List")
   
 
end sub


Sub Price_File_FTP_Failure()  
   
 '**************************************************************
 '	Price File FTP Failure routine for number 12
 '	   
 '
 ' 22/10/2012 - DJ - 2012102210000014
 
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO h.patel@exchange-data.com;s.bhuskute@exchange-data.com;grade@btinternet.com;t.dhokre@exchange-data.com;support@exchange-data.com;webmaster@exchange-data.com -ATTACH O:\Datafeed\Prices\FTPFailure\FTPFAILURE.txt -SUB Price File FTP Failure")
   'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO h.patel@exchange-data.com;s.bhuskute@exchange-data.com;grade@btinternet.com;s.gwira@exchange-data.com;t.dhokre@exchange-data.com;support@exchange-data.com;webmaster@exchange-data.com -ATTACH O:\Datafeed\Prices\FTPFailure\FTPFAILURE.txt -SUB Price File FTP Failure")
   
 
end sub

Sub Missing_Prices_new()  
   
 '**************************************************************
 '	Missing Prices routine for number 13
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   ''WScript.Echo sDate
   ''WScript.Quit
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO support@exchange-data.com;m.tandau@exchange-data.com;s.bhuskute@exchange-data.com;t.dhokre@exchange-data.com;support@exchange-data.com;webmaster@exchange-data.com -ATTACH O:\Datafeed\Prices\missing_files_new\" & sDate & "_NEW_Prices_Missing.txt -SUB Missing Price files New")
   'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO support@exchange-data.com;m.tandau@exchange-data.com;s.bhuskute@exchange-data.com;s.gwira@exchange-data.com;t.dhokre@exchange-data.com;support@exchange-data.com;webmaster@exchange-data.com -ATTACH O:\Datafeed\Prices\missing_files_new\" & sDate & "_NEW_Prices_Missing.txt -SUB Missing Price files New")
   
   
 
end sub

Sub fortis_167_620()  
   
 '**************************************************************
 '	fortis_167_620 for number 14
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   ''WScript.Echo sDate
   ''WScript.Quit
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO ITBusinessDevelopment.dl@uk.abnamroclearing.com;webmaster@exchange-data.com -ATTACH o:\upload\acc\167\feed\" & sDate & ".620 -SUB Acc 167 620 Fortis Backup")
   
   
 
end sub

Sub fortis_167_630()  
   
 '**************************************************************
 '	fortis_167_630 for number 15
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   ''WScript.Echo sDate
   ''WScript.Quit
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO ITBusinessDevelopment.dl@uk.abnamroclearing.com;webmaster@exchange-data.com -ATTACH O:\Datafeed\Equity\630\" & sDate & ".630 -SUB Acc 167 630 Fortis Backup")
   
   
 
end sub

Sub wfi_sample_300()  
   
 '**************************************************************
 '	fortis_167_630 for number 16
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   ''WScript.Echo sDate
   ''WScript.Quit
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com;d.moore@exchange-data.com;y.laifa@exchange-data.com -ATTACH o:\upload\acc\300\feed\" & sDate & "_300.zip -SUB WFI Sample 300")
   
   
 
end sub

Sub LSTAT_weekly()  
  
'**************************************************************
'	Custom routine for Client number 17
'
   
  'sOpspath= "O:\Datafeed\Equity\620_LSTAT\"			
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without an extension  
  'sCustomName = sYear & sMonth & sDay
  sOpspath= "O:\Datafeed\bespoke\mbendi\" & sYear & sMonth & sDay & "_LSTAT_Weekly.txt"			
  'msgbox sOpspath  
  
 Set fs = CreateObject("Scripting.FileSystemObject")

  if fs.FileExists(sOpspath) = False then
    msgbox "No File has been generated for today, Please Regenerate The File!"
  else
    
	'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.migou@exchange-data.com -ATTACH " & sOpspath & " -SUB EDI LSTAT Weekly Feed")
     Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO shaun@mbendi.com;brian@mbendi.com;webmaster@exchange-data.com -ATTACH " & sOpspath & " -SUB EDI LSTAT Weekly Feed")
    'if y = 0 then
    '	msgbox "Check LSTAT Email is in Webmaster"
    if y = 1 then
    	msgbox "LSTAT Email Failed. Check Webmaster to Confirm Failure"
    End if	
 end if
  
  
end sub

Sub NLIST_weekly()  
  
'**************************************************************
'	Custom routine for Client number 18
'
   
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without an extension  
  'sCustomName = sYear & sMonth & sDay
  sOpspath= "O:\Datafeed\Bespoke\mbendi\" & sYear & sMonth & sDay & "_NLIST_Weekly.txt"			
  'msgbox sOpspath  
  
 Set fs = CreateObject("Scripting.FileSystemObject")

  if fs.FileExists(sOpspath) = False then
    msgbox "No File has been generated for today, Please Regenerate The File!"
  else
    
	'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.migou@exchange-data.com -ATTACH " & sOpspath & " -SUB EDI LSTAT 620 weekly")
     Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO shaun@mbendi.com;brian@mbendi.com;webmaster@exchange-data.com -ATTACH " & sOpspath & " -SUB EDI NLIST Weekly Feed")
    'if y = 0 then
    '	msgbox "Check LSTAT Email is in Webmaster"
    if y = 1 then
    	msgbox "LSTAT Email Failed. Check Webmaster to Confirm Failure"
    End if	
 end if
  
  
end sub

Sub UnmatchedBondPrice()  
  
'**************************************************************
'	Custom routine for Client number 19
'
   
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without an extension  
  'sCustomName = sYear & sMonth & sDay
  sOpspath= "O:\Datafeed\Prices\Bonds_Unmatched\Bond_Unmatched.txt"		
  'msgbox sOpspath  
  
 Set fs = CreateObject("Scripting.FileSystemObject")

  if fs.FileExists(sOpspath) = False then
    msgbox "No File has been generated for today, Please Regenerate The File!"
  else
    
     'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.migou@exchange-data.com;webmaster@exchange-data.com -ATTACH " & sOpspath & " -SUB Daily Unmatched Bonds")
     Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO k.herbergs@exchange-data.com;s.bhuskute@exchange-data.com;a.mohammed@exchange-data.com;webmaster@exchange-data.com;m.talbi@exchange-data.com;fi-support-EDIA@exchange-data.com -ATTACH " & sOpspath & " -SUB Daily Unmatched Bonds")
   
    'if y = 0 then
    '	msgbox "Check LSTAT Email is in Webmaster"
    if y = 1 then
    	msgbox "Unmatched bond prices failed. Check Webmaster to Confirm Failure"
    End if	
 end if
  
  
end sub


Sub FTP_Dir()
'**************************************************************
'	Custom routine for Client number 20
'
   
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without an extension  
  'sCustomName = sYear & sMonth & sDay
  sOpspath= "O:\AUTO\FTP_DIR\Reports\" & sYear & sMonth & sDay & "_ftpcompare.csv"		
  'msgbox sOpspath  
  
 Set fs = CreateObject("Scripting.FileSystemObject")

  if fs.FileExists(sOpspath) = False then
    msgbox "No File has been generated for today, Please Regenerate The File!"
  else
    
     Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.migou@exchange-data.com;webmaster@exchange-data.com -ATTACH " & sOpspath & " -SUB Weekly FTP List Comparison")
     'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO m.amaning@exchange-data.com;d.ames@exchange-data.com;b.james@exchange-data.com;a.sabourin@exchange-data.com;s.tjongarero@exchange-data.com;webmaster@exchange-data.com -ATTACH " & sOpspath & " -SUB Weekly FTP List Comparison")
   
    'if y = 0 then
    '	msgbox "Check LSTAT Email is in Webmaster"
    if y = 1 then
    	msgbox "Unmatched bond prices failed. Check Webmaster to Confirm Failure"
    End if	
 end if
  
  
end sub

Sub GBDataTeam()
'**************************************************************
'	Custom routine for Client number 22
'
   
  '-- Build custom output file name, ensuring that
  '		both sMonth and sDay are always two digits
  if Len(Month(Now))=1 then 
  	sMonth="0" & month(now)
  else
  	sMonth=month(now)
  end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if

  sYear = Year(Now)

  '-- Create the Custom filename without an extension  
  'sCustomName = sYear & sMonth & sDay
  sOpspath= sAttach & sYear & sMonth & sDay & "." & sExt		
  'msgbox sOpspath  
  
 Set fs = CreateObject("Scripting.FileSystemObject")

  if fs.FileExists(sOpspath) = False then
    msgbox "No File has been generated for today, Please Regenerate The File!"
  else
    
     Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.migou@exchange-data.com;webmaster@exchange-data.com -ATTACH " & sOpspath & " -SUB GBData Email Test")
     'Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO m.amaning@exchange-data.com;d.ames@exchange-data.com;b.james@exchange-data.com;a.sabourin@exchange-data.com;s.tjongarero@exchange-data.com;webmaster@exchange-data.com -ATTACH " & sOpspath & " -SUB Weekly FTP List Comparison")
   
    'if y = 0 then
    '	msgbox "Check GBData Report Email is in Webmaster"
    if y = 1 then
    	msgbox "GBData Report failed. Check Webmaster to Confirm Failure"
    End if	
 end if
  
  
end sub

Sub tHoldSec()  
   
 '**************************************************************
 '	tHoldSec for number 23
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & "-" & sMonth & "-" & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO webmaster@exchange-data.com;y.migou@exchange-data.com -SUB tHoldSec Completed")
 
end sub

Sub WFI_Markit_Russian_New_Issuance()  
   
 '**************************************************************
 '	SecReport for number 24
 '	   
 '
    if Len(Month(Now))=1 then 
  	  sMonth="0" & month(now)
    else
  	  sMonth=month(now)
    end if
  
  if Len(Day(now))=1 then 
  	sDay = "0" & day(Now)
  else
  	sDay = day(Now)
  end if
  'sday="07"

  sYear = Year(Now)
  
  sDate = sYear & sMonth & sDay
   
   ''WScript.Echo sDate
   ''WScript.Quit
   
   Y = wso.Run ("O:\AUTO\Apps\Emailers\gmailer.jar -USER support@exchange-data.com -PASS KASIA1234 -TO y.laifa@exchange-data.com;d.moore@exchange-data.com;Jar.Singh@markit.com;a.frankowska@exchange-data.com -ATTACH O:\Datafeed\Debt\Markit_Weekly\" & sDate & "_RU_List.txt -SUB Russian New Issuance")
   
end sub
