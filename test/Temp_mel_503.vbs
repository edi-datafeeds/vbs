' ****************************************************************
' Mellon 503 vbs
'
' Daily WSO Mon, Wed, Thu, Fri
' Full extract on Tue
'
' ****************************************************************

' **** SUB MAIN ****

	dim fso,f,L1,wso,colArgs,fldr,fcol,lastday,Seclast,CRLF,sSource,sOpspath,sTxtfile,sTargfile,sDatename,sArchpath

	CRLF = Chr(13) & Chr(10)

	'Set colArgs = WScript.Arguments
 
	'sClientID = colArgs(0)
	'sSource = colArgs(1)
	'sOpspath = "o:\worldequ\wsoexp\"
	
 
	'Response = MsgBox(CRLF & "------------------------------------------------------------" & CRLF & CRLF & _
         '                    "              Run Mellon 503 process" & CRLF & CRLF & _
          '                   "------------------------------------------------------------" & CRLF, vbOkCancel)

	'If Response = vbOK Then  
		Set fso = CreateObject("Scripting.FileSystemObject")
		Set wso = Wscript.CreateObject("Wscript.Shell")
	
		'-- Call custom routine by using the sClientID
		'		which was passed via command Line argument(0)
          if weekday(date)=vbMonday then
		Call full503
  	  else
		Call daily503
	  End If
	'End If

        call TidyUp
        
' **** END SUB MAIN ****



'**************************************************************
'	Remove all reference to any object created early in this
'   script
'
Sub TidyUp()
 	'msg = "Operation has completed"
 	'msgbox msg,,"MEL_503.vbs"
 	'if fso.fileexists(sOpspath & sCustomName & ".TXT") then
 	'	fso.DeleteFile(sOpspath & sCustomName & ".TXT")
 	'End If
  Set fso = Nothing
  Set wso = Nothing
End Sub


Sub Full503()  
   
  ' Generate and number Shochseq
  'Y = wso.Run ("O:\AUTO\Apps\exe\isql.exe -S sqlsvr1 -U sa -P -i o:\apps\sql\wso\OPS\shochseq1_gen.sql", 1, TRUE)
  Y = wso.Run ("O:\auto\scripts\vbs\isql.vbs HAT_MS_Sqlsvr1 O:\auto\Scripts\sql\WCA\SedolSeq1_Daily.sql o:\auto\configs\dbservers.cfg WcaWebload_3 Gen_Daily_SedolSeq1_Dump", 1, TRUE)
    
  ' generate output file
  Y = wso.Run ("o:\AUTO\Apps\exe\prod.exe -a -s HAT_MS_sqlsvr1 -f O:\AUTO\Scripts\sql\WSO\503Mel_full.sql -d o:\Datafeed\wso\503MFULL\", 1, TRUE)

 'Validate File
   Y = wso.Run ("O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs o:\Datafeed\wso\503MFULL\PreSDateSuf.503 AL- EODv2",,true)	    
  
  ' zip and ftp 503 full file
  'Y = wso.Run ("O:\apps\rbt\wso\503mfull.bat", 1, TRUE)
  
  'Tidy File
  fso.DeleteFile("o:\Datafeed\WSO\503mfull\MEL_FULL.ZIP")
  
  'Zip File
  Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & "o:\Datafeed\WSO\503mfull\" & " -m " & "MEL_FULL.ZIP" & " -suf 503 -tidy",,true)	 
 
 'Validate ZIP File
   Y = wso.Run ("O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs o:\Datafeed\WSO\503mfull\MEL_FULL.ZIP - EODv2",,true)	 
   
  'FTP DotCom
  Y = wso.Run ("O:\AUTO\Apps\opsftp\dist\opsftp.jar -site dotcom -upload -local o:/Datafeed/WSO/503MFULL/ -remote /Custom/RM/ -fileext .zip -XCRC on -log o:\AUTO\logs\Ftp.log")
 
  'FTP DotNet
  Y = wso.Run ("O:\AUTO\Apps\opsftp\dist\opsftp.jar -site dotnet -upload -local o:/Datafeed/WSO/503MFULL/ -remote /Custom/RM/ -fileext .zip -XCRC on -log o:\AUTO\logs\Ftp.log", 1, TRUE)
 
 
end sub


Sub Daily503()  
   

  ' generate output file
  'Y = wso.Run ("o:\Apps\exe\production_v06.exe -a -s 4 -f o:\Apps\sql\WSO\503Mel_daily.sql", 1, TRUE)
  Y = wso.Run ("o:\AUTO\Apps\exe\prod.exe -a -s HAT_MS_sqlsvr1 -f O:\AUTO\Scripts\sql\WSO\503Mel_daily.sql -d o:\Datafeed\wso\503MELdup\", 1, TRUE)
  
  'Validate File
   Y = wso.Run ("O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs o:\Datafeed\WSO\503MELdup\PreLDateSuf.503 AL- EODv2",,true)	 
    
   ' dedupe
   Y = wso.Run ("O:\apps\vbe\wca_txtdedup_v06_auto.exe o:\datafeed\WSO\503meldup\mel_secid.txt", 1, TRUE)

   'Validate Deduped File
   Y = wso.Run ("O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs o:\Datafeed\wso\503MEL\PreLDateSuf.503 - EODv2",,true)	 

 ' zip and ftp 503 file
  'Y = wso.Run ("O:\apps\rbt\wso\503mel.bat", 1, TRUE)
  
  'Tidy File
   fso.DeleteFile("o:\Datafeed\WSO\503mel\MEL_FULL.ZIP")
    
  'Zip File
   Y = wso.Run ("J:\java\Prog\Release\ZipFile\jar\ZipFile.jar -i " & "o:\Datafeed\WSO\503mel\" & " -m " & "MEL_FULL.ZIP" & " -suf 503 -tidy",,true)	 

  'Validate ZIP File
   Y = wso.Run ("O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs o:\Datafeed\WSO\503mel\MEL_FULL.ZIP - EODv2",,true)	 
   
  'FTP DotCom
   Y = wso.Run ("O:\AUTO\Apps\opsftp\dist\opsftp.jar -site dotcom -upload -local o:/Datafeed/WSO/503MEL/ -remote /Custom/RM/ -fileext .zip -XCRC on -log o:\AUTO\logs\Ftp.log")
   
  'FTP DotNet
   Y = wso.Run ("O:\AUTO\Apps\opsftp\dist\opsftp.jar -site dotnet -upload -local o:/Datafeed/WSO/503MEL/ -remote /Custom/RM/ -fileext .zip -XCRC on -log o:\AUTO\logs\Ftp.log", 1, TRUE)
 
  

end sub



