'**************************************************************************
'
' 		     Generic File Archiving Programme
'
' Author Scott Kitchingham
' 
' Script for ..
'
' Arguments in order (0) SourceFolder e.g. n:\prices\
'		     (1) TargetFolder e.g. r:\prices\
'		     (2) FileAge = 40
'                    (3) Mode = Arch or NoArch
'                    
'
'**************************************************************************

'***** VARIABLES *****

option Explicit

'Script Arguments
Dim SourceFolder, TargetFolder, Fileage, Mode

'General use FileSystemObject
Dim oFS1, oFS1_Folder, oFS1_File, fred

'Log FileSystemObject			  
Dim oFS2, oFS2_File

'Counters
Dim iFilesCopied,iFoldersCreated,iAlreadyCopied,iBadCopies,iFilesDeleted,iByteSize,iTotalBytesize, oFS3_File,f

'Strings
Dim sMonth, sDay, sYear, CRLF, Response, sYYYYMMDD

Const ForReading = 1, ForWriting = 2, ForAppending = 8

Const auto="Yes", ByteSize = 0, ExtensionsToDelete="avi,mp3,wma", ResultLog="Lite"

'***** PREPARATION *****

'Checks to see if the correct number of parameters have been set
if WScript.Arguments.Count < 4 Then
  If auto<>"Yes" Then
      wscript.echo "There are less than 4 Arguments, CANNOT CONTINUE."
  Else
     If ResultLog<>"Verbose" Then
     Else
       AppendLog "There are less than 4 Arguments, CANNOT CONTINUE."
     End If
  End If
  WScript.Quit(1)	
End if

'Load Argument data
SourceFolder = WScript.Arguments.Item(0)
TargetFolder = WScript.Arguments.Item(1)
FileAge = WScript.Arguments.Item(2)
Mode = WScript.Arguments.Item(3)

'Zero counters
iFilesCopied = 0
iFoldersCreated = 0
iAlreadyCopied = 0
iBadCopies = 0
iFilesDeleted = 0
iByteSize = 0
iTotalBytesize = 0

'Set carriage return + line feed string
'ExtensionsToDelete=""

CRLF = Chr(13) & Chr(10)

'Checks to see if root folders exists
Set oFS1= CreateObject("Scripting.FileSystemObject")
If oFS1.FolderExists(SourceFolder) Then
Else
    If auto<>"Yes" Then
        wscript.echo "The Source folder doesn't exist, CANNOT CONTINUE."
        WScript.Quit
    Else
       If ResultLog<>"Verbose" Then
       Else
         AppendLog "The Source folder doesn't exist, CANNOT CONTINUE."
       End If
    End If
End If

If Mode = "arch" Then
    If oFS1.FolderExists(TargetFolder) Then
    Else
        If auto<>"Yes" Then
            wscript.echo "The Target folder doesn't exist, CANNOT CONTINUE."
            WScript.Quit
        Else
           If ResultLog<>"Verbose" Then
           Else
             AppendLog "The Target folder doesn't exist, CANNOT CONTINUE."
           End If
        End If	
    End If
End If

'Setup log file datestring
if Len(Month(Now))=1 then 
    sMonth="0" & month(now)
else
    sMonth=month(now)
end if
if Len(Day(now))=1 then 
    sDay = "0" & day(Now)
else
    sDay = day(Now)
end if
sYear = Year(Now)
sYYYYMMDD = sYear & sMonth & sDay

'Initialise log
Set oFS2 = CreateObject("Scripting.FileSystemObject")
Set oFS2_File = oFS2.OpenTextFile("n:\FileCull_Logs\" & replace(replace(sourcefolder,":\","_"),"\","_") & "_" & fileage & ".csv", ForAppending, True)
AppendLog "Potential deletions from root path = " & sourcefolder
AppendLog "For files older than days = " & fileage
AppendLog ""

'***** MAIN *****
 
Set oFS1_Folder = oFS1.GetFolder(SourceFolder)
If Mode = "arch" Then
    ArchLevel oFS1_Folder
Else
    CullLevel oFS1_Folder    
End If

'***** FINISH *****

iTotalBytesize=FormatNumber(iTotalBytesize,,,,vbTrue)

If auto<>"Yes" Then
    'Display file stats
    Response = MsgBox(CRLF & "------------------------------------------------------------" & CRLF & CRLF & _
                          "              Files copied   : " & iFilesCopied & CRLF & CRLF & _
                          "              Folders created: " & iFoldersCreated & CRLF & CRLF & _
                          "              Already Copied : " & iAlreadyCopied & CRLF & CRLF & _
                          "              Bad Copies     : " & iBadCopies & CRLF & CRLF & _
                          "              Files Deleted  : " & iFilesDeleted & CRLF & CRLF & _
                          "              ByteSize       : " & iTotalBytesize & CRLF & CRLF & _
                          "------------------------------------------------------------" & CRLF, vbOk)
End If
                         
appendLog "------------------------------------------------------------"
appendLog "              Files copied   : " & iFilesCopied
appendLog "              Folders created: " & iFoldersCreated
appendLog "              Already Copied : " & iAlreadyCopied
appendLog "              Bad Copies     : " & iBadCopies
appendLog "              Files Deleted  : " & iFilesDeleted
appendLog "              ByteSize       : " & iTotalBytesize
appendLog "------------------------------------------------------------"

set oFS1_File = nothing
set oFS1_Folder = nothing
set oFS1 = nothing

'Shut log file
oFS2_File.Close
set oFS2 = nothing

WScript.Quit



'***** SUBROUTINES *****

Sub CullLevel(Folder)
DIM Subfolder, oFS3_Folder, oFS3_Files, ExtensionWanted, FileExtension
    For Each Subfolder in Folder.SubFolders
        '** recursive SOURCE folder
        Set oFS3_Folder = oFS1.GetFolder(Subfolder.Path)
        Set oFS3_Files = oFS3_Folder.Files
        'Wscript.Echo Subfolder
        For Each oFS3_File in oFS3_Files
            iByteSize=oFS3_File.size
	    ExtensionWanted="No"
            for each FileExtension in SPLIT(UCASE(ExtensionsToDelete),",")
	    	if FileExtension = ucase(oFS1.GetExtensionName(oFS3_File.Path)) then
	    	    ExtensionWanted="No"
	    	    'wscript.echo oFS3_File.Path
	    	end if    
	    next
            If dateDiff("d", oFS3_File.DateLastModified, Date) > int(fileage) and iByteSize >= ByteSize and ExtensionWanted="No" Then
                If auto<>"Yes" Then
                    Wscript.Echo "Deleting file from source archive " & oFS3_File.Path
                    'WScript.quit
                Else
                    If ResultLog<>"Verbose" Then
                    Else
                      AppendLog "Deleting file from source archive " & oFS3_File.Path
                    End If
                End If
                If ResultLog<>"Verbose" Then
                Else
                    AppendLog oFS3_File.DateLastModified & "," & chr(34) & oFS3_File.Path & chr(34) & "," & iByteSize
                End If
                '** DELETE ACTUAL SOURCE FILE
                oFS3_File.Delete
                iTotalBytesize=iTotalBytesize+iByteSize
                iFilesDeleted = iFilesDeleted + 1
                '**
            End If
        Next
        CullLevel Subfolder
    Next
End Sub


Sub ArchLevel(Folder)
DIM Subfolder, oFS3_Folder, oFS3_File, oFS3_Files, TargetFile
    For Each Subfolder in Folder.SubFolders
        '** recursive SOURCE folder
        Set oFS3_Folder = oFS1.GetFolder(Subfolder.Path)
        Set oFS3_Files = oFS3_Folder.Files
        '** Wscript.Echo Subfolder
        If oFS1.FolderExists(replace(Subfolder.Path, SourceFolder, TargetFolder)) Then
            '** do nothing
        Else
            '** create target folder if not exist
            oFS1.CreateFolder(replace(Subfolder.Path, SourceFolder, TargetFolder))
            iFoldersCreated = iFoldersCreated +1
        End If
        '** loop through each SOURCE file in current folder
        For Each oFS3_File in oFS3_Files
            iByteSize=oFS3_File.size
            If dateDiff("d", oFS3_File.DateLastModified, Date) > int(fileage) Then
                 '** archive condition are met
                 TargetFile = replace(oFS3_File.Path, SourceFolder, TargetFolder)
                 If oFS1.FileExists(TargetFile) Then
                     '** file already exists !!
		     oFS1.DeleteFile(TargetFile)
                     If ResultLog<>"Verbose" Then
                     Else
                       AppendLog "File has already been copied: " & oFS3_File.Path
                     End If
                     '** Overwrite anyway
		     oFS1.CopyFile oFS3_File.Path, TargetFile
                     iAlreadyCopied = iAlreadyCopied +1
                     If ResultLog<>"Verbose" Then
		     Else
		         AppendLog "Deleting already copied file from source archive " & oFS3_File.Path
		     End If
		     '** DELETE ACTUAL SOURCE FILE ANYWAY
		     oFS3_File.Delete
		     '**
		     iTotalBytesize=iTotalBytesize+iByteSize
		     iFilesDeleted = iFilesDeleted + 1
                 Else
                     On Error Resume Next
                     '** Copy file to archive
                     oFS1.CopyFile oFS3_File.Path, TargetFile
                     '**
                     If Err.Number<>0 Then
                         iBadCopies = iBadCopies +1
                         If ResultLog<>"Verbose" Then
                         Else
                             AppendLog "File copy problem " & err.number, err.description & " " & oFS3_File.Path
                         End If
                     Else
                         iFilesCopied = iFilesCopied +1
                         '** double check that file exists
                         If oFS1.FileExists(TargetFile) Then
                             If ResultLog<>"Verbose" Then
                             Else
                                 AppendLog "Deleting file from source archive " & oFS3_File.Path
                             End If
                             '** DELETE ACTUAL SOURCE FILE
                             oFS3_File.Delete
                             '**
                             iTotalBytesize=iTotalBytesize+iByteSize
                             iFilesDeleted = iFilesDeleted + 1
                         Else
                             '** failed double check !!
                             If ResultLog<>"Verbose" Then
                             Else
                               AppendLog "Target File does not exist on double check!!: "  & TargetFile
                             End If
                             iBadCopies = iBadCopies +1
                         End If
                    End If
                End If
            End If
        Next
        ArchLevel Subfolder
   Next
End Sub


					  
'*********************************

'Return the length of a file or -1 if it does not exist

function GetFileSize()
  DIM fs
  GetFileSize = -1

  Set fs = CreateObject("Scripting.FileSystemObject")

  if fs.FileExists(oFS3_File.Path) = True then
    set f = fs.GetFile(oFS3_File.Path)
    GetFileSize = f.size
  end if
  if fs.FolderExists(oFS3_File.Path) = True then
    set f = fs.GetFolder(oFS3_File.Path)
    GetFileSize = f.size
  end if

Set f = Nothing
Set fs = Nothing
end function


'********************************

Sub AppendLog(ByVal MessageTxt)
oFS2_File.Write MessageTxt & vbcrlf
End Sub
